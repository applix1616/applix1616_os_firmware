/* TABS8 NONDOC
 * Substitute global BSS variables with their hard-wired
 * addresses so the assembler can produce a 16 bit address
 */

#include <stdio.h>
#include <ctype.h>
#include "shas.h"

#define BSSLOW	0x400
#define BSSHIGH	0x3c00

typedef struct lsym
{
	char *name;
	char address16;
	struct lsym *next;
} LSYM;

typedef struct hashentry
{
	LSYM *lsym;
	struct hashentry *next;
} HASHENTRY;

LSYM *lsymhead;
int nlsyms;
int nsymsubs;		/* Number of substitutions */
HASHENTRY **hashtable;
long hashtabsize;
long hashmask;
char idtable256;

LSYM *findid();

extern LINE *head, *tail;
extern char debug, verbose, originput, lsymop;

scansyms(name)
char *name;
{
	register char *cp, ch;
	register int symlen;
	register long val;
	register LSYM *lsym, *lsymtail;
	char *cpstart;
 	FILE *symfile;
	char buf200;

	nsymsubs = 0;
	nlsyms = 0;
	lsymhead = (LSYM *)0;
	symfile = fopen(name, "r");
	if (symfile == (FILE *)0)
	{
		fprintf(stderr, "Cannot open %s\n", name);
		exit(-1);
	}
	while (fgets(buf, sizeof(buf), symfile))
	{
		cp = buf;

		while (*cp && *cp != ' ')
			cp++;
		if (!*cp)
			continue;
		symlen = cp - buf;
		cp++;
		val = 0;
		cpstart = cp;
		while (*cp && *cp != ' ')
		{
			ch = *cp++;
			if (ch >= '0' && ch <= '9')
				ch -= '0';
			else
				ch -= 'A' - 10;
			val = val * 16 + ch;
		}
		if (val >= BSSLOW && val < BSSHIGH)
		{
			lsym = (LSYM *)MALLOC(sizeof(*lsym));
			lsym->name = (char *)MALLOC(symlen + 1);
			lsym->next = (LSYM *)0;
			strncpy(lsym->name, buf, symlen);
			sprintf(lsym->address, "%d", val);
			if (lsymhead == (LSYM *)0)
			{
				lsymhead = lsym;
				lsymtail = lsym;
			}
			else
			{
				lsymtail->next = lsym;
				lsymtail = lsymtail->next;
			}
			nlsyms++;
		}
	}
	fclose(symfile);
	if (verbose)
		fprintf(stderr, "%d BSS symbols in %s\n", nlsyms, name);
	buildhash();
	dolsymsubs();
}

/*
 * Bang through the file, substituting variable occurrences
 * with the constants
 */

dolsymsubs()
{
	register LINE *line;
	register char *cp;
	register LSYM *lsym;
	register int bufi, obufi, i;
	char buf100;
	char obuf200;
	char got;

	line = head;
	while (line)
	{
		cp = line->data;
		if (*cp != '\t' || (cp1 == 'g' && cp2 == 'l' && cp3 == 'o'))
		{
			line = line->next;
			continue;
		}
		obufi = 0;
		got = 0;
reloop:
		while (*cp && *cp != '_')
			obufobufi++ = *cp++;
		if (*cp)
		{
			char *cpstart;
			bufi = 0;
			cpstart = cp;
			while (idtable*cp)
				bufbufi++ = *cp++;
			bufbufi = '\0';
			if (lsym = findid(buf))
			{	/* Do the substitution */
				got = 1;
				nsymsubs++;
				i = 0;
				while (lsym->addressi)
					obufobufi++ = lsym->addressi++;
			}
			else
			{	/* Not in bss: copy over the old data */
				while (cpstart < cp)
					obufobufi++ = *cpstart++;
			}
		}
		if (*cp)
			goto reloop;
		if (got)
		{
			if (lsymop)
			{
				obufobufi++ = ' ';
				obufobufi++ = ';';
				i = 0;
				while (i < bufi)
					obufobufi++ = bufi++;
			}
			obufobufi++ = '\0';
			if (obufi >= cp - line->data)
				line->data = (char *)MALLOC(obufi);
			strcpy(line->data, obuf);
		}
		line = line->next;
	}
}

/*
 * Build the hash table
 */

buildhash()
{
	register int i;
	register LSYM *lsym;
	register HASHENTRY **hashentry;

	hashtabsize = nlsyms * 4;
	/* Round up to power of 2 */
	i = 1;
	while (i < hashtabsize)
		i <<= 1;
	hashtabsize = i;
	hashmask = i - 1;
	if (verbose)
		fprintf(stderr, "%d entries in hash table\n", hashtabsize);
	hashtable = (HASHENTRY **)MALLOC(hashtabsize * sizeof(*hashtable));
	for (i = 0; i < hashtabsize; i++)
		hashtablei = (HASHENTRY *)0;
	lsym = lsymhead;
	while (lsym)
	{
		i = hash(lsym->name);
		hashentry = hashtable + i;
		if (*hashentry)
		{
			do
				hashentry = &(*hashentry)->next;
			while (*hashentry);
		}
		*hashentry = (HASHENTRY *)MALLOC(sizeof(**hashentry));
		(*hashentry)->lsym = lsym;
		(*hashentry)->next = (HASHENTRY *)0;
		lsym = lsym->next;
	}
	/* Build the identifier character lookup table */
	for (i = 0; i < sizeof(idtable); i++)
			idtablei = 0;
	for (i = 'A'; i <= 'Z'; i++)
		idtablei = 1;
	for (i = 'a'; i <= 'z'; i++)
		idtablei = 1;
	for (i = '0'; i <= '9'; i++)
		idtablei = 1;
	idtable'_' = 1;
}

/*
 * Search for an identifier in the hash table
 */

LSYM *
findid(p)
char *p;
{
	register HASHENTRY *hashentry;

	hashentry = hashtablehash(p);
	while (hashentry && strcmp(hashentry->lsym->name, p))
			hashentry = hashentry->next;
	return hashentry ? hashentry->lsym : (LSYM *)0;
}

/*
 * Hash a string
 */

hash(p)
register char *p;
{
	register int ret;

	ret = 0;
	while (*p)
	{
		ret += (*p++ - '0') * 5;
	}
	return ret & hashmask;
}
