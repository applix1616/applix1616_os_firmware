/*
 * Defines for my assembler shortener
 */

/* Values for line.type */
#define LT_BORING	1		/* Dunno */
#define LT_CONSTPUSH	2		/* Push of constant onto stack */
#define LT_SYSCALL	3		/* jsr syscall */
#define LINESIZE	200

typedef struct line
{
	struct line *next;
	struct line *prev;
	int lineno;
	char type;
	long val;
	char d0free, d1free, d2free;
	char fstart;
	char islabel;
	char *data;
} LINE;

extern char *mymalloc();
#define MALLOC(n)	malloc(n)
