/*	TABS4
 * Assembler post-processor
 */

#include <stdio.h>
#include <ctype.h>
#include "shas.h"

char *ourname;
FILE *infile;

LINE *head, *tail;
char debug, verbose, originput, lsymop;
char dosyscalls, doconstpushes, doanymove;
char doa0;
int totsaved;
int nsyscalls;
int a0saves;
int nconstpushes;
char *lsymname;

char *malloc();

extern int nsymsubs;

main(argc, argv)
char *argv;
{
	int arg;
	char *cp;
	char *fname;

	ourname = argv0;

	if (argc < 2)
		usage();

	fname = 0;
	verbose = debug = originput = 0;
	dosyscalls = doconstpushes = 0;
	nsyscalls = nconstpushes = 0;
	doanymove = doa0 = 0;
	lsymop = 0;
	totsaved = 0;
	a0saves = 0;
	lsymname = (char *)0;

	for (arg = 1; arg < argc; arg++)
	{
		cp = argvarg;
		if (*cp == '-')
		{
			switch (*++cp)
			{
			case 'a':
				doa0++;
				break;
			case 'L':
				lsymop++;
				break;
			case 'd':
				debug++;
				break;
			case 'l':
				cp++;
				if (!*cp)
				{
					if (++arg == argc)
						usage();
					cp = argvarg;
				}
				lsymname = cp;
				break;
			case 'm':
				doanymove++;
				break;
			case 'o':
				originput++;
				break;
			case 'p':
				doconstpushes++;
				break;
			case 's':
				dosyscalls++;
				break;
			case 'v':
				verbose++;
				break;
			default:
				usage();
			}
		}
		else
		{
			if (fname)
				usage();
			else
				fname = cp;
		}
	}

	infile = (fname) ? fopen(fname, "r") : stdin;

	if (infile == 0)
	{
		fprintf(stderr, "Cannot open %s", fname ? fname : "stdin");
		perror("");
		exit(-1);
	}
	readfile();
	if (fname)
		fclose(infile);
	if (lsymname)
		scansyms(lsymname);
	if (doa0)
		doa0saves();
	scanfile();
	scand1();

	doit();

	fflush(stdout);

	if (verbose)
	{
		fprintf(stderr, "%s: %d syscalls, %d const pushes, %d BSS substitutions, %d a0 saves, %d bytes saved\n",
			fname ? fname : "stdin", nsyscalls, nconstpushes, nsymsubs,
				a0saves, a0saves * 2 + nsymsubs * 2 + totsaved);
	}
	exit(0);
}

/*
 * Process the lines.
 *
 *	Look for:
 *		move.l	#NN,-(a7)
 *		jsr	_syscall
 *		add.l	#MM,A7
 *	convert to
 *		dc.w	$a000+NN
 *		add.l	#MM-4,A7	(if MM != 4)
 *
 *	Look for:
 *		move.l	#NN,-(a7)
 *	convert to
 *		moveq.l	#NN,dN
 *		move.l	dN,-(a7)
 */

int scplength;

doit()
{
	register LINE *line;
	register int pushval;
	int adjustval;
	register char *data;
	register char *ptr;
	char *destaddr;
	int sign, lineno;
	char isconstpush;
	char shortconstmove;
	char opcode100;

	line = head;
	lineno = 0;
	while (line)
	{
		lineno++;
		data = line->data;
		isconstpush = 0;
		shortconstmove = 0;

		if (scp("\tmove.l\t#", data) && (dosyscalls || doconstpushes))
		{	/* Work out if it is a constant push */
			if (debug > 1)
				fprintf(stderr, "%d: move.l match\r\n", lineno);
			ptr = data + scplength;
			strncpy(opcode, data, ptr - data - 1);
			opcodeptr - data - 1 = '\0';
			pushval = 0;
			if (*ptr == '-')
			{
				sign = -1;
				ptr++;
			}
			else
				sign = 1;
			while (isdigit(*ptr))
				pushval = pushval * 10 + *ptr++ - '0';
			pushval *= sign;
			destaddr = ptr;
			if (doconstpushes && scp(",-(a7)", ptr) || scp(",-(sp)", ptr))
			{
				isconstpush = 1;
				if (debug > 1)
					fprintf(stderr, "%d: Const push of %d\n", lineno, pushval);
			}
			else
				isconstpush = 0;
		}

		if (doanymove && !isconstpush && (*data == '\t') &&
				!scp("\tcmp", data))
		{
			/* Look for a constant move */
			char isaddsub;

			ptr = data + 1;
			while (*ptr && *ptr != '.')
				ptr++;
			if (*ptr && scp(".l\t#", ptr))
			{	/* Shorten any move */
				isaddsub = scp("\tadd", data) || scp("\tsub", data);
				ptr += 3;
				strncpy(opcode, data, ptr - data);
				opcodeptr - data = '\0';	/* strncpy doesn't do this! */
				ptr++;
				pushval = 0;
				if (*ptr == '-')
				{
					sign = -1;
					ptr++;
				}
				else
					sign = 1;
				while (isdigit(*ptr))
					pushval = pushval * 10 + *ptr++ - '0';
				pushval *= sign;
				destaddr = ptr;
				if ((*ptr == ',') && !scp(",a", ptr) && !scp(",d", ptr))
				{	/* Not a move to a register */
					if (!(isaddsub && pushval > 0 && pushval <= 8))
					{	/* Not an addq or subq */
						shortconstmove = 1;
					}
				}
			}
		}

		if (dosyscalls && isconstpush)
		{
			/* Look for syscall */
			data = line->next->data;
			if (scp("\tjsr\t_syscall", data))
			{
				if (debug > 1)
					fprintf(stderr, "%d: syscall\n", lineno + 1);
			}
			else
				goto notsyscall;

			/* Look for the stack adjust */
			data = line->next->next->data;
			if (scp("\tadd.l\t#", data))
			{
				if (debug > 1)
					fprintf(stderr, "%d: stack adjust?\r\n", lineno + 2);
				ptr = data + scplength;
				adjustval = 0;
				if (*ptr == '-')
				{
					sign = -1;
					ptr++;
				}
				else
					sign = 1;
				while (isdigit(*ptr))
					adjustval = adjustval * 10 + *ptr++ - '0';
				pushval *= sign;
				if (debug > 1)
					fprintf(stderr, "adjustval=%d\r\n", adjustval);
				if (scp(",a7", ptr) || scp(",sp", ptr))
				{	/* Got it! */
					if (originput)
					{
						printf(";%s\n;%s\n;%s\n",
							line->data, line->next->data,
							line->next->next->data);
					}
					printf("\tdc.w\t$%x\t; #%d\n", 0xa000 + pushval, pushval);
					totsaved += 10;
					if (adjustval != 4)
					{
						printf("\tadd.l\t#%d,a7\n", adjustval - 4);
						if (adjustval == 12)
							totsaved += 2;	/* Can do addq */
					}
					else
						totsaved += 2;		/* Saved instruction */
					/* Move onto add instruction */
					line = line->next->next->next;
					lineno += 2;
					nsyscalls++;
					continue;
				}
			}
		}
notsyscall:
		/* syscall didn't fit: try for a constant push */
		if (debug > 2)
			fprintf(stderr, "Not a syscall\n");

		/* Can we move it with moveq? */
		if ((doconstpushes && isconstpush) || shortconstmove)
		{
			if (pushval >= -128 && pushval <= 127 &&
				(line->d0free || line->d1free || line->d2free))
			{	/* Can use a moveq */
				int reg;
				if (line->d0free) reg = 0;
				else if (line->d1free) reg = 1;
				else reg = 2;
				if (debug > 2)
					fprintf(stderr, "%d: short push\n", lineno);
				if (originput)
					printf(";%s\n", line->data);
				printf("\tmoveq.l\t#%d,d%d\t; akpm\n", pushval, reg);
				printf("%sd%d%s\n", opcode, reg, destaddr);
				nconstpushes++;
				totsaved += 2;
				line = line->next;
				continue;
			}
		}
		if (originput)
			printf("%s\t\t; 0:%c 1:%c 2:%c \n", line->data,
				line->d0free ? 'F' : 'U', line->d1free ? 'F' : 'U',
					line->d2free ? 'F' : 'U');
		else
			printf("%s\n", line->data);
		line = line->next;
	}
}

/*
 * Read the file in
 */

readfile()
{
	char bufLINESIZE;
	register LINE *temp;
	register char *cpin, *cpout;
	int lineno = 0;

	tail = 0;
	head = 0;
	while (fgets(buf, LINESIZE, infile))
	{
		temp = (LINE *)MALLOC(sizeof(*temp));
		temp->data = MALLOC(strlen(buf) + 1);
		temp->lineno = ++lineno;
		if (!head)
			head = temp;
		else
			tail->next = temp;
		temp->type = LT_BORING;
		temp->d0free = 0;
		temp->d1free = 0;
		temp->d2free = 0;
		cpin = buf;
		cpout = temp->data;
		while (*cpin && *cpin != '\n')
			*cpout++ = *cpin++;
		*cpout = 0;
		temp->prev = tail;
		temp->next = 0;
		temp->fstart = (temp->data0 == '_');
		temp->islabel = (temp->data0 != '\t');
		tail = temp;
	}
}

/*
 * Scan across the file, working out where d1 can be safely used
 */

scanfile()
{
	register LINE *line;
	register char d0free, d1free, d2free;
	register char *cp;

	d0free = 0;
	d1free = 0;
	d2free = 0;
	line = tail;
	while (line)
	{
		line->d0free = d0free;
		line->d1free = d1free;
		line->d2free = d2free;

		if (scp("\tjsr\t_", line->data))
		{	/* d1 free before C jsr */
			d1free = 1;
		}
		else if (d0free || d1free || d2free)
		{	/* If dN appears in the operands or there
			 * is a branch or a jump, unfree dN.
			 */

			if (scp("\tb", line->data))
			{
				d0free = 0;
				d1free = 0;
				d2free = 0;
			}
			else
			{
				cp = line->data;
				while (*cp)
				{
					while (*cp && (*cp != 'd') && (*cp != 'D'))
						cp++;
					if (*cp)
					{
						switch (cp1)
						{
						case '0':
							d0free = 0; break;
						case '1':
							d1free = 0; break;
						case '2':
							d2free = 0; break;
						}
					}
					cp++;
				}
			}
		}
		line = line->prev;
	}
}

/*
 * Scan for functions, seeing if d1 is used anywhere
 */

#define STARTING	1
#define INGOODFUNC	2
#define INBADFUNC	3

scand1()
{
	register LINE *line, *startline, *prevline;
	char state;

	state = STARTING;
	startline = (LINE *)0;
	line = head;
	while (line)
	{
		if (line->fstart)
		{	/* Start of a function */
			if (state == INGOODFUNC)
				markd1(startline, line);	/* Mark d1 as free throughout */
			startline = line;
			state = INGOODFUNC;
		}
		else
		{
			register char *cp;
			cp = line->data;
			while (*cp)
			{
				if ((*cp == 'D' || *cp == 'd') && cp1 == '1')
				{
					state = INBADFUNC;
					break;
				}
				cp++;
			}
		}
		prevline = line;
		line = line->next;
	}
	if (state == INGOODFUNC)
		markd1(startline, prevline);
}

markd1(s, e)
register LINE *s, *e;
{
	while (s != e)
	{
		s->d1free = 1;
		s = s->next;
	}
}

usage()
{
	fprintf(stderr, "Usage: %s -Ladmopsv -llsymfile filename\n", ourname);
	exit(-1);
}

/*
 * Compare s1 with s2, stopping at end of s1. Return 1 on match
 */

scp(s1, s2)
register char *s1, *s2;
{
	scplength = 0;
	while (*s1)
	{
		if (toupper(*s1++) != toupper(*s2++))
			return 0;
		scplength++;
	}
	return 1;
}

char *
mymalloc(n)
register n;
{
	char *ret;
	register char *p;
	ret = (char *)malloc(n);
	if ((int)ret < 1)
	{
		fprintf(stderr, "Out of memory\n");
		exit(-1);
	}
	p = ret;
	while (n--)
		*p++ = 0;
	return ret;
}

#define HUNK	0x1000

char *
malloc(n)
{
	char *xsbrk();
	static char *brkend = (char *)0;
	static char *nextret;
	register char *p;
	char *ret;

	if (brkend == (char *)0)
	{	/* First time */
		nextret = xsbrk(HUNK);
		brkend = nextret + HUNK;
	}

	if (n & 1)
		n++;

	while ((brkend - nextret) < n)
		brkend = xsbrk(HUNK) + HUNK;

	ret = nextret;
	nextret += n;
	p = ret;
	while (n--)
		*p++ = 0;
	return ret;
}

char *
xsbrk(n)
{
	extern char *sbrk();
	char *ret;

	ret = sbrk(n);
	if ((int)ret < 1)
	{
		fprintf(stderr, "Out of memory\n");
		exit(-1);
	}
	return ret;
}

free()
{}

realloc()
{
	fprintf(stderr, "REALLOC\n");
	exit(-1);
}
