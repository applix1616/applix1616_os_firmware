/* TABS4 NONDOC
 * Replace the redundant a0 moves
 */

#include <stdio.h>
#include "shas.h"

extern LINE *head, *tail;
extern char debug, verbose, originput;
extern int totsaved;
extern int a0saves;
extern int scplength;

doa0saves()
{
	register LINE *line, *line2;
	register char *data, *cp;
	char reg;

	line = head;
	while (line)
	{
		data = line->data;
		if (scp("\tmove.l\tA", data))
		{
			cp = data + scplength;
			reg = *cp;
			if (reg >= '3' && reg <= '6' && scp(",A0", cp + 1))
			{
				/*
				 * Scan forwards to see if register is varied
				 * before use
				 */
				line2 = line->next;
				while (line2)
				{
					char got, indest;
					char buf100;

					if (line2->islabel)
						goto nogo;
					cp = line2->data;
					if (cp1 == 'b')		/* Branch */
						goto nogo;
					/* Is this rgister used? */
					while (*cp)
					{
						if (*cp == 'A' && cp1 == reg)
								goto nogo;
						cp++;
					}
					/* Is it a ref to A0? */
					got = 0;
					strcpy(buf, line2->data);
					cp = buf;
					indest = 1;
					while (*cp)
					{
						if (*cp++ == ',')
						{
							indest = 0;
							break;
						}
					}
					cp = buf;
					while (*cp)
					{
						if (*cp == ',')
							indest = 1;
						if (*cp == 'A' && cp1 == '0')
						{
							if (indest)
								goto nogo;
							*cp = 'a';		/* Mark as changed */
							cp1 = reg;
							got = 1;
						}
						cp++;
					}
					if (got)
					{
						/* Unlink old instruction */
						strcpy(line2->data, buf);
						line->prev->next = line->next;
						line = line2;
						a0saves++;
						goto nogo;
					}
					line2 = line2->next;
				}
			}
		}
nogo:
		line = line->next;
	}
}
