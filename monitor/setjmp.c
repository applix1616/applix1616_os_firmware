#include "config.h"
#include "stddefs.h"
#include "cpu.h"

#define STACK_ADJUST 20

setjmp(env)
struct jmp_struct  *env;
{
    ulong   sr, *sp;

    /* Copy A2-A5, D2-D7, FP, USP, SSP & PC to env structure. */
    /* Recall that %a6 = %fp and %a7 = %sp. */

    asm("movl a2,%0" : "=a" (env->A2));
    asm("movl a3,%0" : "=a" (env->A3));
    asm("movl a4,%0" : "=a" (env->A4));
    asm("movl a5,%0" : "=a" (env->A5));
    asm("movl d2,%0" : "=a" (env->D2));
    asm("movl d3,%0" : "=a" (env->D3));
    asm("movl d4,%0" : "=a" (env->D4));
    asm("movl d5,%0" : "=a" (env->D5));
    asm("movl d6,%0" : "=a" (env->D6));
    asm("movl d7,%0" : "=a" (env->D7));

    asm("mov.w %sr,%d0");
    asm("movl d0,%0" : "=a" (sr));

    asm("mov.l %sp, %a0");
    asm("movl a0,%0" : "=a" (sp));

    if (sr & (ulong)0x2000) {
        env->SSP = (ulong)sp;
        env->SSP += STACK_ADJUST;       /* account for the setjmp stack frame */

        asm("mov.l %usp, %a0");
        asm("movl a0,%0" : "=a" (env->USP));
    }
    else {
        env->USP = (ulong)sp;
        env->USP += STACK_ADJUST;       /* account for the setjmp stack frame */
    }
    sp += 3;
    env->FP = *sp;
    sp++;
    env->PC = *sp;

    return(0);                      /* The call to setjmp MUST return 0 */
}

longjmp(env,retval)
struct jmp_struct   *env;
int retval;
{
    // Load A1 iwth the env pointer and D0 with the content of retval.
    asm("mova.l 0x8(fp),a1");
    asm("mov.l 0xc(fp),d0");

    // At this point it is safe to modify FP, but A1 and D0 should be
    // left untouched.

    // See if we are in supervisor mode.  If yes, then restore USP and
    // SP; else just restore SP...
    asm("mov.w sr,d1");
    asm("andi.l &0x2000,%d1");
    asm("tst.l  d0");
    asm("beq  notsuper");
    asm("mova.l 0x8(a1),a0");
    asm("mov.l a0,usp");
    asm("notsuper:");
    asm("mova.l 0x4(a1),sp");

    // Now use indexing off of A1 to load each of the registers from the
    // environment pointed to by env...
    asm("mova.l 0x10(a1),a2");
    asm("mova.l 0x14(a1),a3");
    asm("mova.l 0x18(a1),a4");
    asm("mova.l 0x1c(a1),a5");
    asm("mova.l 0xc(a1),fp");
    asm("mov.l 0x20(a1),d2");
    asm("mov.l 0x24(a1),d3");
    asm("mov.l 0x28(a1),d4");
    asm("mov.l 0x2c(a1),d5");
    asm("mov.l 0x30(a1),d6");
    asm("mov.l 0x34(a1),d7");

    // Finally, load a0 with the PC that is pointed to by the environment
    // and jump to that address...
    asm("mova.l 0x0(a1),a0");
    asm("jmp (a0)");
}

