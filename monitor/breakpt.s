    .file   "breakpt.s"

#include "asm68k.h"

/************************************************************************/
/* at_hdlr:                                                             */
/* Save registers and jump to attrap.                                   */
/* Note: %a7 and %sp are the same thing.                                */
/************************************************************************/
    .text
    .global at_hdlr
at_hdlr:
    mov.l   a0,(regtbl+A0)  /* Store all registers */
    mov.l   a1,(regtbl+A1)
    mov.l   a2,(regtbl+A2)
    mov.l   a3,(regtbl+A3)
    mov.l   a4,(regtbl+A4)
    mov.l   a5,(regtbl+A5)
    mov.l   a6,(regtbl+A6)
    mov.l   d0,(regtbl+D0)
    mov.l   d1,(regtbl+D1)
    mov.l   d2,(regtbl+D2)
    mov.l   d3,(regtbl+D3)
    mov.l   d4,(regtbl+D4)
    mov.l   d5,(regtbl+D5)
    mov.l   d6,(regtbl+D6)
    mov.l   d7,(regtbl+D7)
    mov.l   sp,(regtbl+SS)
    mov.l   2(a7),a0
    mov.l   a0,(regtbl+PC)
    mov.w   (a7),a0
    mov.l   a0,(regtbl+SR)
    mov.l   usp,a0
    mov.l   a0,(regtbl+US)
    jsr attrap              /* Execute high level trap handler. */


/************************************************************************/
/*  async_brk_hdlr:                                                     */
/*  Save registers and jump to asyncbrk.                                */
/*  Used by the debug uart's break interrupt and any other externally   */
/*  generated asynchronous breakpoint.                                  */
/************************************************************************/

    .text
    .global async_brk_hdlr
async_brk_hdlr:
    mov.l   a0,(regtbl+A0)  /* Store all registers */
    mov.l   a1,(regtbl+A1)
    mov.l   a2,(regtbl+A2)
    mov.l   a3,(regtbl+A3)
    mov.l   a4,(regtbl+A4)
    mov.l   a5,(regtbl+A5)
    mov.l   a6,(regtbl+A6)
    mov.l   d0,(regtbl+D0)
    mov.l   d1,(regtbl+D1)
    mov.l   d2,(regtbl+D2)
    mov.l   d3,(regtbl+D3)
    mov.l   d4,(regtbl+D4)
    mov.l   d5,(regtbl+D5)
    mov.l   d6,(regtbl+D6)
    mov.l   d7,(regtbl+D7)
    mov.l   a7,(regtbl+SS)
    mov.l   2(a7),a0
    mov.l   a0,(regtbl+PC)
    mov.w   (a7),a0
    mov.l   a0,(regtbl+SR)
    mov.l   usp,a0
    mov.l   a0,(regtbl+US)
    jsr asyncbreak          /* Execute high level trap handler. */


/****************************************************************************
  trace_hdlr:
  Save registers and jump to tracetrap.
  Identical to trap_hdlr except that it calls trace trap...
*/ 
    .global trace_hdlr
trace_hdlr:
    mov.l   a0,(regtbl+A0)      /* Store all registers */
    mov.l   a1,(regtbl+A1)
    mov.l   a2,(regtbl+A2)
    mov.l   a3,(regtbl+A3)
    mov.l   a4,(regtbl+A4)
    mov.l   a5,(regtbl+A5)
    mov.l   a6,(regtbl+A6)
    mov.l   d0,(regtbl+D0)
    mov.l   d1,(regtbl+D1)
    mov.l   d2,(regtbl+D2)
    mov.l   d3,(regtbl+D3)
    mov.l   d4,(regtbl+D4)
    mov.l   d5,(regtbl+D5)
    mov.l   d6,(regtbl+D6)
    mov.l   d7,(regtbl+D7)
    mov.l   a7,(regtbl+SS)
    mov.l   2(a7),a0
    mov.l   a0,(regtbl+PC)
    mov.w   (a7),a0
    mov.l   a0,(regtbl+SR)
    mov.l   usp,a0
    mov.l   a0,(regtbl+US)
    jsr tracetrap           /* Execute high level trap handler. */


/****************************************************************************
  resume:
  Restore registers and issue rte.
*/ 

    .global resume
resume:
    mov.l   (regtbl+SS),a7  /* Restore registers */
    mov.l   (regtbl+US),a0
    mov.l   a0,usp
    mov.l   (regtbl+PC),a0  /* PC and SR are restored to the stack frame. */
    mov.l   a0,2(a7)
    mov.l   (regtbl+SR),a0
    mov.w   a0,(a7)
    mov.l   (regtbl+A0),a0
    mov.l   (regtbl+A1),a1
    mov.l   (regtbl+A2),a2
    mov.l   (regtbl+A3),a3
    mov.l   (regtbl+A4),a4
    mov.l   (regtbl+A5),a5
    mov.l   (regtbl+A6),a6
    mov.l   (regtbl+D0),d0
    mov.l   (regtbl+D1),d1
    mov.l   (regtbl+D2),d2
    mov.l   (regtbl+D3),d3
    mov.l   (regtbl+D4),d4
    mov.l   (regtbl+D5),d5
    mov.l   (regtbl+D6),d6
    mov.l   (regtbl+D7),d7
    rte
