/*
 * General notice:
 * This code is part of a boot-monitor package developed as a generic base
 * platform for embedded system designs.  As such, it is likely to be
 * distributed to various projects beyond the control of the original
 * author.  Please notify the author of any enhancements made or bugs found
 * so that all may benefit from the changes.  In addition, notification back
 * to the author will allow the new user to pick up changes that may have
 * been made by other users after this version of the code was distributed.
 *
 * Author:  Ed Sutter
 * email:   esutter@lucent.com      (home: lesutter@worldnet.att.net)
 * phone:   908-582-2351            (home: 908-889-5161)
 */

/* DEFAULT_ETHERADD & DEFAULT_IPADD:
 * Refer to notes in ethernet.c function EthernetStartup() for details
 * regarding the use of these definitions.
 */
#define     DEFAULT_ETHERADD "00:60:1D:02:0b:ff"
/* #define  DEFAULT_IPADD    "135.3.94.135" */
/* #define  DEFAULT_IPADD    "DHCP"         */
#define     DEFAULT_IPADD    "BOOTP"

/* LOOPS_PER_SECOND:
 * Approximately the size of a loop that will cause a 1-second delay.
 */
#define LOOPS_PER_SECOND    20000

/* Flash bank configuration: */
#define FLASH_BANK0_WIDTH       2
#define FLASHBANKS              1
#define FLASH_BANK0_BASE_ADDR   0x400000

/* TFS definitions:
 *  TFSSTART:       Base address in FLASH at which TFS starts.
 *  TFSEND:         Size of the flash space (starting at TFSSTART)
 *                  dedicated to TFS.
 *  TFSSPARE:       Location of sector that is used as the spare sector
 *                  by TFS for defragmentation.
 *  TFSSPARESIZE:   Size of the spare sector used by TFS for defragmentation.
 *  TFSFLASHWIDTH:  Width (in bytes) of the flash used by TFS.
 *  TFSSECTORCOUNT: Number of eraseable sectors that TFS covers, not including
 *                  the TFSSPARE sector.
 */
#define TFSSTART        0x440000
#define TFSEND          0x4effff
#define TFSSPARE        0x4f0000
#define TFSSPARESIZE    0x10000
#define TFSSECTORCOUNT  11      /* Sectors 6-17 inclusive are used by TFS. */
#define TFSFLASHWIDTH   2
#define TFS_EBIN_AOUT   1

/* Width macros used by TFS and FLASH operations.
 * Note that only one of WIDTH8, WIDTH16 and WIDTH32 should be set and the
 * WIDTHXX_NOTUESD macros should reflect the opposite state of the WIDTH
 * macros.
 */
#define WIDTH8      0
#define WIDTH16     1
#define WIDTH32     0
#define WIDTH8_NOTUSED  1
#define WIDTH16_NOTUSED 0
#define WIDTH32_NOTUSED 1

/* This macro is used by the flash code to determine how much of the boot
 * flash is protected.  It is usually 0x20000 or 0x30000, but can vary
 * depending on the size of the boot monitor implementation.
 */
#define FLASH_PROTECT_SIZE  0x40000

/* SYMFILE is the name of a file that, if present, the monitor uses to 
 * process symbols on the command line.  A symbol is a whitespace delimited
 * string prefixed by a percent sign (%).
 */
#define SYMFILE "symtbl"

/* Specify the type of 68K CPU here. Only one should be defined. */
#define CPU_68K     0
#define CPU_68302   0
#define CPU_68EN302 1

/* Specify CPU/PLATFORM type and name so that common code can be used
 * for a similar cpu, on different platforms.
 * The 'TYPE' definition is used for ifdefs in the code and the 'NAME'
 * is used for printfs in the code.
 */
#define CPU_TYPE        CPU_68EN302
#define CPU_NAME        "Motorola 68EN302"
#define PLATFORM_TYPE   PPA_68K
#define PLATFORM_NAME   "68K-Based Packet Phone Adapter"

/* Specify the size of the memory block (in monitor space) that is to be
 * allocated to malloc in the monitor.  Note that this size can be dynamically
 * increased through the HEAPBASE and HEAPSTART shell variables.
 */
#define ALLOCSIZE 8*1024

/* Specify inclusion of subsystems within the monitor here.
 * Refer to inc_check.h for a description of each macro.
 */
#define INCLUDE_MEMCMDS         1
#define INCLUDE_PIO             1
#define INCLUDE_EDIT            1
#define INCLUDE_DEBUG           0
#define INCLUDE_DISASSEMBLER    1
#define INCLUDE_UNPACK          0
#define INCLUDE_UNZIP           0
#define INCLUDE_ETHERNET        1
#define INCLUDE_TFTP            1
#define INCLUDE_DHCPBOOT        1
#define INCLUDE_TFS             1
#define INCLUDE_TFSCLI          1
#define INCLUDE_TFSAPI          1
#define INCLUDE_TFSAUTODEFRAG   1
#define INCLUDE_TFSSCRIPT       1
#define INCLUDE_TFSSYMTBL       1
#define INCLUDE_XMODEM          1
#define INCLUDE_LINEEDIT        1
#define INCLUDE_EE              1
#define INCLUDE_FLASH           1
#define INCLUDE_STRACE          0
#define INCLUDE_CAST            0
#define INCLUDE_IDEV            0
#define INCLUDE_EXCTEST         0
#define INCLUDE_CRYPT           0
#define INCLUDE_REDIRECT        0
#define INCLUDE_QUICKMEMCPY     1
#define INCLUDE_PROFILER        0
#define INCLUDE_BBC             0
#define INCLUDE_MEMTRACE        0

/* Inclusion of this next file will make sure that all of the above
 * inclusions are legal; and adjust where necessary.
 */
#include "inc_check.h"
