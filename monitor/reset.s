    .file   "reset.s"

#include "asm68k.h"
#include "cpuio.h"

/***********************************************************************
 *
 * reset.s:
 * At reset, the 68000 looks at locations 0x0 & 0x4 to pick up the
 * supervisor stack pointer (0x0) and the startup point for code
 * execution (0x4).
 * There are a few things to be aware of here...
 * This is for a 68302, so the processor resets and grabs the PC/SSP from
 * location zero.  Ultimately, this code is relocated to a different point
 * in the memory map so that zero can be used for the vector table.
 * This means that the pointer to coldstart must be relative to zero, NOT
 * the actual address established by the linker; hence, notice that the PC
 * is derived from the delta between coldstart and _reset.
 *
 ************************************************************************/
    
    .comm   MonStack,4096

    .global reset, coldstart, warmstart
    .global ipaddr, etheraddr, moncomptr
    .equ    INITIALIZE,3    /* taken from cpu.h */

    .text

reset:
    .long   MonStack+4096-4     /* SSP */
    .long   coldstart - reset       /* PC */

    /* Pointer to the moncom function used by application. */
moncomptr:
    .long   moncom

/*
 * Provide space to allow a programmer to place an ascii
 * string in each of these locations as an optional point
 * of storage for MAC and/or IP address...
 */
ipaddr:
    .byte 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff 
    .byte 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff 
    .byte 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff 
etheraddr:
    .byte 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
    .byte 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
    .byte 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff

coldstart:
    mov.l   #INITIALIZE,d5

morestart:
    move    &0x2700,sr          /* Status Register:             */
                                /* supervisor mode, ints off.   */

    mov.l   &MonStack+4096-4,sp
    move    a7,usp
    mov.l   &0,a6               /* a6 = fp */

    mov.w   &0x0700,BAR.L
    mov.w   &0x0710,MOBAR.L

    and.w   &0xfffe,OR0         /* Option register 0:           */
                                /*  Clear CFC, to ignore FC.    */

/*
  Explanation...
  At this point the code is executing out of the memory that is physically
  connected to CS0, and the base address of CS0 is physical address 0.
  On the 68000 CPU, the vector table is mapped to start at location 0; hence,
  the address space used by CS0 must be moved, and some block of RAM
  (or DRAM) must be mapped to address 0 so that the vector table is writeable.
  To do this, we implement the following:
    - copy a small "adjustment" routine to a block of memory space that is
     writeable and is not part of CS0's memory space.  This adjustment
     routine must re-map the address space of CS0 to some other memory range.
   - jump to that routine and after jumping back from there, the CS0 memory
     will be mapped in some other memory space.
   - now adjust the RAM memory space so that it starts at 0; thus covering
     the vector table space and allowing it to be written.
 
  The following code is dependent on the SYSTEM_XXXX value...
  If SYSTEM_PPAFAXROUTER is 1, then the code assumes that SRAM is
  tied to CS1; otherwise, DRAM is used only.
*/

#if SYSTEM_PPAFAXROUTER
    mov.w   &0xa201,BR1         /* Base register 1: */
                                /*  CS1 starts at: 0x100000 */
                                /*  FC2-0=101; Read/Write; CS is enabled */
    mov.w   &0xdfc0,OR1         /* Option register 1: */
                                /*  6 wait states; Ignore FC; 128K block. */
#else
    mov.w   &0x0009,DRFRSH      /* Setup 1Mbyte DRAM starting at 0x100000 */
    mov.w   &0x1071,DBA0
    mov.w   &0x0401,DCR
#endif
                                /* Copy CS0_Adjust to memory at 0x100000. */
xfertoram:                      /* Note that the location of CS0_Adjust must */
    lea.l   CS0_Adjust(pc),a0   /* be PC relative because at this point in */
    lea.l   end_Adjust(pc),a1   /* the bootup, reset is at location 0, not */
    mov.l   &0x100000,a2        /* the address it is mapped to at link time.*/
xfermore:
    cmp.l   a1,a0
    beq     xferdone
    mov.w   (a0)+,(a2)+
    bra xfermore
xferdone:
    jmp 0x100000                /* Jump to 0x100000 and execute the */
                                /* adjust. */
CS0_Adjust:
    mov.w   &0xc801,BR0         /* 1Mb block begin at 0x400000. */
    mov.w   &0x1e00,OR0
    mov.l   &end_Adjust,a0
    jmp (a0)                    /* After adjustment, return to CS0 */
end_Adjust:                     /* memory space. */

#if SYSTEM_PPAFAXROUTER
    mov.w   &0xa001,BR1         /* Adjust CS1 so that RAM starts */
#else                           /* at address 0. */
    mov.w   &0x0061,DBA0        /* Adjust DRAM base to start */
                                /* at address 0, 2Mbyte. */
#endif

    move.l      d5,a7@-
    jsr         start           /* Get into C-level code. */


/* warmstart:
 * Used to re-start the monitor.  From C, this looks like a function
 * that takes one argument.  That argument ultimately becomes the
 * value passed to start() so that the system can properly startup.
 */
warmstart:
    move.l  a7@(4),d5
    jmp     morestart
