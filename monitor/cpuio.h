/* 68302 IO control registers: */

#define DEFAULT_BAUD_RATE 19200

#define  BAR    0xf2
#define  BASE   0x700000

/* 68en302-specific registers */

/* Module Controller Base Address Register */
#define MOBAR       0xee        /* Defaults to 0xbffe */
#define MOBA        0x710000

#define MBCTL       (MOBA+0x000)
#define IER         (MOBA+0x002)
#define CSER0       (MOBA+0x004)
#define CSER1       (MOBA+0x006)
#define CSER2       (MOBA+0x008)
#define CSER3       (MOBA+0x00a)
#define PCSR        (MOBA+0x00c)

/* DRAM Registers */
#define DCR         (MOBA+0x010)
#define DRFRSH      (MOBA+0x012)
#define DBA0        (MOBA+0x014)
#define DBA1        (MOBA+0x016)

/* Ethernet Registers */
#define ECNTRL      (MOBA+0x800)
#define EDMA        (MOBA+0x802)
#define EMRBLR      (MOBA+0x804)
#define INTR_VECT   (MOBA+0x806)
#define INTR_EVENT  (MOBA+0x808)
#define INTR_MASK   (MOBA+0x80a)
#define ECNFIG      (MOBA+0x80c)
#define ETHER_TEST  (MOBA+0x80e)
#define AR_CNTRL    (MOBA+0x810)

/* CAM Entry Table */
#define CET         (MOBA+0xa00)

/* Buffer Descriptor Table */
#define EBD         (MOBA+0xc00)

/* End of 68en302-specific registers */

#define SCC1BASE    (BASE+0x400)
#define SCC2BASE    (BASE+0x500)
#define SCC3BASE    (BASE+0x600)

#define  RXBD0      (BASE+0x400)    /* Buffer Descriptors: (pg 4.37) */
#define  RXBD0_0    (RXBD0+0x0)     /* Status and control */
#define  RXBD0_1    (RXBD0+0x2)     /* Data length */
#define  RXBD0_2    (RXBD0+0x4)     /* High order data buffer pointer */
#define  RXBD0_3    (RXBD0+0x6)     /* Low order data buffer pointer */
#define  RXBD1      (BASE+0x408)
#define  RXBD1_0    (RXBD1+0x0)
#define  RXBD1_1    (RXBD1+0x2)
#define  RXBD1_2    (RXBD1+0x4)
#define  RXBD1_3    (RXBD1+0x6)
#define  RXBD2      (BASE+0x410)
#define  RXBD2_0    (RXBD2+0x0)
#define  RXBD2_1    (RXBD2+0x2)
#define  RXBD2_2    (RXBD2+0x4)
#define  RXBD2_3    (RXBD2+0x6)
#define  RXBD3      (BASE+0x418)
#define  RXBD3_0    (RXBD3+0x0)
#define  RXBD3_1    (RXBD3+0x2)
#define  RXBD3_2    (RXBD3+0x4)
#define  RXBD3_3    (RXBD3+0x6)
#define  RXBD4      (BASE+0x420)
#define  RXBD5      (BASE+0x428)
#define  RXBD6      (BASE+0x430)
#define  RXBD7      (BASE+0x438)
    
#define  TXBD0      (BASE+0x440)
#define  TXBD0_0    (TXBD0+0x0)
#define  TXBD0_1    (TXBD0+0x2)
#define  TXBD0_2    (TXBD0+0x4)
#define  TXBD0_3    (TXBD0+0x6)
#define  TXBD1      (BASE+0x448)
#define  TXBD1_0    (TXBD1+0x0)
#define  TXBD1_1    (TXBD1+0x2)
#define  TXBD1_2    (TXBD1+0x4)
#define  TXBD1_3    (TXBD1+0x6)
#define  TXBD2      (BASE+0x450)
#define  TXBD2_0    (TXBD2+0x0)
#define  TXBD2_1    (TXBD2+0x2)
#define  TXBD2_2    (TXBD2+0x4)
#define  TXBD2_3    (TXBD2+0x6)
#define  TXBD3      (BASE+0x458)
#define  TXBD3_0    (TXBD3+0x0)
#define  TXBD3_1    (TXBD3+0x2)
#define  TXBD3_2    (TXBD3+0x4)
#define  TXBD3_3    (TXBD3+0x6)
#define  TXBD4      (BASE+0x460)
#define  TXBD5      (BASE+0x468)
#define  TXBD6      (BASE+0x470)
#define  TXBD7      (BASE+0x478)

#define RFCR1       (SCC1BASE+0x80)
#define TFCR1       (SCC1BASE+0x81)
#define MRBLR1      (SCC1BASE+0x82)
#define RBDNM1      (SCC1BASE+0x87)
#define TBDNM1      (SCC1BASE+0x93)
#define PAREC1      (SCC1BASE+0xA2)
#define FRMEC1      (SCC1BASE+0xA4)
#define NOSEC1      (SCC1BASE+0xA6)
#define BRKEC1      (SCC1BASE+0xA8)
#define UADR11      (SCC1BASE+0xAA)
#define UADR21      (SCC1BASE+0xAC)
#define RCCR1C1     (SCC1BASE+0xB0)
#define RCCR1C8     (SCC1BASE+0xBE)

#define  GIMR       (BASE+0x812)    /* Global Interrupt Mode Register */
#define  IPR        (BASE+0x814)    /* Interrupt Pending Register */
#define  IMR        (BASE+0x816)    /* Interrupt Mask Register */
#define  ISR        (BASE+0x818)    /* In-service Register */

#define  TMR1       (BASE+0x840)    /* Timer1 mode register */
#define  TRR1       (BASE+0x842)    /* Timer1 reference register */
#define  TCR1       (BASE+0x844)    /* Timer1 capture register */
#define  TCN1       (BASE+0x846)    /* Timer1 counter register */
#define  TER1       (BASE+0x849)    /* Timer1 event register */

#define  PACNT      (BASE+0x81e)    /* Port A control register */
#define  PADDR      (BASE+0x820)    /* Port A data direction register */
#define  PADAT      (BASE+0x822)    /* Port A data register */
#define  PBCNT      (BASE+0x824)    /* Port B control register */
#define  PBDDR      (BASE+0x826)    /* Port B data direction register */
#define  PBDAT      (BASE+0x828)    /* Port B data register */

#define  BR0        (BASE+0x830)    /* Base Register 0 */
#define  OR0        (BASE+0x832)    /* Option Register 0 */
#define  BR1        (BASE+0x834)    /* Base Register 1 */
#define  OR1        (BASE+0x836)    /* Option Register 1 */
#define  BR2        (BASE+0x838)    /* Base Register 2 */
#define  OR2        (BASE+0x83a)    /* Option Register 2 */
#define  BR3        (BASE+0x83c)    /* Base Register 3 */
#define  OR3        (BASE+0x83e)    /* Option Register 3 */


#define  SCON1      (BASE+0x0882)   /* SCC Control Register */
#define  SCM1       (BASE+0x0884)   /* SCC Mode Register */
#define  DSR1       (BASE+0x0886)   /* SCC Data Sync Register */
#define  SCCE1      (BASE+0x0888)   /* SCC Event Register */
#define  SCCM1      (BASE+0x088a)   /* SCC MaskRegister */
#define  SCCS1      (BASE+0x088c)   /* SCC Status Register */

#define  SIMASK     (BASE+0x8B2)    /* SCC Interface Mask Register */
#define  SIMODE     (BASE+0x8B4)    /* SCC Interface Mode Register */

#ifndef ASSEMBLY_LANGUAGE
/* Structure representing the buffer descriptor: */
struct  bd {
    unsigned short  stat;
    unsigned short  len;
    unsigned char   *ptr;
};
#endif

/* More ethernet controller stuff for the EN302: */
/* BDSIZE: */
/* There is a total of 128 buffer descriptors within the 68EN302's internal */
/* memory.  The value of BDSIZE determines how these buffer descriptors are */
/* used by the ethernet controller (either transmit or receive).  */

/* Buffer descriptor allocation is based on the value of BDSIZE: */
/* Set BDSIZEINFO to one of the XMT__RCV__ values, the macros do the rest. */
#define XMT8RCV120  0x08780000  /* 8 xmt, 120 rcv BDs */
#define XMT16RCV112 0x10700020  /* 16 xmt, 112 rcv BDs */
#define XMT32RCV96  0x20600040  /* 32 xmt, 96 rcv BDs */
#define XMT64RCV64  0x40400060  /* 64 xmt, 64 rcv BDs */
#define BDSIZEINFO  XMT8RCV120
#ifndef ASSEMBLY_LANGUAGE
#define BDSIZE      ((ushort)(BDSIZEINFO & 0xffff))
#else
#define BDSIZE      (BDSIZEINFO & 0xffff)
#endif
#define XBUFMAX     ((BDSIZEINFO & 0xff000000) >> 24)
#define RBUFMAX     ((BDSIZEINFO & 0x00ff0000) >> 16)
#define XBDBASE     EBD
#define RBDBASE     (EBD + (XBUFMAX*8))

#define XBUFCNT     8   /* Must be <= XBUFMAX */
#define RBUFCNT     16  /* Must be <= RBUFMAX */

/* Bits of INTR_EVENT register: */
#define RXB         0x0001
#define TXB         0x0002
#define BSY         0x0004
#define RFINT       0x0008
#define TFINT       0x0010
#define EBERR       0x0020
#define BOD         0x0040
#define GRA         0x0080
#define BABT        0x0100
#define BABR        0x0200
#define HBERR       0x0400

#define VG          0x0100

/* Bits of Transmit buffer descriptor flag: */
#define CSL         0x0001
#define UN          0x0002
#define RCMASK      0x003c
#define RL          0x0040
#define LC          0x0080
#define HB          0x0100
#define DEF         0x0200
#define TC          0x0400
#define LAST        0x0800
#define INT         0x1000
#define WRAPBIT     0x2000
#define TO          0x4000
#define READY       0x8000
