inline setjmp(env)
struct jmp_struct  *env;
{
    unsigned long   sr, *sp;

    /* Copy A2-A5, D2-D7, FP, USP, SSP & PC to env structure. */
    /* Recall that %a6 = %fp and %a7 = %sp. */

    __asm__("movl a2,%0" : "=a" (env->A2));
    __asm__("movl a3,%0" : "=a" (env->A3));
    __asm__("movl a4,%0" : "=a" (env->A4));
    __asm__("movl a5,%0" : "=a" (env->A5));
    __asm__("movl d2,%0" : "=a" (env->D2));
    __asm__("movl d3,%0" : "=a" (env->D3));
    __asm__("movl d4,%0" : "=a" (env->D4));
    __asm__("movl d5,%0" : "=a" (env->D5));
    __asm__("movl d6,%0" : "=a" (env->D6));
    __asm__("movl d7,%0" : "=a" (env->D7));

    asm("mov.w %sr,%d0");
    __asm__("movl d0,%0" : "=a" (sr));

    asm("mov.l %sp, %a0");
    __asm__("movl a0,%0" : "=a" (sp));

    if (sr & (unsigned long)0x2000) {
        env->SSP = (unsigned long)sp;
//      env->SSP += STACK_ADJUST;       /* account for the setjmp stack frame */

        asm("mov.l %usp, %a0");
        __asm__("movl a0,%0" : "=a" (env->USP));

        asm("mov.l %sp, %a0");
        __asm__("movl a0,%0" : "=a" (sp));
    }
    else {
        env->USP = (unsigned long)sp;
//      env->USP += STACK_ADJUST;       /* account for the setjmp stack frame */
    }
    sp += 3;
    env->FP = *sp;
    sp++;
    env->PC = *sp;

    return(0);                      /* The call to setjmp MUST return 0 */
}
