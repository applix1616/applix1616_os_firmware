/* mstat.c:
    The mstat command is provided as a target-specific command for keeping
    track of various monitor-specific stuff... uart baud rate, malloc stats,
    chip-select configuration, etc...

    General notice:
    This code is part of a boot-monitor package developed as a generic base
    platform for embedded system designs.  As such, it is likely to be
    distributed to various projects beyond the control of the original
    author.  Please notify the author of any enhancements made or bugs found
    so that all may benefit from the changes.  In addition, notification back
    to the author will allow the new user to pick up changes that may have
    been made by other users after this version of the code was distributed.

    Author: Ed Sutter
    email:  esutter@lucent.com      (home: lesutter@worldnet.att.net)
    phone:  908-582-2351            (home: 908-889-5161)
*/
#include "config.h"
#include "cpu.h"
#include "genlib.h"
#include "stddefs.h"

extern  void InitUART(), moreset(), ConsoleBaudEnvSet(), CSInfo();

void    mstatshow();
static  ushort  MonitorSTATUS;

char *MstatHelp[] = {
    "Monitor status",
    "-[b:s:]",
    " -b {baud}  set baud rate",
    " -c         dump chip-select info",
    0,
};

int
Mstat(int argc,char *argv[])
{
    int opt;

    /* If no args, just dump current status. */
    if (argc == 1) {
        mstatshow();
        return(0);
    }

    while ((opt=getopt(argc,argv,"b:c")) != -1) {
        switch(opt) {
        case 'b':
            InitUART(atoi(optarg));
            ConsoleBaudEnvSet();
            break;
        case 'c':
            CSInfo();
            return(0);
            break;
        default:
            return(0);
        }
    }
    if (argc != optind)
        return(-1);
    return(0);
}

void
putsr(ushort sr)
{
    asm("mov.l 8(%fp),%d0");
    asm("mov.w %d0,%sr");
}

ushort
getsr(void)
{
    asm("mov.l &0,%d0");
    asm("mov.w %sr,%d0");
}

ulong
getsp(void)
{
    asm("mov.l %sp,%d0");
}

void
InitMonSTATUS(ulong state)
{
    MonitorSTATUS = state;
    putsr((ushort)MonitorSTATUS);
}

void
SetMonSTATUS()
{
    putsr((ushort)MonitorSTATUS);
}

void
mstatshow(void)
{
    monHeader(0);
    mstatshowcom();
    printf("Current:\n");
    printf(" SR: 0x%04x\n",(ushort)getsr());
    printf(" SP: 0x%08lx\n",(ulong)getsp());
}

