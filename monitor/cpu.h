#define SR_TRACE    0x8000
#define SR_SUPER    0x2000

#define MONARGV0    "mon68"

#define VECTOR(v)   (*(unsigned long *)((v)*4))

#define JUMPTOSTART()   asm("jmp coldstart")

#define EXCEPTION   1
#define BREAKPOINT  2
#define INITIALIZE  3
#define SSTEP       4
#define APPLICATION 5
#define MORESTART   6
#define BAILOUT     7
#define MISC        8
#define APP_EXIT    9

#define MONITOR_STATUS      0x2700

struct jmp_struct {
    unsigned long   PC;
    unsigned long   SSP;
    unsigned long   USP;
    unsigned long   FP;
    unsigned long   A2;
    unsigned long   A3;
    unsigned long   A4;
    unsigned long   A5;
    unsigned long   D2;
    unsigned long   D3;
    unsigned long   D4;
    unsigned long   D5;
    unsigned long   D6;
    unsigned long   D7;
};

typedef unsigned long jmp_buf[sizeof(struct jmp_struct)/sizeof(unsigned long)];

#define RESET           (FLASH_BANK0_BASE_ADDR+0x4)
#define RESETFUNC()     ((void(*)())((int)(*(ulong *)RESET) | 0x400000))
#define memcpyL         memcpy
