#include "config.h"
#include "cpu.h"
#include "genlib.h"
#include "stddefs.h"
#if INCLUDE_DEBUG

extern  void setTraceBit(), clrTraceBit(), resume(), installatpoints();
extern  void removeatpoints(), monrestart();
extern  int disass(), settrap();
extern  long getreg();

int StepCount, StepQuiet, SetStepOverBreakpoint;
ushort  *StepOverAdd, StepOverText;

char *SsHelp[] = {
        "Single step",
        "-[o] [count]",
        " -o   step over function",
        0,
};

int
Ss(int argc,char *argv[])
{
    ulong   reg;
    int opt, StepOver;

    StepOver = 0;
    StepQuiet = 0;
    while((opt=getopt(argc,argv,"oq")) != -1) {
        switch(opt) {
        case 'o':
            StepOver = 1;
            break;
        case 'q':
            StepQuiet = 1;
            break;
        default:
            return(0);
        }
    }
    if (argc-(optind-1) == 2)
        StepCount = strtol(argv[optind],(char **)0,0);
    else
        StepCount = 1;

    if (StepOver)
        StepCount = 1;

    StateOfMonitor = APPLICATION;

    if (StepOver) {
        getreg("PC",&reg);
        if ((*(ushort *)reg & 0xffc0) == 0x4e80)
            SetStepOverBreakpoint = 1;
        else
            SetStepOverBreakpoint = 0;
    }
    else 
        SetStepOverBreakpoint = 0;

    setTraceBit();
    resume();
    return(0);
}

/* tracetrap():
        Trap handler called as a result of hitting a trace trap.
        
        * If state of monitor is BREAKPOINT, then this trap was hit
          as a result of the final stage of a breakpoint resumption; so
          install breakpoints, clear the trace bit and resume.
        * If SetStepOverBreakpoint is true, then the previous single
          step command was a step over (step over a function); so a 
          breakpoint is set at the return location of the function,
          the trace bit is cleared, and application is resumed.
        * If StepCount is non-zero then resume with trace bit set.
*/
void
tracetrap()
{
    ulong   reg;

    if (StateOfMonitor == BREAKPOINT) {
        installatpoints();
        StateOfMonitor = APPLICATION;
        clrTraceBit();
        resume();   /* will not return */
    }
    if (StepCount)
        StepCount--;
    if (SetStepOverBreakpoint) {
        removeatpoints();
        getreg("SR",&reg);
        if (reg & SR_SUPER)
            getreg("SS",&reg);
            else
            getreg("US",&reg);
        StepOverAdd = *(ushort **)reg;

        /* Set the stepover breakpoint: */
        StepOverText = settrap(StepOverAdd);

        /* Resume with trace mode off until breakpoint is hit. */
        clrTraceBit();
        resume();   /* will not return */
    }
    if (StepCount) {
#if INCLUDE_DISASSEMBLER
        if (!StepQuiet)
            disass(0,0,0);
#endif
        setTraceBit();
        resume();   /* will not return */
    }
    else {
        showloc();
        clrTraceBit();
        monrestart(SSTEP);
    }
}

void
StepOverTrap()
{
    /* Restore the text: */
    *StepOverAdd = StepOverText;
    SetStepOverBreakpoint = 0;
    showloc();
    monrestart(SSTEP);
}

void
showloc()
{
    ulong   reg;

#if INCLUDE_DISASSEMBLER
    if (!StepQuiet) {
        if (disass(0,0,1) == -1) {
            getreg("PC",&reg);
            printf("Step(s) complete, PC now: 0x%x\n",reg);
        }
    }
#endif
}
#else
void
tracetrap()
{
    printf("tracetrap() called with no debugger\n");
    monrestart(BREAKPOINT);
}
#endif
