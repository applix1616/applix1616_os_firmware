/* main.c:

    General notice:
    This code is part of a boot-monitor package developed as a generic base
    platform for embedded system designs.  As such, it is likely to be
    distributed to various projects beyond the control of the original
    author.  Please notify the author of any enhancements made or bugs found
    so that all may benefit from the changes.  In addition, notification back
    to the author will allow the new user to pick up changes that may have
    been made by other users after this version of the code was distributed.

    Author: Ed Sutter
    email:  esutter@lucent.com      (home: lesutter@worldnet.att.net)
    phone:  908-582-2351            (home: 908-889-5161)
*/
#include "config.h"
#include "info.h"
#include "cpu.h"
#include "cpuio.h"
#include "tfs.h"
#include "tfsprivate.h"
#include "ether.h"
#include "genlib.h"
#include "monapp.h"
#include "stddefs.h"

extern  void InitMonSTATUS(), GlobalIntInit(), reinit(void);
extern  void putsr();
extern  ushort getsr();
int     abcdefghijklmnopqrst;
int     StateOfMonitor, AppExitStatus;

int
main(int argc,char *argv[])
{
    InitMonSTATUS(MONITOR_STATUS);

    /* Config GIMR with IRQ7, IRQ6 & IRQ1 in dedicated */
    /* operational mode. Also, each of these ints must generate */
    /* internal vectors and be edge triggerred.  */
    /* Note, see page 3-27 for GIMR info. */
    GlobalIntInit(0x87a0);

    /* Set user level to its max, then allow monrc file to adjust it. */
    initUsrLvl(MAXUSRLEVEL);

    switch(StateOfMonitor) {
    case INITIALIZE:
        reginit();      /* Init register data structures. */
        init2();        /* Second phase of hardware initialization. */
        init3();        /* Common system initialization stuff... */
        break;
    case MISC:
    case SSTEP:
    case BAILOUT:
    case MORESTART:
    case BREAKPOINT:
#if INCLUDE_DEBUG
        regbackup();
#endif
        break;
    case APP_EXIT:
        reinit();
        printf("\nApplication Exit Status: %d (0x%x)\n",
            AppExitStatus,AppExitStatus);
        break;
    case EXCEPTION:
        reinit();
        showregs();
        exceptionAutoRestart(INITIALIZE);
        break;
    default:
        printf("Unexpected monitor state: 0x%x\n",StateOfMonitor);
        break;
    }

    /* Enter the endless loop of command processing: */
    CommandLoop();

    /* Should not get here. */
    return(0);
}

void
reinit(void)
{
    *(ushort *)BAR = 0x0700;    /* reinit BAR */
    *(ushort *)MOBAR = 0x0710;  /* reinit MOBAR */
    init1(0);                   /* reinit hdware */
    init2();                    /* reinit hdware */
#if INCLUDE_ETHERNET
    EthernetStartup(0,0);       /* reinit ethernet */
#endif
}

void
init2(void)
{
#if INCLUDE_FLASH
    FlashInit();    /* Init flashop data structures. */
#endif
    cacheInit();

#if INCLUDE_ETHERNET
    enreset();      /* Clear the ethernet interface. */
#endif

#if INCLUDE_TFS
    tfsstartup();   /* Init tfs data structures. */
#endif
}

void
monrestart(int val)
{
    warmstart(val);
}

void
intsrestore(ulong sr)
{
    putsr(sr);
}

ulong
intsoff(void)
{
    ushort  sr;

    sr = getsr();
    putsr(sr|0x0700);
    return((ulong)sr);
}

/* some stubs for now... */

void
ctxMON(void)
{
}

void
ctxAPP(void)
{
}

void
cacheInitForTarget(void)
{
}

void
appexit(val)
int val;
{
    intsoff();
    AppExitStatus = val;
#if INCLUDE_DEBUG
    removeatpoints();
#endif
    warmstart(APP_EXIT);
}

/* extValidPassword():
    Refer to validPassword() in common/password.c for notes.
*/
int
extValidPassword(char *password, int ulvl)
{
    return(-1);
}
