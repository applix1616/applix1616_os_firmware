/*
 * Check add.w &5,%sp on return from jsr
 * Check mov.l &4,%d0
 * Byte flags inefficient
 */

#include <stdio.h>
#include <stat.h>

#define NOB		10	/* Buffer 10 lines */

char *file_buf;
char obuf400;
char outbufNOB400;
int outbufindex;		/* Next free entry in outbuf */
int outindex;			/* Next line to go out in outbuf */
char *obufptr, *opstart;

#define MAXNAMES	100
char commnamesMAXNAMES20;	/* The common segment names */
int commlensMAXNAMES;		/* Lengths of names */

char verbose;
int verbop;			/* True if some verbose output on line */
int line;			/* Line numnber */
int dosyscalls;			/* If true, convert ysscalls to line A traps */
int ncantcope;			/* Number of system calls we can't shorten */

int nssaved, nsyscalls;		/* Number of bytes saved, number of syscalls */

char *ourname;

#define skipwhite(p)	while (iswhite(*cp)) cp++
#define iswhite(c)	((c == ' ') || (c == '\t'))

main(argc, argv)
int argc;
char *argv;
{
	register char *cp, *ocp;
	register unsigned short  inparen;	/* Parenthesis depth */
	int argnum;
	int indata;			/* We are in the data segment */
	int commop;		/* The opcode is "comm", in data seg */
	register unsigned int i;
	register unsigned int ncomms;
	int printedit, immediate;
	int nfixes, nafixes;
	int movl;
	int arg;
	char *compcp;
	int doaddresses;
	int dowords;
	int file_size;
	char *ifname;
	int ifd;

	ourname = argv0;
	outbufindex = 0;
	outindex = 1;			/* Point to null strings first */
	verbose = 0;
	doaddresses = 0;
	dowords = 0;
	dosyscalls = 0;
	ifname = (char *)0;
	if (argc < 3)
		usage();
	for (arg = 1; arg < argc; arg++)
	{
		cp = argvarg;
		if (*cp == '-')
		{
			cp++;
			switch (toupper(*cp++))
			{
			case 'V':	/* Verbose setting */
				verbose = atoi(cp);
				break;
			case 'W':
				dowords = 1;
				break;
			case 'A':
				doaddresses = 1;
				break;
			case 'S':
				dosyscalls = 1;
				break;
			default:
				usage();
			}
		}
		else
		{	/* File spec? */
			if (ifname)
				usage();
			ifname = cp;
		}
	}

	file_size = filesize(ifname);
	if (!(file_buf = (char *)malloc(file_size + 2)))
	{
		Vprintf("Out of memory\n");
		exit(1);
	}
	if ((ifd = open(ifname, 0)) < 0)
	{
		perror("open of input");
		exit(1);
	}
	if ((i = read(ifd, file_buf, file_size + 1)) != file_size)
	{
		Vprintf("read returns %d, not %d\n", i, file_size);
		exit(1);
	}
	close(ifd);

	cp = file_buf;
	if (file_buffile_size - 1 != '\n')
	{
		file_buffile_size++ = '\n';
	}
	file_buffile_size = '\0';	/* Clear up the end */

	nsyscalls = nssaved = ncantcope = 0;
	line = ncomms = nfixes = nafixes = 0;
	while (*cp)
	{
		obufptr = &outbufoutbufindex++0;
		outbufindex %= NOB;
		*obufptr = '\0';
		verbop = 0;
		line++;
		if (verbose > 2)
			Vprintf("Line %d", line);
		if (*cp == '|')	/* Comment */
		{
			while (*cp != '\n')
				*obufptr++ = *cp++;
			goto gotline;
		}
/* Process the label field */
		ocp = obuf;
		*ocp = '\0';
		if (*cp == '\n')
			goto gotline;	/* Empty line */
		while (!iswhite(*cp) && (*cp != '\n'))
			*ocp++ = *cp++;	/* Copy label */
		*ocp = '\0';		/* Terminate label */
		output("%s", obuf);

/* Process the opcode field */
		skipwhite(cp);
		if (*cp == '\n')
			goto gotline;	/* Empty or label only */

		ocp = obuf;
		while (!iswhite(*cp) && (*cp != '\n'))
			*ocp++ = *cp++;
		*ocp = '\0';		/* Terminate opcode field */
		output("\t");
		opstart = obufptr;	/* Save opcode start address */
		output("%s\t", obuf);

		/* Work out what the op is */
		commop = movl = 0;
		if (!strcmp(obuf, ".byte") || !strcmp(obuf, ".long"))
		{	/* Can't handle .byte */
			while (*cp != '\n')
				*obufptr++ = *cp++;
			goto gotline;
		}
		else if (!strcmp(obuf, ".data"))
		{	/* Start of the data segment */
			indata = 1;
		}
		else if (!strcmp(obuf, ".text"))
		{	/* Start of the text segment */
			indata = 0;
		}
		else if (!strcmp(obuf, ".comm"))
		{	/* Common segment */
			commop = indata;
		}
		else if (!strcmp(obuf, "move.l"))
		{	/* Move long */
			movl = 1;
		}
/* Process the args */
		argnum = 0;
labloop:
		ocp = obuf;
		while (iswhite(*cp) || (*cp == ','))
			cp++;
		if (*cp == '\n')
			goto gotline;
		if (argnum)
			*obufptr++ = ',';	/* Separator */
		inparen = 0;
		while ((!iswhite(*cp) && (*cp != ',') && (*cp != '\n')) ||
			inparen)
		{
			if (*cp == '\n')
			{
				Vprintf("Panic 1 on line %d\n", line);
				exit(1);
			}
			if (*cp == '(')
				inparen++;
			else
			{
				if (*cp == ')')
					inparen--;
			}
			*ocp++ = *cp++;
		}
		*ocp = '\0';
		/* Is it a common label? */
		printedit = 0;
		if (doaddresses && (*obuf == '#') && movl && (argnum == 0))
		{	/* Check for move to address reg */
			immediate = 1;
			compcp = obuf + 1;
		}
		else
		{
			compcp = obuf;
			immediate = 0;
		}
		for (i = 0; (i < ncomms) && !printedit; i++)
		{
			if (!strncmp(compcp, commnamesi, commlensi))
			{	/* Match */
				if (immediate)
				{
					if (!strncmp(cp, ",A", 2))
					{	/* Can shorten */
						obufptr = opstart;
						output("lea\t");
						output("%s", compcp);
						printedit = 1;
						nafixes++;
					}
				}
				else if (dowords)
				{
					if (strlen(obuf) != commlensi)
					{	/* It is in LABEL+XX form */
						if ((obufcommlensi == '-') ||
						 (obufcommlensi == '+'))
						{
							output("%s", obuf);
							printedit = 1;
						}
					}
					else
					{
						output("%s", obuf);
						printedit = 1;
					}
					nfixes++;
				}
			}
		}
		if (!printedit)
			output("%s", obuf);
		if (!argnum && commop)
		{
			commlensncomms = strlen(obuf);
			strcpy(commnamesncomms++, obuf);
			if (ncomms == MAXNAMES)
			{
				Vprintf("Too many common names\n");
				exit(1);
			}
			if (verbose > 1)
			{
				Vprintf("comm '%s'", obuf);
			}
		}
		argnum++;
		goto labloop;

gotline:
		if (verbop)
			Vprintf("\n");
/*		output("\n");*/
		*obufptr = '\0';
		xmit();		/* Process the buffered line */
		cp++;		/* Skip the newline */
	}
	for (i = 0; i < NOB; i++)
		xmit();
	if (verbose > 1)
	{
		Vprintf("Common data labels: ");
		for (i = 0; i < ncomms; i++)
			Vprintf("%s ", commnamesi);
		Vprintf("\n");
	}
	if (verbose > 0)
	{
		Vprintf("%d comm, ", ncomms);
		Vprintf("%d shorts, ", nfixes);
		Vprintf("%d addr fixes, ", nafixes);
		Vprintf("%d syscalls (%d) (cc%d), ", nsyscalls, nssaved, ncantcope);
		Vprintf("%d bytes saved\n", (nfixes + nafixes) * 2 + nssaved);
	}
	exit(0);
}

/*
 * Another line is in the output queue
 */

xmit()
{
	register char *this, *next, *nnext;
	register unsigned char *cp;
	register unsigned int val;

	this = &outbufoutindex0;
	next = &outbuf(outindex + 1) % NOB0;
	nnext = &outbuf(outindex + 2) % NOB0;

printf("dosyscalls=%d, nnext = /%s/, struc = %d, next = /%s/, struc = %d\n",
		dosyscalls, nnext, strucmp(nnext, "\tjsr\t_syscall"),
		next, strucmp(next, "\tmovel\td0,-(sp)"));

	if (!dosyscalls || strucmp(nnext, "\tjsr\t_syscall") ||
		strucmp(next, "\tmovel\td0,-(sp)"))
	{
		ncantcope--;			/* Kludgey, but so what? */
cantcope:	ncantcope++;
		if (*this)
			puts(this);
		*this = '\0';
	}
	else
	{	/* Got a match! */
		char *index(), *nnnext;
		int nval;

		/* Check that the move is there */
		if (scp("\tmove", this))
			goto cantcope;
		nnnext = &outbuf(outindex + 3) % NOB0;

		/* Find the system call number */
		val = 0;
		cp = (unsigned char *)index(this, '#');	/* Find immediate load of d0 */
		if (!cp)
			goto cantcope;
		while (*++cp != ',')
			val = val * 10 + *cp - '0';
		if (val > 255)
			exit(Vprintf("Yuk panic2 on line %d\n", line));

		/* Now fix the stack adjust after the call to _syscall */
		if (scp("\tadd.l\t#", nnnext) && scp("\tadd.w\t#", nnnext))
			goto cantcope;

		/* Emit the line A trap */
		printf(" cd.w $%x\n", val + 0xa000);	/* The line A opcode */

		/* How much did we save from the move immediate to d0? */
		if (val > 127)		/* It was a movl */
			nssaved += 6;
		else
			nssaved += 2;	/* It was a moveq */
		nssaved += 6;		/* We saved 6 for the jsr also */

		/* Work out adjustment to sp after the system call */
		nval = 0;
		cp = (unsigned char *)index(nnnext, '#');
		while (*++cp != ',')	/* Get size of stack adjustment */
			nval = nval * 10 + *cp - '0';
		if (strucmp(cp, ",A7"))
			exit(Vprintf("Yuk panic4 on line %d\n", line));

		/* Decrement the amount by 4 because d0 is no longer pushed! */
		nval -= 4;		/* We have saved a push! */
		switch (nval)
		{
		case 0:		/* No need to adjust sp */
			nssaved += 2;
			break;
		case 2:
		case 4:
			printf(" addq.l #%d,A7\n", nval);
			break;
		case 8:		/* It was 10, can now use addq */
			printf(" addq.l #%d,A7\n", nval);
			nssaved += 2;
			break;
		default:
			printf(" add.w #%d,A7\n", nval);
			break;
		}
		*this = '\0';
		*next = '\0';
		*nnext = '\0';
		*nnnext = '\0';
		nsyscalls++;
	}
	outindex++;
	outindex %= NOB;
}

/*
 * Build the output string
 */

output(str, p1, p2)
{
	char buf100;
	char *cp;

	sprintf(buf, str, p1, p2);
	cp = buf;
	while (*cp)
		*obufptr++ = *cp++;
}

usage()
{
	Vprintf("Usage: %s -vN -a -w -s\n", ourname);
	exit(1);
}

/* Print out debug stuff or error */
Vprintf(str, p1, p2, p3, p4, p5)
char *str;
{
	fprintf(stderr, str, p1, p2, p3, p4, p5);
	verbop = 1;
}

/* Return the size of a file */
filesize(path)
{
	struct stat statbuf;

	if (stat(path, &statbuf) < 0)
	{
		perror("stat of input");
		exit(1);
	}
	return (int)statbuf.st_size;
}

/*
 * Search for 'ch' in *str. Return pointer if found, else 0
 */

char *
index(str, ch)
register char *str;
register char ch;
{
	for ( ; ; )
	{
		if (*str == ch)
			return str;
		if (!(*str++)) return (char *)0;
	}
}

/*
 * Compare s1 with s2, stopping at end of s1. Return 0 on match
 */

scp(s1, s2)
char *s1, *s2;
{
	while (*s1)
		if (*s1++ != *s2++)
			return 1;
	return 0;
}
