/*	TABS4
 * Assembler post-processor
 */

#include <stdio.h>

/* Values for line.type */
#define LT_BORING	0		/* Dunno */
#define LT_CONSTPUSH	1		/* Push of constant onto stack */
#define LT_SYSCALL	2		/* jsr syscall */
#define LINESIZE	200

char *ourname;
FILE *infile;

struct line
{
	struct line *next;
	struct line *prev;
	char type;
	long val;
	char d0free;
	char data0;			/* Sleazy */
};

#define isdigit(c)	(((c) >= '0') && ((c) <= '9'))

struct line *head, *tail;
char debug, verbose, originput;
char dosyscalls, doconstpushes;
int totsaved;
int nsyscalls;
int nconstpushes;

main(argc, argv)
char *argv;
{
	int arg;
	char *cp, ch;
	char *fname;

	ourname = argv0;

	if (argc < 2)
		usage();

	fname = 0;
	verbose = debug = originput = 0;
	dosyscalls = doconstpushes = 0;
	nsyscalls = nconstpushes = 0;
	totsaved = 0;

	for (arg = 1; arg < argc; arg++)
	{
		cp = argvarg;
		if (*cp == '-')
		{
			switch (*++cp)
			{
			case 'd':
				debug++;
				break;
			case 'o':
				originput++;
				break;
			case 'p':
				doconstpushes++;
				break;
			case 's':
				dosyscalls++;
				break;
			case 'v':
				verbose++;
				break;
			default:
				usage();
			}
		}
		else
		{
			if (fname)
				usage();
			else
				fname = cp;
		}
	}
	if (!fname)
		usage();

	infile = fopen(fname, "r");
	if (infile == 0)
	{
		fprintf(stderr, "Cannot open %s", fname);
		perror("");
		exit(-1);
	}
	readfile();
	fclose(infile);
	scanfile();

	doit();

	fflush(stdout);

	if (verbose)
	{
		fprintf(stderr, "%s: %d syscalls, %d const pushes, %d bytes saved\n",
				fname, nsyscalls, nconstpushes, totsaved);
	}
	exit(0);
}

/*
 * Process the lines.
 *
 *	Look for:
 *		move.l	#NN,-(a7)
 *		jsr	_syscall
 *		add.l	#MM,A7
 *	convert to
 *		dc.w	$a000+NN
 *		add.l	#MM-4,A7	(if MM != 4)
 *
 *	Look for:
 *		move.l	#NN,-(a7)
 *	convert to
 *		moveq.l	#NN,dN
 *		move.l	dN,-(a7)
 */

int scplength;

doit()
{
	register struct line *line;
	register int i, pushval;
	int adjustval;
	register char *data;
	register char *ptr;
	int sign, lineno;
	char isconstmove, isconstpush;

	line = head;
	lineno = 0;
	while (line)
	{
		lineno++;
		data = line->data;
		isconstmove = !scp("\tmove.l\t#", data);
		isconstpush = 0;

		if (isconstmove && (dosyscalls || doconstpushes))
		{	/* Work out if it is a constant push */
			if (debug > 1)
				fprintf(stderr, "%d: move.l match\r\n", lineno);
			ptr = data + scplength;
			pushval = 0;
			if (*ptr == '-')
			{
				sign = -1;
				ptr++;
			}
			else
				sign = 1;
			while (isdigit(*ptr))
				pushval = pushval * 10 + *ptr++ - '0';
			pushval *= sign;
			if (!scp(ptr, ",-(a7)") || !scp(ptr, ",-(sp)"))
			{
				isconstpush = 1;
				if (debug > 1)
					fprintf(stderr, "%d: Const push of %d\n", lineno, pushval);
			}
			else
				isconstpush = 0;
		}

		if (dosyscalls && isconstpush)
		{
			/* Look for syscall */
			data = line->next->data;
			if (!scp("\tjsr\t_syscall", data))
			{
				if (debug > 1)
					fprintf(stderr, "%d: syscall\n", lineno + 1);
			}
			else
				goto notsyscall;

			/* Look for the stack adjust */
			data = line->next->next->data;
			if (!scp("\tadd.l\t#", data))
			{
				if (debug > 1)
					fprintf(stderr, "%d: stack adjust?\r\n", lineno + 2);
				ptr = data + scplength;
				adjustval = 0;
				if (*ptr == '-')
				{
					sign = -1;
					ptr++;
				}
				else
					sign = 1;
				while (isdigit(*ptr))
					adjustval = adjustval * 10 + *ptr++ - '0';
				pushval *= sign;
				if (debug > 1)
					fprintf(stderr, "adjustval=%d\r\n", adjustval);
				if (!scp(",a7", ptr) || !scp(",sp", ptr))
				{	/* Got it! */
					if (originput)
					{
						printf(";%s\n;%s\n;%s\n",
							line->data, line->next->data,
							line->next->next->data);
					}
					printf("\tdc.w\t$%x\t; #%d\n", 0xa000 + pushval, pushval);
					totsaved += 10;
					if (adjustval != 4)
					{
						printf("\tadd.l\t#%d,a7\n", adjustval - 4);
						if (adjustval == 12)
							totsaved += 2;	/* Can do addq */
					}
					else
						totsaved += 2;		/* Saved instruction */
					/* Move onto add instruction */
					line = line->next->next->next;
					lineno += 2;
					nsyscalls++;
					continue;
				}
			}
		}
notsyscall:
		/* syscall didn't fit: try for a constant push */
		if (debug > 2)
			fprintf(stderr, "Not a syscall\n");

		/* Can we push it with moveq? */
		if (doconstpushes && isconstpush)
		{
			if (pushval >= -128 && pushval <= 127 && line->d0free)
			{	/* It is a moveq */
				if (debug > 2)
					fprintf(stderr, "%d: short push\n", lineno);
				if (originput)
					printf(";%s\n", line->data);
				printf("\tmoveq.l\t#%d,d0\t; akpm\n", pushval);
				printf("\tmove.l\td0,-(a7)\n");
				nconstpushes++;
				totsaved += 2;
				line = line->next;
				continue;
			}
		}
printline:
		if (originput)
			printf("%s\t\t; %c\n", line->data, line->d0free ? 'F' : 'U');
		else
			printf("%s\n", line->data);
		line = line->next;
	}
}

/*
 * Read the file in
 */

readfile()
{
	char bufLINESIZE;
	register struct line *temp;
	register char *cpin, *cpout;

	tail = 0;
	head = 0;
	while (fgets(buf, LINESIZE, infile))
	{
		temp = (struct line *)malloc(sizeof(struct line) +
				strlen(buf) + 2);
		if (!head)
			head = temp;
		else
			tail->next = temp;
		temp->type = LT_BORING;
		temp->d0free = 0;
		cpin = buf;
		cpout = temp->data;
		while (*cpin && *cpin != '\n')
			*cpout++ = *cpin++;
		*cpout = 0;
		temp->prev = tail;
		temp->next = 0;
		tail = temp;
	}
}

/*
 * Scan across the file, working out where d0 can be safely used
 */

scanfile()
{
	register struct line *line;
	register char d0free;
	register char *cp;

	d0free = 0;
	line = tail;
	while (line)
	{
		line->d0free = d0free;

		if (!scp("\tjsr\t_", line->data))
		{	/* d0 free before C jsr */
			d0free = 1;
		}
		else if (d0free)
		{	/* If d0 appears in the operands or there
			 * is a branch or a jump, unfree d0.
			 */

			if (!scp("\tb", line->data))
			{
				d0free = 0;
			}
			else
			{
				cp = line->data;
				while (*cp && d0free)
				{
					while (*cp && (*cp != 'd') && (*cp != 'D'))
						cp++;
					if (*cp && cp1 == '0')
						d0free = 0;		/* D0 or d0 */
					else
						cp++;
				}
			}
		}
		line = line->prev;
	}
}

usage()
{
	fprintf(stderr, "Usage: %s -dopsv filename\n", ourname);
	exit(-1);
}

/*
 * Compare s1 with s2, stopping at end of s1. Return 0 on match
 */

#ifdef notdef
scp(s1, s2)
char *s1, *s2;
{
	return strnucmp(s1, s2, strlen(s1));
}
#endif

#asm
		global	_scp
_scp:		move.l	4(sp),a0
		move.l	8(sp),a1

loopon:
		move.b	(a0)+,d0	; At end of source: match
		beq.b	same

		move.b	(a1)+,d1
		cmp.b	d0,d1
		beq.b	loopon

; Maybe case mismatch?

; Convert d0 to upper case
		cmp.b	#'a',d0
		blt	d0notlower
		cmp.b	#'z',d0
		bgt	d0notlower
		sub.b	#$20,d0
d0notlower:
		cmp.b	d0,d1
		beq.b	loopon

; Convert d1 to upper case
		cmp.b	#'a',d1
		blt	d1notlower
		cmp.b	#'z',d1
		bgt	d1notlower
		sub.b	#$20,d1
d1notlower:
		cmp.b	d0,d1
		beq.b	loopon

; No match
		move.b	#1,d0
		rts
same:		sub.l	4(sp),a0
		subq.l	#1,a0
		move.l	a0,_scplength
		clr.l	d0
		rts
#endasm
