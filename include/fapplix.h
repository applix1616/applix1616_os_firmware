/* Header file for Applix 1616 system calls */

extern long	trap7();

/* Shouldn't be needed with my new libraries */
#ifdef notdef

#define	DEV	64		/* offset of character device handles */

#define	CON	DEV+0
#define	SERA	DEV+1
#define	SERB	DEV+2
#define	CENT	DEV+3

#endif
