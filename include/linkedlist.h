/* TABS4 NONDOC */
#ifndef IN_LINKEDLIST_H
#define IN_LINKEDLIST_H
/*
 * Definitions for linked list manipulation library
 */

#include <stdarg.h>

/*
 * You can predefine MYSTRUCT to the name of the structures
 * that are being manipulated or you can leave it alone,
 * in which case it becomes just void *, which is OK.
 */

#ifdef MYSTRUCT
#define LL_TYPE MYSTRUCT
#else
#define LL_TYPE void
#endif

/* llsort.c */
void llSort(LL_TYPE **topptr,
				int (*compare)(LL_TYPE *, LL_TYPE *),
				LL_TYPE *(*getnext)(LL_TYPE *),
				void (*setnext)(LL_TYPE *, LL_TYPE *));

/* Macros to do all the casting... */
#define LLSORT(list, compare, getnext, setnext) \
	llSort((LL_TYPE **)list, \
			(int (*)(LL_TYPE *, LL_TYPE *))compare, \
			(LL_TYPE *(*)(LL_TYPE *))getnext, \
			(void (*)(LL_TYPE *, LL_TYPE *))setnext)

/* dllsort.c */
void dllSort(LL_TYPE **topptr,
				int (*compare)(LL_TYPE *, LL_TYPE *),
				LL_TYPE *(*getnext)(LL_TYPE *),
				void (*setnext)(LL_TYPE *, LL_TYPE *),
				void (*setprev)(LL_TYPE *, LL_TYPE *));

#define DLLSORT(list, compare, getnext, setnext, setprev) \
	dllSort((LL_TYPE **)list, \
			(int (*)(LL_TYPE *, LL_TYPE *))compare, \
			(LL_TYPE *(*)(LL_TYPE *))getnext, \
			(void (*)(LL_TYPE *, LL_TYPE *))setnext, \
			(void (*)(LL_TYPE *, LL_TYPE *))setprev)

/* lladvance.c */
LL_TYPE *llAdvance(LL_TYPE **list,
				LL_TYPE **position,
				LL_TYPE *(*getnext)(LL_TYPE *));

#define LLADVANCE(list, position, getnext) \
		llAdvance((LL_TYPE **)list, \
				(LL_TYPE **)position, \
				(LL_TYPE *(*)(LL_TYPE *))getnext)

/* dllAdvance is the same */
#define dllAdvance llAdvance
#define DLLADVANCE(list, position, getnext) \
		dllAdvance((LL_TYPE **)list, \
				(LL_TYPE **)position, \
				(LL_TYPE *(*)(LL_TYPE *))getnext)

/* llinsert.c */
void llInsert(LL_TYPE **list, LL_TYPE *member,
			LL_TYPE *(*getnext)(LL_TYPE *),
			LL_TYPE (*setnext)(LL_TYPE *, LL_TYPE *));

#define LLINSERT(list, member, getnext, setnext) \
	llInsert((LL_TYPE **)list, (LL_TYPE *)member, \
			(LL_TYPE *(*)(LL_TYPE *))getnext, \
			(LL_TYPE *(*)(LL_TYPE *, LL_TYPE *))setnext)

/* dllinsert.c */
void dllInsert(LL_TYPE **list,
		LL_TYPE *member,
		LL_TYPE *(*getnext)(LL_TYPE *),
		void (*setnext)(LL_TYPE *, LL_TYPE *),
		LL_TYPE *(*getprev)(LL_TYPE *),
		void (*setprev)(LL_TYPE *, LL_TYPE *),
		int insertbefore);

#define DLLINSERT(list, member, getnext, setnext, getprev, setprev, insertbefore) \
	dllInsert((LL_TYPE **)list, \
		(LL_TYPE *)member, \
		(LL_TYPE *(*)(LL_TYPE *))getnext, \
		(void (*)(LL_TYPE *, LL_TYPE *))setnext, \
		(LL_TYPE *(*)(LL_TYPE *))getprev, \
		(void (*)(LL_TYPE *, LL_TYPE *))setprev, \
		insertbefore)

#define DLL_INSERT_BEFORE 1
#define DLL_INSERT_AFTER 0

/* lldelete.c */
LL_TYPE *llDelete(LL_TYPE **list, LL_TYPE **pmember,
		LL_TYPE *(*getnext)(LL_TYPE *),
		void (*setnext)(LL_TYPE *, LL_TYPE *),
		void (*destroy)(LL_TYPE *));
#define LLDELETE(list, member, getnext, setnext, destroy) \
	llDelete((LL_TYPE **)list, (LL_TYPE *)member, \
		(LL_TYPE *(*)(LL_TYPE *))getnext, \
		(void (*)(LL_TYPE *, LL_TYPE *))setnext, \
		(void (*)(LL_TYPE *))destroy);

/* dlldelete.c */
LL_TYPE *dllDelete(LL_TYPE **list, LL_TYPE **pmember,
		LL_TYPE *(*getnext)(LL_TYPE *),
		void (*setnext)(LL_TYPE *, LL_TYPE *),
		LL_TYPE *(*getprev)(LL_TYPE *),
		void (*setprev)(LL_TYPE *, LL_TYPE *),
		void (*destroy)(LL_TYPE *));
#define DLLDELETE(list, member, getnext, setnext, getprev, setprev, destroy) \
	dllDelete((LL_TYPE **)list, \
			(LL_TYPE *)member, \
			(LL_TYPE *(*)(LL_TYPE *))getnext, \
			(void (*)(LL_TYPE *, LL_TYPE *))setnext, \
			(LL_TYPE *(*)(LL_TYPE *))getprev, \
			(void (*)(LL_TYPE *, LL_TYPE *))setprev, \
			(void (*)(LL_TYPE *))destroy);

/* llcount.c */
int llCount(LL_TYPE **list,
			LL_TYPE *(*getnext)(LL_TYPE *));
#define LLCOUNT(list, getnext) \
	LLCOUNT((LL_TYPE **)list, (LL_TYPE *(*)(LL_TYPE *))getnext)

/* dllcount() is the same */
#define dllCount llCount
#define DLLCOUNT(list, getnext) \
	LLCOUNT((LL_TYPE **)list, (LL_TYPE *(*)(LL_TYPE *))getnext)

/* llreverse.c */
void llReverse(LL_TYPE **list,
				LL_TYPE *(*getnext)(LL_TYPE *),
				void (*setnext)(LL_TYPE *, LL_TYPE *));

#define LLREVERSE(list, getnext, setnext) \
		llReverse((LL_TYPE **)list, \
			(LL_TYPE *(*)(LL_TYPE *))getnext, \
			(void (*)(LL_TYPE *, LL_TYPE *))setnext);

/* dllreverse.c */
void dllReverse(LL_TYPE **list,
			LL_TYPE *(*getnext)(LL_TYPE *),
			void (*setnext)(LL_TYPE *, LL_TYPE *),
			void (*setprev)(LL_TYPE *, LL_TYPE *));

#define DLLREVERSE(list, getnext, setnext, setprev) \
		dllReverse((LL_TYPE **)list, \
			(LL_TYPE *(*)(LL_TYPE *))getnext, \
			(void (*)(LL_TYPE *, LL_TYPE *))setnext, \
			(void (*)(LL_TYPE *, LL_TYPE *))setprev);

/* llapply.c */
void llApply(void **list, void (*func)(), void *(*getnext)(void *), va_list);

/* dllapply works the same */
#define dllApply llApply

/* llappend.c */
void llAppend(LL_TYPE **list,
			LL_TYPE *member,
            LL_TYPE *(*getnext)(LL_TYPE *),
            void (*setnext)(LL_TYPE *, LL_TYPE *));
#define LLAPPEND(list, member, getnext, setnext) \
		llAppend((LL_TYPE **)list, \
			(LL_TYPE *)member, \
            (LL_TYPE *(*)(LL_TYPE *))getnext, \
            (void (*)(LL_TYPE *, LL_TYPE *))setnext);

/* llprepend.c */
void llPrepend(LL_TYPE **list, LL_TYPE *member,
            void (*setnext)(LL_TYPE *, LL_TYPE *));
#define LLPREPEND(list, member, setnext) \
		llPrepend((LL_TYPE **)list, (LL_TYPE *)member, \
            (void (*)(LL_TYPE *, LL_TYPE *))setnext);

/* dllprepend.c */
void dllPrepend(LL_TYPE **list, LL_TYPE *member,
            void (*setnext)(LL_TYPE *, LL_TYPE *),
            void (*setprev)(LL_TYPE *, LL_TYPE *));
#define DLLPREPEND(list, member, setnext, setprev) \
		llPrepend((LL_TYPE **)list, (LL_TYPE *)member, \
            (void (*)(LL_TYPE *, LL_TYPE *))setnext, \
            (void (*)(LL_TYPE *, LL_TYPE *))setprev)

/* dllappend.c */
void dllAppend(LL_TYPE **list,
			LL_TYPE *member,
            LL_TYPE *(*getnext)(LL_TYPE *),
            void (*setnext)(LL_TYPE *, LL_TYPE *),
			void (*setprev)(LL_TYPE *, LL_TYPE *));
#define DLLAPPEND(list, member, getnext, setnext, setprev) \
		dllAppend((LL_TYPE **)list, \
			(LL_TYPE *)member, \
            (LL_TYPE *(*)(LL_TYPE *))getnext, \
            (void (*)(LL_TYPE *, LL_TYPE *))setnext, \
            (void (*)(LL_TYPE *, LL_TYPE *))setprev)
			
/* dllbackpatch.c */
void dllBackPatch(LL_TYPE **list,
			LL_TYPE *(*getnext)(LL_TYPE *),
			void (*setprev)(LL_TYPE *, LL_TYPE *));

#define DLLBACKPATCH(list, getnext, setprev) \
			dllBackPatch((LL_TYPE *)list, \
				(LL_TYPE *(*)(LL_TYPE *))getnext, \
				(void (*)(LL_TYPE *, LL_TYPE *))setprev);

/* llmalloc.c */
void *llMalloc(long nbytes);
/* llfree.c */
void llFree(void *);
/* malloc.c */
void *Malloc(long nbytes);
void *Realloc(void *addr, long nbytes);
void Free(void *addr);

#endif /* IN_LINKEDLIST_H */
