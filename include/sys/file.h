/* Modes to access() */

#define X_OK	01		/* Executable */
#define W_OK	02		/* Writable */
#define R_OK	04		/* Readable */

#define F_OK	R_OK
