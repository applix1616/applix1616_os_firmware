typedef	unsigned char	uchar;
typedef	unsigned short	ushort;
typedef	unsigned long	ulong;
typedef	unsigned int	uint;
typedef unsigned long	off_t;
#ifndef TIME_TYPEDEFD
#define TIME_TYPEDEFD
typedef unsigned long	time_t;
#endif
struct utimbuf
{
	time_t actime;	/* access time */
	time_t modtime;	/* modification time */
};
