/* TABS4 NONDOC
 * Header for sound files.
 */

typedef struct snd_header
{
	unsigned long snd_magic;
	unsigned long frequency;	/* Sample frequency in Hz */
	unsigned long flags;		/* Status bits */
	unsigned short nbits;		/* Bits / sample (???) */
	unsigned short pad9;		/* Expansion */
} SND_HEADER;

#define SND_MAGIC	0x314159ed

/* Bits in snd_header.flags */
#define SND_STEREO	0x0001		/* If true, stereo sample */
