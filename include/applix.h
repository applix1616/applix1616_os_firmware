/*
 *	Header file for Applix 1616 C library
 */

#ifndef uchar
typedef	unsigned char	uchar;
#endif
typedef	unsigned short	ushort;
typedef	unsigned long	ulong;
typedef	unsigned int	uint;

/*
 *	file I/O definitions
 */

#define NFCBS		16	/* May have 16 files open at once */
#define FNAMESIZE	32	/* Max file name size */
#define TPALOC		0x4000	/* Where files load in */

/* open() modes */
#define O_RDONLY	1
#define O_WRONLY	2
#define O_RDWR		3

/* File control block structure */
struct fcb
{
	char *file_name;	/* The full pathname */
	ushort openmode;	/* Mode with which file was opened/created */
	ushort bdvrnum;		/* Appropriate driver number */
	ushort *blkmap;		/* Pointer to the file's block map */
	ushort bmindex;		/* Current index into blkmap */
	char *blkbuf;		/* Pointer to buffer for disk I/O */
	ushort status;		/* Non-zero: fcb is being used */
	ushort dirblk;		/* Block which holds file's directory entry */
	ushort dirindex;	/* The position in the directory block */
	int file_size;		/* Accumulated length */
	int file_pos;		/* Where we are */
	ushort blkmapblk;	/* Where the file's block map is on disk */
	ushort changed;		/* Flag: current block must be written out */
	ushort blkstat;		/* Flag: 0: block must be read */
	};

/* Disk directory entry. Must be 64 bytes long! */
struct dir_entry
{
  char file_name[FNAMESIZE];	/* Upper case only */
/* 32 */ char date8;
/* 40 */ ushort reserved;
/* 42 */ char *load_addr;	/* Code execution point */
/* 46 */ int file_size;		/* Total length (No. of blocks for directories)*/
/* 50 */ ushort statbits;	/* Status bits: see below */
/* 52 */ ushort blkmapblk;	/* Block map (start block for directories) */
/* 54 */ char rest10;		/* Padding */
};

/* Masks for statbits field above */
#define BACKEDUP	1		/* File has been backed up somewhere */
#define DIRECTORY	2		/* It is a directory */
#define LOCKED		4		/* Cannot be altered */

/* Number of bytes/directory entry */
#define DIRENTSIZE	64


/*
 * 1616/OS V3.0 system calls
 *
 * Andrew Morton, Applix pty limited
 */

#define STDERR		0x80	/* Standard error file descriptor */
#define STDOUT		0x81	/* Standard output file descriptor */
#define STDIN		0x82	/* Standard input file descriptor */

/*
 * We macro all the system calls as calls to trap7().
 * The system calls are in upper case to distinguish them from
 * normal function calls, such as C library functions
 *
 * Note that the printf, sprintf and fprintf calls are missing here
 * because they use a variable number of arguments, which confuses C
 * preprocessors.
 */

extern long trap7();

#define COLDBOOT()			trap7(0)
#define WARMBOOT(REASON)		trap7(1,REASON)
#define GETCHAR()			trap7(2)
#define SGETCHAR()			trap7(3)
#define PUTCHAR(CH)			trap7(4, CH)
#define SPUTCHAR()			trap7(5)
#define GETC(DVR)			trap7(6, DVR)
#define SGETC(DVR)			trap7(7, DVR)
#define PUTC(DVR, CH)			trap7(8, DVR, CH)
#define SPUTC(DVR)			trap7(9, DVR)
#define ADD_IPDVR(IO, STAT, NAME, PV)	trap7(10, IO, STAT, NAME, PV)
#define LOADREL(IFD, ADDR)		trap7(11, IFD, ADDR)
#define ADD_OPDVR(IO, STAT, NAME, PV)	trap7(12, IO, STAT, NAME, PV)
#define EXIT(I)				trap7(13, I)
#define SET_SIP(DVR)			trap7(14, DVR)
#define SET_SOP(DVR)			trap7(15, DVR)
#define SET_VSVEC(VEC, RATE, CALLVAL)	trap7(16, VEC, RATE, CALLVAL)
#define CLR_VSVEC(VNUM)			trap7(17, VNUM)
#define GET_TICKS()			trap7(18)
#define GET_CPU()			trap7(19)
#define SET_SER(DVR)			trap7(20, DVR)
#define CASWRAW(START, LENGTH, LEADER)	trap7(21, START, LENGTH, LEADER)
#define CASRRAW(BUF, LEADER, MAXHUNK)	trap7(22, BUF, LEADER, MAXHUNK)
#define GETDATE(STR)			trap7(23, STR)
#define SETDATE(STR)			trap7(24, STR)
#define ABORTSTAT()			trap7(25)
#define ENT1INTS(VEC, PRELOAD)		trap7(26, VEC, PRELOAD)
#define DIST1INTS()			trap7(27)
#define SINE(I)				trap7(28, I)
#define DEF_FK(FKNUM, STR)		trap7(29, FKNUM, STR)
#define GETRAND()			trap7(30)
#define SET_640(MODE)			trap7(31, MODE)
#define SET_VDP(PAGE)			trap7(32, PAGE)
#define SET_VAP(PAGE)			trap7(33, PAGE)
#define SET_FGCOL(COL)			trap7(34, COL)
#define SET_BGCOL(COL)			trap7(35, COL)
#define SET_BDCOL(COL)			trap7(36, COL)
#define SET_PAL(PALPOS, COL)		trap7(37, PALPOS, COL)
#define RDCH_SHAPE(CHNO)		trap7(38, CHNO)
#define DEF_CHSHAPE(CHNO, DEFPTR)	trap7(39, CHNO, DEFPTR)
#define DEF_WIND(WIND)			trap7(40, WIND)
#define VID_ADDRESS(X, Y)		trap7(41, X, Y)
#define MOVE_WIND(BUF, MODE)		trap7(42, BUF, MODE)
#define RAWVID(ROW, COL, CH, FG, BG)	trap7(43, ROW, COL, CH, FG, BG)
#define FILL_WIND(COL)			trap7(44, COL)
#define SCURS_MODE(RATE, EN, MASK)	trap7(45, RATE, EN, MASK)
#define MOUSETRAP(N, V)			trap7(46, N, V)
#define FILL(X, Y, VAL)			trap7(47, X, Y, VAL)
#define RSET_PEL(X, Y, VAL)		trap7(50, X, Y, VAL)
#define SET_PEL(X, Y, VAL)		trap7(51, X, Y, VAL)
#define RLINE(X0, Y0, X1, Y1)		trap7(52, X0, Y0, X1, Y1)
#define DRAWLINE(X0, Y0, X1, Y1)	trap7(53, X0, Y0, X1, Y1)
#define RREAD_PEL(X, Y)			trap7(54, X, Y)
#define READ_PEL(X, Y)			trap7(55, X, Y)
#define SGFGCOL(VAL)			trap7(56, VAL)
#define RCIRCLE(X, Y, RADIUS)		trap7(59, X, Y, RADIUS)
#define CIRCLE(X, Y, RADIUS)		trap7(60, X, Y, RADIUS)
#define SDOTMODE(MODE)			trap7(61, MODE)
#define GETMEM(NBYTES, MODE)		trap7(62, NBYTES, MODE)
#define GETFMEM(FIRST, NBYTES, MODE)	trap7(63, FIRST, NBYTES, MODE)
#define FREEMEM(ADDR)			trap7(64, ADDR)
#define CHDIR(ADDR)			trap7(65, ADDR)
#define MKDIR(ADDR, N)			trap7(66, ADDR, N)
#define GETFULLPATH(PATH, MODE)		trap7(67, PATH, MODE)
#define PATHCMP(P1, P2)			trap7(68, P1, P2)
#define FLOADREL(PATH, GM)		trap7(69, PATH, GM)
#define ANIPSEL(IPNUM)			trap7(70, IPNUM)
#define ANOPSEL(OPNUM)			trap7(71, OPNUM)
#define ANOPDIS()			trap7(72)
#define ADC()				trap7(73)
#define DAC(VAL)			trap7(74, VAL)
#define SET_LED(VAL)			trap7(75, VAL)
#define FREETONE(TABLE, TABLEN, L, P)	trap7(76, TABLE, TABLEN, L, P)
#define FTTIME()			trap7(77)
#define	RDIPORT()			trap7(78)
#define RDBIPORT()			trap7(79)
#define SETSTVEC(VECNUM, WHERETO)	trap7(80, VECNUM, WHERETO)
#define NEW_CBUF(BNUM, ADDR, LEN)	trap7(81, BNUM, ADDR, LEN)
#define PROG_SIO(CHAN, SPPTR)		trap7(82, CHAN, SPPTR)
#define GETTDSTR(BUF)			trap7(83, BUF)
#define NLEDIT(STR, LEN)		trap7(84, STR, LEN)
#define LEDIT(STR)			trap7(86, STR)
#define IEXEC(PROMPT)			trap7(87, PROMPT)
#define EXEC(LB)			trap7(88, LB)
#define CALLMRD(N, CMD, ARG)		trap7(89, N, CMD, ARG)
#define SET_KVEC(VEC)			trap7(90, VEC)
#define CLPARSE(A, T, V)		trap7(91, A, T, V)
#define QSORT(B, N, W, C)		trap7(92, B, N, W, C)
#define SLICEARGS(S, A, C)		trap7(93, S, A, C)
#define FIND_DRIVER(IORO, NAME)		trap7(95, IORO, NAME)
#define GET_DVRLIST(IORO)		trap7(96, IORO)
#define EXECA(ARGV)			trap7(97, ARGV)
#define EXECV(PATH, ARGV)		trap7(98, PATH, ARGV)
#define OPTION(OPNUM, SET)		trap7(99, OPNUM, SET)
#define INST_BDVR(BR, BW, BS, NAME, B)	trap7(100, BR, BW, BS, NAME, B)
#define FIND_BDVR(NAME)			trap7(102, NAME)
#define BLKREAD(BLK, BUF, DEV)		trap7(103, BLK, BUF, DEV)
#define BLKWRITE(BLK, BUF, DEV)		trap7(104, BLK, BUF, DEV)
#define OPEN(NAME, MODE)		trap7(105, NAME, MODE)
#define READ(FD, BUF, NBYTES)		trap7(106, FD, BUF, NBYTES)
#define CLOSE(FD)			trap7(107, FD)
#define CREAT(NAME, BITS, ADDR)		trap7(108, NAME, BITS, ADDR)
#define WRITE(FD, BUF, NBYTES)		trap7(109, FD, BUF, NBYTES)
#define UNLINK(NAME)			trap7(110, NAME)
#define RENAME(OLD, NEW)		trap7(111, OLD, NEW)
#define FILESTAT(NAME, BUF)		trap7(112, NAME, BUF)
#define READDIR(DEV, BUF, DP, POS, PAR)	trap7(113, DEV, BUF, DP, POS, PAR)
#define INTERPBEC(EC, BUF)		trap7(114, EC, BUF)
#define SEEK(FD, OFFSET, MODE)		trap7(115, FD, OFFSET, MODE)
#define TELL(FD)			trap7(116, FD)
#define BDMISC(BDNUM, CODE, ARG1)	trap7(117, BDNUM, CODE, ARG1)
#define PROCESSDIR(PN, BUF, MODE)	trap7(118, PN, BUF, MODE)
#define FPUTS(FD, BUF)			trap7(121, FD, BUF)
#define ERRMES(EC)			trap7(122, EC)
#define FGETS(FD, BUF)			trap7(123, FD, BUF)
#define GETROMVER()			trap7(126)
#define NEWCHSET(PTR)			trap7(130, NEWCHSET)

/*
 * What the hell is this???
 *
 * printf, fprintf and sprintf cannot be macroed easily because they use
 * a variable number of arguments. Also, the system call mechanism prevents
 * us from using more than 5 arguments.
 * The following casts use the setstvec system call to read the address of
 * the system's printf(), etc functions, then jump to them. Can use any
 * number of arguments. NEEDS \r before \n's!!!!
 */

#define PRINTF		(*(int(*)())SETSTVEC(48, -1))
#define SPRINTF		(*(int(*)())SETSTVEC(49, -1))
#define FPRINTF		(*(int(*)())SETSTVEC(120, -1))
