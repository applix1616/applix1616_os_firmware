/*
 * Define the bits in the 'envbits' part of the user environment structure,
 * set and read by the 'setenvbits' and'getenvbits' system calls
 */

#define ENVB_PROMPTEN		0x0001	/* Enable working dir in prompt */
#define ENVB_VERBOSE		0x0002	/* Commands run in verbose mode */
#define ENVB_DIRMODE0		0x0004	/* Suppress directory sorting */
#define ENVB_DIRMODE1		0x0008	/* 0: time dir sort 1: alphabetic */
#define ENVB_IBBEEP		0x0010	/* Beep on inbuilt command error */
#define ENVB_NOBAK		0x0020	/* Editors don't generate .bak files */
#define ENVB_ERRBEEP		0x0040	/* Beep when programs exit wth error */
#define ENVB_HIDEFILES		0x0080	/* Hide files with hidden bit set */
#define ENVB_ASSIGNPROMPT	0x0100	/* Truncate prompt with assignments */
#define ENVB_LOWERNAMES		0x0200	/* Enable lower case filenames */
#define ENVB_PROMPTGT		0x0400	/* Enable '>' in prompts */
#define ENVB_DODSIGN		0x0800	/* Do '$' exec environ substititions */
#define ENVB_DOARG		0x1000	/* Do argument environ substititions */
#define ENVB_DOARG0		0x2000	/* Do argv0 environ substititions */
#define ENVB_NOSHELLOUT		0x4000	/* This user cannot shell out */
#define ENVB_LOWBAUD		0x8000	/* User is running on slow terminal */

/* Default environmnet bits */
#define DEF_ENVBITS	(ENVB_PROMPTEN|ENVB_VERBOSE|ENVB_DIRMODE0|\
				ENVB_DIRMODE1|ENVB_IBBEEP|ENVB_PROMPTGT|\
				ENVB_LOWERNAMES|ENVB_DODSIGN|ENVB_DOARG0)

/*
 * Bits in the envstring.mode field. These control the substitution of
 * the particular environment string into EXEC command lines
 */

#define ENVS_DSIGN	0x0001	/* Substitute in '$' */
#define ENVS_ARG	0x0002	/* Substitute in all arguments */
#define ENVS_ARG0	0x0004	/* Substitute in argv0 */

