/* TABS8 NONDOC */
/*   Description of Motorola 68681 DUART chip  */

/*   @(#)duart.h	3.1:  13:57:05 88/01/07  */


/*   Structure defining per channel part of duart  */

/*   **************************************************************
 *	NOTE:   On the terminal processor card,  the DUARTs appear as
 *   byte registers on ODD addresses,  causing each register to be 2
 *   bytes higher than the last one.  This is the cause of the padding.
 *   The odd address is catered for by the reference address.
 */

struct  d_chnl
{
	uchar	d_mode,	d_pad1;		/* Mode register */
	uchar	d_clock,d_pad0;		/* Clock select; status register */
	uchar	d_cmd,	d_pad3;		/* Command register, write only */
	uchar	d_chbuf,d_pad2;		/* Receiver/transmitter buffer */
};

/*   Structure describing the WHOLE duart */

struct  duart
{
	struct	d_chnl	d_achnl;	/* A channel specific regs */
	uchar	d_aux,	d_pad5;		/* Auxiliary register/input change */
	uchar	d_intrg,d_pad4;		/* Interrupt register */
	uchar	d_ucntr,d_pad7;		/* Upper counter/timer */
	uchar	d_lcntr,d_pad6;		/* Lower counter/timer */
	struct	d_chnl	d_bchnl;	/* B channel specific registers */
	uchar	d_ivec,	d_pad9;		/* Vector register; R/W */
	uchar	d_opcr,	d_pad8;		/* Output port configuration */
	uchar	d_obset,d_padb;		/* Output port bit set */
	uchar	d_obclr,d_pada;		/* Output port bit reset */
};

/*   Set up the registers which have the same address but a different name */
#define d_status d_clock		/* Channel status register */
#define	d_ipcr	d_aux			/* Input port change register */
#define	d_isr	d_intrg			/* Interrupt status register */
#define	d_input	d_opcr			/* Input port */
#define	d_cstart	d_obset		/* Start counter */
#define	d_cstop	d_obclr			/* Stop counter */

/*
 *	The magic bits to go into the above registers.
 */

#define	RESET_VECTOR	0xf		/* The DUART's uninitialised vector */

/*   MODE REGISTER 1:  Not normally accessible; requires command to get at */
#define	BITS5		0x00	/* 5 bits per character */
#define	BITS6		0x01	/* Obvious */
#define	BITS7		0x02
#define	BITS8		0x03

#define	ODDPAR		0x04	/* Odd parity */
#define	EVENPAR		0x00	/* Not ODD */

#define	PARON		0x00	/* Enable parity */
#define	PARFORCE	0x08	/* Force parity, uses ODD/EVEN bit above */
#define	PAROFF		0x10	/* No parity */
#define	MULTIDROP	0x18	/* Multidrop mode */

#define	ERROR_CHAR	0x00	/* Per character error mode */
#define	ERROR_BLOCK	0x20	/* Per block error status */

#define	RX_RDY		0x00	/* Interrupt when character available */
#define	FIFO_FULL	0x40	/* Interrupt when FIFO full */

#define	RXRTS_OFF	0x00	/* No RTS control from receiver */
#define	RXRTS_ON	0x80	/* RTS is receiver full handshake */


/*   MODE REGISTER 2: same as above after above read/written once  */
#define	STOPB1		0x07	/* One stop bit, 6->8 bit chars */
#define	STOPB2		0x0f	/* Two stop bits, 6->8 bit chars */
#define	B5STOPB1_5	0x07	/* 1.5 stop bits, 5 bit chars */
#define	B5STOPB2	0x0f	/* Two stop bits, 5 bit chars */

#define	TXIGN_CTS	0x00	/* Transmit ignores state of CTS */
#define	TXUSE_CTS	0x10	/* CTS enables transmitter */

#define	TXIGN_RTS	0x00	/* RTS is independent of data transmission */
#define	TXUSE_RTS	0x20	/* RTS disabled when TX empties */

#define	OPS_NORMAL	0x00	/* Normal opeation - i.e. NOT following */
#define	OPS_ECHO	0x40	/* Auto echo - quite useless */
#define	OPS_LCLOOPB	0x80	/* Local loopback */
#define	OPS_RMLOOPB	0xc0	/* Remote loopback */

/*   Baud rates - set1  */
#define	S1B50		0x00
#define	S1B110		0x01
#define	S1B134_5	0x02
#define	S1B200		0x03
#define	S1B300		0x04
#define	S1B600		0x05
#define	S1B1200		0x06
#define	S1B1050		0x07
#define	S1B2400		0x08
#define	S1B4800		0x09
#define	S1B7200		0x0a
#define	S1B9600		0x0b
#define	S1B38400	0x0c
#define	S1TIMER		0x0d
#define	S1EXT16X	0x0e
#define	S1EXT1X		0x0f

/*    Set 2  */
#define	S2B75		0x00
#define	S2B110		0x01
#define	S2B134_5	0x02
#define	S2B150		0x03
#define	S2B300		0x04
#define	S2B600		0x05
#define	S2B1200		0x06
#define	S2B2000		0x07
#define	S2B2400		0x08
#define	S2B4800		0x09
#define	S2B1800		0x0a
#define	S2B9600		0x0b
#define	S2B19200	0x0c
#define	S2TIMER		0x0d
#define	S2EXT16X	0x0e
#define	S2EXT1X		0x0f

/*   Command register   */
#define	RX_NOP		0x00
#define	RX_GO		0x01		/* Receiver enable */
#define	RX_STOP		0x02		/* Stop receiver */
/* 	RX_ILLEGAL	0x03		   Illegal command */

#define	TX_NOP		0x00
#define	TX_GO		0x04		/* Transmitter enable */
#define	TX_STOP		0x08		/* Transmitter disable */
/*	TX_ILLEGAL	0x0c		   Illegal command */

#define	M_NOP		0x00
#define	M_GETMR1	0x10		/* Next access is to MR1 */
#define	M_RXRESET	0x20		/* Reset receiver */
#define	M_TXRESET	0x30		/* Reset transmitter */
#define	M_ERRESET	0x40		/* Reset error status */
#define	M_BKRESET	0x50		/* Reset break/change status */
#define	M_BREAK		0x60		/* Start break */
#define	M_FIX		0x70		/* Stop break */

/*  STATUS REGISTER   */
#define	RXRDY		0x01		/* Receiver has a character */
#define	FULL_FIFO	0x02		/* Fifo is full */
#define	TXRDY		0x04		/* Transmitter ready for character */
#define	TX_EMPTY	0x08		/* Transmitter shift reg empty */
#define	RX_OVERRUN	0x10		/* Overrun error */
#define	RX_PARITY	0x20		/* Parity error */
#define	RX_FRAME	0x40		/* Framing error */
#define	RX_BREAK	0x80		/* Break received */

/*   Output configuration register  */

/*   Auxiliary control register  */
#define	IP0_OFF_DEL	0x00		/* IP0 change doesn't interrupt */
#define	IP0_ON_DEL	0x01		/* IP0 change will interrupt */

#define	IP1_OFF_DEL	0x00		/* Ditto for IP1 */
#define	IP1_ON_DEL	0x02

#define	IP2_OFF_DEL	0x00		/* IP2 */
#define	IP2_ON_DEL	0x04

#define	IP3_OFF_DEL	0x00		/* IP3 */
#define	IP3_ON_DEL	0x08

#define	CN_EXT		0x00		/* Counter, external clock */
#define	CN_TXCA1X	0x10		/* Counter, 1X channel A Tx clock */
#define	CN_TXCB1X	0x20		/* Counter, 1X channel B Tx clock */
#define	CN_XTAL16X	0x30		/* Counter, crystal clock, x16 */
#define	TM_X1EXT	0x40		/* Timer, external X1 */
#define	TM_X16EXT	0x50		/* Timer, external X16 */
#define	TM_X1XTAL	0x60		/* Timer, crystal X1 */
#define	TM_X16XTAL	0x70		/* Timer, crystal X16 */

#define	BRG_SET1	0x00		/* Baud rate set #1 */
#define	BRG_SET2	0x80		/* Baud rate set #2 */


/*   INPUT PORT CHANGE REGISTER     */
#define	IP0_LEVEL	0x01		/* IP0 Level */
#define	IP1_LEVEL	0x02		/* IP1 */
#define	IP2_LEVEL	0x04
#define	IP3_LEVEL	0x08
#define	IP0_DELTA	0x10		/* IP0 level has CHANGED */
#define	IP1_DELTA	0x20
#define	IP2_DELTA	0x40
#define	IP3_DELTA	0x80

/*   INTERRUPT STATUS REGISTER  & INTERRUPT MASK REGISTER  */
#define	ATX_RDY		0x01		/* Chan A TX ready */
#define	ARX_RDYFF	0x02		/* Chan A RX: fifo full or char ready */
#define	ABRK_DELTA	0x04		/* Chan A delta break */
#define	CNTIMER_RDY	0x08		/* Counter/timer ready */
#define	BTX_RDY		0x10		/* Chan B TX ready */
#define	BRX_RDYFF	0x20		/* Chan B RX: fifo full, char ready */
#define	BBRK_DELTA	0x40		/* Chan B delta break */
#define	INP_DELTA	0x80		/* Input port change */
