/* TABS4 AI NONDOC */
/*
 * A guess at what the directory routines structures are like
 * possibly quite accurate
 */

struct direct
{
	char	*d_name;			/* File name */
	int		d_namlen;			/* Name length */
	int		d_ino;				/* non-zero if valid entry */
};

typedef struct
{
	struct direct *D_dirent;	/* Pointer to current dir array */
	int 	D_cur_entry;		/* Current entry */
	int		D_numents;			/* Number of entries */
} DIR;

DIR *opendir(char *);
struct direct *readdir(DIR *);
void closedir(DIR *);
int seekdir(DIR *, long);
long telldir(DIR *);
void rewinddir(DIR *);

