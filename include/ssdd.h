/*
 * Definitions for the 1616 disk controller
 */

#define SSDDBASE	0xffffc1

#define SSDDDATA	((uchar *)SSDDBASE)	/* Data read/write port */
#define SSDDINTZ	((uchar *)SSDDBASE + 2)	/* Write to interrupt Z80 */
#define SSDDCLRINT	((uchar *)SSDDBASE + 2)	/* Read to clear 68k int */
#define SSDDRXRDY	((uchar *)SSDDBASE + 8)	/* bit 7 set if I/P buffer full */
#define SSDDTXRDY	((uchar *)SSDDBASE + 10)/* bit 7 set if O/P buffer empty */
#define SSDDCOMMAND	((uchar *)SSDDBASE + 12)/* bit 7 set if Z80 sent a command */
#define SSDDCMND	((uchar *)SSDDBASE + 16)/* Command output port */

#define RXRDY		(*SSDDRXRDY & 0x80)	/* True if receiver ready */
#define TXRDY		(*SSDDTXRDY & 0x80)	/* True if transmitter ready */

/* The controller commands - Standard on ALL vers	*/

#define DCC_BLKREAD	1		/* Block read */
#define DCC_BLKWRITE	2		/* Block write */
#define DCC_ERRMES	3		/* Error message */
#define DCC_FORMAT	4		/* Disk format */
#define DCC_FLUSH	6		/* Flush buffers */
#define DCC_READMEM	7		/* Read Z80 memory */
#define DCC_WRITEMEM	8		/* Write Z80 memory */
#define DCC_CALL	9		/* Call a Z80 subroutine */
#define DCC_GETDDROMVER	10		/* Get controller ROM version */
#define DCC_SENDSSROMVER 11		/* Send 1616 ROM version */
#define DCC_SETSTEP	12		/* Set disk step rate	*/

/* Read / Write funny sectored disks	*/
#define	DCC_RDFPY	16		/* read any size sector off disk */
#define	DCC_WRFPY	17		/* write any size sector off disk */

/* caching bits	*/
#define	DCC_DIRREAD	18		/* Tell Z80 that a block is a 'dir' */

#define	DCC_SPECIAL	32		/* a Special write RAM command */

/* SCSI EXTRAS	*/
#define	DCC_STSTOP	35		/* SCSI start / stop	*/
#define	DCC_SFMT	36		/* SCSI format		*/
#define	DCC_SENSE	37		/* read sense data from scsi */
#define	DCC_SCSICMND	38		/* 1616 to send scsi command */
#define	DCC_STAT	39		/* returns stat and msg bytes */
#define	DCC_SET_ID	40		/* sets up dest. scsi ID No. */
#define	DCC_SETZ80_ID	41		/* sets up the Z80's ID	*/

/* Undefined at any one time DON'T use	*/
#define	DCC_TEMP	42		/* temp command for Z80 odds & ends */

/* Z80 SCC bits and pieces V3.x and above	*/
#define	DCC_ZISER	43		/* Z80 serial IN BYTE	*/
#define	DCC_ZISTAT	44		/* Z80 serial IN STAT	*/
#define	DCC_ZOSER	45		/* Z80 serial OUT BYTE	*/
#define	DCC_ZOSTAT	46		/* Z80 serial OUT STAT  */

#define	DCC_MULTI_IO	50		/* Multi io command	*/

#define DCC_NEWDISK	64		/* New disk detected	*/
#define DCC_RES0	65		/* Reset level 0 */
#define DCC_RES1	66		/* Reset level 1 */
#define DCC_RES2	67		/* Reset level 2 */
#define ERROR		-1
