/*	Macros for accessing variable arguments */

#ifndef	_STDARG

typedef void *	va_list1;

#if	i8051 && (MODEL_0 || MODEL_2)

#define	va_start(ap, parmn)	*ap = &parmn;
#define	va_arg(ap, type)	(*--(*(type **)ap))

#else

#define	va_start(ap, parmn)	*ap = (char *)&parmn + sizeof parmn
#define	va_arg(ap, type)	(*(*(type **)ap)++)

#endif

#define	va_end(ap)

#define	_STDARG

#endif	/* STDARG */

/*
 * AKPM: this enables some stdarg style declarations in stdio.h.
 * Do this because sometimes I want to disable stdarg.h
 * by defining _STDARG before including stdio.h.
 * See what happens when people nest include files?
 */

#define HITECH_STDARG
