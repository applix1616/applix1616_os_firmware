/*
 * Definitions for the 5380 chip:
 *
 * Needs SCSI_BASE in gpiomap.h
 */

#ifdef GPCPU
#define SCSI_BASE	0xffde01
#else
#define SCSI_BASE	0xffde00
#endif

#define CUR_DATA	((uchar *)(SCSI_BASE))		/* Read */
#define OP_DATA		CUR_DATA			/* Write */
#define INIT_CMD	((uchar *)(SCSI_BASE + 8))	/* R/W */
#define SCSI_MODE	((uchar *)(SCSI_BASE + 16))	/* R/W */
#define TARGET_CMD	((uchar *)(SCSI_BASE + 24))	/* R/W */
#define	CUR_BUS_STAT	((uchar *)(SCSI_BASE + 32))	/* Read */
#define SELECT_EN	CUR_BUS_STAT			/* Write */
#define BUS_AND_STAT	((uchar *)(SCSI_BASE + 40))	/* Read */
#define S_DMA_SEND	BUS_AND_STAT			/* Write */
#define IP_DATA		((uchar *)(SCSI_BASE + 48))	/* Read */
#define S_DMAT_RCV	IP_DATA				/* Write */
#define RES_PARINT	((uchar *)(SCSI_BASE + 56))	/* Read */
#define S_DMAI_RCV	RES_PARINT			/* Write */
/* Ports with auto wait state */
#define W_IP_DATA	((uchar *)(SCSI_BASE + 48 + 256))
#define W_OP_DATA	((uchar *)(SCSI_BASE + 256))
/* Ports with auto wait state, EOP asserted */
#define EOP_IP_DATA	((uchar *)(SCSI_BASE + 48 + 256 + 128))
#define EOP_OP_DATA	((uchar *)(SCSI_BASE + 256 + 128))

/* Initiator command register INIT_CMD (read) */
#define ASSERT_RST	0x80		/* Assert SCSI /RST */
#define AIP		0x40		/* Arbitration in progress */
#define LA		0x20		/* Lost arbitration */
#define	ASRT_ACK	0x10		/* Assert /ACK */
#define ASRT_BSY	0x08		/* Assert /BSY */
#define ASRT_SEL	0x04		/* Assert /SEL */
#define ASRT_ATN	0x02		/* Assert /ATN */
#define ASRT_DB		0x01		/* Assert data bus */

/* Mode register SCSI_MODE */
#define BLOCKMODE	0x80		/* Block mode DMA */
#define TARGETMODE	0x40		/* SCSI Target mode */
#define EN_PARITY	0x20		/* Enable parity */
#define EN_IPARITY	0x10		/* Enable parity interrupt */
#define EN_IEOP		0x08		/* Enable EOP interrupt */
#define MONITOR_BUSY	0x04		/* Enable interrupt on loss of /BSY */
#define DMA_MODE	0x02		/* Enable DMA mode */
#define ARBITRATE	0x01		/* Start arbitration process */

/* Target command register TARGET_CMD */
#define TC_DATAOUT	0x00		/* Data out phase */
#define TC_DATAIN	0x01		/* Data in */
#define TC_COMMAND	0x02		/* Command phase */
#define TC_STATUS	0x03		/* Status request */
#define TC_MSGOUT	0x06		/* Message out phase */
#define TC_MSGIN	0x07		/* Message in phase */

/* Current SCSI bus status CUR_BUS_STAT */
#define CB_RST		0x80
#define CB_BSY		0x40
#define CB_REQ		0x20
#define CB_MSG		0x10
#define CB_CD		0x08
#define CB_IO		0x04
#define CB_SEL		0x02
#define CB_DBP		0x01

/* Bus and status register BUS_AND_STAT */
#define BS_ENDDMA	0x80		/* End of DMA */
#define BS_DMAREQ	0x40		/* DRQ signal */
#define BS_PARERR	0x20		/* Parity error */
#define BS_IRQ		0x10		/* IRQ active */
#define BS_PHASEMATCH	0x08		/* Phase match! */
#define BS_BUSYERROR	0x04
#define BS_ATN		0x02
#define BS_ACK		0x01

/* General stuff */
#define SCSI_ID(n)	(0x01 << (n))
