/*
 * Structure returned by getpwent(), getpwuid() and getpwnam()
 */

#define PASSWD_FILE	"/etc/passwd"

struct passwd
{
	char *pw_name;		/* The login name */
	char *pw_passwd;	/* Encrypted password string (can be nil) */
	long pw_uid;		/* User ID */
	long pw_gid;		/* Group ID */
	char *pw_description;	/* Long description of user */
	char *pw_dir;		/* User's home directory */
	char *pw_shell;		/* Default startup program: if nil,
					run a shell */
};

extern struct passwd *getpwent(), *getpwnam(char *), *getpwuid(int);
extern loadpwuid(int uid, struct passwd *ppasswd);
extern loadpwlogname(char *logname, struct passwd *ppasswd);
extern lognametouid(char *logname);

