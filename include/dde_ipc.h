/* TABS4 */

/*
 * Definitions for sending IPC blocks to the DDE editor
 *
 * Create a struct dde_ipc, fill it in and fire it at DDE,
 * with BLOCKTX(dde_pid, &dde_ipc, sizeof(dde_ipc), 0);
 */

#define DDE_COMMAND			1
#define DDE_FILEANDLINE		2

typedef struct dde_ipc
{
	long mode;
	long lineno;
	char string256;
} dde_ipc;
