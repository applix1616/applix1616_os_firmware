/* TABS4 NONDOC
 * Character I/O stuff
 */

#ifndef CHARIO_H
#define CHARIO_H

#ifndef STDIN
#define STDERR		0x100	/* Standard error file descriptor */
#define STDOUT		0x101	/* Standard output file descriptor */
#define STDIN		0x102	/* Standard input file descriptor */
#endif

#ifndef NIODRIVERS
#define NIODRIVERS	16	/* 16 input & 16 output drivers */
#endif

#define DNAMESIZE	16	/* Max length of driver name */

/*
 * Under V4.2 the char device structures have changed radically.
 * The following struct is used for both input & output. The
 * ordering is chosem so that programs which directly write
 * to char device driver tables will still work if they
 * are patching the output driver, but not the input driver.
 */

struct chardriver
{
/*  0 */	int (*doop)();		/* Output routine							*/
/*  4 */	int (*opstatus)();	/* Output status routine					*/
/*  8 */	int oppassval;		/* Optional number to pass to O/P driver	*/
/* 12 */	char name[DNAMESIZE];/* Colon-terminated device name			*/
/* 28 */	char **lastlines;	/* Last line recall buffers					*/
/* 32 */	int (*doip)();		/* Input routine							*/
/* 36 */	int (*ipstatus)();	/* Input status routine						*/
/* 40 */	int ippassval;		/* Optional number to pass to I/P driver	*/
/* 44 */	int (*miscvec)();	/* Misc entry point							*/
/* 48 */	int sigintchar;		/* If received, send signal down			*/
/* 52 */	int eofchar;		/* End-of-file char							*/
/* 56 */	int xoffchar;		/* Xoff character							*/
/* 60 */	int xonchar;		/* Xon character							*/
/* 64 */	int resetchar;		/* Reset the system!						*/
/* 68 */	uint rxcount;		/* Number of chars which have come in		*/
/* 72 */	uint txcount;		/* Number which have gone out				*/
/* 76 */	ushort intsig;		/* If set, Interrupt signal pending			*/
/* 78 */	ushort hupsig;		/* If set, Hangup signal pending			*/
/* 80 */	ushort xoffed;		/* If set, awaiting XON: O/P driver sleeps	*/
/* 82 */	ushort kiluser;		/* If set, KILLUSER call pending			*/
/* 84 */	ushort hupmode;		/* DCD loss mode							*/
/* 86 */	ushort rawmode;		/* If set, ignore all special chars			*/
/* 88 */	int (*miorvec)();	/* Multi char read vector					*/
/* 92 */	int (*miowvec)();	/* Multi char write vector					*/
/* 96 */	ushort modebits;	/* Application specific						*/
/* 98 */	ushort statbits;	/* Permissions								*/
};

/*
 * Version 4.2: define character driver miscellaneous commands
 */

#define CDM_OPEN	0			/* Opened. Passed mode						*/
#define CDM_CLOSE	1			/* Closed									*/
#define CDM_SETSIGCHAR	2		/* Set the SIGINT char						*/
#define CDM_READSIGCHAR	3		/* Read the signal char						*/
#define CDM_SETEOFCHAR	4		/* Set the EOF char							*/
#define CDM_READEOFCHAR	5		/* Read the EOF char						*/
#define CDM_SETXOFFCHAR	8		/* Set the XOFF char						*/
#define CDM_READXOFFCHAR 9		/* Read the XOFF char						*/
#define CDM_SETXONCHAR	10		/* Set the XON char							*/
#define CDM_READXONCHAR 11		/* Read the XON char						*/
#define CDM_SETRESETCHAR 12		/* Set the reset character					*/
#define CDM_READRESETCHAR 13	/* Read the reset char						*/
#define CDM_SENDSIGINT	14		/* Send SIGINT to process					*/
#define CDM_SENDSIGHUP	15		/* Send SIGHUP to process					*/
#define CDM_KILLUSER	16		/* Kill all attached processes				*/
#define CDM_SETRAWMODE	17		/* Set/clear RAW mode						*/
#define CDM_READRAWMODE	18		/* Read the raw mode flag					*/
#define CDM_MIORVEC		19		/* Read multichar read entry point			*/
#define CDM_MIOWVEC		20		/* Read multichar write entry point			*/
#define CDM_SETHUPMODE	21		/* Set the hangup policy					*/
#define CDM_READHUPMODE	22		/* Read the hangup policy					*/
#define CDM_HASMISCVEC	23		/* Does driver support misc vector?			*/
#define CDM_SETMODEBITS	24		/* Set/clear/read modebits					*/
#define CDM_SETPERM		25		/* Set permissions							*/
#define CDM_READPERM	26		/* Read permissions							*/
#define CDM_RXTOTAL		27		/* Read/set receive char counter			*/
#define CDM_TXTOTAL		28		/* Read/set transmit char counter			*/

/* The following calls are specifically for the low level driver */
#define CDM_SETMODE		32		/* Set the baud rate, etc					*/
#define CDM_READMODE	33		/* Read the baud rate, etc					*/
#define CDM_SETDTR		34		/* Set/clear DTR							*/
#define CDM_SETRTS		35		/* Set/cler RTS								*/
#define CDM_READDCD		36		/* Return DCD state							*/
#define CDM_READCTS		37		/* Return CTS state							*/
#define CDM_READBREAK	38		/* Return BREAK state						*/
#define CDM_SETHFC		39		/* Set/clear H/W flow control				*/
#define CDM_SETBREAK	40		/* Start/stop break on Tx					*/
#define CDM_TXCOUNT		41		/* Number of chars still to Tx				*/
#define CDM_RXCOUNT		42		/* Number of chars in rx buffer				*/
#define CDM_TXROOM		43		/* Number of chars free in Tx				*/
#define CDM_RXROOM		44		/* Number of chars free in Rx				*/
#define CDM_TXFLUSH		45		/* Flush all output chars					*/
#define CDM_TXPURGE		46		/* Dump all buffered tx chars				*/
#define CDM_RXPURGE		47		/* Dump all buffered Rx chars				*/
#define CDM_RXPEEK		48		/* Have a peek at next Rx char				*/
#define CDM_SETTXBSIZE	49		/* Set Tx buffer size						*/
#define CDM_SETRXBSIZE	50		/* Set Rx buffer size						*/
#define CDM_READTXBSIZE	51		/* Read Tx buffer size						*/
#define CDM_READRXBSIZE	52		/* Read Rx buffer size						*/
#define CDM_VERSION		53		/* Return driver version number				*/
#define CDM_READHFC		54		/* Read hardware flow control state			*/
#define CDM_SETSFC		55		/* Set software flow control for input		*/
#define CDM_READSFC		56		/* Read software flow control for input		*/
#define CDM_ALTERNATE	57		/* Alternate behaviour						*/

/* Bits in modebits field */
#define CDMB_XLATEESC	1		/* Perform escape code translation			*/
#define CDMB_RXSIGPURGE	2		/* Purge rx buffer on signal recognition	*/
#define CDMB_TXSIGPURGE	4		/* Purge tx buffer on signal recognition	*/

#endif

