/* TABS8 NONDOC
 * Header file for GPCPU programming
 *
 * "%W%: Delta %U% %E%"
 */


#ifndef GPIOMAP_H
#define GPIOMAP_H


#define RAMSTART 0			/* Start of RAM                      */
#define RAMEND	0x40000			/* Maximum RAM address + 1           */
// #define ROMSTART 0x600000		/* Start of ROM                      */
#define LATCH	((uchar *)0x700000)	/* Configuration latch (R/W)         */
#define DISPLAYC ((ushort *)0x708000)	/* Display cursor data, low byte     */
#define DISPLAY ((ushort *)0x708020)	/* Display data, low byte (W/O)      */
// #define IPORT	((uchar *)0x710000)	/* Switch input port                 */
#define DUART ((struct duart *)0x718001)/* The on-board DUART                */
#define PIT    ((struct pit *)0x720001)	/* The on-board PI/T                 */

/* The DUARTs on the prototype I/O board*/
#define DUART1 ((struct duart *)0xfff801)
#define DUART2 ((struct duart *)0xfffa01)

/* The DUARTs on the real I/O board */
#define IODUART0BASE 0xffe401
#define IODUART1BASE 0xffe601
#define IODUART0 ((struct duart *)IODUART0BASE)
#define IODUART1 ((struct duart *)IODUART1BASE)

/* The PI/T on the I/O board */
#define IOPITBASE	0xffe801
#define IOPIT	((struct pit *)IOPITBASE)

/* The FDC base address */
#define IOFDCBASE	0xffe001

/* Define bits in the latch */
#define FORCEROM	0x80	/* Should always be high                     */
#define LED		0x40	/* Active low                                */
#define BL		0x20	/* Display blanking (active low)             */
#define CLR		0x10	/* Display CLR signal (active low)           */
#define CUE		0x08	/* Display cursor enable (active high)       */
#define RESWD		0x04	/* Rising edge holds off watchdog timer      */
#define MAP1		0x02	/* Memory map control bit #1 (unimplemented) */
#define MAP0		0x01	/* Memory map control bit #0 (unimplemented) */

struct  hardware
{
	int	fc_save;			/* Where the 68K function */
	int	acc_addr;			/* code, access address,  */
	int	ir_save;			/* etc. are saved when an */
	int	sr_save;			/* exception occurs.      */
	int	pc_save;
	int	excep_code;			/* Exception type */
	int	dregs8;			/* Data registers */
	int	aregs8;			/* Address registers */
	int	ramptr;				/* Pointer to bad memory */
	int	errcode;			/* Diagnostic error type */
	int	ramsize;			/* Number of bytes of RAM */
};

#define	HWMAP	((struct hardware *)0x300)	/* Defined above */

/* Define interrupt levels */
#define WDIRQSPL()	spl7()
#define EIRQ2SPL()	spl6()
#define EIRQ1SPL()	spl5()
#define DUART1SPL()	EIRQ1SPL()
#define DUART2SPL()	EIRQ1SPL()
#define DUARTSPL()	spl4()
#define TIMERSPL()	spl3()
#define PITSPL()	spl2()
#define EIRQ0SPL()	spl1()

#define IODUART0SPL()	EIRQ1SPL()
#define IODUART1SPL()	EIRQ1SPL()
#define IOPITSPL()	EIRQ1SPL()
#define IOTIMERSPL()	EIRQ2SPL()
#define IOFDCSPL()	EIRQ2SPL()
#define IOTIMERVEC	30		/* Level 6 autovector */
#define IOFDCVEC	IOTIMERVEC	/* Shared */

#ifndef CLOCKFREQ
#define CLOCKFREQ	10000000
#endif

#endif
