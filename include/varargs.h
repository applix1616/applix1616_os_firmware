/*ident	"@(#)cfront:incl/varargs.h	1.1" */
#if 0

typedef char *va_list;

#define va_dcl int va_alist;
#define va_start(list) list = (va_list) &va_alist
#define va_end(list)
#define va_arg(list, mode) ((mode *)(list += sizeof(mode)))-1

#ifdef NOTDEF
extern int vprintf(char*, va_list),
	vfprintf(FILE*, char*, va_list),
	vsprintf(char*, char*, va_list),
	setvbuf(FILE*, char*, int, size_t);
#endif
#define _STDARG 1
#endif
