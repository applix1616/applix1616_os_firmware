/* TABS8 NONDOC */
/*   Definition of the register layout of Motoroal 68230 PIT */


/*  @(#)pit.h	3.1: Delta 13:58:18 88/01/07	*/

/*  ********************************************************************
 *
 *	NOTE:   The device registers are on only half the bus,  so they
 *   appear to be two bytes apart in the address space.  This is the
 *   reason for the padding bytes.  This can be avoided using the special
 *   I/O command of the 68000,  but using C implies that this needs
 *   to be a function, so it is easier to set them up this way.
 */

struct  pit
{
	uchar	p_pgcr,	p_pad0;		/* Port General Control */
	uchar	p_psrr,	p_pad1;		/* Port service request */
	uchar	p_paddr,p_pad2;		/* Port A data direction */
	uchar	p_pbddr,p_pad3;		/* Port B data direction */
	uchar	p_pcddr,p_pad4;		/* Port C data direction */
	uchar	p_pivr,	p_pad5;		/* Port interrupt vector */
	uchar	p_pacr,	p_pad6;		/* Port A control */
	uchar	p_pbcr,	p_pad7;		/* Port B control */
	uchar	p_padr,	p_pad8;		/* Port A data */
	uchar	p_pbdr,	p_pad9;		/* Port B data */
	uchar	p_paar,	p_pada;		/* Port A pin values */
	uchar	p_pbar,	p_padb;		/* Port B pin values */
	uchar	p_pcdr,	p_padc;		/* Port C data */
	uchar	p_psr,	p_padd;		/* Port status */
	uchar	p_null0,p_pade;		/* Unused,  reads as zero */
	uchar	p_null1,p_padf;		/* Ditto */
	uchar	p_tcr,	p_padg;		/* Timer control */
	uchar	p_tivr,	p_padh;		/* Timer interrupt vector */
	uchar	p_cpr,  p_padi;		/* For use with movepoiwl() */
	uchar	p_cprh,	p_padj;		/* Counter pre-load, high byte */
	uchar	p_cprm,	p_padk;		/* Ditto, middle byte */
	uchar	p_cprl,	p_padl;		/* Low byte */
	uchar	p_cntr, p_padm;		/* For use with movep: 24 bits only */
	uchar	p_cntrh,p_padn;		/* Counter value; high byte */
	uchar	p_cntrm,p_pado;		/* Ditto, middle */
	uchar	p_cntrl,p_padp;		/* Low byte */
	uchar	p_tsr,	p_padq;		/* Timer status */
	uchar	p_null2,p_pads;
	uchar	p_null3,p_padt;
	uchar	p_null4,p_padu;
	uchar	p_null5,p_padv;
	uchar	p_null6,p_padw;
};

/*
 *	Magic bit definitions:  well, the ones I can understand!
 */

/*   General control register:  p_pgcr  */
#define	P_MODE0		0x00		/* Mode 0: unidirectional 8 bit */
#define	P_MODE1		0x40		/* Mode 1: unidirectional 16 bit */
#define	P_MODE2		0x80		/* Mode 2: bidirectional 8 bit */
#define	P_MODE3		0xc0		/* Mode 3: bidirectioanl 16 bit */

#define	P_H34OFF	0x00
#define	P_H34ON		0x20

#define	P_H12OFF	0x00
#define	P_H12ON		0x10

#define	P_H4TRUE	0x08		/* High voltage when asserted */
#define	P_H4INVTD	0x00		/* Low voltage when inserted */

#define	P_H3TRUE	0x04
#define	P_H3INVTD	0x00

#define	P_H2TRUE	0x02
#define	P_H2INVTD	0x00

#define	P_H1TRUE	0x01
#define	P_H1INVTD	0x00

/*   Port service request register:  p_psrr   */
#define	P_PC4NODMA	0x00		/* PC4 is PC4 with no DMA ops */
#define	P_PC4H1DMA	0x40		/* PC4 = DMAREQ, H1 control */
#define	P_PC4H3DMA	0x60		/* PC4 = DAMREQ, H3 control */

#define	P_PC56		0x00		/* PC5, PC6 are PC5, PC6 */
#define	P_PIRQ6		0x08		/* PIRQ, PC6, autovectored interrupts */
#define	P_PC5IACK	0x10		/* PC5, PIACK */
#define	P_PIRQIACK	0x18		/* PIRQ, PIACK, vectored interrupts */

#define	P_IP1234	0x00		/* High priority: H1S, H2S, H3S, H4S */
#define	P_IP2134	0x01		/* H2, H1, H3, H4 */
#define	P_IP1243	0x02		/* H1, H2, H4, H3 */
#define	P_IP2143	0x03		/* H2, H1, H4, H3 */
#define	P_IP3412	0x04		/* H3, H4, H1, H2 */
#define	P_IP3421	0x05		/* H3, H4, H2, H1 */
#define	P_IP4312	0x06		/* H4, H3, H1, H2 */
#define	P_IP4321	0x07		/* H4, H3, H2, H1 */

/*  Port status register:  p_psr  */
#define	P_H4LEVEL	0x80		/* Level on H4 pin */
#define	P_H3LEVEL	0x40		/* H3 voltage */
#define	P_H2LEVEL	0x20		/* H2 */
#define	P_H1LEVEL	0x10		/* H1 */

#define	P_H4STATUS	0x08		/* H4 status */
#define	P_H3STATUS	0x04		/* H3 */
#define	P_H2STATUS	0x02		/* H2 */
#define	P_H1STATUS	0x01		/* H1 */

/*   The individual port control registers:   p_pabcr  */
#define	P_PSMD00	0x00		/* Sub mode 00 */
#define	P_PSMD01	0x40		/* Sub mode 01 */
#define	P_PSMD10	0x80		/* Sub mode 10 */
#define	P_PSMD11	0xc0		/* Sub mode 11 */
#define	P_PSMD1X	0x80		/* Sub mode 1X */
#define	P_PSMDXX	0x00		/* Sub mode XX */
/*   H2 control mode:  seems to be miscellaneous odds & ends */
#define	P_H2IEDGE	0x00		/* H2 input, edge sensitive, H2S */
#define	P_H2ONEG	0x30		/* H2 output, negated */
#define	P_H2OASS	0x38		/* H2 output, asserted */
/*   Some mode 2 bits & pieces  */
#define	P_H2OLOCK	0x00		/* Interlocked H2/H1 handshake */
#define	P_H2OPULSE	0x08		/* H2 pulsed output */

#define	P_H2IENABLE	0x04		/* H2 interrupt enable */

#define	P_H1IENABLE	0x02		/* H1 interrupt enable */

#define	P_H3CONTROL	0x01		/* H3 control, mode dependent */

/*  TIMER BITS & PIECES  */
#define	P_PRESC		32		/* Prescaler division factor */

/*  Timer control register:  p_tcr  */
/*  First group control Port C pins 3,7 */
#define	P_PC3PC7	0x00		/* Port C pins 3 & 7 are PC3, PC7 */
#define	P_TOUTPC7	0x40		/* Port C pin 3 is TOUT, PC7 */
#define	P_TOAKNOINT	0x80		/* TOUT & TIACK, interrupts off */
#define	P_TOACKVEC	0xa0		/* TOUT & TIACK, vectored interrupts */
#define	P_TOC7NOINT	0xc0		/* TOUT, PC7, no interrupts */
#define	P_TOC7AVEC	0xe0		/* TOUT, PC7, autovectored interrupts */

/*  Select counter rollover or preload from preload register */
#define	P_CNTLOAD	0x00		/* Re-load counter from pre-load reg */
#define	P_CNTROLL	0x10		/* Counter rolls over to 0xffffff */

/*  Select clock source and prescaler control */
#define	P_PC2PRESC	0x00		/* PC2, CLK as input, prescaler */
#define	P_TGATEPRESC	0x02		/* PC2 gates prescaler, CLK input */
#define	P_TINPRESC	0x04		/* PC2 is TIN input, prescaler in */
#define	P_TINNOPRESC	0x06		/* PC2 is TIN input, no prescaler */

#define	P_TIMERGO	0x01		/* Timer enabled */
#define	P_TIMERSTOP	0x00		/* Timer disabled */


/*   Timer status register:  p_tsr  */
#define	P_TZDS		0x01		/* Zero detect indicator & reset */
