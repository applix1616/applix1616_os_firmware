/*
 * Constants & structures for serial I/O
 * Must include storedef.h and types.h first.
 */

/* Structure used for programming a channel */
struct scc_prog
{
	ushort brate;		/* Baud rate in bits/sec */
	ushort rxbits;		/* Rx word length (5/6/7/8) */
	ushort txbits;		/* Tx word length (5/6/7/8) */
	ushort parity;		/* 0 = none, 1 = odd, 2 = even */
	ushort stopbits;	/* 0 = 1, 1 = 1.5, 2 = 2 */
};

/* Offsets from start of SCC channel */
#define SCC_CONT	0		/* Control offset */
#define	SCC_DATA	2		/* Data offset */

/* Read register 0 stuff */
#define SCC_RXBREAK	0x80		/* Break condition */
#define SCC_CTS		0x20		/* CTS mask */
#define SCC_DCD		0x08		/* DCD mask */
#define SCC_TXBE	0x04		/* Tx buffer empty */
#define SCC_RXCA	0x01		/* Rx char available */

/* Write register 3 */
#define SCC_AUTOENABLES	0x20		/* Auto DCD/CTS handshake */

/* Write register 5 */
#define SCC_DTR		0x80		/* DTR mask */
#define SCC_RTS		0x02		/* RTS mask */
#define SCC_TXBREAK	0x10		/* Send BREAK */

/*
 * The following structure is used for controlling SCCs.
 */

struct scc
{
	uint wr5image;			/* Copy of WR5 contents		*/
	uchar *data;			/* Data register		*/
	uchar *cont;			/* Command register		*/
	uint rxmask;			/* Receive mask			*/
	struct scc_prog curprog;	/* Current baud rate, etc	*/
	struct circ_buf *txcbuf,	/* Transmit circular buffer	*/
			*rxcbuf;	/* Receive circular buffer	*/
	struct chardriver *chardriver;	/* Corresponding chardevice fd	*/
	ushort cdindex;			/* char driver index (device fd)*/
	ushort dohfc;			/* Doing h/w flow control	*/
	ushort wr3image;		/* Copy of wr3 contents		*/
	ushort splmask;			/* Interrupt level for this scc	*/
/* 1616/OS V4.3a */
	ushort wr15image;		/* For ext/stat interrupt mode	*/
/* 1616/OS V4.7 */
	uchar dosfc;			/* Doing software flow control */
	uchar needs_xon;		/* Flag: ISR must transmit XON */
	uchar needs_xoff;		/* Flag: ISR must transmit XOFF */
	uchar sent_xoff;		/* Flag: XOFF has been sent */
	uchar xonchar, xoffchar;
};

/* The number of SCC channels supported by the ROMs: 4 chips total */
#define NSCCCHANS	8
