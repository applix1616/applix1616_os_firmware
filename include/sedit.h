/*
 * Header file for screen editor
 */

#define MEM_MAX		30000	/* Maximum amount of memory which the editor attempts to use */
#define IBSIZE		50	/* Insert buffer size */

#ifdef GPCPU
#define NROWS	24
#endif
#ifndef GPCPU
#define NROWS	25
#endif
#define NCOLS	80

#define SCR_TOP	1		/* First displayed line */
#define	STATUS	0		/* Status line */
#define TGETPOS 5		/* X position for getting input line */
#define SEDLINESIZE	100	/* Max line we can input */

#ifdef PUTCHAR
#define opchar(c)	PUTCHAR(c)
#else
#define opchar(c)	putchar(c)
#endif

#ifndef GPCPU
#define CLREOL		"\033T"
#define DELLINE		"\033R"
#define CLRSCRN		"\033*"
#define INSLIN		"\033E"
#define MVCURS1		'\033'
#define MVCURS2		'='
#define POSITION	"\033=%c%c"
#define CLEAREOS	"\033Y"
#define HILITEON	"\033)"
#define HILITEOFF	"\033("
#define CLREOS		"\033Y"
#endif

#ifdef GPCPU
#define CLREOL	"\0330K"
#define DELLINE	"\033M"
#define CLRSCRN	"\0331;1H\0332J"
#define INSLIN	"\033L"
#endif

#ifndef CTL
#define CTL(c)	((c) - 0x40)
#endif

#define MASK(x) (x)
#define INDEX index
#define STRNCMP strncmp

