/* The structure associated with a user process */

struct user
{
	ushort nusers;		/* Number of processes sharing this structure */
	uchar *ibcvec;		/* Inbuilt command disable vector */

/* Video driver externals */
ushort
	mode_640,		/* If true, in 640 mode */
	curs_rate,		/* Flash speed */
	curs_enable,		/* Turn cursor on/off */
	thisfg,
	thisbg,
	cur_vap,		/* The current access page */
	cur_vdp,		/* The current display page */
	vstate,			/* Terminal emulation state */
	esc1save,		/* Save for second char in ESC sequence */
	esc2save,		/* Save for third char in ESC sequence */
	blkcurs,		/* Block cursor flag */
	waswrap,		/* Flag for suppressing newline after wrap */
	insertmode,		/* Character insert mode */
/* A copy of the current window definitions */
	x_start,
	y_start,
	x_end,
	y_end,
	bg_col,
	fg_col,
	curs_x,
	curs_y,
	ourcurs,		/* Hardware cursor follows output cursor */
	x_size,
	vidomode,		/* If true, no esc or control processing is done */
	y_size,
	curpmode,		/* Current graphics plot mode */
	gfgcol,			/* Graphics foreground colour */
	ogfgcol;		/* User's foreground colour */

	int (*plotfunc)();	/* Pointer to our current graphics dot function */
	uint underline;		/* Current underline mask */
	uint g_xsize, g_ysize;
	int g_xstart, g_ystart;
	struct window *cur_window;/* Pointer to current window structure */
	ushort *vidram;		/* Pointer to start of video access RAM */
	ushort *chtab;
	ushort curs_count;	/* Flash counter */
	ushort curs_mask8;	/* The flashing mask */
	long pad;
};
