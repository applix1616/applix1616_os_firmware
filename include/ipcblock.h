/*
 * Define structures and constants for the inter-process block
 * transmit and receive mechanisms
 */

/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */

/*
 * A linked list of the following structures hangs off the receiving
 * process's process table entry, in order of receipt.
 */

typedef struct ipcblock
{
	struct ipcblock *next;	/* Link to next structure */
	long whofrom;		/* The sender PID */
	long blocklen;		/* Size of data block */
	long padd6;		/* Expansion */
	unsigned char data[2];	/* The data is actually placed here
					(overruns the array: varaible sized
					structure kludge */
} IPCBLOCK;

/*
 * Define the modes in the BLOCKRX() system call
 */

#define IPCB_GETHEAD	0	/* Return pointer to head of list */
#define IPCB_BLOCKCOUNT	1	/* How many blocks are queued? */
#define IPCB_BYTECOUNT	2	/* How many bytes are queued? */
#define IPCB_FETCHNEXT	3	/* Return next block, return 0 if none */
#define IPCB_FETCHWAIT	4	/* Get next block, sleep until one available */

