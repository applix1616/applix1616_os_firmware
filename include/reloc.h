/*
 * The relocatable file format and memory resident driver definitions
 */

/*
 * The file format consists of a header, the code (executable at zero), then
 * the relocation info.
 */

struct rel_header {
	ushort magic1;		/* Magic number */
	int textlen;		/* Text length */
	int datalen;		/* Data length */
	int bsslen;		/* BSS length */
	int symtablen;		/* Symbol table length */
	int stackspace;		/* Unused */
	int text_begin;		/* Start address */
	ushort reloc_flag;	/* Relocation bits follow */
	};

#define RELMAG1		0x601a		/* Some funny number */
#define RELMAG2		0xd80ab7f1	/* Another one */
#define RELVERS		2		/* Relocation format version number */
#define MAGICDIFF	0x00020002	/* ORG difference for reloc generator */

/*
 * The memory resident driver format definition
 */

struct mrdriver {
	int magic1;
	int vers;		/* Format version number */
	int rdsize;		/* RAM disk size in k */
	int memusage;		/* Total storage for code & data in all drivers */
	int ndrivers;		/* Number of drivers */
	int magic2;		/* Second magic number */
	int stackspace;		/* System stack space */
	int vramspace;		/* Number of bytes to reserve for video RAM */
	int obramstart;		/* Start of on-board RAM */
	int mrdstart;		/* Start of memory-resident drivers */
	int rdstart;		/* Start of RAM disk */
	int bitmaps;		/* Start of bitmaps */
	int vramstart;		/* Start of video RAM */
/* Version 3.1d addition */
	int colours;		/* Screen colours */
/* Version 4.2a additions */
	short nlastlines;	/* Last line recall depth */
	short ncacheblocks;
	ushort maxfiles;	/* Max. number of  file control blocks */
	ushort ndcentries;	/* Number of directory cache entries */
	long chardrivers;	/* Pointer to the 16 chardriver structs */
	short padd28;		/* Future expansion */
	};

#define MRDVERS		2	/* Memory resident driver format version number */
#define STACKSPACE	0x10000	/* Default free stack space */
#ifdef A1616
#define RDSIZE		200	/* Default RAM disk size */
#define VRAMSPACE	0x8000	/* Default free video RAM space */
#endif /*A1616*/

#ifdef GPCPU
#define RDSIZE		200	/* Default RAM disk size */
#define VRAMSPACE	0x0	/* Default free video RAM space */
#endif

#ifdef A1616
/* Number of blocks needed to buffer bitmap for hard disk partition 0 */
#define HD0SIZE		5	/* 40 Mbyte max */
/* Number of blocks needed to buffer bitmap for hard disk partition 1 */
#define HD1SIZE		2
#endif /*A1616*/

#ifdef GPCPU
#define HD0SIZE		0
#define HD1SIZE		0
#endif

/* Size of bitmap buffers */
#define BITMAPROOM	(3 + HD0SIZE + HD1SIZE) * BLOCKSIZE

/*
 * MRD commands
 */

#define MRD_0RES	0	/* Level 0 reset */
#define MRD_1RES	1	/* Level 1 reset */
#define MRD_2RES	2	/* Level 2 reset */
#define MRD_NAME	3	/* Return pointer to driver name */
#define MRD_VERS	4	/* Return driver version */
#define MRD_ENABLE	5	/* Enable whatever it does */
#define MRD_DISABLE	6	/* Disable whatever it does */
#define MRD_DOIT	7	/* Do whatever it does */
#define MRD_STOPIT	8	/* Stop whatever it is */
#define MRD_EXECENTRY	9	/* Return exec mode entry point */
/* V4.2d addition */
#define MRD_SCHEDULER	10	/* Scheduler has started at boot time */

