/*----------------------------------------------------------------------------+
|                                                                             |
|       Graphics Header File for HiTech C.                                    |
|                                                                             |
|       Written by: Conal Walsh.                                              |
|                                                                             |
+----------------------------------------------------------------------------*/

/* Miscellaneous: */

#define QUERY    -1
#define QUERYL   -1L
#define FONT8x8  0xFFFL
#define FONT8x14 0xFFEL
#define FONT8x16 0xFFDL

/* Patterns: */

#define ALLBLACK     0L
#define ALLWHITE     1L
#define LIGHTGRAY    2L
#define GRAY         3L
#define DARKGRAY     4L
#define DOTS1        5L
#define DOTS2        6L
#define DOTS3        7L
#define DIAGUP1      8L
#define DIAGUP2      9L
#define DIAGUP3      10L
#define DIAGUP4      11L
#define DIAGDOWN1    12L
#define DIAGDOWN2    13L
#define DIAGDOWN3    14L
#define DIAGDOWN4    15L
#define DASHUP       16L
#define DASHDOWN     17L
#define DASHHORZ     18L
#define LINESHORZ    19L
#define DASHVERT     20L
#define LINESVERT    21L
#define BIGBOX       22L
#define SMALLBOX     23L
#define DIAMONDS1    24L
#define DIAMONDS2    25L
#define BRICKSHORZ   26L
#define WEAVE        27L
#define SCALES       28L
#define ALTDASH      29L
#define BRICKSDIAG   30L
#define TRIANGLES    31L

/* Colors: */

#define BLACK        0
#define BLUE         2
#define GREEN        4
#define CYAN         6
#define RED          8
#define MAGENTA      10
#define BROWN        12
#define WHITE        14
#define DKGRAY       1
#define LTBLUE       3
#define LTGREEN      5
#define LTCYAN       7
#define LTRED        9
#define LTMAGENTA    11
#define YELLOW       13
#define INTWHITE     15

/* Logical Colors: */

#define LCNORM       0x0100
#define LCNORMHI     0x0101
#define LCHELP       0x0102
#define LCHELPHI     0x0103
#define LCERROR      0x0104
#define LCERRORHI    0x0105
#define LCNORMEMPH   0x0106
#define LCMARK       0x0107
#define LCRNORM      0x0108
#define LCRNORMHI    0x0109
#define LCRHELP      0x010A
#define LCRHELPHI    0x010B
#define LCRERROR     0x010C
#define LCRERRORHI   0x010D
#define LCRNORMEMPH  0x010E
#define LCRMARK      0x010F

/* Drawing Modes: */

#define SRC          0
#define SRCANDDST    1
#define SRCORDST     2
#define SRCXORDST    3
#define NOTSRC       4
#define NOTSRCANDDST 5
#define NOTSRCORDST  6
#define NOTSRCXORDST 7
#define FOREBACK     8
#define BACKFORE     9
#define FORESAME     10
#define BACKSAME     11
#define SAMEFORE     12
#define SAMEBACK     13
#define FOREFORE     14
#define BACKBACK     15
#define SRCMASK      16

/* Text Styles: */

#define BOLD         0x01
#define ITALIC       0x02
#define OBSCURE      0x04
#define INVERSE      0x08
#define UNDERLINE    0x10
#define COLUMN       0x20

/* Video Planes: */

#define BPLANE       2
#define GPLANE       4
#define RPLANE       8
#define IPLANE       1
#define ALLPLANES    15

/* Pels/Inch for Different Video Modes: */

#define PPIX0        33
#define PPIY0        29
#define PPIX1        65
#define PPIY1        29
#define PPIXC        30         /* MultiSync */
#define PPIYC        45
#define PPIX10       60
#define PPIY10       45

/* Mouse Cursors: */

#define ARROW        0L
#define ARROWUP      1L
#define ARROWLEFT    2L
#define CHECKMARK    3L
#define HAND         4L
#define CROSSX       5L
#define CROSSPLUS    6L
#define HOURGLASS    7L

/* Window Types: */

#define TEXTW        0
#define GRAPW        1

/* Real Screen Window Handles: */

#define FULLSAVE     0
#define FILLSAVE     1
#define NOSAVE       2

/* Graphic Descriptor: */

typedef struct
{
   short  width;        /* Width of Graphic Object (in pels) */
   short  height;       /* Height of Graphic Object (in pels) */
   short  widthr;       /* Width Remaining in Graphic Object (in pels) */
   short  heightr;      /* Height Remaining in Graphic Object (in pels) */
   short  linelen;      /* Graphics Object Line Length (in bytes) */
   char  *origin;       /* Pointer to Origin in Graphic Object Buffer */
   char  *buffer;       /* Pointer to Start of Graphic Object Buffer */
   short  leftbit;      /* Starting Bit Number Within Byte (0 = high bit) */
   short  fontheight;   /* Font Height (in pels) */
} SSEGGD;

#ifndef __NOPROTOTYPES__

/* Function Prototypes: */

void     tprintc    ( char );		
void     tprint     ( char * );
void     tprintl    ( char *, short );
void     tprintd    ( char, short );
void     tprintf    ( char *, ... );
void     tprintfp   ( char ** );
void     teol       ( void );
void     tclear     ( void );
void     tborder    ( short );
void     tscrollv   ( short );
short    tcursor    ( short );
void     tpane      ( short, short, short, short );
void     tat        ( short, short );
short    tcolor     ( short );
short    tstyle     ( short );
short    tmode      ( short );
short    trow       ( void );
short    tcol       ( void );
short    trows      ( void );
short    tcols      ( void );
short    tprow0     ( void );
short    tpcol0     ( void );
short    tprows     ( void );
short    tpcols     ( void );
short    gprintc    ( char );
short    gprint     ( char * );
short    gprintl    ( char *, short );
short    gprintd    ( char, short );
void     gprintf    ( char *, ... );
void     gprintfp   ( char ** );
short    gcolor     ( short, short, short );
void     gline      ( short, short, short, short );
/* Draw an arc */
void     garc       ( short, short, short, short, short, short );
void     gfill      ( short, short, short, short );
void     gget       ( short, short, short, short );
void     gset       ( short, short, short, short );
void     gmap       ( short, short, short, short, short );
void     gpaint     ( short, short );
void     gpaintb    ( short, short, short );
short    gscreen    ( short );
void     gpane      ( short, short, short, short );
void     gclip      ( short, short, short, short );
/* Move graphics text cursor */
void     gat        ( short, short );
long     guse       ( );
void     gpoint     ( short, short );
short    gmode      ( short );
short    gstyle     ( unsigned short );
short    gpalette   ( short, short );
short    gmask      ( short );
short    gdisplay   ( short );
short    grow       ( void );
short    gcol       ( void );
short    grows      ( void );
short    gcols      ( void );
short    gprow0     ( void );
short    gpcol0     ( void );
short    gprows     ( void );
short    gpcols     ( void );
short    gcrow0     ( void );
/* Query left coord of current clipping rectangle */
short    gccol0     ( void );
short    gcrows     ( void );
short    gccols     ( void );
short    gtrows     ( void );
short    gtcols     ( void );
long     gsize      ( short, short );
SSEGGD  *gmake      ( short, short, SSEGGD * );
SSEGGD  *gmakex     ( short, short, SSEGGD *, void * );
short    gsin       ( short, short );
short    gcos       ( short, short );
short    mmode      ( short, short );
void     mpane      ( short, short, short, short );
void     mat        ( short, short );
short    mshow      ( void );
short    mhide      ( void );
short    mcolor     ( short );
long     muse       ( );
void     mpoint     ( short, short );
short    mmrow      ( short );
short    mmcol      ( short );
short    mspeed     ( short );
short    mcheck     ( void );
short    mbutton    ( void );
short    mclick     ( void );
short    mrow       ( void );
short    mcol       ( void );
short    mprow0     ( void );
short    mpcol0     ( void );
short    mprows     ( void );
short    mpcols     ( void );
void     trestore   ( void );
void     tsave      ( void );
short    tread      ( short, short );
short    wmake      ( short, short, short );
short    wuse       ( short );
short    wdrop      ( void );
void     wmove      ( short, short );
void     whide      ( void );
void     wshow      ( void );
short    wtype      ( void );
short    twrow0     ( void );
short    twcol0     ( void );
short    twrows     ( void );
short    twcols     ( void );
short    gwrow0     ( void );
short    gwcol0     ( void );
short    gwrows     ( void );
short    gwcols     ( void );

#endif __NOPROTOTYPES__

#define  FindSSEG() (gcos(0,0)==0)

/* AKPM additions */
#define S_MOFF(func)	mhide(), (func); mshow()

