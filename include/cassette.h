/*
 * Definitions which are needed for cassette I/O
 */

/* Main leader length (at 310 bytes/sec) */
#define MAINLEADER 900

/* Interblock leader */
#define SUBLEADER 600

/* Number of bytes to lock onto main leader */
#define MAINLOCK 300

/* Number of bytes to lock onto interblock leader */
#define SUBLOCK 200

/* The maximum amount of memory which may be transferred. */
#define MAXCASHUNK		8192


