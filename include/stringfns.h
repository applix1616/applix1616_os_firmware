/* TABS4 NONDOC */

/*
 * Prorotypes and definitions for the string library
 */

#define BAD_POINTER	((void *)0xaaaaaaaa)	/* Gets a bus error */

/*  basename.c  */
char 	*baseName(register char *str);

/*  basenamestr.c  */
char	*baseNameStr(register char *str, char *match);

/*  basenamestrLUT.c  */
char	*baseNameStrLUT(register char *str, char *LUT);

/*  charmatch.c  */
int	charMatch(char *str, char *match);

/*  charmatchLUT.c  */
int	charMatchLUT(register char *str, register char *LUT);

/*  extent.c  */
char	*extent(char *str);

/*  extentstr.c  */
char	*extentStr(char *str, char *match);

/*  extentstrLUT.c  */
char	*extentStrLUT(char *str, char *LUT);

/*  leftstr.c  */
char	*leftStr(register char *str, int nchars);

/*  malloc.c  */
void *Malloc(long nbytes);
void *Realloc(void *addr, long nbytes);
void Free(void *addr);

/*  midstr.c  */
char	*midStr(register char *str, int offset, int nchars);

/*  preplut.c  */
void	prepLUT(register char *str, register char *LUT);

/*  rightstr.c  */
char	*rightStr(char *str, int nchars);

/*  splitstring.c  */
int	splitString(char *str, char ***retarray, char *match);

/*  splitstringLUT.c  */
int	splitStringLUT(char *str, char ***retarray, register char *LUT);

/*  stolower.c  */
char	*sToLower(char *str);

/*  stolowercopy.c  */
char	*sToLowerCopy(char *str);

/*  stoupper.c  */
char	*sToUpper(char *str);

/*  stouppercopy.c  */
char	*sToUpperCopy(char *str);

/* strdup.c */
char	*strDup(char *str);

/*  strfree.c  */
void	strFree(char *str);

/*  strmalloc.c  */
char	*strMalloc(int len);

/*  strpfree.c  */
void	strPFree(char **vector);

/*  strpmalloc.c  */
char	**strPMalloc(int npointers);

/*	strsum.c */
char	*strSum(char *str1, char *str2);

/*  substrpos.c  */
int	subStrPos(char *str, int offset, register char *match);

/*  vecfree.c  */
void	vecFree(char **vector);
