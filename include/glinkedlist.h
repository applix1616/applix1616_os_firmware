/* TABS4 NONDOC */

/*
 * Definitions for generic linked list handler
 *
 * You can define the type of object which is to be managed
 * by #defining USERTYPE to it before including this file
 */

#include <stdarg.h>

#ifdef USERTYPE
#define GLL_TYPE USERTYPE
#else
#define GLL_TYPE void
#endif

typedef struct gLLElem
{
	GLL_TYPE *data;			/* User's data */
	struct gLLElem *next;
} gLLElem;

typedef struct gLList
{
	gLLElem *head, *tail;
} gLList;

/* Include the lower level linkedlist definitions */
#define MYSTRUCT GLL_TYPE
#include <linkedlist.h>

/* Utility functions */

/* gllcreat.c */
gLList *gLLCreat(void);

/* gllmisc.c */
gLLElem *gLLGetNext(gLLElem *p);
void gLLSetNext(gLLElem *s, gLLElem *next);

/* gllsort.c */
void gLLSort(gLList *list, int (*compare)(void *, void *));

/* gllreverse.c */
void gLLReverse(gLList *list);

/* gllapply.c */
void gLLApply(gLList *list, void (*func)(), va_list);

/* gllappend.c */
void gLLAppend(gLList *list, void *member);

/* glladvance.c */
void *gLLAdvance(gLList *list, gLLElem **position);

/* gllprepend.c */
void gLLPrepend(gLList *list, void *member);

/* gllcount.c */
int gLLCount(gLList *list);

/* gllinsert.c */
void gLLInsert(gLList *list, gLLElem **position, void *member);

/* glldelete.c */
void *gLLDelete(gLList *list, gLLElem **pmember, void (*destroy)(void *));

/* glluniq.c */
void gLLUniq(gLList *list, int (*compare)(void *, void *), void (*destroy)(void *), int already_sorted);

/* glldestroy.c */
void gLLDestroy(gLList *list, void (*destroy)(void *));

