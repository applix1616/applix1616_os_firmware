/*	TABS4
 * The structure which is dumped in memory at 0x8000 when
 * an exception occurs
 */

struct exception_data
{
	long magic;				/* Validates the following */
	short boot_reason;		/* Why system was reset */
	long dregs[8];
	long aregs[8];
	long accaddr;			/* Access address */
	long fc;				/* Function code */
	long ir;				/* Instruction register */
	long sr;				/* Status register */
	long pc;				/* Program counter */
	char *message;			/* Pointer to string explanation */
	long lastsyscall;		/* Last system call executed */
	long lastload;			/* Last program load address */
	char stacktrace[256];	/* 256 bytes above SP */
	char processname[32];	/* Name of offending process */
	char lastexec[128];		/* Last command executed */
};

/* The dump */
#define EXCEPTION_DATA	((struct exception_data *)0x8000)

/* Magic pattern */
#define RESET_MAGIC	((long)0x34e7a09f)

/* Reasons for booting */
#define BR_SOFTRESET		1	/* ALT-^R */
#define BR_STACKOVERRUN		2	/* Serious process stack overrun */
#define BR_EXCEPTION		3	/* Bus error or whatever */

