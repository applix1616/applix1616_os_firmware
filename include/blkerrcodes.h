/*
 * Error codes for block devices / files
 */

#define BEC_UNKNOWN	-1	/* General error code */

/* Block error codes */
#define BEC_WRTPROT	-2	/* Device write protected */
#define BEC_NOBDRIVER	-3	/* Invalid block driver requested */
#define BEC_NBDVRSPACE	-4	/* Cannot allocate storage for new block driver */
#define BEC_IOERROR	-5	/* Hardware error */
#define BEC_BADBLOCK	-6	/* Invalid block requested */
#define BEC_DISKFULL	-7	/* No unallocated blocks */
#define BEC_BADFD	-8	/* Invalid FCB number */

/* File error codes */
#define BEC_FNBAD	-9	/* Bad file name */
#define BEC_FFULL	-10	/* File is full (128k) */
#define BEC_FNOPEN	-11	/* File not open */
#define BEC_FNROPEN	-12	/* File not open for reading */
#define BEC_FNWOPEN	-13	/* File not open for reading */
#define BEC_FROPEN	-14	/* File open for reading */
#define BEC_FWOPEN	-15	/* File currently open for writing */
#define BEC_NOFCBS	-16	/* No FCBs left */
#define BEC_NOFILE	-17	/* File does not exist */
#define BEC_RPASTEOF	-18	/* Read past EOF */
#define BEC_DUPFNAME	-19	/* Renamed existing file */
#define BEC_BADARG	-20	/* Bad argument to file system call */
#define BEC_SPASTEOF	-21	/* Seek past EOF */
/*#define BEC_FWRONLY	-22*/	/* File is write-only (cannot seek) */
#define BEC_FOPEN	-23	/* File is currently open */
#define BEC_NOBUF	-24	/* No memory buffers for file */
#define BEC_ISDIR	-25	/* File is a directory */
#define BEC_NOTDIR	-26	/* File is not a directory */
#define BEC_DIRNEMPTY	-27	/* Directory not empty */
#define BEC_DIRFULL	-28	/* Directory is full */
#define BEC_LOCKED	-29	/* File is locked */
#define BEC_BADMAGIC	-30	/* Bad magic detected in a file */
#define BEC_USERINT	-31	/* User interrupt killed program */
#define BEC_NOPERM	-32	/* No permission for file access */
#define BEC_EXECDEPTH	-33	/* exec level too deep */
#define BEC_NOPID	-34	/* Too many processes */
#define BEC_KILLED	-35	/* Exitted due to kill */
#define BEC_PIPECLOSED	-36	/* Read/write pipe with other end closed */
#define BEC_BADPID	-37	/* Invalid PID passed to system call */
#define BEC_EXISTS	-38	/* File/directory already exists */
