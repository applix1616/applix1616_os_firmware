/*
 * Structure and storage for date and time
 */

/* Indexes into the datetime array */
#define YEAR	0
#define MONTH	1
#define DAY	2
#define HOUR	3
#define MINUTE	4
#define SECOND	5
#define TENTHS	6

/* The time is incremented by the following number of usecs at each interrupt */
#define TIMEINC	19968

#define DT_FORMATSTRING	"%02d:%02d:%02d %02d %s 19%02d"

#define MONTHNAMES "???JanFebMarAprMayJunJulAugSepOctNovDec"

#ifdef WANTMONTHNAMES
static char *monthname = { "???", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
#endif
