/* TABS4 NONDOC
 * Stuff for my curses emulation library
 */

#ifndef FILE
#include <stdio.h>
#endif

extern int LINES, COLS;

#define stdscr	0

#ifndef TRUE
#define TRUE 		1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef CTL
#	define CTL(c) 		((c) - 0x40)
#endif

#define KEY_NPAGE	CTL('C')
#define KEY_PPAGE	CTL('R')
#define KEY_UP		CTL('E')
#define KEY_DOWN	CTL('X')
#define KEY_LEFT	CTL('S')
#define KEY_RIGHT	CTL('D')
#define KEY_F(n)	(n+256)

#define A_STANDOUT	1
#define A_REVERSE	A_STANDOUT

#define getyx(scr, y, x)			y=Gety(scr), x=Getx(scr)
#define move(line, col)				wmove(stdscr, line, col)
#define addstr(s)					waddstr(stdscr, s)
#define getstr(s)					wgetstr(stdscr, s);
#define clrtoeol()					wclrtoeol(stdscr)
#define mvaddstr(line, col, str)	mvwaddstr(stdscr, line, col, str)
#define standout()					wattrset(stdscr, 1)
#define standend()					wattrset(stdscr, 0)
#define clrtobot()					wclrtobot(stdscr)
#define wrefresh(scr)				refresh()
#define mvaddch(line, col, c)		move(line, col), addch(c)
#define addch(c)					waddch(stdscr, c)

/* Prototypes */
/* curs1.c */
void						initscr(void);
void						buffer_stdout(void);
void						erase(void);
void						wmove(int scr, int line, int col);
void						waddch(int scr, int c);
int							wgetch(void);
void						waddstr(int scr, char *s);
char *						wgetstr(int scr, char *resp);
void						printw();
void						mvprintw();
void						wprintw();
int							mvwgetch(int scr, int line, int col);
void						wattrset(int scr, int mode);
void						wclrtoeol(int scr);
void						mvwaddstr(int scr, int line, int col, char *s);
void						wclrtobot(int scr);
void						mvwprintw();

/* curs2.c */
void						clear(void);
void						mvcur(int dunno1, int dunno2, int line, int col);
int							Gety(int scr);
int							Getx(int scr);

/* getch.c */
int							getch(void);
int							getche(void);
void						ungetch(int c);
void						putch(int c);
int							kbhit(void);

/* refresh.c */
void						refresh(void);
void						preprefresh(void);
void						insertln(void);
void						deleteln(void);

/* stubs.c */
void						cbreak(void);
void						echo(void);
void						raw(void);
void						noraw(void);
void						noecho(void);
void						crmode(void);
void						keypad(void);
void						endwin(void);
void						savetty(void);
void						resetty(void);
void						nocrmode(void);

/* unix/rawgetchar.c */
int							rawgetchar(void);
