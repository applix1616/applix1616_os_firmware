/* TABS4 NONDOC */

/*
 * Definitions for generic doubly linked list handler
 *
 * You can define the type of object which is to be managed
 * by #defining USERTYPE to it before including this file
 */

#ifdef USERTYPE
#define GLL_TYPE USERTYPE
#else
#define GLL_TYPE void
#endif

#include <stdarg.h>
#ifndef STDARG_H_INCLUDED
#define STDARG_H_INCLUDED
#endif

typedef struct gDLLElem
{
	GLL_TYPE *data;			/* User's data */
	struct gDLLElem *next;
	struct gDLLElem *prev;
} gDLLElem;

typedef struct gDLList
{
	gDLLElem *head, *tail;
} gDLList;

/* Include the lower level linkedlist definitions */
#define MYSTRUCT GLL_TYPE
#include <linkedlist.h>

/* Utility functions */

/* gdllcreat.c */
gDLList *gDLLCreat(void);

/* gdllmisc.c */
gDLLElem *gDLLGetNext(gDLLElem *p);
void gDLLSetNext(gDLLElem *s, gDLLElem *next);
gDLLElem *gDLLGetPrev(gDLLElem *p);
void gDLLSetPrev(gDLLElem *s, gDLLElem *prev);

/* gdllsort.c */
void gDLLSort(gDLList *list, int (*compare)(void *, void *));

/* gdllreverse.c */
void gDLLReverse(gDLList *list);

/* gdllapply.c */
void gDLLApply(gDLList *list, void (*func)(), va_list);

/* gllappend.c */
void gDLLAppend(gDLList *list, void *member);

/* gdlladvance.c */
void *gDLLAdvance(gDLList *list, gDLLElem **position);

/* gdllbackup.c */
void *gDLLBackup(gDLList *list, gDLLElem **position);

/* gdllprepend.c */
void gDLLPrepend(gDLList *list, void *member);

/* gdllcount.c */
int gDLLCount(gDLList *list);

/* gdllinsert.c */
void gDLLInsert(gDLList *list, gDLLElem **position, void *member, int insertbefore);

/* gdlldelete.c */
void *gDLLDelete(gDLList *list, gDLLElem **pmember, void (*destroy)(void *));

/* gdlluniq.c */
void gDLLUniq(gDLList *list, int (*compare)(void *, void *), void (*destroy)(void *), int already_sorted);

/* gdlldestroy.c */
void gDLLDestroy(gDLList *list, void (*destroy)(void *));

