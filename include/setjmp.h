#if	z80
typedef	int	jmp_buf4;
#endif

#if	i8086
typedef	int	jmp_buf8;
#endif

#if	i8096
typedef	int	jmp_buf10;
#endif

#if	m68k
typedef	int	jmp_buf16;
#endif

extern	int	setjmp(jmp_buf);
extern void	longjmp(jmp_buf, int);
