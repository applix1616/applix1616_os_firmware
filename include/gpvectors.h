/*
 * GPCPU device interrupt vectors
 */

#ifndef GPVECTORS_H
#define GPVECTORS_H

#define D_INTVEC	100
#define P_INTVEC	128
#define T_INTVEC	160
#define D1_INTVEC	200
#define D2_INTVEC	220

#endif

