EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:onsemi
LIBS:transistors
LIBS:74xgxx
LIBS:applix1616-sch
LIBS:ttl_ieee
LIBS:parallax_propeller
LIBS:Lattice
LIBS:applix1616-s00-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "Applix 1616"
Date "19 jun 2017"
Rev "1"
Comp ""
Comment1 "Master"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 8900 2950 1200 2150
U 5919B687
F0 "Video out" 50
F1 "applix1616-s01.sch" 50
F2 "15MHZ" I L 8900 3100 60 
F3 "-WRPAL" I L 8900 5000 60 
F4 "7.5MHZ" O R 10100 3100 60 
F5 "BC3" I L 8900 3800 60 
F6 "BC2" I L 8900 3900 60 
F7 "BC1" I L 8900 4000 60 
F8 "BC0" I L 8900 4100 60 
F9 "-15MHZ" I L 8900 3200 60 
F10 "EN640" I L 8900 3400 60 
F11 "DISP" I L 8900 3300 60 
F12 "VD1" I L 8900 4300 60 
F13 "D[0..15]" B L 8900 4600 60 
F14 "CSYNC" I L 8900 4800 60 
F15 "VSOUT" I L 8900 3550 60 
F16 "CSOUT" I L 8900 3650 60 
F17 "VD0" I L 8900 4400 60 
F18 "A[1..21]" I L 8900 4500 60 
$EndSheet
$EndSCHEMATC
