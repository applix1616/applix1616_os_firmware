/*
 * Print out diagnostic based on block error code
 */

#include <syscalls.h>

dobec(ec)
int ec;
{
	char buf100;

	trap7(120, STDERR, "%s\r\n", INTERPBEC(ec, buf)); /* fprintf to stderr */
	return ec;
}

