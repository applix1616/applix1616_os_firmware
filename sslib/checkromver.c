/*
 * Check the ROM version for an application
 */

#include <types.h>
#include <files.h>
#include <syscalls.h>

checkromver(minver, maxver)
unsigned long minver, maxver;
{
	unsigned long romver;
	char *e;

	e = (char *)0;

	romver = (unsigned long)GETROMVER();
	if (romver < minver)
		e = "later";
	if (romver > maxver)
		e = "earlier";

	if ((int)e)
	{
		FPRINTF(STDERR, "Requires 1616/OS V%d.%d or %s\r\n",
			maxver/16, maxver&15, e);
		return -1;
	}
	return 0;
}
