/*
 * Remove control chars & white space from a string
 */

char *
strclean(str)
register char *str;
{
	char *cp;

	cp = str;
	while (*str)
	{
		if (*str <= ' ')
			strcpy(str, str + 1);
		else
			str++;
	}
	return cp;
}
