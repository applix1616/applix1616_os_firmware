/*
 * Convert string to upper case.
 */

char *stoupper(char *str)
{
	char *bufsave;

	bufsave = str;
	while (*str)
	{
		*str = toupper(*str);
		str++;
	}
	return bufsave;
}
