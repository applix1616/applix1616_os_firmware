/*
 * Add extension to pathname, if none exists
 */

#include <syscalls.h>

char *roomfor(), *index();

char *
addext(path, ext, gmmode)
char *path;
char *ext;
int gmmode;
{
	register char *cp, *cp1;

	cp = path;
	/* Find the last slash */
	while (cp1 = index(cp, '/'))
		cp = cp1 + 1;
	if (index(cp, '.'))
	{	/* Has extension already */
		return roomfor(path, 0);
	}
	else
	{
		cp = (char *)GETMEM(strlen(path) + strlen(ext) + 1, gmmode);
		if ((int)cp >= 0)
		{
			strcpy(cp, path);
			strcat(cp, ext);
		}
		return cp;
	}
}

