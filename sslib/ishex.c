/* Return true if char is hex */
ishex(ch)
register char ch;
{
	ch = toupper(ch);
	return (isdigit(ch) || ((ch >= 'A') && (ch <= 'F')));
}
