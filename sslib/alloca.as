;
; Local storage allocator.
;
; Usage alloca(nbytes)
;
; Stack frame:
;
;    -------------------------------
;    |         nbytes              |
;    -------------------------------
;    |      return address         |
;SP->-------------------------------
;
; What we do is to subtract 'nbytes' from the SP,
; copy to d0 & return to old address
;
		psect	text

		global	_alloca
_alloca:
		move.l	(sp)+,a0	; Return address
		move.l	(sp),d0		; Requested storage
		add.l	#1,d0		; Round to word multiple
		bclr	#0,d0
		sub.l	d0,sp
		move.l	sp,d0		; For return to caller
		add.l	#4,d0		; Adjust for caller stack adjust
		jmp	(a0)


;
; Calculate length of sring, allocate storage for it & copy
; It across.
;
; Usage: char * stralloca(char *)
;

		global	_stralloca
_stralloca:
		move.l	4(sp),a0
		move.l	(sp),d1		; Return address

		clr.l	d0
cnt:		addq.l	#1,d0
		tst.b	(a0)+
		bne.b	cnt

; d0 holds length

		addq.l	#1,d0
		bclr	#0,d0		; Round up
		move.l	4(sp),a0	; Original string
		sub.l	d0,sp		; New sp
		move.l	sp,a1
		move.l	sp,d0		; Return value

; Copy the string
moveit:		move.b	(a0)+,(a1)+
		bne.b	moveit

		sub.l	#4,sp		; For caller stack adjust
		move.l	d1,a0
		jmp	(a0)
