htoi(str)
char *str;
{
	register unsigned int val = 0;
	register unsigned char ch;

	while (ch = toupper(*str++))
	{
		val *= 16;

		if ((ch >= '0') && (ch <= '9'))
			val += ch - '0';
		else
		{
			if ((ch >= 'A') && (ch <= 'F'))
				val += ch - 'A' + 10;
			else
				return 0;	/* Bad hex */
		}
	}
	return val;
}
