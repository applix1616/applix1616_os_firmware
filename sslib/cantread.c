#include <syscalls.h>

cantread(file, fd, ec)
char *file;
int fd;
int ec;
{
	CLOSE(fd);
	return cantdo(file, ec, "read");
}
