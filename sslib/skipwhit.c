/* Advance a pointer over white space. Return new value. Stop on null */

char *
skipwhite(ptr)
register char *ptr;
{
	while (iswhite(*ptr) && *ptr)
		ptr++;		/* Skip white space */
	return ptr;
}
