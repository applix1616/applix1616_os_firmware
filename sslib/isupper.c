int isupper(ch)
char ch;
{
	return (ch >= 'A') && (ch <= 'Z');
}
