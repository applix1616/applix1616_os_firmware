/*
 * Convert ASCII string to number
 */

unsigned long
atoi(str)
register char *str;
{
	register unsigned long res = 0;

	while ((*str >= '0') && (*str <= '9'))
		res = res * 10 + (*str++ - '0');

	return res;
}

