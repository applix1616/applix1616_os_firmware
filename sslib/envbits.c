/*
 * Read an environment bit
 */

#include <syscalls.h>

int envbits(int mask)
{
	return GETENVBITS(GETPID()) & mask;
}
