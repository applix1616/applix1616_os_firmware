char
tolower(ch)
register char ch;
{
	return isupper(ch) ? ch + 32 : ch;
}
