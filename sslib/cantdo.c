/* General function */

#include <syscalls.h>

cantdo(file, ec, mes)
char *file;
int ec;
char *mes;
{
	trap7(120, STDERR, "Cannot %s %s: ", mes, file); /* fprintf to stderr */
	return dobec(ec);
}
