char toupper(ch)
char ch;
{
	return islower(ch) ? ch - 32 : ch;
}
