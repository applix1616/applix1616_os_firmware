#include <syscalls.h>
#include <blkerrcodes.h>

nomemfor(fn)
char *fn;
{
	PRINTF("Cannot allocate memory for %s\r\n", fn);
	return BEC_NOBUF;
}
