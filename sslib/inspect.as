	global	_rawgetchar
	global	_syscall
	global	_readeofchar
	psect	text
	align.w
_rawgetchar:
	link	A6,#-8
	move.l	#-1,-(A7)
	move.l	#14,-(A7)
	jsr	_syscall
	add.l	#8,A7
	move.l	D0,-8(A6)
	move.l	#2,-(A7)
	jsr	_syscall
	add.l	#4,A7
	move.l	D0,-4(A6)
	cmp.l	#-18,D0
	bne	l4
	cmp.l	#16,-8(A6)
	bge	l4
	move.l	-8(A6),-(A7)
	jsr	_readeofchar
	add.l	#4,A7
	move.l	D0,-4(A6)
	tst.l	D0
	blt	L1
	cmp.l	#255,D0
	ble	l4
L1:
	move.l	#-18,-4(A6)
l4:
	move.l	-4(A6),D0
	unlk	A6
	rts
	align.w
_readeofchar:
	link	A6,#0
	clr.l	-(A7)
	clr.l	-(A7)
	clr.l	-(A7)
	move.l	#5,-(A7)
	move.l	8(A6),-(A7)
	move.l	#133,-(A7)
	jsr	_syscall
	add.l	#24,A7
	unlk	A6
	rts
