isalpha(ch)
register unsigned char ch;
{
	return (ch > 31) && !(ch & 0x80);
}
