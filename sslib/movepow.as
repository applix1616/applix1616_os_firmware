;
;   Provide C access to the movep instruction:  it comes in two flavours,
;	and two lengths (word/long).
;   Usage:   movepox( addr, value )
;	where x is either l for long or w for word.  addr <- value.
;		movepix( addr )
;	where is x is l or w.  Returns the value from the peripheral as long.
;
		psect	text
		global	_movepow,_movepol
_movepow:				; Move word to peripheral
		move.l	4(sp),a0	; Address to start at
		move.w	8(sp),d0	; Value to go out.
		movep.w	d0,0(a0)	; Done
		rts

_movepol:				; Move word to peripheral
		move.l	4(sp),a0	; Address to start at
		move.l	8(sp),d0	; Value to go out.
		movep.l	d0,0(a0)	; Done
		rts
