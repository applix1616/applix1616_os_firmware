/*
 *	move the cursor to the appropriate position.
 */

#include <syscalls.h>
#include <sedit.h>

gotoxy(x, y)
register int x, y;
{
	if (x >= width)
	{
		y += x / width;
		x = x % width;
	}
	PUTCHAR(MVCURS1);
	PUTCHAR(MVCURS2);
	PUTCHAR(y + ' ');
	PUTCHAR(x + ' ');
}
