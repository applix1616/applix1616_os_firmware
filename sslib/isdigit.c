int isdigit(ch)
register char ch;
{
	return (ch >= '0') && (ch <= '9');
}
