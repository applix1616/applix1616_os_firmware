/*
 * Count number of entries in a null terminated argv list
 */

countargs(argv)
char **argv;
{
	int ret;

	for (ret = 0; *argv++; ret++)
		;
	return ret;
}
