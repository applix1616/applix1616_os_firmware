/*
 * Return true if string has wildcards
 */

gotwild(str)
char *str;
{
	return (index(str, '*') || index(str, '?'));
}

