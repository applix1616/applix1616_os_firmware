/*
 * getchar() ignoring control-D eof char
 *
 * If this function returns negative, it is an error or FILE eof
 */

#include <types.h>
#include <syscalls.h>
#include <blkerrcodes.h>
#include <chario.h>

rawgetchar()
{
	int ch;
	int stdin = SET_SIP(-1);

	ch = GETCHAR();
	if (ch == BEC_RPASTEOF)
	{
		if (stdin < NIODRIVERS)
		{
			ch = readeofchar(stdin);
			if (ch < 0 || ch > 255)
				ch = BEC_RPASTEOF;
		}
	}
	return ch;
}

readeofchar(fd)
{
	return CDMISC(fd, CDM_READEOFCHAR, 0, 0, 0);
}
