/*
 * Convert a hex char to 0-15. Assume correct hex
 */

hdtoi(ch)
register char ch;
{
	if (ch < ':') return ch - '0';
	if (ch < 'G') return ch - 55;
	return ch - 87;
}
