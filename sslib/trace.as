		psect	text
;
; Trace mode fiddling
;
		global	_traceon,_traceoff
_traceon:	or.w	#$8000,sr
		rts

_traceoff:	and.w	#$7fff,sr
		rts
