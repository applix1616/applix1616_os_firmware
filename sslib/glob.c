/* TABS4 NONDOC
 * File-name wildcard pattern matching for GNU. Copyright (C) 1985,
 * 1988, 1989, 1990 Free Software Foundation, Inc.
 */

#include <stdlib.h>
#include <string.h>
#include <options.h>

static int glob_match_after_star();
EXTERN int miscmodes;

/* Return nonzero if PATTERN has any special globbing chars in it.  */
int
glob_pattern_p(pattern)
char *pattern;
{
	register char *p = pattern;
	register char c;

	while ((c = *p++) != '\0')
		switch (c)
		{
		case '?':
		case '':
		case '*':
			return 1;

		case '\\':
			if (*p++ == '\0')
				return 0;
		}

	return 0;
}

/*
 * Match the pattern PATTERN against the string TEXT; return 1 if it
 * matches, 0 otherwise.
 *
 * A match means the entire string TEXT is used up in matching.
 *
 * In the pattern string, `*' matches any sequence of characters, `?'
 * matches any character, SET matches any character in the specified
 * set, !SET matches any character not in the specified set.
 *
 * A set is composed of characters or ranges; a range looks like character
 * hyphen character (as in 0-9 or A-Z). 0-9a-zA-Z_ is the set of
 * characters allowed in C identifiers. Any other character in the
 * pattern must be matched exactly.
 *
 * To suppress the special syntactic significance of any of `*?!-\', and
 * match the character exactly, precede it with a `\'.
 *
 * If DOT_SPECIAL is nonzero, `*' and `?' do not match `.' at the
 * beginning of TEXT.
 */

int
glob_match(pattern, text, dot_special)
char *pattern, *text;
int dot_special;
{
	register char *p = pattern, *t = text;
	register char c;

	while ((c = *p++) != '\0')
		switch (c)
		{
		case '?':
			if (*t == '\0')
				return 0;
			else
				++t;
			break;
		case '\\':
			if (*p++ != *t++)
				return 0;
			break;
		case '*':
			return glob_match_after_star(p, t);
		case '':
			{
				register char c1 = *t++;
				int invert;

				if (c1 == '\0')
					return 0;

				invert = (*p == '!');

				if (invert)
					p++;

				c = *p++;
				for(;;)
				{
					register char cstart = c, cend = c;

					if (c == '\\')
					{
						cstart = *p++;
						cend = cstart;
					}

					if (cstart == '\0')
						return 0;	/* Missing ''. */

					c = *p++;

					if (c == '-')
					{
						cend = *p++;
						if (cend == '\\')
							cend = *p++;
						if (cend == '\0')
							return 0;
						c = *p++;
					}
					if (c1 >= cstart && c1 <= cend)
						goto match;
					if (c == '')
						break;
				}
				if (!invert)
					return 0;
				break;
		match:

				/*
				 * Skip the rest of the ... construct that
				 * already matched.
				 */
				while (c != '')
				{
					if (c == '\0')
						return 0;
					c = *p++;
					if (c == '\0')
						return 0;
					if (c == '\\')
						p++;
				}
				if (invert)
					return 0;
				break;
			}

		default:
			if (c != *t++)
				return 0;
		}

	return *t == '\0';
}


/*
 * Like glob_match, but match PATTERN against any final segment of
 * TEXT.
 */

static int
glob_match_after_star(pattern, text)
char *pattern, *text;
{
	register char *p = pattern, *t = text;
	register char c, c1;

	while ((c = *p++) == '?' || c == '*')
		if (c == '?' && *t++ == '\0')
			return 0;

	if (c == '\0')
		return 1;

	if (c == '\\')
		c1 = *p;
	else
		c1 = c;

	--p;
	for(;;)
	{
		if ((c == '' || *t == c1) && glob_match(p, t, 0))
			return 1;
		if (*t++ == '\0')
			return 0;
	}
}
