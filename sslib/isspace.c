int isspace(ch)
char ch;
{
	return (ch == ' ') || (ch == '\t') || (ch == '\n');
}
