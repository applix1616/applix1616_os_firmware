int
islower(ch)
register char ch;
{
	return (ch >= 'a') && (ch <= 'z');
}
