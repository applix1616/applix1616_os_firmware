/*
 * Convert a hex ascii digit to binary
 */

digtohex(ch)
register unsigned char ch;
{
	if (ch < ':') return ch - '0';
	return ch - 55;
}
