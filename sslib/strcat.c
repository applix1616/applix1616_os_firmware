char *strcat(s1,s2)
register char *s1, *s2;
{
	char *temp;

	temp = s1;
	while(*s1) s1++;
	do
		*s1++ = *s2;
	while (*s2++);
	return temp;
}
