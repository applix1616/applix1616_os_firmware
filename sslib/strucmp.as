		psect	text
;
;/* Compare strings, ignoring case */
;
;strucmp(s1, s2)
;register char *s1, *s2;
;{
;	while (*s1)
;	{
;		if ((*s1 != *s2) && (toupper(*s1) != toupper(*s2)))
;			break;
;		s1++;
;		s2++;
;	}
;	return *s1 - *s2;
;}
		global	_strucmp
_strucmp:
		move.l	4(sp),a0	; s1
		move.l	8(sp),a1	; s2
		moveq.l	#0,d0
		moveq.l	#0,d1
l1:		move.b	(a1)+,d1	; *s2
		move.b	(a0)+,d0	; *s1
		beq.b	cmpend		; End of s1
		cmp.b	d0,d1
		beq.b	l1
		cmp.b	#$7b,d0	; d0 - 'z': C clear if d0 > 'z'
		bcc.b	d0ok
		cmp.b	#$61,d0	; C clear if d0 > 'a' - 1
		bcs.b	d0ok
		sub.b	#$20,d0
d0ok:
		cmp.b	#$7b,d1	; C clear if d1 > 'z'
		bcc.b	d1ok
		cmp.b	#$61,d1	; C clear if d1 > 'a' - 1
		bcs.b	d1ok
		sub.b	#$20,d1
d1ok:
		cmp.b	d0,d1
		beq.b	l1		; Loop on
cmpend:		sub.l	d1,d0
		rts
