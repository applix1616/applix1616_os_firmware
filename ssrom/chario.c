/* TABS4 NONDOC
 * Handle the system I/O functions
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
#include <files.h>
#include <blkerrcodes.h>
#include <signal.h>


#include "constants.h"
#include "debug.h"
#include "mondefs.h"
#include "monfns.h"
#include "setmem.h"
#include "fileio.h"
#include "movstr.h"
#include "dirfns.h"
#include "proc.h"
#include "newmem.h"
#include "chario.h"



int kb_getc(), kb_istat(), con_misc();
int video_putc(), video_ostat();
int scc_getc(), scc_istat(), scc_putc(), scc_ostat(), scc_misc();
int cent_getc(), cent_istat(), cent_putc(), cent_ostat();
int file_getc(), file_istat(), file_putc(), file_ostat();



/* The arrays of input & output driver functions */
struct chardriver *chardrivers;

extern uint maxfiles;
extern int syssafe;
extern int switchpending;
extern int ouruid;
extern int nlastlines;
extern int charsignalpending;
extern int reslevel;

extern int entry(int addr, int nargs,int argstr,int  argtype,int  argval,int  p1,int p2);

int chread(uint fd, register uchar *buf, register uint nbytes);
int chwrite(uint fd,uchar *buf,int nbytes);
int doset(uint newval,uint *poldval);


char firstchtime;

/*
 * Some I/O device identifiers
 */

#ifdef A1616
char *dn_con = "con:";
char *dn_cent = "cent:";
#endif /*A1616*/

char *dn_sa = "sa:";
char *dn_sb = "sb:";
char *dn_null = "null:";

/* The numbers of the standard drivers */
uint stdout, stdin, stderr, conin, conout;

/* fd of NULL: */
uint nullfd;

int instipdvr(), instopdvr(), instdvr();
char *fix_dname();

int _getc(), _sgetc();
int _putc(uint fd,uchar ch);
int _sputc();

/*
 * Set up default stuff
 */

void chio_init(void)
{
	int null_putc(), null_ostat();
	int null_getc(), null_istat();

/* Set up invalid entries */
	firstchtime = !reslevel;
/* Preserve con: driver */
	if (reslevel)
	{
		clearmem(chardrivers + 1, (NIODRIVERS - 1) * sizeof(*chardrivers));
		chardrivers[0].xoffed = 0;
	}
	else
		clearmem(chardrivers, NIODRIVERS * sizeof(*chardrivers));

/* Set up default entries */
	/* 0 */
#ifdef A1616
	ADD_XIPDVR((ulong)kb_getc|TOPBIT, kb_istat, dn_con, 0, con_misc);
	ADD_OPDVR(video_putc, video_ostat, dn_con, 0);
#endif /*A1616*/

	/* 1 */
	ADD_XIPDVR((ulong)scc_getc|TOPBIT, scc_istat, dn_sa, 0, scc_misc);
	ADD_OPDVR(scc_putc, scc_ostat, dn_sa, 0);

	/* 2 */
	ADD_XIPDVR((ulong)scc_getc|TOPBIT, scc_istat, dn_sb, 1, scc_misc);
	ADD_OPDVR(scc_putc, scc_ostat, dn_sb, 1);

	/* 3 */
#ifdef A1616
	ADD_IPDVR(null_getc, null_istat, dn_cent, 0);
	ADD_OPDVR(cent_putc, cent_ostat, dn_cent, 0);
#endif /*A1616*/

	/* 4 */
	ADD_IPDVR(null_getc, null_istat, dn_null, 0);
	ADD_OPDVR(null_putc, null_ostat, dn_null, 0);

	chardrivers[4].statbits = 0;	/* Read/write/creat OK on null: */

#ifdef A1616
	nullfd = 4;
#endif /* A1616 */
#ifdef GPCPU
	nullfd = 2;
#endif /* GPCPU */
	firstchtime = 0;

	set_io(0);
}

/*
 * Set up default I/O devices: If passed arg is 0, make it the screen, else
 * use switch 2 to determine if it is serial or K/B
 */

void set_io(int mode)
{
	register int i;
#ifdef A1616
	if ((*IPORT & 0x40) && mode)
	{	/* Usa SA: */
		i = 1;
		FPRINTF(0, "\r\nSwitch 2 open: using SA: for console\r\n");
	}
	else
		i = 0;
#endif /* A1616 */
#ifdef GPCPU
	i = 0;
#endif /*GPCPU*/
	conin = i;
	stdin = i;
	stdout = i;
	conout = i;
	stderr = i;
}

int null_putc(void) { return 0; }
int null_ostat(void) { return 1; }
int null_getc(void) { return BEC_RPASTEOF; }
int null_istat(void) { return 1; }

/*
 * System call 2: Standard input code. The GETCHAR() macro vectors here
 * If out of input, return -1 and reassign input to NULL:
 */

int _getchar(void)
{
	int retval;
	uchar ch;

	if ((retval = chread(stdin, &ch, 1)) < 1)
	{
		if (retval == 0)
			return BEC_RPASTEOF;
		if (stdin >= NIODRIVERS)
		{
			if ((retval != BEC_RPASTEOF) && (retval != BEC_PIPECLOSED))
			{	/* If not out of input, report error on screen */
				/* cantread closes file */
				cantread((char *)FILESTAT(stdin, 0), stdin, retval);
			}
			else
				tryclose(stdin);
			stdin = FIND_DRIVER(0, dn_null);
		}
		return retval;
	}
	return ((stdin < NIODRIVERS) && (ch == chardrivers[stdin].eofchar) &&
				!chardrivers[stdin].rawmode) ? BEC_RPASTEOF : ch;
}

/*
 * System call 3: Standard input status
 */

int _sgetchar(void)
{
	return _sgetc(stdin);
}

/*
 * System call 4: Standard output.
 */

int _putchar(uchar ch)
{
	int retval, stdosave;

	if ((retval = chwrite(stdout, &ch, 1)) < 0)
	{	/* May be file error */
		if ((stdout >= NIODRIVERS) && (stdout < (NIODRIVERS+maxfiles)))
		{
			stdosave = stdout;
			stdout = FIND_DRIVER(1, dn_null);
			if (retval != BEC_PIPECLOSED)
				cantdo("output", retval, "write");
		}
	}
	return retval;
}

/*
 * System call 5: Standard output status
 */

int _sputchar(void)
{
	return _sputc(stdout);
}

/*
 * System call 6: Call an input driver. Call stdin if bad driver
 * number. Close file and return error code if EOF.
 */

int _getc(uint fd)
{
	uchar ch;
	int retval;

	if ((retval = chread(fd, &ch, 1)) < 1)
		return retval ? retval : BEC_RPASTEOF;
	return ((fd < NIODRIVERS) && (ch == chardrivers[fd].eofchar) &&
					!chardrivers[fd].rawmode) ? BEC_RPASTEOF : ch;
}

int chread(uint fd, register uchar *buf, register uint nbytes)
{
	register int retval;
	register uint dvrnum;
	register int passval;
	register int (*fptr)();
	register struct chardriver *chardriver;
	int nread;

	if (fd >= 0x80)
		dvrnum = fixfd(&fd);	/* Convert STDIN, STDOUT, STDERR */
	else
		dvrnum = fd;

	if (dvrnum >= NIODRIVERS)
	{	/* File or pipe */
		return READ(fd, buf, nbytes);
	}

	chardriver = chardrivers + dvrnum;

	if (!(fptr = chardriver->doip))
		return BEC_BADFD;

	passval = chardriver->ippassval;

	/* Does the driver support multichar reads? */
	if (chardriver->miorvec)
	{
		return entry((int) chardriver->miorvec, dvrnum,(int) buf, nbytes, passval,0,0);
	}

	nread = 0;
	while (nbytes--)
	{
		if ((retval = entry((int) fptr, passval,0,0,0,0,0)) < 0)
			return retval;
		if ((retval == chardriver->eofchar) && !chardriver->rawmode)
		{
			if (nread)
				return nread;
			return BEC_RPASTEOF;	/* eof on start of line */
		}
		*buf++ = retval;
		nread++;
		if (retval == '\n' && !chardriver->rawmode)
			return nread;
		if (switchpending)		/* Do a context switch */
			GETROMVER();
	}
	return nread;
}

/*
 * System call 7: Call an input driver status routine
 */

int _sgetc(uint dvrnum)
{
	int (*fptr)();

	if (dvrnum >= 0x80)
		fixfd(&dvrnum);		/* Convert STDIN, STDOUT, STDERR */

	if (dvrnum < NIODRIVERS)	/* Char driver? */
	{
		fptr = chardrivers[dvrnum].ipstatus;
		return ((fptr) ? entry((int) fptr, chardrivers[dvrnum].ippassval,0,0,0,0,0) :
				entry((int) chardrivers[conin].ipstatus, chardrivers[conin].ippassval,0,0,0,0,0));
	}
	else	/* File redirection or bad driver */
		return fileready(dvrnum, 1);
}

/*
 * System call 8: Call an output driver
 */

int _putc(uint fd,uchar ch)
{
	return chwrite(fd, &ch, 1);
}

int chwrite(uint fd,uchar *buf,int nbytes)
{
	register int (*fptr)();
	register struct chardriver *chardriver;
	register uint dvrnum;
	register int passval;
	register int retval;
	int *pswitchpending;

	if (fd >= 0x80)
		dvrnum = fixfd(&fd);	/* Convert STDIN, STDOUT, STDERR */
	else
		dvrnum = fd;

	if (dvrnum >= NIODRIVERS)
		return WRITE(fd, buf, nbytes);

	chardriver = chardrivers + dvrnum;

	if (!(fptr = chardriver->doop))
		return BEC_BADFD;

	passval = chardriver->oppassval;

	/* Does the driver support multichar writes? */
	if (chardriver->miowvec)
	{
		return entry((int) chardriver->miowvec, dvrnum,(int) buf, nbytes, passval,0,0);
	}

	pswitchpending = &switchpending;

	while (nbytes--)
	{
		if ((retval = entry((int)fptr, *buf++, passval,0,0,0,0)) < 0)
			return retval;
		/*
		 * This loop can lock out context switches. If one is pending,
		 * do a harmless system call to force the switch
		 * I've forgotten why! need to test this
		 */
	 	if (*pswitchpending)
			GETROMVER();
	}
	return 0;
}

/*
 * System call 9: Get an output driver status
 */

int _sputc(uint dvrnum)
{
	int (*fptr)();

	if (dvrnum >= 0x80)
		fixfd(&dvrnum);		/* Convert STDIN, STDOUT, STDERR */

	if (dvrnum < NIODRIVERS)	/* Char driver? */
	{
		fptr = chardrivers[dvrnum].opstatus;
		return (fptr) ? entry((int)fptr, chardrivers[dvrnum].oppassval,0,0,0,0,0) :
			entry((int)chardrivers[conout].opstatus, chardrivers[conout].oppassval,0,0,0,0,0);
	}
	else	/* File redirection or bad input */
		return fileready(dvrnum, 0);
}

/*
 * System call 10: Install an input driver. Return the driver number
 * V4.2: If MSB of iovec set, miscvec is valid.
 */
/* Really function pointers */

int _add_ipdvr(ulong iovec,ulong statvec,char *name,int passval,int (*miscvec)())
{
	return instdvr(iovec & TOPBITM1, statvec, name,
		1, passval, (iovec & TOPBIT) ? miscvec : (int(*)())0);
}

/*
 * System call 12: Install an output driver. Return the driver number
 */

int _add_opdvr(ushort (*iovec)(),ushort (*statvec)(),char *name,int passval)
{
	return instdvr(iovec, statvec, name, 0, passval, (int (*)())0);
}

/*
 * Install an input or output driver in the supplied table
 * If iovec == 0, we are deleting an entry: only copy if found by name
 *
 * If 'isinput' true, overwrite input vector part, set up defaults
 */

int instdvr(int (*iovec)(),int (*statvec)(),char *name,int isinput,int passval,int (*miscvec)())
{
	register unsigned short i;
	register struct chardriver *chardriver;
	register int v;
	register int retval;
	register char samename;
	char pass;
	int eofcsave;			/* Save CON: eof char */
	int ressave, sigsave;	/* Save CON: reset & sigchars */
	char buf[100];

	syssafe++;
	movstr(buf, name, DNAMESIZE);
	/*
	 * Two passes. First looks for same name, second looks for gap
	 */

	for (pass = 0; pass < 2; pass++)
	{
		chardriver = &chardrivers[0];
		for (i = 0; i < NIODRIVERS; i++, chardriver++)
		{
			samename = !strucmp(chardriver->name, buf);

			/* Dumping the device: trash last lines */
			if (samename && !iovec && isinput)
				DUMPLASTLINES(i);
			if (samename || (pass && !chardriver->doip))
			{	/* Is the entry empty? */
				if (isinput)
				{
					v = 256;
					/*
					 * Nice to keep con: signal char the same
					 */
					eofcsave = firstchtime ? v : chardriver->eofchar;
					ressave = firstchtime ? 0x92 : chardriver->resetchar;
					sigsave = firstchtime ? 0x83 : chardriver->sigintchar;
					clearmem(chardriver, sizeof(*chardriver));
					if (i == 0)
					{	/* con: */
						chardriver->eofchar = eofcsave;
						chardriver->sigintchar = sigsave;
						chardriver->xoffchar = 's' + 0x80;
						chardriver->xonchar = 's' + 0x80;
						chardriver->resetchar = ressave;
					}
					else
					{	/* Inherits con: eof char, unless sa: or sb: */
						chardriver->eofchar =
							(i < 3) ? v : chardrivers[0].eofchar;
						chardriver->sigintchar = v;
						chardriver->xoffchar = v;
						chardriver->xonchar = v;
						chardriver->resetchar = v;
					}
					chardriver->doip = iovec;
					chardriver->ipstatus = statvec;
					chardriver->miscvec = miscvec;
					chardriver->statbits = FS_NOREAD|FS_NOWRITE|FS_NOEXEC;
					movstr(chardriver->name, buf, DNAMESIZE);
					chardriver->ippassval = passval;
/* See if it is multi-char capable */
					v = CDMISC(i, CDM_MIORVEC, 0,0,0);
					if (v < 0)
						v = 0;
					chardriver->miorvec = (int (*)())v;
					v = CDMISC(i, CDM_MIOWVEC, 0,0,0);
					if (v < 0)
						v = 0;
					chardriver->miowvec = (int (*)())v;
				}
				else
				{
					chardriver->doop = iovec;
					chardriver->opstatus = statvec;
					chardriver->oppassval = passval;
				}
				retval = i;
				goto out;
			}
		}
	}
	retval = -1;
out:
	syssafe--;
	return retval;
}

/*
 * System call 133: miscellaneous char device entry point
 */

int _cdmisc(uint dvrnum,int cmd,int p1,int p2,int p3)
{
	register struct chardriver *chardriver;
	register int retval;

	if (dvrnum >= NIODRIVERS)
		fixfd(&dvrnum);
	if (dvrnum >= NIODRIVERS)
		return BEC_BADARG;
	chardriver = &chardrivers[dvrnum];
	if (!chardriver->doip)
		return BEC_BADARG;
	/* Call the driver */
	if (chardriver->miscvec)
		retval = entry((int) chardriver->miscvec, dvrnum,
				chardriver->ippassval, cmd, p1, p2, p3);
	else
		retval = 0;
	switch (cmd)
	{
	case CDM_SETSIGCHAR:
		chardriver->sigintchar = p1;
	case CDM_READSIGCHAR:
		return chardriver->sigintchar;
	case CDM_SETEOFCHAR:
		chardriver->eofchar = p1;
	case CDM_READEOFCHAR:
		return chardriver->eofchar;
	case CDM_SETXOFFCHAR:
		chardriver->xoffchar = p1;
	case CDM_READXOFFCHAR:
		return chardriver->xoffchar;
	case CDM_SETXONCHAR:
		chardriver->xonchar = p1;
	case CDM_READXONCHAR:
		return chardriver->xonchar;
	case CDM_SETRESETCHAR:
		chardriver->resetchar = p1;
	case CDM_READRESETCHAR:
		return chardriver->resetchar;
	case CDM_SENDSIGINT:
		retval = chardriver->intsig;
		chardriver->intsig = 1;
		chardriver->xoffed = 0;
		charsignalpending++;
		break;
	case CDM_SENDSIGHUP:
		retval = chardriver->hupsig;
		chardriver->hupsig = 1;
		chardriver->xoffed = 0;
		charsignalpending++;
		break;
	case CDM_KILLUSER:
		retval = chardriver->kiluser;
		chardriver->kiluser = 1;
		chardriver->xoffed = 0;
		charsignalpending++;
		break;
	case CDM_SETRAWMODE:
		chardriver->rawmode = p1;
	case CDM_READRAWMODE:
		retval = chardriver->rawmode;
		break;
	case CDM_SETHUPMODE:
		chardriver->hupmode = p1;
	case CDM_READHUPMODE:
		retval = chardriver->hupmode;
		break;
	case CDM_HASMISCVEC:
		retval = (chardriver->miscvec != (int (*)())0);
		break;
	case CDM_SETMODEBITS:
		if (p1 == 0)
			chardriver->modebits |= p2;
		else if (p1 == 1)
			chardriver->modebits &= ~p2;
		else if (p1 == 2)
			chardriver->modebits = p2;
		retval = chardriver->modebits;
		break;
	case CDM_SETPERM:
		if (p1 == 0)
			chardriver->statbits |= p2;
		else if (p1 == 1)
			chardriver->statbits &= ~p2;
		else if (p1 == 2)
			chardriver->statbits = p2;
	case CDM_READPERM:
		retval = chardriver->statbits;
		break;
	case CDM_RXTOTAL:
		retval = chardriver->rxcount;
		break;
	case CDM_TXTOTAL:
		retval = chardriver->txcount;
		break;
	}
	return retval;
}

/*
 * System call 14: Assign standard input. Return old setting
 */

int _set_sip(uint newip)
{
	return doset(newip, &stdin);
}

/*
 * System call 15: Assign standard output. Return old setting
 */

int _set_sop(uint newop)
{
	return doset(newop, &stdout);
}

/*
 * System call 20: Assign standard error, return old setting
 */

int _set_ser(uint newop)
{
	return doset(newop, &stderr);
}

int doset(uint newval,uint *poldval)
{
	uint oldsave;

	syssafe++;
	fixfd(&newval);		/* Convert STDERR, STDIN, etc */

	oldsave = *poldval;
	if (newval < (NIODRIVERS + maxfiles))
		*poldval = newval;
	syssafe--;
	return oldsave;
}

/*
 * System call 95:  Find a driver in the passed array. 'ioro' is used as
 * follows:
 *	0: Find input driver
 * 	1: Find output driver
 * return driver index or -1 if not there
 *
 * V4.2: Just return pointer to driver struct
 */

int _find_driver(int ioro,char *name)
{
	register int i;
	register struct chardriver *chardriver;
	char namebuf[DNAMESIZE];

	movstr(namebuf, name, DNAMESIZE);

	if ((int)name < NIODRIVERS)
		return (long)&chardrivers[(int)name];

	for (i = 0, chardriver = chardrivers; i < NIODRIVERS; i++, chardriver++)
	{
		if (chardriver->doip && !strucmp(chardriver->name, namebuf))
			return i;
	}
	return BEC_FNBAD;
}

/*
 * Find index of driver by name, can be either input or output
 */

int openchdvr(char *name,int mode)
{
	int retval;

	if (!strucmp(name, "tty:"))
		return getttydev((mode == 1) ? 1 : 2);
	if (!strucmp(name, "stdin:"))
		return stdin;
	if (!strucmp(name, "stdout:"))
		return stdout;
	if (!strucmp(name, "stderr:"))
		return stderr;

	if ((retval = FIND_DRIVER(0, name)) >= 0)	/* Input driver */
	{
		if (ouruid && (chardrivers[retval].statbits &
				((mode == 1) ? FS_NOREAD:FS_NOWRITE)))
			return BEC_NOPERM;
		CDMISC(retval, CDM_OPEN, mode,0,0);
	}
	return retval;
}

/*
 * A char device has been closed
 */

int closechdvr(int dvrnum)
{
  return CDMISC(dvrnum, CDM_CLOSE,0,0,0);
}

/*
 * System call 96: Return pointer to chardrivers
 */

struct chardriver *
_get_dvrlist(n)
int n;
{
	return chardrivers;
}

/*
 * Release all last lines for a character device driver
 * If dvrnum == -1, do it for ALL drivers
 */

void dumplastlines(uint dvrnum)
{
	register struct chardriver *chardriver;
	register char **lastlines;
	register int i, j;

	fixfd(&dvrnum);
	syssafe++;
	for (i = 0; i < NIODRIVERS; i++)
	{
		chardriver = chardrivers + ((dvrnum < NIODRIVERS) ? dvrnum : i);
		if (chardriver && (lastlines = chardriver->lastlines))
		{
			for (j = 0; j < nlastlines; j++)
			{
				maybefree((int)lastlines[j]);
				lastlines[j] = (char *)0;
			}
			dofreemem(lastlines);
			chardriver->lastlines = (char **)0;
		}
		if (dvrnum < NIODRIVERS)
			break;
	}
	syssafe--;
}

/* Toggle end-of file char for console */
void toggleeof(void)
{
	register int *p;

	p = &chardrivers[0].eofchar;

	if (*p == 4)
		*p = 256;
	else if (*p == 256)
		*p = 4;
}

/*
 * Miscellaneous entry point for CON: driver
 */

int con_misc(int dvrnum,int passval,int cmd,int p1,int p2,int p3)
{
	extern int video_mwrite();
	extern char kb_vi_mode;
	int ret = 0;

	if (cmd == CDM_MIOWVEC)
		ret = (long)video_mwrite;
	else if (cmd == CDM_ALTERNATE)
	{
		if (p1 == 0 || p1 == 1)
			kb_vi_mode = p1;
		ret = kb_vi_mode;
	}
	else
		ret = 0;
	return ret;
}

/*
 * Get a char, ignoring EOF
 */

int rawgetc(int fd)
{
	register int ch;
	register int dvrnum;

	ch = GETC(fd);
	if (ch == BEC_RPASTEOF)
	{
		if (fd >= 0x80)
			dvrnum = fixfd((uint *) &fd);	/* Convert STDIN, STDOUT, STDERR */
		else
			dvrnum = fd;

		if (dvrnum < NIODRIVERS)
		{
			ch = chardrivers[dvrnum].eofchar;
			if (ch < 0 || ch > 255)
				ch = BEC_RPASTEOF;
		}
	}
	return ch;
}
