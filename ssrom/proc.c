/* TABS4
 * Process control & red tape fiddling
 */
#include <types.h>
#include <syscalls.h>
#include <process.h>
#include <signal.h>
#include <blkerrcodes.h>


#include <hwdefs.h>

//#include <splfns.h>

#include "constants.h"
#include "chario.h"
#include "mondefs.h"
#include "newmem.h"
#include "kernel.h"
#include "ipcblock.h"
#include "proc.h"


extern PROC **proctable;	/* The process table */
extern int syssafe;			/* Zero if system can be interrupted */
extern int switchpending;	/* Set in interrupt code to indicate suspended switch */
extern int curpid;
extern char stackanalyse;	/* If true, print stack stats on exit */
extern int timeslice;
extern int mtnotup;			/* If true, scheduler has not started */
extern int (*cswvector)();	/* Context switch vector */
extern int charsignalpending;	/* SIGHUP or SIGINT from char device */
extern int nextpg;

extern int lastpid;	/* In memory manager */
extern uint vsi_count;		/* System clock */
extern char *cur_path;
extern int fsbusy, fsbpid;
extern int ouruid, umask;
extern int reslevel;
extern struct chardriver *chardrivers;
extern int memerrflag;
extern ulong lastenvbits;

extern char *room1for(), *room0for();

/*
 * System call 129
 *
 * Do things with processes
 * Mode
 *	0: GETPID()
 *	1: GETPPID(pid)
 *	2: EXIT(code)
 *	3: KILL(pid)
 *	4: SLEEP(ticks)
 *	5: GETPROCTAB(pid)
 *	6: CWD(pid, path)
 *	7: LOCKIN(mode) mode == 1: lock, mode == 0: unlock
 *	8: stackanalyse(mode)
 *	9: WAIT(pid)
 *	10: NICE(pid, niceval)
 *	11: SIGSEND(pid, sig, arg)
 *	12: SIGCATCH(vector)
 *	13: SENDINTERRUPT(rootpid)
 *	14: PROCTRACE(rootpid)
 *	15: GETPROCFLAGS(pid)
 *	16: ISINTERACTIVE(pid)
 *	17: spchecking(pid, mode)
 *	18: CSVEC(vec)
 *	19: GETPCURPID()
 *	20: READSIGVEC(pid)
 *	21: FSBPTR()
 *	22: FSPPTR()
 *	23: SSPTR()
 *	24: KILLUSER(homeshell)
 *	25: SIGBLOCK(pid, blk)
 *	26: ALARM(nticks)
 *	27: SIGBLOCKER(pid, sig, arg)
 *	28: SNOOZE(vec, arg1, arg2)
 *	29: SIGUSER(pid, sig, arg)
 *	30: FINDHOMESHELL(pid)
 *	31: SETSHNICE(homeshell, nice)
 *	32: LASTCHILD(pid)
 *	33: SWPPTR()
 *	34: KILLDOWN(startpid)
 *	35: SIGDOWN(startpid, sig, arg)
 *	36: KILLUID(uid)
 *	37: SIGUID(uid, sig, arg)
 *	38: SETSIGEXIT(pid, mode)
 *	39: SETPG(pg) 0=read, 1=next, any other=set
 *	40: SIGPG(pg, sig, arg)
 *	41: KILLPG(pg)
 *	42:	SETPROCUMASK(pid, umask)
 *	43:	SETENVBITS(pid, mask, set)
 *	44: GETENVBITS(pid)
 *	45: NAMETOPID(pid)
 *	46: BLOCKTX(destpid, blockaddr, blocklen, sig)
 *	47: BLOCKRX(mode)
 *	48: SETENV(pid, name, setting, mode)
 *	49:	GETENV(pid, name)
 *	50: bgnice(pid, niceval)
 */

int _proccntl(uint mode,uint arg1,uint arg2,uint arg3,uint arg4)
{
	register int retval;
	register uint i;
	register PROC *proc;
#ifdef notdef
	int waitforzero();
#endif
	/* Map those modes which need to have the pid validity checked */
	static char needpid[] =

		/*  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 */
		  { 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1,

		/* 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 */
		    1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1,
		/* 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 */
		    1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0,
		/* 48 49 50 51 52 53 54 55 56                      */
		    1, 1, 1 };

	/* Map those modes for which permission is needed */
	static char permtab[] =

		/*  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 */
		  { 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0,

		/* 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 */
		    0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1,
		/* 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 */
		    0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0,
		/* 48 49 50 51 52 53 54 55 56                      */
			0, 0, 1 };

	if (mtnotup)
		return 0;	/* Called before scheduler initialisation */

	if (mode >= sizeof(needpid))
		return BEC_BADARG;

	syssafe++;
	retval = 0;

	if (needpid[mode])
	{
		if (arg1 >= MAXPIDS)
			arg1 = pidlookup((char *)&arg1);
		if ((arg1 >= MAXPIDS) || ((proc = proctable[arg1]) == 0))
			retval = BEC_BADPID;
		else if (permtab[mode] && ouruid && (proc->uid != ouruid))
			retval = BEC_NOPERM;
	}
	else
		proc = proctable[curpid];

	if (retval < 0)
		goto out;

	switch (mode)
	{
	case 0:		/* getpid */
		retval = curpid;
		break;
	case 1:		/* getppid */
		retval = proc->parent;
		break;
	case 2:		/* exit */
		proc->exitcode = arg1;
		arg1 = curpid;
	case 3:		/* kill */
axeit:
		if (arg1 < 1)	/* Can't kill root shell */
		{
			retval = BEC_BADPID;
			break;
		}
		proc->flags |= PS_EXIT;
		if (mode != 2)
			proc->exitcode = BEC_KILLED;
		if ((arg1 == curpid) && (mode < 4))	/* An exit */
			switchpending = 1;
		if ((mode == 3) || (mode == 11))
			proc->flags |= PS_KILLED;
		proc->whennext = 0;			/* Alert scheduler */
		retval = 0;
		break;
	case 4:		/* sleep */
		/* If signal pending, break the sleep now & deschedule */
		proc->whennext = proc->procsig ? 0 : vsi_count + arg1;
		/*
		 * The system call handler will pick up switchpending and
		 * deschedule curpid immediately
		 */
		switchpending = 1;
		break;
	case 5:		/* getproctab */
		retval = (int)proc;
		break;
	case 6:		/* cwd */
		dofreemem(proc->cur_path);
		proc->cur_path = room1for(arg2);
		break;
	case 7:		/* lockin */
		if (arg1)
			syssafe++;
		else
			syssafe--;
		break;
	case 8:		/* Set stack analysis */
		retval = stackanalyse;
		stackanalyse = arg1;
		break;
	case 9:		/* Wait on pid */
		syssafe--;
		while (proctable[arg1])
#ifdef notdef
			SNOOZE(waitforzero, &proctablearg1, 0);
#endif
			SLEEP(0);
		syssafe++;
		break;
	case 10:	/* Set nice */
		retval = proc->timeslice & 0xff;
		if ((int)arg2 <= 50 && (int)arg2 >= -50)
		{
			proc->timeslice = arg2;
			proc->tscount = 0;
		}
		else
			retval = BEC_BADARG;
		break;
	case 11:	/* SIGSEND(pid, val1, val2) */
signalit:
		if (arg2 == SIGSTOP)
		{
			while (fsbusy && (fsbpid == arg1))
			{	/* Don't stop it in fs! */
				syssafe--;
				SLEEP(1);
				syssafe++;
			}
			proc->flags |= PS_STOPPED;
		}
		else if (arg2 == SIGCONT)
			proc->flags &= ~PS_STOPPED;
		else
		{	/* Normal signal */
			proc->whennext = 0; /* Wake it up */
			if (proc->snoozevec)
			{	/* Make SNOOZE() return 1, restart him */
				USERREGS *pureg;

				pureg = (USERREGS *)proc->sp;
				pureg->regs[0] = 1;
				proc->snoozevec = 0;
			}
			if (!proc->sigvec)  /* No vector installed: kill it */
				goto axeit;
			sigput(proc, arg2, arg3);
		}
		if ((arg1 == curpid) && (mode != 13))
			switchpending = 1;	/* Deschedule immediately */
		break;
	case 12:	/* SIGCATCH(vector) */
		retval = proc->sigvec;
		if (!arg1)
			freeprocsignals(proc);
		proc->sigvec = arg1;
		break;
	case 13:	/* SENDINTERRUPT(rootpid) */
	case 14:	/* PROCTRACE(rootpid) */
	case 27:	/* SIGBLOCKER(rootpid, a1, a2) */
		/* Run down, find blocking process */
		while ((proc->flags & PS_BLOCKED) && (unsigned)arg1 < MAXPIDS)
		{
			arg1 = proc->child;
			proc = proctable[arg1];
		}
		if (mode == 13)
		{
			/* Found non-blocked process: signal it */
			arg2 = SIGINT;
			arg3 = 0;
			/* Don't deschedule now, because this may be from an ISR */
			goto signalit;
		}
		else if (mode == 27)
		{	/* Send signal to blocking process */
			goto signalit;
		}
		else
			proc->flags |= PS_TRACETOG;	/* Toggle trace mode */
		break;
	case 15:	/* GETPROCFLAGS(pid) */
		retval = proc->flags;
		break;
	case 16:	/* isinteractive */
		/* Go up through program's history, looking for a blocked shell */
		while ((proc->flags & PS_PARBLOCKED) &&
			!(proc->flags & PS_SHELLPROC) && arg1)
		{
			arg1 = proc->parent;
			proc = proctable[arg1];
		}
		/*
		 * proc is now an ancestor that is either a shell, or has not blocked
		 * its parent (is asynchronous).
		 * It must be a shell AND blocked for the process to be interactive.
		 */
		if (arg1 && (proc->flags & PS_SHELLPROC))
			retval = 1;
		break;
	case 17:	/* spchecking(pid, mode) */
		retval = proc->flags & PS_NOSPCHECK;
		if (mode)
			proc->flags |= PS_NOSPCHECK;
		else
			proc->flags &= ~PS_NOSPCHECK;
		break;
	case 18:	/* CSVEC(vec) */
		retval = (int)cswvector;
		cswvector = (int (*)())arg1;
		break;
	case 19:	/* GETPCURPID() */
		retval = (int)&curpid;
		break;
	case 20:	/* READSIGVEC(pid) */
		retval = (int)proc->sigvec;
		break;
 	case 21:	/* FSBPTR() */
		retval = (int)&fsbusy;
		break;
 	case 22:	/* FSPPTR() */
		retval = (int)&fsbpid;
		break;
	case 23:	/* SSPTR() */
		retval = (int)&syssafe;
		break;
	case 24:	/* KILLUSER(homeshell) */
		for (arg2 = 1; arg2 < MAXPIDS; arg2++)
		{
			if (proctable[arg2] && proctable[arg2]->homeshell == arg1)
			{
				syssafe--;
				KILL(arg2);
				syssafe++;
				retval++;
			}
		}
		break;
	case 25:	/* SIGBLOCK(pid, blk) */
		if (arg2)	/* Disallow signals */
			proc->flags |= PS_SIGBLOCK;
		else
		{		/* Allow them. If one pending, send it now */
			proc->flags &= ~PS_SIGBLOCK;
			switchpending = 1;
		}
		break;
	case 26:	/* ALARM(nticks) */
		if (!arg1)
			proc->alarmtime = 0;
		else if (arg1 != (uint)-1)
			proc->alarmtime = vsi_count + arg1;
		retval = proc->alarmtime ? (proc->alarmtime - vsi_count) : TOPBIT;
		break;
	case 28:	/* SNOOZE(vec, arg1, arg2) */
		if (proc->procsig)
			retval = 1;		/* Indicate broken snooze */
		else
		{
			proc->snoozevec = (int (*)())arg1;
			proc->snoozearg1 = arg2;
			proc->snoozearg2 = arg3;
		}
		switchpending = 1;
		break;
	case 29:	/* SIGUSER(pid, sig, arg) */
		if (!(proc->flags & PS_SHELLPROC))
			retval = BEC_BADPID;	/* Target pid must be shell */
		else
		{
			int i;
			retval = 0;
			for (i = 0; i < MAXPIDS; i++)
			{
				if ((proc = proctable[i]) &&
					(proc->homeshell == arg1))
				{
					SIGSEND(i, arg2, arg3);
					retval++;
				}
			}
			SIGSEND(arg1, arg2, arg3);
		}
		break;
	case 30:	/* FINDHOMESHELL(pid) */
		retval = proc->homeshell;
		break;
	case 31:	/* SETSHNICE(homeshell, nice) */
		if (!(proc->flags & PS_SHELLPROC))
			retval = BEC_BADPID;
		else
		{
			if ((int)arg2 <= 50 && (int)arg2 >= -50)
			{
				proc->timeslice = arg2;		/* The shell itself */
				proc->tscount = 0;
				for (i = 0; i < MAXPIDS; i++)
				{
					if ((proc = proctable[i]) &&
						(proc->homeshell == arg1))
					{
						proc->timeslice = arg2;
						proc->tscount = 0;
					}
				}
			}
			else
				retval = BEC_BADARG;
		}
		break;
	case 32:	/* LASTCHILD(pid) */
		retval = proc->lastchildpid;
		break;
	case 33:	/* swpptr */
		retval = (long)&switchpending;
		break;
	case 34:	/* KILLDOWN(startpid) */
	case 35:	/* SIGDOWN(startpid, val1, val2) */
		while ((unsigned)arg1 < MAXPIDS)
		{
			if (arg1 != curpid)
			{
				if (mode == 34)
					KILL(arg1);
				else
					SIGSEND(arg1, arg2, arg3);
			}
			arg1 = proctable[arg1]->child;
		}
		break;
	case 36:	/* KILLUID(uid) */
	case 37:	/* SIGUID(uid, sig, arg) */
		for (i = 0; i < MAXPIDS; i++)
		{
			if ((proc = proctable[i]) && (proc->uid == arg1))
			{
				if (i == curpid)
					continue;
				syssafe--;
				if (mode == 36)
					KILL(i);
				else
					SIGSEND(i, arg2, arg3);
				syssafe++;
			}
		}
		proc = proctable[curpid];
		if (proc->uid == arg1)
		{
			syssafe--;
			if (mode == 36)
				KILL(arg1);
			else
				SIGSEND(arg1, arg2, arg3);
			syssafe++;
		}
		break;
	case 38:	/* SETSIGEXIT(pid, mode) */
		if (arg2)
			proc->flags |= PS_SIGEXIT;
		else
			proc->flags &= ~PS_SIGEXIT;
		break;
	case 39:	/* SETPG(pg) 0=read, 1=next, any other=set */
		if (arg1 == 1)
			proc->pg = ++nextpg;
		else if (arg1)
			proc->pg = arg1;
		retval = proc->pg;
		break;
	case 40:	/* SIGPG(pg, sig, arg) */
	case 41:	/* KILLPG(pg) */
		for (i = 0; i < MAXPIDS; i++)
		{
			if ((proc = proctable[i]) && (proc->pg == arg1))
			{
				if (i == curpid)
					continue;
				syssafe--;
				if (mode == 41)
					KILL(i);
				else
					SIGSEND(i, arg2, arg3);
				syssafe++;
			}
		}
		proc = proctable[curpid];
		if (proc->pg == arg1)
		{
			syssafe--;
			if (mode == 41)
				KILL(arg1);
			else
				SIGSEND(arg1, arg2, arg3);
			syssafe++;
		}
		break;
	case 42:	/* SETPROCUMASK(pid, umask) */
		if (arg2 != -1)
			proc->umask = arg2;
		retval = proc->umask;
		break;
	case 43:	/* SETENVBITS(pid, mask, set) */
		if (arg3)
			proc->shellenv->envbits |= arg2;
		else
			proc->shellenv->envbits &= ~arg2;
		if (!ouruid)	/* Preserve Super user's environment bits */
			lastenvbits = proc->shellenv->envbits;
		/* Fall through */
	case 44:	/* GETENVBITS(pid) */
		retval = proc->shellenv->envbits;
		break;
	case 45:	/* NAMETOPID(name) */
		retval = arg1;
		break;
	case 46:	/* BLOCKTX(destpid, blockaddr, blocklen, sig) */
		retval = _blocktx(proc, arg2, arg3, arg4);
		break;
	case 47:	/* BLOCKRX(mode) */
		retval = _blockrx(proc, arg1);
		break;
	case 48:	/* SETENV(pid, name, setting, mode) */
		retval = _setenv(proc, arg2, arg3, arg4);
		break;
	case 49:	/* GETENV(pid, name, mode) */
		retval = _getenv(proc, arg2, arg3);
		break;
	case 50:	/* Set background nice value */
		retval = proc->bgtimeslice & 0xff;
		if ((int)arg2 <= 50 && (int)arg2 >= -50)
			proc->bgtimeslice = arg2;
		break;
	default:
		retval = BEC_BADARG;
		break;
	}
out:
	syssafe--;
	return retval;
}

/* The ps command */
int dops(void)
{
	register char **pp;
	register int pid;
	register short flags;
	PROC ourproc;
	char buf[20];

	//	printf("\r\nPID PPID HPID TS   TIME   STATUS   PC    I  O  E\r\n\r\n");
	PRINTF("\r\nPID PPID HPID TS   TIME   STATUS   PC    I  O  E\r\n\r\n");
	for (pid = 0; pid < MAXPIDS; pid++)
	{
		syssafe++;
		if (proctable[pid] == 0)
		{
			syssafe--;
			continue;
		}
		else
		{
			movmem(proctable[pid], &ourproc, sizeof(ourproc));
			syssafe--;
			flags = ourproc.flags;
			/* Print it out */
			PRINTF("%3d %3d  %3d %3d %8s %c%c%c%c%c%c %06X %2d %2d %2d ",
				ourproc.pid,
				ourproc.parent,
				ourproc.homeshell,
				ourproc.timeslice,
				tosecs(buf, ourproc.ticks),
				(flags & PS_BLOCKED) ? 'W' : '-',
				(flags & PS_PARBLOCKED) ? 'S' : 'A',
				(flags & PS_EXIT) ? 'E' : '-',
				(flags & PS_BINARY) ? 'B' : '-',
				(flags & PS_STOPPED) ? 'H' : '-',
				(flags & PS_SHELLPROC) ? 'I' : '-',
				((USERREGS *)ourproc.sp)->pc,
				ourproc.idev,
				ourproc.odev,
				ourproc.edev);
			pp = ourproc.argv;
			while (proctable[pid] && *pp)
				PRINTF(" %s", *pp++);
			prcrlf();
		}
	}
	prcrlf();
}

/* The kill and wait commands */
int dokillwait(int argc,char *argv[],char *argtype,uint *argval,int iswait)
{
	register int pid;
	register int rv, retval;
	register PROC *proc;
	register char *cp, ch;
	int sigtosend, kaflag;
	int arg;

	if (iswait)
	{
		int thepid;
		syssafe++;
		if (argc == 1)
		{	/* Wait on last asynchronous process */
			int lasttime, curpar;
			curpar = proctable[curpid]->parent;
			lasttime = 0;
			thepid = 0;
			for (pid = 0; pid < MAXPIDS; pid++)
			{	/* Find most recent process */
				if (pid == curpid)
					continue;
				if (proc = proctable[pid])
				{
					if ((proc->parent == curpar) &&
						(proc->starttime > lasttime))
					{
						thepid = pid;
						lasttime = proc->starttime;
					}
				}
			}
		}
		else
			if ((thepid = pidlookup(argv[1])) < 0)
				eprintf("%s: No such PID\r\n", argv[1]);
		syssafe--;
		if ((thepid < 1) || (thepid >= MAXPIDS))
			return -1;
		WAIT(thepid);
		return 0;
	}

	/* kill */
	retval = 0;
	kaflag = 0;
	sigtosend = 0;
	arg = 1;
	cp = argv[1];
	if (*cp++ == '-')
	{
		while (ch = toupper(*cp))
		{
			if (ch == 'K')
				kaflag |= 1;
			else if (ch == 'A')
				kaflag |= 2;
			else
			{
				sigtosend = atoi(cp);
				break;
			}
			cp++;
		}
		arg = 2;
	}
	if (sigtosend == 0)
		sigtosend = SIGTERM;
	for ( ; arg < argc; arg++)
	{
		cp = argv[arg];
		if ((pid = pidlookup(cp)) >= 0)
		{
			char *namesave[MAXPIDS];
			for (rv = 0; rv < MAXPIDS; rv++)
				namesave[rv] = proctable[rv] ?
					room0for(proctable[rv]->name) : (char *)0;
			rv = BEC_NOPERM;
			switch (kaflag)
			{
			case 0:	/* Just kill */
				rv = SIGSEND(pid, sigtosend, 0);
				break;
			case 1:	/* -k */
				rv = KILL(pid);
				break;
			case 2:	/* -a */
				rv = SIGDOWN(pid, sigtosend, 0);
				break;
			case 3:	/* -ak */
				rv = KILLDOWN(pid);
				break;
			}
			SLEEP(0); SLEEP(0); SLEEP(0);
			if (rv < 0)
				retval = cantdo(cp, rv, "kill");
			for (rv = 0; rv < MAXPIDS; rv++)
			{
				if (namesave[rv] && !proctable[rv])
				{
					PRINTF("%2d '%s': Terminated\r\n",
						rv, namesave[rv]);
				}
			}
		}
		else
			retval = cantdo(cp, BEC_BADPID, "kill");
	}
	return retval;
}

/* Look up a pid from the passed string. Return a PID or -1 if not found */
int pidlookup(char *arg)
{
	register int pid, i;
	register PROC *proc;

	pid = -1;
	syssafe++;

	if (isdigit(*arg))
	{
		pid = atoi(arg);
		if ((pid >= MAXPIDS) || (proctable[pid]==0) ||
			(proctable[pid]->flags & PS_KILLED))
			pid = -1;
	}
	else
	{	/* Assume it is a string */
		for (i = 0; i < MAXPIDS; i++)
		{
			proc = proctable[i];
			if (proc && !(proc->flags & PS_KILLED) &&
				!strucmp(bindex(proc->name, '/'), arg))
			{
				pid = i;
				break;
			}
		}
	}
	syssafe--;
	return pid;
}

/*
 * A process has run afoul of the memory allocator.
 * We must cope with a fault in file system memory allocation.
 * If fsbusy is true and curpid == fsbpid, this is the case.
 * Handle it by clearing fsbusy and doing an exit. The current process will
 * be yanked on return from the exit (proccntl) system call and trashed
 * by the scheduler
 */

void memfault(int ret)
{
	int insys = 0;
	register PROC *proc;

	proc = proctable[curpid];
	if (insys = (fsbusy && (fsbpid == curpid)))
		fsbusy = 0;
	if (insys)
	{
		conprintf("Process '%s': %s%s .. killing\r\n",
				proc->name, ERRMES(ret),
					insys ? " within file system" : "");
		KILL(curpid);	/* Mark it for death */
	}
	if (!proc->nomemerrflag && memerrflag)
	{	/* Signal the program */
		if (!proc->sigvec)
			FPRINTF(proc->edev,
				"%s: memory allocation failure\r\n",
					proc->name);
		SIGSEND(curpid, SIGSEGV, ret);
	}
}

char *
pidname(pid)
{
	return proctable[pid]->name;
}

char *
curibcvec()
{
	return proctable[curpid]->shellenv->ibcvec;
}

setibcvec(vec)
uchar *vec;
{
	register struct shellenv *shellenv;

	syssafe++;
	shellenv = proctable[curpid]->shellenv;
	maybefree(shellenv->ibcvec);
	if (vec)
	{
		shellenv->ibcvec = (char *)getmem1(NMONCMDS);
		movmem(vec, shellenv->ibcvec, NMONCMDS);
	}
	else
		shellenv->ibcvec = 0;
	syssafe--;
}

/*
 * Work out fd of home shell's stdin/stdout. If it is to a file,
 * back up until we hit char device
 * mode = 1 for stdin, 2 for stdout.
 */

#define UNTIL(n) while (!(n))

int getttydev(int mode)
{
	int pid, dev;
	register PROC *proc;

	if (mtnotup)
		return 0;
	pid = curpid;
	do
	{
		proc = proctable[pid];
		dev = (mode == 1) ? proc->orig_idev : proc->orig_odev;
		pid = proc->homeshell;
	}
	UNTIL ((pid == 0) ||
		((dev < NIODRIVERS) && (proc->flags & PS_SHELLPROC)));

	return dev;
}

/*
 * Scan char device tables for pending signals, send any needed.
 */
void docharsignals(void)
{
	register int dvrnum;
	register struct chardriver *chardriver;

	for (chardriver = chardrivers, dvrnum = 0;
		dvrnum < NIODRIVERS; chardriver++, dvrnum++)
	{
		if (!chardriver->doip)
			continue;
		if (chardriver->kiluser)
			dochsig(dvrnum, 0);
		else if (chardriver->hupsig)
		{	/* Has precedence over interrupt */
			dochsig(dvrnum, SIGHUP);
		}
		else if (chardriver->intsig)
			dochsig(dvrnum, SIGINT);
		chardriver->hupsig = 0;
		chardriver->intsig = 0;
		chardriver->kiluser = 0;
	}
}

dochsig(dvrnum, sig)
{
	register PROC *proc;
	register int pid;
	int s;

	s = SCC_SPL();
	charsignalpending--;
	if (charsignalpending < 0)
		charsignalpending = 0;
	splx(s);

	for (pid = 0; pid < MAXPIDS; pid++)
	{
		proc = proctable[pid];
		if (proc && (proc->orig_idev == dvrnum) &&
			(proc->flags & PS_SHELLPROC))
		{	/* Found the shell process */
			if (sig == 0)
			{	/* Murder everyone */
				KILLUSER(pid);
				KILL(pid);
			}
			else if (sig == SIGHUP)
			{	/* SIGHUP to all processes */
				SIGUSER(pid, SIGHUP, 0);
			}
			else	/* SIGINT to blocker */
				SENDINTERRUPT(pid);
			break;		/* It will propagate */
		}
	}
}

setmemfault(arg)
uint arg;
{
	if (arg < 2)
		proctable[curpid]->nomemerrflag = arg;
	return proctable[curpid]->nomemerrflag;
}
