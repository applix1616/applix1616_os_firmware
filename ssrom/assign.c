/* TABS4 NONDOC
 * Pathname assignment code
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <reloc.h>
#include <blkerrcodes.h>
#include <constants.h>
//#include <splfns.h>

#define NASSIGNS	32		/* Number of paths we can assign */
#define OLDASSIGNSIZE	20		/* Maximum old length */
#define NEWASSIGNSIZE	42		/* Maximum new length */

struct assign
{
	char old[OLDASSIGNSIZE];
	char new[NEWASSIGNSIZE];
	ushort uid;
} assigns[NASSIGNS];

extern char *cur_path;
extern char *room0for(), *roomfor(), *index();
extern char slash;
extern int ouruid;

/* Read back assignments */
/* modified for modern c */
char *getassname(uint n)
{
  int wantnew, wantuid;

  wantuid = n & (TOPBIT >> 1);
  wantnew = n & TOPBIT;
  n &= 0xff;

  if (n >= NASSIGNS)
    return((char *)-1);

  if (wantuid)
  {
    //return((char *)assigns[n].uid);
  }
  return((wantnew) ? assigns[n].new : assigns[n].old);
}

void assign_init(void)
{
	clearmem(assigns, sizeof(assigns));
}

/*
 * The assign command
 *
 * Usage: ASSIGN		Prints out current definitions
 *        ASSIGN -		Removes all definitions
 *        ASSIGN /OLD		Removes definition for /OLD
 *        ASSIGN /OLD NEW	Defines or redefines /OLD as NEW
 */
int _doassign(int argc, char *argv)
{
	register int i;
	register int oldindex;
	register struct assign *assign;
	register char *av1, *av2;

	if (argc == 1)
	{	/* Display current settings */
		for (assign = assigns; assign < assigns + NASSIGNS; assign++)
			if (assign->old && ((assign->uid == ouruid) || !assign->uid))
				PRINTF("%-4d: %s assigned as %s\r\n",
					assign->uid, assign->old, assign->new);
		return 0;
	}

	av1 = &argv[1];
	if (!strucmp(av1, "-"))
	{
		for (assign = assigns; assign < assigns + NASSIGNS; assign++)
		{
			if (!ouruid || assign->uid == ouruid)
				assign->old[0] = '\0';
		}
		return 0;
	}

	if (*av1 != '/')
	{
		eprintf("%s: Must start with '/'\r\n", av1);
		return -1;
	}

	oldindex = -1;		/* Assume not found */
	for (i = 0, assign = assigns; assign < assigns + NASSIGNS; assign++, i++)
	{
		if (!strucmp(av1, assign->old) &&
			(!ouruid || (assign->uid == ouruid)))
		{
			oldindex = i;
			break;
		}
	}

	if (argc == 2)
	{	/* Attempt to delete an assign */
		if (oldindex == -1)
		{	/* Attempted to delete non-existent assign */
			return eprintf("%s: no such assignment\r\n", av1);
		}
		assign->old[0] = '\0';
		return 0;
	}

/* argc == 3: Define a new assignment */

	if (argv[20] == '\\')
		av2 = room0for(argv[2] + 1);
	else
		av2 = (char *)GETFULLPATH(argv[2], 0);

	if (!scp(av1, av2))
	{	/* Cannot assign /f0 to /f0/bin, etc */
		eprintf("Cannot recursively assign\r\n");
		goto badfree;
	}

	if (oldindex == -1)
	{	/* Define new assignment */
		for (i = 0, assign = assigns; assign < assigns+NASSIGNS; assign++,i++)
		{
			if (!assign->old[0])
			{
				oldindex = i;
				break;
			}
		}
	}

	if (oldindex == -1)
	{	/* No room for it */
		eprintf("No room for new assignment\r\n");
badfree:
		return -1;
	}

	movstr(assign->old, av1, OLDASSIGNSIZE);
	movstr(assign->new, av2, NEWASSIGNSIZE);
	assign->uid = ouruid;
	return 0;
}

/*
 * Trash assignments for a particular uid
 */
trashassign(uid)
{
	register struct assign *assign;

	for (assign = assigns; assign < assigns + NASSIGNS; assign++)
		if (assign->uid == uid)
			assign->old[0] = '\0';
}


/*
 * Perform substitutions. Return nil pointer if no change made.
 * If fowards true, normal substitution.
 * If forwards false, backward substitution for iexec prompt:
 *	attempt to find longest matching one
 * If current uid non-zero, search for that uid first
 */

char *assignsub(char *path,uint mode, int forwards)
{
	register int i, len, bestlen;
	int so, sn, pass;
	register char *o, *n;
	register struct assign *assign;
	char *cp;

	cp = (char *)0;
	bestlen = 0;
	/*
	 * Pass 0: search for assigns matching our uid
	 *		(bit tricky: works for uid 0)
	 * Pass 1: search for assigns with uid 0
	 */
	for (pass = 0; pass < (ouruid ? 2 : 1); pass++)
	{
		for (i = 0; i < NASSIGNS; i++)
		{
			assign = assigns + i;
			if (forwards)
				o = assign->old, n = assign->new;
			else
				n = assign->old, o = assign->new;

			if (*n && *o && (assign->uid == (pass ? 0 : ouruid)) &&
					!scp(o, path))
			{	/* Must substitute */
				so = strlen(o);
				sn = strlen(n);
				if (!forwards)
				{
					if (so < bestlen)
						continue;
					bestlen = so;
					maybefree(cp);
				}
				len = strlen(path) - so + sn;
				cp = (char *)GETMEM(len + 1, mode);
				strcpy(cp, n);		/* New prefix */
				strcat(cp, path + so);
				if (forwards)
					return cp;
			}
		}
	}
	return cp;
}


/*
 * Translate a relative path into an absolute one, allocate the memory
 * for it & return.
 * Flag to GETMEM(): temp or permanent storage
 */

char *lowgetfullpath(register char *opathname, int gmflag)
{
	register char *fullpath;
	register int nbackup = 0;	/* Number of '..'s */
	char *pathname = __builtin_alloca(strlen(opathname) + 1);

    /* 4.7: convert '//' to '/' and strip trailing '/' */
	{
		char *new = pathname;
		char *old = opathname;

		while (*old)
		{
			*new = *old;
			if (*old == '/')
			{
				while (old[1] == '/')
					old++;
			}
			new++;
			old++;
		}
		*new = '\0';						/* Null terminate */
		new--;								/* Back up to last char */
		while (new > pathname && *new == '/')
			*new-- = '\0';
	}

	if (ischdev(pathname))
		return roomfor(pathname, gmflag);

	if (*pathname == '/')
	{
		fullpath = roomfor(pathname, gmflag);
		goto gotpath2;
	}

	/* Interpret leading "." or ".." */
	while (*pathname == '.')
	{
		switch (pathname[1])
		{
		case '\0':	/* Ends with a '.' */
			pathname++;
			goto gotdots;
		case '/':	/* "./" : skip it */
			pathname += 2;
			break;
		case '.':	/* ".." */
			switch (pathname[2])
			{
			case '/':
				nbackup++;
				pathname += 3;
				break;
			case '\0':
				nbackup++;
				pathname += 2;
			default:
				goto gotdots;
			}
			break;
		default:	/* .aa, etc */
			goto gotdots;
		}
	}
gotdots:
	fullpath = (char *)GETMEM(strlen(pathname) + strlen(cur_path) + 2,gmflag);
	if ((int)fullpath < 0)
	  intfail(402, &gmflag - 1, 0);
	else
	{
	  strcpy(fullpath, cur_path);
	  while (nbackup--)
	    splitpath(fullpath);
	  if (*pathname)
	    {
	      strcat(fullpath, &slash);
	      strcat(fullpath, pathname);
	    }
	}

	if (strlen(fullpath) < 2)
	{	/* Too many ..'s: default to name of current block driver */
		char *ptr;

		dofreemem(fullpath);
		fullpath = roomfor(cur_path, gmflag);
		if (ptr = index(fullpath + 1, '/'))
			*ptr = '\0';
	}
gotpath2:
	stoupper(fullpath);
	return fullpath;
}


/*
 * The fullpath system call: Return a full path based upon the
 * passed relative or absolute pathname.
 *
 * Handles assignments. System call 67.
 *
 * New tricks in 4.2b:
 *	do environment string substitutions also.
 */

char *_getfullpath(char *path,uint mode)
{
	char *newcp, *ret;
	/*char *lowgetfullpath(), *assignsub(); */

	if (mode > 1)
	{
		conprintf("Garbage memmode to GETFULLPATH(%s, %d)\r\n", path, mode);

		mode = 0;		/* ?? */
	}
	/* Do environ substitution */
	newcp = (char *)ENVSUB(path, 1, mode);
	/* Look for an assignment */
	if (*newcp == '/')
	{
		ret = assignsub(newcp, mode, 1);
		if (ret)
		{
			dofreemem(newcp);
			newcp = ret;
		}
	}

	/* Now pass it off to old fullpath */
	ret = lowgetfullpath(newcp, mode);

	dofreemem(newcp);		/* Release memory */
	return ret;
}


