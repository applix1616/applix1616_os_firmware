/*
 * Block driver for the 1616 disk drive controller
 */

#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <reloc.h>
#include <blkerrcodes.h>
#include <hwdefs.h>

//#include <splfns.h>

#include "ssdd.h"
#include "mondefs.h"
#include "spl.h"
#include "psprintf.h"
#include "blockio.h"
#include "ssdddvr.h"


int ssddvers;		/* Disk controller ROM version */
int diskverbose;	/* If set, print out error messages */
char z80interlock;	/* The interlock flag */
int z80pid;		/* The PID which has tied it down */

extern int curpid;
extern uint randnum;	/* Random number to seed */
extern char *bitmaps;
extern int mtnotup;	/* True if multi-tasking resources not available */
extern int syssafe;
extern int vsi_count;
extern int reslevel;

extern int berrtest(int loc);
extern int _rdblk();
extern int _wrblk();

static int ssddread(int block,char *buffer,char unit);
static int ssddwrite(int,char *,char), ssddmisc();
static int dispderr(char), ddtimeout();
static void putzcmnd(uchar cmd);
static int putzbyte(uchar byte), putzword(int word), getzbyte(void);
static int timeout(uchar *addr,uchar bit,uchar setting,uint delay);

static int ssdd0read(int block,char *buffer)
{ return ssddread(block, buffer, 0); }

static int ssdd0write(int block,char *buffer)
{ return ssddwrite(block, buffer, 0); }

static int ssdd0misc(code, arg1)
{ return ssddmisc(code, arg1, 0); }

static int ssdd1read(int block,char *buffer)
{ return ssddread(block, buffer, 1); }

static int ssdd1write(int block,char *buffer)
{ return ssddwrite(block, buffer, 1); }

static int ssdd1misc(code, arg1)
{ return ssddmisc(code, arg1, 1); }

static int ssdd2read(int block,char *buffer)
{ return ssddread(block, buffer, 2); }

static int ssdd2write(int block,char *buffer)
{ return ssddwrite(block, buffer, 2); }

static int ssdd2misc(code, arg1)
{ return ssddmisc(code, arg1, 2); }

static int ssdd3read(int block,char *buffer)
{ return ssddread(block, buffer, 3); }

static int ssdd3write(int block,char *buffer)
{ return ssddwrite(block, buffer, 3); }

static int ssdd3misc(code, arg1)
{ return ssddmisc(code, arg1, 3); }


static int ssddread(int block,char *buffer,char unit)
{
	register char ch;
	register uchar *ssdddata = SSDDDATA;
	int ec;

	LOCKZ80(5, 0);			/* Grab the Z80 */

	putzcmnd(DCC_BLKREAD);
	ch = *ssdddata;			/* Clear any pending data */

	if ((putzbyte(unit)) || (putzword(block)))
		goto err;

	if ((ec = getzbyte()) < 0)
		goto err;

	LOCKZ80(6, 0);			/* Free it */

	if (ec)
		return dispderr(ec);
	return _rdblk(buffer);

err:	LOCKZ80(6, 0);
	return ddtimeout();
}

static int ssddwrite(int block,char *buffer,char unit)
{
	register int ch;
	register uchar *ssdddata = SSDDDATA;
	int ec;

	LOCKZ80(5, 0);		/* Get it */
	putzcmnd(DCC_BLKWRITE);
	ch = *ssdddata;		/* Clear pending data */

	if ((putzbyte(unit)) || (putzword(block)))
		goto err;
	_wrblk(buffer);

	if ((ec = getzbyte()) < 0)
		goto err;

	LOCKZ80(6, 0);		/* Free it */

	if (ec)
		return dispderr(ec);
	return 0;

err:	LOCKZ80(6, 0);
	return ddtimeout();
}

#ifdef notdef
ssddwait()
{
	register int i;
	extern int _speed;

	i = _speed ? 0x10001 : 0x8001;	/* Experimentally determined! */

	/* 44 cycles per loop: 6 usec @ 7.5 MHz */
	/* 20,000 / 6 = 3,333 */
	while (i--)
	{
		if (*SSDDRXRDY & 0x80)
			return;
	}
	i =
	while (!(*SSDDRXRDY & 0x80))
		if (!mtnotup)
			SLEEP(0);
}
#endif

/*
 * Miscellaneous entry point
 */

static int ssddmisc(int code,int arg1, int unit)
{
	switch (code)
	{
#ifdef notdef
	case MB_RESET:
		break;
#endif
	case MB_FLUSH:
		LOCKZ80(5, 0);
		putzcmnd(DCC_FLUSH);
		putzbyte(unit);
		LOCKZ80(6, 0);
		break;
	case MB_VERS:	/* Get driver version */
		return ssddvers;
	case MB_NEWDISK:
		if (ssddvers > 0x14)
		{	/* Only send for version 1.5 and higher */
			LOCKZ80(5, 0);
			putzcmnd(DCC_NEWDISK);
			putzbyte(unit);
			LOCKZ80(6, 0);
		}
		break;
	}
	return 0;
}

/*
 * Arbitrate Z80.
 *
 * Commands:
 *	0:	Get lock status
 *	1:	Get pointer to lock
 *	2:	Get pid of owner
 *	3:	Get pointer to pid
 *	4:	Request access: waits 'arg1' ticks
 *	5:	Request access: waits indefinitely
 *	6:	Unlock z80 (must be owner)
 */

int _z80lock(int cmd,int arg1)
{
	register int retval;
	register int quittime;
	register int s;

	if (mtnotup)
		return 1;

	syssafe++;
	s = SCC_SPL();
	switch (cmd)
	{
	case 0:	/* Get lock status */
		retval = z80interlock;
		break;
	case 1:	/* Get pointer to lock status */
		retval = (int)&z80interlock;
		break;
	case 2:	/* Get PID of lock owner */
		retval = z80pid;
		break;
	case 3:	/* Pointer to pid */
		retval = (int)&z80pid;
		break;
	case 4:	/* Request access: waits 'arg1' ticks */
		quittime = vsi_count + arg1;
	case 5:	/* Request access, wait forever */
		for ( ; ; )
		{
			if (z80interlock == 0)
			{	/* Grant access */
				z80interlock = 1;
				z80pid = curpid;
				retval = 0;
				goto gotit;
			}
			/* Not ready: allow interrupts & syssafe, nick off */
			_splx(s);
			syssafe--;
			SLEEP(0);
			syssafe++;
			s = SCC_SPL();
			if ((cmd == 4) && (vsi_count > quittime))
			{
				retval = 1;
				goto gotit;
			}
		}
gotit:		break;
	case 6:	/* Unlock z80 (must be owner) */
		if (z80interlock == 0)
			retval = BEC_BADARG;
		else
		{
			retval = 0;
			z80interlock = 0;
			z80pid = 0;
		}
		break;
	default:
		retval = BEC_BADARG;
	}
	_splx(s);
	syssafe--;
	return retval;
}

/*
 * Get the meaning of a disk error and display it
 */

static int dispderr(char ec)
{
	register uchar *cp;
	register int ch;
	uchar buf[100];

	LOCKZ80(5, 0);
	ch = *SSDDDATA;		/* Clear any input */
	putzcmnd(DCC_ERRMES);
	if (putzbyte(ec))
	{
tmout:
		LOCKZ80(6, 0);
		return ddtimeout();
	}

	cp = buf;
	for ( ; ; )
	{
		ch = getzbyte();
		if (ch < 0)
			goto tmout;
		*cp++ = ch;
		if (!ch)
		{
			LOCKZ80(6, 0);
			if (diskverbose)
				eprintf("\r\n%s\r\n"); //, buf);
			return BEC_IOERROR;
		}
	}
}

/*
 * Display a message when the controller times out
 */

static int ddtimeout(void)
{
	if (diskverbose)
		eprintf("\r\nDisk controller timeout\r\n");
	return BEC_IOERROR;
}

/*
 * Put a command to the Z80. If TXRDY times out, just ram it in.
 */

static void putzcmnd(uchar cmd)
{
	if (timeout(SSDDTXRDY, 0x80, 1, 1000))	/* Give it a second */
	{
		if (diskverbose)
			PRINTF("No TXRDY: Command = $%02x\r\n", cmd);
	}
	*SSDDCMND = cmd;
}

/*
 * Send a byte to the Z80. Return 0 if OK
 */
static int putzbyte(uchar byte)
{
	if (timeout(SSDDTXRDY, 0x80, 1, 6000))
		return ERROR;
	*SSDDDATA = byte;
	return 0;
}

static int putzword(int word)
{
	if (putzbyte(word >> 8))
		return ERROR;
	return putzbyte(word);
}

/*
 * Get a byte from the Z80. Return ERROR if timeout, else byte
 */

static int getzbyte(void)
{
	if (timeout(SSDDRXRDY, 0x80, 1, 6000))
		return ERROR;
	return *SSDDDATA;
}

/*
 * Wait for the specified amount of time for a bit to become ready
 * uchar *addr;		The I/O address
 * register uchar bit;	The bit to test
 * uchar setting;		The value we want (0 or 1)
 * unsigned delay;	Maximum number of loops
 */

static int timeout(uchar *addr,uchar bit,uchar setting,uint delay)
{
	register unsigned i;
	register uchar mask;
	extern char is68010;

	mask = setting ? bit : 0;
	if (CPUSPEED())
		delay *= 2;

	if (is68010)
		delay *= (is68010);

	while (delay--)
	{
		for (i = 0; i < 71; i++)
		{
			if ((*addr & bit) == mask)
				return 0;
			randnum++;		/* Spin the random number */
		}
	}
	return ERROR;
}


void ssdd_init(void)
{
	register uint vers, i;

	z80interlock = 0;
	z80pid = 0;

	if(reslevel == 0)
		ssddvers = 0;
	if(berrtest((int)SSDDDATA))
		return;		/* No disk controller */
	if (reslevel < 2)
		diskverbose = 1;
	*SSDDINTZ = 1;		/* Turn off Z80 interrupt */
	vers = *SSDDCLRINT;	/* Clear any pending 68K interrupt */
	if (reslevel)
	{
		if (ssddvers > 0x14)			/* Only if version > 1.4 */
			putzcmnd(DCC_RES0 + reslevel);	/* Tell it the reset level */
		return;		/* Only install once */
	}
	vers = *SSDDDATA;	/* Clear any pending byte */
	putzcmnd(DCC_SENDSSROMVER);
	if (putzbyte(GETROMVER()))
		goto err;
	putzcmnd(DCC_GETDDROMVER);
	if ((int)(vers = getzbyte()) < 0)
		goto err;
	if (vers > 0x14)	/* Only if version > 1.4 */
		putzcmnd(DCC_RES0 + reslevel);	/* Tell it the reset level */
	for (i = 0; i < 2; i++)
	{
		putzcmnd(DCC_SETSTEP);
		putzbyte(i);
		putzbyte(3);	/* 12 msec step rate */
	}
	ssddvers = vers;
	INST_BDVR(ssdd0read, ssdd0write, ssdd0misc, "/f0", bitmaps + BLOCKSIZE);
	INST_BDVR(ssdd1read, ssdd1write, ssdd1misc, "/f1", bitmaps + 2 * BLOCKSIZE);
	if (vers >= 0x20)
	{
		int unit;

		for (unit = 2; unit <= 3; unit++)
		{
		  char buf[BLOCKSIZE];

		  if (ssddread(0, buf, unit) < 0)
		    {
		      FPRINTF(STDERR,
			      "Cannot read root block of /h%d: Not installed\r\n",
			      unit - 2);
		    }
		  else
		    {
		      char *bmbuf;
		      int bufsize;
		      int nblocks;

		      nblocks = ((struct rootblock *)buf)->nblocks;
		      bufsize = ((nblocks + 8191) / 8) & ~BSIZEM1;
		      bmbuf = (char *)GETMEM(bufsize, 1);

		      if (unit == 2)
			{
			  INST_BDVR(ssdd2read, ssdd2write, ssdd2misc, "/h0", bmbuf);
			}
		      else
			{
			  INST_BDVR(ssdd3read, ssdd3write, ssdd3misc, "/h1", bmbuf);
			}
		    }
		}
	}
err:	;
}
