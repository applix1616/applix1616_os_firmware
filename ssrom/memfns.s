/*		psect	text*/
	.text
#include "asm68k.h"

/* ;
; Low-level stuff for the memory allocator
;
; Search backwards from an address for non-$ffffffff longword
;
; Usage: tabindex = searchdown(address, tabindex)
; Search starts at *address
; Returns new value for tabindex or -ve if none found
; */
		.global	_searchdown, searchdown
searchdown:
_searchdown:
		move.l	4(sp),a0	/*; Address*/
		move.l	8(sp),d0	/*; tabindex*/
		move.l	#-1,d1		/*; Comparison pattern*/
		addq.l	#4,a0		/*; Compensate for first predecrement*/
srchloop:
		cmp.l	-(a0),d1
		dbne	d0,srchloop
gotfree:
		rts			/*; Underflow*/

/*;
; Set or clear bits in a table We are passed a longword pointer,
; the initial mask, the number of bits and a value to set them to
;
; setbits(table, mask, nbits, value)
*/
		.global	_setbits, setbits
setbits:
_setbits:
		move.l	d2,-(sp)
		move.l	d3,-(sp)
		move.l	12(sp),a0	/*; Table pointer*/
		move.l	16(sp),d0	/*; Mask*/
		move.l	20(sp),d1	/*; nbits*/
		beq	nobits		/*; Nothing to do*/
		subq.l	#1,d1		/*; Predec for dbf*/
		tst.l	24(sp)
		bne.b	seton

/*; Clear bits*/
		move.l	d0,d2
		not.l	d2		/*; AND mask*/
setoff:		and.l	d2,(a0)		/*; Clear a bit*/
		ror.l	#1,d2
		bcc.b	sof1		/*; Start with longwords*/
		dbf	d1,setoff	/*; Next bit*/
		bra	nobits		/*; Done*/
/*; Now go in longwords*/
sof1:
		move.l	d1,d0		/*; Remaining bit count*/
		lsr.l	#5,d0		/*; Long count*/
		moveq.l	#0,d3
		addq.l	#4,a0
		bra.b	sof2
sof3:		move.l	d3,(a0)+	/*; Clear 32 bits*/
sof2:		dbf	d0,sof3
/*; Clean up last longword*/
		move.l	#0x7fffffff,d2	/*; AND mask*/
		and.l	#31,d1		/*; Remainder of counter*/
		bra.b	sof5
sof4:		and.l	d2,(a0)
		ror.l	#1,d2
sof5:		dbf	d1,sof4
		bra	nobits

/* ; Set bits on*/
seton:		or.l	d0,(a0)
		lsr.l	#1,d0
		beq.b	son5
		dbf	d1,seton
		bra.b	nobits		/*; Done*/
/*; Do it in longwords*/
son5:		move.l	d1,d0		/*; Remaining count*/
		lsr.l	#5,d0		/*; Long counter*/
		addq.l	#4,a0		/*; Next longword*/
		moveq.l	#-1,d3
		bra.b	son2
son1:		move.l	d3,(a0)+
son2:		dbf	d0,son1
/*; Clean up last longword*/
		move.l	#0x80000000,d2	/*; OR mask*/
		and.l	#31,d1
		bra.b	son3
son4:		or.l	d2,(a0)
		lsr.l	#1,d2
son3:		dbf	d1,son4

nobits:		move.l	(sp)+,d3
		move.l	(sp)+,d2
		rts

/*;
; Invert nlongs longwords at *addr
;
; invlongs(addr, nlongs)
;*/
		.global	_invlongs,invlongs
invlongs:
_invlongs:	move.l	4(sp),a0	/*; addr*/
		move.l	8(sp),d0	/*; nlongs*/
		bra.b	iend
iloop:		not.l	(a0)+
iend:		dbf	d0,iloop
		rts

/*;
; AND nlongs longwords at *saddr into *daddr
;
; andlongs(saddr, daddr, nlongs)
;*/
		.global	_andlongs, andlongs
andlongs:
_andlongs:	move.l	4(sp),a0	/*; saddr*/
		move.l	8(sp),a1	/*; daddr*/
		move.l	12(sp),d0	/*; nlongs*/
		bra.b	aend
aloop:		move.l	(a0)+,d1
		and.l	d1,(a1)+
aend:		dbf	d0,aloop
		rts

/*;
; Return size of largest free run
;
; bitrun(addr, maxbytes)
;
; d0 = max, d1 = maxbytes, d2 = bit index, d3 = count
;*/
		.global	_bitrun, bitrun
bitrun:
_bitrun:
		move.l	4(sp),a0
		move.l	8(sp),d1
		subq.l	#1,d1		/*;Predec for dbf*/
		movem.l	d2/d3/d4/d5/d6/d7/a2/a4/a5,-(sp)
		clr.l	d3		/*;count = 0;*/
		move.l	d3,d0		/*;max = 0;*/
nxtbyte:
		moveq.l	#7,d2
samebyte:
		btst	d2,(a0)
		beq.b	zerobit
		cmp.l	d3,d0		/*;d0 - d3*/
		bcc.b	smaller
		move.l	d3,d0		/*;Larger run*/
smaller:	clr.l	d3
		bra.b	nxtbit
zerobit:	addq.l	#1,d3		/*;Count++*/
nxtbit:		dbf	d2,samebyte	/*;Next bit index*/
		addq.l	#1,a0
		dbf	d1,nxtbyte
		cmp.l	d3,d0		/*;d0 - d3*/
		bcc.b	smaller2
		move.l	d3,d0		/*;Larger run*/
smaller2:
		asl.l	#5,d0		/*;32 bytes per bit*/
		movem.l	(sp)+,d2/d3/d4/d5/d6/d7/a2/a4/a5
		rts
