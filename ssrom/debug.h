/* TABS8 */

#ifdef DEBUG

#define PRINTREAD		(*(char *)0x380)
#define PRINTWRITE		(*(char *)0x381)
#define PRINTMBIO		(*(char *)0x382)
#define PRINTPROCESSDIR		(*(char *)0x383)
#define PRINTFILESTAT		(*(char *)0x384)
#define	PRINTGETFULLPATH	(*(char *)0x385)
#define DOCACHEING		(*(char *)0x386)
#define CACHEDEBUG		(*(char *)0x387)
#define PRINTLLIO		(*(char *)0x388)
#define PRINTMRD		(*(char *)0x389)
#define PRINTCDM		(*(char *)0x38a)
#define PRINTCIRCBUF		(*(char *)0x38b)

#endif
