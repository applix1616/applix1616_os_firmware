#ifndef DIRFNS_H
#define DIRFNS_H

extern char *stoupper( char *str);
extern int strucmp(char *s1,char *s2);
extern void realstoupper(char *str);
extern int hidden(register struct dir_entry *pdirent);

#endif
