/*; TABS8 NONDOC*/


#include "asm68k.h"


		.text
/*;
; The VIA interrupt dispatcher.
;
; For each pending VIA interrupt call the routine pointed to by the following
; vectors (in this order):
;
;	$100	;	Timer 1 timeout
;	$104	;	Timer 2 timeout
;	$108	;	CB1 interrupt
;	$10c	;	CB2 interrupt
;	$110	;	Shift register interrupt
;	$114	;	CA1 interrupt
;	$118	;	CA2 interrupt
;
; The called code must preserve D3-D7 and A3-A6 and must return with an RTS. It
; must also clear the VIA interrupt flag
;*/

.equ V_VECBASE,0x100		/*; The table of simulated VIA vectors*/
.equ VIABASE,  0x700100
.equ V_IFR,VIABASE+26
.equ V_IER,VIABASE+28

	.global	_via_aisr
	.global	_syssafe,_syssp,_switchpending,_timeslice,_wastick

_via_aisr:
		movem.l	d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6,-(sp)
		move.b	V_IFR,d4		/*; Which interrupts are pending?*/
		and.b	V_IER,d4
		move.l	#V_VECBASE,a4		/*; Pointer to VIA vectors*/
		asl.b	#1,d4			/*; Skip top bit*/
vloop:		asl.b	#1,d4
		bcc.b	noint			/*; Branch if not pending*/
		move.l	(a4),a0			/*; Get the vector*/
		jsr	(a0)			/*; Go to the code*/
noint:		addq.l	#4,a4			/*; Point to next vector*/
		tst.b	d4			/*; Any more bits?*/
		bne.b	vloop

/*; Was it a clock tick?*/
		tst.l	wastick
		beq.b	notyet
		clr.l	wastick
/*; Is it time for a process switch?*/
		subq.l	#1,timeslice
		bpl.b	notyet
		tst.l	syssafe
		bne.b	notsafe			/*;  System busy: can't switch*/
/*; Set up system stack, return to scheduler via rte to set spl0*/
		move.l	sp,d0			/*;  Return sp to scheduler*/
		move.l	syssp,sp
		movem.l	(sp)+,d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6
		rte				/*;  Back to scheduler at level 0*/

notsafe:	move.l	sp,switchpending	/*; Flag to systrap() (sp non-zero)*/
notyet:
		movem.l	(sp)+,d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6
		rte
