		psect	text
;
; Alter the Stack pointer
;

	global	_setsp

_setsp:
	move.l	(sp),a0		;Save return address
	move.l	4(sp),sp	;Adjust SP
	jmp	(a0)		;Back again
