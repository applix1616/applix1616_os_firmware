		.text
#include "asm68k.h"

/*
; Move a block of memory.
;
; Usage: _movmem(from, to, length)
;	 char *from, *to;
;	 long length;
;
*/

		.global	_movmem, movmem
_movmem:
movmem:
		move.l	12(sp),d1	// Zero length?
		beq.b	movexit
		move.l	4(sp),a0	// From
		move.l	8(sp),a1	//; To
		move.l	a1,d0
		cmp.l	a0,d0				//; To - from. Carry clear if To >= From
		beq.b	movexit			//; Zero distance
		bcs.b	movdown

//; Move memory higher. Start at end of block

		add.l	d1,a0
		add.l	d1,a1

		subq.l	#1,d1
		swap	d1			//; Pre-swap
movu2:
		swap	d1
movup:
		move.b	-(a0),-(a1)
		dbf	d1,movup
		swap	d1
		dbf	d1,movu2
movexit:
		rts

movdown:
		subq.l	#1,d1
		swap	d1		//; Pre-swap
movd3:
		swap	d1
movd2:
		move.b	(a0)+,(a1)+
		dbf	d1,movd2
		swap	d1
		dbf	d1,movd3
		rts

/*;
; Swap two areas of memory
;
; Usage: _swapmem(p1, p2, len)
;	 char *p1, *p2;
;	 long len;
;
*/
		.global	_swapmem, __swapmem
_swapmem:
__swapmem:
		move.l	4(sp),a0
		move.l	8(sp),a1
		move.l	12(sp),d0

mloop:
		dbf	d0,not0l		//; Pre-decrement for test
		rts							//; Zero length

not0l:
		swap	d0			//; Pre-swap
swaploop:
		swap	d0
		move.b	(a0),d1
		move.b	(a1),(a0)+
		move.b	d1,(a1)+
		dbf	d0,not0l
		swap	d0
		dbf	d0,swaploop
		rts

/*;
; Move a 64 byte directory entry
;
; movde(from, to)
;
*/
		.global	_movde
_movde:
		move.l	4(sp),a0		//; from
		move.l	8(sp),a1		//; to

		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		rts
