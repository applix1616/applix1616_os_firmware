/*
 * Block IPC code
 */

#include <types.h>
#include <syscalls.h>
#include <ipcblock.h>
#include <process.h>
#include <signal.h>
#include <blkerrcodes.h>

EXTERN int curpid, syssafe;

IPCBLOCK *ipcfetch(IPCBLOCK **addr);


/*
 * Transmit a block
 */

int _blocktx(PROC *proc,char *blockaddr,long blocklen,long sig)
{
	register IPCBLOCK **pipcblock, *ipcblock;
	int curpsave;

	pipcblock = &proc->ipcblock;
	while (*pipcblock)
		pipcblock = &(*pipcblock)->next;

	curpsave = curpid;
	curpid = proc->pid;
	ipcblock = (IPCBLOCK *)getmem0(sizeof(*ipcblock) + blocklen);
	curpid = curpsave;
	if ((int)ipcblock < 0)
		return ((int)ipcblock);
	clearmem(ipcblock, sizeof(*ipcblock));
	ipcblock->next = (IPCBLOCK *)0;
	ipcblock->whofrom = curpid;
	ipcblock->blocklen = blocklen;
	movmem(blockaddr, ipcblock->data, blocklen);
	*pipcblock = ipcblock;
	if (sig)
		SIGSEND(proc->pid, SIGBLOCKRX, curpid);
	return 0;
}

/*
 * Receive interrogation
 */

long _blockrx(PROC *proc,int mode)
{
	register IPCBLOCK *ipcblock;
	register long blockcount, bytecount, retval;
	//	int ipcfetch();

	ipcblock = proc->ipcblock;

	switch (mode)
	{
	case IPCB_GETHEAD:	/* Return pointer to head of list */
		retval = (long)ipcblock;
		break;
	case IPCB_BLOCKCOUNT:	/* How many blocks are queued? */
	case IPCB_BYTECOUNT:	/* How many bytes are queued? */
		blockcount = bytecount = 0;
		while (ipcblock)
		{
			blockcount++;
			bytecount += ipcblock->blocklen;
			ipcblock = ipcblock->next;
		}
		retval = (mode == IPCB_BLOCKCOUNT) ? blockcount : bytecount;
		break;
	case IPCB_FETCHWAIT:	/* Get next block, sleep until one available */
		while (!(ipcblock = proc->ipcblock))
		{
			syssafe--;
			if (SNOOZE(ipcfetch, &proc->ipcblock, 0))
			{	/* Signalled: break out */
				retval = 0;
				break;
			}
			SLEEP(0);
			syssafe++;
		}
		/* Fall through */
	case IPCB_FETCHNEXT:	/* Return next block, return 0 if none */
		if (ipcblock)
			proc->ipcblock = ipcblock->next;	/* Unlink */
		retval = (long)ipcblock;
		break;
	default:
		retval = BEC_BADARG;
		break;
	}
	return retval;
}

IPCBLOCK *ipcfetch(IPCBLOCK **addr)
{
	return *addr;
}
