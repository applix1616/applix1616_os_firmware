#ifndef FILEIO_H
#define FILEIO_H

extern struct fcb **ppfcbs;


extern int incuser(int fd, int newpid,char *name);
extern int bmbin(int size);
extern int tryclose(int fd);
extern int fixfd(uint *pfd);
extern int fileready(int fd,int issgetc);
extern void closeprocfiles(int pid);
extern void fixOpenFiles(ushort bdvrnum, ushort olddirb,ushort oldnblks,ushort newdirb);
extern int dorename(register char *pathname,register char *newpathname,int canmove);

#endif
