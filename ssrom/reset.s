|
| ROM startup code
|
	.file		"reset.s"

#include "asm68k.h"

	.extern _asbootup
	.global	coldstart, _initsp

coldstart:
_initsp:
		.long	0x300		| Initial SP
		.long	_asbootup	| Entry point

	.end
