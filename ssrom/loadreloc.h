/*
 * Binary program loading code
 */
#ifndef LOADRELOC_H
#define LOADRELOC_H


extern int loadreloc(int ifd,int addr,char *fname,int copyheader);
extern int dofload(char *path,int gmmode,int verbose,int *pstackspace);

#endif
