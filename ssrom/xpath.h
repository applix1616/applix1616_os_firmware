/* TABS8 NONDOC
 * Memory copy of the cached execution paths
 */

struct xpath
{
	char xpname[MAXPATHLENGTH];	/* Directory name */
	struct xplist
	{
		char name[FNAMESIZE-5];	/* Name of the file (minus extent) */
		char type;		/* Index into suffixes */
	} *xplist;
	int ninlist;			/* Number of entries in xplist */
};
