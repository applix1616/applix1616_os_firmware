#ifndef MEMFNS_H
#define MEMFNS_H




extern int searchdown(void *address,int tabindex);
extern void setbits(void *table, int mask, int nbits,int value);
extern int bitrun(void *addr, int maxbytes);
extern void invlongs(void *addr,int nlongs);
extern void andlongs(void *saddr,void *daddr,int nlongs);

#endif
