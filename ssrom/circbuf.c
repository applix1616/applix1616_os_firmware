/* TABS4
 * Circular buffer managing functions (chars only)
 */

#include <types.h>
#include <syscalls.h>
#include <storedef.h>
#include <hwdefs.h>
#include <storedef.h>
#include <exception.h>
#include <spl.h>
//#include <splfns.h>
#include "chario.h"
#include "debug.h"
#include "serial.h"
#include "newmem.h"
#include "circbuf.h"



/* The sort of random number */
uint randnum;

/* Serial I/O buffers */
uchar scca_ibuf[SER_RXBUFSIZE];
uchar sccb_ibuf[SER_RXBUFSIZE];
uchar scca_obuf[SER_TXBUFSIZE];
uchar sccb_obuf[SER_TXBUFSIZE];
uchar ta_buf[KB_BUFSIZE];		/* K/B buffer */
uchar cent_obuf[CENT_BUFSIZE];		/* Centronics O/P buffer */

extern struct chardriver *chardrivers;

/*
 * Put a char in the buffer (no overflow check)
 * If dev >= 0, it is an input device; check for SIGINT, SIGHUP
 */
void put_cbuf(CIRCBUFFER *cb,int ch,int dev)
{
	volatile register uint *hptr;
	register struct chardriver *chardriver;

	if ((dev >= 0) && !(chardriver = chardrivers + dev)->rawmode)
	{
	 	chardriver->rxcount++;
		if ((ch == chardriver->xonchar) || (ch == chardriver->xoffchar))
		{	/* Much stuffing about so xonchar & xoffchar can be same char */
			if ((ch == chardriver->xonchar) && chardriver->xoffed)
			{	/* Restarting */
				chardriver->xoffed = 0;
				/* If it was serial channel, restart Tx */
				if (dev == 1 || dev == 2)
				{
					int s;
					s = SCC_SPL();
					do_tx(dev - 1);
					_splx(s);
				}
			}
			else if (ch == chardriver->xoffchar)
				chardriver->xoffed = 1;
			return;
		}

		if (ch == chardriver->sigintchar)
		{
		  CDMISC(dev, CDM_SENDSIGINT,0,0,0);
			if (chardriver->modebits & CDMB_RXSIGPURGE)
			  CDMISC(dev, CDM_RXPURGE,0,0,0);
			if (chardriver->modebits & CDMB_TXSIGPURGE)
			  CDMISC(dev, CDM_TXPURGE,0,0,0);
			return;
		}
		else if (ch == chardriver->resetchar)
		  WARMBOOT(BR_SOFTRESET);
	}

	hptr = &(cb->head);
	cb->buf[(*hptr)++] = ch;
	if (*hptr >= cb->cbsize)
		*hptr = 0;
}

/* Return a char from the buffer */
char get_cbuf(CIRCBUFFER *cb)
{
	volatile register uint *tptr;
	register uchar ch;
	int snoozeptrdiff();

	tptr = &(cb->tail);
	while (cb->head == *tptr)
	{
		randnum++;		/* Spin the random number */
		SNOOZE(snoozeptrdiff, &cb->head, tptr);
	}
	ch = cb->buf[(*tptr)++];
	if (*tptr >= cb->cbsize)
		*tptr = 0;
	return ch;
}

int snoozeptrdiff(uint *p1,uint *p2)
{
	return *p1 - *p2;
}

/*
 * Return number of chars in buffer.
 */
int cb_bnum(CIRCBUFFER *cb)
{
	register int nbuf;

	if ((nbuf = cb->head - cb->tail) < 0)
		return cb->cbsize + nbuf;
	else
		return nbuf;
}

/*
 * Return number of char positions free in buffer
 */

int cb_bfree(CIRCBUFFER *cb)
{
	register int nbuf;

	if ((nbuf = cb->tail - cb->head) <= 0)
		return cb->cbsize + nbuf - 1;
	else
		return nbuf - 1;
}

/*
 * Wait until there is some room in a buffer
 */
void wroom_cbuf(CIRCBUFFER *cb)
{
	int cb_bfree();

	while (!cb_bfree(cb))
	{
		randnum++;		/* Spin the random number */
		SNOOZE(cb_bfree, cb, 0);
	}
}

/* Serial I/O structures and default buffers */
extern struct circ_buf scca_icbuf, scca_ocbuf, sccb_icbuf, sccb_ocbuf;
#ifdef A1616
extern struct circ_buf kb_cbuf;
extern  struct circ_buf cent_ocbuf;	/* Centronics storage */
#endif

/*
 * Zap circular buffer pointers so they are not freed accidently
 */

void cbuf_init(void)
{
	scca_icbuf.freeable = 0;
	sccb_icbuf.freeable = 0;
	scca_ocbuf.freeable = 0;
	sccb_ocbuf.freeable = 0;
#ifdef A1616
	kb_cbuf.freeable = 0;
	cent_ocbuf.freeable = 0;
#endif
}

static void maybecbfree(struct circ_buf *cbp)
{
	if (cbp->freeable)
	{
		dofreemem(cbp->buf);
		cbp->freeable = 0;
	}
}

/*
 * System call 81: Reassign an I/O device circular buffer area and size
 *
 * Alter char device circular buffer size.
 * If buf_addr == 0, set buffer to internal default
 * If buf_addr == 1, set buffer to buf_len bytes, allocated in mode 1
 * If buf_addr == -1, return pointer to circbuf structure defined by 'bnum'
 * Otherwise, use buffer at *buf_addr, length 'buf_len'.
 */

/* 0=RxA 1=TxA 2=RxB 3=TxB 4=centron 5=K/B */
/* The new buffer (0 = default) */
long _new_cbuf(int bnum,uchar *buf_addr,uint buf_len)
{
	register struct circ_buf *this_cbuf;
	register uchar *def_buf;
	register uint def_size;
	long retval;
	int s;

	retval = 0;

	s = SCC_SPL();
	switch (bnum)
	{
	case 0:			/* RxA */
		this_cbuf = &scca_icbuf;
		def_buf = scca_ibuf;
		def_size = SER_RXBUFSIZE;
		break;
	case 1:			/* TxA */
		this_cbuf = &scca_ocbuf;
		def_buf = scca_obuf;
		def_size = SER_TXBUFSIZE;
		break;
	case 2:			/* RxB */
		this_cbuf = &sccb_icbuf;
		def_buf = sccb_ibuf;
		def_size = SER_RXBUFSIZE;
		break;
	case 3:			/* TxB */
		this_cbuf = &sccb_ocbuf;
		def_buf = sccb_obuf;
		def_size = SER_TXBUFSIZE;
		break;
#ifdef A1616
	case 4:
		this_cbuf = &cent_ocbuf;
		def_buf = cent_obuf;
		def_size = CENT_BUFSIZE;
		break;
	case 5:
		this_cbuf = &kb_cbuf;
		def_buf = ta_buf;
		def_size = KB_BUFSIZE;
		break;
#endif
	default:
		retval = -1;		/* Bad value */
		goto out;
	}

	if (!buf_addr)		/* Reset default */
	{
		this_cbuf->cbsize = def_size;
		maybecbfree(this_cbuf);
		this_cbuf->buf = def_buf;
	}
	else if (buf_addr == (uchar *)1)
	{	/* Allocate buffer, length in buf_len */
		uchar *p;

		p = (uchar *)getmem1(buf_len);
		if ((int)p > 0)
		{	/* Alloc OK */
			this_cbuf->cbsize = buf_len;
			maybecbfree(this_cbuf);
			this_cbuf->buf = p;
			this_cbuf->freeable = 1;
		}
		else
			retval = (long)p;
	}
	else if (buf_addr == (uchar *)-1)
	{	/* Return pointer to structure */
		retval = (long)this_cbuf;
		goto out;
	}
	else			/* New assignment */
	{
		maybecbfree(this_cbuf);
		this_cbuf->cbsize = buf_len;
		this_cbuf->buf = buf_addr;
	}

	this_cbuf->head = this_cbuf->tail = 0;	/* Empty the buffer */
out:
	_splx(s);
	return retval;
}

/*
 * System call 30: return the random number
 */

uint _getrand(void)
{
	return randnum;
}
