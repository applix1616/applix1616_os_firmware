		.text
/*
* Handy assembler routines
*/
#include "asm68k.h"

		.global	_reset,reset
reset:
_reset:
		reset
		rts

		.global	_spl0,_spl1,_spl2,_spl3,_spl4,_spl5,_spl6,_spl7
_spl0:
		clr.w	d1
		bra.b	splcomn
_spl1:
		move.w	#$100,d1
		bra.b	splcomn
_spl2:
		move.w	#$200,d1
		bra.b	splcomn
_spl3:
		move.w	#$300,d1
		bra.b	splcomn
_spl4:
		move.w	#$400,d1
		bra.b	splcomn
_spl5:
		move.w	#$500,d1
		bra.b	splcomn
_spl6:
		move.w	#$600,d1
		bra.b	splcomn
_spl7:
		move.w	#$700,d1

splcomn:
		move.w	sr,d0		/* Return old level in d0 */
spldoit:
		and.l		#$700,d0
		cmp.w		d1,d0			/* d0 - d1: C set if d1 > d0 */
		bcc.b	nodo
		bset		#13,d1		/* Remain in supervisor state */
		move.w	d1,sr
nodo:
		rts

/*
* Indirectly set new level, if higher than present one.
*/
		.global	_newspl, newspl
newspl:
_newspl:
		move.l	4(a7),d1
		bra.b	splcomn

		.global	_splx		/* Set priority as argument */
_splx:
		move.l 4(a7),d0	/* Get the new level */
		and.l	#$700,d0	/* Isolate the level */
		bset	#13,d0		/* Maintain supervisor state */
		move.w	d0,sr		/* Do it */
		rts

/*
* Delay a multiple of 100 microseconds
*/

		.global	_delay

_delay:
		move.l	4(a7),d0		/* Delay length */
dloop1:
		move.w	#73,d1
dloop2:
		dbf	d1,dloop2			/* 10;n cycles */
		subq.w	#1,d0			/* 4 cycles */
		bne.b	dloop1		  /* 10 cycles */
		rts
