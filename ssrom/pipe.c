/*
 * Pipe functions
 */

#include <types.h>
#include <syscalls.h>
#include <files.h>

#include <blkerrcodes.h>
#include <signal.h>

#include "fcb.h"
#include "blkio.h"
#include "filefns.h"
#include "newmem.h"
#include "pipe.h"

/* File control blocks */
extern struct fcb **ppfcbs;
extern int curpid;

extern char *room1for();

/*
 * System call 132: Create a pipe
 */

int _pipe(int *ptr)
{
	register int retval;
	register int infcbnum, outfcbnum;
	register struct fcb *infcbptr, *outfcbptr;
	register struct apipe *apipe;
	char buf[20];

	fsentry(0);		/* Protect ourselves */
	if ((infcbnum = get_fcb(0, 1)) < 0)
	{
		retval =(int)infcbnum;
		goto err;
	}
	if ((outfcbnum = get_fcb(0, 1)) < 0)
	{
		retval = (int)outfcbnum;
		goto err2;
	}
	infcbptr = ppfcbs[infcbnum];
	SPRINTF(buf, "<Ipipe %d>", infcbnum);
	infcbptr->file_name = room1for(buf);

	outfcbptr = ppfcbs[outfcbnum];
	SPRINTF(buf, "<Opipe %d>", outfcbnum);
	outfcbptr->file_name = room1for(buf);

	infcbptr->pipefcb = (struct pipefcb *)getmem1(sizeof(struct pipefcb));
	if ((int)infcbptr->pipefcb < 0)
	{
		retval = (int)infcbptr->pipefcb;
		goto err3;
	}
	outfcbptr->pipefcb = (struct pipefcb*)getmem1(sizeof(struct pipefcb));
	if ((int)outfcbptr->pipefcb < 0)
	{
		retval = (int)outfcbptr->pipefcb;
		goto err4;
	}
	if ((retval = getmem1(sizeof(struct apipe))) < 0)
		goto err5;
	apipe = (struct apipe *)retval;
	apipe->flags = 0;
	infcbptr->pipefcb->apipe = apipe;
	infcbptr->pipefcb->isoutput = PIPEINPUT;
	outfcbptr->pipefcb->apipe = apipe;
	outfcbptr->pipefcb->isoutput = PIPEOUTPUT;
	apipe->head = 0;
	apipe->tail = 0;
	ptr[0] = outfcbnum + NIODRIVERS;
	ptr[1] = infcbnum + NIODRIVERS;
	retval = 0;
	goto ok;

err5:	dofreemem(outfcbptr->pipefcb);
err4:	dofreemem(infcbptr->pipefcb);
err3:	free_fcb(outfcbnum, 1);
err2:	free_fcb(infcbnum, 1);
err:
ok:	fsexit(0);
	return retval;
}

/*
 * Close one end of a pipe. We know that only one process owns it
 * and that it is a valid pipe.
 * Come here after doing one fsentry()
 */

int pipeclose(int fcbnum)
{
	register int retval;
	register struct fcb *fcbptr;
	register struct pipefcb *pipefcb;
	register struct apipe *apipe;

	fcbptr = ppfcbs[fcbnum];
	pipefcb = fcbptr->pipefcb;
	apipe = pipefcb->apipe;
	if (pipefcb->isoutput & PIPEOUTPUT)
	{	/* Closing output end */
		if (apipe->flags & PIPEOUTPUT)
		{	/* Output already closed */
			retval = BEC_BADARG;
			goto err;
		}
		apipe->flags |= PIPEOUTPUT;	/* Mark output as closed off */
	}
	else if (pipefcb->isoutput & PIPEINPUT)
	{	/* Closing output end */
		if (apipe->flags & PIPEINPUT)
		{	/* Input already closed */
			retval = BEC_BADARG;
			goto err;
		}
		apipe->flags |= PIPEINPUT;	/* Mark input as closed off */
	}
	else
	{
#ifdef notdef
		conprintf("pipeclose(%d): panic\r\n", fcbnum);
#endif
		retval = BEC_BADARG;
		goto err;
	}
	dofreemem(pipefcb);
	free_fcb(fcbnum, 1);
	if (apipe->flags == (PIPEINPUT|PIPEOUTPUT))
	{	/* Fully closed */
		dofreemem(apipe);
	}
	retval = 0;
err:
	return retval;
}

/*
 * Read from a pipe. We know it is a valid FCB
 * Come here after doing one fsentry()
 */

int piperead(int fcbnum,uchar *buf,uint nbytes)
{
	register int retval;
	register int nread;
	register struct apipe *apipe;
	register uchar *data;
	struct pipefcb *pipefcb;
	struct fcb *fcbptr;

	fcbptr = ppfcbs[fcbnum];
	pipefcb = fcbptr->pipefcb;
	apipe = pipefcb->apipe;
	nread = nbytes;

	if (!(pipefcb->isoutput & PIPEOUTPUT))
	{	/* Read from wrong end! */
		retval = BEC_BADFD;
		goto err;
	}
	data = apipe->data;
	/* We can read */
	while (nbytes)
	{
		while (apipe->head == apipe->tail)
		{	/* Did writer close the input? */
			if (apipe->flags & PIPEINPUT)
				goto closed;
			fsexit(0);
			SLEEP(0);
			fsentry(0);
		}
		*buf++ = data[apipe->tail++];
		apipe->tail %= PIPEBUFSIZE;
		nbytes--;
	}
closed:	retval = nread - nbytes;
err:	fsexit(0);
	return retval;
}

/*
 * Write to a pipe. We know it is a valid FCB
 * Come here after doing one fsentry()
 */

int pipewrite(int fcbnum,uchar *buf,uint nbytes)
{
	register int retval;
	register int nwritten;
	register struct apipe *apipe;
	register uchar *data;
	register uchar *flags;
	struct fcb *fcbptr;
	struct pipefcb *pipefcb;

	fcbptr = ppfcbs[fcbnum];
	pipefcb = fcbptr->pipefcb;
	apipe = pipefcb->apipe;
	nwritten = nbytes;

	if (!(pipefcb->isoutput & PIPEINPUT))
	{	/* Wrote to wrong end! */
		retval = BEC_BADFD;
		goto err;
	}
	data = apipe->data;
	flags = &apipe->flags;
	/* We can write */
	while (nbytes--)
	{
		while (((retval = (*flags & PIPEOUTPUT)) == 0) &&
			((apipe->head + 1) % PIPEBUFSIZE) == apipe->tail)
		{
			fsexit(0);
			SLEEP(0);
			fsentry(0);
		}
		if (retval)
		{
			retval = BEC_PIPECLOSED;
			goto err;
		}
		data[apipe->head++] = *buf++;
		apipe->head %= PIPEBUFSIZE;
	}
/*	retval = nwritten;*/
	retval = 0; /* Stupid return value needed by old OS compatibility */
err:	fsexit(0);
/* Experiment */
	if (retval == BEC_PIPECLOSED)
		SIGSEND(GETPID(), SIGPIPE, 0);
	return retval;
}
