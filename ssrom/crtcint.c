/*
 * Init CRTC
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
//#include <splfns.h>

#include "movmem.h"
#include "psprintf.h"
#include "crtcint.h"



uchar crtc_tab[] =
{
  119,		/* Horizontal total: 15625 Hz line rate */
  80,		/* Horizontal displayed */
  96,		/* Hsync position */
  10,		/* Hsync width */
  77,		/* Vertical total: 312/4 - 1 */
  0,		/* Vertical total adjust */
  50,		/* Vertical displayed: 200/4 char lines */
  63,		/* Vsync position */
  0,		/* No interlace */
  3,		/* Max scan line address: 4 rows/char */
  0x20,		/* No cursor */
  0,		/* Cursor end */
  0,		/* Start address high */
  0,		/* Start address low */
  (uchar)(CRTC_SAVE/256),	/* Cursor register high */
  (uchar) CRTC_SAVE	/* Cursor register low */
};

uchar crtcimage[14];
uchar usercrtcimage[14];	/* What the user wants */
uchar usercrtcset;		/* Flag: use user's settings */


/*
 * Calculate & set tick rate
 */

void setclock(uchar *crtc)
{
	int usecs;

	/* Vertical frequency */
	usecs = (crtc[4] * 4 + crtc[5] + 1) *
			(crtc[0] + 1) * 8 / 15;
	trap7(83, (char *)0, 1, usecs, 0);	/* GETTDSTR() */
}


int badset(void)
{
	return eprintf("Bad setting\r\n");
}

/*
 * System call 140: CRTC control.
 * Mode = 0: program it from *ptr
 * Mode = 1: read settings to *ptr
 * Mode = 2: set default settings
 */

void _crtc_init(int mode,uchar *ptr)
{
	register uint rptr;

	if (mode == 2)
		ptr = usercrtcset ? usercrtcimage : crtc_tab;

	if (mode == 0 || mode == 2)
	{
		movmem(ptr, crtcimage, 14);
		for (rptr = 0; rptr < 14; rptr++)
		{
			*CRTC_ADDR = rptr;
			*CRTC_DATA = *ptr++;
		}
	}
	if (mode == 1)
		movmem(crtcimage, ptr, 14);
	if (mode == 2)
		setclock(crtcimage);
}

/*
 * Usage: vmode Hfreq Vfreq Hadjust Vadjust
 */

int vmode(int argc, int *argval)
{
	register int hfreq, vfreq;
	int hadjust, vadjust;
	int htotal, hspos, hswidth;

	int vtlines, vspos;
	int vtot, vtadj;

	hfreq = argval[1];
	vfreq = argval[2];
	hadjust = argval[3];
	vadjust = argval[4];

	if (hfreq < 14000 || hfreq > 30000)
		return badset();

	if (vfreq < 40 || vfreq > 100)
		return badset();

	if (argc > 3)
	{
		if ((hadjust < -20) || (hadjust > 20))
			return badset();
	}
	else
		hadjust = 0;

	if (argc > 4)
	{
		if ((vadjust < -20) || (vadjust > 20))
			return badset();
	}
	else
		vadjust = 0;

	/* Horizontal count */
	htotal = (15000000 / 8) / hfreq;
	hspos = (htotal + 80) / 2 - 5 + hadjust;

	/* Horizontal sync width */
	hswidth = 10;
	if ((htotal - 80) < 11)
		hswidth = ((htotal - 80) * 2) / 3;

	/* Vertical line count */
	vtlines = 15000000 / 8 / htotal / vfreq;
	vtot = vtlines / 4;
	vtadj = vtlines % 4;

#ifdef notdef
	PRINTF("htotal = %d, hspos = %d, hswidth = %d\n",
			htotal, hspos, hswidth);
	PRINTF("vtlines = %d, vtot = %d, vtadj = %d\n",
			vtlines, vtot, vtadj);
	PRINTF("usecs = %d\n", usecs);
#endif

	movmem(crtc_tab, usercrtcimage, 14);
	usercrtcset = 1;

	usercrtcimage[0] = htotal - 1;
	usercrtcimage[2] = hspos;
	usercrtcimage[3] = hswidth;
	usercrtcimage[4] = vtot - 1;
	usercrtcimage[5] = vtadj;
	usercrtcimage[7] = (vtot + 50) / 2 + vadjust;

	CRTC_INIT(0, usercrtcimage);
	setclock(usercrtcimage);
	vspos = vspos ;

	return 0;
}


