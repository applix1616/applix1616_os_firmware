
/*

*/

	.text
/*
 ;bitcount(addr, nbits)
 ;
 ;Count the number of set bits at *addr, MSB first. Stop after 'nbits' bits
*/

#include "asm68k.h"


		.global	_bitcount, bitcount
bitcount:
_bitcount:
		movem.l	d2/d3,-(a7)	 /*; d2/d3 */
		move.l	12(a7),a0		/*; Table pointer*/
		move.l	16(a7),d1		/*; Bit counter*/
		moveq.l	#0,d0				/*; Count */
		subq.l	#1,d1
		bmi.b	done
mloop:
		moveq.l	#7,d2			/*; Bit index */
ml2:
		move.b	(a0)+,d3
		bne.b	not0
ml3:
		subq.l	#8,d1			/*; All zeroes */
		bpl.b	ml2
		bra.b	done
not0:
		cmp.b	#0xff,d3			/*; All ones? */
		bne.b	loop8
		addq.l	#8,d0			/*; Eight bits */
		bra.b	ml3
loop8:
		asl.b	#1,d3
		bcc.b	noadd
		addq.l	#1,d0
noadd:
		subq.l	#1,d1
		bmi.b	done
		dbf	d2,loop8
		bra.b	mloop

done:
		movem.l	(a7)+,d2/d3	/*; d2/d3 */
		rts
