/* TABS4 NONDOC
 * 68k monitor command executor
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <ctype.h>
#include <options.h>

#include "mondefs.h"
#include "scc.h"
#include "files.h"
#include "chario.h"
#include "blkerrcodes.h"
#include "spspace.h"
#include "envbits.h"
#include "exfile.h"
#include "assign.h"
#include "monexec.h"
#include "montabs.h"
#include "psprintf.h"
#include "monfns.h"
#include "prcrlf.h"
#include "setmem.h"
#include "ssdefs.h"
#include "movmem.h"
#include "systrap.h"
#include "srload.h"
#include "term.h"
#include "seditor.h"
#include "datetime.h"
#include "blockio.h"
#include "cantdo.h"
#include "newmem.h"
#include "process.h"
#include "proc.h"
#include "env.h"
#include "crtcint.h"
#include "ischdev.h"
#include "bindex.h"
#include "fcb.h"
#include "filefns.h"
#include "monfns.h"
#include "strnucmp.h"
#include "fileio.h"
#include "inbuilt.h"

#define SSASMOFFSET	0x26000	/* Hard-wired offset to SSASM */

int editing;			/* If true, EDIT has been invoked */
int miscmodes;			/* Various options */
int dummyoption;

extern char *dumpaddr;		/* Defined in MON.C */
extern uint stdin, stdout, stderr;
extern int enerrbeep;		/* Beep in eprintf() */
/* Flag the commands which must be checked for arg1 <= arg2 */
extern int execstackspace;	/* Default EXEC stack space */
extern int reslevel;
#ifdef notdef
extern int (*wildcomp)();
extern int defwildcomp();
#endif
extern int curpid, ouruid;
extern char is68010;

char *dowa(), *putinmem(), *skipwhit(), *room0for();

/* Flag the commands which must be checked for arg1 <= arg2 */
extern char chk1le2[];


/* Monitor initialisations */
void mon_init(void)
{
	editing = 0;
	ALIAS(AL_RMALL, 0);		/* Scrap all aliases */
	if (!reslevel)
	{
		miscmodes = 0;
		dummyoption = 0;
		execstackspace = BIN_STACKSPACE;
		assign_init();
	}
	dumpaddr = (char *)0x4000;
#ifdef notdef
	wildcomp = defwildcomp;
#endif
	xp_init();
}

int inbuilt(int argc,register char *argv[],char *argtype,uint *argval)
{
	register uchar *ptr, *ptr2;
	register uint i;
	register uint val, av1;
	register uint av2;
	register int retval;
	register char *as1;
	uint av3;
	ushort argt, mcnum, docrlf;
	struct scc_prog sccp;
	struct blkdriver *bdptr;
	uchar dt[8];			/* For date/time init */
	uchar tdbuf[22];
	char vb2;
	char __verbose;

	static uchar cchars[] = "NULSOHSTXETXEOTENQACKBELBS HT LF VT FF CR SO SI DLEDC1DC2DC3DC4NAKSYNETBCANEM SUBESCFS GS RS US SPC";

	retval = 0;
	__verbose = verbose();

	if (argc >= MAXMONPAR)
		argc = MAXMONPAR;

	mcnum = searchib(argv[0]);

/* Now check the syntax */
	for (i = 0; i < MAXMONPAR - 1; i++)
	{	/* Extract the 4 syntax bits */
		val = csyntax[mcnum][(i > 7) ? 3 : (i>>1) >> (i & 1 ? 0 : 4)];
		argt = (uchar)argtype[i + 1];		/* The type of this argument */

/* Check that the argument type was expected */
		if (!(argt & val))
		{
syntaxerror:
			dohelp(argv[0], STDERR);
			if (enerrbeep && envbits(ENVB_ERRBEEP|ENVB_IBBEEP) == (ENVB_ERRBEEP|ENVB_IBBEEP))
					PUTC(STDERR, 7);
			goto badcmd;
		}
	}

	av1 = argval[1];		/* Speed things up */
	av2 = argval[2];
	av3 = argval[3];
	as1 = argv[1];

/* See if we need to check that arg1 <= arg2 */
	if ((argc > 2) && chk1le2[mcnum] && (av1 > av2))
	{
		eprintf("Arg1 > Arg2\r\n");
		goto badcmd;
	}

	retval = 0;
	vb2 = (__verbose && (argc > 2));

/* Now execute the command */
	switch(mcnum)
	{
	case 0:		/* mdb */
		i = 1;
		goto dodm;
	case 1:		/* mdw */
		i = 2;
		goto dodm;
	case 2:		/* mdl */
		i = 4;
dodm:		if (argc == 1)
			av1 = (int)dumpaddr;
		dm((uchar *)av1,(uchar *) ((argc == 3) ? av2 : (av1 + 127)), i);
		prcrlf();
		break;
	case 3:		/* mrb */
		i = 1;
		goto dorb;
	case 4:		/* mrw */
		i = 2;
		goto dorb;
	case 5:		/* mrl */
		i = 4;
dorb:		for ( ; ; )
		{
			printmem((char *)av1, i);
			PUTCHAR('\r');
		}
	case 6:		/* mwb */
		wm(argc, argv, argval, 1);
		break;
	case 7:		/* mww */
		wm(argc, argv, argval, 2);
		break;
	case 8:		/* mwl */
		wm(argc, argv, argval, 4);
		break;
	case 9:		/* mwa */
		wm(argc, argv, argval, 0);
		break;
	case 10:	/* mfb */
		bsetmem((char *)av1, av3, av2 - av1 + 1);
		break;
	case 11:	/* mfw */
		if ((is68010 > 1) || !(av1 & 1))
			wsetmem((short *)av1, av3, (av2 - av1 + 2) >> 1);
		else
			goto odd;
		break;
	case 12:	/* mfl */
		if ((is68010 > 1) || !(av1 & 1))
			lsetmem((void *)av1, av3, (av2 - av1 + 4) >> 2);
		else
		{
odd:		eprintf("%s is odd\r\n");	//, as1);
			goto reterr;
		}
		break;
	case 13:	/* mfa */
		ptr = (uchar *)av1;	/* Start */
		do
		{
			ptr = (uchar *)dowa(ptr, argv[3], 1);
		}
		while (ptr <= (uchar *)av2);
		break;
	case 14:	/* mmove */
		movmem((void *)av1,(void *) av3, av2 - av1 + 1);
		break;
	case 15:	/* mcmp */
		ptr = (uchar *)av1;
		ptr2 = (uchar *)av3;
		for (av3 = 0; av3 <= (av2 - av1); av3++)
		{
			if (*ptr != *ptr2)
			{
				PRINTF("%08X-%02X %08X-%02X\r\n",
					ptr, *ptr, ptr2, *ptr2);
			}
			ptr++;
			ptr2++;
		}
		break;
	case 16:	/* msearch */
		ptr = (uchar *)av1;
		for (av3 = 0; av3 <= (av2 - av1); av3++)
		{
			for (val = 3; ((val < argc) &&
				(ptr[val-3] == (uchar)argval[val])); val++)
				;
			if (val == argc)
				PRINTF("#%08X ", ptr);
			ptr++;
		}
		prcrlf();
		break;
	case 17:	/* base */
		for (i = 1; i < argc; i++)
		{
			val = argval[i];
			PRINTF("%-8X = .%-10d = %%%b = 0%o\r\n",
				val, val, val, val);
		}
		break;
	case 18:	/* expr */
		retval = expr(argc, argv, argval);
		break;
	case 19:	/* go */
		retval = entry((void *)av1, argc - 2,(int) argv + 2,(int) argtype + 2,(int) argval + 2,0,0);
		PRINTF("Return: $%08X\r\n", retval);
		break;
	case 20:	/* srec */
		retval = load_srec(argc, argv);
		break;
	case 21:	/* help */
		retval = help(argc, argv);
		break;
	case 22:	/* move */
		retval = move(argc, argv, 0);
		break;
	case 23:	/* mwaz */
		wm(argc, argv, argval, 5);
		break;
	case 24:	/* cio */
		retval = docio(argc, av1);
		break;
	case 25:	/* terma */
		retval = term(dn_sa, argc == 2);
		break;
	case 26:	/* termb */
		retval = term(dn_sb, argc == 2);
		break;
	case 27:	/* serial */
		av1 -= 10;
		if (av1 > 1)
		{
			eprintf("Bad channel\r\n");
			goto reterr;
		}
		if (argc == 2)
		{
			movmem((void *)PROG_SIO(av1, 0), &sccp, sizeof(sccp));
			PRINTF(".%d %d %d %d %d\r\n", sccp.brate,
				sccp.rxbits + 5, sccp.txbits + 5,
				sccp.parity, sccp.stopbits);
		}
		else
		{
			if (argc != 7 && argc != 10)
				goto syntaxerror;
			sccp.brate = av2;
			sccp.rxbits = av3 - 5;
			sccp.txbits = argval[4] - 5;
			sccp.parity = argval[5];
			sccp.stopbits = argval[6];
			if (PROG_SIO(av1, &sccp) < 0)
			{
				eprintf("Invalid setting\r\n");
				goto reterr;
			}
			if (argc == 10)		/* Set xon/xoff */
				CDMISC(av1 + 1, CDM_SETSFC, argval[7], argval[8], argval[9]);
		}
		break;
	case 28:	/* fkey */
		if (--av1 <= 9)
			DEF_FK(av1, argv[2]);
		else
		{
			eprintf("Invalid FK number\r\n");
			goto reterr;
		}
		break;
	case 29:	/* setdate */
		for (i = 0; i < 6; i++)
			dt[i] = argval[i+1];
		dt[6] = 0;
		if (SETDATE(dt))
			retval = eprintf("Bad date/time\r\n");
		/* Fall through to print it out */
	case 30:	/* date */
		PRINTF("%s\r\n", GETTDSTR(tdbuf));
		break;
	case 31:	/* copy */
		retval = move(argc, argv, 1);
		break;
	case 32:	/* syscall */
		PRINTF(__verbose ? "Call returns $%08X\r\n" : "%08x",
		 retval = trap7(av1, av2, av3, argval[4], argval[5], argval[6]));
		break;
	case 33:	/* delete */
		while (*(++argv))
		{
			ptr = (uchar *)argv;
			if (vb2)
				PRINTF("delete(%s)\r\n", ptr);
			if ((retval = UNLINK(ptr)))
				cantunlink((char *)ptr, retval);
		}
		break;
	case 34:	/* cat */
		retval = (argc == 1) ? docio(argc, -1) : cat(argc, argv);
		break;
	case 35:	/* rename */
		if ((retval = RENAME(as1, argv[2])) < 0)
			cantrename(as1, retval);
		break;
	case 36:	/* dir */
		retval = dir(argc, argv[0], 0);
		break;
	case 37:	/* msave */
		retval = msave(argv, argval);
		break;
	case 38:	/* mload */
		retval = mload(argc, argv,(uint) argval, 0,0);
		if (retval >= 0)
			retval = 0;
		break;
	case 39:	/* tsave */
		retval = tsave(argc, argv,argval, 0);
		ding();
		break;
	case 40:	/* tload */
		retval = tload(argc, argv, 0);
		ding();
		break;
	case 41:	/* dirs */
		retval = dir(argc, argv[0], 1);
		break;
	case 42:	/* itload */
		i = 0;
tloadloop:	for ( ; ; )
		{
			tload(argc, argv, i);
			ding();
		}
	case 43:	/* cd */
		retval = dochdir(argc, argv);
		break;
	case 44:	/* echo */
		docrlf = 1;
		argv++;
		/* Look for -n option */
		if ((argc > 1) && (!strcmp(as1, "-N") || !strcmp(as1, "-n")))
		{
			argv++;
			docrlf = 0;
		}
		while (*argv)
		{
			PRINTF("%s", *argv++);
			if (*argv)
				PUTCHAR(' ');	/* Prevent a trailing blank */
		}
		if (docrlf)
			prcrlf();
		break;
	case 45:	/* mkdir */
		retval = domkdir(argc, argv, (uint) argval);
		break;
	case 46:	/* edit */
		editing = 1;
		retval = sedit(argc,(uchar **) argv);
		editing = 0;
		break;
	case 47:	/* ssio */
#ifdef notdef
		if (stdin < NIODRIVERS)	/* Char device only allowed */
			stdin |= 0x8000; /* Make current I/O devices permanent */
		if (stdout < NIODRIVERS)
			stdout |= 0x8000;
		if (stderr < NIODRIVERS)
			stderr |= 0x8000;
#endif
		break;
	case 48:	/* quit */
		retval = QUITCODE;
		break;
	case 49:	/* tarchive */
		retval = tsave(argc, argv,0, 1);
		ding();
		break;
	case 50:	/* pause */
		SLEEP(av1);
		break;
	case 51:	/* tverify */
		i = 1;
		goto tloadloop;
	case 52:	/* ascii */
		{
			int wanthigh;
			wanthigh = 0;
			ptr2 = (uchar *)"$%02X:";
			if (argc == 2)
			{
				if (*as1 <= 'Z')
					wanthigh = 128;
				if (toupper(*as1) == 'D')
					ptr2 = (uchar *)"%3d:";
			}
			for (i = 0; i < 128; i++)
			{
				val = (i >> 3) + ((i & 7) << 4);
				if (val > 127)
					PRINTF("         ");
				else
				{
					PRINTF(ptr2, val + wanthigh);
					if ((val + wanthigh) <= ' ')
					{
						ptr = cchars + val * 3;
						PRINTF("%c%c%c  ", *ptr, *(ptr + 1),
							*(ptr + 2));
					}
					else
						PRINTF(" %c   ", val + wanthigh);
				}
				if ((i & 7) == 7)
					prcrlf();
			}
		}
		break;
	case 53:	/* time */
		val = GET_TICKS();	/* Current */
		retval = AEXECA(argv + 1, 0);
		val = GET_TICKS() - val;	/* Elapsed ticks */
		FPRINTF(STDERR, "Execution time: %s\r\n", tosecs((char *)tdbuf, val));
		break;
	case 54:	/* xpath */
		retval = xpath(argc, argv[0]);
		break;
	case 55:	/* option */
		if (argc == 2)
			PRINTF("%d\r\n", OPTION(OPT_READ|av1, 0));
		else
		{
			if ((retval = OPTION(av1, av2)) < 0)
				eprintf("%d: bad option\r\n"); //, av1);
		}
		break;
	case 56:	/* volumes */
		prcrlf();
		for (i = 0; i < NBLKDRIVERS; i++)
		{
			if (newdisk(i))
				continue;

			if ((int)(bdptr = (struct blkdriver *)FIND_BDVR(i)) >= 0)
			{	/* Device there */
				PRINTF("%s: %s: %-4d blocks, %sremovable\r\n",
					bdptr->devname,
					bdptr->rootblock.rootdir.file_name,
					bdptr->rootblock.nblocks,
					bdptr->rootblock.removable ? "" : "not ");
			}
		}
		prcrlf();
		break;
	case 57:	/* touch */
		GETDATE(dt);
		while (*(++argv))
		{
			int x;
			ptr = (uchar *)argv;
			if (vb2)
				PRINTF("touch(%s)\r\n", ptr);
			if ((x = PROCESSDIR(ptr, dt, 6)) < 0)
				retval = cantdo((char *)ptr, x, "touch");
		}
		break;
	case 58:	/* filemode */
		if (av1 > 1)
			retval = eprintf("Bad mode: %d\r\n"); //, av1);
		else
		{
			int x;

			for (i = 3; i < argc; i++)
			{
				ptr = (uchar *)argv[i];
				if (__verbose && (argc > 4))
					PRINTF("%s\r\n", ptr);
				if ((x = PROCESSDIR(ptr, av2, 7 + av1)) < 0)
					retval = cantdo((char *)ptr, x, "modify");
			}
		}
		break;
	case 59:	/* type */
		for (i = 1; (retval <= 0) && (i < argc); i++)
		{
		  ptr =(uchar *)argv[i];
		  av1 = strlen((char *)ptr);       /* av1 = strlen(ptr = (uchar *)argv[i]);*/
		  if (vb2)
		    {	/* Display filename */
		      ptr2 = (uchar *)room0for(ptr);
		      bsetmem((char *)ptr2, '*', av1); /* Build asterisk string */
		      ptr2[av1] = '\0';		/* Terminate it */
		      PRINTF("%s\r\n%s\r\n%s\r\n\r\n", ptr2, ptr, ptr2);
		    }
		  ptr2 = (uchar *)getmem0(av1 + 10);
		  SPRINTF(ptr2, "cio <%s", ptr);
		  retval = EXEC(ptr2);
		}
		break;
#ifdef HAVE_SSASM
	case 60:	/* ssasm */
		{
			extern int start();	/* Start of ROMs */

			retval = ((int)(start)) + SSASMOFFSET;
			retval = entry(retval, argc, argv);	/* Jump to SSASM */
		}
		break;
#endif
	case 61:	/* assign */
		retval = DOASSIGN(argc, argv);
		break;
	case 62:	/* ps */
		retval = dops();
		break;
	case 63:	/* kill */
		retval = dokillwait(argc, argv, argtype, argval, 0);
		SLEEP(5);	/* Time for message to emerge */
		break;
	case 64:	/* wait */
		retval = dokillwait(argc, argv, argtype, argval, 1);
		break;
	case 65:	/* set */
		retval = set(argc, argv[0]);
		break;
	case 66:	/* term */
		retval = term(argv[1], argc == 3);
		break;
	case 67:	/* vmode */
		retval = vmode(argc,(int *)argval);
		break;
#ifdef ZMODEM
	case 68:	/* getz */
		retval = zm_main(1, argc,argv);
		break;
	case 69:	/* putz */
		retval = zm_main(0, argc, argv);
		break;
	case 70:	/* geta */
	case 71:	/* getb */
		retval = z_get_put(mcnum - 70, 0, "sz1616", argc, argv);
		break;
	case 72:	/* puta */
	case 73:	/* putb */
		retval = z_get_put(mcnum - 72, 1, "rz1616", argc, argv);
		break;
	case 74:	/* printa */
	case 75:	/* printb */
		retval = z_get_put(mcnum - 74, 1, "print1616", argc, argv);
		break;
	case 76:	/* boota */
	case 77:	/* bootb */
		retval = remote_boot(mcnum - 76, argc, argv);
		break;
#endif /* ZMODEM */
	default:
reterr:		retval = -1;
	}
	goto next;

badcmd:
	if (retval >= 0)
		retval = -1;		/* Error */
next:
	EXIT(retval);
	return(retval);
}

void ding(void)
{
	PUTCHAR(7);
}

/*
 * The 'move' and 'copy' command.
 *
 * move file file .. dir
 * move file file
 * If non-zero, copy command
 */

int move(int argc,char *argv[], int copymode)
{
	register int i, retval;
	register int finalretval = 0;
	register char *cp, *pathname, *lastpath;
	struct dir_entry odirent;	/* Output directory entry */
	int lastisdir, toch;

	lastpath = argv[argc - 1];
	if ((toch = ischdev(lastpath)))	/* Character device? */
		lastisdir = 1;
	else
	{
		if ((retval = FILESTAT(lastpath, &odirent)) < 0)
		{
			if (retval == BEC_NOFILE)
				lastisdir = 0;	/* New file */
			else
				goto yukk;	/* Some other error */
		}
		else
			lastisdir = odirent.statbits & DIRECTORY;
	}

	/* If multiple files specified, must be to a directory */
	if ((argc > 3) && !lastisdir)
	{
		retval = BEC_NOTDIR;
yukk:
		cantdo(argv[argc - 1], retval, copymode ? "copy to" : "move to");
		goto err;
	}

	for (i = 1; i < argc - 1; i++)
	{
		pathname = argv[i];
		if (lastisdir)
		{
			if (ischdev(pathname) && !toch)
			{
				eprintf("Can't copy %s to a directory\r\n");	//, pathname);
				continue;
			}
			if (toch)
				domove(pathname, lastpath, 1);
			else
			{
				cp = (char *)getmem0(strlen(pathname) +
					strlen(lastpath) + 2);
				SPRINTF(cp, "%s/%s", lastpath,
					bindex(pathname, '/'));
				retval = domove(pathname, cp, copymode);
			}
		}
		else
			retval = domove(pathname, argv[2], copymode);
		if (retval < 0)
			finalretval = retval;
	}
	retval = finalretval;
err:
	return retval;
}

/*
 * Low level stuff: decide whether to copy or rename, set up paths, etc
 */

int domove(char *from,char *to,int copymode)
{
	register char *fromfullpath, *tofullpath;
	register int retval;
	int mustcopy;
	char *errname;
	int toch, fromch;
	int dbdvrnum, locked, wasthere;
	struct dir_entry fdirent, tdirent, odirent;

	fromfullpath = (char *)GETFULLPATH(from, 0);
	if (!(toch = ischdev(to)))
		tofullpath = (char *)GETFULLPATH(to, 0);
	else
		tofullpath = to;

	fromch = ischdev(from);
	errname = (char *)0;
	if (!toch && !PATHCMP(fromfullpath, tofullpath))
	{
		retval = BEC_FNBAD;
		eprintf("%s and %s are identical\r\n"); //, from, to);
		goto err;
	}

	if (!fromch && ((retval = get_bdev(fromfullpath, 0)) < 0))
	{	/* Get source block device */
		cantopen(fromfullpath, retval);
		goto err;
	}
	mustcopy = retval;
	if (!toch && ((retval = get_bdev(tofullpath, 0)) < 0))
	{	/* Get destination block device */
		cantopen(tofullpath, retval);
		goto err;
	}
	mustcopy -= retval;	/* If zero, call rename */
	if (verbose())
		PRINTF("%s -> %s\r\n", fromfullpath, tofullpath);

	errname = fromfullpath;
	if ((retval = FILESTAT(fromfullpath, &fdirent)) < 0)
		goto err;

	wasthere = (!toch && (FILESTAT(tofullpath, &odirent) >= 0));
	locked = fdirent.statbits & LOCKED;
	if (mustcopy || copymode || fromch)
	{
		int ifd, ofd;

		/* Read the source directory entry */
		if (!toch)
		{
			dbdvrnum = get_bdev(tofullpath, 0);
			if (BDMISC(dbdvrnum, MB_FREEBLOCKS, 0) <=
				(fdirent.file_size + BSIZEM1) / BLOCKSIZE)
			{
				eprintf("%s: will not fit: no copy\r\n"); //,fromfullpath);
				retval = BEC_DISKFULL;
				goto e2;
			}
		}

		if ((ifd = retval = OPEN(fromfullpath, SSO_RDONLY)) < 0)
			goto e2;
		errname = tofullpath;
		if ((ofd = retval = CREAT(tofullpath, 0, 0)) < 0)
		{
			CLOSE(ifd);
			goto e2;
		}
		if ((retval = pump(ifd, ofd, fromfullpath, tofullpath)))
		{
			CLOSE(ifd);
			CLOSE(ofd);
			errname = 0;	/* pump() prints message */
			goto e2;
		}
		CLOSE(ifd);
		if ((retval = CLOSE(ofd)) < 0)
			goto e2;

		/* Update the output file's date and statbits */
		if (!fromch && !toch)
		{
			if ((retval = PROCESSDIR(tofullpath, &tdirent, 2)) < 0)
				goto e2;
			tdirent.statbits = (tdirent.statbits & FS_HASFFB) |
				(fdirent.statbits & ~FS_HASFFB);
			movmem(fdirent.date,tdirent.date,sizeof(fdirent.date));
			/* move preserves uid & bits, copy resets uid */
			tdirent.uid = copymode ? ouruid : (fromch ? ouruid : fdirent.uid);
#ifdef notdef
			tdirent.load_addr =
				wasthere ? odirent.load_addr : fdirent.load_addr;
#else		/* 4.5: Always preserve load address */
			tdirent.load_addr = fdirent.load_addr;
#endif
			if ((retval = PROCESSDIR(tofullpath, &tdirent, 3)) < 0)
				goto e2;
		}
		if (!copymode && !fromch && !locked)
		{/* Copied OK, it is a move command, so delete the old file */
			if ((retval = UNLINK(fromfullpath)) < 0)
			{
				errname = fromfullpath;
				goto e2;
			}
		}
e2:		;
	}
	else
	{	/* No need to copy: just shuffle directory entries */
		char ch;

		errname = tofullpath;
		if (!strnucmp(fromfullpath, tofullpath, strlen(fromfullpath)) &&
			(((ch = tofullpath[strlen(fromfullpath)]) == 0) || (ch == '/')))
		{	/* Can't copy directory into itself! */
			retval = BEC_FNBAD;
			goto err;
		}
		if (((retval = UNLINK(tofullpath)) < 0) && (retval != BEC_NOFILE))
				goto err;
		retval = dorename(fromfullpath, tofullpath, 1);
		errname = fromfullpath;
	}
err:
#ifdef notdef
	dofreemem(fromfullpath);
	if (!toch)
		dofreemem(tofullpath);
#endif
	if ((retval < 0) && (errname))
	{
		eprintf("%s: ");		//, errname);
		dobec(retval);
	}
	return (retval < 0) ? retval : 0;
}

/*
 * System call 99: Set internal option. If bit 31 of 'opnum' set, just read it
 */

extern int excepcls;		/* Flag: clear screen on exception (mon.c) */
extern int diskverbose;		/* Flag: floppy error messages enabled */
extern int exceprestart;	/* If true, do warm boot on exception */
extern int traceregs;		/* If true, dump registers when tracing */
extern int canwriterb;		/* Enable write to system blocks */
extern int vidomode;		/* Video output mode in video driver */
extern int hashonmiss;		/* Reread xpaths on no command match error */
extern struct chardriver *chardrivers;
extern int memerrflag;

/* ADD TO HELP WHEN YOU ADD AN OPTION, andrew */

_option(opnum, set)
int opnum;
{
	register int rdonly, origval, mask, *ptr;
	struct chardriver *chardriver;

	rdonly = opnum & OPT_READ;	/* True if only reading it */
	opnum &= ~OPT_READ;
	mask = 0;
	ptr = (int *)0;
	origval = BEC_BADARG;		/* Bad option */
	/*
	 * Botch for option 6 caused by changing eofchar from system wide
	 * to per device. Similar for option 9
	 */
	if (opnum == 6 || opnum == 9)
		chardriver = &chardrivers[getttydev(1)];

	switch (opnum)
	{
	case 0:	/* enable directory in prompt */
		mask = ENVB_PROMPTEN; break;
	case 1:	/* enable verbose mode */
		mask = ENVB_VERBOSE; break;
	case 2:	/* directory sorting: 0 = none, 1 = time, 2 = alphabetic */
		switch (set)
		{
		case 0: mask = 0; break;
		case 1:	mask = ENVB_DIRMODE0; break;
		case 2:	mask = ENVB_DIRMODE0|ENVB_DIRMODE1; break;
		default: goto badopt;
		}
		if (!envbits(ENVB_DIRMODE0))
			origval = 0;
		else
			origval = envbits(ENVB_DIRMODE1) ? 2 : 1;
		if (!rdonly)
		{
			SETENVBITS(curpid, ENVB_DIRMODE0|ENVB_DIRMODE1, 0);
			SETENVBITS(curpid, mask, 1);
		}
		goto out;
	case 3:		/* exception mode: 1=stack trace, 2=kill, 4=stop */
		ptr = &excepcls; break;
	case 4:		/* enable disk error messages */
		ptr = &diskverbose; break;
	case 5:		/* enable process kill on memory allocation failure */
		ptr = &memerrflag; break;
	case 6:		/* set eof character */
		ptr = &chardriver->eofchar; break;
	case 7:		/* file stack space */
		ptr = &execstackspace; break;
	case 8:		/* enable write to disk system blocks */
		ptr = &canwriterb; break;
	case 9:		/* enable ALT-^R reset */
		ptr = &chardriver->resetchar; break;
	case 10:	/* unused */
	case 11:	/* enable all ALT K/B codes */
		goto unused;
	case 12:	/* enable bell on errors */
		mask = ENVB_IBBEEP; break;
	case 13:	/* enable restart after exceptions */
		ptr = &exceprestart; break;
	case 14:	/* enable register display in trace */
		ptr = &traceregs; break;
	case 15:	/* miscellaneous */
		ptr = &miscmodes; break;
	case 16:	/* unused */
unused:		origval = 0;
		goto out;
	case 17:	/* enable lower case pathnames */
		mask = ENVB_LOWERNAMES; break;
	case 18:	/* set video output mode */
		ptr = &vidomode; break;
	case 19:	/* enable automatic xpath reread */
		ptr = &hashonmiss; break;
	default:
badopt:
		goto out;
	}

	if (ptr)
	{
		origval = *ptr;
		if (!rdonly)
			*ptr = set;
	}
	else if (mask)
	{
		origval = envbits(mask) ? 1 : 0;
		if (!rdonly)
			SETENVBITS(curpid, mask, set ? 1 : 0);
	}
out:
	return origval;
}

optionhelp()
{
	static char unused[] = "unused";
	static char *opmes[] = {
/*  0 */	"%sdirectory in prompt",
/*  1 */	"%sverbose mode",
/*  2 */	"directory sorting: 0 = none, 1 = time, 2 = alphabetic",
/*  3 */	"exception mode: 1=stack trace, 2=kill, 4=stop",
/*  4 */	"%sdisk error messages",
/*  5 */	"%sprocess kill on memory allocation failure",
/*  6 */	"set eof character",
/*  7 */	"EXEC file stack space",
/*  8 */	"%swrite to disk system blocks",
/*  9 */	"ALT-^R reset character",
/* 10 */	unused,
/* 11 */	unused,		/* "%sall ALT K/B codes", */
/* 12 */	"%sbell on errors",
/* 13 */	"%srestart after exceptions",
/* 14 */	"%sregister display in trace",
/* 15 */	"miscellaneous",
/* 16 */	unused,
/* 17 */	"%slower case pathnames",
#ifdef GPCPU
		"",
#else
/* 18 */	"set video output mode",
#endif
/* 19 */	"%sautomatic xpath reread"
	};

	register int i;
	char buf[100];

	for (i = 0; i < sizeof(opmes) / sizeof(*opmes); i++)
	{
		SPRINTF(buf, opmes[i], "enable ");
		PRINTF("  %2d %4d: %s\r\n", i, OPTION(OPT_READ|i, 0), buf);
	}
}

int verbose(void)
{
	return (envbits(ENVB_VERBOSE) && ISINTERACTIVE(GETPID()));
}
