#ifndef PIPE_H
#define PIPE_H


extern int piperead(int fcbnum,uchar *buf,uint nbytes);
extern int pipeclose(int fcbnum);
extern int pipewrite(int fcbnum,uchar *buf,uint nbytes);

#endif
