/* TABS4
 * Line editor.
 */

#ifndef LEDIT_H
#define LEDIT_H

/* Editor externals */
typedef struct ledthing
{
	ushort cursor;		/* Where the cursor is in the string */
	uchar *newbuf;		/* Pointer to new string */
	uchar *oldbuf;		/* Pointer to last displayed string */
	ushort cursscrnpos;	/* Where the lt->cursor is on the screen */
	ushort oldlen;		/* Length of previously displayed string */
	ushort len;
	int outfd;
} LEDTHING;


extern int nlastlines;

extern void ledit_init(void);
extern void redisp(struct ledthing *lt);
extern void getlen(LEDTHING *lt);
extern void insch(LEDTHING *lt,char ch,int length);
extern void remch(LEDTHING *lt);
extern int getredir(char *buf,char ch,char **gtltptr);

#endif
