/*
 * Default stack spaces
 */

#define SHELL_STACKSPACE	0x2000	/* .shell file interpreter */
#define RSHELL_STACKSPACE	0x2000	/* Root shell */
#define BIN_STACKSPACE		0x2000	/* Binaries with 0 in header */
#define MRD_STACKSPACE		0x2000	/* MRD calls */
#define IBC_STACKSPACE		0x3000	/* Inbuilt commands */
