/* TABS8 NONDOC
 *	test command
 */

/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */

/*
 * Modes:
 *	test +/-isthere		filename
 *	test +/-isdir		filename
 *	test +/-isolder		filename1 filename2
 *	test +/-islarger	filename1 filename2
 *	test +/-hasextent	filename extent
 *	test +/-error		command
 */


#include <types.h>
#include <syscalls.h>
#include <files.h>

#define NELEM(array)	(sizeof(array)/sizeof(*array))

#define T_THERE		1
#define T_ISDIR		2
#define T_ISOLDER	3
#define T_ISLARGER	4
#define T_HASEXTENT	5
#define T_ERROR		6

char *ourname;

struct cmd
{
	char *cmd;		/* Command */
	uchar nargs;		/* Number of args to this mode */
	char dostat1, dostat2;	/* If set, stat corresponding file */
	char token;
} cmds =
{
	{ "isthere",	1,	1,	0,	T_THERE},
	{ "isdir",	1,	1,	0,	T_ISDIR},
	{ "isolder",	2,	1,	1,	T_ISOLDER},
	{ "islarger",	2,	1,	1,	T_ISLARGER},
	{ "hasextent",	2,	0,	0,	T_HASEXTENT},
	{ "error",	255,	0,	0,	T_ERROR}
};

main(argc, argv)
char *argv;
{
	register int i;
	register struct cmd *pcmd;
	register int retval;
	char *cp, sign;
	struct dir_entry dirent1, dirent2;
	int stat1, stat2;

	ourname = argv0;
	if (argc < 3)
		usage(1);

	cp = argv1;
	sign = *cp++;
	if ((sign != '+') && (sign != '-'))
		usage(2);

	pcmd = (struct cmd *)0;
	for (i = 0; i < NELEM(cmds); i++)
	{
		if (!strucmp(cp, cmdsi.cmd))
		{
			pcmd = cmds + i;
			break;
		}
	}
	if (!pcmd)
		usage(3);

	if (pcmd->nargs != 255)
	{
		if (argc != (pcmd->nargs + 2))
			usage(4);

		if (pcmd->dostat1)
		{
			stat1 = FILESTAT(argv2, &dirent1);
			if (stat1 < 0)
				memset(&dirent1, 0, sizeof(dirent1));
		}

		if (pcmd->dostat2)
		{
			stat2 = FILESTAT(argv3, &dirent2);
			if (stat2 < 0)
				memset(&dirent2, 0, sizeof(dirent2));
		}
	}

	switch (pcmd->token)
	{
	case T_THERE:
		retval = (stat1 >= 0);
		break;
	case T_ISDIR:
		retval = (stat1 >= 0) && (dirent1.statbits & DIRECTORY);
		break;
	case T_ISOLDER:
		if ((stat1 >= 0) && (stat2 < 0))
			retval = 1;	/* Second file not there: call it older */
		else if ((stat1 < 0) && (stat2 >= 0))
			retval = 0;
		else if ((stat1 < 0) && (stat2 < 0))
			retval = 0;	/* Dunno */
		else
		{
			retval = 1;	/* Assume 1 is older than 2 */
			for (i = 0; i < 8; i++)
			{
				if (dirent1.datei < dirent2.datei)
				{
					retval = 0;
					break;
				}
			}
		}
		break;
	case T_ISLARGER:
		retval = dirent1.file_size > dirent2.file_size;
		break;
	case T_HASEXTENT:
		{
			char *p;

			p = argv2;
			while (*p)
				p++;
			p -= strlen(argv3);
			if (p < argv2)
				retval = 0;
			else
				retval = (strucmp(p, argv3) == 0);
		}
		break;
	case T_ERROR:
		retval = EXECA(argv + 2) < 0;
		break;
	}

	if (sign == '+')
		EXIT(retval);
	else
		EXIT(!retval);
}

strucmp(s1, s2)
register char *s1, *s2;
{
		while (*s1)
		{
			if (toupper(*s1) != toupper(*s2))
				return 1;
			s1++;
			s2++;
		}
		return *s2;
}

usage(n)
{
	FPRINTF(STDERR, "Usage(%d):\r\n", n);
	FPRINTF(STDERR, "       %s +/-isthere   filename\r\n", ourname);
	FPRINTF(STDERR, "       %s +/-isdir     filename\r\n", ourname);
	FPRINTF(STDERR, "       %s +/-isolder   filename1 filename2\r\n", ourname);
	FPRINTF(STDERR, "       %s +/-islarger  filename1 filename2\r\n", ourname);
	FPRINTF(STDERR, "       %s +/-hasextent filename extent\r\n", ourname);
	FPRINTF(STDERR, "       %s +/-error     command\r\n", ourname);
 	EXIT(-1);
}
