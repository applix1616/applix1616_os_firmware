/* TABS4 NONDOC
 * for command
 */

#include <syscalls.h>

static char usagemessage = "Usage: %s args\r\n";
char *ourname;

struct line
{
	char *line;
	struct line *next;
};

static char *roomfor();

main(argc, argv)
char *argv;
{
	int arg;
	char *cp, ch;
	char buf512;
	struct line *head, *line;

	ourname = argv0;
	if (argc < 2)
		usage();
	line = head = (struct line *)0;
	for ( ; ; )
	{
		PRINTF(">");
		buf0 = '\0';
		if (!LEDIT(buf))
			break;
		if (line == (struct line *)0)
		{
			line = (struct line *)GETMEM(sizeof(*line), 0);
			head = line;
			head->line = roomfor(buf);
			head->next = 0;
		}
		else
		{
			line->next = (struct line *)GETMEM(sizeof(*line), 0);
			line = line->next;
			line->next = (struct line *)0;
			line->line = roomfor(buf);
		}
	}

	for (arg = 1; arg < argc; arg++)
	{
		cp = argvarg;
		line = head;
		while (line)
		{
			SPRINTF(buf, line->line, cp, cp, cp, cp, cp, cp,
				cp, cp, cp, cp, cp, cp, cp, cp);
			EXEC(buf);
			line = line->next;
		}
	}
	EXIT(0);
}

static char *
roomfor(s)
char *s;
{
	char *ret;

	ret = (char *)GETMEM(strlen(s) + 1, 0);
	strcpy(ret, s);
	return ret;
}

usage()
{
	FPRINTF(STDERR, usagemessage, ourname);
	EXIT(-1);
}
