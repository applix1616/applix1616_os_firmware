/* TABS4 NONDOC
 * Change file ownership
 */

/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */

#include <types.h>
#include <syscalls.h>
#include <stdio.h>
#include <blkerrcodes.h>

char *ourname;

main(argc, argv)
char *argv;
{
	int fretval = 0;
	register int curuid, uid, arg, retval;

	ourname = argv0;

	if (argc < 3)
		usage();
	curuid = getuid();
	uid = stringtouid(argv1);
	if (uid < 0)
	{
		FPRINTF(STDERR,
			"chown: cannot find '%s' in /etc/passwd\r\n", argv1);
		EXIT(-1);
	}

	if (curuid && (uid == 0))
	{
		FPRINTF(STDERR,
			"chown: Cannot chown files to UID 0: Permission denied\r\n");
		EXIT(BEC_NOPERM);
	}

	for (arg = 2; arg < argc; arg++)
	{
		retval = PROCESSDIR(argvarg, uid, 4);
		if (retval < 0)
		{
			fretval = retval;
			FPRINTF(STDERR,
				"chown: Cannot change UID of %s: %s\r\n",
					argvarg, ERRMES(retval));
		}
	}
	EXIT(fretval);
}

usage()
{
	FPRINTF(STDERR, "Usage: %s UID/username filename ...\r\n", ourname);
	EXIT(-1);
}
