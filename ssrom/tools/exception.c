/*
 * Convert the exception information at 0x8000 into
 * readable form
 */

#include <types.h>
#include <syscalls.h>
#include <exception.h>

char *ourname;

#define E EXCEPTION_DATA

main(argc, argv)
char *argv;
{
	register ushort i;
	register int *regptr;
	static char Lsrchars = "cvznx--------s-t";
	static char Hsrchars = "CVZNX---124--S-T";
	static char rbr = "Reason for reboot: ";
	int wanttrace;

	ourname = argv0;
	wanttrace = 0;

	if (argc > 1)
		wanttrace = 1;

	if (E->magic != RESET_MAGIC)
		PRINTF("No exception information present\r\n");

	switch (E->boot_reason)
	{
	case BR_SOFTRESET:
		PRINTF("%sReset due to ALT-^R type soft reset\r\n", rbr);
		break;
	case BR_STACKOVERRUN:
		PRINTF("%sStack overrun\r\n", rbr);
		break;
	case BR_EXCEPTION:
		PRINTF("%sException\r\n", rbr);
		break;
	default:
		PRINTF("Boot diagnostic code: %d\r\n", E->boot_reason);
		break;
	}

	if (E->magic != RESET_MAGIC)
		EXIT(0);

	/* Pointer within ROMs? */
	if ((E->message >= (char *)0x500000) &&
			(E->message < (char *)0x520000))
		PRINTF("%s\r\n", E->message);

/* AKPM development stuff */
	if ((E->message >= (char *)0x280000) &&
			(E->message < (char *)0x2a0000))
		PRINTF("%s\r\n", E->message);

	/* Dump the registers */
	PRINTF("FC:%04X Acc addr:%08X IR:%04X", E->fc, E->accaddr, E->ir);
	PRINTF(" Next PC:%08X SR:%04X (", E->pc, E->sr);

	i = 15;
	do
		PUTCHAR((E->sr & (1 << i)) ? Hsrcharsi : Lsrcharsi);
	while (i--);
	PRINTF(")\r\n");

	PRINTF("D0-D7:");
	for (i = 0; i < 8; i++)
		PRINTF(" %08X", E->dregsi);
	PRINTF("\r\n");

	PRINTF("A0-A7:");
	for (i = 0; i < 8; i++)
		PRINTF(" %08X", E->aregsi);
	PRINTF("\r\n");

	PRINTF("Name of process scheduled at exception time: %s\r\n",
			E->processname);
	PRINTF("Address at which it was scheduled: $%X\r\n", E->lastload);
	PRINTF("Last command executed: %s\r\n", E->lastexec);
	PRINTF("Last system call performed: %d\r\n", E->lastsyscall);

	if (wanttrace)
	{
		char buf100;
		PRINTF("Stack trace:\r\n");
		SPRINTF(buf, "mdw %x %x", E->stacktrace,
			E->stacktrace + sizeof(E->stacktrace) - 1);
		EXEC(buf);
	}
}
