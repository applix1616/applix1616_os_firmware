/* TABS4 NONDOC
 * Environment manipulation program.
 * Usage: 	setenv ENVVAR=SETTING	Adds setting
 *		setenv ENVVAR		Deletes setting
 *
 * If ENVVAR is in the hard-wired table, manipulate process
 * environment instead.
 */

#include <types.h>
#include <stdio.h>
#include <syscalls.h>
#include <envbits.h>

#define NELEM(array)	(sizeof(array)/sizeof(*array))

char envname = "/usr/lib/environ";

struct envbit
{
	char *name;		/* Human (?) readable name */
	uint mask;		/* Bit mask in shellenv.envbits */
	char *desc;		/* Long description */
} envbits =
{
	{ "PROMPTEN", ENVB_PROMPTEN, "Enable working directory in prompt" },
	{ "VERBOSE", ENVB_VERBOSE, "Enable verbose mode" },
	{ "DIRMODE0", ENVB_DIRMODE0, "Enable directory sorting" },
	{ "DIRMODE1", ENVB_DIRMODE1, "0: Date sort 1: Alphabetic sort" },
	{ "IBBEEP", ENVB_IBBEEP, "Enable bell on inbuilt command errors" },
	{ "NOBAK", ENVB_NOBAK, "Disable '.bak' file generation" },
	{ "ERRBEEP", ENVB_ERRBEEP, "Enable bell on all errors" },
	{ "HIDEFILES", ENVB_HIDEFILES, "Hide files with hidden bit set" },
	{ "ASSIGNPROMPT", ENVB_ASSIGNPROMPT, "Use assigns to shorten prompt" },
	{ "LOWERNAMES", ENVB_LOWERNAMES, "Enable lower case filenames" },
	{ "PROMPTGT", ENVB_PROMPTGT, "Enable '>' in prompt" },
	{ "DODSIGN", ENVB_DODSIGN, "Do '$' exec environ substititions" },
	{ "DOARG", ENVB_DOARG, "Do all argument environ substititions" },
	{ "DOARG0", ENVB_DOARG0, "Do command name environ substititions"},
	{ "NOSHELLOUT", ENVB_NOSHELLOUT, "Deny shell escapes from applications"},
	{ "LOWBAUD", ENVB_LOWBAUD, "Running on a low-speed terminal"}
};

/* A struct for manipulating entries in memory */
struct enventry
{
	char *entry;
	struct enventry *next;
	struct enventry *prev;
} *envhead, *envtail;

char *ourname;
char fileread = 0;
ulong curenvbits, uid;
extern char *strchr(), *malloc();

main(argc, argv)
char *argv;
{
	int arg;
	char *cp;
	FILE *envfp;
	register struct enventry *ep;
	char buf500;
	char fname200;

	ourname = argv0;

	if (GETROMVER() >= 0x42)
		curenvbits = GETENVBITS(GETPID());

	if (argc < 2)
		usage();

	if (!strcmp(argv1, "-q") || !strcmp(argv1, "-v"))
		longhelp();

	strcpy(fname, envname);
	if (uid = READUID())
	{
		char buf220;
		SPRINTF(buf2, "%d", uid);
		strcat(fname, buf2);
	}

	for (arg = 1; arg < argc; arg++)
	{
		int bufi;
		char setting;

		cp = argvarg;
		bufi = 0;
		while (*cp && (*cp != '='))
			bufbufi++ = *cp++;
		bufbufi = 0;
		setting = (*cp++ == '=');
		if (chkenvbits(buf, setting, cp))
			continue;
		if (!fileread)
			readenv(fname);
		if (rmenv(buf))
		{
			if (setting)
			{
				addenv(argvarg);
			}
		}
		else
		{
			if (setting)
				addenv(argvarg);
			else
				fprintf(stderr,
					"setenv: %s: No such setting\n", buf);
		}
	}
	if (fileread)
	{
		envfp = fopen(fname, "w");
		if (!envfp)
		{
			fprintf(stderr, "setenv: Cannot create %s", fname);
			perror("");
			exit(-1);
		}
		ep = envhead;
		while (ep)
		{
			fprintf(envfp, "%s\n", ep->entry);
			ep = ep->next;
		}
		fclose(envfp);
	}
	exit(0);
}

/* See if it is an environment bit, return 0 if not */
chkenvbits(buf, setting, cp)
char *buf;
int setting;
char *cp;
{
	register int i;
	int val;

	if (GETROMVER() < 0x42)
		return 0;

	for (i = 0; i < NELEM(envbits); i++)
	{
		if (!strucmp(envbitsi.name, buf))
			break;
	}
	if (i == NELEM(envbits))
		return 0;

	switch (envbitsi.mask)
	{
	case ENVB_NOSHELLOUT:
		if ((curenvbits & ENVB_NOSHELLOUT) && uid)
		{
			FPRINTF(STDERR, "%s: %s: Permission denied\r\n", ourname, envbitsi.name);
			return 0;
		}
		break;
	}

	if (setting)
		val = atoi(cp);
	else
		val = 0;
	SETENVBITS(GETPID(), envbitsi.mask, val);
	return 1;
}

rmenv(entry)
register char *entry;
{
	register struct enventry *ep;
	register char *cp;
	register int bufi;
	int retval = 0;
	char buf500;

	ep = envhead;
	while (ep)
	{
		bufi = 0;
		cp = ep->entry;
		while (*cp && (*cp != '='))
			bufbufi++ = *cp++;
		bufbufi = 0;
		if (!strcmp(buf, entry))
		{
			retval = 1;
			if (ep == envhead)
				envhead = envhead->next;
			if (ep == envtail)
				envtail = envtail->prev;
			if (ep->prev)
				ep->prev->next = ep->next;
			if (ep->next)
				ep->next->prev = ep->prev;
			free(ep->entry);
			free(ep);
		}
		ep = ep->next;
	}
	return retval;
}

addenv(entry)
char *entry;
{
	register struct enventry *ep;

	ep = (struct enventry *)malloc(sizeof(*ep));
	ep->next = 0;
	ep->prev = envtail;
	ep->entry = malloc(strlen(entry) + 1);
	strcpy(ep->entry, entry);
	if (envhead)
	{
		envtail->next = ep;
		envtail = ep;
	}
	else
		envhead = envtail = ep;
}

readenv(fname)
char *fname;
{
	int arg;
	char *cp;
	int uid;
	FILE *envfp;
	register struct enventry *ep;
	char buf500;

	if (fileread)
		return;
	envfp = fopen(fname, "r");
	if (envfp == 0)
	{
		fprintf(stderr, "setenv: Cannot open %s\n", fname);
		exit(-1);
	}

	envhead = 0;
	envtail = 0;
	/* Build entries */
	while (fgets(buf, sizeof(buf), envfp))
	{
		cp = strchr(buf, '\n');
		if (cp)
			*cp = 0;
		ep = (struct enventry *)malloc(sizeof(*ep));
		ep->next = 0;
		ep->prev = 0;
		ep->entry = (char *)malloc(strlen(buf) + 1);
		strcpy(ep->entry, buf);
		ep->prev = envtail;
		if (!envhead)
			envhead = ep;
		else
			envtail->next = ep;
		envtail = ep;
	}
	fileread = 1;
	fclose(envfp);
}

usage()
{
	fprintf(stderr, "Usage: %s -q ENVVAR=SETTING ..\n", ourname);
	fprintf(stderr, "          Use 'setenv -q' for long help message\r\n");
	exit(-1);
}

strucmp(s1, s2)
register char *s1, *s2;
{
		while (*s1)
		{
			if (toupper(*s1) != toupper(*s2))
				return 1;
			s1++;
			s2++;
		}
		return *s2;
}

longhelp()
{
	int i;

	PRINTF("Inbuilt environment bits:\r\n");
	for (i = 0; i < NELEM(envbits); i++)
	{
		PRINTF("%d %-12s %s\r\n",
			(GETENVBITS(GETPID()) & envbitsi.mask) ? 1 : 0,
			envbitsi.name,
			envbitsi.desc);
	}
	EXIT(0);
}
