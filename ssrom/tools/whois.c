/* TABS4 NONDOC
 * Read current uid, convert UIDs to lognames, etc.
 */

/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */

#include <types.h>
#include <syscalls.h>
#include <passwd.h>

char *ourname;
int finalret = 0;

usage()
{
	FPRINTF(STDERR, "Usage: %s UID or logname\r\n", ourname);
	EXIT(-1);
}

main(argc, argv)
char *argv;
{
	register int uid, arg;
	register char *av;

	ourname = argv0;

	if (argc == 1)
	{
		uid = READUID();
		disp(uid);
		usage();
	}

	for (arg = 1; arg < argc; arg++)
	{
		av = argvarg;
		uid = stringtouid(av);
		if (uid < 0)
		{
			FPRINTF(STDERR, "%s: '%s': No such user in /etc/passwd\r\n",
						ourname, av);
			finalret = -1;
		}
		else
			disp(uid);
	}

	EXIT(finalret);
}

disp(uid)
{
	register struct passwd *pwd;

	pwd = getpwuid(uid);
	if (pwd == (struct passwd *)0)
	{
		FPRINTF(STDERR, "%s: uid %d: Not in /etc/passwd\r\n", ourname, uid);
		finalret = -1;
	}
	PRINTF("User %d (%s): group %d, logname '%s', home is %s\r\n",
			uid, pwd->pw_description, pwd->pw_gid, pwd->pw_name, pwd->pw_dir);
}
