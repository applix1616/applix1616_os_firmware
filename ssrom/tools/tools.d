.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "Operating system tools"
.footer   "Page $#p"

saveenv.xrel and loadenv.xrel

     These programs record and restore the current shell process's��environment in a disk file.  The environment bits (as set with��setenv), the environment strings (as set by sset) and the currnet��inbuilt command disable vector are recorded.

.include "/usr/lib/trailer.dh"

