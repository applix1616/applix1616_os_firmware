/* TABS8 NONDOC
 * Start a shell, new UID
 */

/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */

#include <syscalls.h>

char *ourname;

usage()
{
	FPRINTF(STDERR, "Usage: %s -uUID -mUMASK\r\n", ourname);
	EXIT(-1);
}

main(argc, argv)
char *argv;
{
	int arg;
	char *cp;

	ourname = argv0;
	if (argc < 2)
		usage();

	for (arg = 1; arg < argc; arg++)
	{
		cp = argvarg;
		if (*cp == '-')
		{
			if (!cp1 || !cp2)
				usage();
			switch (toupper(cp1))
			{
			case 'U':
				SETUID(atoi(cp + 2));
				break;
			case 'M':
				SETUMASK(atoi(cp + 2));
				break;
			default:
				usage();
			}
		}
		else
			usage();
	}
	IEXEC(1);
	EXIT(0);
}
