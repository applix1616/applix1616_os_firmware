/*
 * A program which prints out vital statistics about the currently
 * resident MRDs
 *
 * AKPM, 23 July, 1988
 */

#include <types.h>
#include <stdio.h>
#include <syscalls.h>
#include <reloc.h>

main(argc, argv)
int argc;
char *argv;
{
	register struct mrdriver *mrdriver;
	register struct rel_header *rel_header;
	int driver;

/* Get the pointer to the internal MRD structure */
	mrdriver = (struct mrdriver *)CALLMRD(-1, 0, 0);

	printf("Main header information (at $%X)\n", mrdriver);
	printf("MAGIC1: $%X, Version: $%X, RDsize: $%X, Memusage: $%X\n",
			mrdriver->magic1, mrdriver->vers,
			mrdriver->rdsize, mrdriver->memusage);
	printf("%d drivers, MAGIC2: $%X, stackspace: $%X, vramspace: $%X\n",
			mrdriver->ndrivers, mrdriver->magic2,
			mrdriver->stackspace, mrdriver->vramspace);
	printf("OBRAM: $%X, MRDstart: $%X, RDstart: $%X, bitmaps: $%X\n",
			mrdriver->obramstart, mrdriver->mrdstart,
			mrdriver->rdstart, mrdriver->bitmaps);
	printf("Default colours: $%X, nlastlines: %d, ncacheblocks: %d\n",
		mrdriver->colours, mrdriver->nlastlines,
		mrdriver->ncacheblocks);
	printf("Maxfiles: %d, Directory cache entries: %d\r\n",
			mrdriver->maxfiles, mrdriver->ndcentries);
	printf("Character device driver structures at address $%X\r\n",
			mrdriver->chardrivers);

	printf("\nPer driver information:\n");

/* Scan the drivers */
	rel_header = (struct rel_header *)mrdriver->mrdstart;
	for (driver = 0; driver < mrdriver->ndrivers; driver++)
	{
		printf("Driver %d: %s, version $%X, ", driver, 
			CALLMRD(driver, MRD_NAME, 0),
			CALLMRD(driver, MRD_VERS, 0));
		printf("exec entry point: $%X\n",
			CALLMRD(driver, MRD_EXECENTRY, 0));

		printf("\tStart: $%X, textlen: $%X, datalen: $%X, bsslen: $%X\n",
			rel_header, rel_header->textlen,
			rel_header->datalen, rel_header->bsslen);
		printf("\tsymtablen: $%X, textbegin: $%X, relocflag: $%X\n",
			rel_header->symtablen, rel_header->text_begin,
			rel_header->reloc_flag);
		printf("\tCode start: $%X\n",
			((long)rel_header) + sizeof(*rel_header));
		/* Skip to nextr MRD */
		rel_header = (struct rel_header *)(
				((long)(rel_header)) +
				sizeof(*rel_header) +
				rel_header->textlen +
				rel_header->datalen +
				rel_header->bsslen);
	}
}
