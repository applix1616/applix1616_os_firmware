/* TABS8 NONDOC
 * Kill or signal a process group
 */

/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */

#include <syscalls.h>

char *ourname;

main(argc, argv)
char *argv;
{
	char *cp;
	int arg;
	int signalit = 0;
	int sigtosend;
	int pg;

	ourname = argv0;

	if (argc < 2)
		usage();

	arg = 1;
	cp = argvarg;
	if (*cp == '-')
	{
		cp++;
		arg++;
		sigtosend = atoi(cp);
		signalit = 1;
	}

	for ( ; arg < argc; arg++)
	{
		pg = atoi(argvarg);
		if (signalit)
			SIGPG(pg, sigtosend, 0);
		else
			KILLPG(pg);
	}
}

usage()
{
	FPRINTF(STDERR, "Usage: %s -NN process_group process_group\r\n", ourname);
	EXIT(-1);
}
