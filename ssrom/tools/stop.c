/* TABS8 NONDOC */

/*
 * Stop a process
 */

#include <types.h>
#include <syscalls.h>
#include <process.h>
#include <signal.h>

char *ourname;

main(argc, argv)
char *argv;
{
	int arg;
	char *cp;
	int retval;

	ourname = argv0;

	if (argc < 2)
		usage();

	if (GETROMVER() < 0x42)
	{
		FPRINTF(STDERR, "%s: Buy new ROMs\r\n", ourname);
		EXIT(-1);
	}

	for (arg = 1; arg < argc; arg++)
	{
		cp = argvarg;
		retval = SIGBLOCKER(cp, SIGSTOP, 0);
		if (retval < 0)
		{
			FPRINTF(STDERR, "%s: Cannot stop %s: %s\r\n",
				ourname, cp, ERRMES(retval));
		}
	}
}

usage()
{
	FPRINTF(STDERR, "Usage: %s pid pid ...\r\n", ourname);
	EXIT(-1);
}
