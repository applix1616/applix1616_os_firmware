/* TABS8 NONDOC
 * if command
 */

/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */

#include <types.h>
#include <syscalls.h>

char *ourname;
char **newargv;
char **argv;

main(argc, av)
char *av;
{
	char *cp;
	int arg;
	int thenpos, elsepos;
	int ret;

	argv = av;
	ourname = argv0;
	thenpos = 0;
	elsepos = 0;
	newargv = (char **)GETMEM((argc + 1) * sizeof(*newargv), 0);

	for (arg = 1; (arg < argc) && !elsepos; arg++)
	{
		cp = argvarg;
		if (!strucmp(cp, "then") && !thenpos)
		{
			int ifcount;

			ifcount = 0;
			thenpos = arg;
			for ( ; arg < argc; arg++)
			{	/* Look for matching else */
				cp = argvarg;
				if (!strucmp(cp, "if"))
					ifcount++;
				else if (!strucmp(cp, "else") && !ifcount--)
				{
					elsepos = arg;
					break;
				}
			}
		}
	}
	if (!thenpos)
		usage();

	if (elsepos && (elsepos < thenpos))
		usage();

	ret = doexec(1, thenpos);
	if (ret)
	{
		ret = doexec(thenpos + 1, elsepos ? elsepos : argc);
	}
	else if (elsepos)
	{
		ret = doexec(elsepos + 1, argc);
	}
	else
		ret = 0;
	EXIT(ret);
}

/* Execute a command from argv vector */
doexec(first, last)
{
	int i, pos;

	pos = 0;
	for (i = first; i < last; i++)
		newargvpos++ = argvi;
	newargvpos = (char *)0;

	return EXECA(newargv);
}

usage()
{
	FPRINTF(STDERR, "Usage: %s command then command else command\r\n",
		ourname);
	EXIT(-1);
}

strucmp(s1, s2)
register char *s1, *s2;
{
	while (*s1)
	{
		if (toupper(*s1) != toupper(*s2))
			return 1;
		s1++;
		s2++;
	}
	return *s2;
}
