/* TABS8 NONDOC
 * Save current environment in a file
 */

#include <types.h>
#include <syscalls.h>
#include <process.h>
#include <files.h>
#include "envversion.h"

static char usagemessage =
 	"Usage: %s filename\r\n";

/* The name of this process: keep it global */
char *ourname;
char *filename = (char *)0;
int ofd;
int envversion = ENVVERSION;

void usage();

main(argc, argv)
char *argv;
{
	char *cp;
	int arg;
	int val;
	int uid, ret;
	uchar *ibc;
	register ENVSTRING *envstring;

	ourname = argv0;
	uid = READUID();

	for (arg = 1; arg < argc; arg++)
	{
		cp = argvarg;
		if (*cp == '-')
		{	/* An option */
			char ch;
			while (*++cp)
			{	/* Process option characters */
				switch (ch = *cp)
				{
				default:	/* Unrecognised option */
					usage();
				}
			}
		}
		else
		{
			if (filename == (char *)0)
				filename = cp;
			else
				usage();
		}
	}

	/* Check that both compulsory args were supplied */
	if (filename == (char *)0)
		usage();

	SETUID(0);
	if ((ofd = CREAT(filename, 0, 0)) < 0)
	{
		FPRINTF(STDERR, "%s: Cannot create '%s': %s\r\n",
			ourname, filename, ERRMES(ofd));
		doexit(ofd);
	}
	SETUID(uid);

	dowrite(&envversion, sizeof(envversion));

	/* Write environment bits */
	val = GETENVBITS(GETPID());
	dowrite(&val, sizeof(val));

	/* Write command disable vector */
	ibc = (uchar *)READIBCVEC();
	if ((int)ibc < 0)
	{
		FPRINTF(STDERR, "%s: Cannot read command disable vector: %s\r\n",
			ourname, ibc);
		doexit(ibc);
	}
	if (ibc)
	{
		char ch = 1;
		dowrite(&ch, 1);
		dowrite(ibc, 256);
	}
	else
	{
		char ch = 0;
		dowrite(&ch, 1);
	}

	/* Write environment strings */
	val = 0;
	while ((int)(envstring = (ENVSTRING *)(GETENV(GETPID(), val, -1))) > 0)
	{
		dowrite(&envstring->mode, sizeof(envstring->mode));
		writestr(envstring->name);
		writestr(envstring->setting);
		val++;
	}
	CLOSE(ofd);
	SETUID(0);
	if (uid)
	{
		if ((ret = PROCESSDIR(filename, 0, PD_SETUID)) < 0)
		{
			FPRINTF(STDERR, "%s: Cannot set UID of %s: %s\r\n",
				ourname, filename, ERRMES(ret));
			doexit(ret);
		}
		if ((ret = PROCESSDIR(filename, FS_NOREAD|FS_NOWRITE|FS_NOEXEC,
			PD_CLEARSTATBITS)) < 0)
		{
			FPRINTF(STDERR,
				"%s: Cannot set permissions on %s: %s\r\n",
				ourname, filename, ERRMES(ret));
			doexit(ret);
		}
	}
	doexit(0);
}

writestr(s)
char *s;
{
	int len;
	len = strlen(s);
	dowrite(&len, sizeof(len));
	dowrite(s, len);
}

dowrite(ptr, len)
uchar *ptr;
{
	int ret;

	if ((ret = WRITE(ofd, ptr, len)) < 0)
	{
		FPRINTF(STDERR, "%s: Cannot write %s: %s\r\n",
			ourname, filename, ERRMES(ret));
		doexit(ret);
	}
}

doexit(code)
{
	if (ofd > 0)
		CLOSE(ofd);
	if (code < 0)
		UNLINK(filename);
	EXIT(code);
}

void
usage()
{
	FPRINTF(STDERR, usagemessage, ourname);
	EXIT(-1);
}
