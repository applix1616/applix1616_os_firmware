/* TABS4
 * Front end to the 'set' command: set a variable
 * and save the new settings in '$(HOME)/settings.shell'
 * You should arrange for $(HOME)/settings.shell
 * to be executed at level 1 and 2 resets to restore
 * the previous environment strings.
 */
/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */

#include <types.h>
#include <syscalls.h>
#include <passwd.h>

char *ourname;

main(argc, argv)
char *argv;
{
	int retval, uid, reload;
	char *home;
	char command200;
	struct passwd *pwd;

	ourname = argv0;

	if (argc < 2)
		usage();
	if (GETROMVER() < 0x42)
	{
		FPRINTF(STDERR, "%s: But new ROMs\r\n", ourname);
		EXIT(-1);
	}

	reload = (!strcmp(argv1, "-l") || !strcmp(argv1, "-L"));

	if (!reload)
	{
		argv0 = "set";
		retval = AEXECA(argv, 0);	/* Run the 'set' command */
		if (retval < 0)
			EXIT(retval);
	}

#ifdef notdef
	/*
	 * Use the 'set' inbuilt command to fill up the setting file
	 */
	home = (char *)GETENV(GETPID(), "home", -1);
	if (home == (char *)0)
		home = (char *)GETENV(GETPID(), "HOME", -1);
	if (home == (char *)0)
	{
		FPRINTF(STDERR,
			"sset: environment variable 'HOME' not set\r\n");
		EXIT(-1);
	}
#endif
	uid = READUID();
	pwd = getpwuid(uid);
	if (pwd == (struct passwd *)0)
	{
		FPRINTF(STDERR,
			"sset: cannot find your entry in /etc/passwd\r\n");
		EXIT(-1);
	}
	home = pwd->pw_dir;

	if (reload)
		SPRINTF(command, "%s/settings", home);
	else
	{
		SPRINTF(command, "echo set - >%s/settings.shell", home);
		EXEC(command);
		SPRINTF(command, "set >>%s/settings.shell", home);
	}
	EXIT(EXEC(command));
}

usage()
{
	FPRINTF(STDERR,
		"Usage: %s -l name=setting name=setting ...\r\n",
			ourname);
	EXIT(-1);
}
