/* TABS4 NONDOC
 * Set mode of files
 */

/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */

#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <blkio.h>
#include <blkerrcodes.h>
#include <options.h>

char *ourname;
uint ouruid;
int finalretval;

usage(n)
{
	FPRINTF(STDERR, "Usage(%d): %s +/-rwxabhl dev:/file file..\r\n",
				n, ourname);
	FPRINTF(STDERR, "       +r: Permit reading\r\n");
	FPRINTF(STDERR, "       +w: Permit writing\r\n");
	FPRINTF(STDERR, "       +x: Permit execution\r\n");
	FPRINTF(STDERR, "       +a: Set archive bit\r\n");
	FPRINTF(STDERR, "       +b: Set boring bit\r\n");
	FPRINTF(STDERR, "       +h: Set hidden bit\r\n");
	FPRINTF(STDERR, "       +l: Set locked bit\r\n");

	EXIT(-1);
}

main(argc, argv)
char *argv;
{
	register int setmask, clearmask;
	register int arg;
	register char *cp, ch;
	char gotfiles;

	ourname = argv0;
	ouruid = READUID();

	gotfiles = 0;
	finalretval = 0;
	setmask = 0;
	clearmask = 0;

	for (arg = 1; arg < argc; arg++)
	{
		cp = argvarg;
		if (*cp == '+' || *cp == '-')
		{
			if (!(ch = *cp))
				usage(1);
			/* Build mask assuming that options are to be set */
			while (*++cp)
			{
				switch (toupper(*cp))
				{
				case 'R':
					if (ch == '+')		/* Make it readable */
						clearmask |= FS_NOREAD;
					else				/* Make it non-readable */
						setmask |= FS_NOREAD;
					break;
				case 'W':
					if (ch == '+')		/* Make it writeable */
						clearmask |= FS_NOWRITE;
					else				/* Make it non-writeable */
						setmask |= FS_NOWRITE;
					break;
				case 'X':
					if (ch == '+')		/* Make it executable */
						clearmask |= FS_NOEXEC;
					else				/* Make it non-executable */
						setmask |= FS_NOEXEC;
					break;
				case 'A':
					if (ch == '+')		/* Set archive bit */
						setmask |= BACKEDUP;
					else
						clearmask |= BACKEDUP;
					break;
				case 'B':
					if (ch == '+')		/* Set boring bit */
						setmask |= FS_BORING;
					else
						clearmask |= FS_BORING;
					break;
				case 'H':
					if (ch == '+')		/* Set hidden bit */
						setmask |= FS_HIDDEN;
					else
						clearmask |= FS_HIDDEN;
					break;
				case 'L':
					if (ch == '+')
						setmask |= LOCKED;
					else
						clearmask |= LOCKED;
					break;
				default:
					usage(2);
				}
			}
		}
		else
			gotfiles = 1;		
	}

	if (!gotfiles || (!setmask && !clearmask))
		usage(3);

	/* Got permissions. Now pound through the files */
	for (arg = 1; arg < argc; arg++)
	{
		int ret;
		int bdvrnum;

		cp = argvarg;
		if (*cp == '-' || *cp == '+')
			continue;
		cp = (char *)GETFULLPATH(cp, 0);
		if ((*cp == '/') && !index(cp + 1, '/'))
		{
			/* It is a block device! */
			if (ouruid)
				yuk(cp, BEC_NOPERM);
			else
			{
				if ((bdvrnum = FIND_BDVR(cp)) >= 0)
					diddledev(cp, bdvrnum, setmask, clearmask);
				else
					yuk(cp, bdvrnum);
			}
			continue;
		}
		if (setmask)
		{
			ret = PROCESSDIR(cp, setmask, 8);
			if (ret < 0)
			{
				yuk(cp, ret);
				continue;
			}
		}
		if (clearmask)
		{
			ret = PROCESSDIR(cp, clearmask, 7);
			if (ret < 0)
			{
				yuk(cp, ret);
				continue;
			}
		}
	}
	EXIT(finalretval);
}

/*
 * Alter a block device's permission bits
 */

diddledev(name, bdvrnum, setmask, clearmask)
char *name;
{
	static char blkbufBLOCKSIZE;
	register int retval;
	register struct rootblock *rbptr;

	if ((retval = BLKREAD(ROOTBLOCK, blkbuf, bdvrnum)) < 0)
	{
		FPRINTF(STDERR, "%s: Cannot read root block of '%s': %s\r\n",
				ourname, name, retval);
		return -1;
	}

	rbptr = (struct rootblock *)blkbuf;
	rbptr->rootdir.statbits |= setmask;
	rbptr->rootdir.statbits &= ~clearmask;
	OPTION(OPT_CANWRITERB, 1);
	retval = BLKWRITE(ROOTBLOCK, blkbuf, bdvrnum);
	OPTION(OPT_CANWRITERB, 0);

	/* Force reread of device's bitmap & root block */
	BDMISC(bdvrnum, MB_REREAD, 0);
	if (retval < 0)
	{
		FPRINTF(STDERR, "%s: Cannot write root block of '%s': %s\r\n",
					ourname, name, retval);
		return -1;
	}
	return 0;
}

yuk(name, err)
{
	FPRINTF(STDERR, "%s: Cannot alter '%s': %e\r\n", ourname, name, err);
	finalretval = err;
}
