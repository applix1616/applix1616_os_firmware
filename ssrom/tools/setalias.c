/*
 * Test alias system call
 */

/* Copyright (c) 1990, Applix pty limited. Andrew Morton. */

#include <types.h>
#include <syscalls.h>
#include <mondefs.h>
#include <signal.h>

#define MAXALIASES	40

char *ourname;
int aliashandle;		/* Number of our alias vector */
char aliasing;			/* Flag: perform aliasing */
int mainpid;			/* PID of this program */
uint maxaliases;

/* A structure for command aliasing */
typedef struct analias
{
	char *old;
	char *new;
} ANALIAS;

ANALIAS *aliases;

char *roomfor();

main(argc, argv)
char *argv;
{
	int sighndl(), aliasentry();
	int i;


	ourname = argv0;

	if (argc > 1)
		maxaliases = atoi(argv1);
	else
		maxaliases = MAXALIASES;

	if (maxaliases < 10)
		usage();

	aliases = (ANALIAS *)GETMEM(maxaliases * sizeof(*aliases), 0);

	if ((long)aliases < 0)
	{
		FPRINTF(STDERR, "Out of memory\r\n");
		usage();
	}

	mainpid = GETPID();
	aliasing = 1;

	for (i = 0; i < maxaliases; i++)
		aliasesi.old = 0;

	if (GETROMVER() < 0x42)
	{
		FPRINTF(STDERR, "Buy new ROMs\r\n");
		EXIT(-1);
	}

	if (ISINTERACTIVE(GETPID()))
	{
		FPRINTF(STDERR, "You must run %s asynchronously\r\n",
				ourname);
		EXIT(-1);
	}

	/* Install signal and alias vectors */
	aliashandle = -1;	/* For race conditions */
	SIGCATCH(sighndl);
	aliashandle = ALIAS(AL_SETVEC, aliasentry);
	if (aliashandle < 0)
	{
		FPRINTF(STDERR, "alias system call returns %d\r\n",
				aliashandle);
		EXIT(aliashandle);
	}

	/* Nothing more for this PID to do but hold its memory allocated */
	for ( ; ; )
		SLEEP(0x7fffffff);		/* Bliss */
}

/*
 * The operating system calls this piece of code from within the
 * AEXECA system call. We are passed a pointer to AEXECA's pointer
 * to a list of pointers to the arguments about to be executed.
 * We are also passed the number of time AEXECA has found a match
 * in attempting to resolve aliases, which can indicate recursive
 * definitions.
 *
 * We can return:
 *	0  to indicate no interest.
 *	1  to indicate that we have modified the argument list
 * <other> to supply an address to jump to to satisfy this EXEC.
 */

aliasentry(pargv, nmatches)
register char ***pargv;
{
	register char **argv;
	register ANALIAS *aptr;
	int retval;
	int doaliascommand();

	argv = *pargv;

	if (!strucmp(argv0, "alias"))
	{
		/*
		 * The command is an 'alias' one! Tell the system where
		 * to jump to.
		 */
		return (int)doaliascommand;
	}

	if (!aliasing)
		return 0;

/* Is the command anywhere in our internal tables? */
	retval = 0;
	LOCKIN(1);	/* Reentrancy protection now */
	for (aptr = aliases; (aptr < &aliasesmaxaliases) && !retval; aptr++)
	{
		if (!strucmp(aptr->old, argv0))
		{	/* Yup. replace it */
			FREEMEM(argv0);
			argv0 = roomfor(aptr->new, 0);
			retval = 1;	/* Tell the OS we replaced it */
		}
	}
	LOCKIN(0);
	return retval;	/* Not interested */
}

/*
 * The command is 'alias', so the system schedules the following
 * piece of code. As usual, we are passed the argc, argv, argtype & argval
 * arrays.
 */

doaliascommand(argc, argv)
char *argv;
{
	register ANALIAS *aptr, *freeaptr;
	char *av1;
	char foundone;

	av1 = argv1;

	if (argc == 2)
	{
		if (!strucmp(av1, "on"))
		{	/* Enable aliasing */
			aliasing = 1;
			EXIT(0);
		}
		if (!strucmp(av1, "off"))
		{	/* Disable aliasing */
			aliasing = 0;
			EXIT(0);
		}
		if (!strucmp(av1, "exit"))
		{	/* Turf out this program */
			SIGSEND(mainpid, SIGINT, 0);
			EXIT(0);
		}
	}

	foundone = 0;
	freeaptr = (ANALIAS *)0;
	if (argc < 2)
		PRINTF("Aliasing currently %sabled\r\n",
				aliasing ? "en" : "dis");

	for (aptr = aliases; aptr < &aliasesmaxaliases; aptr++)
	{
		if (!aptr->old)
		{	/* Empty entry */
			if (freeaptr == (ANALIAS *)0)
				freeaptr = aptr;		/* Record it */
			continue;
		}

		if (argc == 1)
		{	/* Listing aliases */
			PRINTF("%s aliased to %s\r\n", aptr->old, aptr->new);
		}
		else
		{	/* Removing or adding an alias */
			LOCKIN(1);
			if (!strucmp(aptr->old, argv1))
			{	/* Found it */
				foundone = 1;
				if (argc == 2)
					relaptr(aptr);
				else
				{	/* Replacement */
					dofree(aptr->new);
					aptr->new = roomfor(argv2, 1);
				}
			}
			LOCKIN(0);
		}
	}

	if (argc == 1)
		EXIT(0);

	if ((argc == 2) && !foundone)
	{
		FPRINTF(STDERR, "%s: no such alias\r\n", argv1);
		EXIT(-1);
	}

	/* Replacing */
	if (!foundone)
	{
		if (freeaptr == (ANALIAS *)0)
		{
			FPRINTF(STDERR, "No room for new alias\r\n");
			EXIT(-1);
		}
		freeaptr->old = roomfor(argv1, 1);
		freeaptr->new = roomfor(argv2, 1);
		EXIT(0);
	}
}

sighndl()
{
	register ANALIAS *aptr;

	SLEEP(5);	/* Time for the sigsend in doaliascommand() to exit */
	if (aliashandle >= 0)
	{	/* Our vector is installed */
		ALIAS(AL_RMVEC, aliashandle);
	}

/* Free all that mode 1 memory */
	for (aptr = aliases; aptr < &aliasesmaxaliases; aptr++)
		relaptr(aptr);

	PRINTF("\r\n%s exits\r\n", ourname);
	EXIT(0);
}

relaptr(aptr)
ANALIAS *aptr;
{
	if (aptr->old)
	{
		dofree(aptr->old);
		dofree(aptr->new);
		aptr->old = 0;
	}
}

strucmp(s1, s2)
register char *s1, *s2;
{
	while (*s1)
	{
		if ((*s1 != *s2) && (toupper(*s1) != toupper(*s2)))
			break;
		s1++;
		s2++;
	}
	return *s1 - *s2;
}

/* Allocate room for & copy across a string */

char *
roomfor(cp, mode)
char *cp;
{
	char *ret;

	ret = (char *)GETMEM(strlen(cp) + 1, mode);
	strcpy(ret, cp);
	return ret;
}

dofree(addr)
char *addr;
{
	if (FREEMEM(addr) < 0)
	{
		FPRINTF(0,
"%s: Molto panico freeing memory at address 0x%x. Reset me please\r\n",
				ourname, addr);
		LOCKIN(1);
		for ( ; ; )
			;
	}
}

usage()
{
	FPRINTF(STDERR, "Usage: %s max_aliases\r\n", ourname);
	EXIT(-1);
}
