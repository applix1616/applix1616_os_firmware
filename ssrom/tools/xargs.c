/* TABS8 NONDOC
 *
 *  xargs.c:  Read strings from stdin and build multiple commands
 *
 *       Usage:  xargs command arg1 arg2 .. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syscalls.h>

static char usagemessage =
 	"Usage: %s command arg1 arg2 ...\n";

/* The name of this process: keep it global */
char *ourname;

void usage();
int doit(char *cmd);
int doexec(char *cmd, char **args, int argno);

main(argc, argv)
char *argv;
{
	char *cp;
	int arg;
	char *cmd;
	int i, retval;

	ourname = argv0;

	for (arg = 1; arg < argc; arg++)
	{
		cp = argvarg;
		if (*cp == '-')
		{	/* An option */
			char ch;
			while (*++cp)
			{	/* Process option characters */
				switch (ch = *cp)
				{
				default:	/* Unrecognised option */
					usage();
				}
			}
		}
	}

	i = 0;
	for (arg = 1; arg < argc; arg++)
	{
		i += strlen(argvarg) + 1;
	}

	cmd = malloc(i);
	*cmd = '\0';

	for (arg = 1; arg < argc; arg++)
	{
		strcat(cmd, argvarg);
		strcat(cmd, " ");
	}

	retval = doit(cmd);

	exit(retval);
}

#define CMDMAX	200

int
doit(char *cmd)
{
	char buf1000;
	char *argsCMDMAX;
	int argno;
	register char *cp;

	argno = 0;
	while (fgets(buf, sizeof(buf), stdin))
	{
		cp = buf;		/* Chop silly fgets newline */
		while (*cp && (*cp != '\n') && (*cp != '\r'))
			cp++;
		*cp = '\0';

		argsargno = malloc(strlen(buf) + 1);
		strcpy(argsargno++, buf);
		if (argno == CMDMAX)
		{
			int ret;
			if ((ret = doexec(cmd, args, argno)) < 0)
				return ret;
			argno = 0;
		}
	}

	if (argno)
	{
		int ret;
		if ((ret = doexec(cmd, args, argno)) < 0)
			return ret;
	}
	return 0;
}

/*
 * Build the command
 */

int
doexec(char *cmd, char **args, int argno)
{
	int totlen, i;
	char *thelot;
	extern int errno;
	int ret;

	totlen = strlen(cmd) + 2;
	for (i = 0; i < argno; i++)
	{
		totlen += strlen(argsi) + 1;
	}

	thelot = malloc(totlen);
	if (thelot == (char *)0)
	{
		fprintf(stderr, "%s: Out of memory!\n", ourname);
		exit(errno);
	}

	strcpy(thelot, cmd);
	strcat(thelot, " ");
	for (i = 0; i < argno; i++)
	{
		strcat(thelot, argsi);
		strcat(thelot, " ");
	}

	ret = EXEC(thelot);

	free(thelot);
	for (i = 0; i < argno; i++)
		free(argsi);
	return ret;
}

void
usage()
{
	fprintf(stderr, usagemessage, ourname);
	exit(-1);
}
