/* TABS4 NONDOC
 * Load environment file
 */

#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <process.h>
#include <blkerrcodes.h>
#include "envversion.h"

static char usagemessage =
 	"Usage: %s filename\r\n";

/* The name of this process: keep it global */
char *ourname;
char *filename;
int infd;
int envversion;

void usage();
char *readstring();

main(argc, argv)
char *argv;
{
	char *cp;
	int arg;
	int val, ret;
	char bb;
	uchar ibcarray256;
	struct dir_entry dirent;

	ourname = argv0;

	for (arg = 1; arg < argc; arg++)
	{
		cp = argvarg;
		if (*cp == '-')
		{	/* An option */
			char ch;
			while (*++cp)
			{	/* Process option characters */
				switch (ch = *cp++)
				{
				default:	/* Unrecognised option */
					usage();
				}
			}
		}
		else
		{
			if (filename == (char *)0)
				filename = cp;
			else
				usage();
		}
	}

	/* Check that both compulsory args were supplied */
	if (filename == (char *)0)
		usage();

	if ((ret = FILESTAT(filename, &dirent)) < 0)
	{
		FPRINTF(STDERR, "%s: Cannot stat %s: %s\r\n",
			ourname, filename, ERRMES(ret));
		doexit(ret);
	}

	if (dirent.uid)
	{
		FPRINTF(STDERR, "%s: '%s' not owned by UID 0: abort\r\n",
			ourname, filename);
		SETUID(0);
		UNLINK(filename);
		doexit(BEC_NOPERM);
	}

	if ((infd = OPEN(filename, O_RDONLY)) < 0)
	{
		FPRINTF(STDERR, "%s: Cannot open '%s': %s\r\n",
			ourname, filename, ERRMES(infd));
		doexit(infd);
	}

	doread(&envversion, sizeof(envversion));
	if (envversion != ENVVERSION)
	{
		FPRINTF(STDERR, "%s: environment save file '%s' wrong version\r\n",
				ourname, filename);
	}

	/* Set environment bits */
	doread(&val, sizeof(val));

	if ((ret = SETENVBITS(GETPID(), -1, 0)) < 0)	/* Clear all bits */
	{
		FPRINTF(STDERR, "%s: Cannot clear environment bits: %s\r\n",
			ourname, ERRMES(ret));
		doexit(ret);
	}

	if ((ret = SETENVBITS(GETPID(), val, 1)) < 0)	/* Set bits */
	{
		FPRINTF(STDERR, "%s: Cannot set environment bits: %s\r\n",
			ourname, ERRMES(ret));
		doexit(ret);
	}

	/* Set ibcvec */
	doread(&bb, 1);
	if (bb)
	{
		doread(ibcarray, 256);
		if ((ret = SETIBCVEC(ibcarray)) < 0)
		{
			FPRINTF(STDERR, "%s: Cannot set command disable vector: %s\r\n",
				ourname, ERRMES(ret));
			doexit(ret);
		}
	}

	/* Read environment strings */
	TRASHENVSTRINGS(GETPID());
	while ((uint)(ret = TELL(infd)) < dirent.file_size)
	{
		char *name, *setting;
		ushort mode;

		doread(&mode, sizeof(mode));
		name = readstring();
		setting = readstring();
		if ((ret = SETENV(GETPID(), name, setting, mode)) < 0)
		{
			FPRINTF(STDERR,
				"%s: Cannot set environment string '%s' to '%s': %s\r\n",
				ourname, name, setting, ERRMES(ret));
			doexit(ret);
		}
		FREEMEM(name);
		FREEMEM(setting);
	}

	if (ret < 0)
	{
		FPRINTF(STDERR, "%s: Error reading %s: %s\r\n",
			ourname, filename, ERRMES(ret));
		doexit(ret);
	}

	doexit(0);
}

doread(ptr, len)
uchar *ptr;
{
	int ret;

	if ((ret = READ(infd, ptr, len)) != len)
	{
		FPRINTF(STDERR, "%s: Error reading %s: %s\r\n",
			ourname, filename,
			(ret < 0) ? (char *)ERRMES(ret) : "short file");
		doexit((ret < 0) ? ret : -1);
	}
}

char *
readstring()
{
	char *s;
	uint len;

	doread(&len, sizeof(len));
	if (len > 500)
	{
		FPRINTF(STDERR, "%s: Environment string too long (%d) in '%s'\r\n",
			ourname, len, filename);
		doexit(-1);
	}

	s = (char *)GETMEM(len + 1, 0);
	doread(s, len);
	slen = '\0';
	return s;
}

doexit(code)
{
	if (infd > 0)
		CLOSE(infd);
	EXIT(code);
}

void
usage()
{
	FPRINTF(STDERR, usagemessage, ourname);
	EXIT(-1);
}
