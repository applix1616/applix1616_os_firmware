/* TABS4 NONDOC
 * Do things to V4.2 char devices
 */

/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */

#include <types.h>
#include <syscalls.h>
#include <chario.h>
#include <scc.h>
#include <reloc.h>

#define NELEM(array)	(sizeof(array)/sizeof(*array))

#define BAUD		1
#define RXBITS		2
#define TXBITS		3
#define PARITY		4
#define STOPBITS	5
#define LL			6			/* Last lines */
#define S			0x40000000
#define R			0x20000000

char *ourname;
struct scc_prog scc_prog;
char mustprog;
int dev;
int retval;
struct chardriver *chardriver;
char fromstdin;

struct table
{
	char *cmd;			/* User input */
	int setcmd;			/* cdmisc command to set it */
	int readcmd;		/* cdmisc command to read it */
	char *msg;			/* A description */
	char printstuff;	/* If zero, don't print read info */
	char numericonly;	/* If true, can only be assigned to number */
	char superonly;		/* Can only be done by UID 0 */
} table =
{
{ "SIGCHAR", CDM_SETSIGCHAR, CDM_READSIGCHAR, "Signal character", 1, 1, 0},
{ "EOFCHAR", CDM_SETEOFCHAR, CDM_READEOFCHAR, "EOF character", 1, 1, 0},
{ "RESETCHAR", CDM_SETRESETCHAR, CDM_READRESETCHAR, "Reset character", 1, 1, 1},
{ "XOFFCHAR", CDM_SETXOFFCHAR, CDM_READXOFFCHAR, "XOFF character", 1, 1, 0},
{ "XONCHAR", CDM_SETXONCHAR, CDM_READXONCHAR, "XON character", 1, 1, 0},
{ "RAWMODE", CDM_SETRAWMODE, CDM_READRAWMODE, "Raw mode", 1, 1, 0},
{ "SIGINT", 0, CDM_SENDSIGINT, "Send SIGINT", 0, 1, 1},
{ "SIGHUP", 0, CDM_SENDSIGHUP, "Send SIGHUP", 0, 1, 1},
{ "KILLUSER", CDM_KILLUSER, 0, "Send KILLUSER", 0, 1, 1},
{ "DTR", CDM_SETDTR, 0, "DTR signal", 1, 1, 1},
{ "RTS", CDM_SETRTS, 0, "RTS signal", 1, 1, 1},
{ "DCD", 0, CDM_READDCD, "DCD signal", 1, 1, 0},
{ "CTS", 0, CDM_READCTS, "CTS signal", 1, 1, 0},
{ "BREAK", CDM_SETBREAK, CDM_READBREAK, "Break condition", 1, 1, 1},
{ "HFC", CDM_SETHFC, CDM_READHFC, "Hardware flow control", 1, 1, 1},
{ "HUPMODE", CDM_SETHUPMODE, CDM_READHUPMODE, "Hangup on DCD low", 1, 1, 1},
{ "HASMISC", 0, CDM_HASMISCVEC, "Miscellaneous support", 1, 1, 0},
{ "PERMS", CDM_SETPERM, CDM_READPERM, "Permission bits", 1, 1, 1},
{ "TXCOUNT", 0, CDM_TXCOUNT, "Tx buffer count", 1, 1, 0},
{ "RXCOUNT", 0, CDM_RXCOUNT, "Rx buffer count", 1, 1, 0},
{ "TXROOM", 0, CDM_TXROOM, "Tx buffer free count", 1, 1, 0},
{ "RXROOM", 0, CDM_RXROOM, "Rx buffer free count", 1, 1, 0},
{ "TXFLUSH", 0, CDM_TXFLUSH, "Flush transmit buffer", 0, 1, 0},
{ "TXPURGE", 0, CDM_TXPURGE, "Purge transmit buffer", 0, 1, 0},
{ "RXPURGE", 0, CDM_RXPURGE, "Purge receive buffer", 0, 1, 0},
{ "RXPEEK", 0, CDM_RXPEEK, "Peek at receive buffer", 1, 1, 0},
{ "VERSION", 0, CDM_VERSION, "Driver version", 1, 1, 0},
{ "BAUD", BAUD|S, BAUD|S, "Baud rate", 1, 1, 1},
{ "RXBITS", RXBITS|S, RXBITS|R, "Receive bits", 1, 1, 1},
{ "TXBITS", TXBITS|S, TXBITS|R, "Transmit bits", 1, 1, 1},
{ "PARITY", PARITY|S, PARITY|R, "Parity", 1, 1, 1},
{ "STOPBITS", STOPBITS|S, STOPBITS|R, "Stop bits", 1, 1, 1},
{ "TXBSIZE", CDM_SETTXBSIZE, CDM_READTXBSIZE, "Tx buffer size", 1, 1, 1},
{ "RXBSIZE", CDM_SETRXBSIZE, CDM_READRXBSIZE, "Rx buffer size", 1, 1, 1},
{ "RXTOTAL", 0, CDM_RXTOTAL, "channel receive count", 1, 1, 0},
{ "TXTOTAL", 0, CDM_TXTOTAL, "channel transmit count", 1, 1, 0},
{ "XLATEESC", CDMB_XLATEESC, -1, "escape code translation", 1, 1, 1},
{ "RXSIGPURGE", CDMB_RXSIGPURGE, -1, "purge Rx on signal", 1, 1, 0},
{ "TXSIGPURGE", CDMB_TXSIGPURGE, -1, "purge Tx on signal", 1, 1, 0},
{ "LL", LL|S, LL|R, "Set/get last lines", 0, 0, 0}
};

main(argc, argv)
char *argv;
{
	int arg;
	int romver;
	char *cp;
	int goodval;

	ourname = argv0;

	romver = GETROMVER();
	if (romver < 0x42)
	{
		FPRINTF(STDERR, "%s: Buy new ROMs\r\n", ourname);
		EXIT(-1);
	}
#ifdef notdef
	if (romver > 0x43)
	{
		FPRINTF(STDERR,
	"%s: \007Warning. This program was written for 1616/OS version 4.3.\r\n",
			ourname);
		FPRINTF(STDERR,
	"It may be incompatible with version %d.%d\r\n",
				romver / 16, romver % 16);
	}
#endif
	if (argc < 2)
		usage(0);

	cp = argv1;
	while (*cp)
		cp++;

	if (argc == 2)
	{
		if (!strucmp(argv1, "-v") || !strucmp(argv1, "-q"))
		{
			help();
			EXIT(0);
		}
		if (cp-1 == ':')
			usage(0);
	}

	if (cp-1 != ':')
	{
		dev = SET_SIP(-1);		/* Read stdin */
		arg = 1;
		fromstdin = 1;
	}
	else
	{
		dev = OPEN(argv1, 1);
		arg = 2;
		fromstdin = 0;
	}

	if (dev < 0 || dev >= NIODRIVERS)
	{
		FPRINTF(STDERR, "%s: Cannot open '%s': %s\r\n", ourname,
					fromstdin ? "stdin" : argv1, (dev < 0) ?
						(char *)ERRMES(dev) : "No such character device\r\n");
		EXIT(-1);
	}

	chardriver = (struct chardriver *)FIND_DRIVER(0, dev);

	/* Get an image of current line mode */
	mustprog = 0;
	cdmisc(CDM_READMODE, &scc_prog);

	retval = 0;
	for ( ; arg < argc; arg++)
	{
		char cmd200;
		register char *cpin, *cpout;
		char *cpisave;
		int val;

		cpin = argvarg;
		cpout = cmd;

		/* Split up the command */
		while (*cpin && (*cpin != '='))
			*cpout++ = *cpin++;
		*cpout = '\0';

		if (*cpin)
		{
			cpin++;		/* Skip '=' */
			if (!*cpin)
			{
				bad(argvarg);
				retval = -1;
				continue;
			}
		}
		cpisave = cpin;

		val = 0;
		goodval = 1;
		while (*cpin)
		{
			if (*cpin >= '0' && *cpin <= '9')
				val = val * 10 + *cpin++ - '0';
			else
			{
				goodval = 0;
				break;
			}
		}
		doit(cmd, cpisave, val, goodval);
	}

	if (mustprog)
	{
		if (cdmisc(CDM_SETMODE, &scc_prog) < 0)
		{
			FPRINTF(STDERR, "%s: Error reprogramming %s\r\n",
					ourname, argv1);
			retval = -1;
		}
	}
	EXIT(retval);
}

doit(cmd, setting, val, goodval)
register char *cmd, *setting;
register int val;
{
	register struct table *tbl;
	int i;
	int found;

	found = 0;
	for (i = 0; i < NELEM(table); i++)
	{
		tbl = table + i;
		if (!strucmp(cmd, tbl->cmd))
		{
			found = 1;
			if (tbl->numericonly && *setting && !goodval)
			{
				FPRINTF(STDERR, "%s: %s: can only be assigned numeric values\r\n",
									ourname, cmd);
				retval = -1;
				return;
			}
			if (*setting && (tbl->setcmd == 0))
			{
				FPRINTF(STDERR,
					"%s: Cannot assign a value with the '%s' (%s) command\r\n",
						ourname, cmd, tbl->msg);
				retval = -1;
				return;
			}
			if (*setting && tbl->superonly && READUID())
			{
				FPRINTF(STDERR, "%s: Permission denied on '%s' command\r\n",
					ourname, cmd);
				retval = -1;
				return;
			}
			if (!*setting && (tbl->readcmd == 0))
			{
				FPRINTF(STDERR,
					"%s: Cannot read back a value with the '%s' (%s) command\r\n",
						ourname, cmd, tbl->msg);
				retval = -1;
				return;
			}

			if ((tbl->readcmd != -1) &&
				((tbl->setcmd & S) || (tbl->readcmd & R)))
			{	/* It is an internal command for baud rate, etc? */
				unsigned short *up;
				int adj = 0;
				up = 0;

				switch (tbl->setcmd & 0xff)
				{
				case BAUD:
					up = &scc_prog.brate;
					break;
				case RXBITS:
					up = &scc_prog.rxbits;
					adj = 5;
					break;
				case TXBITS:
					up = &scc_prog.txbits;
					adj = 5;
					break;
				case PARITY:
					up = &scc_prog.parity;
					break;
				case STOPBITS:
					up = &scc_prog.stopbits;
					break;
				case LL:
					dolastlines(setting);
					break;
				default:
					FPRINTF(STDERR, "Yuk!\r\n");
					EXIT(-1);
				}

				if (up)
				{
					if (*setting)
					{
						*up = val - adj;
						mustprog = 1;
					}
					else if (tbl->printstuff)
					{
						PRINTF("%s: %d\r\n", tbl->msg, *up + adj);
					}
				}
			}
			else
			{	/* cdmisc call */
				if (*setting)
				{
					int ret;

					if (tbl->setcmd == CDM_SETPERM)
						ret = cdmisc(tbl->setcmd, 2, val);
					else if (tbl->readcmd == -1)		/* modebits */
						ret = cdmisc(CDM_SETMODEBITS, val ? 0:1, tbl->setcmd);
					else
						ret = cdmisc(tbl->setcmd, val);
					if (ret < 0)
					{
						FPRINTF(STDERR, "%s: Error setting %s (%s)\r\n",
							ourname, cmd, tbl->msg);
						retval = -1;
					}
				}
				else
				{
					int ret;
					if (tbl->readcmd == -1)
					{	/* modebits */
						ret = ((cdmisc(CDM_SETMODEBITS, 4)&tbl->setcmd) != 0);
					}
					else if ((ret = cdmisc(tbl->readcmd)) < 0)
					{
						FPRINTF(STDERR, "%s: Error reading %s (%s)\r\n",
								ourname, cmd, tbl->msg);
						retval = -1;
					}
					if (tbl->printstuff)
						PRINTF("%s: %d\r\n", tbl->msg, ret);
				}
			}
		}
	}
	if (!found)
	{
		FPRINTF(STDERR, "%s: %s: Invalid command\r\n", ourname, cmd);
		usage(1);
		retval = -1;
	}
}

cdmisc(p1, p2, p3)
{
	return CDMISC(dev, p1, p2, p3, 0);
}

bad(cmd)
char *cmd;
{
	FPRINTF(STDERR, "%s: %s: Unrecognised command\r\n", ourname, cmd);
}

/*
 * Read/write lastlines
 */

dolastlines(setting)
char *setting;
{
	char totty;				/* True if to screen */
	int fd;
	int nlastlines;
	struct mrdriver *mrdriver;
	int i;
	int n;
	char **lastlines;

	totty = (SET_SOP(-1) < NIODRIVERS);

	/* Work out how many lastlines there are */
	mrdriver = (struct mrdriver *)CALLMRD(-1, 0, 0);
	nlastlines = mrdriver->nlastlines;

	if (*setting)			/* Setting the lastlines */
	{
		char buf500;

		if (!nlastlines)
		{
			FPRINTF(STDERR, "%s: nlastlines = 0!\r\n", ourname);
			return;
		}

		fd = OPEN(setting, 1);
		if (fd < 0)
		{
			FPRINTF(STDERR, "%s: Cannot open %s: %E\r\n", ourname, setting, fd);
			retval = -1;
			return;
		}
		DUMPLASTLINES(dev);

		lastlines = (char **)GETMEM(nlastlines * sizeof(*lastlines), 1);
		for (i = 0; i < nlastlines; i++)
			lastlinesi = (char *)0;

		/* Install in reverse. First line in file is most recent command */
		for (i = 0; i < nlastlines; i++)
		{
			n = nlastlines - 1 - i;
			if (FGETS(fd, buf) <= 0)
				break;
			lastlinesn =
					(char *)GETMEM(strlen(buf) + 1, 1);
			strcpy(lastlinesn, buf);
		}

		chardriver->lastlines = lastlines;
		CLOSE(fd);
	}
	else
	{		/* Printing lastlines */
		lastlines = chardriver->lastlines;
		if (lastlines)
		{
			if (totty)
				PRINTF("Maximum number of last lines: %d\r\n\r\n", nlastlines);

			/* If to file, print most recent first */
			for (i = 0; i < nlastlines; i++)
			{
				n = (totty) ? i : nlastlines - 1 - i;
				if (lastlinesn)
					PRINTF("%s\r\n", lastlinesn);
			}
		}
	}
}

strucmp(s1, s2)
register char *s1, *s2;
{
		while (*s1)
		{
			if (toupper(*s1) != toupper(*s2))
				return 1;
			s1++;
			s2++;
		}
		return *s2;
}

usage(n)
{
	FPRINTF(STDERR, "\r\n");
	FPRINTF(STDERR, "Usage: %s DEV: setting ... setting=VAL ...\r\n",
			ourname);
	FPRINTF(STDERR, "or:    %s -q   for a listing of settings\r\n",
			ourname);
	FPRINTF(STDERR, "DEV: defaults to %s's standard input\r\n", ourname);
	FPRINTF(STDERR, "\r\n");
	if (!n)
		EXIT(-1);
}

help()
{
	int i;
	static char fmt = "%-10s %-25s %c %-10s %-25s\r\n";
	int n;

	PRINTF("\r\n");
	PRINTF(fmt, "Setting", "    Usage", ' ', "Setting", "    Usage");
	PRINTF(fmt, "-------", "    -----", ' ', "-------", "    -----");
	PRINTF("\r\n");

	n = NELEM(table);
	for (i = 0; i < n; i += 2)
	{
		PRINTF(fmt, tablei.cmd, tablei.msg,
				(i < (n-1)) ? '|':' ',
				(i < (n-1)) ? tablei+1.cmd:"",
				(i < (n-1)) ? tablei+1.msg:"");
	}
	PRINTF("\r\n");
}
