/* TABS8 NONDOC
 * Set the homeshell Umask
 */

#include <syscalls.h>
#include <types.h>
#include <files.h>

char *ourname;

main(argc, argv)
char *argv;
{
	int umask = FS_NOREAD|FS_NOEXEC|FS_NOWRITE;
	char *cp;

	ourname = argv0;
	if (argc > 2)
		usage();
	if (argc == 1)
	{
		umask = SETPROCUMASK(GETPID(), -1);
		PRINTF("Current umask: $%04x  ", umask);
		PRINTF(umask & BACKEDUP ? "A" : " ");
		PRINTF(umask & FS_BORING ? "B" : " ");
		PRINTF(umask & FS_HIDDEN ? "H" : " ");
		PRINTF(umask & LOCKED ? "L" : " ");
		PRINTF(umask & FS_NOREAD ? " " : "R");
		PRINTF(umask & FS_NOWRITE ? " " : "W");
		PRINTF(umask & FS_NOEXEC ? " " : "X");
		PRINTF("\r\n");
		EXIT(0);
	}

	cp = argv1;
	while (*cp)
	{
		switch (toupper(*cp++))
		{
		case 'A':
			umask |= BACKEDUP;
			break;
		case 'B':
			umask |= FS_BORING;
			break;
		case 'H':
			umask |= FS_HIDDEN;
			break;
		case 'L':
			umask |= LOCKED;
			break;
		case 'R':
			umask &= ~FS_NOREAD;
			break;
		case 'W':
			umask &= ~FS_NOWRITE;
			break;
		case 'X':
			umask &= ~FS_NOEXEC;
			break;
		case '-':
			break;
		default:
			usage();
		}
	}

	SETPROCUMASK(FINDHOMESHELL(GETPID()), umask);
	EXIT(0);
}

usage()
{
	FPRINTF(STDERR, "Usage: %s A|B|H|L|R|W|X|-\r\n", ourname);
	EXIT(-1);
}
