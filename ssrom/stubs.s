#
# Stub out things not used in the gpcpu implementation
#
		.globl	gr_init,_sdotmode,_sgfgcol,_rcircle,_circle,_fill
		.globl	anio_init
		.globl	_def_fkey,_set_kvec,kb_init,cent_init,_rset_pel
		.globl	_set_pel,_rline,_line,_rread_pel,_read_pel
		.globl	_sgbgcol,_sgtexture

		.globl	video_init,_set_640,_set_vdp,_set_vap,_set_fgcol
		.globl	_set_bgcol,_set_bdcol,_set_pal,_rdch_shape,draw_vchar
		.globl	_def_chshape,_def_wind,_vid_address,_move_wind
		.globl	_fill_wind,_scurs_mode,_newchset,_mousetrap
		.globl	_caswraw, _casrraw, _anipsel, _anopsel, _anopdis
		.globl	_adc, _dac, _set_led, _freetone, _fttime, _rdiport
		.globl	_rdbiport, cas_init, _sine, _ent1ints, _dist1ints

/*_caswraw:
_casrraw:
_anipsel:
_anopsel:
_anopdis:
_adc:
_dac:
_set_led:
_freetone:
_fttime:
_rdiport:
_rdbiport:
_sine:
cas_init:
gr_init:
anio_init:
_sdotmode:
_sgfgcol:
_rcircle:
_circle:
_fill:
_def_fkey:
_set_kvec:
kb_init:
cent_init:
_rset_pel:
_set_pel:
_rline:
_line:
_rread_pel:
_read_pel:
_sgbgcol:
_sgtexture:
video_init:
_set_640:
_set_vdp:
_set_vap:
_set_fgcol:
_set_bgcol:
_set_bdcol:
_set_pal:
_rdch_shape:
draw_vchar:
_def_chshape:
_def_wind:
_vid_address:
_move_wind:
_fill_wind:
_scurs_mode:
_newchset:
_mousetrap:
_ent1ints:
_dist1ints:
		rts*/
		.data	1
		.comm	enkbreset,4
		.comm	enalts,4
		.comm	enaltspecial,4

