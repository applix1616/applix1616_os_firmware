/* TABS4 NONDOC
 * Monitor command execution functions
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <stdbool.h>
#include <ctype.h>

#ifdef A1616
#include "cassette.h"
#endif
#include <files.h>
#include <blkerrcodes.h>
#include "envbits.h"
#include <process.h>

#include "mondefs.h"
#include "prcrlf.h"
#include "strucmp.h"
#include "psprintf.h"
#include "fcb.h"
#include "blockio.h"
#include "filefns.h"
#include "dirfns.h"
#include "newmem.h"
#include "datetime.h"
#include "inbuilt.h"
#include "movmem.h"
#include "nomemfor.h"
#include "monfns.h"

char *dowa(), *putinmem();
char hextab[] = "0123456789ABCDEF";

extern char *cur_path;		/* Full path of current directory */
extern uchar *dumpaddr;		/* Defined in MON.C */

extern void prbyte(uint ch);
extern int prch(char ch);
extern int dodir(char *path,int mode,int *pcolumn);
extern void pde(struct dir_entry *dirptr,char * path,int mode,int *pcolumn);
extern int rdallmode(char *path,long *pdirsize);
extern int direrr(char *file,int ec);
extern int cantcreat(char *file,int ec);
extern int cantwrite(char *file,int fd,int ec);



/*
 * Dump out memory from *first to *last inclusive. Size = 1, 2 or 4 for byte,
 * word or long. Do not print final linefeed.
 */

void dm(uchar *first, uchar *last,ushort size)
{
	register unsigned chbi;		/* Index to chbuf */
	register ushort i, col;
	char chbuf[50];			/* For char data */

	if (size > 1)		/* Even address if necessary */
		first = (uchar *)((int)first & ~1);

	do
	{
		PRINTF("%06X-", first);
		col = 9;
		chbi = 0;
		do
		{
			for (i = 0; i < size; i++)
			{
				prbyte(*first);
				col += 2;
				chbuf[chbi++] = prch(*first++);
				chbuf[chbi] = '\0';
			}
			PUTCHAR(' ');
			col++;
		}
		while (
			(first <= last) &&
			((size == 4) || ((int)first & 15)) &&
			(chbi != 16)
			);

		for ( ; col < 58; PUTCHAR(' '), col++)
				;	/* Tab out */
		PRINTF("->%s<-", chbuf);
		if (first <= last)
			prcrlf();
	}
	while (first <= last);
	dumpaddr = first;			/* Set up new default */
}

/*
 * Write memory. If > 2 args, write it straight in, else prompt for I/P
 * If size == 0, ascii string write.
 * If size == 5, ascii string with trailing null.
 */

int wm(int argc,char *argv[],uint *argval,ushort size)
{
	register char *ptr, *as0;
	register unsigned short moveval, i;
	char lbuf[MAXIPLINE];

	moveval = (size) ? size : 1;

	ptr = (char *)( argval[1] );		/* Pointer to target dest */
#if 0
	if (((size == 2) || (size == 4)) && ((int)ptr & 1)) /* Odd address */
	{
		return eprintf("Odd address\r\n");
	}
#endif
	if (argc > 2)			/* Direct write */
	{
		switch (size)
		{
		case 0:
			ptr = dowa(ptr, argv[2], 1);	/* Write string */
			break;
		case 5:
			ptr = dowa(ptr, argv[2], 0);
			break;
		default:
			for (i = 2; i < argc; i++)
			{
				ptr = putinmem(ptr, size, argval[i]);
			}
		}
	}
	else
	{	/* Prompted input */
		char *args[MAXMONPAR + 1];
		char argt[MAXMONPAR];		/* Types of args */
		uint argnums[MAXMONPAR];	/* Values */
		int nar;			/* Number of args */
		for ( ; ; )
		{
reenter:		printmem(ptr, size);
			PRINTF(": ");
			lbuf[0] = '\0';
			LEDIT(lbuf);		/* Get input */
			nar = SLICEARGS(lbuf, args, 0);	/* Chop it up */
			CLPARSE(args, argt, argnums);
			as0 = args[0];

			if (nar == 0)
			{	/* Go to next location */
				ptr += moveval;
				goto reenter;
			}

			if (!strucmp(as0, "."))
				return 0;

			if (!strucmp(as0, "R"))
			{
				ptr -= moveval;
				goto reenter;
			}

			switch (size)
			{
			case 0:		/* Ascii string */
				ptr = dowa(ptr, as0, 1);
				break;
			case 5:		/* Ascii, trailing null */
				ptr = dowa(ptr, as0, 0);
				break;
			default:
				if (argt[0] != NUMBER)
				{
					eprintf("Bad number\r\n");
					goto reenter;
				}
				ptr = putinmem(ptr, size, argnums[0]);
				break;
			}
		}
	}
	return 0;
}

/*
 * Expression evaluator. Scan line, getting ops and numbers
 */

int expr(int argc,char *argv[],uint *argval)
{
	register uint val;
	register uint i;
	register uint av;		/* argval copy */
	register char badexm;
	register char *as;		/* argv copy */

	if (argc & 1)
	{
		badexm = 0;
		goto badex;	/* Must be an even number */
	}
	val = argval[1];
	for (i = 2; i < argc; i += 2)
	{
		as = argv[i];
		av = argval[i + 1];
		if (strlen(as) > 1)
		{
			badexm = 0;
			goto badex;
		}
		switch (*as)
		{
		case 'x':
		case 'X':
		case '*':
			val *= av;
			break;
		case '/':
			if (!av)	/* Check divide by zero */
			{
				badexm = 1;
				goto badex;
			}
			val /= av;
			break;
		case '+':
			val += av;
			break;
		case '-':
			val -= av;
			break;
		case '%':
			if (!av)	/* Check divide by zero */
			{
				badexm = 1;
				goto badex;
			}
			val %= av;
			break;
		case '&':
			val &= av;
			break;
		case '|':
			val |= av;
			break;
		case '^':
			val ^= av;
			break;
		default:
			badexm = 0;
			goto badex;
		}
	}
	PRINTF("Result: %X, .%d, %%%b\r\n", val, val, val);
	return 0;

badex:	return eprintf(badexm ? "Division by zero\r\n" : "Bad expression\r\n");
}

/*
 * List file directory.
 * Mode = 0: Full listing
 * Mode = 1: Names only
 */

int dir(int argc,char *argv[],int mode)
{
	register int i, retval;
	int column;

	column = 0;

	if (argc == 1)
		retval = dodir(cur_path, mode, &column);
	else
	{
		for (i = 1; i < argc; i++)
		{
			if ((retval = dodir((char *)argv[i], mode, &column)) < 0)
				break;
		}
	}
	if (column && mode)
		prcrlf();	/* Clean up last linefeed */
	return retval;
}

int dodir(char *path,int mode,int *pcolumn)
{
	register int bdvrnum;
	register int i;
	register int retval;
	register struct dir_entry *pdir;
	register char *fullpath;
	int totblks;
	int fblksused = 0;
	int tblksused = 0;
	int tbytesused = 0;
	int nfiles = 0;
	struct dir_entry dirent;
	struct dir_entry *opdir;
	uint dirsize;

	/* May be a directory spec */
	fullpath = (char *)GETFULLPATH(path, 0);
	if ((retval = bdvrnum = get_bdev(fullpath, 0)) < 0)
		goto gotdirerr;
	if ((retval = newdisk(bdvrnum)) < 0)
		goto gotdirerr;
	if ((retval = FILESTAT(fullpath, &dirent)) < 0)
		goto gotdirerr;
	if (!(dirent.statbits & DIRECTORY))
	{	/* It is not a directory: print out the entry */
		if (!hidden(&dirent))
			pde(&dirent, path, mode, pcolumn);
		dofreemem(fullpath);
		return 0;
	}

	*pcolumn = 0;
/* Full directory listing: read in the entire directory */
	if ((retval = rdallmode(fullpath, (long *)dirsize)) < 0)
		goto gotdirerr;
	opdir = pdir = (struct dir_entry *)retval;

	PRINTF("\r\nDevice: %s    Volume name: %s    Directory: %s\r\n",
		bdrivers[bdvrnum].devname,
		bdrivers[bdvrnum].rootblock.rootdir.file_name, fullpath);

	if (!mode)
		PRINTF("\r\n Attr    User   Length      Load             Date\r\n\r\n");
	else
		prcrlf();

	for (i = 0; i < dirsize; i++, pdir++)
	{
		if (!pdir->file_name[0] || hidden(pdir))
			continue;
		nfiles++;
		pde(pdir, pdir->file_name, mode, pcolumn);
		if (pdir->statbits & DIRECTORY)
		{
			fblksused += pdir->file_size;
			tbytesused += pdir->file_size * BLOCKSIZE;
		}
		else
		{
			tbytesused += pdir->file_size;
			fblksused += ((pdir->file_size + BSIZEM1) / BLOCKSIZE) + 1;
		}
	}
	dofreemem(opdir);
	prcrlf();
	*pcolumn = 0;
	if (mode == 1)
		prcrlf();
gotdirerr:
	if (retval < 0)			/* Some error */
		direrr(fullpath, retval);
	dofreemem(fullpath);
	if (retval < 0)
		return retval;

	tblksused = BDMISC(bdvrnum, MB_USEDBLOCKS, 0);
	totblks = BDMISC(bdvrnum, MB_NBLOCKS, 0);

	PRINTF("%d bytes (%d blocks) used in %d file%s, %d used, %d free, %d total\r\n\r\n",
		tbytesused, fblksused, nfiles, (nfiles == 1) ? "" : "s", /* WvB */
		tblksused, totblks - tblksused, totblks);

	return 0;
}

/* Do a rdalldir in current dir mode */
int rdallmode(char *path,long *pdirsize)
{
	return RDALLDIR(path, 0,
		envbits(ENVB_DIRMODE0) ? (envbits(ENVB_DIRMODE1) ? 2 : 1) : 0,
			pdirsize);
}

/*
 * Print out a directory entry
 */

void pde(struct dir_entry *dirptr,char * path,int mode,int *pcolumn)
{
	register ushort statbits;
	register int column, len;
	extern char slash[];
	char dtbuf[50];

	if (mode == 0)
	{
		statbits = dirptr->statbits;
		PRINTF("%c%c%c%c%c%c | %3d | %8d | %8x | %s | %s\r\n",
			statbits & DIRECTORY ? 'D' : '-',
			statbits & LOCKED ? 'L' : '-',
			statbits & BACKEDUP ? 'A' : '-',
			statbits & FS_NOREAD ? '-' : 'R',
			statbits & FS_NOWRITE ? '-' : 'W',
			statbits & FS_NOEXEC ? '-' : 'X',
			dirptr->uid,
			dirptr->file_size * ((dirptr->statbits & DIRECTORY) ?
				BLOCKSIZE : 1),
			dirptr->load_addr,
			dtformat(dirptr->date, dtbuf),
			path);
		return;
	}

	/* Short form */
	column = *pcolumn;
	strcpy(dtbuf, dirptr->file_name);
	if (dirptr->statbits & DIRECTORY)
		strcat(dtbuf, slash);
	len = strlen(dtbuf) + 1;
	if (column + len >= 80)
	{
		prcrlf();
		column = 0;
	}
	PRINTF("%s ", dtbuf);
	column += strlen(dtbuf) + 1;
	if (column > 64)
	{
		prcrlf();
		column = 0;
	}
	else while (column & 15)
	{
		PUTCHAR(' ');
		column++;
	}
	*pcolumn = column;
}

/*
 * Copy files: the 'cat' command. We have at least one argument
 */

int cat(int argc,char *argv[])
{
	register int i;
	register int ifd;
	register int retval;

	/* Go through all the files */
	for (i = 1; i < argc; i++)
	{
		if ((ifd = OPEN(argv[i], SSO_RDONLY)) < 0)
		{
			cantopen(argv[i], ifd);
			continue;
		}
		if ((retval = pump(ifd, STDOUT, argv[i], "output")) < 0)
		{
			CLOSE(ifd);
			goto err;
		}
		CLOSE(ifd);
	}
	retval = 0;
err:	return retval;
}

/*
 * Save memory onto a file
 */

int msave(char *argv[],uint *argval)
{
	register int ofd;
	register int retval;

	if ((ofd = CREAT(argv[3], 0, argval[1])) < 0)
		return cantcreat(argv[3], ofd);
	if ((retval = WRITE(ofd, argval[1], argval[2] - argval[1] + 1)) < 0)
		return cantwrite(argv[3], ofd, retval);
	return doclose(argv[3], ofd);
}

/*
 * Load memory from a file. If flag = 0, it is 'mload' command. If flag = 1 or 2,
 * load file for execution using passed name. If flag == 1, no error messages
 */

int mload(int argc,char *argv[],uint *argval,int flag,char *name)
{
	register int ifd, nread;
	register int retval;
	register char *laddr;
	register char *ifname;
	struct dir_entry dirent;

	switch (flag)
	{
	case 0:
		ifname = argv[1];
		break;
	default:
		ifname = name;
		break;
	}

	if ((retval = FILESTAT(ifname, &dirent)) < 0)
	{
		if (flag != 1)
			cantopen(ifname, retval);
		return retval;
	}
	laddr = ((flag == 0) && (argc == 3)) ?
			(char *)argval[2] : dirent.load_addr;
/* If it is not a 'ml fname xxxx' command, check load address */
	if ((flag != 0) || (argc != 3))
	{
		if (laddr < (char *)TPALOC)
		{
			if (flag != 1)
				badladdr(ifname);
			return -1;
		}
	}

	if ((ifd = OPEN(ifname, SSO_RDONLY)) < 0)
	{
		if (flag != 1)
			cantopen(ifname, ifd);
		return ifd;
	}
	nread = READ(ifd, laddr, dirent.file_size);
	if (nread != dirent.file_size)
	{
		if (flag != 1)
			cantread(ifname, ifd, nread);
		CLOSE(ifd);
		return nread;
	}
	if (flag == 0)
		prloaded(nread,(int) laddr);
	ifd = CLOSE(ifd);
	if ((ifd < 0) && (flag != 1))
		cantdo(ifname, ifd, "close");
	return (ifd < 0) ? -1 : (int)laddr;
}

void prloaded(int nbytes,int addr)
{
	PRINTF("$%X bytes loaded to $%X\r\n", nbytes, addr);
}

/*
 * Save disk files to tape. If flag == 1, only save non-backed up files
 */

int tsave(int argc,char *argv[],uint *argval,int flag)
{
#ifdef A1616
	register int ifd;
	register int cnt;
	register int arg;
	register int retval;
	register char *ifname;
	register char *buf;
	register struct dir_entry *dirent;
	int blksize;
	char dirb[DIRENTSIZE];

	dirent = (struct dir_entry *)dirb;
	buf = (char *)getmem0(MAXCASHUNK);
	if ((int)buf < 0)
		return BEC_NOBUF;

	for (arg = 1; arg < argc; arg++)
	{
		ifname = argv[arg];

		if ((retval = FILESTAT(ifname, dirent)) < 0)
		{
			ifd = retval;
			goto badfile;
		}

		if (flag && dirent->statbits & BACKEDUP)
			continue;	/* Don't need to write it */

		if ((ifd = OPEN(ifname, SSO_RDONLY)) < 0)
		{
badfile:		cantopen(ifname, ifd);
			continue;
		}

		if (verbose() && (argc > 2))
			PRINTF("%s\r\n", ifname);
		cnt = dirent->file_size;

/* Write out the main leader */
		CASWRAW(dirent, DIRENTSIZE, MAINLEADER);

/* Write the data */
		do
		{
			blksize = (cnt > MAXCASHUNK) ? MAXCASHUNK : cnt;
			if ((retval = READ(ifd, buf, blksize)) < 0)
			{
				cantread(ifname, ifd, retval);
				continue;	/* Next file */
			}
			CASWRAW(buf, blksize, SUBLEADER);
			cnt -= blksize;
		}
		while (cnt);

		doclose(ifname, ifd);
/* Now mark the backup bit */
		dirent->statbits |= BACKEDUP;
		if ((retval = PROCESSDIR(ifname, dirent, 3)) < 0)
			direrr(ifname, retval);
	}
	dofreemem(buf);
#endif /*A1616*/
return(retval);
}

/* Load a file from tape. If 'verflag' set, just verify */
int tload(int argc,char *argv[],int verflag)
{
#ifdef A1616
	register int cnt;
	register int retval;
	register int nread;		/* Number of bytes read */
	register int ofd;
	register char *ofname;
	register struct dir_entry *dirent, *ndirent;
	extern char *tempfname;
	char *buf;
	char dirb[DIRENTSIZE], ndirb[DIRENTSIZE];

	dirent = (struct dir_entry *)dirb;
	ndirent = (struct dir_entry *)ndirb;
	buf = (char *)dogetmem(MAXCASHUNK, 0, "tape read");
	if ((int)buf < 0)
		return BEC_NOBUF;

	if ((retval = CASRRAW(dirent, MAINLOCK, DIRENTSIZE)) < 0)
	{
		eprintf("Error reading header\r\n");
		goto err;
	}
	if (retval != DIRENTSIZE)
	{
		eprintf("Wrong header size: %d, not %d\r\n"); //, retval, DIRENTSIZE);
		retval = -1;
		goto err;
	}

	if (verbose() || verflag)
		PRINTF("File: %s  ", dirent->file_name);
	if (!verflag)
	{
		ofname = (argc == 2) ? argv[1] : dirent->file_name;
		if ((ofd = CREAT(tempfname, dirent->uid, dirent->load_addr)) < 0)
		{
			retval = cantcreat(tempfname, ofd);
			goto err;
		}
	}

	cnt = dirent->file_size;

/* Now pull in the file */
	do
	{
		if ((nread = CASRRAW(buf, SUBLOCK, MAXCASHUNK)) < 0)
		{
			if (!verflag)
			{
				WRITE(ofd, buf, MAXCASHUNK);
				CLOSE(ofd);
			}
			retval = eprintf("Read error\r\n");
			goto err;
		}
		if (!verflag)
		{
			if ((retval = WRITE(ofd, buf, nread)) < 0)
			{
				retval = cantwrite(tempfname, ofd, retval);
				goto err;
			}
		}
		if ((cnt -= nread) < 0)
		{
			if (!verflag)
				CLOSE(ofd);
			eprintf("Last block too long\r\n");
			retval = -1;
			goto err;
		}
	}
	while (cnt);
	retval = 0;

err:
	dofreemem(buf);
	if (retval < 0)
		return retval;

	if (!verflag)
	{
		if (doclose(tempfname, ofd))
			return -1;	/* Error */
/* Read in the new file's directory entry */
		if ((retval = PROCESSDIR(tempfname, ndirent, 2)) < 0)
			return cantread(tempfname, -1, retval);
/* Diddle backup flag & date */
		ndirent->statbits |= BACKEDUP;
		movmem(dirent->date, ndirent->date, sizeof(ndirent->date));
/* Now write the directory entry out */
		if ((retval = PROCESSDIR(tempfname, ndirent, 3)) < 0)
			return cantread(tempfname, -1, retval);
/* Rename it */
		UNLINK(ofname);
		RENAME(tempfname, ofname);
	}
	if (verbose())
		PRINTF("Done\r\n");
	return 0;
#endif /*A1616*/
}

int cantcreat(char *file,int ec)
{
	return cantdo(file, ec, "create");
}


int cantopen(char *file,int ec)
{
	return cantdo(file, ec, "open");
}

int cantunlink(char *file,int ec)
{
	return cantdo(file, ec, "unlink");
}

int cantrename(char *file,int ec)
{
	return cantdo(file, ec, "rename");
}

int cantwrite(char *file,int fd,int ec)
{
	CLOSE(fd);
	return cantdo(file, ec, "write");
}

int cantread(char *file,int fd,int ec)
{
	CLOSE(fd);
	return cantdo(file, ec, "read");
}

/* General function */
int cantdo(char *file,int ec,char *mes)
{
	int pid;

	if ((pid = GETPID()) >= 0)
	{
		struct proc *proctab;

		if ((int)(proctab = (struct proc *)GETPROCTAB(pid)) >= 0)
		{
			char *name;

			if ((name = proctab->name) != NULL)
				eprintf("%s: ");				//, name);
		}
	}

	eprintf("Cannot %s %s: ");	//, mes, file);
	return dobec(ec);
}

int direrr(char *file,int ec)
{
	eprintf("Directory error on %s: ");	//, file);
	return dobec(ec);
}

int doclose(char *file,int fd)
{
	int retval;

	if ((retval = CLOSE(fd)) == false)
	{
		return cantdo(file, retval, "close");
	}
	return 0;
}

int badladdr(char *file)
{
	return eprintf("%s: Bad load address\r\n"); //, file);
}

/*
 * Print out diagnostic based on block error code
 */

int dobec(int ec)
{
//	char buf[100];

	eprintf("%s\r\n");	//, INTERPBEC(ec, buf));
	return ec;
}

/*
 * MKDIR() monitor command: creates a directory
 */

int domkdir(int argc,char *argv[],uint argval)
{
	int arg, ret, retval;

	retval = 0;
	for (arg = 1; arg < argc; arg++)
	{
		if ((ret = MKDIR(argv[arg], 1)) < 0)
			retval = cantcreat(argv[arg], ret);
	}
	return retval;
}

/*
 * Change directory: free up current stuff and copy across new
 */

int dochdir(int argc,char *argv[])
{
	int retval;

	if (argc == 1)
	{
		PRINTF("%s\r\n", cur_path);
		return 0;
	}

	fsentry(0);
	if ((retval = CHDIR(argv[1])) < 0)
		cantdo(argv[1], retval, "cd to");
	else
		CWD(GETPPID(GETPID()), cur_path);
	fsexit(0);
	return retval;
}

/*
 * Move string to memory, adding null if flag == 0.  Return updated pointer
 */

char *
dowa(ptr, str, flag)
register char *ptr;
register char *str;
int flag;
{
	while (*str)
		*ptr++ = *str++;
	if (flag == 0)
		*ptr++ = '\0';
	return ptr;
}

/*
 * Put a value (byte, word or long) into memory. Return updated pointer.
 */

char *
putinmem(ptr, size, val)
register char *ptr;
register ushort size;
register int val;
{
#if 0
	register ushort i;
	register char *ptr2;

	ptr2 = ptr;
	ptr += size - 1;
	for (i = 0; i < size; i++)
	{
		*ptr-- = val;
		val >>= 8;
	}
	return ptr2 + size;
#endif
	register char *ret;

	ret = ptr + size;
	if (size == 1)
	{
		while (size--)
			*ptr++ = val;
	}
	else if (size == 2)
	{
		ushort *p = (ushort *)ptr;

		size /= 2;
		while (size--)
			*p++ = val;
	}
	else
	{
		ulong *p = (ulong *)ptr;

		size /= 4;
		while (size--)
			*p++ = val;
	}

	return ret;

}

/*
 * Output the address and contents of the memory at *ptr, with given size
 */

void printmem(char *ptr,uint size)
{
	register ushort i;

	PRINTF("%08X-", ptr);
	if ((size == 0) || (size == 5))
		PUTCHAR(prch(*ptr));
	else
	{
		for (i = 0; i < size; i++)
			PRINTF("%02X", (*ptr++) & 0xff);
	}
}

/*
 * Return '_' if not printable char
 */

int prch(char ch)
{
	return isalpha(ch) ? ch : '_';
}

/* Print out a byte (quicker than printf) */
void prbyte(uint ch)
{
	PUTCHAR(hextab[(ch >> 4) & 0xf]);
	PUTCHAR(hextab[ch & 0xf]);
}

/*
 * Read from input, write to output until EOF
 */

int pump(int ifd,int ofd,char *ifname,char *ofname)
{
	register int nread;
	register int retval;
	register char *xferbuf;
	int xbsize;

	xbsize = GETMEM(0, 2);	/* Find largest block avail. */
	if (xbsize < 20000)
	{
oom:		return nomemfor("copy");
	}
	xferbuf = (char *)getmem0(xbsize -= 10000);
	if (((int)xferbuf) < 0)
		goto oom;
	do
	{
		if ((nread = READ(ifd, xferbuf, xbsize)) < 0)
		{	/* Error reading from input */
			retval = cantread(ifname, ifd, nread);
			goto err;
		}
		else
		{
			if ((retval = WRITE(ofd, xferbuf, nread)) < 0)
			{	/* Output error: crash out */
				if (retval != BEC_PIPECLOSED)
					cantwrite(ofname, 0, retval);
				goto err;
			}
		}
	}
	while (nread);
	retval = 0;
err:	dofreemem(xferbuf);
	return retval;
}
