

#ifndef MONFNS_H
#define MONFNS_H


extern int cantdo(char *file,int ec,char *mes);
extern int cantread(char *file,int fd,int ec);
extern int cantopen(char *file,int ec);
extern int cantunlink(char *file,int ec);
extern int cat(int argc,char *argv[]);
extern int cantrename(char *file,int ec);
extern int cantcreat(char *file,int ec);

extern int mload(int argc,char *argv[],uint *argval,int flag,char *name);
extern int doclose(char *file,int fd);
extern int dir(int argc,char *argv[],int mode);
extern void dm(uchar *first, uchar *last,ushort size);
extern void printmem(char *ptr,uint size);
extern int wm(int argc,char *argv[],uint *argval,ushort size);
extern int expr(int argc,char *argv[],uint *argval);
extern int msave(char *argv[],uint *argval);
extern int tsave(int argc,char *argv[],uint *argval,int flag);
extern int tload(int argc,char *argv[],int verflag);
extern int dochdir(int argc,char *argv[]);
extern int domkdir(int argc,char *argv[],uint argval);
extern int pump(int ifd,int ofd,char *ifname,char *ofname);
extern int dobec(int ec);
extern int badladdr(char *file);
extern void prloaded(int nbytes,int addr);

#endif
