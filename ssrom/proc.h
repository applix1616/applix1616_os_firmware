
#ifndef PROC_H
#define PROC_H


extern void memfault(int ret);
extern int _proccntl(uint mode,uint arg1,uint arg2,uint arg3,uint arg4);
extern void docharsignals(void);
extern int getttydev(int mode);
extern int dops(void);
extern int pidlookup(char *arg);
extern int dokillwait(int argc,char *argv[],char *argtype,uint *argval,int iswait);

#endif
