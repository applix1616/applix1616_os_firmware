/* Move strings, terminating to correct length. Return pointer to new string */

#ifndef MOVSTR_H
#define MOVSTR_H

extern char *movstr(char *to, char *from, int len);

#endif
