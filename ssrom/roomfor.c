/*
 * Allocate room for a string
 */

//#include <syscalls.h>
#include "roomfor.h"

char *
roomfor(str, gmmode)
char *str;
int gmmode;
{
	char *retval;

	retval = (char *)GETMEM(strlen(str) + 1, gmmode);

	if ((int)retval >= 0)
		strcpy(retval, str);
	return retval;
}

char *room0for(char *str)
{
	return roomfor(str, 0);
}

char *room1for(char *str)
{
	return roomfor(str, 1);
}
