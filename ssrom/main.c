/* TABS4 NONDOC
 * C part of ROM code. Entered if bit 7 of the input port is set
 */
#include <string.h>
#include <types.h>

#include <syscalls.h>
#include <hwdefs.h>
#include <storedef.h>
#include <via.h>
#include <reloc.h>
#include <files.h>
#include <options.h>
#include <blkerrcodes.h>

//#include <chario.h>
#include <exception.h>
#include <spl.h>
#include <process.h>

#include "proc.h"
#include "kernel.h"
#include "vdriver.h"
#include "vsint.h"
#include "grdriver.h"
#include "cvidtest.h"
#include "circbuf.h"
#include "datetime.h"
#include "casio2.h"
#include "centron.h"
#include "serial.h"
#include "analog.h"
#include "blockio.h"
#include "inbuilt.h"
#include "newmem.h"
#include "prcrlf.h"
#include "rddriver.h"
#include "fcb.h"
#include "filefns.h"
#include "ssdddvr.h"
#include "loadreloc.h"
#include "leditor.h"
#include "debug.h"
#include "chario.h"
#include "psprintf.h"
#include "movstr.h"
#include "spspace.h"
#include "systrap.h"
#include "crtcint.h"
#include "scc.h"
#include "monfns.h"


#define ROMVERSION	0x47				/* Version 4.7a ROM */
#define SLOTROM_ID	0x62b596a7
#define JTABSIZE	150					/* Room for system trap table */
#define POWERUPVAL	((uint)0xc1a9a6f2)	/* Special pattern */
#define SYSTEM_TRAP	0x9c				/* The system entry trap vector */

/*
 * Storage common with assembler for exception handling
 */
extern int _initsp();		/* For doing checksum */
extern void _delay(int t);
extern void reset();
extern void movmem();
extern void clearmem();
extern void setsp();		/* setsp.s */
extern int strucmp(char *s1,char *s2);
//extern int entry(struct rel_header *, int, ... );
extern int berrtest(int loc);

extern void _asbootup(void);

int regsave[16];
int accaddr;
ushort fcsave;
ushort irsave;
ushort srsave;
ushort wastype0;
int pcsave;
char *excepmes;				/* Pointer to exception message */
char is68010;				/* 0: 68000 1: 68010 */
ushort lastsyscall, excepsyscall;

/* RAM copy of trap table */
int rjtab[JTABSIZE];

char *rdstart;				/* Start of RAM disk */
char *mrdstart;				/* Start of memory resident drivers */

/* Start & size of on-board RAM */
int obramstart;
int obramsize;

/* Pointer used by monitor */
char *dumpaddr;

/* If non-zero, clear screen in exception */
int excepcls;

/* If non-zero, dump registers on trace exception */
int traceregs;

/* Start of day power up pattern */
int sodmagic, sodflag;

/* Longs used to tell if we have just been turned on */
int pwruplongs[4];
int pwruptab[] =
{
	POWERUPVAL,
	~POWERUPVAL,
	POWERUPVAL ^ 0xa5a5a5a5,
	POWERUPVAL ^ 0x5a5a5a5a
};

ushort romver;

/* The main memory resident driver */
struct mrdriver mrdriver;
ushort nobeep;			/* Suppress beeps */

/*
 * Exception error messages
 */

char berrmes[] = "Bus error";
char addresserr[] = "Address error";
char illegalmes[] = "Illegal op";
char zdividemes[] = "Zero divide";
char chkmes[] = "CHK";
char trapvmes[] = "TRAPV";
char privmes[] = "Privilege";
#ifdef notdef
char l1010mes[] = "Line 1010";
#endif
char l1111mes[] = "Line 1111";
char unintmes[] = "Uninitialised interrupt";
char spurmes[] = "Spurious interrupt";
char trapmes[] = "";

char irq1mes[] = "IRQ 1";
char irq2mes[] = "IRQ 2";
char irq3mes[] = "IRQ 3";
char irq4mes[] = "IRQ 4";
char irq5mes[] = "IRQ 5";
char irq6mes[] = "IRQ 6";
char irq7mes[] = "IRQ 7";
char badsyscall[] = "Bad syscall";

char tempfname[] = "...TEMP...";

char *vramstart;
char *bitmaps;
int beepsave;				/* Save for enerrbeep during initialisation */
int _speed;					/* 1 for 15 MHz, 0 for 7.5 MHz */
int excepflag;				/* If == POWERUPVAL, don't clear screen */
int exceprestart;			/* If non-zero, restart on exceptions */
int _gooddevs, _bootunit;
int reslevel;
int nmrdrivers;

extern uint maxfiles;		/* Number of file control blocks */
extern int nlastlines;
extern uint vsi_count;
extern ushort abort_flag;
extern int ndcentries;		/* Number of directory cache entries */

extern int syssafe;			/* If non-zero, no switches can occur */
extern int switchpending;
extern char *rdstart;
extern struct blkdriver bdrivers[];
extern int ssddvers;		/* 1616 disk drive controller firmware version */
extern int enerrbeep;		/* If true, enable beep in eprintf() */
extern int fsbusy, fsbpid;	/* In filefns.c */
extern int curpid;
extern int stdout;
extern uint conout;			/* Console: either con: or sa: */
extern int mtnotup;			/* Flag saying multi-tasking has not started */
extern int mmnotup;			/* Flag saying memory manager has not started */
extern int volume;
extern struct chardriver *chardrivers;
extern char *lastexec;


void cprintf(char *str, void *, void *, void * );
void dumpregs(int type0, int wantregs);

#ifdef MDEBUG

extern long *Tloc;

#define D(n) ZZZ(n)

ZZZ(n)
{
	*Tloc++ = (long)n;
}

#else
#define D(n)
#endif

/*
 * main() is called both in test 7 mode and in useful mode.
 */

int main(istest7)
{
	register uint i, pass;
	register ushort *codeptr;
	int _volume;
	char tdbuf[25];

	extern unsigned short _via_aisr(), scc_aisr();	/* The assembler code */

D(1);
	_splx(0x0700);
	_delay(2000);			/* .2 of a second */
	reset();				/* Reset the disk controller properly */
	mtnotup = -1;
	mmnotup = -1;
	romver = ROMVERSION;
	syssafe = 1;
	switchpending = 0;
	fsbusy = 0;
	fsbpid = 0;
	curpid = 0;
	nobeep = 0;
	abort_flag = 0;
D(2);
	nmrdrivers = 0;
D(102);
#ifdef A1616
	*V_DDRA |= CLRCASIRQ|ECASRIRQ|ECASWIRQ;
	*V_AREG &= ~CLRCASIRQ;
	*V_AREG |= ECASRIRQ|ECASWIRQ;
#endif
	excepflag = (excepflag == POWERUPVAL) ? 1 : 0;
D(103);
/* Set up system call entry point */
	*((int *) SYSTEM_TRAP) = (int) _sys_trap;
/* Set up RAM vector table */
	movmem(_jtab, rjtab, JTABSIZE * 4);
D(104);
/* Work out reset level */
	if (!istest7)
	{
		for (i = 0; i < (sizeof(pwruptab)/sizeof(*pwruptab)); i++)
			if (pwruptab[i] != pwruplongs[i])
				goto level0;
/* See if correct value is in CRTC cursor register (yet another power-on word!) */
		*CRTC_ADDR = 14;		/* Cursor high */
		i = *CRTC_DATA << 8;
		*CRTC_ADDR = 15;		/* Cursor low */
		i |= (*CRTC_DATA & 0xff);
D(105);
		if ((i & 0x3fff) != CRTC_SAVE)
		{
level0:
			/* Work out if it is start of day */
			sodflag = (sodmagic != POWERUPVAL);
			sodmagic = POWERUPVAL;
			reslevel = 0;
			traceregs = 1;
			usercrtcset = 0;
			*CRTC_ADDR = 15;
			*CRTC_DATA = CRTC_SAVE & 0xff;
			*CRTC_ADDR = 14;
			*CRTC_DATA = CRTC_SAVE >> 8;
D(106);
			movmem(pwruptab, pwruplongs, sizeof(pwruptab));
		}
		else
		{
			if (vsi_count < (5 * 50))
				reslevel = 1;
			else
				reslevel = 2;
		}
	}

	if (!reslevel)
	{	/* Default RAM disk sizes based on DIL sw */
		static int defrds[]= {304, 200, 104, 24};
		int defrdsize;
D(107);
		excepcls = 1;		/* Clear screen on exception during boot */
		excepflag = 0;
		beepsave = 1;
		exceprestart = 1;	/* Restart on exceptions */
		maxfiles = DEF_MAXFILES;
		ndcentries = DEF_NDCENTRIES;
		nlastlines = 10;/* Default */

	/* Set up the high memory map, assuming no MRDRIVERS */
		defrdsize = defrds[(*IPORT & 0x30) >> 4];
		vramstart = (char *)obramstart + obramsize - VRAMSPACE;
		chardrivers = (struct chardriver *)(vramstart -	NIODRIVERS * sizeof(*chardrivers));
		bitmaps = (char *)chardrivers - BITMAPROOM;
D(108);
		rdstart = bitmaps - defrdsize*BLOCKSIZE;/* Default */
		mrdstart = rdstart;			/* Default */
		clearmem(&mrdriver, sizeof(mrdriver));	/* No MRdrivers */
		mrdriver.rdsize = defrdsize;			/* Default */
		mrdriver.stackspace = STACKSPACE;		/* Default */
		mrdriver.vramspace = VRAMSPACE;			/* Default */
		mrdriver.maxfiles = DEF_MAXFILES;
		mrdriver.ndcentries = DEF_NDCENTRIES;
#ifdef IOCACHE
		mrdriver.ncacheblocks = 0;				/* Default */
#endif
	}
	else
		beepsave = enerrbeep;

	enerrbeep = 0;				/* No beeps until all I/O initialised */
	berrtest(0xffff01);			/* Phillip's scsi thing */
/* Set up vectors to interrupt dispatchers */
	*((int *) S_INTVEC) = (int) _scc_aisr;
	*((int *) V_INTVEC) = (int) _via_aisr;
	chio_init();				/* Character I/O 		*/
D(109);
	if (!reslevel)
		mrdriver.colours = 0x0000fa50;
	set_colours(mrdriver.colours);	/* Default */
	vsint_init();				/* Vertical sync interrupts	*/
D(110);
	VIDEO_INIT(excepflag ? 3 : reslevel);	/* Video		*/
D(160);
	gr_init();					/* Graphics			*/
D(111);
/* Enough initialisation for test7 code */
	if (istest7)
		cvidtest();
	cbuf_init();				/* Cirular buffers		*/
D(112);
	dt_init();					/* Date / time			*/
	cas_init();					/* Cassette I/O			*/
D(113);
	cent_init();				/* Centronics			*/
D(114);
	comms_init();				/* SCC communications		*/
D(115);
	anio_init();				/* Analog I/O init		*/
D(116);
	blkio_init();				/* Block I/O init		*/
D(117);
	mon_init();					/* The command interpreter	*/
D(118);
	mem_init(0x2000);			/* Memory manager		*/
#ifdef IOCACHE
	iocache_init(0);			/* Block device cache		*/
#endif
D(119);
	KB_INIT(reslevel);			/* Keyboard			*/
	_newpid();					/* Set up a table for mode 0 memory */
D(120);
	strcpy(tdbuf, "  ");
	GETTDSTR(tdbuf+2);
	if (!reslevel)
		tdbuf[0] = '\0';		/* Meaningless date */
D(121);
	if (!excepflag)
	{
		PUTCHAR(26);
		cprintf("Applix 1616/OS V4.7a  Level %d reset%s\n",
				&reslevel, tdbuf, NULL);
	}
	else
		prcrlf();
D(0);
	if (reslevel == 0)
	{

		cprintf("Copyright (c) 1987-1993 Applix pty limited",NULL,NULL,NULL);
		cprintf("Andrew Morton",NULL,NULL,NULL);
		cprintf("Compiled: 14 August 1993",NULL,NULL,NULL);

		codeptr = (ushort *)_initsp;
		i = 0;
		do
			i += *codeptr++;
		while (codeptr < (ushort *)(((int)_initsp) + 0x20000));
		cprintf("ROM checksum: $%08x", (void *)i, NULL,NULL);

		/* Do the following so that the RAM disk is block
		   driver 0, even though it is not the first
		   installed */
		rdsk_init(-1);
	}
D(1);
	file_init();			/* Initialise file I/O		*/
D(2);
	ssdd_init();			/* Find the disk controller	*/
	{
		extern int (*user_patch)();
		if (user_patch)
			(*user_patch)(reslevel, 0);
	}

D(3);
	_gooddevs = -1;				/* Assume all devices OK */
	_bootunit = -1;

	if (!reslevel)
	{
		int ifd, retval;
		char ifname[FNAMESIZE + 20];
		int lunit, unit, nretry;
		int mask;

		if (ssddvers)
			cprintf("V%d.%d disk controller", (void *) (ssddvers >> 4), (void *) (ssddvers & 15),NULL);

		OPTION(OPT_DISKVERBOSE, 0);	/* Prevent messages */
		for (lunit = ((*IPORT & 0x40) ? 0 : 1); lunit < NBLKDRIVERS; lunit++)
		{	/* Search the drivers */
			if (lunit == 0)
				unit = 3;	/* Search /H0 first! */
			else
				unit = lunit;
			mask = ~(1 << unit);
			_gooddevs |= ~mask;
			if (!bdrivers[unit].bread)
			{
				_gooddevs &= mask;
				continue;	/* No driver */
			}
			SPRINTF(ifname, "%s/mrdrivers", bdrivers[unit].devname);
			nretry = 10;
retry:
			ifd = OPEN(ifname, SSO_RDONLY);
			if ((ifd == BEC_IOERROR) && (unit == 3) && nretry--)
			{
				delay(10000);	/* 1 second */
				goto retry;	/* Another go for /H0 */
			}
			if (ifd < 0)
			{
				if (ifd != BEC_NOFILE)
					_gooddevs &= mask;
				continue;
			}
			else
			{
				char *loadp;
				struct mrdriver tempmrd;

				cprintf("Loading %s", (void *)ifname, NULL, NULL);
				if ((retval = READ(ifd, &tempmrd, sizeof(tempmrd)))
					!= sizeof(tempmrd))
				{
					goto badread;
				}
				if ((tempmrd.magic1 != RELMAG1) || (tempmrd.magic2
					!= (uint)RELMAG2))
					goto badread;
				if (tempmrd.vers > RELVERS)
				{
					PRINTF("MRDRIVERS version unsupported\r\n");
					goto badread;
				}
				/* It seems OK */
				movmem(&tempmrd, &mrdriver, sizeof(mrdriver));

				vramstart = (char *)obramstart + obramsize -
						mrdriver.vramspace;

				chardrivers = (struct chardriver *)(vramstart -
					NIODRIVERS * sizeof(*chardrivers));
				chio_init();		/* Redo the structs */
				comms_init();		/* Uses chardrivers */
				bitmaps = (char *)chardrivers - BITMAPROOM;
				rdstart = bitmaps - mrdriver.rdsize*BLOCKSIZE;
				mrdstart = rdstart - mrdriver.memusage;
				if (mrdriver.nlastlines)
					nlastlines = mrdriver.nlastlines;
				if (mrdriver.maxfiles >= 10 &&
					mrdriver.maxfiles <= MAXFILES)
					maxfiles = mrdriver.maxfiles;
				else
					mrdriver.maxfiles = maxfiles;

				if (mrdriver.ndcentries >= 5 &&
					mrdriver.ndcentries < 512)
					ndcentries = mrdriver.ndcentries;
				else
					mrdriver.ndcentries = ndcentries;

				/* Read in the drivers */
				loadp = mrdstart;
				for (i = 0; i < mrdriver.ndrivers; i++)
				{
					loadp = (char *)loadreloc(ifd,(int) loadp, ifname,1);
					if ((int)loadp < 0)
						goto badread;
				}
				CLOSE(ifd);
				_bootunit = unit;
				goto ok;
badread:			PRINTF("Error reading %s\r\n", ifname);
				CLOSE(ifd);
			}	/* if read ok */
		}	/* for unit */
ok:		;
	}
D(4);
	nmrdrivers = mrdriver.ndrivers;
	mrdriver.obramstart = obramstart;
	mrdriver.mrdstart = (int)mrdstart;
	mrdriver.rdstart = (int)rdstart;
	mrdriver.vramstart = (int)vramstart;
	mrdriver.bitmaps = (int)bitmaps;
	mrdriver.chardrivers = (long)chardrivers;
	if (mrdriver.colours == 0)
		mrdriver.colours = 0x0000fa50;
	set_colours(mrdriver.colours);
D(5);
	setsp(mrdstart - 4);		/* The new stack pointer */
	/* Reinstall Memory manager with new SP */
	mem_init(0x1000);
D(6);
	/*
	 * From now to schedstart mode 0 memory cannot be allocated.
	 * This is because no exectables have been allocated.
	 * GETMEM() will complain if it is.
	 */
#ifdef IOCACHE
	iocache_init(mrdriver.ncacheblocks); /* Block device cache */
#endif
	file_init();				/* Re-initialise file I/O	*/
D(7);
	ledit_init();				/* Initialise line editor things */
D(8);
	rdsk_init(reslevel);		/* Initialise/install RAM disk	*/
D(9);
	ssdd_init();				/* Reinstall Magnetic disk drivers */
D(10);
	_splx(0);
D(11);
	_volume = volume;
	volume = 10;
/* Ascertain CPU speed */
	_speed = 0;					/* Do beep assuming 7.5 MHz */
	i = GET_TICKS();
	lbeep(2000, 0);				/* Uses timer 1 */
	i = GET_TICKS() - i;
	_speed = (i < 10) ? 8 : 0;
	volume = _volume;
D(12);
	if (!reslevel)
	{
		cprintf("%dK RAM disk,  %dK video memory",
		 	(void *)rdstart,(void *) (mrdriver.vramspace / 1024),NULL);
		cprintf("%d drivers occupying $%X bytes at $%X",
			(void *)mrdriver.ndrivers, (void *)mrdriver.memusage, (void *) mrdstart);
		cprintf("System processor: %s MHz %d", (void *) (_speed ? "15" : "7.5"),
				(void *)((is68010) ? 68010 : 68000),NULL);
	}
D(13);
/* Search for ROMs */
	for (pass = 0; pass < 2; pass++)
	{
		for (i = 0x00800000; i < 0x01000000; i += 0x00100000)
		{
			if (!(berrtest(i) || berrtest(i+1)))
			{
				if (*((int *)i) == SLOTROM_ID)
				{
					cprintf("Calling ROM code at $%x", (void *)(i + 4),NULL,NULL);
					(*(int(*)())(i + 4))(pass, reslevel);
				}
			}
		}
	}
	enerrbeep = beepsave;		/* Restore beep status */
D(14);
	/* Do this again because it seems to help */
	KB_INIT(3);
D(15);
	if (!ssddvers)			/* No disk device */
		set_io(1);
D(16);
	/* We probably won't get repeating exceptions by now */
	excepflag = 0;
	if (!reslevel)
		excepcls = 0;
D(17);
	startsched();
	return(0);
}

/*
 * System call 89: call a memory resident driver. If mrdno == -1,
 * return pointer to internal memory resident driver header
 *
 * If mrdno > 0x1000, assume it points to a string and we are calling by name
 */

int _callmrd(uint mrdno, int cmd,int  arg)
{
	register uint i, retval;
	register struct rel_header *rhptr;

	if (mrdno == -1)
		return (int)&mrdriver;

	if (mrdno >= mrdriver.ndrivers)
	{
		if (mrdno >= 0x1000)
		{	/* Search for the mrd with the name at *mrdno */
			for (i = 0; i < mrdriver.ndrivers; i++)
			{
				if (!(retval = strucmp((char *)mrdno,
						(char *)CALLMRD(i, MRD_NAME, 0))))
					return i;
			}
		}
		return -1;	/* Bad driver number or not there */
	}

/* Scan through the MRdrivers */
	rhptr = (struct rel_header *)mrdstart;
	while (mrdno--)
	{
		if (rhptr->magic1 != RELMAG1)
		{
			eprintf("\007Corrupted MRdrivers");
			pwruplongs[0] = 0;	/* Force level 0 reset */
			intfail(500,(int *) &mrdno,0,0);
		}
		i = rhptr->textlen + rhptr->datalen + rhptr->bsslen;
		rhptr++;	/* Points to driver code after header */
		rhptr = (struct rel_header *)((int)rhptr + i);
	}
	rhptr++;		/* Point to code after header */
	/* entry() saves all registers, supports EXIT() */
	return entry(rhptr, cmd, arg,0,0,0,0);
}

/*
 * Handle hardware exceptions. Assembler code comes here after
 * saving registers, etc
 * If pc != 0, we are called from scheduler with a serious stack overrun.
 */

void cexception(int pc)
{
	char *exmes;
	char buf[20];
	int goon;

D(100);
	switchpending = 0;
	syssafe = 1;
	goon = ((excepcls & 2) && !mtnotup);
	if (pc)
		EXCEPTION_DATA->pc = pc;
#if 0
	if ((!goon && !mtnotup) || pc)
#endif
	{	/* Build the exception message */
D(189);
		EXCEPTION_DATA->magic = RESET_MAGIC;
		movmem(regsave, EXCEPTION_DATA->dregs, sizeof(regsave));
		EXCEPTION_DATA->accaddr = (long)accaddr;
		EXCEPTION_DATA->fc = (long)fcsave;
		EXCEPTION_DATA->ir = (long)irsave;
		EXCEPTION_DATA->sr = (long)srsave;
		EXCEPTION_DATA->pc = (long)pcsave;
		EXCEPTION_DATA->message = excepmes;
		EXCEPTION_DATA->lastsyscall = excepsyscall;
		EXCEPTION_DATA->lastload = curpidname(1);
		movstr(EXCEPTION_DATA->processname, (char *)curpidname(0),
				sizeof(EXCEPTION_DATA->processname));
		movmem(regsave[15], EXCEPTION_DATA->stacktrace,
				sizeof(EXCEPTION_DATA->stacktrace));
		movstr(EXCEPTION_DATA->lastexec, lastexec,
				sizeof(EXCEPTION_DATA->lastexec));
	}
D(101);
movmem(EXCEPTION_DATA, 0x100000, sizeof(*EXCEPTION_DATA));
	if (pc)
		WARMBOOT(BR_STACKOVERRUN);
D(200);
	if (excepcls & 4)
	{
		if (excepcls & 8)
			pwruplongs[0] = 0;
		_spl7();
		for ( ; ; )
			;
	}
D(102);
	if ((srsave & 0x0700) || (fsbusy && (fsbpid == curpid)))
		goon = 0;

	/* Let error output go to con: or sa: only */
	if (stdout > 1)
		SET_SOP(conout);
D(103);
	if (!goon)
	{
		_spl7();
		reset();
		if (excepcls & 1)
		{
			chio_init();
			VIDEO_INIT(2);
			PUTCHAR(12);
		}
		else
			prcrlf();
	}
	else
		prcrlf();
D(104);
	if (excepmes == trapmes)
	{
		SPRINTF(buf, "Trap #%d", *((short *)(pcsave - 2)) & 15);
		exmes = buf;
	}
	else
		exmes = excepmes;
D(105);
	PRINTF("Exception: %s, process '%s', load = $%x, last syscall = %d\r\n",
			exmes, curpidname(0), curpidname(1), excepsyscall);
	dumpregs(wastype0, 1);
	if (excepcls & 1)
	{
		PRINTF("\r\n\nStack trace:\r\n");
		/* Memory above stack */
		dm((uchar *)regsave[15],(uchar *) regsave[15] + 191, 2);	/* Memory dump */
	}
D(106);
	if (exceprestart && excepflag == 0)
	{	/* We are allowed to restart and probably not recursive errors */
		if (goon)
		{
			_splx(0);
			syssafe = 0;
			PRINTF("\r\n\007");
			EXIT(-1);			/* ! */
		}
		else
		{
			excepflag = POWERUPVAL;
			WARMBOOT(BR_EXCEPTION);
		}
	}
	pwruplongs[0] = 0;	/* Force level 0 crash when reset is hit */
	for ( ; ; )
		;
}

void dumpregs(int type0, int wantregs)
{
	register ushort i;
	register int *regptr;
	static char Lsrchars[] = "cvznx--------s-t";
	static char Hsrchars[] = "CVZNX---124--S-T";

	if (type0)
		PRINTF("FC:%04x Acc addr:%08x IR:%04x",
			fcsave, accaddr, irsave);
	PRINTF(" Next PC:%08x SR:%04x: ", pcsave, srsave);

	i = 15;
	do
		PUTCHAR((srsave & (1 << i)) ? Hsrchars[i] : Lsrchars[i]);
	while (i--);

	if (wantregs)
	{
		PRINTF("\r\nD0-D7:");
		for (i = 0, regptr = regsave; i < 16; i++)
		{
			if (i == 8)
				PRINTF("\r\nA0-A7:");
			PRINTF(" %08x", *regptr++);
		}
	}
}

void _warmboot(short reason)
{
	_splx(0x0700);
	EXCEPTION_DATA->boot_reason = reason;
	_asbootup();
}

/*
 * Handle trace mode exceptions
 */

void trace(void)
{
	PRINTF("Trace trap:");
	dumpregs(0, traceregs);
	PRINTF("\r\n");
}

/*
 * System call 80: Set new system trap vector: return old vector
 * If `whereto' == 0, set up default
 */

int _setstvec(register int vecnum,register int whereto)
{
	register int vecsave = -1;

	if (vecnum < 160L)
	{
		vecsave = rjtab[vecnum];
		if (whereto >= 0)
			rjtab[vecnum] = whereto ? whereto : _jtab[vecnum];
	}
	return vecsave;
}

/*
 * System call 126: Return ROM version number
 */

int _getromver()
{
	return ROMVERSION;
}

/*
 * System call 19: return system processor type
 * 0 = 68000
 * 1 = 68010
 */

int _get_cpu()
{
	return is68010;
}

/*
 * System call 143: set the CPU type (!)
 *	0 = 68000
 *	1 =	68010
 *	2 = 68020
 *	3 = 68030
 *	4 = 68040
 *	5 = unused
 *	6 = 68060
 *	7 = taken by Philips
 *	8-F = future.
 */

void _set_cpu(cputype)
{
	is68010 = cputype;
}

/*
 * System call 94: return CPU speed. 0 = 7.5 MHz, 1 = 15 MHz.
 */

int _cpuspeed()
{
	return _speed;
}

/*
 * Print a string centred on the screen
 */
void cprintf(char *str, void *p1, void *p2, void *p3 )
{
	char buf[100];
	int len;

	SPRINTF(buf, str, p1, p2, p3);
	len = 80 - strlen(buf);
	while (len > 0)
	{
		PUTCHAR(' ');
		len -= 2;
	}
	PRINTF("%s\r\n", buf);
}
