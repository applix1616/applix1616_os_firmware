/*
 * Read an environment bit
 */

#include <syscalls.h>

envbits(mask)
{
	return GETENVBITS(GETPID()) & mask;
}
