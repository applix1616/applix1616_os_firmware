#ifndef SETMEM_H
#define SETMEM_H

extern void _lsetmem(void *address,long val,long count);
extern void clearmem(void *addr, long nbytes);
extern void _bsetmem(char *address,long val,long count);
extern void _wsetmem(short *address,long val,long count);

#endif
