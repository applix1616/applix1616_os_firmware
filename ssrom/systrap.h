#ifndef SYSTRAP_H
#define SYSTRAP_H

extern int _jtab[];

extern int entry(void *addr,int nargs,int argstr,int argtype,int argval,int p1,int p2);
extern int _sys_trap(void); /* systrap.s */
extern int enterproc(void *ptr);


#endif
