/*
 * Centronics printer driver code
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
#include <via.h>
#include <storedef.h>

#include "circbuf.h"
#include "centron.h"

struct circ_buf cent_ocbuf;
int centack_isr();

void cent_init(void)
{
	*V_DDRA &= ~CENTBUSY;			/* Set up busy bit input */
	*V_DDRA |= CENTSTB;			/* Set up strobe output */
	NEW_CBUF(4, 0, 0);			/* Set up circular buffer */
	*((int *)VIV_CA1) = (int)centack_isr;	/* Set up interrupt vector */
	SET_VSVEC(centack_isr, 1, 0);		/* Set up poll vector */

/* Program the VIA */
	*V_PCR |= 1;				/* CA1 positive edge */
	*V_IER = 0x82;				/* Enable the interrupt */
	*V_AREG |= CENTSTB;			/* Put strobe high */
}

cent_ostat()
{
	return cb_bfree(&cent_ocbuf);
}

cent_putc(ch)
int ch;
{
	wroom_cbuf(&cent_ocbuf);
	put_cbuf(&cent_ocbuf, ch, -1);
	return 0;
}

/*
 * Write a char out to the printer at interrupt time
 */

centack_isr()
{
	/* The read from V_AREG clears the interrupt source */
	if ( (!(*V_AREG & CENTBUSY)) && cb_bnum(&cent_ocbuf))
	{	/* Not busy, char ready */
		*CENTLATCH = clval = get_cbuf(&cent_ocbuf);
		centdelay();
		*V_AREG &= (char) ~CENTSTB;		/* Drop STROBE */
		centdelay();
		*V_AREG |= CENTSTB;
	}
}

/*
 * Delay about 5 usecs (60 cycles) for signal settling. JSR=36 cycles
 */

asm("	_centdelay:		");
asm("		jsr cd2		");
asm("	cd2:			");
asm("		rts			");
