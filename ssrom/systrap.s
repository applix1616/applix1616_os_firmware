/*; TABS8*/

#include "asm68k.h"

		.text
/*;
; Field trap #SYSTEM calls by saving registers and vectoring to code.
; The code is entered with the following parameters: code(d1, d2, a0, a1, a2)
;
*/
		.global	_sys_trap		/*; , _trappcsave*/
		.global	_syssafe,_switchpending,_syssp
		.global	__empty

_sys_trap:	movem.l	d1/d2/d3/a0/a1/a2/a3,-(sp)
/*;		move.l	30(sp),_trappcsave		; Call PC*/
		cmp.w	#149,d0			/*; d0 - size*/
		bcs.b	goodcall		/*; Branch if <*/
		jmp	__empty			/*; Bad system call*/
goodcall:
		movem.l	d1/d2/a0/a1/a2,-(sp)	/*; Stack passed parameters*/
		move.w	d0,lastsyscall
		asl.l	#2,d0
		move.l	#rjtab,a0
		move.l	0(a0,d0.w),a0
		jsr	(a0)			/*; Go to code*/
		add.l	#20,sp			/*; Adjust the stack*/
		movem.l	(sp)+,d1/d2/d3/a0/a1/a2/a3

/*; Is a process switch pending?*/
scexit:
		move.w	d0,-(sp)
		move.w	sr,d0			/*; Check we are at level 0*/
		and.w	#0x0700,d0
		beq.b	canswtch
		move.w	(sp)+,d0
		bra.b	noswtch			/*; No switch if not at level 0*/
canswtch:
		move.w	(sp)+,d0
		or.w	#0x0300,sr		/*; Stop interrupts*/
		tst.l	switchpending
		beq.b	noswtch
		tst.l	syssafe
		bne.b	noswtch			/*; Can't interrupt system*/
/*; OK, save registers and return to scheduler
;		clr.l	_switchpending*/
		/*; Save user regs*/
		movem.l	d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6,-(sp)
		move.l	sp,d0			/*; User SP*/
		move.l	syssp,sp
		/*; Restore scheduler's regs*/
		movem.l	(sp)+,d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6
noswtch:
		rte				/*; Restore interrupt level*/

/*;
; The Line A trap entry point
;*/
		.global	__linea,_is68010

__linea:
		move.l	2(sp),a0		/*; The Call PC*/
		/*;Unusual: the line A trap appears to push the address of the
		;offending opcode, not the address + 2!*/
		move.w	(a0),d0			/*; The Line A opcode*/
		and.w	#0x0fff,d0		/*; The system call number*/
		cmp.w	#149,d0
		bcs.b	goodlinea
/*;		move.l	a0,_trappcsave		; Call PC*/
		jmp	__empty			/*; Bad system call*/
goodlinea:
		move.w	d0,lastsyscall
		asl.l	#2,d0			/*; Long index*/
		move.l	#rjtab,a0		/*; RAM jump table*/
		move.l	0(a0,d0.w),a0
		addq.l	#2,2(sp)		/*; Return address points to next opcode*/
		lea	28(sp),a1		/*; Pointer to last arg for 68010*/
		tst.b	is68010
		bne.b	linea010
		subq.l	#2,a1			/*; 2 less bytes for 68000*/

linea010:
		movem.l	d2/d3/a2/a3,-(sp)	/*; Save local registers from Hitech C*/
		move.l	-(a1),-(sp)		/*; Restack the arguments to the*/
		move.l	-(a1),-(sp)		/*; system call*/
		move.l	-(a1),-(sp)
		move.l	-(a1),-(sp)
		move.l	-(a1),-(sp)
		jsr	(a0)			/*; Do the call*/
		add.w	#20,sp			/*; Adjust for stacked args*/
		movem.l	(sp)+,d2/d3/a2/a3
		bra	scexit			/*; Go test for context switch*/

/*;
; Reschedule user code
;
*/
		.global	_enterproc, enterproc
enterproc:
_enterproc:
		move.w	sr,-(sp)		/*; Stack SR for RTE exit*/
		move.l	6(sp),d0		/*; User SP*/
		movem.l	d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6,-(sp)	/*; Scheduler regs*/
		move.l	sp,syssp
		move.l	d0,sp
		movem.l	(sp)+,d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6 /*; User regs*/
		rte

/*; Return point for programs which return from their signal handlers*/
		.global	_sigreturn, sigreturn
sigreturn:
_sigreturn:
		addq.l	#6,sp			/*; Adjust for pushed args*/
		addq.l	#6,sp			/*; Adjust for pushed args*/
		movem.l	(sp)+,#0x7fff		/*; Restore normal user code context*/
		rte				/*; Return to user code*/

/*; Come here if user program ends via RTS*/
		.global	_procreturn, procreturn
procreturn:
_procreturn:
		move.l	d0,d2			/*; Exit code*/
		bra	doex
/*;
; Entry point for the exit() system call 13
;*/
		.global	_exit,__exit
__exit:
_exit:		move.l	4(sp),d2
doex:		move.l	#129,d0			/*; proccntl(2, code)*/
		moveq.l	#2,d1			/*; exit*/
		trap	#7

		trap	#1

/*;
; Enter user transient program here
;
; Usage: entry(addr, nargs, argstr, argtype, argval, p1, p2)
;               8      12     16      20       24    28  32
;*/
		.global	_entry
_entry:		link	a6,#0
		movem.l	d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5,-(sp)
		move.l	32(a6),-(sp)
		move.l	28(a6),-(sp)
		move.l	24(a6),-(sp)	/*; Start pushing args to user program*/
		move.l	20(a6),-(sp)
		move.l	16(a6),-(sp)
		move.l	12(a6),-(sp)	/*; All pushed*/
		move.l	8(a6),a0	/*; Pointer to code*/
		jsr	(a0)		/*; Ta Ta*/
/*;Return point*/
doexit:		add.l	#24,sp
		movem.l	(sp)+,d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5
		unlk	a6
		rts

		.global	__warmboot,__exit,__sine,__rset_pel,__set_pel
		.global	__rline,__line,__rread_pel,__read_pel
		.global	__sgbgcol,__sgtexture,__empty,__coldboot
		.global	_jtab
_jtab:
	.int	__warmboot	/*; 0: Warm boot the system*/
	.int	__warmboot	/*; 1: Warm boot the system*/
	.int	_getchar	/*; 2: Standard I/P*/
	.int	_sgetchar	/*; 3: Standard I/P status*/
	.int	_putchar	/*; 4: Standard O/P*/
	.int	_sputchar	/*; 5: Standard O/P status*/
	.int	_getc		/*; 6: Call an I/P driver*/
	.int	_sgetc		/*; 7: Get I/P driver status*/
	.int	_putc		/*; 8: Call an O/P driver*/
	.int	_sputc		/*; 9: Get O/P driver status*/
	.int	_add_ipdvr	/*; 10: Install an I/P driver*/
	.int	_loadrel	/*; 11: Load relocatable code*/
	.int	_add_opdvr	/*; 12: Install an O/P driver*/
	.int	__exit		/*; 13: User abort code*/
	.int	_set_sip	/*; 14: Assign standard I/P*/
	.int	_set_sop	/*; 15: Assign standard O/P*/
	.int	_set_vsvec	/*; 16: Install a VS vector*/
	.int	_clr_vsvec	/*; 17: Delete a VS vector*/
	.int	_get_ticks	/*; 18: Return VS count*/
	.int	_get_cpu	/*; 19: Return CPU type*/
	.int	_set_ser	/*; 20: Set standard error*/
	.int	_caswraw	/*; 21: Raw cassette O/P*/
	.int	_casrraw	/*; 22: Raw cassette I/P*/
	.int	_getdate	/*; 23: Get the date and time*/
	.int	_setdate	/*; 24: Set the date and time*/
	.int	_abortstat	/*; 25: Read abort status*/
	.int	_ent1ints	/*; 26: Enable timer1 output interrupts*/
	.int	_dist1ints	/*; 27: Disable timer1 output interrupts*/
	.int	__sine		/*; 28: Sine function*/
	.int	_def_fk	/*; 29: Define a function key*/
	.int	_getrand	/*; 30: Return random number*/
	.int	_set_640	/*; 31: Set/clear 640 dot mode*/
	.int	_set_vdp	/*; 32: Set the displayed page*/
	.int	_set_vap	/*; 33: Set the access page*/
	.int	_set_fgcol	/*; 34: Set the foreground colour*/
	.int	_set_bgcol	/*; 35: Set the background colour*/
	.int	_set_bdcol	/*; 36: Set the border colour*/
	.int	_set_pal	/*; 37: Define a pallette entry*/
	.int	_rdch_shape	/*; 38: Get pointer to char shape*/
	.int	_def_chshape	/*; 39: Define a char shape*/
	.int	_def_wind	/*; 40: Define a window*/
	.int	_vid_address	/*; 41: Pixel byte address*/
	.int	_move_wind	/*; 42: Swap a window*/
	.int	_draw_vchar	/*; 43: Raw video char O/P*/
	.int	_fill_wind	/*; 44: Fill a window*/
	.int	_scurs_mode	/*; 45: Set cursor mode*/
	.int	_mousetrap	/*; 46: Mouse intercept vector*/
	.int	_fill		/*; 47: Area fill*/
	.int	_printf		/*; 48: printf function*/
	.int	_sprintf	/*; 49: sprintf function*/
	.int	__rset_pel	/*; 50: Raw pixel set*/
	.int	__set_pel	/*; 51: Pixel set in window*/
	.int	__rline		/*; 52: Raw line draw*/
	.int	__line		/*; 53: Line draw in window*/
	.int	__rread_pel	/*; 54: Raw pixel read*/
	.int	__read_pel	/*; 55: Pixel read in window*/
	.int	_sgfgcol	/*; 56: Set graphics foreground colour*/
	.int	__sgbgcol	/*; 57: Set graphics background colour*/
	.int	__sgtexture	/*; 58: Set graphics texture*/
	.int	_rcircle	/*; 59: Raw circle draw*/
	.int	_circle		/*; 60: Circle draw in window*/
	.int	_sdotmode	/*; 61: Set dot draw mode*/
	.int	_getmem		/*; 62: Get some memory*/
	.int	_getfmem	/*; 63: Get a particular block of memory*/
	.int	_freemem	/*; 64: Free a block of memory*/
	.int	_chdir		/*; 65: Change directory*/
	.int	_mkdir		/*; 66: Make a directory*/
	.int	_getfullpath	/*; 67: Get full file path*/
	.int	_pathcmp	/*; 68: Path compare*/
	.int	_floadrel	/*; 69: Relocate & load file by pathname*/
	.int	_anipsel	/*; 70: Select analog I/P*/
	.int	_anopsel	/*; 71: Select analog O/P*/
	.int	_anopdis	/*; 72: Disable analog O/P's*/
	.int	_adc		/*; 73: Perform AD conversion*/
	.int	_dac		/*; 74: D/A conversion*/
	.int	_set_led	/*; 75: Set LED*/
	.int	_freetone	/*; 76: Play free tone*/
	.int	_fttime	/*; 77: Return freetone status*/
	.int	_rdiport	/*; 78: Read input port*/
	.int	_rdbiport	/*; 79: Return buffered I/P port*/
	.int	_setstvec	/*; 80: Set a system trap vector*/
	.int	_new_cbuf	/*; 81: Assign new circular buffer area*/
	.int	_prog_sio	/*; 82: Program serial I/O channel*/
	.int	_gettdstr	/*; 83: Get time/date string*/
	.int	_nledit		/*; 84: Edit shorter line*/
	.int	__empty		/*; 85: Unused (was lbedit)*/
	.int	_ledit		/*; 86: Raw text edit*/
	.int	_iexec		/*; 87: Drop into monitor*/
	.int	_exec		/*; 88: Single monitor command*/
	.int	_callmrd	/*; 89: Call an MRD*/
	.int	_set_kvec	/*; 90: Set K/B scan vector*/
	.int	_clparse	/*; 91: Interpret argument array*/
	.int	_qsort		/*; 92: Sort code*/
	.int	_sliceargs	/*; 93: Separate args, expand wildcards*/
	.int	_cpuspeed	/*; 94: Get CPU speed*/
	.int	_find_driver	/*; 95: Find named driver number*/
	.int	_get_dvrlist	/*; 96: Get I or O driver list*/
	.int	_execa		/*; 97: exec(argv)*/
	.int	_execv		/*; 98: exec(path, argv)*/
	.int	_option		/*; 99: Set option*/
	.int	_inst_bdvr	/*; 100: Install block driver*/
	.int	__coldboot	/*; 101: Cold start the system*/
	.int	_find_bdvr	/*; 102: Find a block driver*/
	.int	_blkread	/*; 103: Read a block*/
	.int	_blkwrite	/*; 104: Write a block*/
	.int	_open		/*; 105: Open a file*/
	.int	_read		/*; 106: Read some bytes*/
	.int	_close		/*; 107: Close a file*/
	.int	_creat		/*; 108: Create an O/P file*/
	.int	_write		/*; 109: Write some bytes*/
	.int	_unlink 	/*; 110: Delete a file*/
	.int	_rename		/*; 111: Rename a file*/
	.int	_filestat	/*; 112: Read a file's directory entry*/
	.int	_readdir	/*; 113: Sequential directory read*/
	.int	_interpbec	/*; 114: Interpret block I/O error code*/
	.int	_seek		/*; 115: Seek to file position*/
	.int	_tell		/*; 116: Return current file pointer position*/
	.int	_bdmisc		/*; 117: Call block driver misc function*/
	.int	_processdir	/*; 118: Directory processing*/
	.int	_multiblkio	/*; 119: Multi-block I/O call*/
	.int	_fprintf	/*; 120: fprintf*/
	.int	_fputs		/*; 121: fputs*/
	.int	_errmes	/*; 122: Return pointer to error message*/
	.int	_fgets		/*; 123: fgets*/
	.int	_rdalldir	/*; 124: Read a whole directory*/
	.int	_oscontrol	/*; 125: Fiddly internal things*/
	.int	_getromver	/*; 126: Return ROM version number*/
	.int	_z80lock	/*; 127: Z80 arbitration*/
	.int	_aexeca	/*; 128: Sync/async exec*/
	.int	_proccntl	/*; 129: Process control*/
	.int	_newchset	/*; 130: Use a new char set table*/
	.int	_schedprep	/*; 131: Schedule a process*/
	.int	_pipe		/*; 132: Create a pipe*/
	.int	_cdmisc	/*; 133: Char device misc entry point*/
	.int	__empty		/*; 134: Link system call.*/
	.int	_alias		/*; 135: Aliasing control*/
	.int	_xbread		/*; 136: Xbus read*/
	.int	_xbread		/*; 137: Xbus write*/
	.int	_xbread		/*; 138: Xbus miscellaneous*/
	.int	_spr		/*; 139: Low-level printf*/
	.int	_crtc_init	/*; 140: Program the CRTC*/
	.int	_chkperm	/*; 141: Check file permissions*/
	.int	_fnledit	/*; 142: FD based line edit*/
	.int	_set_cpu	/*; 143: Set the CPU type*/
	.int	__empty		/*; 144: Unused*/
	.int	__empty		/*; 145: Unused*/
	.int	__empty		/*; 146: Unused*/
	.int	__empty		/*; 147: Unused*/
	.int	__empty		/*; 148: Unused*/
	.int	__empty		/*; 149: Unused*/

/*;REMEMBER TO CHANGE LIMIT IN COMPARISON ON ENTRY*/
_jtabe:
