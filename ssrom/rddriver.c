/*
 * Driver for RAM disk
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
#include <files.h>
#include <blkerrcodes.h>
#include <reloc.h>


#include "blockio.h"
#include "setmem.h"
#include "stubs.h"
#include "rddriver.h"

extern char *rdstart;		/* Where the disk is */

char *rdcksm;			/* The block checksum bytes */
int rdnblocks;			/* Size of disk in blocks */

char rdname[] = "/rd";

extern ushort romver;		/* Our ROM version number */
extern char *bitmaps;		/* Block device bitmaps */

/*
 * Initialise & install the driver. Work out disk size, start address, etc
 */

extern struct mrdriver mrdriver;

void rdsk_init(int reslevel)
{
	register int swtch;
	register struct rootblock *rbptr;
	int rdblkread(), rdblkwrite(), rdmisc();
	int blkbuf[BLOCKSIZE/4];

	if (reslevel == -1)
	{
		INST_BDVR(rdblkread, rdblkwrite, rdmisc, rdname, bitmaps);
		return;		/* Special case: to reserve block driver #0 */
	}

	rdnblocks = mrdriver.rdsize;

	rdcksm = (char *)rdstart + RDCKSMBLK * BLOCKSIZE; /* Where checksum table is */

	if (!reslevel)
	{
/* Set up the root block */
		clearmem(blkbuf, BLOCKSIZE);
		rbptr = (struct rootblock *)blkbuf;
		rbptr->nblocks = rdnblocks;
		rbptr->ssosver = romver;
		rbptr->bitmapstart = RDBITMAPBLOCK;
		rbptr->dirstart = RDDIRBLOCK;
		rbptr->ndirblocks = RDNDIRBLOCKS;
		rbptr->bootblock = 0;		/* Unbootable */
		rbptr->removable = 0;		/* Not removable */

		clearmem(&rbptr->rootdir, sizeof(rbptr->rootdir));
		strcpy(rbptr->rootdir.file_name, rdname);
		rbptr->rootdir.file_size = RDNDIRBLOCKS;
		rbptr->rootdir.statbits = DIRECTORY;
		rbptr->rootdir.blkmapblk = RDDIRBLOCK;

		rdblkwrite(ROOTBLOCK, blkbuf);

/* Set up the block use bitmap block */
		clearmem(blkbuf, BLOCKSIZE);
		blkbuf[0] = (int)RDBLKMASK;
		rdblkwrite(RDBITMAPBLOCK, blkbuf);

/* Clear the directory blocks */
		clearmem(blkbuf, BLOCKSIZE);
		for (swtch = RDDIRBLOCK; swtch < (RDDIRBLOCK + RDNDIRBLOCKS); swtch++)
			rdblkwrite(swtch, blkbuf);
		INST_BDVR(rdblkread, rdblkwrite, rdmisc, rdname, bitmaps);
	}

}

int rdblkread(int block,char *buf)
{
	char cksm;

	if ((uint)block >= (uint)rdnblocks)
		return BEC_BADBLOCK;
	_set_led(1);
	cksm = _rdmove(rdstart + block * BLOCKSIZE, buf);
	_set_led(0);
	if (cksm != rdcksm[block])
		return BEC_IOERROR;
	return 0;
}

rdblkwrite(block, buf)
int block;
char *buf;
{
	if ((uint)block >= (uint)rdnblocks)
		return BEC_BADBLOCK;
	_set_led(1);
	rdcksm[block] = _rdmove(buf, rdstart + block * BLOCKSIZE);
	_set_led(0);
	return 0;
}

/*
 * Miscellaneous call entry point
 */

rdmisc(code, arg1)
{
	return 0;
}
