/* TABS4
 * Task scheduling stuff
 */

#include <types.h>
#include <syscalls.h>
#include <spl.h>
#include <signal.h>
#include <reloc.h>
#include <exception.h>
#include "spspace.h"
#include "process.h"
#include "exfile.h"
#include "mondefs.h"
#include "envbits.h"
#include "newmem.h"
#include "datetime.h"
#include "psprintf.h"
#include "fileio.h"
#include "env.h"
#include "setmem.h"
#include "countargs.h"
#include "prcrlf.h"
#include "newmem.h"
#include "ssdefs.h"
#include "movmem.h"
#include "proc.h"
#include "boot.h"
#include "exfile.h"
#include "systrap.h"
#include "kernel.h"
//#include <splfns.h>

/* Time slice for each process in 20 msec ticks */
#define TIMESLICE	1

PROC **proctable;	/* The process table */
int syssafe;		/* Zero if system can be interrupted */
int syssp;		/* Used by assembly code for saving system SP */
int switchpending;	/* Set in interrupt code to indicate suspended switch */
int curpid;
int curhomeshell;	/* PID of home shell for curpid */
char stackanalyse;	/* If true, print stack stats on exit */
int timeslice;
int mtnotup;		/* If true, scheduler has not started */
int (*cswvector)();	/* Context switch vector */
int charsignalpending;	/* SIGHUP or SIGINT from char device */
int nextpg;

extern int lastpid;	/* In memory manager */
extern uint stdin, stdout, stderr;
extern uint vsi_count;		/* System clock */
extern char *cur_path;
extern int fsbusy, fsbpid;
extern int ouruid, umask;
extern char is68010;
extern int reslevel;
extern int memerrflag;
extern char *excepmes;
extern int nmrdrivers;
extern uint randnum;

extern char iexecname;
extern char *room1for(), *room0for();
extern int enterproc(void *addr);
extern void bootup(void);
extern void cexception(int);


char last_path[80];

void scheduler(void);
void rootshell(int argc,char *argv);
void crankup(int argc,char *argv);
void freepsig(PROC *proc);
void freeprocsignals(PROC *proc);


/* Active processes */
uchar runlist[MAXPIDS];
int runlcount;		/* Number of entries in it */
int runlpos;		/* Current position */
ulong lastenvbits;

/* Inline function to find next PID in run list */
#define SCANPIDS() \
{ \
	if (runlpos >= runlcount) \
		runlpos = 0; \
	curpid = runlist[runlpos++]; \
} \

void proc_init(void)
{
	proctable = (PROC **)getzmem1(MAXPIDS * sizeof(*proctable));
	curpid = 0;
	curhomeshell = 0;
	switchpending = 0;
	stackanalyse = 0;
	cswvector = 0;
	runlcount = 0;
	runlpos = 0;
	syssafe = 1;
	nextpg = 2;
	charsignalpending = 0;
	if (reslevel == 0)
		lastenvbits = (ulong)DEF_ENVBITS;
#ifdef notdef
	else
		lastenvbits &= (ulong)DEF_ENVBITS;
#endif
}

/*
 * The system is starting: install the root process in the
 * process table and fire it off
 */

void startsched(void)
{
	static char *argv[2] = {"<startup>", 0};

	_splx(0x0200);
	proc_init();
	mtnotup = 0;
	curpid = 0;
	SCHEDPREP(rootshell, argv, SCP_ISSHELL, 0x2000);
	scheduler();
}

/*
 * The top of the process tree
 */

void rootshell(int argc,char *argv)
{
	extern int (*user_patch)();

	if (reslevel == 0)
		*last_path = 0;

/* This has been moved: call mrd's before using file system */
	{
		int i;

		/* The MRdrivers and boot code are owned by rootshell */
		/* Call the MRdrivers with the reset level */
		for (i = 0; i < nmrdrivers; i++)
			CALLMRD(i, reslevel, 0);
	}

	if (user_patch)
		(*user_patch)(reslevel, 1);

	if (!(*last_path) || CHDIR(last_path) < 0)
		CHDIR("/rd");
	rehash(0);		/* Reread execution path directories */
#ifdef ANU_VERSION
	do_ssasm_macros();
#endif
	/* Run crankup as a task so we can save stack space in rootshell */
	SCHEDPREP(crankup, argv, 0, MRD_STACKSPACE);
	lastpid = 1;		/* Make the shell process 1 (maybe) */
	/* cd again in case autoexec, etc did a chdir */
	if (*last_path)
		CHDIR(last_path);
	birthday();
	if (user_patch)
		(*user_patch)(reslevel, 2);

	for ( ; ; )
	{
		IEXEC(4);
		conprintf("root shell died: restarting\r\n");
	}
}

void crankup(int argc,char *argv)
{
	int i;

	/* New stuff: call mrds so they can schedule stuff */
	for (i = 0; i < nmrdrivers; i++)
		CALLMRD(i, MRD_SCHEDULER, 0);

	/* Attempt to boot a block device: presumably run autoexec */
	bootup();
}

/*
 * System call 131
 *
 * Prepare some code for execution. Put an entry in the process table and set
 * it up. Make copies of its arguments.
 * Return some error code, or the new pid.
 */

int _schedprep(long calladdr,char **argv,int flags,uint stackspace)
{
	register int pid;
	register int argc;
	register int retval;
	register int arg;
	register char **sp;
	register PROC *proc, *curproc;
	USERREGS *uregs;
	char **argcopy;
	char *argtype;
	uint *argval;
	int curpidsave;
	int procreturn();

	if (mtnotup)
	{
		conprintf("Panic in schedprep: system not started\r\n");
		_splx(0x0700);
		for ( ; ; )
			;
	}

	syssafe++;
	curproc = proctable[curpid];	/* CAN BE NIL AT INIT TIME */
	if (syssafe < 1)
	{
		conprintf("Panic in schedprep: syssafe = %d\r\n", syssafe);
		return -1;
	}
	pid = GETMEM(0, 11);		/* Get PID from memory allocator */
	if (pid < 0)
	{
		retval = pid;
		goto out;
	}
	proc = (PROC *)getzmem1(sizeof(*proc));
	if ((int)proc < 0)
	{
		conprintf("Out of memory in schedprep\r\n");
		retval = (int)proc;
nomem2:
		clearpidmem(pid);
		goto out;
	}
	proc->pid = pid;
	proc->parent = curpid;
	proc->child = -1;
	proc->idev = stdin;
	proc->odev = stdout;
	proc->edev = stderr;
	proc->orig_idev = proc->idev;
	proc->orig_odev = proc->odev;
	proc->orig_edev = proc->edev;
	proc->lastchildpid = -1;

	if (curproc)
	{
		curproc->lastchildpid = pid;
		proc->pg = curproc->pg;
	}
	else
		proc->pg = nextpg++;

	proc->uid = ouruid;
	proc->umask = umask;
	/* Inherit parent working directory */
	proc->cur_path = room1for(cur_path);
	incuser(stdin, pid,  argv[0]);
	incuser(stdout, pid, argv[0]);
	incuser(stderr, pid, argv[0]);

	proc->starttime = vsi_count;
	proc->flags = (flags & SCP_ISBINARY) ? PS_BINARY : 0;
	proc->loadaddr = calladdr;

	/*
	 * If the current process (the parent) is a shell one,
	 * it becomes the new process's home process.
	 */
	if (!curproc || (curproc->flags & PS_SHELLPROC))
	{
		proc->homeshell = curpid;
	}
	else
	{
		proc->homeshell = curproc->homeshell;
	}

	/*
	 * If new process is a shell type process then create a new
	 * environment structure for it
	 */
	if (flags & SCP_ISSHELL)
	{
		struct shellenv *shellenv;

		proc->flags |= PS_SHELLPROC;	/* Shell type process */
		shellenv = (struct shellenv *)getzmem1(sizeof(struct shellenv));
		proc->shellenv = shellenv;
		shellenv->nusers = 1;
		shellenv->ibcvec = (char *)0;
		if (!curproc)
		{	/* <startup> */
			shellenv->envbits = lastenvbits;
		}
		else
		{
			shellenv->envbits = curproc->shellenv->envbits;
			copyenvstrings(proc, curproc);
			if (curproc->shellenv->ibcvec)
			{
				shellenv->ibcvec = (char *)getmem1(NMONCMDS);
				movmem(curproc->shellenv->ibcvec, shellenv->ibcvec,
						NMONCMDS);
			}
		}
	}
	else
	{
		proc->shellenv = curproc->shellenv;
		proc->shellenv->nusers++;
	}

	/* startup gets default timeslice. From then on everyone inherits */
	if (!curpid || !curproc)
	{	/* startup */
		proc->timeslice = 1;
		proc->bgtimeslice = 0;
	}
	else
	{
		proc->timeslice =
			(flags & SCP_ISASYNC) ? curproc->bgtimeslice : curproc->timeslice;
		proc->bgtimeslice = curproc->bgtimeslice;
	}

	stackspace = (stackspace + 3) & ~3;
	proc->stack = (long)getmem1(stackspace);
	if (proc->stack < 0)
	{
		eprintf("Cannot allocate memory for stack space\r\n");
		retval = proc->stack;
nomem1:
		dofreemem(proc);
		goto nomem2;
	}
	if (stackanalyse)
		lsetmem(&proc->stack, -1, stackspace / 4);
	proc->name = room1for(argv[0]);
	proc->stackspace = stackspace;
	proc->stackbot = proc->stack + proc->stackspace / 8;
	/* Prepare the processes argument list in its stack space */
	argc = countargs(argv);
	curpidsave = curpid;	/* Allocate args in user memory */
	curpid = pid;		/* So MM allocates args in user space */
	argcopy = (char **)getmem0((argc + 1) * sizeof(*argcopy));
	for (arg = 0; arg < argc; arg++)
	{
		argcopy[arg] = room0for(argv[arg]);
		if ((int)argcopy[arg] < 0)
		{
			eprintf("Cannot get memory for arguments\r\n");
			dofreemem(proc->name);
			retval = -1;
			curpid = curpidsave;
			goto nomem1;
		}
	}
	argcopy[arg] = 0;	/* Terminate the pointer list */
	proc->argv = argcopy;
	argtype = (char *)getmem0((MAXMONPAR+1) * sizeof(*argtype));
	argval = (uint *)getmem0((MAXMONPAR+1) * sizeof(*argval));
	CLPARSE(argv, argtype, argval);		/* Evaluate argtype */

	curpid = curpidsave;

	/* Start setting up entry stack frame */
	sp = (char **)(proc->stack + proc->stackspace);
	*--sp = (char *)argval;
	*--sp = argtype;
	*--sp = (char *)argcopy;
	*--sp = (char *)argc;
	/* Now shove on a return address for programs which do an RTS */
	*--sp = (char *)procreturn;

	if (is68010)
	{	/* Extra word for 68010 */
		ushort *wsp;

		wsp = (ushort *)sp;
		*--wsp = 0;
		sp = (char **)wsp;
	}
	/* Make a register set */
	sp = (char **)((int)sp - sizeof(USERREGS));
	uregs = (USERREGS *)sp;
	clearmem(uregs, sizeof(*uregs));
	uregs->sr = 0x2000;		/* Supervisor mode */
	uregs->pc = calladdr;
	proc->sp = (long)uregs;
	/* Make it official! */
	proctable[pid] = proc;
	runlist[runlcount++] = pid;

	/* If synchronous, block the parent */
	if (!(flags & SCP_ISASYNC) && (pid != 0))
	{
		if (curproc)
		{
			curproc->flags |= PS_BLOCKED;
			curproc->child = pid;
		}
		proc->flags |= PS_PARBLOCKED;
		switchpending = 1;	/* Force immediate exit from curpid */
	}

	/* Unblock signals for the parent */
	SIGBLOCK(curpid, 0);

	retval = pid;
out:
	syssafe--;
	return retval;
}

/*
 * The main processing loop. Run around process table.
 * There must be one or more entries in the process table!
 */

void scheduler(void)
{
	register int starttime, stoptime;
	register int i;
	int fscleanup = 0;
	int dyingpid = -1;	/* The process which is to die */
	int foundpid;
	register long *sp;
	register PROC *proc, *parproc;
	USERREGS *uregs;
	int sigreturn();

	curpid = 0;
	syssafe = 1;
loop:	/* Save an indent level (!) */
	proc = proctable[curpid];
	/* Run this process for a while */
	stdin = proc->idev;
	stdout = proc->odev;
	stderr = proc->edev;
	umask = proc->umask;
	ouruid = proc->uid;
	cur_path = proc->cur_path;
	/* Does it have a signal pending? */
	if (proc->procsig && !(proc->flags & PS_SIGBLOCK))
	{
		if (fsbusy && (fsbpid == curpid))
			switchpending = 1;
		else
		{
			PROCSIG *procsig = proc->procsig;
			/* Alter the stack frame to enter signal handler */
			sp = (long *)proc->sp;
			*--sp = procsig->whofrom;
			*--sp = procsig->sigarg;
			*--sp = procsig->sigtype;	/* Args to signal handler */
			*--sp = (long)sigreturn;	/* signal handler cleanup code */
			freepsig(proc);				/* Unlink it */

			if (is68010)
			{	/* Extra word for 68010 */
				ushort *wsp;

				wsp = (ushort *)sp;
				*--wsp = 0;
				sp = (long *)wsp;
			}
			/* Make a register set */
			uregs = (USERREGS *)sp;
			uregs--;
			clearmem(uregs, sizeof(*uregs));
			uregs->sr = 0x2000;		/* Supervisor mode */
			uregs->pc = proc->sigvec;
			proc->sp = (long)uregs;
		}
	}
	if (proc->flags & PS_TRACETOG)
	{
		((USERREGS *)proc->sp)->sr ^= (ushort)0x8000;
		proc->flags &= ~PS_TRACETOG;
	}
	curhomeshell = proc->homeshell;

	/* Tell external code about switch */
	if (cswvector)
		(*cswvector)(curpid, proc, vsi_count, 0);

	starttime = vsi_count;
	_splx(0x0200);
	/*
	 * If switchpending is true, the process will be tugged on the
	 * termination of system calls, as well as on clock ticks
	 */
	if (!switchpending)
		switchpending = fscleanup;
	timeslice = proc->timeslice;
	/* 4.6 */
	if (timeslice < 0)
	{
		timeslice = 1;
		proc->tscount = 0;
	}

	syssafe--;
	proc->sp = enterproc(&proc->sp);		/* Go do it */
	syssafe++;
	_splx(0);

	/* 4.7: switchpending can still be set here */
	if (!fsbusy)
		switchpending = 0;

	/* Tell external code about switch */
	if (cswvector)
		(*cswvector)(curpid, proc, vsi_count, 1);

	randnum++;
	proc->idev = stdin;			/* Pick up any changed standard descriptors */
	proc->odev = stdout;
	proc->edev = stderr;
	proc->cur_path = cur_path;		/* If it did a CHDIR() */
	proc->umask = umask;
	proc->uid = ouruid;
	/* Restore system's UID */
	ouruid = 0;
	if ((proc->sp < proc->stackbot) && !(proc->flags & PS_NOSPCHECK))
	{	/* Stack overrun */
		int timer;
		conprintf("Process '%s' stack overrun. sp=$%x, limits:$%x<->$%x\r\n",
					proc->name, proc->sp, proc->stackbot,
						proc->stack + proc->stackspace);
		conprintf("Use 'chmem' to fix.\r\n");
		if (proc->sp < proc->stack)
		{
reboot:			conprintf("Rebooting...");
			for (timer = 0; timer < 600000; timer++)
				;
			prcrlf();
			prcrlf();
			excepmes = "Stack overrun";
			cexception(((USERREGS *)proc->sp)->pc);
		}
		else
		{
			if (fsbusy && (curpid == fsbpid))
			{
				conprintf("in file system: ");
				goto reboot;
			}
			conprintf("Killing\r\n");
			proc->flags |= PS_EXIT|PS_KILLED;
		}
	}
	stoptime = vsi_count;
	proc->ticks += stoptime - starttime;

	if (fscleanup && (fsbusy == 0))
	{	/* The FS is free! kill the process which is waiting to die */
		curpid = dyingpid;
		fscleanup = 0;
	}
	else
	{
		/* If alive, search from next entry */
		if (!(proc->flags & PS_EXIT))
			SCANPIDS();
	}
nxtpid:
	proc = proctable[curpid];

	if (proc->alarmtime && (vsi_count > proc->alarmtime))
	{
		SIGSEND(curpid, SIGALRM, 0);
		proc->alarmtime = 0;
	}

	if (proc->flags & PS_EXIT)
	{	/* A dead process */
		int childpid;

		if (fscleanup && (curpid != dyingpid))
		{	/* Can't kill this one yet! */
			SCANPIDS();
			goto nxtpid;
		}
		/*
		 * Test to see if any process is using the file system.
		 * If so, we can't close up this processes files, so run
		 * the process which is using the file system until it
		 * frees it.
		 * If THIS process died in the file system, run it until
		 * it leaves it.
		 * If another process holds the FS, run it until it leaves
		 * the FS, then come back here
		 */
		if (fsbusy)
		{
			fscleanup = 1;
			dyingpid = curpid;
			curpid = fsbpid;
			goto loop;	/* Run it until it leaves FS */
		}

		/*
		 * Find all its child processes and make startup adopt them
		 */
		for (childpid = 0; childpid < MAXPIDS; childpid++)
		{	/* Borrow parproc coz its a register */
			if ((parproc = proctable[childpid]))
			{
				/* Adopt children */
				if (parproc->parent == curpid)
				{
					parproc->parent = 0;
					/* Prevent it from awakening startup */
					parproc->flags &= ~PS_PARBLOCKED;
				}
				/* Make startup the home shell */
				if (parproc->homeshell == curpid)
					parproc->homeshell = 0;
				/* If it wants signalling about exit, do it */
				if ((parproc->flags & PS_SIGEXIT) && (childpid != curpid))
					SIGSEND(childpid, (SIGEXIT), curpid);
			}
		}

		parproc = proctable[proc->parent];
/* Bug here: made root shell have self as child, hangs up SENDINTERRUPT() */
#ifdef notdef
		parproc->child = 1;	/* Hasn't got one now */
#else
		if (parproc->child == curpid)
			parproc->child = -1;
/* And also: */
		if (parproc->lastchildpid == curpid)
			parproc->lastchildpid = -1;
#endif
		/* If it blocked its parent, let parent run again */
		if (proc->flags & PS_PARBLOCKED)
		{
			USERREGS *pureg;
			/* Copy the exit code into parent's d0 */
			pureg = (USERREGS *)parproc->sp;
			pureg->regs[0] = proc->exitcode;
			/* Wake up parent */
			parproc->flags &= ~PS_BLOCKED;
		}
		else
		{	/* Signal parent about its death */
			if (proc->parent && parproc->sigvec)
			/* Don't use system call to avoid yanking on return */
				_proccntl(11, proc->parent, SIGCLD, curpid,0);
		}
		/* Do we want stack stats? */
		if (stackanalyse)
		{
			int *spptr, count;
			spptr = (int *)proc->stack;
			count = proc->stackspace / 4;
			while (count && (*spptr == -1))
			{
				spptr++;
				count--;
			}
			FPRINTF(parproc->edev,
			"'%s' ticks: %d memory usage: stack $%x, allocated $%x\r\n",
				proc->name, proc->ticks, count * sizeof(int),
					calcprocmem());
		}
		/* Accumulate child time */
		parproc->childtime += proc->ticks;
		if (proc->flags & PS_BINARY)
		{
			if (stackanalyse)
				conprintf("freeing 0x%x\r\n", proc->loadaddr);
			dofreemem((void *)proc->loadaddr);
		}
		dofreemem((void *)proc->stack);

		if (--proc->shellenv->nusers <= 0)
		{
			maybefree(proc->shellenv->ibcvec);
			TRASHENVSTRINGS(proc->pid);
			dofreemem(proc->shellenv);
			/* This should stop is from being used again */
			proc->shellenv = (struct shellenv *)0xbbbbbbbb;
		}

		tryclose(proc->idev);
		tryclose(proc->odev);
		tryclose(proc->edev);
		closeprocfiles(curpid);
		dofreemem(proc->cur_path);
		dofreemem(proc->name);

		/* Free the list of signals */
		freeprocsignals(proc);

		/* Take it out of the run list */
		foundpid = 0;
		for (i = 0; i < MAXPIDS - 1; i++)
		{
			if (runlist[i] == curpid)
				foundpid = 1;
			if (foundpid)
				runlist[i] = runlist[i + 1];
		}
		runlcount--;

		dofreemem(proc);

		/* Empty the slot in the process table */
		proctable[curpid] = 0;

		/* The next call INVALIDATES the memory manager state
		 * until curpid is incremented to a sane PID
		 */
		clearpidmem(curpid);
		SCANPIDS();	/* Find next entry */
		goto nxtpid;
	}

	/* Look to see if any char device signals are pending */
	if (charsignalpending)
		docharsignals();

	randnum++;

	/*
	 * Don't run curpid if
	 *    Blocked or stopped
	 *    Snoozing & snooze thing says not ready
	 *    Process has -ve timeslice and count has not expired
	 *    Sleeping
	 */
	if
	(
			(proc->flags & (PS_BLOCKED|PS_STOPPED))
		||	(proc->snoozevec &&
			  !(*proc->snoozevec)(proc->snoozearg1,proc->snoozearg2,vsi_count))
		||	(proc->timeslice < 0 && --proc->tscount > proc->timeslice)
		||	(proc->whennext > vsi_count)
	)
	{
		SCANPIDS();
		goto nxtpid;
	}

	if (proc->snoozevec)
	{	/* Arisen from snooze: Make the return value zero */
		uregs = (USERREGS *)proc->sp;
		uregs->regs[0] = 0;
		proc->snoozevec = 0;
	}

	goto loop;
}

#if 0
scanpids()
{
	if (runlpos >= runlcount)
		runlpos = 0;
	curpid = runlistrunlpos++;
}
#endif
/*
 * Return either pid name or load address address
 */

int curpidname(mode)
{
	PROC *proc;

	if (mtnotup || !(proc = proctable[curpid]))
		return (mode == 0) ? (int)"" : 0;
	return (mode == 0) ? (int)proc->name : proc->loadaddr;
}

/*
 * Append a signal to a process's list
 */

void sigput(PROC *proc,int sigtype,int sigarg)
{
	PROCSIG **pprocsig, *procsig;

	pprocsig = &proc->procsig;
	while (*pprocsig)
		pprocsig = &(*pprocsig)->next;
	procsig = (PROCSIG *)getzmem1(sizeof(*procsig));
	procsig->sigtype = sigtype;
	procsig->sigarg = sigarg;
	procsig->whofrom = curpid;
	*pprocsig = procsig;
}

/*
 * Dump all signals from list
 */

void freeprocsignals(PROC *proc)
{
	while (proc->procsig)
		freepsig(proc);
}

/*
 * Dump one signal from head
 */

void freepsig(PROC *proc)
{
	PROCSIG *procsig;

	procsig = proc->procsig->next;
	dofreemem(proc->procsig);
	proc->procsig = procsig;
}
