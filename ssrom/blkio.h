/* TABS4 NONDOC
 * Header file for files & block I/O
 */

#define BLOCKSIZE	1024		/* How big the disk blocks are */
#define BSIZEM1		(BLOCKSIZE-1)	/* Block size less 1 */
#define BSIZEO2		(BLOCKSIZE/2)	/* No. of entries in a blockmap block */
#define BSIZEO2M1	(BSIZEO2 - 1)

#define NBLKDRIVERS	16		/* Number of permitted block drivers */
#define BOOTADDR	0x3c00		/* Where boot block is loaded */

#define DIRPERBLOCK	(BLOCKSIZE / sizeof(struct dir_entry))

struct rootblock
{
		unsigned short nblocks;		/* Number of blocks */
		unsigned short ssosver;		/* 1616/OS creation version */
		unsigned short bitmapstart;	/* Where bitmap starts */
		unsigned short dirstart;	/* Where directory starts */
		unsigned short ndirblocks;	/* Number of directory blocks */
		unsigned short removable;	/* If non-zero, media removable */
		unsigned short bootblock;	/* If non-zero, block to load & boot */
		unsigned int special;		/* Random number */
		struct dir_entry rootdir;	/* The root directory */
};

struct blkdriver
{
		char devname[FNAMESIZE];	/* Device name */
		int (*bread)();
		int (*bwrite)();
		int (*misc)();				/* Miscellaneous functions */
		unsigned int usetimer;		/* Time since last used (12.5 Hz) */
		uchar *bitmap;				/* Block usage bitmap */
		uchar bitmapchanged;		/* True if the bitmap must be written */
		struct rootblock rootblock;	/* Root block from disk */
		/* Additions for V3.3 */
		int (*multirw)();			/* Nonzero: supports multisector I/O */
		int version;				/* Multi-sector drivers version */
		ushort bmoffset;			/* Offset into bitmap for free search */
		ushort nblksused;			/* No. of set bits in bitmap */
		ushort disabled;			/* Device disabled */
		ushort uid0;				/* Only accessible by UID 0 */
		ushort wrprot;				/* Cannot write */
};

/* The structure pointed to by a multi-block driver when it installs itself */
struct multidriver
{
		int (*misc)();		/* Miscellaneous entry point */
		int (*multirw)();
		int version;		/* Multi I/O version type */
		int reserved13;	/* 16 longwords */
};

/* Commands passed to multiblkio system call and to multi-block device drivers */

#define MIO_SWRITE 0		/* sequential multi-block write */
#define MIO_SREAD 1			/* sequential multi-block read */
#define MIO_RWRITE 2		/* random multi-block write */
#define MIO_RREAD 3			/* random multi-block read */


/* Disk block definitions */
#define ROOTBLOCK	0		/* Contains number of blocks, etc. */

/* RAM disk specific stuff */
#define RDBITMAPBLOCK	1	/* Block usage map */
#define RDCKSMBLK		2	/* Where block cksm's are */
#define RDBOOTBLOCK		3	/* Unused boot block */
#define RDDIRBLOCK		4	/* First directory block */

#define RDNDIRBLOCKS	4	/* 4k directory: 64 files */
#define RDBLKMASK	0xff000000L	/* Reserve 8 blocks */

/* Define the misc() calls */
#define MB_FLUSH	1	/* Flush buffers */
#define MB_RESET	2	/* Reset has occurred */
#define MB_VERS		3	/* Return driver version number */
#define MB_NEWDISK	4	/* New disk has been detected */
#define MB_DIRREAD	5	/* About to do directory block I/O */
#define MB_NDIRREAD	6	/* About to do non-directory block I/O */
#define MB_BMBREAD	7	/* About to do blockmap I/O */
#define MB_DEBUG	8	/* MH's debug code: set up a debug flag */

/* Internal misc() calls: no call made to device driver */
#define MB_NBLOCKS		0x100	/* Request total block count */
#define MB_USEDBLOCKS	0x101	/* Request number of used blocks */
#define MB_FREEBLOCKS	0x102	/* Request free block count */
#define MB_READROOT		0x103	/* Get copy of root block */
#define MB_REREAD		0x104	/* Force reread of rootblock, bitmap */
#define MB_DISABLE		0x105	/* Disable the device */
#define MB_UID0			0x106	/* Only UID0 can access */
#define MB_WRPROT		0x107	/* Write protect it */
