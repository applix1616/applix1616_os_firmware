#ifndef CACHECONT_H
#define CACHECONT_H

extern void cache_init(void);
extern int findde(char *fullpath,uint *pbdvrnum,uint *pdirpos,struct dir_entry *pdirent);
extern void cachedel(char *fullpath);
extern int cacheadd(char *fullpath,ushort bdvrnum,struct dir_entry *dirptr,uint dirindex);

#endif
