/*;
; Compare s1 with s2, stopping at end of s1. Return 0 on match
;*/

		.global	_scp
_scp:
		move.l	4(sp),a0
		move.l	8(sp),a1

loopon:
		move.b	(a0)+,d0	/*; At end of source: match*/
		beq.b	same

		move.b	(a1)+,d1
		cmp.b	d0,d1
		beq.b	loopon

/*; Maybe case mismatch?*/

/*; Convert d0 to upper case*/
		cmp.b	#'a',d0
		blt		d0notlower
		cmp.b	#'z',d0
		bgt		d0notlower
		sub.b	#0x20,d0
d0notlower:
		cmp.b	d0,d1
		beq.b	loopon

/*; Convert d1 to upper case*/
		cmp.b	#'a',d1
		blt		d1notlower
		cmp.b	#'z',d1
		bgt		d1notlower
		sub.b	#0x20,d1
d1notlower:
		cmp.b	d0,d1
		beq.b	loopon

/*; No match*/
		move.b	#1,d0
		rts
same:
		clr.l	d0
		rts

