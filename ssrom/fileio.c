/*	TABS4
 * File level I/O driver
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <files.h>

#include <blkerrcodes.h>



#include "constants.h"
#include "debug.h"
#include "blockio.h"
#include "fcb.h"
#include "filefns.h"
#include "ischdev.h"
#include "chario.h"
#include "newmem.h"
#include "pipe.h"
#include "movmem.h"
#include "monfns.h"
#include "cachecont.h"
#include "scp.h"
#include "roomfor.h"
#include "splitpath.h"
#include "strucmp.h"
#include "movstr.h"
#include "fileio.h"

/* File control blocks */
struct fcb **ppfcbs;
uint maxfiles;			/* Number of file control blocks */

extern char *cur_path;	/* Full path of current directory */
extern int curpid;		/* Current process ID */
extern char slash;
extern uint stdout, stdin, stderr;
extern int umask;		/* File creation/modification mask */
extern struct blkdriver bdrivers[NBLKDRIVERS];
extern int ouruid;

int growblockmap(struct fcb *fcbptr,uint bmindex);
int chknincp(char *fullpath);
int dorename(register char *pathname,register char *newpathname,int canmove);


/*
 * System call 105: Open a file or char device for input/output. Return code on error,
 * else fd for future references.
 *
 * Open modes:
 * SSO_RDONLY (1):	Read only
 * SSO_APPEND (2):
 * SSO_WRONLY (2):	Read/write (append) (was write only in pre-3.0 O/S)
 * SSO_RDWR   (3):	Read/write
 * V4.2b:
 * SSO_TRUNCATE (4):	Read/write, truncated
 *
 * V3.4 and on, SSO_WRONLY creates if not there
 * V4.4: added extra load_addr argument so files created
 *       with a news load_addr can get that correctly.
 */
int _open(char *pathname,register uint mode, uint load_addr)
{
	register char *fullpath;
	register int fcbnum, bdvrnum;
	register struct fcb *fcbptr;
	register int retval;
	int trunc;
	char fname[FNAMESIZE];
	struct dir_entry dirent;

	if ((trunc = (mode == SSO_TRUNCATE)))
		mode = SSO_RDWR;
	if (!*pathname)
		return BEC_FNBAD;
	if (!mode || (mode > SSO_RDWR))
		return BEC_BADARG;
	fsentry(0);

	fullpath =  (char *)GETFULLPATH(pathname, 0);

/* Is it a char device? */
	if (ischdev(fullpath))
	{
		retval = openchdvr(fullpath, mode);
		goto out;
	}

	if ((retval = FILESTAT(fullpath, &dirent)) < 0)
	{
		if ((retval == BEC_NOFILE) && (mode == SSO_APPEND))
		{
			fsexit(0);
			return CREAT(pathname, 0, 0);
		}
		goto out;
	}

	if ((retval == SSO_RDWR) && (mode != SSO_RDONLY))
	{	/* Can't write it twice */
		retval = BEC_FWOPEN;
		goto out;
	}

	if ((mode == SSO_RDWR) || (mode == SSO_APPEND))
	{	/* Writing */
		if (retval)	/* Can't write to file being read */
		{
			retval = BEC_FROPEN;
			goto out;
		}
		if (dirent.statbits & LOCKED)
		{
			retval = BEC_LOCKED;
			goto out;
		}

		retval = CHKPERM(&dirent, FS_NOWRITE, fullpath);

		if (retval)
			goto out;
	}
	else
	{
		retval = CHKPERM(&dirent, FS_NOREAD, fullpath);
		if (retval)
			goto out;
	}

	if (dirent.statbits & DIRECTORY)
	{
		retval = BEC_ISDIR;
		goto out;
	}
	if ((bdvrnum = get_bdev(fullpath, fname)) < 0)
	{
		retval = bdvrnum;
		goto out;
	}
	if ((fcbnum = get_fcb(fullpath, 0)) < 0)
	{
		retval = fcbnum;
		goto out;
	}
	fcbptr = ppfcbs[fcbnum];

	fcbptr->bdvrnum = bdvrnum;
	if (!(retval = find_file(fullpath,
						fcbnum,
							trunc ? 3 : 1,
								0,
									trunc ? (char *)load_addr : 0)))
	{
		fcbptr->openmode = (mode == SSO_APPEND) ? SSO_RDWR : mode;
		if (mode == SSO_APPEND)
		{	/* Append mode: seek to end */
			retval = SEEK(fcbnum + NIODRIVERS, 0, 2);
			if (retval < 0)
			{
				free_fcb(fcbnum, 0);
				goto out;
			}
		}
		retval = fcbnum + NIODRIVERS;	/* Return the fd */
	}
	else
	{
		free_fcb(fcbnum, 0);	/* Open failed: re-free the fcb */
		if (retval >= 0)
			retval = BEC_NOFILE;
	}
out:
	dofreemem(fullpath);
	fsexit(0);
	return retval;
}

/*
 * Passed fd, return true if it is a file or if it is a pipe and some chars
 * are available
 */

int fileready(int fd,int issgetc)
{
	register struct fcb *fcbptr;
	register struct pipefcb *pipeptr;
	register struct apipe *apipeptr;

	fd -= NIODRIVERS;
	if (fd >= maxfiles || ((fcbptr = ppfcbs[fd]) == 0))
		return BEC_BADFD;
	if ((pipeptr = fcbptr->pipefcb))
	{
		if ((issgetc && !pipeptr->isoutput) ||
			(!issgetc && pipeptr->isoutput))
			return BEC_BADFD;
		apipeptr = pipeptr->apipe;
		return issgetc ? apipeptr->head - apipeptr->tail :
			apipeptr->head - (apipeptr->tail + 1) % PIPEBUFSIZE;
	}
	return 1;	/* Regular file */
}

/*
 * System call 106: Read some bytes. Return number of bytes read,
 * or code on error
 */

int _read(uint fd,char *buf,int nbytes)
{
	register struct fcb *fcbptr;
	register uint fcbnum;
	register int nread = 0;
	register int retval = 0;
	register int bbufindex;
	register int ntomove;

	if (fd >= 0x80)
		fixfd(&fd);		/* Convert STDIN, STDOUT, STDERR */

	if (fd < NIODRIVERS)
	{	/* Character device */
		retval = chread(fd, (uchar *) buf, nbytes);
		if (retval == BEC_RPASTEOF)
			retval = 0;
		return retval;
	}

	fsentry(0);

	fcbnum = fd - NIODRIVERS;
	if (fcbnum >= maxfiles)
	{
		retval = BEC_BADFD;
		goto err;
	}
	fcbptr = ppfcbs[fcbnum];
	if (!fcbptr)
	{
		retval = BEC_BADFD;
		goto err;
	}

	if (fcbptr->pipefcb)
		return piperead(fcbnum,(uchar *) buf, nbytes);

	if (fcbptr->file_pos >= fcbptr->file_size)
	{
		retval = 0;	/* Can't read anything */
		goto err;
	}

	bbufindex = fcbptr->file_pos & BSIZEM1;
	if (nbytes > BLOCKSIZE - bbufindex)	/* Need to read blocks */
		BDMISC(fcbptr->bdvrnum, MB_NDIRREAD, 0);
	while (nbytes)
	{
		if (!fcbptr->blkstat)
		{
			/* Can we do a multi-block read? */
			ntomove = fcbptr->file_size - fcbptr->file_pos;
			if (ntomove > nbytes)
				ntomove = nbytes;
			if ((ntomove >= BLOCKSIZE) && !(fcbptr->file_pos & BSIZEM1))
			{
				uint nblks;

				/* V4.7 addition */
				if (fcbptr->changed)
				{
					retval = flush(fcbptr);
					if (retval < 0)
						goto err;
				}

				nblks = ntomove / BLOCKSIZE;
				fcbptr->bmindex = fcbptr->file_pos / BLOCKSIZE;

				retval = MULTIBLKIO(fcbptr->bdvrnum, MIO_RREAD, buf,
					fcbptr->blkmap + fcbptr->bmindex, nblks);
				fcbptr->bmindex += nblks;
				nblks *= BLOCKSIZE;
				fcbptr->file_pos += nblks;
				buf += nblks;
				nbytes -= nblks;
				nread += nblks;
				/*
				 * Do this test here so that bad blocks are not
				 * infinitely read
				 */
				if (retval < 0)
					goto err;
			}
			/* Must re-fill buffer with remainder of read */
			if (nbytes)
			{
				if ((retval = refill(fcbptr)))
					goto err;
			}
			else
				goto readit;	/* All we want */
		}
/* Calculate minimum of nbytes, number in buffer, remainder in file */
		ntomove = BLOCKSIZE - bbufindex;
		if (nbytes < ntomove)
			ntomove = nbytes;
		if (ntomove > (fcbptr->file_size - fcbptr->file_pos))
			ntomove = fcbptr->file_size - fcbptr->file_pos;

		movmem(&fcbptr->blkbuf[bbufindex], buf, ntomove);
		nbytes -= ntomove;
		bbufindex += ntomove;
		fcbptr->file_pos += ntomove;
		nread += ntomove;
		buf += ntomove;
		if (bbufindex == BLOCKSIZE)
		{
			bbufindex = 0;
			fcbptr->blkstat = 0;
		}
		if (fcbptr->file_pos >= fcbptr->file_size)
			break;
	}
readit:
	retval = nread;
err:
	fsexit(0);
	return retval;
}

/*
 * Another user has inherited a file
 */

int incuser(int fd, int newpid,char *name)
{
	register struct fcb *fcbptr;

	if (fd < NIODRIVERS)
		return 0;	/* Char device */
	fcbptr = ppfcbs[fd - NIODRIVERS];
	if (fcbptr == 0)
	{
		//eprintf("Panic in incuser(%s): fd = %d\r\n", name, fd);
		return -1;
	}
	//fcbptr->owners[newpid++];
	fcbptr->nusers++;
	return 0;
}

/*
 * Someone has exitted
 */

int tryclose(int fd)
{
	struct fcb *fcbptr;

	if (fd < NIODRIVERS)
		return CLOSE(fd);	/* Char device */
	fcbptr = ppfcbs[fd - NIODRIVERS];
	if (fcbptr == 0)
	{
		//eprintf("Panic in tryclose0x%x: fd = %d\r\n", *(&fd - 1), fd);
		return 0;
	}
	return doclose(fcbptr->file_name, fd);
}

/* Clean up a processes files */
void closeprocfiles(int pid)
{
	register int fcbnum;
	register struct fcb *fcbptr;

	for (fcbnum = 0; fcbnum < maxfiles; fcbnum++)
	{
		if ((fcbptr = ppfcbs[fcbnum]) == 0)
			continue;
		if (fcbptr->owners[pid])
		{	/* This process opened or inherited the file */
			doclose(fcbptr->file_name, fcbnum + NIODRIVERS);
		}
	}
}

/*
 * System call 107: close file which is open for reading/writing. Return 0 if
 * ok, else code.
 * If bit 31 of fd set, flush file buffers, leave open (sync)
 */

int _close(uint fd)
{
	register struct fcb *fcbptr;
	register struct dir_entry *dirptr;
	register uint fcbnum;
	register uint bdvrnum;
	register int retval = 0;
	int ec = 0;
	char *dirb = 0;
	uint flushing;

	if (fd == (uint)-1)
	{	/* sync fs */
		for (fcbnum = 0; fcbnum < maxfiles; fcbnum++)
		{
			if (ppfcbs[fcbnum])
				CLOSE((fcbnum + NIODRIVERS) | 0x80000000);
			return 0;
		}
	}

	flushing = fd & 0x80000000;
	fd &= 0x7fffffff;

	if ((fd > (NIODRIVERS + maxfiles)) && !flushing)
		return 0;	/* Suppress close of stdin, stdout, etc */

	if (fd >= 0x80)
		fixfd(&fd);		/* Convert STDIN, STDOUT, STDERR */

	if (fd < NIODRIVERS)
		return closechdvr(fd);	/* Close on char device OK */

	fcbnum = fd - NIODRIVERS;
	fsentry(0);
	if (fcbnum >= maxfiles)
	{
		retval = BEC_BADFD;
		goto err;
	}
	fcbptr = ppfcbs[fcbnum];
	if (!fcbptr)
	{
		retval = BEC_FNOPEN;
		goto err;
	}

	if ((retval = dogetmem(BLOCKSIZE, 0, "close")) < 0)
		goto err;
	dirb = (char *)retval;

	/* Only owner can close it, anyone can flush */
	if (!flushing && (fcbptr->owners[curpid] == 0))
	{	/* No longer own the file: quietly exit */
		retval = 0;
		goto err;
	}

	if (!flushing)
	{	/* A real close */
		//fcbptr->owners[curpid--];
		if (fcbptr->owners[curpid] < 0)
			fcbptr->owners[curpid] = 0;
		if (--(fcbptr->nusers) != 0)
		{
			retval = 0;
			goto err;
		}
	}

	if (fcbptr->pipefcb)
	{
		if (flushing)
		{
			retval = 0;	/* Flushing a pipe? */
			goto err;
		}
		/* Not sure why I did it in this order... */
		retval = pipeclose(fcbnum);
		goto err;
	}

	bdvrnum = fcbptr->bdvrnum;
	if (fcbptr->openmode != SSO_RDONLY && fcbptr->needsflush)
	{	/* Writing */
		fcbptr->needsflush = 0;
		ec = flush(fcbptr);	/* Write out rest of buffer */
		BDMISC(bdvrnum, MB_BMBREAD, 0);
/* Write out the block map block first (a bit risky but more efficient) */
		if (fcbptr->file_size)
		{
			if ((retval = MULTIBLKIO(bdvrnum, MIO_SWRITE, fcbptr->blkmap,
				fcbptr->blkmapblk, bmbin(fcbptr->file_size))) < 0)
				goto errcls;
		}

/* Update the copy of the bitmap block BEFORE the directory entry */
		if ((retval = wrbitmap(bdvrnum)))
			goto errcls;
/* Read the directory block */
		BDMISC(bdvrnum, MB_DIRREAD, 0);
		if ((retval = BLKREAD(fcbptr->dirblk, dirb, bdvrnum)))
			goto errcls;
		dirptr = (struct dir_entry *)
			(dirb + fcbptr->dirindex * DIRENTSIZE);
		dirptr->file_size = fcbptr->file_size;
/*
 * Was:		dirptr->statbits |= (umask & ~DIRECTORY) | FS_HASFFB;
 *			dirptr->uid = ouruid;
 * 4.2: Preserve mode and UID.
 *
 * NO! set uid to current to prevent uid 0 files from being modified.
 */
/* 4.2b: bugger it, uid 0 shouldn't have writable executable files */
/* 4.2c: reset it if not UID 0: prevents people altering executables */
/* This duplicates the setting up of fcbptr->uid, but whatever */
/*		if (ouruid)
			dirptr->uid = ouruid;
		else
*/
		dirptr->uid = fcbptr->uid;
		dirptr->statbits |= FS_HASFFB;
		dirptr->statbits &= ~BACKEDUP;
		dirptr->magic = V4_2MAGIC;
		dirptr->blkmapblk = fcbptr->blkmapblk;
		/* Set up first 4 blocks */
		movmem(fcbptr->blkmap, dirptr->ffblocks,
					sizeof(dirptr->ffblocks));
		GETDATE(dirptr->date);	/* Date the file */
/* Update directory cache entry */
		cacheadd(fcbptr->file_name, bdvrnum, dirptr,
			(fcbptr->dirindex << 16) | fcbptr->dirblk);
/* Write out the directory block */
		BDMISC(bdvrnum, MB_DIRREAD, 0);
		if ((retval = BLKWRITE(fcbptr->dirblk, dirb, bdvrnum)))
			goto errcls;
	}
	retval = ec;		/* Possible error during flush() */

/* Problem closing: free the FCB anyway */
errcls:
	if (!flushing)
		free_fcb(fcbnum, 0);
err:
	fsexit(0);
	if (dirb)
		dofreemem(dirb);
	return retval;
}

/*
 * System call 108: create a file for output. First unlink it. Return 0 if OK.
 * Return fd of open file or char device.
 */

int _creat(char *pathname,int statbits,int load_addr)
{
	register char *fullpath;
	register int fcbnum, bdvrnum;
	register struct fcb *fcbptr;
	register int retval;
	char fname[FNAMESIZE];
	struct dir_entry dirent;

	if (checkfname((uchar*) pathname))
		return BEC_FNBAD;

	fsentry(0);
	fullpath = (char *)GETFULLPATH(pathname, 0);

	if (ischdev(fullpath))
	{
		retval = openchdvr(fullpath, SSO_RDWR);
		goto err;
	}

	statbits = ((statbits | umask) & ~DIRECTORY) | FS_HASFFB;

/* Get the file's current status */
	retval = FILESTAT(fullpath, &dirent);
	if (retval >= 0)
	{	/* File exists */
		if (dirent.statbits & DIRECTORY)
		{	/* Can't create a directory! */
			retval = BEC_ISDIR;
			goto err;
		}
		if (CHKPERM(&dirent, FS_NOWRITE, fullpath))
		{	/* No write permission */
			retval = BEC_NOPERM;
			goto err;
		}
		statbits = dirent.statbits;
	}

/* If the file is open for reading or writing, return error */
	switch (retval)
	{
	case 0:		/* File exists */
		/* Pass the OPEN() system call another argument */
		retval = trap7(105, fullpath, SSO_TRUNCATE, load_addr);
		goto err;
	case SSO_RDONLY:		/* Open for reading */
		retval = BEC_FROPEN;
		goto err;
	case SSO_RDWR:
		retval = BEC_FWOPEN;
		goto err;
	case BEC_NOFILE:
		break;
	default:	/* Some other error */
			goto err;
	}

/* Get block device number and a file control block */
	if ((retval = bdvrnum = get_bdev(fullpath, fname)) < 0)
		goto err;	/* No such device */
	if ((retval = fcbnum = get_fcb(fullpath, 0)) < 0)
		goto err;	/* No spare fcbs */
	fcbptr = ppfcbs[fcbnum];
	fcbptr->bdvrnum = bdvrnum;

	if (!(retval = find_file(fullpath, fcbnum, 0L, statbits,(char *) load_addr)))
	{
		fcbptr->openmode = SSO_RDWR;
		retval = (int)(fcbnum + NIODRIVERS);	/* Return file descriptor */
		goto err;
	}
	free_fcb(fcbnum, 0);
err:
	fsexit(0);
	dofreemem(fullpath);
	return retval;
}

/*
 * System call 109: write to file. Return 0 if OK, else code.
 */

int _write(uint fd,char *buf,int nbytes)
{
	register struct fcb *fcbptr;
	register uint fcbnum;
	register int bbufindex, ntomove;
	register uint bdvrnum;
	register int retval = 0;

	/* V4.7 */
	if (nbytes == 0)
		return 0;

	if (fd >= 0x80)
		fixfd(&fd);		/* Convert STDIN, STDOUT, STDERR */

	if (fd < NIODRIVERS)
	{	/* Character device */
		return chwrite(fd,(uchar *) buf, nbytes);
	}

	fsentry(0);

	fcbnum = fd - NIODRIVERS;
	if (fcbnum >= maxfiles)
	{
		retval = BEC_BADFD;
		goto err;
	}
	fcbptr = ppfcbs[fcbnum];
	if (!fcbptr)
	{
		retval = BEC_FNOPEN;
		goto err;
	}

	if (fcbptr->pipefcb)
		return pipewrite(fcbnum,(uchar *) buf, nbytes);

	if (fcbptr->openmode != SSO_RDWR)
	{
		retval = BEC_FNWOPEN;
		goto err;
	}

	fcbptr->needsflush = 1;
	bdvrnum = fcbptr->bdvrnum;
	bbufindex = fcbptr->file_pos & BSIZEM1;

	BDMISC(bdvrnum, MB_NDIRREAD, 0);
	while (nbytes)
	{
		if (!fcbptr->blkstat)
		{	/* The block is not in memory */
			if ((nbytes >= BLOCKSIZE) && !(fcbptr->file_pos & BSIZEM1))
			{	/* Can use multi-block I/O */
				uint nblks;	/* Number of blocks to write */
				uint ninfile;	/* No. of blocks in file now */
				int blk;
				uint bmindex;

				/* May need to flush old contents */
				if (fcbptr->changed)
				{
					retval = flush(fcbptr);
					if (retval < 0)
						goto err;
				}
				bmindex = fcbptr->file_pos / BLOCKSIZE;
				fcbptr->bmindex = bmindex;
				nblks = nbytes / BLOCKSIZE;
				ninfile = (fcbptr->file_size + BSIZEM1) /
						BLOCKSIZE;
				/* Allocate all the blocks */
				for (blk = 0; blk < nblks; blk++)
				{
					int newind;

					newind = bmindex + blk;
					if (newind < ninfile)
						continue;
					if (((newind & BSIZEO2M1) == 0) && newind)
					{	/* Grow the blockmap */
						if ((retval = growblockmap(fcbptr, newind)) < 0)
						{
							goto bomb;
						}
					}
					if ((retval = get_free(bdvrnum)) < 0)
					{ /* Disk full: re-free the blocks */
bomb:					  while (--blk >= 0)
					    freeblk(bdrivers + bdvrnum,
					        fcbptr->blkmap[bmindex + blk]);
						goto err;
					}
					/* Save the block */
					fcbptr->blkmap[newind] = retval;
					BDMISC(bdvrnum, MB_NDIRREAD, 0);
				}
				/* Write out all those blocks */
				retval = MULTIBLKIO(bdvrnum, MIO_RWRITE,
					buf, fcbptr->blkmap + bmindex, nblks);
				if (retval < 0)
					goto err;
				fcbptr->bmindex += nblks;
				nblks *= BLOCKSIZE;
				buf += nblks;
				nbytes -= nblks;
				fcbptr->file_pos += nblks;
				if (fcbptr->file_pos > fcbptr->file_size)
					fcbptr->file_size = fcbptr->file_pos;
				if (nbytes == 0)
					goto allwritten;
			}
			if ((retval = refill(fcbptr)))
				goto err;
			if ((fcbptr->file_pos == fcbptr->file_size) && !bbufindex)
			{	/* Must allocate a new block */
				if (fcbptr->bmindex && !(fcbptr->bmindex & BSIZEO2M1))
				{	/* Must grow blockmap */
						if ((retval = growblockmap(fcbptr,fcbptr->bmindex))<0)
						{
							CLOSE(fcbnum + NIODRIVERS);
							goto err;
						}
				}
				if ((retval = get_free(bdvrnum)) < 0)
				{
					CLOSE(fcbnum + NIODRIVERS);
					goto err;	/* Disk full */
				}
				fcbptr->blkmap[fcbptr->bmindex] = retval;
				fcbptr->blkstat = 1;	/* The block is OK */
				retval = 0;
			}
		}
		ntomove = BLOCKSIZE - bbufindex;
		if (ntomove > nbytes)
			ntomove = nbytes;	/* Number of bytes to move */

		movmem(buf, &fcbptr->blkbuf[bbufindex], ntomove);
		bbufindex += ntomove;
		buf += ntomove;
		nbytes -= ntomove;
		if (fcbptr->file_size < (fcbptr->file_pos += ntomove))
			fcbptr->file_size = fcbptr->file_pos;
		fcbptr->changed = 1;
		if (bbufindex == BLOCKSIZE)
		{	/* Buffer full */
			fcbptr->blkstat = 0;	/* Block now wrong */
			bbufindex = 0;
		}
	}
allwritten:
err:
	fsexit(0);
	return retval;
}

/* System call 110: delete a file */

int _unlink(char *pathname)
{
	int retval;
	char *fullpath;

	if (!*pathname)
		return BEC_FNBAD;
	if ((retval = chkfpc(pathname)))
		return retval;
	fsentry(0);
	fullpath = (char *)GETFULLPATH(pathname, 0);
	if (!chknincp(fullpath))
		/* Check we aren't deleting current directory! */
		retval = BEC_FNBAD;
	else
		retval = PROCESSDIR(fullpath, "", 0);
	dofreemem(fullpath);
	fsexit(0);
	return (retval < 0) ? retval : 0;
}

/* Check that the path is not under the current directory */
int chknincp(char *fullpath)
{
	return scp(fullpath, cur_path);
}

/* System call 111: rename a file */

int _rename(char *pathname,char *newpathname)
{
	int retval;
	fsentry(0);
	retval = dorename(pathname, newpathname, 0);
	fsexit(0);
	return retval;
}

int dorename(register char *pathname,register char *newpathname,int canmove)
{
  /* If true, can shift files to other directories */
	register int retval;
	struct dir_entry dirent;
	register char *pn, *newpn;
	char *p;
	char fname[FNAMESIZE];
	char newfname[FNAMESIZE];

	if (!*pathname || !*newpathname)
		return BEC_FNBAD;
	if ((long)(pn = (char *)GETFULLPATH(pathname, 0)) < 0)
		return (int) pn;

	if (!canmove)
	{	/* Build path of new pathname */
		newpn = (char *)getmem0(strlen(pn) + strlen(newpathname) + 2);
		if (index(newpathname, '/'))
		{	/* Cope with 'rename a/b a/c ' */
			char v, *temp, *npn;

			npn = (char *)GETFULLPATH(newpathname, 0);
			temp = (char *)room0for(pn);
			splitpath(temp);
			splitpath(npn);
			v = strucmp(temp, npn);
			dofreemem(temp);
			dofreemem(npn);
			if (v)
			{	/* Paths differ */
				retval = BEC_FNBAD;
				goto err2;
			}
			strcpy(newpn, newpathname);
		}
		else
		{	/* Simple name only */
			strcpy(newpn, pn);
			splitpath(newpn);
			strcat(newpn, &slash);
			strcat(newpn, newpathname);
		}
	}
	else
		newpn = (char *)GETFULLPATH(newpathname, 0);

	/* Make sure they are full pathnames */
	p = (char *)GETFULLPATH(pn, 0);
	dofreemem(pn);
	pn = p;
	p = (char *)GETFULLPATH(newpn, 0);
	dofreemem(newpn);
	newpn = p;

	if (!chknincp(pn))
	{	/* Check we aren't renaming current directory! */
		retval = BEC_FNBAD;
		goto err;
	}

	if (get_bdev(pn, fname) != get_bdev(newpn, newfname))
	{
		retval = BEC_FNBAD;
		goto err;
	}
	if ((retval = chkfpc(pn)))
		goto err;

	if (checkfname((uchar *)newpn))
	{
		retval = BEC_FNBAD;
		goto err;
	}
	retval = FILESTAT(newpn, &dirent);
	if (((uint)retval) <= SSO_RDWR)
	{
		retval = BEC_DUPFNAME;
		goto err;
	}

	if (retval != BEC_NOFILE)
		goto err;		/* Some other error */
	if ((retval = PROCESSDIR(pn, &dirent, 2)) < 0)
		goto err;		/* Read orig. pathname */
	if (dirent.statbits & LOCKED)
	{
		retval = BEC_LOCKED;
		goto err;
	}

	if ((retval = CHKPERM(&dirent, FS_NOWRITE, pn)) < 0)
		goto err;

	movstr(dirent.file_name, newfname, FNAMESIZE);	/* Rename it */
	if ((retval = PROCESSDIR(newpn, &dirent, 5)) < 0) /* Write in new dir */
		goto err;
	dirent.file_name[FNAMESIZE - 1] = *dirent.file_name;
	*dirent.file_name = '\0';
	retval = PROCESSDIR(pn, &dirent, 3);		/* Zap old one */
	if (retval < 0)
	{	/* Something wrong: zap renamed copy */
		PROCESSDIR(newpn, &dirent, 3);
	}
err:
	dofreemem(pn);
	dofreemem(newpn);
err2:	return (retval < 0) ? retval : 0;
}

/*
 * System call 112: get file status.
 *
 * Return:
 *	0 for file there, closed.
 *	1 for file open for read.
 *	2 for file open for write.
 *	3 for file open for both.
 *	BEC_NOFILE if not present
 *	Other error code if detected
 *
 * Attempt to read the file directory entry to *buf.
 *
 * If pathname is in the range 16-maxfiles, return pointer to filename
 *    or zero if not open now.
 * If pathname == -1, return pointer to file control block table
 * If pathname == -2, return maxfiles
 * the fcb is free
 */

int _filestat(char *pathname,char *buf)
{
	register int fcbnum;
	register struct fcb *fcbptr;
	register int fstat = 0;
	register int retval;
	register uint pnasfd;			/* pathname as a fd */
	uint _pnasfd;
	char *pnptr;

	pnasfd = (uint)pathname;

	if (pnasfd == (uint)-1)
		return (int)ppfcbs;
	else if (pnasfd == (uint)-2)
		return maxfiles;

	if (pnasfd >= 0x80)
	{
		_pnasfd = pnasfd;
		pnasfd = fixfd(&_pnasfd);
	}

	if (pnasfd < (NIODRIVERS + maxfiles))
	{
		if (pnasfd < NIODRIVERS)
		{	/* Return pointer to device name */
			return FIND_DRIVER(0, pnasfd);
		}
		fcbptr = ppfcbs[pnasfd - NIODRIVERS];
		return (fcbptr) ? (int)fcbptr->file_name : 0;
	}

	if (!*pathname)
		return BEC_FNBAD;

	/* Full paths allow us to use strcmp, not pathcmp! */
	pnptr = (char *)GETFULLPATH(pathname, 0);

/* Scan the fcb's */
	fsentry(0);
	for (fcbnum = 0; fcbnum < maxfiles; fcbnum++)
	{
		fcbptr = ppfcbs[fcbnum];
		if (fcbptr && (!strucmp(fcbptr->file_name, pnptr)))
		{
			fstat = fcbptr->openmode;
			goto gotit;
		}
	}
gotit:
	/* 4.2: Sync file system if this file is open for write */
	if (fstat == SSO_RDWR || fstat == SSO_WRONLY)
		CLOSE((uint)(fcbnum + NIODRIVERS)|0x80000000);

/*
 * If file not there, fstat = 0. Try to read directory entry, return result.
 * If the file is there, read its entry and if no error, return fstat
 */
	retval = PROCESSDIR(pnptr, buf, 2);
	fsexit(0);
	dofreemem(pnptr);
	return (retval < 0) ? retval : fstat;
}

/*
 * System call 113: scan for a directory entry. Passed initial directory index, returns
 * next directory index, -1 if at end. Must be passed parent directory entry.
 */

int _readdir(int bdvrnum,char *buf,struct dir_entry *dirent,int pos,struct dir_entry *pardirent)
{
	register struct dir_entry *dptr;
	register int retval;

/*
	if (retval = CHKPERM(pardirent, FS_NOREAD, bdvrnum))
		return retval;
*/
	fsentry(0);
	while (pos < (pardirent->file_size * (DIRPERBLOCK)))
	{
		if (!(pos & ((DIRPERBLOCK)-1)))
		{	/* Need to read new directory block */
			BDMISC(bdvrnum, MB_DIRREAD, 0);
			if ((retval = BLKREAD(pardirent->blkmapblk +
				pos / (DIRPERBLOCK), buf, bdvrnum)))
				goto out;
		}
		dptr = (struct dir_entry *)(buf + (pos & (DIRPERBLOCK-1))
						* DIRENTSIZE);
		if (*(dptr->file_name))
		{	/* Found an entry */
			movde(dptr, dirent);
			retval = pos + 1;
			goto out;
		}
		pos++;
	}
	retval = -1;
out:
	fsexit(0);
	return retval;
}

/*
 * System call 115: seek to position.
 * If mode = 0, absolute seek.
 * If mode = 1, relative seek.
 * If mode = 2, seek to EOF
 * Return new position, or error code
 */

int _seek(uint fd,int offset,uint mode)
{
	register struct fcb *fcbptr;
	register uint fcbnum;
	register int retval = 0;

	if (fd >= 0x80)
		fixfd(&fd);		/* Convert STDIN, STDOUT, STDERR */

	if (fd < NIODRIVERS)
		return 0;	/* Seek OK on char device */
	fcbnum = fd - NIODRIVERS;

	if (fcbnum >= maxfiles)
	{
		retval = BEC_BADFD;
		goto err2;
	}
	fcbptr = ppfcbs[fcbnum];
	if (!fcbptr)
		return BEC_FNOPEN;

	if (fcbptr->pipefcb)
		return BEC_BADFD;

	fsentry(0);

/* Now convert 'offset' to an absolute position */
	switch (mode)
	{
	case 0:
		break;		/* Seek position is absolute */
	case 1:
		offset += fcbptr->file_pos;
		break;
	case 2:
		offset = fcbptr->file_size;
		break;
	default:
		retval = BEC_BADARG;
		goto err;
	}

	if (((uint)offset) > fcbptr->file_size)
	{
		retval = BEC_SPASTEOF;
		goto err;
	}

/* Is a new block needed? */
	if ((offset & ~BSIZEM1) != (fcbptr->file_pos & ~BSIZEM1))
	{	/* Block change */
		fcbptr->blkstat = 0;
	}
	fcbptr->file_pos = offset;

err:	fsexit(0);
err2:	return retval ? retval : offset;
}

/*
 * System call 116: Return current file pointer
 */

int _tell(uint fd)
{
	register struct fcb *fcbptr;
	register uint fcbnum;

	if (fd >= 0x80)
		fixfd(&fd);		/* Convert STDIN, STDOUT, STDERR */

	if (fd < NIODRIVERS)
		return 0;	/* Tell OK on char device. Return start of file */
	fcbnum = fd - NIODRIVERS;
	if (fcbnum >= maxfiles)
		return BEC_BADFD;
	fcbptr = ppfcbs[fcbnum];
	if (!fcbptr)
		return BEC_FNOPEN;

	if (fcbptr->pipefcb)
		return BEC_BADFD;

	return fcbptr->file_pos;
}

/* Fix a STDIN, STDOUT or STDERR file descriptor to the real one */
int fixfd(uint *pfd)
{
	register uint fd;

	fd = *pfd;
/* Version 2.4 compatibility: gone now */
#ifdef notdef
	if ((fd >= (NIODRIVERS + NFCBS)) && (fd < (NIODRIVERS + NFCBS + NFCBS)))
		fd -= NFCBS;
#endif
	switch (fd)
	{
	case 0x80:		/* Kludge for Hitech C */
	case STDERR:	fd = stderr;
			break;
	case 0x81:
	case STDOUT:	fd = stdout;
			break;
	case 0x82:
	case STDIN:	fd = stdin;
			break;
	}
	return (*pfd = fd);
}

/*
 * Grow a file's blockmap during writing. We are passed the fcb pointer
 * and the desired blockmap index.
 *
 */

int growblockmap(struct fcb *fcbptr,uint bmindex)
{
	register ushort *blkmap;
	register int retval;
	register int totblocks, i;
	int bdvrnum, blkmapblk;

	totblocks = bmindex / BSIZEO2 + 1;
	bdvrnum = fcbptr->bdvrnum;
	blkmap = (ushort *)getmem1(totblocks * BLOCKSIZE);
	if ((int)blkmap < 0)
		return (int)blkmap;

	/* Copy the block list */
	movmem(fcbptr->blkmap, blkmap, bmindex * sizeof(*blkmap));
	dofreemem(fcbptr->blkmap);
	fcbptr->blkmap = blkmap;

	/* Now get disk storage for the table */
	if ((retval = getdirblks(bdvrnum, totblocks)) < 0)
		return retval;

	blkmapblk = retval;

	/* Now free old storage */
	for (i = 0; i < totblocks - 1; i++)
		freeblk(&bdrivers[bdvrnum], fcbptr->blkmapblk + i);

	fcbptr->blkmapblk = blkmapblk;
	if ((retval = wrbitmap(bdvrnum)) < 0)
		return retval;

	return 0;
}

/*
 * Number of blockmap blocks based on file size
 */

int bmbin(int size)
{
	return (size - 1) / (BLOCKSIZE * BSIZEO2) + 1;
}

/*
 * V4.7 addition: alter the directory position in any open
 * files.  This is done when directories are moved due to
 * directory growth.
 */

fixOpenFiles(bdvrnum, olddirb, oldnblks, newdirb)
ushort bdvrnum;
{
	register int fcbnum;
	register struct fcb *fcbptr;

	for (fcbnum = 0; fcbnum < maxfiles; fcbnum++)
	{
		if ((fcbptr = ppfcbs[fcbnum]) == 0)
			continue;
		if (fcbptr->bdvrnum == bdvrnum &&
			fcbptr->dirblk >= olddirb &&
			fcbptr->dirblk < (olddirb + oldnblks))
			fcbptr->dirblk += newdirb - olddirb;
	}
}
