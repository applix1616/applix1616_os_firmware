/* TABS4 NONDOC
 * Directory entry cache control
 */

#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <files.h>

#include <blkerrcodes.h>
//#include <splfns.h>
#include "blockio.h"
#include "cachecont.h"


#define FPSIZE	80			/* Max length of a full pathname */

struct cache
{
  char fullpath[FPSIZE];                /* Name */
  ushort bdvrnum;			/* Block driver for directory entry */
  uint dirindex;			/* Directory index */
  uint age;				/* Ageing for LRU */
  struct dir_entry dirent;
};

struct cache *dcache;
int ndcentries;
extern char *splitpath(), *room0for();
extern struct blkdriver bdrivers[];
extern int diskverbose;

static int slashsane(register char *cp)
{
	if (!*cp)
		return BEC_FNBAD;
	while (*cp)
	{
		if (*cp == '/')
		{
			cp++;
			if ((*cp == '/') || (*cp == 0))
				return BEC_FNBAD;
		}
		else
			cp++;
	}
	return 0;
}

/*
 * Passed a full pathname, scan its parent directory for its directory
 * entry, place it in the desired location
 */

static int scande(struct dir_entry *pdirent,char *fullpath,uint *pbdvrnum,uint *pdirpos)
{
	register char *hfullpath, *fname;
	register struct dir_entry *dirbuf;
	register int bdvrnum;
	register int retval;
	struct dir_entry pardir;
	uint pardirpos;
	char fnbuf[FNAMESIZE];

	if (isdevid(fullpath))
	{	/* Is /rd, etc */
		retval = FIND_BDVR(fullpath);
		if (retval < 0)
			return retval;
		movde(&bdrivers[retval].rootblock.rootdir, pdirent);
		*pbdvrnum = retval;
		*pdirpos = 0;
		return 0;
	}
	/* Hack off the filename spec and get the parent directory entry */
	hfullpath = room0for(fullpath);
	fname = splitpath(hfullpath);
	retval = findde(hfullpath, pbdvrnum, &pardirpos, &pardir);

	if ((retval >= 0) && !(pardir.statbits & DIRECTORY))
		retval = BEC_NOTDIR;

	if (retval < 0)
	{
		dofreemem(hfullpath);
		return retval;
	}

	/* Now scan the directory for our filename */
	bdvrnum = get_bdev(fullpath, fnbuf);
	if (bdvrnum < 0)
	{
		dofreemem(hfullpath);
		return bdvrnum;
	}
	dirbuf = (struct dir_entry *)getmem0(BLOCKSIZE);
	retval = 0;
	BDMISC(bdvrnum, MB_DIRREAD, 0);
	while (retval >= 0)
	{
		retval = READDIR(bdvrnum, dirbuf, pdirent, retval, &pardir);
		if (retval == -1)	/* End of the directory */
			retval = BEC_NOFILE;
		if ((retval >= 0) && !strucmp(pdirent->file_name, fname))
		{	/* Found it */
			*pbdvrnum = bdvrnum;
			/* readdir returns 1 + directory pos */
			retval--;
			*pdirpos = (retval / DIRPERBLOCK + pardir.blkmapblk) |
					((retval & (DIRPERBLOCK-1)) << 16);
			retval = 0;
			break;
		}
	}
	dofreemem(hfullpath);
	dofreemem(dirbuf);
	return retval;
}

/*
 * Find an entry in the cache. Return 0 if found, or -1
 * MUST do a newdisk() before we call this function to check for floppy swap.
 */

static int cachefind(struct dir_entry *pdirent,register char *fullpath,uint *bdptr,uint *pdirindex)
{
	register struct cache *cacheptr;
	register uint cacheindex;
	register int retval;

	cacheptr = dcache;
	retval = -1;

	for (cacheindex = 0; cacheindex < ndcentries; cacheindex++, cacheptr++)
	{
		if (!strucmp(fullpath, cacheptr->fullpath))
		{
			movde(&cacheptr->dirent, pdirent);
			cacheptr->age = 0;
			*bdptr = cacheptr->bdvrnum;
			*pdirindex = cacheptr->dirindex;
			retval = 0;
		}
		else
			cacheptr->age++;
	}
	return retval;
}

void cache_init(void)
{
	uint size;

	size = ndcentries * sizeof(struct cache);
	dcache = (struct cache *)getzmem1(size);
}

/*
 * Passed a full pathname, find its directory entry, place it in
 * cache.
 */
		/* Where caller wants it placed */
int findde(char *fullpath,uint *pbdvrnum,uint *pdirpos,struct dir_entry *pdirent)
{
	register int retval, bdvrnum;

	if ((retval = slashsane(fullpath)) < 0)
		goto out;
	if ((bdvrnum = retval = get_bdev(fullpath, 0)) < 0)
		goto out;
	/* newdisk() will zap cache entries if floppy swapped */
	if ((retval = newdisk(retval)) < 0)
		goto out;
	if (isdevid(fullpath))
	{	/* Is /rd, etc */
		*pbdvrnum = bdvrnum;
		*pdirpos = 0;
		movde(&bdrivers[bdvrnum].rootblock.rootdir, pdirent);
		retval = 0;
		goto out;
	}
	retval = cachefind(pdirent, fullpath, pbdvrnum, pdirpos);
	if (retval != 0)
	{
		retval = scande(pdirent, fullpath, pbdvrnum, pdirpos);
		if (retval == 0)
			cacheadd(fullpath, *pbdvrnum, pdirent, *pdirpos);
	}
out:
	return retval;
}

/*
 * Add a new entry to the cache
 * If no empty entries, trash oldest one
 */

int cacheadd(char *fullpath,ushort bdvrnum,struct dir_entry *dirptr,uint dirindex)
{
	register struct cache *cacheptr;
	register uint cacheindex;
	register uint age;
	register uint oldest;
	int isin;

	if (strlen(fullpath) >= (FPSIZE - 1))
		return 0;	/* Won't fit! */

	oldest = 0x8000;
	cacheptr = dcache;
	isin = age = 0;

	for (cacheindex = 0; cacheindex < ndcentries; cacheindex++, cacheptr++)
	{
		if ((!isin) &&
			(!*cacheptr->fullpath ||
				!strucmp(cacheptr->fullpath, fullpath)))

		{	/* Replacement */
			if (!*cacheptr->fullpath)
				strcpy(cacheptr->fullpath, fullpath);
			cacheptr->bdvrnum = bdvrnum;
			cacheptr->dirindex = dirindex;
			cacheptr->age = 0;
			movde(dirptr, &cacheptr->dirent);
			isin = 1;
		}
		else
		{	/* Used, not this file */
			if (!strucmp(fullpath, cacheptr->fullpath))
			/* Delete old entry */
				*cacheptr->fullpath = 0;
			else
			{
				cacheptr->age++;
				if (cacheptr->age > age)
				{
					oldest = cacheindex;
					age = cacheptr->age;
				}
			}
		}
	}
	if (!isin)
	{	/* Replace the oldest entry */
#ifdef notdef
		if (oldest == 0x8000)
			return eprintf("Panic in cacheadd");
#endif
		cacheptr = dcache + oldest;
		strcpy(cacheptr->fullpath, fullpath);
		cacheptr->age = 0;
		cacheptr->bdvrnum = bdvrnum;
		cacheptr->dirindex = dirindex;
		movde(dirptr, &cacheptr->dirent);
	}
	return 0;
}

/*
 * Delete an entry from the cache due to it being altered.
 * If 'fullpath' is a block driver number, delete all entries for that
 * device (disk change)
 */
void cachedel(register char *fullpath)
{
	register uint cacheindex;
	register struct cache *cacheptr;
	register int isbdvr;
	register ushort bdvrnum;

	cacheptr = dcache;
	isbdvr = 0;

	if ((uint)*fullpath < NBLKDRIVERS)
	{	/* It is a block driver */
		isbdvr = 1;
		bdvrnum = (ushort)*fullpath;
	}

	for (cacheindex = 0; cacheindex < ndcentries; cacheindex++, cacheptr++)
	{
		if ((isbdvr && cacheptr->bdvrnum == bdvrnum) ||
			 (!isbdvr && !strucmp(fullpath, cacheptr->fullpath)))
		{
			*cacheptr->fullpath = 0;
		}
	}
}
