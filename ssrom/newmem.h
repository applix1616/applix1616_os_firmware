/*
 * All the memory allocation stuff.
 */
#ifndef NEWMEM_H
#define NEWMEM_H

extern void mem_init(int stackspace);
extern int _newpid(void);
extern void intfail(int ec,int *ppc, int arg1, int arg2);
extern long getmem0(int n);
extern long getzmem0(int n);
extern long getmem1(int n);
extern long getzmem1(int n);
extern int clearpidmem(int pid);
extern int dofreemem(void *addr);
extern int maybefree(void *addr);
extern int dogetmem(int nbytes,int mode,char *who);
extern int calcprocmem(void);

#endif
