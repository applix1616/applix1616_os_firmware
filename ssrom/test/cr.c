#include <1616/syscalls.h>

main()
{
	int ofd;

	PRINTF("CLOSE(-9)\r\n");
	CLOSE(-9);
	PRINTF("Creating\r\n");
	ofd = CREAT("", 0, 0);
	PRINTF("Closing %d\r\n", ofd);
	CLOSE(ofd);
	PRINTF("Unlinking\r\n");
	UNLINK("");
}
