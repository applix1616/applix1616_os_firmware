/* TABS8 */


#include <syscalls.h>

usage()
{
	FPRINTF(STDERR, "Usage: opener nopens openmode pathnamestart\r\n");
	EXIT(-1);
}

main(argc, argv)
char *argv;
{
	int n, i, mode;
	int starttime;
	int fds256;
	char buf100;

	if (argc != 4)
		usage();

	n = atoi(argv1);
	mode = atoi(argv2);

	starttime = GET_TICKS();
	for (i = 0; i < n; i++)
	{
		SPRINTF(buf, "%s%d", argv3, i);
		fdsi = OPEN(buf, mode);
		if (fdsi < 0)
		{
			FPRINTF(STDERR, "Error opening %s: %s\r\n",
					buf, ERRMES(fdsi));
			EXIT(-1);
		}
	}
	showtime(starttime, "Time to open %d files", n);

	starttime = GET_TICKS();
	for (i = 0; i < n; i++)
	{
		CLOSE(fdsi);
	}
	showtime(starttime, "Time to close %d files", n);

}

showtime(st, msg, p1, p2, p3)
{
	int elapse;

	elapse = GET_TICKS() - st;

	PRINTF(msg, p1, p2);
	PRINTF(": %d ticks\r\n", elapse);
}
