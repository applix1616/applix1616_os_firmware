/* TABS8 */

#ifdef DEBUG

#define PRINTREAD		(*(char *)0x380)
#define PRINTWRITE		(*(char *)0x381)
#define PRINTMBIO		(*(char *)0x382)
#define PRINTPROCESSDIR		(*(char *)0x383)
#define PRINTFILESTAT		(*(char *)0x384)
#define	PRINTGETFULLPATH	(*(char *)0x385)
#endif
