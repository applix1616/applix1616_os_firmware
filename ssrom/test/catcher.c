#include <syscalls.h>
#include <signal.h>

int st2000, sa2000, wf2000;
int index = 0;

catcher(sigtype, sigarg, whofrom)
{
	int i;

if (*(char *)0x3a0 == 0x5a)
	PRINTF("catcher(%d, %d, %d)\r\n", sigtype, sigarg, whofrom);

	if (sigtype == SIGTERM)
	{
		PRINTF("catcher: got %d signals:\r\n", index);
		for (i = 0; i < index; i++)
			PRINTF("%d/%d/%d ", sti, sai, wfi);
		EXIT(0);
	}
	stindex = sigtype;
	saindex = sigarg;
	wfindex = whofrom;
	index++;
}

main()
{
	SIGCATCH(catcher);
	for ( ; ; )
		SLEEP(5);
}
