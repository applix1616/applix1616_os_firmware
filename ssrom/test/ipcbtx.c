/*
 * Test ipc block send/receive
 * sender part.
 */

#include <types.h>
#include <syscalls.h>
#include <ipcblock.h>
#include <process.h>

main(argc, argv)
char *argv;
{
	char buf200;
	int rxpid;
	int i;

	rxpid = NAMETOPID("ipcbrx");
	PRINTF("Receiver pid is %d\r\n", rxpid);
	if (rxpid < 0)
		EXIT(rxpid);
	for (i = 0; i < 100; i++)
	{
		buf3 = i;
		BLOCKTX(rxpid, buf, 100, 0);
	}
/*	BLOCKTX(rxpid, &i, 4, 1); */
}
