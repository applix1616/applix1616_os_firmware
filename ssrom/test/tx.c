/*
 * Transmit bytes
 */

#include <syscalls.h>

char buf1024;

main(argc, argv)
char *argv;
{
	int nhunks;
	int start, stop;

	nhunks = (argc > 1) ? atoi(argv1) : 128;

	start = GET_TICKS();
	while (nhunks--)
		WRITE(STDOUT, buf, 1024);
	stop = GET_TICKS();
	FPRINTF(STDERR, "Took %d ticks\r\n", stop - start);
}
