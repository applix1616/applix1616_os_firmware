#include <syscalls.h>

char buf1024;

main()
{
	int nread;
	int pid;

	pid = GETPID();

	FPRINTF(0, "%d starts ", pid);
	do
	{
		nread = READ(STDIN, buf, 1024);
		if (nread > 0)
			WRITE(STDOUT, buf, nread);
	}
	while (nread > 0);
	FPRINTF(0, "%d ends ", pid);
}
