/*
 * Test alarm syscall
 */

#include <syscalls.h>

sigh(sig, arg, arg2)
{
	PRINTF("RING\r\n");
	ALARM(100);
}

main()
{
	SIGCATCH(sigh);
	ALARM(100);
	for ( ; ; )
		SLEEP(1000000);
}
