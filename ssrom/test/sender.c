#include <syscalls.h>
#include <signal.h>

main(argc, argv)
char *argv;
{
	int pid, i, count;

	if (argc > 1)
		count = atoi(argv1);
	else
		count = 100;

	pid = NAMETOPID("catcher");
	PRINTF("catcher pid is %d\r\n", pid);

	for (i = 0; i < count; i++)
		SIGSEND(pid, i + 1000, i + 2000);
	SIGSEND(pid, SIGTERM, 0);
	PRINTF("sender exits\r\n");
}
