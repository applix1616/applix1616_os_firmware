/*
 * Test ipc block send/receive
 * receiver part.
 */

#include <types.h>
#include <syscalls.h>
#include <ipcblock.h>
#include <signal.h>

int gogo = 0;
int data1000;
int nbl = 0;

catch(sigtype, sigarg, whofrom)
{
	int i;
	PRINTF("Rx: got signal(%d, %d) from %d\r\n", sigtype, sigarg, whofrom);
	if (sigtype == SIGTERM)
	{
		PRINTF("Got %d data blocks. Contents:\r\n", nbl);
		for (i = 0; i < nbl; i++)
			PRINTF("%d ", datai);
		PRINTF("\r\n");
		EXIT(0);
	}
	gogo = 1;
}

main(argc, argv)
char *argv;
{
	register IPCBLOCK *ipcblock;

	SIGCATCH(catch);

	while (!gogo)
		SLEEP(100);

	for ( ; ; )
	{
		ipcblock = (IPCBLOCK *)BLOCKRX(IPCB_FETCHWAIT);
		PRINTF("Rx: got %d bytes from PID %d: %d\r\n",
			ipcblock->blocklen, ipcblock->whofrom,
				ipcblock->data3);
		datanbl++ = ipcblock->data3;
		FREEMEM(ipcblock);
	}
}
