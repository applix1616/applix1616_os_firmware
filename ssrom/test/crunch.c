#include <types.h>
#include <files.h>
#include <stdio.h>
#include <syscalls.h>

#define SS	100000

char bufSS;
int loop, subloop;

sigh()
{
	PRINTF("Exit at %d.%d\r\n", loop, subloop);
	EXIT(0);
}

main()
{
	int fd;

	SIGCATCH(sigh);
	fd = CREAT("x", 0, 0);
	if (fd >= 0)
	{
		for (loop = 0; loop < 10; loop++)
		{
			subloop = 0;
			SEEK(fd, 0, 0);
			subloop = 1;
			WRITE(fd, buf, sizeof(buf));
			subloop = 2;
			SEEK(fd, 0, 0);
			subloop = 3;
			READ(fd, buf, sizeof(buf));
			subloop = 4;
		}
	}
	else
		FPRINTF(STDERR, "Can't open 'x': %s\r\n",
				ERRMES(fd));
	PRINTF("Normal exit\r\n");
}
