	global	_main
	global	_trap7
	global	_sp
	global	_doit
	psect	text
	align.w
_main:
f40:
; move.l sp,_sp ;#
	move.l	_sp,-(A7)
	move.l	#19f,-(A7)
	move.l	#-1,-(A7)
	move.l	#48,-(A7)
	move.l	#80,-(A7)
	jsr	_trap7
	add.l	#12,A7
	move.l	D0,A2
	jsr	(A2)
	add.l	#8,A7
	rts
	jmp	f40
	global	_alloca
	align.w
_doit:
	link	A6,#-48
	move.l	#5,-44(A6)
	clr.l	-48(A6)
	jmp	l6
l3:
	move.l	-44(A6),-(A7)
	jsr	_alloca
	add.l	#4,A7
	move.l	D0,D2
	move.l	A6,A0
	move.l	-48(A6),D3
	asl.l	#2,D3
	move.l	D2,-40(A0,D3.l)
move.l sp,_sp ;#
	move.l	_sp,-(A7)
	move.l	A6,A0
	move.l	-48(A6),D3
	asl.l	#2,D3
	move.l	-40(A0,D3.l),-(A7)
	move.l	-44(A6),-(A7)
	move.l	#39f,-(A7)
	move.l	#-1,-(A7)
	move.l	#48,-(A7)
	move.l	#80,-(A7)
	jsr	_trap7
	add.l	#12,A7
	move.l	D0,A2
	jsr	(A2)
	add.l	#16,A7
	add.l	#11,-44(A6)
	add.l	#1,-48(A6)
l6:
	cmp.l	#10,-48(A6)
	blt	l3
	unlk	A6
	rts
	psect	data
19:
	dc.b		115,112,61,48,120,37,120,46,32,67,97,108,108,105,110,103
	dc.b		13,10,0
29:
	dc.b		82,101,116,117,114,110,46,32,115,112,61,48,120,37,120,13
	dc.b		10,0
39:
	dc.b		97,108,108,111,99,32,111,102,32,37,100,32,114,101,116,117
	dc.b		114,110,115,32,48,120,37,120,46,32,83,80,61,32,48,120
	dc.b		37,120,13,10,0
	psect	bss
	align.w	
_sp:
	ds.b		4
	psect	text
