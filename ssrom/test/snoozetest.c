#include <syscalls.h>

#define LOC	((int *)0x4000)

main()
{
	int ret;
	int testaddr(), sighndl();

	*LOC = 0;
	SIGCATCH(sighndl);

	ret = SNOOZE(testaddr, LOC, 0);
	PRINTF("snooze returns %d\r\n", ret);
}

testaddr(addr)
int *addr;
{
	return *addr;
}

sighndl()
{
}
