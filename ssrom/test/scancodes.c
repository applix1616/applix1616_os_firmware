/*
 * Test K/B raw mode
 */

#include <types.h>
#include <storedef.h>
#include <syscalls.h>

main()
{
	int i;
	int j;

	SET_KVEC(1);
	PRINTF("Read of rawmode: %d\r\n", SET_KVEC(3));
	for (i = 0; i < 20; i++)
		PRINTF("0x%x ", GETCHAR());
	SET_KVEC(2);
	PRINTF("Read of rawmode: %d\r\n", SET_KVEC(3));
	for ( ; ; )
	{
		for (j = 0; j < 10000; j++)
		PRINTF("CTL:%2d SHIFT:%2d ALT: %2d CAPSLOCK:%2d NUMLOCK:%2d\r\n",
			ak_ctrl, ak_shift, ak_alt, capslock, numlock);
	}
}
