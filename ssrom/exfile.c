/*	TABS4
 * Prepare a file for execution
 */

#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <blkerrcodes.h>
#include <reloc.h>
#include <process.h>
#include <signal.h>
#include <options.h>
#include "mondefs.h"
#include "xpath.h"
#include "dirfns.h"
#include "newmem.h"
#include "countargs.h"
#include "cantdo.h"
#include "loadreloc.h"
#include "spspace.h"
#include "exfile.h"

char *suffixes[] = { ".xrel", ".shell", ".exec" };

extern char *room1for(), *room0for(), *skipwhite(), *bindex(), *index();

int nxpaths;
int hashonmiss;				/* If true, reread xpaths if command miss */
extern int ouruid;
extern int reslevel;
extern int curpid, syssafe;
extern int miscmodes;

/* Execution and data paths */
struct xpath xpaths[NXPATHS];

void xp_init(void)
{
	register int i;

	/* Mark all paths as containing no files */
	for (i = 0; i < NXPATHS; i++)
		xpaths[i].xplist = 0;
	if (reslevel == 0)
	{
		hashonmiss = 1;
		nxpaths = 0;
	}
}

/*
 * If it is binary, allocate and load.
 * If it is shell, return pointer to doshell().
 */

int prepfile(register char **argv, int *pisbinary,int *pstackspace)
{
	register int retval;
	register int i, xpindex;
	register struct xpath *xpptr;
	register struct xplist *xlptr;
	register char *name;
	register int fretval;
	char *uppername;
	int laddr;
	int doshell();
	char *fullpath;
	int namecnt;
	struct dir_entry dirent;
	char triedonce;
	char *hasslash;
	char **paths, *path;
	int pathindex, npaths;

	name = argv[0];
	uppername = room0for(argv[0]);
	if (!(miscmodes & OPM_CASESENSITIVE))
		realstoupper(uppername);

	fretval = retval = 0;
	triedonce = 0;

	/* Look for 'path' environment string */
	syssafe++;
	if ((path = (char *)GETENV(curpid, "PATH", -1)))
	{	/* split it up */
		paths = (char **)getzmem0((MAXMONPAR + 2) * sizeof(*paths));
		if ((fretval = SLICEARGS(path, paths + 1, 0) < 0))
		{
			syssafe--;
			return fretval;
		}
		paths[0] = "";
		npaths = countargs(paths);
	}
	else
		npaths = 1;
	syssafe--;

	fullpath = (char *)getmem0(strlen(name) + 100);

	/* Try xpaths first? */
	if ((hashonmiss & 2) && !(hasslash = index(name, '/')))
		goto trypath;

trydot:
	for (pathindex = 0; pathindex < npaths; pathindex++)
	{
		for (i = 0; i < 3; i++)
		{
			if (pathindex == 0)
				SPRINTF(fullpath, "%s%s", name, suffixes[i]);
			else
			{
				if (strlen(paths[pathindex]) > 80)
					continue;
				SPRINTF(fullpath, "%s/%s%s",
					paths[pathindex], name, suffixes[i]);
			}
			retval = FILESTAT(fullpath, &dirent);
			if ((retval >= 0) &&
				CHKPERM(&dirent, FS_NOEXEC, fullpath) && !(hashonmiss & 2))
			{	/* Perhaps can't read . : try xpaths */
				fretval = BEC_NOPERM;	/* Better code than 'no such file' */
				goto trypath;
			}
			if (retval == BEC_IOERROR)
				goto ioerr;
			if (retval == 0 || retval == 1)
			{	/* In this directory */
				if ((retval = CHKPERM(&dirent, FS_NOEXEC, fullpath)))
				{
					fretval = retval;
					goto cantexec;
				}
				/* Only uid 0 code can be executed */
				if (dirent.uid && (i != 1))
				{
					retval = BEC_NOPERM;
					fretval = retval;
					goto cantexec;
				}
				if (i == 1)
					goto ldshell;
				goto ldbin;
			}
		}
	}
ioerr:
	if (hashonmiss & 2)
		goto cantexec;
trypath:
	if (!index(name, '/'))
	{	/* Don't try other paths if given full or relative path */
reread:
		for (xpindex = 0; xpindex < nxpaths; xpindex++)
		{
			xpptr = &xpaths[xpindex];
			xlptr = xpptr->xplist;
			for (namecnt = 0; namecnt < xpptr->ninlist;
				namecnt++, xlptr++)
			{
				if (!strucmp(uppername, xlptr->name))
				{	/* Found it: Check that execute permitted on "." */
					if (ouruid)
					{
						if ((retval = FILESTAT(xpptr->xpname, &dirent)) < 0)
							continue;
		/* This code prevents execution of code from directories
		 * which do not have execute permission set
		 * Bit pointless coz giving full path works
		 */
#ifdef notdef
						/* Was fs_noexec|fs_noread here */
						if (retval = CHKPERM(&dirent, FS_NOEXEC,xpptr->xpname))
						{
							fretval = retval;
							continue;
						}
#endif
					}
					/* Now create the full pathname */
					dofreemem(fullpath);
					fullpath =
					 (char *)getmem0(strlen(xpptr->xpname) + strlen(name) + 8);
					SPRINTF(fullpath,"%s/%s%s", xpptr->xpname,name,suffixes[(int)xlptr->type]);
					if ((retval = FILESTAT(fullpath,&dirent)) < 0)
						continue;
					retval = BEC_NOPERM;
					if (dirent.uid ||
						(retval = CHKPERM(&dirent, FS_NOEXEC, fullpath)))
					{
						fretval = retval;
						continue;
					}
					/* It passes */
					if (xlptr->type == 1)
						goto ldshell;
					goto ldbin;
				}
			}
		}
		/* Not found. If rehashing enabled, reread the directories and retry */
		if ((hashonmiss & 1) && !triedonce)
		{
			triedonce = 1;
			rehash(1);
			goto reread;
		}
		if (hashonmiss & 2)
			goto trydot;
	}
cantexec:
	dofreemem(fullpath);
	/* If no other error, return file not found */
	retval = fretval ? fretval : ((retval < 0) ? retval : BEC_NOFILE);
	laddr = cantdo(name, retval, "execute");
	goto gotit;

ldbin:
	laddr = dofload(fullpath, 1, 1, pstackspace);
	*pisbinary = 1;
	dofreemem(fullpath);
	goto gotit;

ldshell:
	laddr = (int)doshell;
	dofreemem(argv[0]);
	argv[0] = room1for(fullpath);		/* A bit shakey ? */
	dofreemem(fullpath);
	*pisbinary = 0;
	*pstackspace = SHELL_STACKSPACE;
gotit:
	dofreemem(uppername);
	if (path)
	{
		for (pathindex = 1; pathindex < npaths; pathindex++)
			dofreemem(paths[pathindex]);
		dofreemem(paths);
	}
	return laddr;
}

/*
 * Shell file processing
 */

int doshell(int argc, char **argv)
{
	register int exindex;
	register uint arg;
	register int retval;
	register int ifd;
	register char *exptr, *ptr;
	register char *exbuf;
	char *fullpath, pc;
	char echoing, trapping;
	char *workbuf;
	static char *shname = "shell interpreter";
	int _ouruid;
	int ptrlen;
	char wholefile;
	int pipefds[2];

	if ((retval = dogetmem(MAXIPLINE * 3, 0, shname)) < 0)
		return retval;
	workbuf = (char *)retval;
	if ((retval = dogetmem(MAXIPLINE * 3 + 3, 0, shname)) < 0)
	{
		dofreemem(workbuf);
		return retval;
	}
	exbuf = (char *)retval;

	fullpath = argv[0];	/* Set up by prepfile() */

	/* Open the file as root, since prepfile() has checked the X bit */
	_ouruid = ouruid;
	ouruid = 0;
	ifd = OPEN(fullpath, SSO_RDONLY);
	ouruid = _ouruid;

	if (ifd < 0)
	{
		cantopen(fullpath, ifd);
		retval = ifd;
		goto err;
	}
	echoing = trapping = 0;		/* Not echoing commands */
exloops:
	if (fgets2(ifd, workbuf, 2 * MAXIPLINE) == 0)
	{
		CLOSE(ifd);
		retval = 0;
		goto err;
	}
	exindex = 0;

/* Look for special commands */
	strcpy(exbuf, workbuf);
	ptr = exptr = skipwhite(exbuf);
	while (*exptr && (!iswhite(*exptr)))
		exptr++;	/* Find end of first word */
	*exptr = '\0';		/* Terminate it */
	if (!strucmp(ptr, "-"))
	{
		echoing = 0;
		goto exloops;
	}
	if (!strucmp(ptr, "+"))
	{
		echoing = 1;
		goto exloops;
	}
	if (!strucmp(ptr, "notrap"))
	{
		trapping = 0;
		goto exloops;
	}
	if (!strucmp(ptr, "trap"))
	{
		trapping = 1;
		goto exloops;
	}
	if (!strucmp(ptr, "trap2"))
	{
		trapping = 2;
		goto exloops;
	}

	ptr = workbuf;
	while (*ptr && (exindex < (MAXIPLINE*3)))
	{
		if (*ptr == '$')
		{
			if (*++ptr == '$')	/* Double '$' */
				exbuf[exindex++] = '$';
			else
			{
				int firstarg, lastarg;

				if ((arg = (unsigned)*ptr) == (unsigned)'*')
				{	/* All args */
					firstarg = 1;
					lastarg = argc;
				}
				else
				{
					arg -= '0';
					if (arg < argc)
					{
						firstarg = arg;
						lastarg = arg + 1;
					}
					else
						firstarg = -1;
				}

				if (firstarg >= 0)
				{	/* Valid $n form */
					while (firstarg < lastarg)
					{
					  exptr = argv[firstarg++];
						while (*exptr &&
							(exindex < (MAXIPLINE*3)))
								exbuf[exindex++] =
									*exptr++;
						/* If $*, add separator space */
						if ((*ptr == '*') &&
							(firstarg != lastarg))
							exbuf[exindex++] = ' ';
					}
				}
				else
				{	/* Not an expandable argument: omit the $n
						only if it was a valid attempt to invoke
						an argument */
					if ((arg > 9) && (*ptr != '*'))
					{
					  exbuf[exindex++] = '$';
					  exbuf[exindex++] = *ptr;
					}
				}
			}
		}
		else
			exbuf[exindex++] = *ptr;
		ptr++;
	}
	exbuf[exindex] = '\0';


	if (echoing)
		PRINTF("--->%s\r\n", exbuf);

	/* V4.2 addition: Look for << redirection */
	ptr = exbuf + exindex;

	while ((ptr > (exbuf + 2)) &&
			(!*ptr || ((pc = *ptr) >= 'A' && pc <= 'Z')))
	{
	  if ((*(ptr-1) == '<') && (*(ptr-2) == '<'))
		{	/* A << redirection */
			char *argvec[2];
			int sipsave, pid;
			long getsret, ofd;
			int shellsighndl();

			if (wholefile = (*ptr == '\0'))
				GETC(ifd);		/* Gobble a \n */

			if ((retval = PIPE(pipefds)) < 0)
			{
				cantcreat("pipe", retval);
				goto crash;
			}
			*(ptr-2) = '\0';		/* Trash the << bit */

			/* Prepare for the exec */
			SIGCATCH(shellsighndl);
			argvec[0] = exbuf;
			argvec[1] = (char *)0;
			sipsave = SET_SIP(pipefds[0]);
			retval = pid = AEXECA(argvec, 1);
			SET_SIP(sipsave);

			if (retval < 0)
			{
pipeerr:
			  CLOSE(pipefds[0]);
			  CLOSE(pipefds[1]);
			  goto crash;
			}

			ofd = pipefds[1];
			/* Feed it */
			if (wholefile)
			{
				int len;
				do
				{
					len = READ(ifd, workbuf, sizeof(workbuf));
					WRITE(ofd, workbuf, len);
				}
				while (len);
			}
			else
			{
				ptrlen = strlen(ptr);
				while (getsret = fgets2(ifd, workbuf, MAXIPLINE + 3))
				{
					if (!strncmp(ptr, workbuf, ptrlen))
						break;
					if ((retval = WRITE(ofd, workbuf, strlen(workbuf))) < 0)
						goto pipeerr;
					PUTC(ofd, '\r');
					PUTC(ofd, '\n');
				}
			}
			CLOSE(pipefds[0]);
			CLOSE(pipefds[1]);
			if (wholefile || (lastchar(workbuf) != '&'))
				WAIT(pid);
			SIGCATCH(0);
			goto exloops;
		}
		ptr--;		/* While ptr */
	}

	if ((((retval = EXEC(exbuf)) < 0) && trapping) ||
			(retval && (trapping == 2)))
	{
		eprintf("\r\nError $%x: terminating %s\r\n", retval, fullpath);
crash:
		CLOSE(ifd);
		goto err;
	}
	goto exloops;
err:
	return retval;
}

/*
 * Handle signals during piped << processing
 */

shellsighndl(sig, arg)
{
	if (sig == SIGPIPE)
		EXIT(0);
	if (sig != SIGCLD)
		SIGBLOCKER(LASTCHILD(GETPID()), sig, arg);
}

/*
 * The 'xpath' command.
 *
 * Usage: xpath + file file ..
 * If +, add paths.
 * If -, delete all paths
 * If neither, replace files
 */

int xpath(int argc,char *argv)
{
	register int i, xpindex, retval;
	char *temp;

	retval = 0;
	if (argc == 1)
	{	/* print xpath command */
		for (xpindex = 0; xpindex < nxpaths; xpindex++)
			PRINTF("%s ", xpaths[xpindex].xpname);
		prcrlf();
		goto out;
	}

	if (!strucmp(argv[1], "-"))
	{
			nxpaths = 0;
			goto out;
	}

	if (!strucmp(argv[1], "+"))
	{
		xpindex = nxpaths;
		i = 2;
	}
	else
	{
		xpindex = 0;
		i = 1;
	}

	for ( ; i < argc; i++)
	{
		register int j;

		if (xpindex == NXPATHS)
		{
			eprintf("Too many xpaths\r\n");
			retval = -1;
			goto out;
		}

		temp = (char *)GETFULLPATH(argv[i], 0);
		for (j = 0; j < xpindex; j++)
			if (!strucmp(xpaths[j].xpname, temp))
				goto isin;	/* Already got it */
		if (strlen(temp) < MAXPATHLENGTH)
			strcpy(xpaths[xpindex++].xpname, temp);
		else
		{
			eprintf("%s: path too long\r\n", temp);
			retval = -3;
		}
isin:		dofreemem(temp);
	}
	nxpaths = xpindex;
out:	if (retval >= 0)
		rehash(1);
	return retval;
}

/*
 * Rebuild the search path list
 */

int rehash(int dofreeing)
{
	register struct xpath *xpptr;
	register struct xplist *xlptr;
	register struct dir_entry *pdir;
	register int retval = 0;
	register int i, nnames;
	struct dir_entry *dirptr;
	char *fnptr;
	uint dirsize;
	char *fpath;
	int xpindex, arg, pass;

	/* First free all the old ones */
	if (dofreeing)
	{
		xpptr = xpaths;
		for (i = 0; i < NXPATHS; i++, xpptr++)
		{
			if (xpptr->xplist)
				dofreemem(xpptr->xplist);
			xpptr->xplist = 0;
		}
	}

	xpptr = xpaths;
	for (xpindex = 0; xpindex < nxpaths; xpindex++, xpptr++)
	{
		fpath = xpptr->xpname;
		xpptr->ninlist = 0;
		/* Read the parent directory */
		retval = RDALLDIR(fpath, 0, 0, &dirsize);
		if (retval < 0)
		{
			cantdo(fpath, retval, "scan");
			continue;
		}
		dirptr = (struct dir_entry *)retval;
		nnames = 0;
		/* Pass 1: count files. Pass2, copy them in */
		for (pass = 0; pass < 2; pass++)
		{
			xlptr = xpptr->xplist;
			/* Through the directory three times, search for target files */
			for (i = 0; i < 3; i++)
			{
				pdir = dirptr;	/* Directory start */
				/* Scan for executable files */
				for (arg = 0; arg < dirsize; arg++, pdir++)
				{
					fnptr = pdir->file_name;
					if (!strucmp(bindex(fnptr,'.')-1, suffixes[i]))
					{	/* Executable */
						if (pass)
						{
							char buf[FNAMESIZE + 2];

							strcpy(buf, fnptr);
							/* Chop off the extension */
							*(bindex(buf, '.') - 1) = '\0';
							strcpy(xlptr->name, buf);
							if (!(miscmodes & OPM_CASESENSITIVE))
								realstoupper(xlptr->name);
							xlptr->type = i;
							xlptr++;
						}
						else
							nnames++;
					}
				}	/* Next directory entry */
			}		/* Next suffix */
			if (pass == 0)
			{
				if (nnames)
				{
					xpptr->xplist = (struct xplist *)getmem1(
						nnames * sizeof(struct xplist));
				}
				xpptr->ninlist = nnames;
			}
		}
		dofreemem(dirptr);
	}			/* Next xpath */
	return retval;
}

/* Return pointer to an xpath for oscontrol */
char *
xpname(n)
uint n;
{
	return (n < nxpaths) ? xpaths[n].xpname : (char *)-1;
}
