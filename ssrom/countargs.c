/*
 * Count number of entries in a null terminated argv list
 */

int countargs(char **argv)
{
	int ret;

	for (ret = 0; *argv++; ret++)
		;
	return ret;
}
