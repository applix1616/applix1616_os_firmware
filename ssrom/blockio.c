/* TABS4
 * Block I/O handling code
 */

#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <blkerrcodes.h>
#include <options.h>
#include <hwdefs.h>
//#include <splfns.h>
#include "debug.h"
#include "setmem.h"
#include "strucmp.h"
#include "movstr.h"
#include "filefns.h"
#include "bitcount.h"
#include "movmem.h"
#include "systrap.h"
#include "psprintf.h"
#include "newmem.h"
#include "prcrlf.h"
#include "spl.h"
#include "cachecont.h"
#include "blockio.h"

#define BDTIMEOUT	4	/* 2 sec timeout before root block checked */

int curstuffvalid;	/* Zero if new disk detected */
char *cur_path;		/* Full path of current directory */
int cur_pbdvr;		/* Block driver number of current path */
struct dir_entry cur_pdir; /* Copy of directory entry for current directory */
char *cur_dir;		/* Cached copy of current directory contents */
int canwriterb;		/* If set, can write to root block */
char indirblock;	/* State of current disk I/O */
uint bdlockin;		/* Which drivers to lock in */

/* The drivers */
struct blkdriver bdrivers[NBLKDRIVERS];

char *room1for();
static int cachecheck(register uint bdvrnum,register uint firstblock,uint nblocks,uint isread,uchar *addr);
int _find_bdvr(char *devname);
void outofmem(int i);
int doio(uint bdvrnum,uint isread,uint addr,uint firstblock,int nblocks);
int diskchange(struct rootblock *rbptr,uint special,char *dateptr);


extern int diskverbose;
extern int _gooddevs, _bootunit;
extern int reslevel;
extern int syssafe;
extern int ouruid;

extern void cprintf(char *str, void *p1, void *p2, void *p3 ); /* found in main */

/* Initialise block drivers */
void blkio_init(void)
{
	register struct blkdriver *bdptr;
	register int i;

	canwriterb = 0;
	curstuffvalid = 0;
	if (reslevel == 0)
	{
		clearmem(bdrivers, sizeof(bdrivers));
		bdlockin = 0;
	}
	bdptr = bdrivers;
	for (i = 0; i < NBLKDRIVERS; i++, bdptr++)
	{
		bdptr->bitmapchanged = 0;
		bdptr->rootblock.special = -1;
		bdptr->usetimer = 1000000;	/* Force reread */
		BDMISC(i, MB_RESET, reslevel);
	}
	SET_VSVEC(checkbdvr, 25, 0);	/* Call at 2 Hz */
}

/*
 * System call 100: Install a block I/O driver. Return driver number, or code on
 * error.
 *
 * If the driver supports multi-block I/O, 'misc' POINTS to misc entry point
 * followed by multi-block I/O point. This is flagged by a control-A in front
 * ofthe devname: kludgey, but backward & forward compatible
 */

int _inst_bdvr(int (*bread)(void),int (*bwrite)(void),int (*misc)(void),char *devname,uchar *bitmapbuf)
{
	register int bdvrnum;
	register struct blkdriver *bdptr;
	register struct multidriver *mdptr;
	register int retval;
	register int ismulti;
	int pass;

	ismulti = 0;
	if (*devname == 0x01)
	{
		devname++;
		ismulti = 1;
	}
	mdptr = (struct multidriver *)misc;

	/*
	 * Two passes: first looks for same name, second for gap
	 */
	for (pass = 0; pass < 2; pass++)
	{
		bdptr = bdrivers;
		for (bdvrnum = 0; bdvrnum < NBLKDRIVERS; bdvrnum++, bdptr++)
		{
		  if(!strucmp(bdptr->devname, devname) || (pass && bread && !bdptr->bread))
		  {	/* Empty entry or replacement */
				bdptr->bread = bread;
				bdptr->bwrite = bwrite;
				/* Permit read of root block */
				bdptr->rootblock.nblocks = 8;
				/* Force read of bitmap */
				bdptr->usetimer = 1000000;
				if (ismulti)	/* Multi-block driver? */
				{
					bdptr->multirw = mdptr->multirw;
					bdptr->misc = mdptr->misc;
					bdptr->version = mdptr->version;
				}
				else
				{
					bdptr->multirw = 0;
					bdptr->misc = misc;
					bdptr->version = 0;
				}
				movstr(bdptr->devname, devname, FNAMESIZE);
#ifdef notdef
				stoupper(bdptr->devname);
#endif
				bdptr->bitmap = bitmapbuf;
				bdptr->bitmapchanged = 0;
				retval = bdvrnum;
				goto out;
			}
		}
	}
	retval = BEC_NBDVRSPACE;
out:
	return retval;
}

/*
 * System call 102: Find a block driver. Return driver number or code.
 * If called with -1, return current driver structure pointer.
 * If called with 0 to (NBLKDRIVERS - 1) return pointer to driver structure.
 */

int _find_bdvr(char *devname)
{
	register int bdvrnum;
	register struct blkdriver *bdptr;
	register int dnlong;

	dnlong = (int)devname;

	if (dnlong == -1)
	/* 4.1 version
		return (int)(&bdriverscur_pbdvr);
	 */
		return get_bdev(cur_path, 0);

	if (dnlong < NBLKDRIVERS)
	{
		if (!bdrivers[dnlong].bread)
			return BEC_NOBDRIVER;
/*		if ((retval = newdisk(dnlong)) < 0)
			return retval;
*/
		return (int)(&bdrivers[dnlong]);
	}

	bdptr = bdrivers;
	for (bdvrnum = 0; bdvrnum < NBLKDRIVERS; bdvrnum++, bdptr++)
	{
		if (bdptr->bread && (!strucmp((char *)dnlong, bdptr->devname)))
			return bdvrnum;
	}
	return BEC_NOBDRIVER;
}

/*
 * System call 103: block read. Return non-zero on error
 */

int _blkread(int block,char *buf,int bdvrnum)
{
	return MULTIBLKIO(bdvrnum, MIO_SREAD, buf, block, 1);
}

/*
 * System call 104: block write. Return non-zero on error
 */

int _blkwrite(int block,char *buf,int bdvrnum)
{
	return MULTIBLKIO(bdvrnum, MIO_SWRITE, buf, block, 1);
}

/*
 * System call 117: call a block driver's miscellaneous entry point.
 */

int _bdmisc(uint bdvrnum,uint code,uint arg1)
{
	register struct blkdriver *bdptr;
	register int retval = 0;

	bdptr = &bdrivers[bdvrnum];
	if ((bdvrnum >= NBLKDRIVERS) || (!bdptr->bread))
		return BEC_NOBDRIVER;

	fsentry(0);

	switch (code)
	{
	case MB_BMBREAD:
		indirblock = 2;
		break;
	case MB_DIRREAD:
		indirblock = 1;
		break;
	case MB_NDIRREAD:
		indirblock = 0;
		break;
#ifdef IOCACHE
	case MB_FLUSH:
		iocachetrash(bdvrnum);
		break;
#endif
	}

	switch (code)
	{
	case MB_NBLOCKS:
		retval = bdptr->rootblock.nblocks;
		break;
	case MB_USEDBLOCKS:
		retval = bitcount(bdptr->bitmap, bdptr->rootblock.nblocks);
		break;
	case MB_FREEBLOCKS:
		retval = bdptr->rootblock.nblocks -
			bitcount(bdptr->bitmap, bdptr->rootblock.nblocks);
		break;
	case MB_READROOT:
		movmem(&bdptr->rootblock,(void *) arg1, sizeof(bdptr->rootblock));
		retval = arg1;
		break;
	case MB_REREAD:		/* Force root block/bitmap reread */
		bdptr->usetimer = 1000000;
		break;
	case MB_DISABLE:	/* Disable the device */
		if (arg1 >= 0)
			bdptr->disabled = arg1;
		retval = bdptr->disabled;
		break;
	case MB_UID0:		/* Only UID 0 accesses it */
		if (arg1 >= 0)
			bdptr->uid0 = arg1;
		retval = bdptr->uid0;
		break;
	case MB_WRPROT:		/* Write protect it */
		if (arg1 >= 0)
			bdptr->wrprot = arg1;
		retval = bdptr->wrprot;
		break;
	default:
		if (bdptr->misc)	/* Dispatch it */
			retval = entry(bdptr->misc, code, arg1,0,0,0,0);
		break;
	}
	fsexit(0);
	return retval;
}

int suppwrite(int block,struct blkdriver *bdptr)
{	/* Not allowed to write system blocks */
	eprintf("Suppressed write to block  on \r\n"); //, block); /*bdptr->devname); */
	return BEC_BADBLOCK;
}

/*
 * Force a re-read of the disk's root block and bitmap if it is invalid or timed out
 */

int newdisk(uint bdvrnum)
{
	register struct blkdriver *bdptr;
	register int retval;

	retval = 0;

	if (bdvrnum >= NBLKDRIVERS)
	{
		retval = BEC_NOBDRIVER;
		goto err2;
	}
	bdptr = &bdrivers[bdvrnum];
	fsentry(0);
	if ((bdptr->rootblock.nblocks < 2) || (bdptr->usetimer > BDTIMEOUT))
	{	/*
		 * Use blkread to trash the cache entries, read bitmap if
		 * necessary
		 */
		retval = BLKREAD(0, 0, bdvrnum);
		if ((retval >= 0) && (bdvrnum == cur_pbdvr) && !curstuffvalid)
		{
			/*
			 * Panic: changed disk in current path. cd to top
			 */
			dofreemem(cur_path);
			cur_path = room1for(bdptr->devname);
			dofreemem(cur_dir);
#ifdef IOCACHE
			iocachetrash(bdvrnum);
#endif
			cur_dir = (char *)getmem1(bdptr->rootblock.rootdir.file_size * BLOCKSIZE);
			if ((int)cur_dir < 0)
				outofmem(0);
			movde(&bdptr->rootblock.rootdir, &cur_pdir);
			/* cur_pbdvr doesn't change */
			BDMISC(bdvrnum, MB_DIRREAD, 0);
			if ((retval = MULTIBLKIO(bdvrnum, MIO_SREAD, cur_dir,
				cur_pdir.blkmapblk, cur_pdir.file_size)) < 0)
					goto err;
			curstuffvalid = 1;
		}
	}
err:	fsexit(0);
err2:	return retval;
}

/*
 * Run through the block drivers, incrementing the use timer for all removable
 * units
 */

void checkbdvr(void)
{
	register int i;
	register struct blkdriver *bdptr;

	bdptr = bdrivers;
	i = NBLKDRIVERS - 1;
	while (i--)
	{
		if (bdptr->rootblock.removable)
			bdptr->usetimer++;
		bdptr++;
	}
}

/*
 * Boot the system: scan all the block drivers, searching for a bootable unit
 */

void bootup(void)
{
	register int lunit, unit;
	register struct blkdriver *bdptr;

	for (lunit = -1; lunit < NBLKDRIVERS; lunit++)
	{
		unit = lunit;
		if (lunit == -1)
		{
			if ((reslevel == 0) && (_bootunit >= 0))
				unit = _bootunit;
			else
				continue;
		}
		else if (*IPORT & 0x40)
		{	/* Swap /f0 & /h0 */
			if (lunit == 1)
				unit = 3;
			else if (lunit == 3)
				unit = 1;
		}

		if (!(_gooddevs & (1 << unit)))
			continue;		/* Bad device */
		if (!newdisk(unit))		/* Read in the root block */
		{
			bdptr = &bdrivers[unit];
			if (bdptr->rootblock.bootblock)
			{
				if (BLKREAD(bdptr->rootblock.bootblock,
						BOOTADDR, unit) >= 0)
				{
					cprintf("Booting from %s", bdptr->devname,0,0);
					prcrlf();
					prcrlf();
					if (reslevel == 0) /* Reenable mesages at level 0 */
						OPTION(OPT_DISKVERBOSE, 1);
					entry((void *)BOOTADDR, reslevel, unit,0,0,0,0);
					return;
				}
			}
		}
	}
}

void outofmem(int i)
{
	_spl7();
	conprintf("Panic: out of memory. PC = $%x", *(&i - 1));
	for ( ; ; )
		;
}

/*
 * System call 119
 *
 * Handle multiple block I/O requests
 *
 * We are passed:
 *
 * block device number
 * command
 *	MIO_SWRITE (0) = sequential multi-block write
 *	MIO_SREAD (1) = sequential multi-block read
 *	MIO_RWRITE (2) = random multi-block write
 *	MIO_RREAD (3) = random multi-block read
 * target memory address
 * start block (sequential) OR block list pointer (random)
 * nblocks
 */

int _multiblkio(register int bdvrnum,register uint cmd,char *addr,register ushort *blockspec,uint nblocks)
{
	register struct blkdriver *bdptr;
	register struct rootblock *rbptr;
	register int retval;
	register uint blockindex;
	int isseq, isread;
	int lockedin;

	if (cmd > MIO_RREAD)
		return BEC_BADARG;

	fsentry(0);

	if ((lockedin = (bdlockin & (1 << bdvrnum))))
		syssafe++;	/* Goes quicker on /s0, etc */

	bdptr = &bdrivers[bdvrnum];
	if ((bdvrnum >= NBLKDRIVERS) || (bdptr->bread == 0))
	{	/* Bad driver or no device installed */
		retval = BEC_NOBDRIVER;
		goto err;
	}

	/*
	 * Is the disk currently valid?
	 */
	if (bdptr->usetimer > BDTIMEOUT)
	{	/* Must read in root block & bitmap */
		char blkbuf[BLOCKSIZE];
		int changed;
		rbptr = (struct rootblock *)blkbuf;
		/* Flush the buffers in the block device driver first */
		if ((retval = BDMISC(bdvrnum, MB_FLUSH, 0)) < 0)
			goto err;
		if ((retval = doio(bdvrnum, 1,(uint) rbptr, ROOTBLOCK, 1)) < 0)
			goto err;
		/*
		 * Check for a swapped disk & mark cache as invalid if necessary
		 */
		if ((changed = diskchange(rbptr,bdptr->rootblock.special,
				bdptr->rootblock.rootdir.date)))
		{ /* A new disk. Make sure we can't write out the old bitmap! */
			if (bdptr->bitmapchanged)
				eprintf("\007Device swapped, write still pending!\r\n");
			bdptr->bitmapchanged = 0;
			bdptr->bmoffset = 0;	/* Rescan bitmap */
			cachedel((char *)bdvrnum);
			if (bdvrnum == cur_pbdvr)
			{	/* Same driver as current one */
				curstuffvalid = 0;
			}
			if ((retval = BDMISC(bdvrnum,MB_NEWDISK,0)) < 0)
				goto err;
		}
		movmem(rbptr, &bdptr->rootblock, sizeof(bdptr->rootblock));
		/* Make sure it is a multiple of 8 */
		bdptr->rootblock.nblocks &= ~7;
#ifdef IOCACHE
		if (changed)
			iocachetrash(bdvrnum);
#endif
		if (changed || ((bdptr->usetimer > BDTIMEOUT) &&
					!bdptr->bitmapchanged))
		{	/* Read in the bitmap */
			int nbmb;	/* No. of blocks in bitmap */
			nbmb = (bdptr->rootblock.nblocks +
				(BLOCKSIZE * 8 - 1)) / (BLOCKSIZE * 8);
			if (	(bdptr->rootblock.bitmapstart >=
				bdptr->rootblock.nblocks) ||
				(bdptr->rootblock.bitmapstart >=
				bdptr->rootblock.dirstart) )
			{	/* Rubbish in root block */
				retval = BEC_BADBLOCK;
				goto err;
			}

			if ((retval = doio(bdvrnum, 1,(uint) bdptr->bitmap,
				bdptr->rootblock.bitmapstart, nbmb)) < 0)
				goto err;
			/* Calculate verification block count */
			bdptr->nblksused = BDMISC(bdvrnum, MB_USEDBLOCKS, 0);
		}
#ifdef notdef
		if (((int)blockspec) == -1)
		{
			retval = 0;	/* Just wanted rootblock, bitmap */
			goto err;
		}
#endif
	}

	isseq = ((cmd == MIO_SREAD) || (cmd == MIO_SWRITE));
	isread = ((cmd == MIO_SREAD) || (cmd == MIO_RREAD));

	if ((int)addr < 0x3c00)		/* Called by newdisk OR error */
		goto noerr;

	/* Check for write to root block, etc */
	if (!isread && !canwriterb)
	{
		if (isseq)
		{
			if ((uint)blockspec < bdptr->rootblock.dirstart)
			{
				retval = suppwrite((int)blockspec, bdptr);
				goto err;
			}
		}
		else
		{
			for (blockindex = 0; blockindex < nblocks; blockindex++)
			{
				if (blockspec[blockindex] <
					bdptr->rootblock.dirstart)
				{
					retval = suppwrite(blockspec[blockindex], bdptr);
					goto err;
				}
			}
		}
	}

	/*
	 * Perform the I/O.
	 */
	if (isseq)
	{
		retval = cachecheck(bdvrnum, blockspec[0], nblocks, isread,(uchar *) addr);
		if (retval < 0)
			goto err;
	}
	else
	{
		/*
		 * Attempt to convert runs of blocks in a table into
		 * sequential calls.
		 */
		int seqcmd, nseqblks;
		seqcmd = (cmd == MIO_RREAD) ? MIO_SREAD : MIO_SWRITE;
		for (blockindex = 0; blockindex < nblocks; )
		{
			nseqblks = 1;
			if ((blockindex == nblocks - 1) ||
			 (blockspec[blockindex+1] != blockspec[blockindex] + 1))
			{	/* Not in a run */
				retval = cachecheck(bdvrnum,
				 blockspec[blockindex], nseqblks, seqcmd, (uchar *)addr);
			}
			else
			{	/* A sequential run */
				while (((blockindex + nseqblks) < nblocks) &&
					((blockspec[blockindex] + nseqblks ==
					blockspec[blockindex] + nseqblks - 1 + 1)))
					nseqblks++;
				retval = cachecheck(bdvrnum, blockspec[blockindex],
					nseqblks, seqcmd,(uchar *) addr);
			}
			addr += nseqblks * BLOCKSIZE;
			if (retval < 0)	/* An error? */
			{
				retval = BEC_IOERROR;
				goto err;
			}
			blockindex += nseqblks;
		}
	}
noerr:
	bdptr->usetimer = 0;
	retval = 0;
err:
	if (lockedin)
		syssafe--;
	fsexit(0);
	return retval;
}

/*
 * Block I/O and cache management.
 *
 * We are passed the count of blocks, number of blocks, readflag, memory address.
 * Perform the I/O, keeping the cache up to date.
 * 'isread' = 0 to write, 1 to read
 */

static int cachecheck(register uint bdvrnum,register uint firstblock,uint nblocks,uint isread,uchar *addr)
{
	register uint lastblock;
	register int retval;
	register struct blkdriver *bdptr;
	register uchar *uptr, mask;

	lastblock = firstblock + nblocks;
	bdptr = &bdrivers[bdvrnum];

	/* Check that all the blocks being read are reserved: consistency thing */
	uptr = bdptr->bitmap + (firstblock >> 3);
	mask = 0x80 >> (firstblock & 7);
	for (retval = 0; retval < nblocks; retval++)
	{
		if (!(*uptr & mask))
		{
			if (!(OPTION(OPT_MISC|OPT_READ, 0) &
					OPM_DONT_REPORT_FREE_BLOCK_READS))
		conprintf("\007Block %d on %s unreserved: Volume may be damaged\r\n",
					retval + firstblock, bdptr->devname);
		}
		if (!(mask >>= 1))
		{
			mask = 0x80;
			uptr++;
			while (*uptr == 0xff && retval < nblocks)
			{	/* Quicker */
				uptr++;
				retval += 8;
			}
		}
	}

	retval = 0;

	if (bdvrnum == cur_pbdvr && curstuffvalid)
	{	/* Look for blocks in cache */
		uint cbstart, cbend;
		cbstart = cur_pdir.blkmapblk;
		cbend = cbstart + cur_pdir.file_size;
		if ((firstblock < cbend) && (lastblock > cbstart))
		{
			uint olstart, olend;
			char *cdaddr;
			/* Calculate overlap region */
			olstart = (firstblock > cbstart) ? firstblock : cbstart;
			olend = (lastblock < cbend) ? lastblock : cbend;
			if (firstblock < olstart)
			{
				if ((retval = doio(bdvrnum, isread,(uint) addr,
						firstblock, olstart - firstblock)) < 0)
					goto err;
			}
			addr += (olstart - firstblock) * BLOCKSIZE;
			cdaddr = cur_dir + (olstart - cbstart) * BLOCKSIZE;
			/* Now move in/out cached memory */
			if (isread)
				movmem(cdaddr, addr, (olend - olstart) * BLOCKSIZE);
			else
			{	/* Update cache & write out */
				movmem(addr, cdaddr, (olend - olstart) * BLOCKSIZE);
				if ((retval = doio(bdvrnum, isread,(uint) addr,
						olstart, olend - olstart)) < 0)
					goto err;
			}
			addr += (olend - olstart) * BLOCKSIZE;
			/* The rest? */
			if (olend < lastblock)
			{
				if ((retval = doio(bdvrnum, isread,(uint) addr,
					olend, lastblock - olend)) < 0)
					goto err;
			}
			goto on1;
		}
	}
	/* Not on current disk or not in cache */
	retval = doio(bdvrnum, isread,(uint) addr, firstblock, nblocks);
	goto on1;
err:	curstuffvalid = 0;
on1:
	if ((retval < 0) && (!isread))
	{	/* Suppress bitmap rewrite if I/O error */
		bdptr->bitmapchanged = 0;
		if (bdvrnum == cur_pbdvr)
			curstuffvalid = 0;	/* Toss cache */
	}
	return retval;
}

/*
 * Compare the passed special number and date with that in the passed rootblock
 */

int diskchange(struct rootblock *rbptr,uint special,char *dateptr)
{
	if (rbptr->special != special)
		return 1;
	for (special = 0; special < sizeof(rbptr->rootdir.date); special++)
	{
		if (dateptr[special] != rbptr->rootdir.date[special])
			return 1;
	}
	return 0;
}

/*
 * Invalidate current directory cache
 */

void dumpcurdir(int bdvrnum)
{
	if (bdvrnum == cur_pbdvr)
		curstuffvalid = 0;
}

#ifndef IOCACHE
/*
 * At last, perform the device driver call
 */

int doio(uint bdvrnum,uint isread,uint addr,uint firstblock,int nblocks)
{
	struct blkdriver *bdptr;
	register uint block;
	register int retval, (*rw)();

	bdptr = &bdrivers[bdvrnum];

	if (bdptr->disabled)
		return BEC_IOERROR;
	if (bdptr->uid0 && ouruid)
		return BEC_NOPERM;
	if (bdptr->wrprot && !isread)
		return BEC_WRTPROT;

	if (bdptr->multirw)
		retval = entry(bdptr->multirw, isread, addr, firstblock, nblocks,0,0);
	else
	{
		/*
	 	 * If the device driver does not support multi-block I/O,
		 * fudge it.
		 */
		rw = (isread) ? bdptr->bread : bdptr->bwrite;
		for (block = 0; block < nblocks; block++)
		{
			retval = entry(*rw, firstblock++, addr,0,0,0,0);
			addr += BLOCKSIZE;
			if (retval < 0)
				goto err;
		}
	}
err:
	return retval;
}
#endif
