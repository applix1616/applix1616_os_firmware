/*
 * All the memory allocation stuff.
 */

#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <blkerrcodes.h>
#include <process.h>
//#include <fcb.h>
#include <spl.h>


#include "setmem.h"
#include "memfns.h"
#include "bitcount.h"
#include "proc.h"
#include "psprintf.h"
#include "monfns.h"
#include "ssdefs.h"
#include "newmem.h"
//#include <splfns.h>

#define HUNKSIZE	32		/* 32 bytes / bit */
#define BYTESPERLONG	(32*HUNKSIZE)	/* No. of bytes described by a long */
#define TOPBIT		((unsigned long)0x80000000)	/* Bit 31 */
#define ALLONES		0xffffffff

int memerrflag;		/* If true, out-of-memory causes internal error */

uint *glbtable;		/* Pointer to global usage table */
uint *endtable;		/* Bitmap of block end markers */
uint glblongs;		/* Size of glbtable in longwords */
uint glbhunks;		/* Size of tables in hunks */
uint **exectables;	/* Pointers to tables for each exec */
uint arenabot;		/* Lowest address in arena */
uint arenatop;		/* Highest address in arena (1k multiple) */
/* memchecting is used to deceive programs which use too much RAM */
uint memcheating;
int lastpid;		/* Previous slot in exec tables for pids */
int mmnotup;		/* Flag saying memory manager has not started */
uchar malloc_debug_mode;

long getmem1(int n);
long getzmem(int n, int mode);


extern int curpid;	/* Current process ID */
extern int syssafe;
extern int reslevel;

void mem_init(int stackspace)
{
	uint ramsize;		/* arenatop - arenabot */
	extern int _initsp();

/* The following kludge allows things to work for the ROM or RAM versions */
	arenatop = (int)&stackspace - stackspace;	/* 64k stack area */
	if ((int)_initsp < arenatop)
		arenatop = (int)_initsp;
	arenabot = TPALOC;
	lastpid = 0;
	ramsize = arenatop - arenabot;
	glblongs = ramsize / BYTESPERLONG;
	glbhunks = glblongs * 32;
	glbtable = (uint *)(arenatop -= ((glblongs + 1) * sizeof(*glbtable)));
	endtable = (uint *)(arenatop -= ((glblongs + 1) * sizeof(*endtable)));
	exectables = (uint **)(arenatop -= ((MAXPIDS + 1) *
			sizeof(*exectables)));
	/* Round arenatop down to a 1k multiple */
	arenatop = (arenatop - (BYTESPERLONG - 1)) & (~(BYTESPERLONG - 1));
	ramsize = arenatop - arenabot;
	glblongs = ramsize / BYTESPERLONG;
	glbhunks = glblongs * 32;
	lsetmem(glbtable, 0, glblongs + 1);
	lsetmem(endtable, 0, glblongs + 1);
	lsetmem(exectables, 0, MAXPIDS + 1);

	if (reslevel == 0)
	{
		memerrflag = 1;
		memcheating = 2;
	}
	mmnotup = 0;
	malloc_debug_mode = 0;
}

/*
 * System call 62: get some memory.
 * Usage of 'mode':
 *	If mode == 0, allocate 'nbytes' memory, freeable on exec exit
 *	If mode == 1, allocate 'nbytes' memory, non-freeable
 *	If mode == 2, return size of largest available free block
 *	If mode == 3, return total free memory count
 *	If mode == 4, return glbtable
 *	If mode == 5, return endtable
 *	If mode == 6, return glblongs
 *	If mode == 7, return exectables
 *	If mode == 8, return arenatop
 * 	If mode == 9, set memcheating to nbytes
 *	If mode == 10, return REAL total free count
 *	If mode == 11, allocate new pid for exec
 *	If mode == 12, set malloc_debug_mode to 'nbytes'
 */

int _getmem(uint nbytes,int mode)
/* 0: freeable memory on program exit */
{
	register int tabindex;
	register uint mask;
	register uint nhunks, hunkcnt;
	register uint *table;
	int retval;
	uint masksave;
	uint endindex, endmask;
	char retry;

	if (mode == 11)
	{
		retval = _newpid();
		goto out2;
	}

	if (mmnotup)
		PRINTF("getmem(%d, %d) : not ready\r\n", nbytes, mode);

	syssafe++;
	retry = 0;

	if ((mode == 0) || (mode == 1))
	{	/* A normal malloc */
		if (!nbytes)
			nbytes++;
		nhunks = (nbytes + (HUNKSIZE - 1)) / HUNKSIZE;
		/* Look for a free long in the master table */
again:
		tabindex = glblongs;
		table = glbtable + tabindex;
rescan:
		/* Kludgey here, but it must terminate eventually */
		tabindex--;
		table--;
		tabindex = searchdown(table, tabindex);
		if (tabindex < (int)(nhunks / 32)) /* No free memory! */
		{
			int nneeded;	/* Number of extra bits needed */
			nneeded = nhunks - tabindex * 32;
			mask = TOPBIT;
			while (mask)
			{
				if (table[tabindex] & mask)
					goto setbit;
				nneeded--;
				mask >>= 1;
			}
setbit:			if (nneeded > 0)
			{
				if (retry++ == 0)
				{
					DUMPLASTLINES(-1);
#ifdef notdef
					TRASHENVSTRINGS(-1);
#endif
					goto again;
				}
				retval = BEC_NOBUF;
				goto out;
			}
		}
		/* tabindex points to non-full entry */
		table = glbtable + tabindex;
		/* Find first free bit */
		mask = 1;
toosmall:
		hunkcnt = *table;	/* Borrow hunkcnt */
		while (mask & hunkcnt)
			mask <<= 1;
		if (mask == 0)
			goto rescan;
		/* Record the position of the last bit in the segment */
		endindex = tabindex;
		endmask = mask;
		/* See if it is big enough */
		hunkcnt = nhunks;
		while (--hunkcnt)
		{
			mask <<= 1;
			if (!mask)
			{
				mask = 1;
				tabindex--;
				table--;
				while (hunkcnt > 32 && !*table)
				{	/* 32 at a time */
					tabindex--;
					table--;
					hunkcnt -= 32;
				}
			}
			if (mask & *table)
				goto toosmall;
		}
		/* It fits! tabindex and mask describe the location */
		retval = TPALOC + (tabindex * BYTESPERLONG);
		masksave = mask;
		while (mask <<= 1)
			retval += HUNKSIZE;
		mask = masksave;
		/* Now set all the bits in the master table */
		setbits(table, mask, nhunks, 1);
		/* If it is releasable memory, mark it in the exec table */
		if (mode == 0)
			setbits(exectables[curpid + tabindex], mask,
				nhunks, 1);
		/* Set the end marker */
		endtable[endindex] |= endmask;
		/* Debug mode? */
		if (malloc_debug_mode & 1)
		{
			lsetmem(&retval,
				(mode == 0) ? 0x99999999 : 0xbbbbbbbb,
				((nhunks * HUNKSIZE) / sizeof(long)));
		}
		/* Done! */
	}
	else if ((mode == 2) || (mode == 10))
	{	/* Return size of largest free block */
		retval = bitrun(glbtable, glblongs * 4);
		if (memcheating && (mode == 2))
			retval /= memcheating;
	}
	else if (mode == 3)
	{	/* Return total free memory */
		retval = bitcount(glbtable, glblongs * 32);
		/* Convert to byte count */
		retval *= HUNKSIZE;
		/* Convert to free memory */
		retval = arenatop - arenabot - retval;
	}
	else
	{
		switch (mode)
		{
 		case 4: retval = (int)glbtable;
			break;
 		case 5: retval = (int)endtable;
			break;
 		case 6: retval = (int)glblongs;
			break;
 		case 7: retval = (int)exectables;
			break;
 		case 8: retval = (int)arenatop;
			break;
		case 9: retval = memcheating;
			if (((int)nbytes) > 0)
				memcheating = nbytes;
			break;
		case 12:
			if (nbytes < 33)
				malloc_debug_mode = nbytes;
			retval = malloc_debug_mode;
			break;
		default: retval = BEC_BADARG;
			break;
		}
	}
out:
	syssafe--;
	if (retval == BEC_NOBUF)
		memfault(retval);
out2:
	return retval;
}

/*
 * System call 63: Get some specifically addressed memory
 */

int _getfmem(uint firstaddr,uint nbytes,uint mode)
{
	register uint mask;
	register uint nhunks;
	register uint hunkindex;
	register uint tabindex;
	register uint *table;
	uint nkbytes;
	uint tabisave, masksave, nhunksave;
	int retval;

	syssafe++;
	retval = firstaddr;
	if (!nbytes)
		nbytes++;
	if (firstaddr & (HUNKSIZE-1))
	{
		//eprintf("getfmem: address 0x%x not 32 byte multiple\r\n",firstaddr);
		retval = BEC_NOBUF;
		goto out;
	}
	nhunks = (nbytes + (HUNKSIZE - 1)) / HUNKSIZE;
	hunkindex = (firstaddr - TPALOC) / HUNKSIZE;
	if (hunkindex >= glbhunks || (hunkindex + nhunks) > glbhunks)
	{
		retval = BEC_NOBUF;
		goto out;
	}
	/* Check to see that these hunks are free */
	tabindex = hunkindex / 32;
	tabisave = tabindex;
	mask = TOPBIT;
	mask >>= (hunkindex & 31);
	masksave = mask;
	nhunksave = nhunks;
	table = glbtable + tabindex;
	while (nhunks--)
	{
		if (*table & mask)
		{
sorry:
			retval = BEC_NOBUF;
			goto out;
		}
		mask >>= 1;
		if (!mask)
		{	/* Search in longs */
			mask = TOPBIT;
			tabindex++;
			table++;
			nkbytes = nhunks / 32;
			tabindex += nkbytes;
			while (nkbytes--)
				if (*table++)
					goto sorry;
			nhunks &= 31;
		}
	}
	/* Fine, set the appropriate bits */
	setbits(glbtable + tabisave, masksave, nhunksave, 1);
	/* If it is releasable memory, mark it in the exec table */
	if (mode == 0)
		setbits(exectables[curpid + tabisave], masksave,
			nhunksave, 1);
	/* Set the end marker */
	mask <<= 1;
	if (!mask)
	{
		mask = 1;
		tabindex--;
	}
	endtable[tabindex] |= mask;
	if (malloc_debug_mode & 2)
	{
		lsetmem(&retval,(mode == 0) ? 0x99999999 : 0xbbbbbbbb,
			((nhunksave * HUNKSIZE) / sizeof(long)));
	}
out:	syssafe--;
	return retval;
}

/*
 * System call 64: Free a block of memory.
 * If bit 31 of 'addr' set, return size of the block, don't free it
 * If block not allocated, return negative.
 */

int _freemem(uint addr)
{
	register uint hunkindex;
	register uint mask, tabindex;
	register uint nhunks;
	register uint *table, *tablesave;
	uint tabisave, masksave;
	int wantsize;
	int retval;

	syssafe++;
	wantsize = 0;
	if (addr & TOPBIT)
	{
		wantsize = 1;
		addr &= ~TOPBIT;
	}

	if (addr & (HUNKSIZE-1))
	{
		conprintf("freemem: address 0x%x not a 32 byte multiple\r\n",
			addr);
		retval = BEC_BADARG;
		goto out;
	}
	hunkindex = (addr - TPALOC) / HUNKSIZE;
	if (hunkindex >= glbhunks)
	{
		conprintf("freemem: address 0x%x not in arena\r\n", addr);
		retval = BEC_BADARG;
		goto out;
	}
	tabindex = hunkindex / 32;
	mask = TOPBIT >> (hunkindex & 31);
	/* Check that it was allocated */
	if (!(glbtable[tabindex] & mask))
	{
		conprintf("freemem: address 0x%x not allocated\r\n", addr);
		retval = BEC_BADARG;
		goto out;
	}
	/* Count the number of blocks to remove */
	tabisave = tabindex;
	masksave = mask;
	nhunks = 1;
	table = endtable + tabindex;
	while (!(*table & mask))
	{
		nhunks++;
		mask >>= 1;
		if (!mask)
		{
			mask = TOPBIT;
			tabindex++;
			table++;
			tablesave = table;
			while (!*table)
				table++;
			nhunks += (table - tablesave) * 32;
			tabindex += table - tablesave;
		}
	}
	if (wantsize)
	{
		retval = nhunks * HUNKSIZE; /* Return the size in bytes */
		goto out;
	}
	endtable[tabindex] &= ~mask;	/* Clear the end marker */
	/* May need to start next search from here */
	tabindex = tabisave;
	mask = masksave;
	/* Clear the bits in the master table */
	setbits(glbtable + tabindex, mask, nhunks, 0);
	/* Clear the bits in the exec table */
	if (exectables[curpid])
	{	/* There is a live process running */

		/* User memory is mode 0, but freemem cannot distinguish:
		 * this freeing SHOULD have no effect if the memory was mode 1
		 */
		if (exectables[curpid][tabindex] & mask)
			setbits(exectables[curpid + tabindex], mask,
				nhunks, 0);
	}
	retval = 0;
	if (malloc_debug_mode & 4)
	{
		lsetmem(&addr,0xdddddddd,((nhunks * HUNKSIZE) / sizeof(long)));
	}

out:	syssafe--;
/*
	if (retval == BEC_BADARG)
		memfault(retval);
*/
	return retval;
}

/*
 * Get a new process ID. Search for an empty slot in the exec table,
 * If found it is the new PID. Allocate storage for the process memory
 * table
 */

int _newpid(void)
{
	register uint **_exectables = exectables;
	register int pid, apid;
	int retval;

	for (pid = 0; pid < MAXPIDS; pid++)
	{
		apid = (pid + lastpid) % MAXPIDS;
		if (!_exectables[apid])
		{
			pid = apid;
			goto got;
		}
	}
	return BEC_NOPID;
got:
	lastpid = pid + 1;
	retval = getmem1(glblongs * sizeof(**exectables));
	if (retval < 0)
		return retval;
	lsetmem(&retval, 0, glblongs);
	exectables[pid] = (uint *)retval;
	return pid;
}

/*
 * Clear a processes memory: do this by freeing the memory in
 * the master table
 */

int clearpidmem(int pid)
{
	register uint *tableptr;

	tableptr = exectables[pid];
	/* Invert all bits for anding */
	invlongs(tableptr, glblongs);
	andlongs(tableptr, glbtable, glblongs);
	andlongs(tableptr, endtable, glblongs);	/* Clear up end markers */
	dofreemem(tableptr);
/*
 * IT IS ESSENTIAL THAT NO FREES BE DONE BETWEEN THE NEXT STATEMENT AND THE
 * INCREMENTING OF curpid
 */
	exectables[pid] = 0;
	return 0;
}

/*
 * This function is called whenever an internal inconsistency error is detected.
 * We are passed the error code, a pointer 4 bytes beyond the return address
 * for the function which detected the error and two diagnostic arguments.
 */

void intfail(int ec,int *ppc, int arg1, int arg2)
{
	_spl7();
	conprintf("Internal error %d: call PC = $%x. %X %X\r\n",
			ec, *(ppc - 1), arg1, arg2);
	for ( ; ; )
		;
}

int dofreemem(void *addr)
{
	int *p;
	int retval;

	retval = FREEMEM(addr);
	if (retval < 0)
	{
		p = addr;
		p--;
		conprintf("freemem(0x%x)0x%x returns 0x%x\r\n",
				addr, *p, retval);
	}
	return retval;
}

/* Work out the process' memory usage */
int calcprocmem(void)
{
	return bitcount(exectables[curpid], glblongs * 32) * HUNKSIZE;
}

int dogetmem(int nbytes,int mode,char *who)
{
	int retval;

	if ((retval = GETMEM(nbytes, mode)) < 0)
		cantdo(who, retval, "allocate memory for");
	return retval;
}

int maybefree(void *addr)
{
	int retval;

	if(addr)
	{
		if ((retval = dofreemem(addr)) < 0)
			conprintf("MBF:0x%x", (int)(addr - 1));
		return retval;
	}
	return 0;
}

long getmem0(int n)
{ return GETMEM(n, 0); }

long getmem1(int n)
{ return GETMEM(n, 1); }

long getzmem0(int n)
{ return getzmem(n, 0); }

long getzmem1(int n)
{ return getzmem(n, 1); }

long getzmem(int n, int mode)
{
	long ret;

	ret = GETMEM(n, mode);
	if (ret > 0)
		clearmem((void *)ret, n);
	return ret;
}
