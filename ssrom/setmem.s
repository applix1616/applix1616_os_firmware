		.text

#include "asm68k.h"

/*
;Clear memory bytes
;clearmem(addr, nbytes)
*/
		.global	_clearmem, clearmem
clearmem:
_clearmem:
		move.l	4(sp),a0	/*; Address*/
		move.l	8(sp),d1	/*; Count*/
		moveq.l	#0,d0			/*; Value*/
		bra.b	btst0

/*
; Set memory bytes
;
; Usage: _bsetmem(address, val, count)
;	char *address;
;	long val;
;	long count;
*/

		.global	__bsetmem,_bsetmem
__bsetmem:
_bsetmem:
		move.l	4(sp),a0	/*; Address*/
		move.w	10(sp),d0	/*; Value*/
		move.l	12(sp),d1	/*; Count*/
		bra.b	btst0
bset2:
		swap	d1
bnot0:
		move.b	d0,(a0)+
btst0:
		dbf	d1,bnot0
		swap	d1		/*; Get upper 16 bits*/
		dbf	d1,bset2
		rts

/*
; Set memory words
;
; Usage: _wsetmem(address, val, count)
;	word *address;
;	long val;
;	long count;
*/

		.global	__wsetmem,_wsetmem
__wsetmem:
_wsetmem:
		move.l	4(sp),a0	/*; Address*/
		move.w	10(sp),d0	/*; Value*/
		move.l	12(sp),d1	/*; Count*/
		bra.b	wtst0
wset2:
		swap	d1
wnot0:
		move.w	d0,(a0)+
wtst0:
		dbf	d1,wnot0
		swap	d1
		dbf	d1,wset2
		rts

/*
; Set memory longwords
;
; Usage: _lsetmem(address, val, count)
;	long *address;
;	long val;
;	long count;
*/

		.global	__lsetmem,_lsetmem
_lsetmem:
__lsetmem:
		move.l	4(sp),a0	/*; Address*/
		move.l	8(sp),d0	/*; Value*/
		move.l	12(sp),d1	/*; Count*/
		bra.b	ltst0
lset2:
		swap	d1
lnot0:
		move.l	d0,(a0)+
ltst0:
		dbf	d1,lnot0
		swap	d1
		dbf	d1,lset2
		rts
