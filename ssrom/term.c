/*
 * The terminal function
 */

#include <types.h>
#include <syscalls.h>
#include <spspace.h>
#include <process.h>
#include <signal.h>
#include <files.h>

EXTERN int curpid;
EXTERN int stdin, stdout, stderr;

/*
 * Terminal mode. Echo until alt-^C hit
 */

rxproc(argc, argv)
char *argv[];
{
	int fd, ch;

	char buf[64];
	int blen, nc;

	fd = OPEN(argv[1], 1);
#if 0
	for ( ; ; )
	{
		PUTCHAR(ch = GETC(fd));
		if (argc > 2)
			PUTC(stderr, ch);
	}
#endif
	blen = 0;
	for ( ; ; )
	{
		if (blen < sizeof(buf) && (nc = SGETC(fd)))
		{
			while (nc-- && blen < sizeof(buf))
				buf[blen++] = GETC(fd);
		}
		else
		{
			WRITE(stdout, buf, blen);
			if (argc > 2)
				WRITE(stderr, buf, blen);
			blen = 0;
		}
	}
}

termkill()
{
	SIGSEND(LASTCHILD(curpid), SIGINT, 0);
	EXIT(0);
}

int term(char *channame,int echostderr)
char *channame;		/* The device drivers */
{
	char *argv[4];
	int fd;

	fd = OPEN(channame, 1);
	if (fd < 0)
		return cantopen(channame, fd);

	argv[0] = "<Rxproc>";
	argv[1] = channame;
	argv[2] = echostderr ? "-l" : (char *)0;
	argv[3] = (char *)0;
	SCHEDPREP(rxproc, argv, SCP_ISASYNC, BIN_STACKSPACE);
	SIGCATCH(termkill);

	for ( ; ; )
		PUTC(fd, rawgetchar());
}

/*
 * cio stuff
 */

int docio(int argc,int av1)
{
	register int ntoread, nread, val, retval;
	char ciobuf[256];

	ntoread = 1;
	val = av1;
	if (argc == 1)
	{
		val = 0x12345678;
		if (stdin >= NIODRIVERS || stdout >= NIODRIVERS)
			ntoread = sizeof(ciobuf);	/* To/from file */
	}

	for ( ; ; )
	{
		if ((nread = READ(stdin, ciobuf, ntoread)) <= 0)
			return retval = nread;
		if (*ciobuf == val)	/* Match specified end */
			return 0;
		if ((retval = WRITE(stdout, ciobuf, nread)) < 0)
			return retval;
	}
}
