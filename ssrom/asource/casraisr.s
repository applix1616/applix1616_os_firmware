/* psect	text */
	.text
/*
;
; Cassette read ISR
;
*/
		.global	_casr_aisr

_casr_aisr:	movem.l	d0/d1/d2/d3/a0/a1/a2/a3,-(sp)		/* Save scratch registers */
		jsr	casr_cisr
		movem.l	(sp)+,d0/d1/d2/d3/a0/a1/a2/a3
		rte
