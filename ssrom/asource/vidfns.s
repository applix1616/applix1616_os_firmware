		.text
/*;
; Calculate the address of the byte which holds the indicated pixel
;

; long
; pixelbyte(y, x)
; register unsigned short y, x;
; {
; 
; 	return (long)
; 		(
; 		((((y/4) * NCOLS) * 2 + x/4) & $1fff)
; 		+ ((y & 3) << 13) + (long)(vidram)
; 		);
; }
*/
		.global	_pixelbyte
_pixelbyte:	move.l	d2,-(sp)
		move.l	8(sp),d1	/*; y*/
		move.w	d1,d0		/*; d0 = y*/
		lsr.w	#2,d0		/*; y / 4*/
		mulu	#80,d0		/*; *NCOLS (extends d0)*/
		lsl.w	#1,d0		/*; * 2*/
		move.l	12(sp),d2	/*; x*/
		lsr.w	#2,d2
		add.w	d2,d0		/*; + x/4*/
		and.w	#0x1fff,d0	/*; & $1fff*/
		and.w	#3,d1		/*; (y & 3)*/
		move.w	#13,d2
		asl.w	d2,d1		/*; << 13*/
		or.w	d1,d0
		add.l	vidram,d0	/*; + vidram0*/
		move.l	(sp)+,d2
		rts

/*;
; Set a row of chars to passed value.
; Usage:
;	vidsetmem(offset, nwords, colour)
;	short offset, nwords, colour;
;
;
; a0 = vidram
; d0 = nwords, d1 = colour
;
*/
	.global	_vidsetmem
_vidsetmem:
		move.l	vidram,a0
		add.l	4(sp),a0	/*; Offset*/
		move.l	8(sp),d0	/*; nwords*/
		move.l	12(sp),d1	/*; colour*/
		bra.b	loope2
loop2:
		move.w	d1,0x2000(a0)
		move.w	d1,0x4000(a0)
		move.w	d1,0x6000(a0)
		move.w	d1,0x00a0(a0)
		move.w	d1,0x20a0(a0)
		move.w	d1,0x40a0(a0)
		move.w	d1,0x60a0(a0)
		move.w	d1,(a0)+
loope2:		dbf	d0,loop2
		rts

/*;
; Video RAM/memory move function.
;
; Usage:
;	vmovline(offset, buf, nwords, mode)
;	short offset;
;	short *buf;
;	short nwords, mode;
;
; Mode = 0: swap memory & screen
; Mode = 1: screen -> memory
; Mode = 2: memory -> screen
;
; Registers:
; a0-a3: vidram0-vidram3
; a4: buf
; d1: offset, d2: offset2, d3: nwords, d4: mode, d5: temp
;
*/
	.global	_vmovline
_vmovline:	link	a6,#0
		movem.l	d2/d3/d4/d5/a2/a3/a4,-(sp)
		move.l	vidram,a0
		lea	0x2000(a0),a1
		lea	0x4000(a0),a2
		lea	0x6000(a0),a3
		move.l	8(a6),d1	/*; offset*/
		move.w	d1,d2
		add.w	#160,d2		/*; offset2*/
		move.l	12(a6),a4	/*; buf*/
		move.l	16(a6),d3	/*; nwords*/
		move.l	20(a6),d4	/*; mode*/
		bra	loope3
loop3:
		tst.w	d4		/*; Mode 0?*/
		bne	notm0
/*; Mode 0: swap*/
		move.w	(a4),d5		/*; Read from buffer*/
		move.w	0(a0,d1.w),(a4)+
		move.w	d5,0(a0,d1.w)	/*; Write to screen*/

		move.w	(a4),d5		/*; Read from buffer*/
		move.w	0(a0,d2.w),(a4)+
		move.w	d5,0(a0,d2.w)	/*; Write to screen*/

		move.w	(a4),d5		/*; Read from buffer*/
		move.w	0(a1,d1.w),(a4)+
		move.w	d5,0(a1,d1.w)	/*; Write to screen*/

		move.w	(a4),d5		/*; Read from buffer*/
		move.w	0(a1,d2.w),(a4)+
		move.w	d5,0(a1,d2.w)	/*; Write to screen*/

		move.w	(a4),d5		/*; Read from buffer*/
		move.w	0(a2,d1.w),(a4)+
		move.w	d5,0(a2,d1.w)	/*; Write to screen*/

		move.w	(a4),d5		/*; Read from buffer*/
		move.w	0(a2,d2.w),(a4)+
		move.w	d5,0(a2,d2.w)	/*; Write to screen*/

		move.w	(a4),d5		/*; Read from buffer*/
		move.w	0(a3,d1.w),(a4)+
		move.w	d5,0(a3,d1.w)	/*; Write to screen*/

		move.w	(a4),d5		/*; Read from buffer*/
		move.w	0(a3,d2.w),(a4)+
		move.w	d5,0(a3,d2.w)	/*; Write to screen*/

		bra	l3
notm0:		cmp.w	#1,d4
		bne	notm1
		move.w	0(a0,d1.w),(a4)+	/*; Screen -> memory*/
		move.w	0(a0,d2.w),(a4)+
		move.w	0(a1,d1.w),(a4)+	/*; Screen -> memory*/
		move.w	0(a1,d2.w),(a4)+
		move.w	0(a2,d1.w),(a4)+	/*; Screen -> memory*/
		move.w	0(a2,d2.w),(a4)+
		move.w	0(a3,d1.w),(a4)+	/*; Screen -> memory*/
		move.w	0(a3,d2.w),(a4)+
		bra	l3
notm1:		move.w	(a4)+,0(a0,d1.w)	/*; Memory -> screen*/
		move.w	(a4)+,0(a0,d2.w)
		move.w	(a4)+,0(a1,d1.w)	/*; Memory -> screen*/
		move.w	(a4)+,0(a1,d2.w)
		move.w	(a4)+,0(a2,d1.w)	/*; Memory -> screen*/
		move.w	(a4)+,0(a2,d2.w)
		move.w	(a4)+,0(a3,d1.w)	/*; Memory -> screen*/
		move.w	(a4)+,0(a3,d2.w)
l3:		addq.w	#2,d1
		addq.w	#2,d2
loope3:		dbf	d3,loop3
		move.l	a4,d0		/*; Return new pointer*/
		movem.l	(sp)+,d2/d3/d4/d5/a2/a3/a4
		unlk	a6
		rts

/*;
; Move a video line from one place to another.
;
; Usage:
;	vmove(foffset, toffset, nwords)
;	short foffset, toffset, nwords;
;
; Registers:
; a0: source, a1: destination, d0: nwords
;
*/
		.global	_vmove
_vmove:		
		move.l	vidram,a0
		move.l	a0,a1
		add.l	4(sp),a0	/*; foffset*/
		add.l	8(sp),a1	/*; toffset*/
		move.l	12(sp),d0	/*; nwords*/
		bra	loopend
loop:
		move.w	0x2000(a0),0x2000(a1)
		move.w	0x4000(a0),0x4000(a1)
		move.w	0x6000(a0),0x6000(a1)
		move.w	0x00a0(a0),0x00a0(a1)
		move.w	0x20a0(a0),0x20a0(a1)
		move.w	0x40a0(a0),0x40a0(a1)
		move.w	0x60a0(a0),0x60a0(a1)
		move.w	(a0)+,(a1)+
loopend:	dbf	d0,loop
		rts
