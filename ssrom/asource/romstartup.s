|
| ROM startup code
|
		.text
		.globl	start,_start,initsp,_asbootup

start:
_start:
initsp:
		.long	0x300		| Initial SP
		.long	_asbootup	| Entry point
