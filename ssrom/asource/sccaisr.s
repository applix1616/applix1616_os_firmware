		.text
/*;
; The SCC interrupt dispatcher.
;
; For each pending SCC interrupt call the routine pointed to by the following 
; vectors (in this order):
;
;	$140	;	Channel A receive
;	$144	;	Channel A transmit
;	$148	;	Channal A EXT/STAT
;	$14c	;	Channel B receive 
;	$150	;	Channel B transmit
;	$154	;	Channal B EXT/STAT
;
; The called code must preserve D3-D7 and A3-A6 and must return with an RTS. It
; must also clear the SCC interrupt flag
;
*/
	
.equ S_VECBASE,0x140			/*; The table of simulated SCC vectors*/
.equ SCC_ACONT,0x700004		/*; SCC channel A control register*/

		.global	_scc_aisr
_scc_aisr:	movem.l	d0/d1/d2/d3/d4/a0/a1/a2/a3/a4,-(sp)	/*; Save scratch regs*/
		move.b	SCC_ACONT,d0		/*; Clear SCC pointers*/
		move.b	#3,SCC_ACONT		/*; Prepare to read Interrupt Pending bit register*/
		move.b	SCC_ACONT,d4
		move.l	#S_VECBASE,a4		/*; Pointer to SCC vectors*/
		asl.b	#2,d4			/*; Skip top bits*/
vloop:		asl.b	#1,d4
		bcc.b	noint			/*; Branch if not pending*/
		move.l	(a4),a0			/*; Get the vector*/
		jsr	(a0)			/*; Go to the code*/
noint:		addq.l	#4,a4			/*; Point to next vector*/
		tst.b	d4			/*; Any more bits?*/
		bne.b	vloop
		movem.l	(sp)+,d0/d1/d2/d3/d4/a0/a1/a2/a3/a4
		rte

