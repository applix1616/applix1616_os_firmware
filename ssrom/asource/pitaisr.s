/*;
; Assembler front end to the timer interrupt code
; */

		.text
		.global	_vs_aisr
		.global	_syssafe,_syssp,_switchpending,_timeslice

_vs_aisr:	movem.l	d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6,-(sp)
		jsr	vs_isr
/* Is it time for a process switch? */
		subq.l	#1,timeslice
		bpl.b	notyet
		tst.l	syssafe
		bne.b	notsafe		/* ; System busy: can't switch*/
/*; Set up system stack, return to scheduler via rte to set spl0*/
		move.l	sp,d0		/* ; Return sp to scheduler */
		move.l	syssp,sp	/* ; Restore scheduler regs */
		movem.l	(sp)+,d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6
		rte			/* ; Back to scheduler at level 0 */

notsafe:	move.l	sp,switchpending /* ;Flag to systrap() (sp non-zero) */
notyet:		movem.l	(sp)+,d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6
		rte

