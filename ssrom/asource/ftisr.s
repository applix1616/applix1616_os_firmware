/*		psect	text
;
; The freetone() isr code.
;

;	ftisr()
;	{
;		*V_AREG ;= ECASWIRQ;
;		*V_AREG &= ~ECASWIRQ;		//* Reset IRQ 
;		*DACLATCH = dlval = *ftptr++;
;		if (ftptr == fttable + fttlen)
;			ftptr = fttable;	/* Wrap around 
;		if (!--ftlength)
;			dist1ints();		/* Finished 
;	}
;
*/
	.text
.equ DACLATCH,0x600081			/* DAC latch */
.equ V_AREG,0x700102			/* VIA A data register */
.equ dlval,0x305				/* DAC latch copy */

	.global	_ftisr,ftptr,fttable,fttlen,ftlength,syscall

_ftisr:		movem.l	d0/a0,-(a7)

		bset	#2,V_AREG
		bclr	#2,V_AREG
		move.l	ftptr,a0
		move.b	(a0)+,d0
		move.b	d0,dlval
		move.b	d0,DACLATCH
		move.l	a0,ftptr	/* Updated pointer */
		move.l	fttlen,d0
		add.l	fttable,d0
		cmp.l	d0,a0
		bne.b	nowrap
		move.l	fttable,ftptr
nowrap:		subq.l	#1,ftlength
		bne.b	moretogo

		movem.l	d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6,-(a7)
		move.l	#27,-(a7)		/* Disable interrupts */
		jsr	_syscall
		addq.l	#4,a7
		movem.l	(a7)+,d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6

moretogo:	movem.l	(a7)+,d0/a0
		rte
