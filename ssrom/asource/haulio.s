/*;	if (reading)
;	while (nblocks--)
;	{
;		if (timeout(BUS_AND_STAT, BS_DMAREQ, BS_DMAREQ, TIMEOUT))
;			return -1;
;		length = 512;
;		while (length--)
;			*ptr++ = *W_IP_DATA;
;	}
;	else similar with *W_OP_DATA = *ptr++;
*/
	.text
.equ SCSI_BASE, 0xffde00
.equ BUS_AND_STAT, SCSI_BASE+40
.equ W_IP_DATA,	SCSI_BASE+48+256
.equ W_OP_DATA,	SCSI_BASE+256
.equ BS_DMAREQ,	6		/*;Bit 6 (0x40)*/
.equ BERRVEC,	8

/*;
; haulio(nblocks|nbytes, address, mode)
; mode: 0 for multiple block write
;	1 for multiple block read
;	2 for multiple byte write
;	3 for multiple byte read
;
*/
	
/*; Set 'dints' to 1 if using 1:1 interleave */
.equ dints, 0

		.global	_haulio,_ssptr,_hioptr

_haulio:	link	a6,#0
		movem.l	d2/d3/d4/a2/a3/a4/a5,-(sp)

/*; Lock this process in!*/
		move.l	ssptr,a0
		addq.l	#1,(a0)

		.if	dints
		move	sr,d4
		move.w	d4,-(sp)
		or.w	#0x0200,d4	/*;Shut out VIA interrupts*/
		.endif

		move.l	BERRVEC,a4	/*;Save bus error vector*/
		move.l	#berr,BERRVEC
		move.l	sp,a5		/*;Save SP*/
		move.l	8(a6),d1	/*;Block/byte counter*/
		moveq.l	#BS_DMAREQ,d2	/*;Bit index*/
		moveq.l	#31,d3		/*;For d0 preload*/
		move.l	12(a6),a0	/*;address pointer*/
		move.l	#BUS_AND_STAT,a2 /*;Status register*/
		move.l	#500000,a3	/*;Preload for d0 in timeout routine*/

		move.l	#W_OP_DATA,a1	/*;Output address for write*/
		move.l	16(a6),d0	/*;Mode*/
		beq	whaulend	/*;block write*/
		cmp.l	#2,d0
		beq	ws		/*;byte write*/
		move.l	#W_IP_DATA,a1	/*;Input address for read*/
		cmp.l	#3,d0
		beq	rs		/*;byte read*/
		bra	rhaulend	/*;block read*/

rhaulloop:	move.l	a3,d0		/*;Good long timeout*/
rhaultout:	btst	d2,(a2)
		bne	rhaulit		/*;DRQ is asserted: go!*/
		subq.l	#1,d0
		bne	rhaultout
/*;Panic: No DRQ*/
		bra	haulfail

/*; (192 + 10) * 32 = 6464 cycles for 512 bytes
; 512 * 7,500,000 / 6464 = 594,059 bytes/sec*/
rhaulit:
		.if dints
		move.w	d4,sr
		.endif
		move.l	d3,d0
rhloop:		.rept	16		/*;12 * 16 = 192*/
		move.b	(a1),(a0)+
		.endr
		dbf	d0,rhloop	/*;10 cycles*/
rhaulend:	dbf	d1,rhaulloop	/*;Next sector*/

hauldone:	moveq.l	#0,d0
haulout:	move.l	a4,BERRVEC	/*;Restore Bus error vector*/

		.if 	dints
		move.w	(sp)+,d1
		move.w	d1,sr		/*; splx()*/
		.endif

		move.l	a0,hioptr
		movem.l	(sp)+,d2/d3/d4/a2/a3/a4/a5
		unlk	a6

/*; Let other processes fly*/
		move.l	ssptr,a0
		subq.l	#1,(a0)

		rts

haulfail:	moveq.l	#-1,d0
		bra.b	haulout

/*;
; Ditto for writing
;*/
whaulloop:	move.l	a3,d0		/*;Good long timeout*/
whaultout:	btst	d2,(a2)
		bne	whaulit		/*;DRQ is asserted: go!*/
		subq.l	#1,d0
		bne	whaultout
/*;Panic: No DRQ*/
		bra	haulfail

/*; Now write the byte out. The disk will not assert DRQ until it has written
; the byte, so wait for it to go*/
whaulit:	.if	dints
		move.w	d4,sr
		.endif
		move.b	(a0)+,(a1)
whaull2:	move.l	a3,d0		/*;Good long timeout*/
whault2:	btst	d2,(a2)
		bne	whaulit2	/*;DRQ is asserted: go!*/
		subq.l	#1,d0
		bne	whault2
/*;Panic: No DRQ*/
		bra	haulfail

/*; (192 + 10) * 32 = 6464 cycles for 512 bytes
; 512 * 7,500,000 / 6464 = 594,059 bytes/sec*/
whaulit2:	move.l	d3,d0
		bra	whaulit3
whloop:		move.b	(a0)+,(a1)	/*;12 * 16 = 192*/
whaulit3:	.rept	15
		move.b	(a0)+,(a1)
		.endr
		dbf	d0,whloop	/*;10 cycles*/
whaulend:	dbf	d1,whaulloop	/*;Next sector*/
		bra	hauldone

/*;
; Read (d1) bytes
;
;rs2:		if	dints
;		move.w	d4,sr
;		endif
;		move.l	a3,d0		;Timeout counter
;rs3:		btst	d2,(a2)
;		bne.b	rs4		;DRQ ready
;		subq.l	#1,d0
;		bne.b	rs3
;		bra	haulfail
;rs4:		move.b	(a1),(a0)+
;rsend:		dbf	d1,rs2
;		sub.l	#$10000,d1
;		bpl.b	rs2
;		bra	hauldone
*/

rs:		move.l	a3,d0		/*; Timeout counter*/
		bra.b	rsend
rs2:		btst	d2,(a2)		/*; DRQ there?*/
		beq.b	rsnodrq
		move.b	(a1),(a0)+
rsend:		dbf	d1,rs2
		move.l	a3,d0		/*; Reload timeout counter*/
		sub.l	#0x10000,d1
		bpl.b	rs2
		bra	hauldone
rsnodrq:	subq.l	#1,d0
		bpl.b	rs2
		bra	haulfail

/*;
; Write (d1) bytes
;
;ws2:		if	dints
;		move.w	d4,sr
;		endif
;		move.l	a3,d0		;Timeout counter
;ws3:		btst	d2,(a2)
;		bne.b	ws4		;DRQ ready
;		subq.l	#1,d0
;		bne.b	ws3
;		bra	haulfail
;ws4:		move.b	(a0)+,(a1)
;wsend:		dbf	d1,ws2
;		sub.l	#$10000,d1
;		bpl.b	ws2
;		bra	hauldone
*/
ws:		move.l	a3,d0
		bra.b	wsend
ws2:		btst	d2,(a2)
		beq.b	wsnodrq
		move.b	(a0)+,(a1)
wsend:		dbf	d1,ws2
		move.l	a3,d0		/*; Reload timeout counter*/
		sub.l	#0x10000,d1
		bpl.b	ws2
		bra	hauldone
wsnodrq:	subq.l	#1,d0
		bpl.b	ws2
		bra	haulfail

/*;
; Bus error encountered
;*/
berr:		move.l	#-2,d0
		move.l	a5,sp
		bra	haulout


	