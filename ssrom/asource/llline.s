/*		psect	text*/
	.text
/*;
; Low level line routine
;
; Usage: llline(xstart, ystart, xend, yend)
;
*/
	
/*; Local variable offsets */

.equ XSTART,8
.equ YSTART,12
.equ XEND,16
.equ YEND,20

.equ DELTAX,-8
.equ DELTAY,-12
.equ ADELTAX,-16
.equ ADELTAY,-20
.equ XSIGN,-24
.equ YSIGN,-28

/*;
; D1=YSTART (in loop), D2=XINC/YINC, D4=XACC/YACC, D6=MASK, D7=NSTEPS
; D6/16 = XSIGN, D6/17 = YSIGN, D0 = D5 = working, D3 = 2(4)
;
; A1 = 3(15), A2 = SPTR, A3 = 0x7f60, A4 = 0x2000, A5 = 0xc000(0xf000)
;
; Draw relative to the current window
;
; _rline(xstart, ystart, xend, yend): system call 52
; _line(xstart, ystart, xnd, yend): system call 53
;
*/
	.global	__rline,__line
	.global	_g_xsize,_g_ysize,_g_xstart,_g_ystart,_gfgcol
	.global	_cline,_vidram,_mode_640
	.global	_grtodo
__rline:
__line:
		jsr	grtodo

/*		;If any of the passed args are outside the current window, call
		;C line code. If any of the endpoints are negative, they are
		;outside the window. If any are > g_xsize (g_ysize), they are
		;outside the window. This checking is done unsigned, so we just
		;need to compare with gxsize (g_ysize) */
		move.l	g_xsize,d0
		cmp.l	4(sp),d0	/*;g_xsize - xstart*/
		bcs.b	outside		/*;xstart > g_xsize*/
		beq.b	outside		/*;xstart == g_xsize*/
		cmp.l	12(sp),d0	/*;gxsize - xend*/
		bcs.b	outside
		beq.b	outside

		move.l	g_ysize,d0
		cmp.l	8(sp),d0
		bcs.b	outside
		beq.b	outside
		cmp.l	16(sp),d0
		bcs.b	outside
		beq.b	outside
		bra.b	okline		/*;Within window*/
outside:	jmp	cline

okline:		/*;Offset the points for current window*/

		move.l	g_xstart,d0
		add.l	d0,4(sp)
		add.l	d0,12(sp)
		move.l	g_ystart,d0
		add.l	d0,8(sp)
		add.l	d0,16(sp)

		link	a6,#-64		/*;Local vars*/
		movem.l	d2/d3/d4/d5/d6/d7/a2/a3/a4/a5,-(sp)

		move.l	#0x7f60,a3
		move.l	#0x2000,a4

/*;Set up the left and right aligned bit mask initialisation values. In 640 mode
; they are 3 & $c000. In 320 mode they are 15 & $f000*/
		tst.w	mode_640
		beq.b	in320		/*;Branch if 320 mode*/
		move.l	#3,a1
		move.l	#0xc000,a5
		moveq.l	#2,d3		/*;Interpixel shift distance*/
		bra	mgoon
in320:		move.l	#15,a1
		move.l	#0xf000,a5
		moveq.l	#4,d3
mgoon:
		moveq.l	#0,d6
		move.l	XEND(a6),d0	/*;deltax = xend - xstart*/
		sub.l	XSTART(a6),d0
		bpl.b	dxispos
		bset	#16,d6		/*;Get sign of delta-x*/
dxispos:	move.l	d0,DELTAX(a6)

		move.l	YEND(a6),d0	/*;deltay = yend - ystart*/
		sub.l	YSTART(a6),d0
		bpl.b	dyispos
		bset	#17,d6
dyispos:	move.l	d0,DELTAY(a6)

		move.l	DELTAX(a6),d0	/*;adeltax = (deltax<0)?-deltax : deltax;*/
		bpl	dxplus
		neg.l	d0
dxplus:		move.l	d0,ADELTAX(a6)

		move.l	DELTAY(a6),d0	/*;adeltay = (deltay < 0) ? -deltay : deltay;*/
		bpl	dyplus
		neg.l	d0
dyplus:		move.l	d0,ADELTAY(a6)

		move.l	ADELTAY(a6),d0	/*;if (adeltay >= adeltax)*/
		cmp.l	ADELTAX(a6),d0	/*;    d0 - ADELTAX(a6)*/
		bcs	else1
		move.l	ADELTAY(a6),d7	/*;    nsteps = adelaty*/
		beq	end_if1		/*; We have dy>=dx, dy = 0, so 0 length line*/

		move.l	ADELTAX(a6),d2	/*;    xinc = (adeltax * 32768 + 16384) / nsteps;*/
		swap	d2
		lsr.l	#1,d2
		add.l	#16384,d2
		move.l	d7,d0
		divu	d0,d2
		swap	d2
		move.w	#32768,d2	/*;yinc = 32768*/
		bra	end_if1
else1:
		move.l	ADELTAX(a6),d7	/*;    nsteps = adeltax;*/

		move.l	ADELTAY(a6),d2	/*;    yinc = (adeltay * 32768 + 16384) / nsteps;*/
		swap	d2
		lsr.l	#1,d2
		add.l	#16384,d2
		move.l	d7,d0
		divu	d0,d2
		swap	d2
		move.w	#32768,d2	/*;xinc = 32768*/
		swap	d2
end_if1:
		addq.l	#1,d7		/*;nsteps++*/
/*; sptr = ((ystart/4) * 160 + xstart/4(2) + ((ystart&3) << 13) + vidram) & ~1*/
		move.l	YSTART(a6),d0
		lsr.l	#2,d0		/*;ystart / 4*/
		mulu	#160,d0		/*;           * 160*/
		move.l	XSTART(a6),d1
		lsr.l	#1,d1		/*;xstart / 2*/
		tst.w	mode_640
		beq.b	cin320		/*;Branch if 320 mode*/
		lsr.l	#1,d1		/*;xstart / 2*/
cin320:
		add.l	d1,d0		/*; Accumulate in D0*/
		move.l	YSTART(a6),d1
		and.l	#3,d1		/*;ystart & 3*/
		swap	d1		/*;<<16*/
		lsr.l	#3,d1		/*;<<13*/
		add.l	d1,d0		/*;Accumulate*/
		add.l	vidram,d0
		bclr	#0,d0
		move.l	d0,a2

		move.w	a5,d6
		move.l	XSTART(a6),d0
		tst.w	mode_640
		beq.b	min320
		/*;Initial mask value for 640 mode*/
		and.l	#7,d0		/*;mask = $c000 >> ((xstart & 7) * 2);*/
		add.l	d0,d0
		lsr.w	d0,d6
		bra.b	mingoon
		/*;Initial mask value for 320 mode*/
min320:		and.l	#3,d0		/*;mask = $f000 >> ((xstart & 3) * 4);*/
		lsl.b	#2,d0
		lsr.w	d0,d6
mingoon:
		moveq.l	#0,d4		/*; xacc = yacc = 0;*/
		move.l	YSTART(a6),d1

/*;
; Everything is set up: call the appropriate low level code
;
*/
		.global	_curpmode
		moveq.l	#0,d0
		move.w	curpmode,d0
		beq	wwhileend	/*;Write through*/
		subq.l	#1,d0
		beq	owhileend	/*;Or*/
		subq.l	#1,d0
		beq	awhileend	/*;And*/
		bra	xwhileend	/*;Xor*/

/*;Write through mode*/
wwhile:
		move.w	(a2),d0
		not.w	d6
		and.w	d6,d0		/*;Clear our bits*/
		not.w	d6
		move.w	gfgcol,d5	/*;colour*/
		and.w	d6,d5		/*;Isolate these bits*/
		or.w	d5,d0
		move.w	d0,(a2)

		add.l	d2,d4		/*;    xacc += xinc; yacc += yinc*/
		btst	#31,d4		/*;    if (xacc >= 32768)*/
		beq.b	wxaccnov
		bclr	#31,d4		/*;      xacc -= 32768*/
		btst	#16,d6		/*;      if (deltax < 0)*/
		beq.b	wdxpos
		lsl.w	d3,d6		/*;        mask <<= 2(4)*/
		bne	wmaskn0a	/*;        if (!mask)*/
		move.w	a1,d6		/*;          mask = 3(15)*/
		subq.l	#2,a2		/*;          sptr--*/
wmaskn0a:	bra.b	wendifdx	/*;      else*/
wdxpos:		lsr.w	d3,d6		/*;        mask >>= 2(4)*/
		bne.b	wmaskn0b	/*;        if (!mask)*/
		move.w	a5,d6		/*;          mask = $c000($f000)*/
		addq.l	#2,a2		/*;          sptr++*/
wmaskn0b:
wendifdx:
wxaccnov:
		btst	#15,d4		/*;    if (yacc >= 32768)*/
		beq.b	wyaccnov
		bclr	#15,d4		/*;      yacc -= 32768*/
		btst	#17,d6		/*;      if (deltay < 0)*/
		beq.b	wdypos
		sub.l	a4,a2		/*;        sptr -= $1000*/
		move.b	d1,d0		/*;        if (!(ystart & 3))*/
		and.b	#3,d0
		bne.b	wystn0a
		add.l	a3,a2		/*;          sptr += $3fb0*/
wystn0a:	subq.w	#1,d1		/*;        ystart--*/
		bra.b	wendifdy	/*;      else*/
wdypos:		add.l	a4,a2		/*;        sptr += $1000*/
		addq.w	#1,d1		/*;        ystart++*/
		move.b	d1,d0		/*;        if (!(ystart & 3))*/
		and.b	#3,d0
		bne.b	wystn0b
		sub.l	a3,a2		/*;          sptr -= $3fb0*/
wystn0b:
wendifdy:
wyaccnov:
wwhileend:	dbf	d7,wwhile
		bra	finish

/*;Or through mode*/
owhile:
		move.w	d6,d0
		and.w	gfgcol,d0	/*;color & mask*/
		or.w	d0,(a2)		/*;    *sptr ;= mask*/

		add.l	d2,d4		/*;    xacc += xinc; yacc += yinc*/
		btst	#31,d4		/*;    if (xacc >= 32768)*/
		beq.b	oxaccnov
		bclr	#31,d4		/*;      xacc -= 32768*/
		btst	#16,d6		/*;      if (deltax < 0)*/
		beq.b	odxpos
		lsl.w	d3,d6		/*;        mask <<= 2(4)*/
		bne	omaskn0a	/*;        if (!mask)*/
		move.w	a1,d6		/*;          mask = 3(15)*/
		subq.l	#2,a2		/*;          sptr--*/
omaskn0a:	bra.b	oendifdx	/*;      else*/
odxpos:		lsr.w	d3,d6		/*;        mask >>= 2(4)*/
		bne.b	omaskn0b	/*;        if (!mask)*/
		move.w	a5,d6		/*;          mask = $c000($f000)*/
		addq.l	#2,a2		/*;          sptr++*/
omaskn0b:
oendifdx:
oxaccnov:
		btst	#15,d4		/*;    if (yacc >= 32768)*/
		beq.b	oyaccnov
		bclr	#15,d4		/*;      yacc -= 32768*/
		btst	#17,d6		/*;      if (deltay < 0)*/
		beq.b	odypos
		sub.l	a4,a2		/*;        sptr -= $1000*/
		move.b	d1,d0		/*;        if (!(ystart & 3))*/
		and.b	#3,d0
		bne.b	oystn0a
		add.l	a3,a2		/*;          sptr += $3fb0*/
oystn0a:	subq.w	#1,d1		/*;        ystart--*/
		bra.b	oendifdy	/*;      else*/
odypos:		add.l	a4,a2		/*;        sptr += $1000*/
		addq.w	#1,d1		/*;        ystart++*/
		move.b	d1,d0		/*;        if (!(ystart & 3))*/
		and.b	#3,d0
		bne.b	oystn0b
		sub.l	a3,a2		/*;          sptr -= $3fb0*/
oystn0b:
oendifdy:
oyaccnov:
owhileend:	dbf	d7,owhile
		bra	finish

/*;And in mode*/
awhile:
		move.w	d6,d0
		and.w	gfgcol,d0	/*;mask & colour*/
		not.w	d0
		and.w	d0,(a2)		/*;    *sptr ;= mask*/

		add.l	d2,d4		/*;    xacc += xinc; yacc += yinc*/
		btst	#31,d4		/*;    if (xacc >= 32768)*/
		beq.b	axaccnov
		bclr	#31,d4		/*;      xacc -= 32768*/
		btst	#16,d6		/*;      if (deltax < 0)*/
		beq.b	adxpos
		lsl.w	d3,d6		/*;        mask <<= 2(4)*/
		bne	amaskn0a	/*;        if (!mask)*/
		move.w	a1,d6		/*;          mask = 3(15)*/
		subq.l	#2,a2		/*;          sptr--*/
amaskn0a:	bra.b	aendifdx	/*;      else*/
adxpos:		lsr.w	d3,d6		/*;        mask >>= 2(4)*/
		bne.b	amaskn0b	/*;        if (!mask)*/
		move.w	a5,d6		/*;          mask = $c000($f000)*/
		addq.l	#2,a2		/*;          sptr++*/
amaskn0b:
aendifdx:
axaccnov:
		btst	#15,d4		/*;    if (yacc >= 32768)*/
		beq.b	ayaccnov
		bclr	#15,d4		/*;      yacc -= 32768*/
		btst	#17,d6		/*;      if (deltay < 0)*/
		beq.b	adypos
		sub.l	a4,a2		/*;        sptr -= $1000*/
		move.b	d1,d0		/*;        if (!(ystart & 3))*/
		and.b	#3,d0
		bne.b	aystn0a
		add.l	a3,a2		/*;          sptr += $3fb0*/
aystn0a:	subq.w	#1,d1		/*;        ystart--*/
		bra.b	aendifdy	/*;      else*/
adypos:		add.l	a4,a2		/*;        sptr += $1000*/
		addq.w	#1,d1		/*;        ystart++*/
		move.b	d1,d0		/*;        if (!(ystart & 3))*/
		and.b	#3,d0
		bne.b	aystn0b
		sub.l	a3,a2		/*;          sptr -= $3fb0*/
aystn0b:
aendifdy:
ayaccnov:
awhileend:	dbf	d7,awhile
		bra	finish

/*;Xor in mode*/
xwhile:
		move.w	d6,d0
		and.w	gfgcol,d0	/*;mask & colour*/
		eor.w	d0,(a2)		/*;    *sptr ;= mask*/

		add.l	d2,d4		/*;    xacc += xinc; yacc += yinc*/
		btst	#31,d4		/*;    if (xacc >= 32768)*/
		beq.b	xxaccnov
		bclr	#31,d4		/*;      xacc -= 32768*/
		btst	#16,d6		/*;      if (deltax < 0)*/
		beq.b	xdxpos
		lsl.w	d3,d6		/*;        mask <<= 2(4)*/
		bne	xmaskn0a	/*;        if (!mask)*/
		move.w	a1,d6		/*;          mask = 3(15)*/
		subq.l	#2,a2		/*;          sptr--*/
xmaskn0a:	bra.b	xendifdx	/*;      else*/
xdxpos:		lsr.w	d3,d6		/*;        mask >>= 2(4)*/
		bne.b	xmaskn0b	/*;        if (!mask)*/
		move.w	a5,d6		/*;          mask = $c000($f000)*/
		addq.l	#2,a2		/*;          sptr++*/
xmaskn0b:
xendifdx:
xxaccnov:
		btst	#15,d4		/*;    if (yacc >= 32768)*/
		beq.b	xyaccnov
		bclr	#15,d4		/*;      yacc -= 32768*/
		btst	#17,d6		/*;      if (deltay < 0)*/
		beq.b	xdypos
		sub.l	a4,a2		/*;        sptr -= $1000*/
		move.b	d1,d0		/*;        if (!(ystart & 3))*/
		and.b	#3,d0
		bne.b	xystn0a
		add.l	a3,a2		/*;          sptr += $3fb0*/
xystn0a:	subq.w	#1,d1		/*;        ystart--*/
		bra.b	xendifdy	/*;      else*/
xdypos:		add.l	a4,a2		/*;        sptr += $1000*/
		addq.w	#1,d1		/*;        ystart++*/
		move.b	d1,d0		/*;        if (!(ystart & 3))*/
		and.b	#3,d0
		bne.b	xystn0b
		sub.l	a3,a2		/*;          sptr -= $3fb0*/
xystn0b:
xendifdy:
xyaccnov:
xwhileend:	dbf	d7,xwhile
		bra	finish

finish:
		movem.l	(sp)+,d2/d3/d4/d5/d6/d7/a2/a3/a4/a5
		unlk	a6
		jmp	grdone
