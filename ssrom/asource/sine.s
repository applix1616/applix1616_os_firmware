		.text
/*;
; Return the sine of the input
;
; Usage:
;
; int sine(val)
; long val;
;
; 'val' is 0-1023 for 0-2*PI
;
*/
		.global	__sine

__sine:		move.l	4(sp),d1
		clr.w	d0
		move.b	d1,d0		/*; Index into 1/4 wave table*/
		btst	#8,d1		/*; Is it the mirror image part?*/
		beq.b	indexok
		not.b	d0		/*; Reverse index*/
indexok:	move.l	#sinetab,a0
		move.b	0(a0,d0.w),d0	/*; Get value from table*/
		btst	#9,d1		/*; Is it the negative part?*/
		beq.b	signok
		neg.b	d0
signok:		ext.w	d0		/*; Return a signed sine*/
		ext.l	d0
		rts

