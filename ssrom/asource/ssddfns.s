		.text
/*;
; 1616 disk controller support functions
;*/

.equ ssddrxrdy,0xffffc9
.equ ssddtxrdy,0xffffcb
.equ ssdddata, 0xffffc1

/*; _rdblk(buffer) : grab 1024 bytes*/
		.global	__rdblk

__rdblk:	move.l	4(sp),a0	/*; Data buffer*/
		move.l	a2,d1		/*; Save A2*/
		move.l	#ssddrxrdy,a1	/*; Rx ready bit*/
		move.l	#ssdddata,a2	/*; Data register*/
		move.w	#1023,d0	/*; Block size - 1*/
rdloop:		tst.b	(a1)		/*; Data available?*/
		bpl.b	rdloop
		move.b	(a2),(a0)+	/*; Read it in*/
		dbf	d0,rdloop
		move.l	d1,a2		/*; Restore A2*/
		moveq.l	#0,d0		/*; No error*/
		rts

/*; _wrblk(buffer) : write 1024 bytes*/
		.global	__wrblk

__wrblk:
		move.l	4(sp),a0	/*; Data buffer*/
		move.l	a2,d1
		move.l	#ssddtxrdy,a1	/*; Tx ready bit*/
		move.l	#ssdddata,a2
		move.w	#1023,d0
wrloop:		tst.b	(a1)		/*; Ready to transmit?*/
		bpl.b	wrloop
		move.b	(a0)+,(a2)
		dbf	d0,wrloop
		move.l	d1,a2
		moveq.l	#0,d0
		rts

/*; _rdmove(from, to) : Move 1024 bytes, return 8 bit checksum*/

		.global	__rdmove
__rdmove:
		move.l	4(sp),a0	/*; from*/
		move.l	8(sp),a1	/*; to*/

		move.l	a0,d0		/*; Is either odd?*/
		move.l	a1,d1
		or.l	d1,d0
		btst	#0,d0
		beq.b	botheven

/*; Do it in bytes*/
		move.w	#1023,d0	/*; Count*/
rdml:
		move.b	(a0)+,(a1)+	/*; Move*/
		dbf	d0,rdml
docksm:
		move.l	4(sp),a0	/*; Find the even pointer*/
		move.l	a0,d0
		btst	#0,d0
		beq.b	a0even
		move.l	8(sp),a0	/*; Must be even!*/

a0even:
		moveq.l	#0,d1
		move.l	#255,d0
cksmloop:
		add.l	(a0)+,d1
		dbf	d0,cksmloop
gotcksm:
		moveq.l	#0,d0
		move.b	d1,d0
		lsr.l	#8,d1
		add.b	d1,d0
		lsr.l	#8,d1
		add.b	d1,d0
		lsr.l	#8,d1
		add.b	d1,d0
		rts

/*; Do it in longs*/
botheven:
		move.w	#255,d0
		moveq.l	#0,d1
rdml2:
		add.l	(a0),d1
		move.l	(a0)+,(a1)+
		dbf	d0,rdml2
		bra	gotcksm
