		.text
/*;
; Software screen scroll function
;
; Usage: scrollup(baseaddr, bgcol)
; baseaddr: start of video page
; bgcol: 16 bit fill value
;*/
		.global	_scrollup

_scrollup:
		move.l	4(sp),a0	/*;Screen start*/
		move.l	8(sp),d0	/*;Background colour*/
		movem.l	d2/d3/d4/d5/d6/d7/a2/a3/a4/a5/a6,-(sp)	/*;Save regs*/

		move.l	d0,-(sp)	/*;For later on*/
		move.l	a0,-(sp)

/*;Do the scroll now*/
		moveq.l	#23,d0		/*;Line counter*/
lloop:		move.l	d0,-(sp)
		add.l	#320,a0		/*;Start of source line*/
		move.l	a0,-(sp)
		bsr	movlin		/*;Move the line up*/
		move.l	(sp)+,a0
		move.l	(sp)+,d0
		dbf	d0,lloop


/*; Now clear the bottom line of the screen: 4 x 320 lines
; We clear 52 bytes at a time, so the 320 bytes requires 6 loops, with 8 bytes
; left over
*/
		move.l	(sp)+,a0	/*;Screen start address*/
		move.l	(sp)+,d0	/*;Fill colour*/
		move.w	d0,d1
		swap	d1
		move.w	d0,d1		/*;Replicate the 16 bits in d1*/
		add.l	#8000,a0	/*;Point to end of bottom line*/

		move.l	d1,d2
		move.l	d1,d3
		move.l	d1,d4
		move.l	d1,d5
		move.l	d1,d6
		move.l	d1,d7
		move.l	d1,a1
		move.l	d1,a2
		move.l	d1,a3
		move.l	d1,a4
		move.l	d1,a5
		move.l	d1,a6

		moveq.l	#5,d0
clrloop:	movem.l	d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6,-(a0)
		movem.l	d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6,0x2000(a0)
		movem.l	d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6,0x4000(a0)
		movem.l	d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6,0x6000(a0)
		dbf	d0,clrloop

/*;Clean up the last 8 bytes*/
		movem.l	d1/d2,-(a0)
		movem.l	d1/d2,0x2000(a0)
		movem.l	d1/d2,0x4000(a0)
		movem.l	d1/d2,0x6000(a0)

/*;All done!*/
		
		movem.l	(sp)+,d2/d3/d4/d5/d6/d7/a2/a3/a4/a5/a6	/*;Restore*/
		rts

/*;
; Low level code to move up one line
; Move 4 * 320 bytes from (a0) to -320(a0)
; The move is done 56 bytes at a time
;
; Move 5 times. (5 * 56 = 280), clear up the last 40 bytes afterwards
;*/
movlin:		moveq.l	#3,d0		/*;Loop 4 times*/
mloop:		move.l	d0,-(sp)

		movem.l	(a0)+,d0/d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6
		movem.l	d0/d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6,-376(a0)

		movem.l	(a0)+,d0/d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6
		movem.l	d0/d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6,-376(a0)

		movem.l	(a0)+,d0/d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6
		movem.l	d0/d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6,-376(a0)

		movem.l	(a0)+,d0/d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6
		movem.l	d0/d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6,-376(a0)

		movem.l	(a0)+,d0/d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6
		movem.l	d0/d1/d2/d3/d4/d5/d6/d7/a1/a2/a3/a4/a5/a6,-376(a0)

/*;Move the last 40 bytes*/
		movem.l	(a0)+,d1/d2/d3/d4/d5/a1/a2/a3/a4/a5
		movem.l	d1/d2/d3/d4/d5/a1/a2/a3/a4/a5,-360(a0)

/*;Move a0 onto next line*/
		add.l	#7872,a0	/*;$2000 - 320*/

		move.l	(sp)+,d0
		dbf	d0,mloop
		rts
