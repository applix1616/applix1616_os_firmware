/*	psect	text */
	.text
/*
 Graphics functions
*/

/*
 The system call 50 entry point

 _rset_pel(x, y, val)
*/
		.global	__rset_pel,plotfunc
__rset_pel:	move.l	plotfunc,a0
		jmp	(a0)

/*
 System call 51 entry point: plot relative to window

 set_pel(x, y, val)
*/
		.global	__set_pel,vidram
		.global	g_xsize,g_ysize,g_xstart,g_ystart
		.global	grtodo,grdone
	
__set_pel:	jsr	grtodo
		move.l	4(sp),d0
		cmp.l	g_xsize,d0	/* x - g_xsize */
		bcc.b	poutside	/*g_xsize <= x */
		move.l	8(sp),d0
		cmp.l	g_ysize,d0	/*y - g_ysize */
		bcc.b	poutside	/* g_ysize <= y */
		move.l	g_xstart,d0
		add.l	d0,4(sp)	/* x += g_xstartt */
		move.l	g_ystart,d0
		add.l	d0,8(sp)	/* y += g_ystartt */
		move.l	plotfunc,a0
		jmp	(a0)
poutside:	jmp	grdone		/* Clip */


/* The four 640 mode pixel write routines. */ 


/* Set a pixel in 'overwrite' mode */
		.global	_hwritepix
_hwritepix:	move.l	8(sp),d0	/* Y */
		move.l	d0,d1
		lsr.w	#2,d0		/* Y/4 */
		mulu	#160,d0
		add.l	vidram,d0
		and.w	#3,d1		/* Y&3 */
		swap	d1		/* << 16 */
		lsr.l	#3,d1		/* >> 3 */
		add.l	d1,d0		/*  Add it in */
		move.l	d0,a0
		move.l	4(sp),d0	/* X */
		move.l	d0,d1
		lsr.w	#2,d0
		add.l	d0,a0

		and.b	#3,d1		/* x & 3 */
		move.w	14(sp),d0	/* Get val */
		ror.b	#2,d0		/* Shift to bits 6&7 */
		and.w	#0x00c0,d0
		swap	d0
		move.w	#0xff3f,d0
		lsr.l	d1,d0
		lsr.l	d1,d0		/* Mask = ~$c0 >> (x&3)*2 */

		move.b	(a0),d1		/* Read the current data */
		and.b	d0,d1		/* Mask bits */
		swap	d0
		or.b	d0,d1
		move.b	d1,(a0)
		jmp	grdone

/* Set a pixel in 'or' mode */
		.global	_horpix
_horpix:	move.l	8(sp),d0	/* Y */
		move.l	d0,d1
		lsr.w	#2,d0		/* Y/4 */
		mulu	#160,d0
		add.l	vidram,d0
		and.w	#3,d1		/* Y&3 */
		swap	d1		/*  << 16 */
		lsr.l	#3,d1		/*  >> 3 */
		add.l	d1,d0		/*  Add it in */
		move.l	d0,a0
		move.l	4(sp),d0	/*  X */
		move.l	d0,d1
		lsr.w	#2,d0
		add.l	d0,a0

		and.b	#3,d1
		move.w	14(sp),d0	/* Get val */
		ror.b	#2,d0		/* Shift to bits 6&7 */
		and.w	#0x00c0,d0
		lsr.b	d1,d0		/* val >>= (x&3) */
		lsr.b	d1,d0		/* val >>= (x&3) */

		or.b	d0,(a0)		/* Write the data */
		jmp	grdone

/* Set a pixel in 'and' mode */
		.global	_handpix
_handpix:	move.l	8(sp),d0	/* Y */
		move.l	d0,d1
		lsr.w	#2,d0		/* Y/4 */
		mulu	#160,d0
		add.l	vidram,d0
		and.w	#3,d1		/* Y&3 */
		swap	d1		/*  << 16 */
		lsr.l	#3,d1		/* >> 3 */
		add.l	d1,d0		/*  Add it in */
		move.l	d0,a0
		move.l	4(sp),d0	/* X */
		move.l	d0,d1
		lsr.w	#2,d0
		add.l	d0,a0

		and.b	#3,d1
		move.w	14(sp),d0	/* Get val */
		ror.b	#2,d0		/* Shift to bits 6&7 */
		and.w	#0x00c0,d0
		lsr.b	d1,d0		/* val >>= (x&3) */
		lsr.b	d1,d0		/* val >>= (x&3) */

		not.b	d0		/* Invert the data */
		and.b	d0,(a0)		/* Clear bits from val */
		jmp	grdone

/* Set a pixel in 'xor' mode */
		.global	_hxorpix
_hxorpix:	move.l	8(sp),d0	/* Y */
		move.l	d0,d1
		lsr.w	#2,d0		/* Y/4 */
		mulu	#160,d0
		add.l	vidram,d0
		and.w	#3,d1		/* Y&3 */
		swap	d1		/* << 16 */
		lsr.l	#3,d1		/* >> 3 */
		add.l	d1,d0		/* Add it in */
		move.l	d0,a0
		move.l	4(sp),d0	/* X */
		move.l	d0,d1
		lsr.w	#2,d0
		add.l	d0,a0

		and.b	#3,d1
		move.w	14(sp),d0	/* Get val */
		ror.b	#2,d0		/* Shift to bits 6&7 */
		and.w	#0x00c0,d0
		lsr.b	d1,d0		/* val >>= (x&3) */
		lsr.b	d1,d0		/* val >>= (x&3) */

		eor.b	d0,(a0)		/* Invert data with val */
		jmp	grdone


/*
 The four 320 mode pixel write routines.

 Pixel address = vidram + (y/4 * 160) + (y&3)<<13 + x/2
 Pixel mask = $f0 >> ((x&1)*2)
 Pixel data = (data rotated right) >> ((x&1)*4)
*/

/* Set a pixel in 'write through' mode */
		.global	_lwritepix
_lwritepix:	move.l	8(sp),d0	/* Y */
		move.l	d0,d1
		lsr.w	#2,d0		/* Y/4 */
		mulu	#160,d0
		add.l	vidram,d0
		and.w	#3,d1		/* Y&3 */
		swap	d1		/* << 16 */
		lsr.l	#3,d1		/* >> 3 */
		add.l	d1,d0		/* Add it in */
		move.l	d0,a0
		move.l	4(sp),d0	/* X */
		move.l	d0,d1
		lsr.w	#1,d0
		add.l	d0,a0

		and.b	#1,d1
		lsl.b	#2,d1
		move.w	14(sp),d0	/* Get val */
		ror.b	#4,d0		/* Shift to bits 4,5,6&7 */
		and.w	#0x00f0,d0
		swap	d0
		move.w	#0xff0f,d0
		lsr.l	d1,d0		/* Mask = ~$f0 >> (x&1)*4 */

		move.b	(a0),d1		/* Read the current data */
		and.b	d0,d1		/* Mask bits */
		swap	d0
		or.b	d0,d1
		move.b	d1,(a0)
		jmp	grdone


/* Set a pixel in 'or' mode */
		.global	_lorpix
_lorpix:	move.l	8(sp),d0	/* Y */
		move.l	d0,d1
		lsr.w	#2,d0		/* Y/4 */
		mulu	#160,d0
		add.l	vidram,d0
		and.w	#3,d1		/* Y&3 */
		swap	d1		/* << 16 */
		lsr.l	#3,d1		/* >> 3 */
		add.l	d1,d0		/* Add it in */
		move.l	d0,a0
		move.l	4(sp),d0	/* X */
		move.l	d0,d1
		lsr.w	#1,d0
		add.l	d0,a0

		and.b	#1,d1
		lsl.b	#2,d1
		move.w	14(sp),d0	/* Get val */
		ror.b	#4,d0		/* Shift to bits 6&7 */
		and.w	#0x00f0,d0
		lsr.b	d1,d0		/* val >>= (x&1)*4 */

		or.b	d0,(a0)		/* Write the data */
		jmp	grdone

/* Set a pixel in 'and' mode */
		.global	_landpix
_landpix:	move.l	8(sp),d0	/* Y */
		move.l	d0,d1
		lsr.w	#2,d0		/* Y/4 */
		mulu	#160,d0
		add.l	vidram,d0
		and.w	#3,d1		/* Y&3 */
		swap	d1		/* << 16 */
		lsr.l	#3,d1		/* >> 3 */
		add.l	d1,d0		/* Add it in */
		move.l	d0,a0
		move.l	4(sp),d0	/* X */
		move.l	d0,d1
		lsr.w	#1,d0
		add.l	d0,a0

		and.b	#1,d1
		lsl.b	#2,d1
		move.w	14(sp),d0	/* Get val */
		ror.b	#4,d0		/* Shift to bits 6&7 */
		and.w	#0x00f0,d0
		lsr.b	d1,d0		/* val >>= (x&1)*4 */

		not.b	d0		/* Invert the data */
		and.b	d0,(a0)		/* Clear bits from val */
		jmp	grdone

/* Set a pixel in 'xor' mode */
		.global	_lxorpix
_lxorpix:	move.l	8(sp),d0	/* Y */
		move.l	d0,d1
		lsr.w	#2,d0		/* Y/4 */
		mulu	#160,d0
		add.l	vidram,d0
		and.w	#3,d1		/* Y&3 */
		swap	d1		/* << 16 */
		lsr.l	#3,d1		/*  >> 3 */
		add.l	d1,d0		/* Add it in */
		move.l	d0,a0
		move.l	4(sp),d0	/* X */
		move.l	d0,d1
		lsr.w	#1,d0
		add.l	d0,a0

		and.b	#1,d1
		lsl.b	#2,d1
		move.w	14(sp),d0	/* Get val */
		ror.b	#4,d0		/* Shift to bits 6&7 */
		and.w	#0x00f0,d0
		lsr.b	d1,d0		/* val >>= (x&1)*4 */

		eor.b	d0,(a0)		/* Invert data with val */
		jmp	grdone

/*
 The pixel reading functions
*/

/*
System call 55: Read pixel relative to current window. read_pel(x, y)
*/
		.global	__read_pel,g_xstart,g_ystart
	
__read_pel:	jsr	grtodo
		move.l	g_xstart,d0
		add.l	d0,4(sp)
		move.l	g_ystart,d0
		add.l	d0,8(sp)
		jsr	grdone
		/* Fall through */

/*
System call 54: Raw pixel read. rread_pel(x, y)
*/
		.global	__rread_pel,_mode_640
__rread_pel:	jsr	grtodo
		move.l	8(sp),d0	/* Y */
		move.l	d0,d1
		lsr.w	#2,d0		/* Y/4 */
		mulu	#160,d0
		add.l	vidram,d0
		and.w	#3,d1		/* Y&3 */
		swap	d1		/* << 16 */
		lsr.l	#3,d1		/* >> 3 */
		add.l	d1,d0		/* Add it in */
		move.l	d0,a0
		move.l	4(sp),d0	/* X */
		move.l	d0,d1
		tst.w	mode_640	/* Which mode? */
		beq	rin32		/* Branch if 320 mode */
/*
* In 640 mode:
* Pixel address = vidram + (y/4 * 160) + (y&3)<<13 + x/4
* Return value = (((data rotated 2) >> ((x&3)*2) & 3)
* 640 mode
*/
		lsr.l	#2,d0		/* x/4 */
		add.l	d0,a0

		and.b	#3,d1
		eor.b	#3,d1
		move.b	(a0),d0		/* Read the data */
		lsr.b	d1,d0
		lsr.b	d1,d0
		and.l	#3,d0
		jmp	grdone

/*
* In 320 mode:
* Pixel address = vidram + (y/4 * 160) + (y&3)<<13 + x/2
* Return value = ((x & 1) ? data : (data >> 4)) & 15
*/
rin32:		lsr.w	#1,d0		/* x/2 */
		add.l	d0,a0

		move.b	(a0),d0		/* The data */
		btst	#0,d1		/* x & 1 */
		bne.b	rnoshift
		lsr.b	#4,d0		/* Even X: shift top 4 bits to bits 0 - 3 */
rnoshift:	and.l	#15,d0
		jmp	grdone

/*
* Defunct graphics system calls
*/
		.global	__sgtexture,__sgbgcol
__sgtexture:				/* System call 58: Set texture */
__sgbgcol:				/* System call 57: Set background colour */
		rts
