		.text
/*;
; Make a system call from within C.
;
; Usage: syscall(call_no, p1, p2, p3, p4, p5)
;
; All values are longs
;*/

		.global	_syscall,trap7
_syscall:
trap7:
		movem.l	d2/a2,-(sp)
		movem.l	12(sp),d0/d1/d2/a0/a1/a2
		trap	#7		/*; Go to ROM handler*/
		movem.l	(sp)+,d2/a2
		rts

