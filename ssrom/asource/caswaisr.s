/* psect	text */
	.text
/*
;
; Cassette output ISR
;
*/
	
.equ VIABASE,0x700100
.equ V_AREG,VIABASE+2
.equ V_T1CL,VIABASE+8
.equ V_IFR,VIABASE+26
.equ CLRCASIRQ,5		/* Bit 5 */
.equ AMUXLATCH,0x600181

		.global	_casw_aisr,caswcptr
		.global	cwmask,casbufcnt,casbuf,castail,cwtemp
_casw_aisr:
		movem.l	d0/a0/a1,-(sp)		/* Save whatever */
		move.l	#V_AREG,a0
		bset	#2,(a0)			/* Clear the interrupt F/F */
		bclr	#2,(a0)
		move.l	caswcptr,a1			/* The current state pointer (*V_AREG in a0) */
		jmp	(a1)				/* Go to it */

cwexit:		movem.l	(sp)+,d0/a0/a1
		rte
/*
;
; State 0: do nothing
;
*/
		.global	_cws0
_cws0:
cws0:		bra	cwexit

/*
;
; State 1: in the high part of a 0. Set data low & goto state 2
;
*/
		.global	_cws1
_cws1:
cws1:		bclr	#CLRCASIRQ,(a0)
		move.l	#cws2,caswcptr
		bra	cwexit
/*
;
; State 2: in the low part of a 0 or a 1. Set the data high and get next bit,
; then go to appropriate state
; */
cws2:		bset	#CLRCASIRQ,(a0)
		move.w	cwmask,d0		/* Which bit are we up to? */
		lsr.w	#1,d0
		bne.b	morebits		/* This byte not exhausted */
		move.l	#casbuf,d0		/* Circular buffer */
		add.l	castail,d0
		move.l	d0,a0
		move.b	(a0),d0
		move.w	d0,cwtemp		/* Move data from buffer to working location */
		sub.w	#1,casbufcnt		/* Decrement buffer count */
		add.l	#1,castail		/* Circulate index */
		moveq.l	#15,d0			/* Mask for 16 byte buffer */
		and.l	d0,castail
		move.w	#0x80,d0		/* Mask top bit */

morebits:	move.w	d0,cwmask
		and.w	cwtemp,d0		/* What is this bit? */
		beq.b	tos1			/* If bit is a zero go to state 1 */
		move.l	#cws3,caswcptr
		bra.w	cwexit
tos1:		move.l	#cws1,caswcptr
		bra	cwexit

/*
;
; State 3: first high part of a 1. Go to state 4
; */
cws3:		move.l	#cws4,caswcptr
		bra	cwexit

/* ;
; State 4: second high part of a 1. Put output low and goto state 5
; */
cws4:		bclr	#CLRCASIRQ,(a0)
		move.l	#cws5,caswcptr
		bra	cwexit
/*
;
; State 5: First low part of a 1. Leave output low and goto state 2
; */
cws5:		move.l	#cws2,caswcptr
		bra	cwexit
