	.text
/*	;
*	; The character shapes are in a packed format: one bit per pixel
*	;
*/
	
		.global	_charset
_charset:
/* ; Character 0x00 */
	dc.b	0b00000000		/* dc.b	%00000000 */
	dc.b	0b00000000
	dc.b	0b00000000
	dc.b	0b00000000
	dc.b	0b00000000
	dc.b	0b00000000
	dc.b	0b00000000
	dc.b	0b00000000
/* ; Character 0x01 */
	dc.b	0b01111110
	dc.b	0b10000001
	dc.b	0b10100101
	dc.b	0b10000001
	dc.b	0b10111101
	dc.b	0b10011001
	dc.b	0b10000001
	dc.b	0b01111110
/* ; Character 0x02 */
	dc.b	0b01111110
	dc.b	0b11111111
	dc.b	0b11011011
	dc.b	0b11111111
	dc.b	0b11000011
	dc.b	0b11100111
	dc.b	0b11111111
	dc.b	0b01111110
/* ; Character 0x03 */
	dc.b	0b01101100
	dc.b	0b11111110
	dc.b	0b11111110
	dc.b	0b11111110
	dc.b	0b01111100
	dc.b	0b00111000
	dc.b	0b00010000
	dc.b	0b00000000
/* ; Character 0x04 */
		dc.b	0b00010000
		dc.b	0b00111000
		dc.b	0b01111100
		dc.b	0b11111110
		dc.b	0b01111100
		dc.b	0b00111000
		dc.b	0b00010000
		dc.b	0b00000000
/* ; Character 0x05 */
		dc.b	0b00111000
		dc.b	0b01111100
		dc.b	0b00111000
		dc.b	0b11111110
		dc.b	0b11111110
		dc.b	0b01111100
		dc.b	0b00111000
		dc.b	0b01111100
/*; Character 0x06 */
		dc.b	0b00010000
		dc.b	0b00010000
		dc.b	0b00111000
		dc.b	0b01111100
		dc.b	0b11111110
		dc.b	0b01111100
		dc.b	0b00111000
		dc.b	0b01111100
/*; Character 0x07 */
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00000000
/* ; Character 0x08 */
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11100111
		dc.b	0b11000011
		dc.b	0b11000011
		dc.b	0b11100111
		dc.b	0b11111111
		dc.b	0b11111111
/*; Character 0x09 */
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01000010
		dc.b	0b01000010
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/*; Character 0x0A*/
		dc.b	0b11111111
		dc.b	0b11000011
		dc.b	0b10011001
		dc.b	0b10111101
		dc.b	0b10111101
		dc.b	0b10011001
		dc.b	0b11000011
		dc.b	0b11111111
/*; Character 0x0B*/
		dc.b	0b00001111
		dc.b	0b00000111
		dc.b	0b00001111
		dc.b	0b01111101
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b01111000
/* Character 0x0C*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b01111110
		dc.b	0b00011000
/* Character 0x0D*/
		dc.b	0b00111111
		dc.b	0b00110011
		dc.b	0b00111111
		dc.b	0b00110000
		dc.b	0b00110000
		dc.b	0b01110000
		dc.b	0b11110000
		dc.b	0b11100000
/* Character 0x0E*/
		dc.b	0b01111111
		dc.b	0b01100011
		dc.b	0b01111111
		dc.b	0b01100011
		dc.b	0b01100011
		dc.b	0b01100111
		dc.b	0b11100110
		dc.b	0b11000000
/* Character 0x0F*/
		dc.b	0b10011001
		dc.b	0b01011010
		dc.b	0b00111100
		dc.b	0b11100111
		dc.b	0b11100111
		dc.b	0b00111100
		dc.b	0b01011010
		dc.b	0b10011001
/* Character 0x10*/
		dc.b	0b10000000
		dc.b	0b11100000
		dc.b	0b11111000
		dc.b	0b11111110
		dc.b	0b11111000
		dc.b	0b11100000
		dc.b	0b10000000
		dc.b	0b00000000
/* Character 0x11*/
		dc.b	0b00000010
		dc.b	0b00001110
		dc.b	0b00111110
		dc.b	0b11111110
		dc.b	0b00111110
		dc.b	0b00001110
		dc.b	0b00000010
		dc.b	0b00000000
/* Character 0x12*/
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b01111110
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b01111110
		dc.b	0b00111100
		dc.b	0b00011000
/* Character 0x13*/
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00000000
		dc.b	0b01100110
		dc.b	0b00000000
/* Character 0x14*/
		dc.b	0b01111111
		dc.b	0b11011011
		dc.b	0b11011011
		dc.b	0b01111011
		dc.b	0b00011011
		dc.b	0b00011011
		dc.b	0b00011011
		dc.b	0b00000000
/* Character 0x15*/
		dc.b	0b00111110
		dc.b	0b01100011
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b00111000
		dc.b	0b11001100
		dc.b	0b01111000
/* Character 0x16*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b01111110
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x17*/
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b01111110
		dc.b	0b00011000
		dc.b	0b01111110
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b11111111
/* Character 0x18*/
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b01111110
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x19*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b01111110
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x1A*/
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00001100
		dc.b	0b11111110
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x1B*/
		dc.b	0b00000000
		dc.b	0b00110000
		dc.b	0b01100000
		dc.b	0b11111110
		dc.b	0b01100000
		dc.b	0b00110000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x1C*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11000000
		dc.b	0b11000000
		dc.b	0b11000000
		dc.b	0b11111110
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x1D*/
		dc.b	0b00000000
		dc.b	0b00100100
		dc.b	0b01100110
		dc.b	0b11111111
		dc.b	0b01100110
		dc.b	0b00100100
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x1E*/
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b01111110
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x1F*/
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b01111110
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x20:*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x21: !*/
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x22: "*/
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b01001000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x23: #*/
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b11111110
		dc.b	0b01101100
		dc.b	0b11111110
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b00000000
/* Character 0x24: $*/
		dc.b	0b00010000
		dc.b	0b01111110
		dc.b	0b11010000
		dc.b	0b01111100
		dc.b	0b00010110
		dc.b	0b11111100
		dc.b	0b00010000
		dc.b	0b00000000
/* Character 0x25: %*/
		dc.b	0b00000000
		dc.b	0b11000110
		dc.b	0b11001100
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b01100110
		dc.b	0b11000110
		dc.b	0b00000000
/* Character 0x26: &*/
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b00111000
		dc.b	0b01110110
		dc.b	0b11011100
		dc.b	0b11001100
		dc.b	0b01110110
		dc.b	0b00000000
/* Character 0x27: '*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x28: (*/
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b00110000
		dc.b	0b00110000
		dc.b	0b00011000
		dc.b	0b00001100
		dc.b	0b00000000
/* Character 0x29: )*/
		dc.b	0b00110000
		dc.b	0b00011000
		dc.b	0b00001100
		dc.b	0b00001100
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b00000000
/* Character 0x2A: */
		dc.b	0b00000000
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b11111111
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x2B: +*/
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b01111110
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x2C: ,*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00110000
/* Character 0x2D: -*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x2E: .*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x2F: */
		dc.b	0b00000010
		dc.b	0b00000110
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b01100000
		dc.b	0b01000000
		dc.b	0b00000000
/* Character 0x30: 0*/
		dc.b	0b01111100
		dc.b	0b11000110
		dc.b	0b11001110
		dc.b	0b11011110
		dc.b	0b11110110
		dc.b	0b11100110
		dc.b	0b01111100
		dc.b	0b00000000
/* Character 0x31: 1*/
		dc.b	0b00011000
		dc.b	0b00111000
		dc.b	0b01011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x32: 2*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b00000110
		dc.b	0b00011100
		dc.b	0b00110000
		dc.b	0b01100010
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x33: 3*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b00000110
		dc.b	0b00011100
		dc.b	0b00000110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x34: 4*/
		dc.b	0b00011100
		dc.b	0b00111100
		dc.b	0b01101100
		dc.b	0b11001100
		dc.b	0b11111110
		dc.b	0b00001100
		dc.b	0b00011110
		dc.b	0b00000000
/* Character 0x35: 5*/
		dc.b	0b01111110
		dc.b	0b01100000
		dc.b	0b01111100
		dc.b	0b00000110
		dc.b	0b00000110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x36: 6*/
		dc.b	0b00011100
		dc.b	0b00110000
		dc.b	0b01100000
		dc.b	0b01111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x37: 7*/
		dc.b	0b01111110
		dc.b	0b01000110
		dc.b	0b00000110
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x38: 8*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x39: 9*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111110
		dc.b	0b00000110
		dc.b	0b00001100
		dc.b	0b00111000
		dc.b	0b00000000
/* Character 0x3A: */
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x3B: */
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00110000
/* Character 0x3C: <*/
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b01100000
		dc.b	0b00110000
		dc.b	0b00011000
		dc.b	0b00001100
		dc.b	0b00000000
/* Character 0x3D: =*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x3E: >*/
		dc.b	0b01100000
		dc.b	0b00110000
		dc.b	0b00011000
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b01100000
		dc.b	0b00000000
/* Character 0x3F: ?*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b00000110
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x40: @*/
		dc.b	0b01111100
		dc.b	0b11000110
		dc.b	0b11011110
		dc.b	0b11011110
		dc.b	0b11011110
		dc.b	0b11000000
		dc.b	0b01111100
		dc.b	0b00000000
/* Character 0x41: A*/
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01111110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00000000
/* Character 0x42: B*/
		dc.b	0b11111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b11111100
		dc.b	0b00000000
/* Character 0x43: C*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b11000000
		dc.b	0b11000000
		dc.b	0b11000000
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x44: D*/
		dc.b	0b11111000
		dc.b	0b01101100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01101100
		dc.b	0b11111000
		dc.b	0b00000000
/* Character 0x45: E*/
		dc.b	0b11111110
		dc.b	0b01100010
		dc.b	0b01101000
		dc.b	0b01111000
		dc.b	0b01101000
		dc.b	0b01100010
		dc.b	0b11111110
		dc.b	0b00000000
/* Character 0x46: F*/
		dc.b	0b11111110
		dc.b	0b01100010
		dc.b	0b01101000
		dc.b	0b01111000
		dc.b	0b01101000
		dc.b	0b01100000
		dc.b	0b11110000
		dc.b	0b00000000
/* Character 0x47: G*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b11000000
		dc.b	0b11000000
		dc.b	0b11001110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x48: H*/
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01111110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00000000
/* Character 0x49: I*/
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x4A: J*/
		dc.b	0b00011110
		dc.b	0b00001100
		dc.b	0b00001100
		dc.b	0b00001100
		dc.b	0b00001100
		dc.b	0b11001100
		dc.b	0b01111000
		dc.b	0b00000000
/* Character 0x4B: K*/
		dc.b	0b11000110
		dc.b	0b11001100
		dc.b	0b11011000
		dc.b	0b11110000
		dc.b	0b11011000
		dc.b	0b11001100
		dc.b	0b11000110
		dc.b	0b00000000
/* Character 0x4C: L*/
		dc.b	0b11110000
		dc.b	0b01100000
		dc.b	0b01100000
		dc.b	0b01100000
		dc.b	0b01100000
		dc.b	0b01100010
		dc.b	0b11111110
		dc.b	0b00000000
/* Character 0x4D: M*/
		dc.b	0b11000110
		dc.b	0b11101110
		dc.b	0b11111110
		dc.b	0b11010110
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b00000000
/* Character 0x4E: N*/
		dc.b	0b11000110
		dc.b	0b11100110
		dc.b	0b11110110
		dc.b	0b11011110
		dc.b	0b11001110
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b00000000
/* Character 0x4F: O*/
		dc.b	0b01111100
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b01111100
		dc.b	0b00000000
/* Character 0x50: P*/
		dc.b	0b11111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01111100
		dc.b	0b01100000
		dc.b	0b01100000
		dc.b	0b11110000
		dc.b	0b00000000
/* Character 0x51: Q*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01101110
		dc.b	0b00111100
		dc.b	0b00001110
		dc.b	0b00000000
/* Character 0x52: R*/
		dc.b	0b11111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01111100
		dc.b	0b01101100
		dc.b	0b01100110
		dc.b	0b11100110
		dc.b	0b00000000
/* Character 0x53: S*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100000
		dc.b	0b00111100
		dc.b	0b00000110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x54: T*/
		dc.b	0b01111110
		dc.b	0b01011010
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x55: U*/
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x56: V*/
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x57: W*/
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b11010110
		dc.b	0b11111110
		dc.b	0b11101110
		dc.b	0b11000110
		dc.b	0b00000000
/* Character 0x58: X*/
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b01101100
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b00000000
/* Character 0x59: Y*/
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x5A: Z*/
		dc.b	0b11111110
		dc.b	0b10000110
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b01100010
		dc.b	0b11111110
		dc.b	0b00000000
/* Character 0x5B: */
		dc.b	0b01111100
		dc.b	0b01110000
		dc.b	0b01110000
		dc.b	0b01110000
		dc.b	0b01110000
		dc.b	0b01110000
		dc.b	0b01111100
		dc.b	0b00000000
/* Character 0x5C: */
		dc.b	0b01000000
		dc.b	0b01100000
		dc.b	0b00110000
		dc.b	0b00011000
		dc.b	0b00001100
		dc.b	0b00000110
		dc.b	0b00000010
		dc.b	0b00000000
/* Character 0x5D: */
		dc.b	0b01111100
		dc.b	0b00011100
		dc.b	0b00011100
		dc.b	0b00011100
		dc.b	0b00011100
		dc.b	0b00011100
		dc.b	0b01111100
		dc.b	0b00000000
/* Character 0x5E: ^*/
		dc.b	0b00010000
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b11000110
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x5F: _*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111111
/* Character 0x60: `*/
		dc.b	0b00110000
		dc.b	0b00110000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0x61: a*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01111000
		dc.b	0b00001100
		dc.b	0b01111100
		dc.b	0b11001100
		dc.b	0b01110110
		dc.b	0b00000000
/* Character 0x62: b*/
		dc.b	0b11100000
		dc.b	0b01100000
		dc.b	0b01111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b11011100
		dc.b	0b00000000
/* Character 0x63: c*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100000
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x64: d*/
		dc.b	0b00011100
		dc.b	0b00001100
		dc.b	0b01111100
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b01110110
		dc.b	0b00000000
/* Character 0x65: e*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01111110
		dc.b	0b01100000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x66: f*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100000
		dc.b	0b11111000
		dc.b	0b01100000
		dc.b	0b01100000
		dc.b	0b11110000
		dc.b	0b00000000
/* Character 0x67: g*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01110110
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b01111100
		dc.b	0b00001100
		dc.b	0b11111000
/* Character 0x68: h*/
		dc.b	0b11100000
		dc.b	0b01100000
		dc.b	0b01101100
		dc.b	0b01110110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b11100110
		dc.b	0b00000000
/* Character 0x69: i*/
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00111000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x6A: j*/
		dc.b	0b00000110
		dc.b	0b00000000
		dc.b	0b00001110
		dc.b	0b00000110
		dc.b	0b00000110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
/* Character 0x6B: k*/
		dc.b	0b11100000
		dc.b	0b01100000
		dc.b	0b01100110
		dc.b	0b01101100
		dc.b	0b01111000
		dc.b	0b01101100
		dc.b	0b11100110
		dc.b	0b00000000
/* Character 0x6C: l*/
		dc.b	0b00111000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x6D: m*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11101100
		dc.b	0b11111110
		dc.b	0b11010110
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b00000000
/* Character 0x6E: n*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01011100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00000000
/* Character 0x6F: o*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x70: p*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11011100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01111100
		dc.b	0b01100000
		dc.b	0b11110000
/* Character 0x71: q*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01110110
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b01111100
		dc.b	0b00001100
		dc.b	0b00011110
/* Character 0x72: r*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11011100
		dc.b	0b01110110
		dc.b	0b01100000
		dc.b	0b01100000
		dc.b	0b11110000
		dc.b	0b00000000
/* Character 0x73: s*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00111110
		dc.b	0b01100000
		dc.b	0b00111100
		dc.b	0b00000110
		dc.b	0b01111100
		dc.b	0b00000000
/* Character 0x74: t*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b01111110
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011010
		dc.b	0b00001100
		dc.b	0b00000000
/* Character 0x75: u*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b01110110
		dc.b	0b00000000
/* Character 0x76: v*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x77: w*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b11010110
		dc.b	0b11111110
		dc.b	0b01101100
		dc.b	0b00000000
/* Character 0x78: x*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11000110
		dc.b	0b01101100
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b11000110
		dc.b	0b00000000
/* Character 0x79: y*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111110
		dc.b	0b00000110
		dc.b	0b01111100
/* Character 0x7A: z*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b01001100
		dc.b	0b00011000
		dc.b	0b00110010
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x7B: {*/
		dc.b	0b00001110
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b01110000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00001110
		dc.b	0b00000000
/* Character 0x7C: */
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x7D: }*/
		dc.b	0b01110000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00001110
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b01110000
		dc.b	0b00000000
/* Character 0x7E: ~*/
		dc.b	0b01110110
		dc.b	0b11011100
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000

		dc.b	0b00000000
		dc.b	0b00010000
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b11111110
		dc.b	0b00000000
/* Character 0x80 */
		dc.b	0b01111000
		dc.b	0b11001100
		dc.b	0b11000000
		dc.b	0b11001100
		dc.b	0b01111000
		dc.b	0b00011000
		dc.b	0b00001100
		dc.b	0b01111000
/* Character 0x81*/
		dc.b	0b00000000
		dc.b	0b11001100
		dc.b	0b00000000
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x82*/
		dc.b	0b00001110
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01111110
		dc.b	0b01100000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x83*/
		dc.b	0b01111110
		dc.b	0b11000011
		dc.b	0b00111100
		dc.b	0b00000110
		dc.b	0b00111110
		dc.b	0b01100110
		dc.b	0b00111111
		dc.b	0b00000000
/* Character 0x84*/
		dc.b	0b11001100
		dc.b	0b00000000
		dc.b	0b01111000
		dc.b	0b00001100
		dc.b	0b01111100
		dc.b	0b11001100
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x85*/
		dc.b	0b11100000
		dc.b	0b00000000
		dc.b	0b01111000
		dc.b	0b00001100
		dc.b	0b01111100
		dc.b	0b11001100
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x86*/
		dc.b	0b00110000
		dc.b	0b00110000
		dc.b	0b01111000
		dc.b	0b00001100
		dc.b	0b01111100
		dc.b	0b11001100
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x87*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100000
		dc.b	0b01100000
		dc.b	0b00111100
		dc.b	0b00000110
		dc.b	0b00011100
/* Character 0x88*/
		dc.b	0b01111110
		dc.b	0b11000011
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01111110
		dc.b	0b01100000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x89*/
		dc.b	0b01100110
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01111110
		dc.b	0b01100000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x8A*/
		dc.b	0b01110000
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01111110
		dc.b	0b01100000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x8B*/
		dc.b	0b01100110
		dc.b	0b00000000
		dc.b	0b00111000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x8C*/
		dc.b	0b01111100
		dc.b	0b11000110
		dc.b	0b00111000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x8D*/
		dc.b	0b01110000
		dc.b	0b00000000
		dc.b	0b00111000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x8E*/
		dc.b	0b11000110
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b11000110
		dc.b	0b11111110
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b00000000
/* Character 0x8F*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01111110
		dc.b	0b01100110
		dc.b	0b00000000
/* Character 0x90*/
		dc.b	0b00001110
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00110000
		dc.b	0b00111100
		dc.b	0b00110000
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x91*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01111111
		dc.b	0b00001100
		dc.b	0b01111111
		dc.b	0b11001100
		dc.b	0b01111111
		dc.b	0b00000000
/* Character 0x92*/
		dc.b	0b00111110
		dc.b	0b01101100
		dc.b	0b11001100
		dc.b	0b11111110
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b11001110
		dc.b	0b00000000
/* Character 0x93*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x94*/
		dc.b	0b00000000
		dc.b	0b01100110
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x95*/
		dc.b	0b00000000
		dc.b	0b01110000
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x96*/
		dc.b	0b01111000
		dc.b	0b11001100
		dc.b	0b00000000
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x97*/
		dc.b	0b00000000
		dc.b	0b11100000
		dc.b	0b00000000
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0x98*/
		dc.b	0b00000000
		dc.b	0b01100110
		dc.b	0b00000000
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111110
		dc.b	0b00000110
		dc.b	0b01111100
/* Character 0x99*/
		dc.b	0b11000011
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0x9A*/
		dc.b	0b01100110
		dc.b	0b00000000
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0x9B*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b01111110
		dc.b	0b11000000
		dc.b	0b11000000
		dc.b	0b01111110
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0x9C*/
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b01100100
		dc.b	0b11110000
		dc.b	0b01100000
		dc.b	0b11100110
		dc.b	0b11111100
		dc.b	0b00000000
/* Character 0x9D*/
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b01111110
		dc.b	0b00011000
		dc.b	0b01111110
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0x9E*/
		dc.b	0b11111000
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b11111010
		dc.b	0b11000110
		dc.b	0b11001111
		dc.b	0b11000110
		dc.b	0b11000111
/* Character 0x9F*/
		dc.b	0b00001110
		dc.b	0b00011011
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b11011000
		dc.b	0b01110000
/* Character 0xA0*/
		dc.b	0b00001110
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b00000110
		dc.b	0b00111110
		dc.b	0b01100110
		dc.b	0b00111111
		dc.b	0b00000000
/* Character 0xA1*/
		dc.b	0b00011100
		dc.b	0b00000000
		dc.b	0b00111000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0xA2*/
		dc.b	0b00000000
		dc.b	0b00001110
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0xA3*/
		dc.b	0b00000000
		dc.b	0b00011100
		dc.b	0b00000000
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0xA4*/
		dc.b	0b00000000
		dc.b	0b01111100
		dc.b	0b00000000
		dc.b	0b01111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00000000
/* Character 0xA5*/
		dc.b	0b01111110
		dc.b	0b00000000
		dc.b	0b01100110
		dc.b	0b01110110
		dc.b	0b01111110
		dc.b	0b01101110
		dc.b	0b01100110
		dc.b	0b00000000
/* Character 0xA6*/
		dc.b	0b00111100
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b00111110
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xA7*/
		dc.b	0b00011100
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00011100
		dc.b	0b00000000
		dc.b	0b00111110
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xA8*/
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b01100000
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00000000
/* Character 0xA9*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00111111
		dc.b	0b00110000
		dc.b	0b00110000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xAA*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111100
		dc.b	0b00001100
		dc.b	0b00001100
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xAB*/
		dc.b	0b11000011
		dc.b	0b11000110
		dc.b	0b11001100
		dc.b	0b11011110
		dc.b	0b00110011
		dc.b	0b01100110
		dc.b	0b11001100
		dc.b	0b00001111
/* Character 0xAC*/
		dc.b	0b11000011
		dc.b	0b11000110
		dc.b	0b11001100
		dc.b	0b11011011
		dc.b	0b00110111
		dc.b	0b01101111
		dc.b	0b11001111
		dc.b	0b00000011
/* Character 0xAD*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0xAE*/
		dc.b	0b00000000
		dc.b	0b00110011
		dc.b	0b01100110
		dc.b	0b11001100
		dc.b	0b01100110
		dc.b	0b00110011
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xAF*/
		dc.b	0b00000000
		dc.b	0b11001100
		dc.b	0b01100110
		dc.b	0b00110011
		dc.b	0b01100110
		dc.b	0b11001100
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xB0*/
		dc.b	0b00100010
		dc.b	0b10001000
		dc.b	0b00100010
		dc.b	0b10001000
		dc.b	0b00100010
		dc.b	0b10001000
		dc.b	0b00100010
		dc.b	0b10001000
/* Character 0xB1*/
		dc.b	0b01010101
		dc.b	0b10101010
		dc.b	0b01010101
		dc.b	0b10101010
		dc.b	0b01010101
		dc.b	0b10101010
		dc.b	0b01010101
		dc.b	0b10101010
/* Character 0xB2*/
		dc.b	0b11011011
		dc.b	0b01110111
		dc.b	0b11011011
		dc.b	0b11101110
		dc.b	0b11011011
		dc.b	0b01110111
		dc.b	0b11011011
		dc.b	0b11101110
/* Character 0xB3*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xB4*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b11111000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xB5*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b11111000
		dc.b	0b00011000
		dc.b	0b11111000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xB6*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b11110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xB7*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xB8*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111000
		dc.b	0b00011000
		dc.b	0b11111000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xB9*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b11110110
		dc.b	0b00000110
		dc.b	0b11110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xBA*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xBB*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111110
		dc.b	0b00000110
		dc.b	0b11110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xBC*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b11110110
		dc.b	0b00000110
		dc.b	0b11111110
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xBD*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b11111110
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xBE*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b11111000
		dc.b	0b00011000
		dc.b	0b11111000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xBF*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xC0*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xC1*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xC2*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xC3*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011111
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xC4*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xC5*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b11111111
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xC6*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011111
		dc.b	0b00011000
		dc.b	0b00011111
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xC7*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110111
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xC8*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110111
		dc.b	0b00110000
		dc.b	0b00111111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xC9*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00111111
		dc.b	0b00110000
		dc.b	0b00110111
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xCA*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b11110111
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xCB*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b11110111
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xCC*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110111
		dc.b	0b00110000
		dc.b	0b00110111
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xCD*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xCE*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b11110111
		dc.b	0b00000000
		dc.b	0b11110111
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xCF*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xD0*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xD1*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xD2*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xD3*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00111111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xD4*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011111
		dc.b	0b00011000
		dc.b	0b00011111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xD5*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00011111
		dc.b	0b00011000
		dc.b	0b00011111
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xD6*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00111111
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xD7*/
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b11111111
		dc.b	0b00110110
		dc.b	0b00110110
		dc.b	0b00110110
/* Character 0xD8*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b11111111
		dc.b	0b00011000
		dc.b	0b11111111
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xD9*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b11111000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xDA*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00011111
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xDB*/
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11111111
/* Character 0xDC*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11111111
/* Character 0xDD*/
		dc.b	0b11110000
		dc.b	0b11110000
		dc.b	0b11110000
		dc.b	0b11110000
		dc.b	0b11110000
		dc.b	0b11110000
		dc.b	0b11110000
		dc.b	0b11110000
/* Character 0xDE*/
		dc.b	0b00001111
		dc.b	0b00001111
		dc.b	0b00001111
		dc.b	0b00001111
		dc.b	0b00001111
		dc.b	0b00001111
		dc.b	0b00001111
		dc.b	0b00001111
/* Character 0xDF*/
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b11111111
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xE0*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01110110
		dc.b	0b11011100
		dc.b	0b11001000
		dc.b	0b11011100
		dc.b	0b01110110
		dc.b	0b00000000
/* Character 0xE1*/
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01111100
		dc.b	0b01100110
		dc.b	0b01111100
		dc.b	0b01100000
		dc.b	0b01100000
/* Character 0xE2*/
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b01100110
		dc.b	0b01100000
		dc.b	0b01100000
		dc.b	0b01100000
		dc.b	0b01100000
		dc.b	0b00000000
/* Character 0xE3*/
		dc.b	0b00000000
		dc.b	0b11111110
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b00000000
/* Character 0xE4*/
		dc.b	0b01111110
		dc.b	0b01100110
		dc.b	0b00110000
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b01100110
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0xE5*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b11011000
		dc.b	0b11011000
		dc.b	0b11011000
		dc.b	0b01110000
		dc.b	0b00000000
/* Character 0xE6*/
		dc.b	0b00000000
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01111100
		dc.b	0b01100000
		dc.b	0b11000000
/* Character 0xE7*/
		dc.b	0b00000000
		dc.b	0b01110110
		dc.b	0b11011100
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0xE8*/
		dc.b	0b01111110
		dc.b	0b00011000
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00111100
		dc.b	0b00011000
		dc.b	0b01111110
/* Character 0xE9*/
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b11000110
		dc.b	0b11111110
		dc.b	0b11000110
		dc.b	0b01101100
		dc.b	0b00111000
		dc.b	0b00000000
/* Character 0xEA*/
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b11000110
		dc.b	0b11000110
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b11101110
		dc.b	0b00000000
/* Character 0xEB*/
		dc.b	0b00011100
		dc.b	0b00110000
		dc.b	0b00011000
		dc.b	0b01111100
		dc.b	0b11001100
		dc.b	0b11001100
		dc.b	0b01111000
		dc.b	0b00000000
/* Character 0xEC*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b11011011
		dc.b	0b11011011
		dc.b	0b01111110
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xED*/
		dc.b	0b00000110
		dc.b	0b00001100
		dc.b	0b01111110
		dc.b	0b11011011
		dc.b	0b11011011
		dc.b	0b01111110
		dc.b	0b01100000
		dc.b	0b11000000
/* Character 0xEE*/
		dc.b	0b00011100
		dc.b	0b00110000
		dc.b	0b01100000
		dc.b	0b01111100
		dc.b	0b01100000
		dc.b	0b00110000
		dc.b	0b00011100
		dc.b	0b00000000
/* Character 0xEF*/
		dc.b	0b00111100
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b01100110
		dc.b	0b00000000
/* Character 0xF0*/
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xF1*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b01111110
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0xF2*/
		dc.b	0b01100000
		dc.b	0b00110000
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b01100000
		dc.b	0b00000000
		dc.b	0b11111100
		dc.b	0b00000000
/* Character 0xF3*/
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b00011000
		dc.b	0b00001100
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00000000
/* Character 0xF4*/
		dc.b	0b00001110
		dc.b	0b00011011
		dc.b	0b00011011
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
/* Character 0xF5*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b11011000
		dc.b	0b11011000
		dc.b	0b01110000
/* Character 0xF6*/
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b01111110
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
/* Character 0xF7*/
		dc.b	0b00000000
		dc.b	0b01110110
		dc.b	0b11011100
		dc.b	0b00000000
		dc.b	0b01110110
		dc.b	0b11011100
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xF8*/
		dc.b	0b00111000
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b00111000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xF9*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xFA*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00011000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xFB*/
		dc.b	0b00001111
		dc.b	0b00001100
		dc.b	0b00001100
		dc.b	0b00001100
		dc.b	0b11101100
		dc.b	0b01101100
		dc.b	0b00111100
		dc.b	0b00011100
/* Character 0xFC*/
		dc.b	0b01111000
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b01101100
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xFD*/
		dc.b	0b00111000
		dc.b	0b00001100
		dc.b	0b00011000
		dc.b	0b00110000
		dc.b	0b00111100
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xFE*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00111100
		dc.b	0b00111100
		dc.b	0b00111100
		dc.b	0b00111100
		dc.b	0b00000000
		dc.b	0b00000000
/* Character 0xFF*/
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
		dc.b	0b00000000
