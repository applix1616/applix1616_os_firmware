
/*	psect	text */
	.text
;
/* Quickly put a char into video RAM.
; If bit 31 of fgmask is set, underline it
; If bit 30 of fgmask is set, invert the whole char
;
; Usage:
;	adrawvch(row, col, chsptr, fgmask, bgmask)
;	ushort row, col, *chsptr, fgmask, bgmask;
 
;	vrindex = (row * (NCOLS*2) + col) & $fff;
;
;	vidramvrindex = (*chsptr & fgmask) ; (~(*chsptr) & bgmask);
;	chsptr++;
;	vidramvrindex+$1000 = (*chsptr & fgmask) ; (~(*chsptr) & bgmask);
;	chsptr++;
;	vidramvrindex+$2000 = (*chsptr & fgmask) ; (~(*chsptr) & bgmask);
;	chsptr++;
;	vidramvrindex+$3000 = (*chsptr & fgmask) ; (~(*chsptr) & bgmask);
;	chsptr++;
;
;	vidramvrindex+$0050 = (*chsptr & fgmask) ; (~(*chsptr) & bgmask);
;	chsptr++;
;	vidramvrindex+$1050 = (*chsptr & fgmask) ; (~(*chsptr) & bgmask);
;	chsptr++;
;	vidramvrindex+$2050 = (*chsptr & fgmask) ; (~(*chsptr) & bgmask);
;	chsptr++;
;	vidramvrindex+$3050 = (*chsptr & fgmask) ; (~(*chsptr) & bgmask);
*/

/*
;
; Call adrawvch lots of times: charbang(ptr, count)
; We know no 'underline' attributes are set
		global	_charbang,_fg_col,_bg_col,_curs_x,_x_start
		global	_curs_y,_y_start,_chtab

; A0 = pointer to video RAM
; D4 = counter
; A4 = pointer to char string
; Stop on control char or counter expiry
; Return number of chars written
*/
	.global fg_col,bg_col
	.global y_start,x_start
	.global curs_y,curs_x
	.global vidram
	.global chtab
	
_charbang:
		movem.l	d2/d4/d5/a4,-(sp)
		move.l	20(sp),a4		/* ptr to characters */
		move.l	24(sp),d4

		move.w	fg_col,d0		/* If fgcol = $ffff & bgcol = 0, faster */
		cmp.w	#0xffff,d0		/* code used */
		bne	cantdofast
		tst.w	bg_col
		bne	cantdofast

/* Get pointer to video RAM into a0 */
		move.w	y_start,d1
		add.w	curs_y,d1		/* d1 = row */
		move.w	x_start,d2
		add.w	curs_x,d2		/*  d2 = col */
		mulu	#160,d1			/* d1 = row*160 */
		add.w	d2,d1			/* + col */
		and.w	#0x0fff,d1		/* d1 is now vrindex */
		asl.w	#1,d1			/* Word index */
		move.l	vidram,a0
		add.l	d1,a0			/* a1 now points to the start address */
		move.l	chtab,d5

zoomloop:	subq.l	#1,d4
		bmi	zoomdone
		moveq.l	#0,d0
		move.b	(a4)+,d0		/* The char to draw */
		cmp.b	#0x20,d0			/* d0 - $20. C set if d0 < $20 */
		bcc	zoomok
		subq.l	#1,a4
		bra	zoomdone
zoomok:
		lsl.l	#4,d0			/* Index into char table */
		add.l	d5,d0
		move.l	d0,a1
		move.w	(a1)+,(a0)
		move.w	(a1)+,0x2000(a0)
		move.w	(a1)+,0x4000(a0)
		move.w	(a1)+,0x6000(a0)
		move.w	(a1)+,0xa0(a0)
		move.w	(a1)+,0x20a0(a0)
		move.w	(a1)+,0x40a0(a0)
		move.w	(a1)+,0x60a0(a0)
		addq.w	#2,a0
		bra	zoomloop

zoomdone:	move.l	a4,d0			/* Calculate length. */
		sub.l	20(sp),d0
		movem.l	(sp)+,d2/d4/d5/a4
		rts


	
cantdofast:
		clr.l	d0
		move.w	bg_col,d0
		move.l	d0,-(sp)
		move.w	fg_col,d0
		move.l	d0,-(sp)
		subq.l	#4,sp			/* room for pointer to charset */
		move.w	curs_x,d0
		add.w	x_start,d0
		move.l	d0,-(sp)
		move.w	curs_y,d0
		add.w	y_start,d0
		move.l	d0,-(sp)
		move.l	chtab,d5
bangloop:	subq.l	#1,d4
		bmi	bangdone
		moveq.l	#0,d0
		move.b	(a4)+,d0
		cmp.b	#32,d0
		bcc	bangok
		subq.l	#1,a4
		bra	bangdone
bangok:
		lsl.l	#4,d0
		add.l	d5,d0
		move.l	d0,8(sp)		/* chtab pointer */
		bsr	_adrawvch
		addq.l	#1,4(sp)		/* col++ */
		bra	bangloop

bangdone:	add.w	#20,sp
		bra	zoomdone

		.global	_adrawvch,_vidram
_adrawvch:	link	a6,#0
		movem.l	d2/d3/d4/d5,-(sp)
		move.l	8(a6),d1		/* d1 = row */
		move.l	12(a6),d2		/* d2 = col */
		move.l	16(a6),a0		/* a0 = chsptr */
		move.l	20(a6),d3		/* d3 = fgmask */
		move.l	24(a6),d4		/* d4 = bgmask */

/*
; Bit 31: Underline
; Bit 30: Bold
; Bit 29: Italicise
; Bit 28: Subscript
; Bit 27: Superscript
*/
		.global	boldbuf
		btst	#30,d3			/* Bold? */
		beq.b	notbold
		move.l	(a0)+,boldbuf
		move.l	(a0)+,boldbuf+4
		move.l	(a0)+,boldbuf+8
		move.l	(a0)+,boldbuf+12
		moveq.l	#7,d0
		move.l	#boldbuf,a0
bloop:		move.w	(a0),d1
		lsr.w	#2,d1
		or.w	d1,(a0)+
		dbf	d0,bloop
		move.l	8(a6),d1		/* Restore */
		move.l	#boldbuf,a0

notbold:
		.global	subbuffer,itbuffer,_subtable
		move.l	d3,d0
		and.l	#0x38000000,d0		/* Italics or sub/super needed? */
		beq.w	notspcl
		btst	#29,d3			/* Italics? */
		beq	notital

/* Italicise the letter */
		move.l	(a0)+,itbuffer
		move.l	(a0)+,itbuffer+4
		move.l	(a0)+,itbuffer+8
		move.l	(a0)+,itbuffer+12
		move.l	#itbuffer,a0

		move.w	(a0),d0
		lsr.w	#2,d0			/* 0 ->2 */
		move.w	d0,(a0)+
		move.w	(a0),d0
		lsr.w	#2,d0			/* 1 ->1 */
		move.w	d0,(a0)+
		move.w	(a0),d0
		lsr.w	#2,d0			/* 2 ->1 */
		move.w	d0,(a0)+

		addq.l	#4,a0			/* 4, 5 */

		move.w	(a0),d0
		lsl.w	#2,d0			/* 6 <-1 */
		move.w	d0,(a0)+
		move.w	(a0),d0
		lsl.w	#2,d0			/* 7 <-1 */
		move.w	d0,(a0)+
		move.w	(a0),d0
		lsl.w	#2,d0			/* 7 <-1 */
		move.w	d0,(a0)+

		move.l	#itbuffer,a0

notital:	move.l	d3,d0
		and.l	#0x18000000,d0		/* Sub or super? */
		beq	notspcl

/*
; The scrunching mask is in bits 24-31 of d4
; Make the scrunched copy
*/
		movem.l	d4/a4,-(sp)
/* Get mask into d4 */
		lsr.l	#8,d4
		lsr.l	#8,d4
		lsr.l	#8,d4
/* Clear working buffer */
		move.l	#subbuffer,a4
		moveq.l	#7,d0
clrloop:	clr.l	(a4)+
		dbf	d0,clrloop
		move.l	#subbuffer,a4
		btst	#28,d3			/* If subscript, offset the pointer */
		beq.b	notsub
		addq.l	#6,a4		
notsub:		tst.b	d4			/* Do the scrunch */
		beq.b	scrunched
		lsl.b	#1,d4
		bcc.b	skip
		move.w	(a0),(a4)+		/* Copy a row */
skip:		addq.l	#2,a0
		bra.b	notsub
scrunched:
		movem.l	(sp)+,d4/a4
		move.l	#subbuffer,a0

notspcl:
		mulu	#160,d1		/* d1 = row*160 */
		add.w	d2,d1			/* + col */
		and.w	#0xfff,d1		/* d1 is now vrindex */
		asl.w	#1,d1			/* Word index */
		move.l	vidram,a1
		add.l	d1,a1			/* a1 now points to the start address */

/* Do we need to use bg & fg masks? */
		cmp.w	#0xffff,d3
		bne	usemasks
		tst.w	d4
		bne	usemasks

/* Don't need to use them */
		move.w	(a0)+,(a1)
		move.w	(a0)+,0x2000(a1)
		move.w	(a0)+,0x4000(a1)	/* Write it in */
		move.w	(a0)+,0x6000(a1)	/* Write it in */
		move.w	(a0)+,0x00a0(a1)	/* Write it in */
		move.w	(a0)+,0x20a0(a1)	/* Write it in */
		move.w	(a0)+,0x40a0(a1)	/* Write it in */
		bra	donemasks		/* Go test for underline */

usemasks:
		move.w	(a0)+,d5		/* d5 = *chsptr++ */
		move.w	d5,d0
		and.w	d3,d0			/* *chsptr & fgmask */
		not.w	d5
		and.w	d4,d5			/* (~*chsptr) & bgmask */
		or.w	d5,d0
		move.w	d0,(a1)		/* Write it in */

		move.w	(a0)+,d5		/* d5 = *chsptr++ */
		move.w	d5,d0
		and.w	d3,d0			/* *chsptr & fgmask */
		not.w	d5
		and.w	d4,d5			/* (~*chsptr) & bgmask */
		or.w	d5,d0
		move.w	d0,0x2000(a1)		/* Write it in */

		move.w	(a0)+,d5		/* d5 = *chsptr++ */
		move.w	d5,d0
		and.w	d3,d0			/* *chsptr & fgmask */
		not.w	d5
		and.w	d4,d5			/* (~*chsptr) & bgmask */
		or.w	d5,d0
		move.w	d0,0x4000(a1)		/* Write it in */

		move.w	(a0)+,d5		/* d5 = *chsptr++ */
		move.w	d5,d0
		and.w	d3,d0		; *chsptr & fgmask
		not.w	d5
		and.w	d4,d5			/* (~*chsptr) & bgmask */
		or.w	d5,d0
		move.w	d0,0x6000(a1)		/* Write it in */

		move.w	(a0)+,d5		/* d5 = *chsptr++ */
		move.w	d5,d0
		and.w	d3,d0		; *chsptr & fgmask
		not.w	d5
		and.w	d4,d5			/* (~*chsptr) & bgmask */
		or.w	d5,d0
		move.w	d0,0x00a0(a1)		/* Write it in */

		move.w	(a0)+,d5		/* d5 = *chsptr++ */
		move.w	d5,d0
		and.w	d3,d0		; *chsptr & fgmask
		not.w	d5
		and.w	d4,d5			/* (~*chsptr) & bgmask */
		or.w	d5,d0
		move.w	d0,0x20a0(a1)		/* Write it in */

		move.w	(a0)+,d5		/* d5 = *chsptr++ */
		move.w	d5,d0
		and.w	d3,d0		; *chsptr & fgmask
		not.w	d5
		and.w	d4,d5			/* (~*chsptr) & bgmask */
		or.w	d5,d0
		move.w	d0,0x40a0(a1)		/*  Write it in */

donemasks:
/* Test for underline mode */
		btst	#31,d3
		beq.b	nouline
		move.l	d3,d4			/* Move foreground mask to background */
nouline:	move.w	(a0)+,d5		/* d5 = *chsptr++ */
		move.w	d5,d0
		and.w	d3,d0			/* *chsptr & fgmask */
		not.w	d5
		and.w	d4,d5			/* (~*chsptr) & bgmask */
		or.w	d5,d0
		move.w	d0,0x60a0(a1)		/* Write it in */

		movem.l	(sp)+,d2/d3/d4/d5
		unlk	a6
		rts
