#ifndef IPCBLOCK_H
#define IPCBLOCK_H

extern int _blocktx(PROC *proc,char *blockaddr,long blocklen,long sig);
extern long _blockrx(PROC *proc,int mode);

#endif
