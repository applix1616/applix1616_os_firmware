/*
 * Screen editor
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <mondefs.h>
#include <sedit.h>
#include <blkerrcodes.h>
#include <options.h>
#include <envbits.h>
//#include <splfns.h>

uchar *mem_buf;		/* Start of text */
uchar *end_buf;		/* Points to null after last line */
int top_lin;		/* Line number for line at top of screen */
int bot_lin;		/* Number of line beyond last displayed */
int cur_lin;		/* Current */
uchar *top_lp;		/* Points to source for top line */
uchar *bot_lp;		/* Points to the string beyond the last displayed */
uchar *cur_lp;		/* Start of current line */
uchar *cur_ptr;		/* Current working pointer */
int cur_off;		/* cur_ptr - cur_lp (column offset of cursor) */
int save_coff;		/* Flag for preserving cur_off */
int bot_y;		/* Display line at which no text is displayed */
int top_y;		/* Display line of top displayed line */
int cur_y;		/* Current Y cursor pos */
int cur_x;		/* Current X cursor pos */
int lin_siz;		/* Number of screen lines occupied by a string */
int lin_dlen;		/* Number of screen uchars occupied by a string */
int scrnleft;		/* Used for preventing printing from going off the bottom */
int max_mem;		/* Maximum buffer size */
int lin_len;		/* Number of real uchars in a string */
int width;		/* Screen width */
int height;		/* Screen height */
int ibindex;		/* Index for insert buffer */
int nreps;		/* Number of repeats for search/substitute */
int nfound;		/* Number of successful searches/substitutes */
int dlscren;		/* Flag: Enable scrolling in dlscren() */
int modflag;		/* Flag: file modified */
uchar *blkstart;	/* Start of block */
uchar *blkend;		/* End of block */
uchar *newcur_ptr;	/* Used for keeping track of where we are */
uchar **markers;	/* Markers */
int blkset;		/* Flag: block is marked */
int hilit;		/* Flag: highlight mode is set */
int scr_top;		/* Current screen top */
int frozen;		/* Flag: screen frozen */
int quitflag;		/* Flag: time to exit */
int tabwidth;		/* Optional tab size */

uchar *file_name;
uchar *statmes;		/* Top left status message */

uchar *srchstr;		/* String to search for */
uchar *subsold;		/* Old string for substitute */
uchar *subsnew;		/* New string for substitute */

uchar _editsignalled;

ushort efilestat;	/* Edited file's status bits */
long euserid;		/* Edited file's user ID */

EXTERN int vsi_count;

uchar *getnext(), *get_prev();
uchar *fwd_fnw(), *rev_fnw();
//uchar *index();

int sedit(int argc,uchar **argv)
{
	register int fsize, retval, addr;
	int sighndl();

	uchar *_markers[10];		/* Markers */
	uchar _file_name[SEDLINESIZE];
	uchar _statmes[50];		/* Top left status message */
	uchar _srchstr[SEDLINESIZE];	/* String to search for */
	uchar _subsold[SEDLINESIZE];	/* Old string for substitute */
	uchar _subsnew[SEDLINESIZE];	/* New string for substitute */

	struct dir_entry dirent;

#ifdef ROM256
	if (argc < 2)
	{
		PRINTF("Usage: %s file tabwidth\r\n", argv0);
		return -1;
	}
#endif
	markers = _markers;
	file_name = _file_name;
	statmes = _statmes;
	srchstr = _srchstr;
	subsold = _subsold;
	subsnew = _subsnew;

	if (FILESTAT(argv[1], &dirent) < 0)
	{
		fsize = 0;
		efilestat = 0;
		euserid = -1;
	}
	else
	{
		if (dirent.statbits & DIRECTORY)
		{
			FPRINTF(STDERR, "%s is a directory\r\n", argv[1]);
			retval = BEC_ISDIR;
			goto err2;
		}
		fsize = dirent.file_size;
		efilestat = dirent.statbits & ~(LOCKED|BACKEDUP);
		euserid = dirent.uid;
	}
	fsize = (fsize + 0x8000) & ~0x3ff;
	retval = OPTION(OPT_MEMERRFLAG, 0);
	addr = getmem0(fsize);
	OPTION(OPT_MEMERRFLAG, retval);
	if (addr < 0)
		return nomemfor(argv[1]);
	mem_buf = (uchar *)addr;
	SIGCATCH(sighndl);
	_editsignalled = 0;
	retval = tosedit(argc, argv, fsize);
	FREEMEM(addr);
err2:	return retval;
}

sighndl()
{
	_editsignalled = 1;
}

editsignalled()
{
	uchar ret;

	ret = _editsignalled;
	_editsignalled = 0;
	return ret;
}

tosedit(argc, argv, edsize)
int argc;
char *argv;
int edsize;
{
	register int i;
	register int ch, nxtch;
	register uchar isdel;
	register uchar *tempptr;
	uchar ibuf[IBSIZE+50];	/* Insert buffer */
	uchar insch;		/* Flag: uchar was inserted */
	int oldlin_dlen;	/* Previous displayed line length */
	int nexty;		/* Where line after current starts */
	int whento;

	clr_scrn();
	PRINTF("%dK edit buffer\r\n", edsize / 1024);
	max_mem = edsize;
	sedinit();
	end_buf = mem_buf;
	cur_ptr = cur_lp = mem_buf;

	tabwidth = (argc == 3) ? atoi(argv[2]) : 8;
	if (!tabwidth)
		tabwidth++;

	movstr(file_name, argv[1], SEDLINESIZE);
	if (read_file(file_name, 0) < 0)
		blkswap(cur_ptr, 0, "\r\n", 2);
	modflag = ibindex = nxtch = save_coff = 0;
	redraw(mem_buf, 1, scr_top);

for (;;)
{
	if (ibindex == 0)	/* No chars to be inserted */
	{
		set_param(cur_lp);
		oldlin_dlen = lin_dlen;
		if (!save_coff)
			cur_off = cur_x;
		isdel = save_coff = 0;
	}

#ifdef original
	if (!nxtch && !SGETC(STDIN))	/* Avoid wasting time */
	{
		statline(cur_x, cur_y);
		*statmes = '\0';
	}
	gotoxy(cur_x, cur_y);
#else /*original*/

#define SLEEPLEN 50
	gotoxy(cur_x, cur_y);
	if (!nxtch)
	{
		whento = vsi_count + (envbits(ENVB_LOWBAUD) ? SLEEPLEN : 18);
		while (vsi_count < whento)
		{
			if (SGETC(STDIN))
				goto keyhit;
			SLEEP(1);
		}
		statline(cur_x, cur_y);
		*statmes = '\0';
	}
keyhit:
#endif /* original*/

	nexty = cur_y + oldlin_dlen/width + 1;
	ch = (nxtch) ? nxtch : rawgetc(STDIN);
	if (ch < 0)
	{	/* End of file. restore stdin */
		SET_SIP(OPEN("tty:", 1));
		sstatmes("EOF on input");
		continue;
	}

	nfound = nxtch = insch = 0;
	if (((ch >= ' ') && (ch != 0x7f)) || (ch == '\r') || (ch == '\t') || (ch == 0x10))
	{	/* An insertable character */
		sethil(cur_ptr);	/* Set highlight mode if necessary */
		if (ch == 0x10)		/* ^P */
		{
			sstatmes("^P");
			statline(cur_x, cur_y);
			ch = rawgetc(STDIN);
			statmes[0] = '\0';
			statline(cur_x, cur_y);
		}
		insch = 1;		/* A char has been added */
		ibuf[ibindex++] = ch;
		if (ch == '\r')
			ibuf[ibindex++] = '\n';
		else
		{
			if (ch == '\t')
			{
				do
				{
					opchar(' ');
				}
				while ((++cur_x) % tabwidth);
			}
			else
			{
				if (ch < ' ')
				{
						opchar('^');
						opchar(ch + 0x40);
						cur_x += 2;
				}
				else
				{
					opchar(ch);
					cur_x++;
				}
			}
			/* New length of this line */
			dlscren = 1;	/* Enable scrolling by draw_lin */
			lin_dlen = draw_lin(cur_ptr, cur_x, cur_y);
			dlscren = 0;
			if ((lin_dlen/width) > (oldlin_dlen/width))
			{	/* New line created */
				oldlin_dlen = lin_dlen;
				if ((cur_y + lin_dlen/width + 1) > height)
				{	/* draw_lin() caused a scroll */
					cur_y--;
					if (--top_y < scr_top)
					{	/* Top line pushed off */
						set_param(top_lp);
						top_y += lin_siz;
						top_lp = getnext(top_lp);
						top_lin++;
					}
				}
				else
				{	/* Use insert line here one day */
					draw(getnext(cur_lp), cur_lin+1, cur_y+oldlin_dlen/width+1);
				}
			}
			else
				oldlin_dlen = lin_dlen;
		}	/* ch == '\r' */
		cur_off = cur_x;
	}	/* ch insertable */

cntld:

/* If ch was a cr or ch non-insertable or buffer full, move the insert buffer in */
	if ((ch == '\r') || (!insch && ibindex) || (ibindex >= IBSIZE))
	{	/* Time to move the insert buffer in */
		blkswap(cur_ptr, 0, ibuf, ibindex);
		cur_ptr += ibindex;
		bot_lp += ibindex;
		ibindex = 0;
		if (ch == '\r')
		{	/* CR inserted */
			clr_eol();
			set_param(cur_lp);
			cur_y += lin_siz;	/* Move onto newly created line */
			cur_lp = getnext(cur_lp);
			if ((++cur_lin >= bot_lin++))
			{	/*zz This line is the last one */
				bot_lin = cur_lin;
				bot_lp = cur_lp;
				bot_y = cur_y;
				scrl_up();
			}
			else
			{	/* There are lines below this one */
				set_param(cur_lp);
				if ((cur_y + lin_siz) > nexty)
				{	/* It is necessary to move lines down */
					gotoxy(0, nexty);
					PRINTF(INSLIN);	/* Move rest down */
					if (++bot_y >= height)
					{
						set_param(bot_lp = get_prev(bot_lp));
						bot_lin--;
						bot_y -= lin_siz;
					}
				}
				draw_lin(cur_lp, 0, cur_y);
			}
		}
		if (!insch) set_param(cur_lp);
	}

	if (insch)
		goto skipsw;

	switch (ch)
	{
	case CTL('L'):
		nreps = 1;
		search();
		break;
	case CTL('N'):
		nreps = 1;
		substitute();
		break;
	case CTL('H'):
		isdel = 1;
	case CTL('S'):
		if (MASK(*--cur_ptr) == '\n')
			cur_ptr--;
		goto rev;
	case CTL('A'):
		cur_ptr = rev_fnw(cur_ptr, 0);
rev:		if (cur_ptr < mem_buf)
		{
			cur_ptr = mem_buf;
			if (isdel)	/* At top of file */
				goto revout;
		}
		if (cur_ptr < cur_lp)
		{	/* Backed up a line */
			cur_lin--;
			cur_lp = get_prev(cur_lp);
			set_param(cur_lp);
			cur_y -= lin_siz;
			if (cur_lp < top_lp)
				scrl_dn();
			set_param(cur_lp);	/* For delete() usage */
		}
		if (isdel)
			delete(1);	/* Handle DEL */
revout:		break;
	case CTL('U'):
		sstatmes("^U");
		statline(cur_x, cur_y);
		ch = rawgetc(STDIN);
		if (ch == CTL('U'))		/* Double ^U */
			ch = 0;
		else
			ch -= '0';
		if (putundobuf(ch) < 0)
			sstatmes("Can't put buffer");
		break;
	case CTL('V'):
		if (cur_ptr > cur_lp)
		{
			i = cur_ptr - cur_lp;	/* No. of uchars */
			cur_ptr = cur_lp;
			delete(i);
		}
		break;
	case CTL('D'):
		if (cur_ptr == end_buf)
			break;
		if (MASK(*cur_ptr) == '\r')
			cur_ptr += 2;
		else
			cur_ptr++;
		goto fwd;
	case CTL('F'):
		if (cur_ptr == end_buf)
			break;
		cur_ptr = fwd_fnw(cur_ptr, 0);
fwd:		if (cur_ptr == end_buf)
		{
			cur_ptr -= 2;
			break;
		}
		if	(
				(cur_ptr >= (tempptr = getnext(cur_lp))) &&
				!((cur_ptr == end_buf) && !endatcr())
			)
		{	/* Have moved to another line */
			cur_lp = tempptr;
			cur_lin++;
			cur_y += lin_siz;
			if (cur_lp == bot_lp)
				scrl_up();
			set_param(cur_lp);
		}
		break;
	case CTL('E'):
		cntrle();
		break;
	case CTL('R'):
		do
		{
			nxtch = i = 0;
			while ((i < mpaglen()) && (cur_lp != mem_buf))
			{
				cur_ptr = cur_lp = get_prev(cur_lp);
				set_param(cur_lp);
				cur_y -= lin_siz;
				cur_lin--;
				i += lin_siz;
			}
		}
		while (SGETC(STDIN) && ((nxtch = rawgetc(STDIN)) == CTL('R')));
		fix_cpos();
		redwhole();
		break;
	case CTL('X'):
		cntrlx(1);
		break;
	case CTL('C'):
		do
		{
			nxtch = i = 0;
			while ((i < mpaglen()) && ((tempptr = getnext(cur_ptr)) != end_buf))
			{
				cur_ptr = cur_lp = tempptr;
				cur_lin++;
				cur_y += lin_siz;
				set_param(cur_lp);
				i += lin_siz;
			}
		}
		while (SGETC(STDIN) && ((nxtch = rawgetc(STDIN)) == CTL('C')));
		fix_cpos();
		redwhole();
		break;
	case CTL('W'):
		if (cur_lin == (bot_lin-1))
			cntrle();
		else
			fix_cpos();
		if (top_lin != 1)
		{
			dn_scrl();
			top_y++;
			cur_y++;
			bot_y++;
			if (bot_y > height)
			{	/* Bottom line pushed off */
				bot_lin--;
				set_param(bot_lp = get_prev(bot_lp));
				bot_y -= lin_siz;
			}
			set_param(tempptr = get_prev(top_lp));
			if (lin_siz <= (top_y - scr_top))
			{	/* Can fit another line */
				top_y -= lin_siz;
				top_lin--;
				top_lp = tempptr;
				draw_lin(top_lp, 0, top_y);
			}
		}
		break;
	case CTL('Z'):
		if (cur_lin == top_lin)
			cntrlx(1);
		else
			fix_cpos();
		if (bot_lp != end_buf)
		{
			up_scrl(1);
			top_y--;
			cur_y--;
			bot_y--;
			if (top_y < scr_top)
			{	/* Top line pushed off */
				set_param(top_lp);
				top_y += lin_siz;
				top_lin++;
				top_lp = getnext(top_lp);
			}
			draw_lin(bot_lp, 0, bot_y);
			set_param(bot_lp);
			if ((bot_y + lin_siz) <= height)
			{
				bot_y += lin_siz;
				bot_lin++;
				bot_lp = getnext(bot_lp);
			}
		}
		break;
	case 0x1b:	/* ESC */
		redwhole();
		break;
	case CTL('K'):
		quitflag = 0;
		cntrlkq('K');
		if (quitflag)
			return 0;	/* The editor exit point */
		break;
	case CTL('Q'):
		cntrlkq('Q');
		break;
	case CTL('B'):
		tempptr = getnext(cur_ptr);
		if (cur_ptr == cur_lp)
			cur_ptr = tempptr - 2;
		else	/* Go to start */
			cur_ptr = cur_lp;
		break;
	case CTL('G'):
	case 0x7f:	/* DEL */
		delete(1);
		break;
	case CTL('T'):
		delete(fwd_fnw(cur_ptr) - cur_ptr, 0);
		break;
	case CTL('Y'):
		tempptr = getnext(cur_lp);
		if (tempptr == end_buf)
		{	/* Last line */
			cur_ptr = cur_lp;
			delete(tempptr - cur_lp - 2);
		}
		else
		{
			i = tempptr - cur_lp;
			predelete(cur_lp, i);
			blkswap(cur_lp, i, cur_lp, 0);
			cur_ptr = cur_lp;
			dellines(cur_y, lin_siz);
			bot_y -= lin_siz;
			bot_lp -= i;
			bot_lin--;
			if (cur_lin == bot_lin)
				scrl_up();
			else
			{
				set_param(tempptr = get_prev(bot_lp));
				draw(tempptr, bot_lin - 1, bot_y - lin_siz);
			}
		}
		break;
	case CTL('J'):
		if (SET_SIP(-1) < NIODRIVERS)
			cntrlx(0);
		break;
	default:
		bad_cmd();
		break;
	}
skipsw:
	continue;
}
}

/*
 * Do a control-X
 */

cntrlx(dofix)
{
	if (getnext(cur_ptr) != end_buf)
	{
		cur_ptr = cur_lp = getnext(cur_ptr);
		cur_lin++;
		cur_y += lin_siz;
		if (cur_lin == bot_lin)
			scrl_up();
		set_param(cur_lp);
	}
	if (dofix)
		fix_cpos();
}

/*
 * Do a control-E command
 */

cntrle()
{
	if (cur_lp != mem_buf)
	{
		cur_ptr = cur_lp = get_prev(cur_lp);
		set_param(cur_lp);
		cur_y -= lin_siz;
		cur_lin--;
		if (cur_lin < top_lin)
			scrl_dn();
		set_param(cur_lp);
	}
	fix_cpos();
}

/*
 * Control-K and Control-Q commands
 */

cntrlkq(korq)
uchar korq;
{
	register int i;
	register uchar ch;
	register int wantline;
	register int blksize;
	register uchar *tempptr;
	register uchar *first, *last;
	uchar *fname;
	static char filenotsaved[] = "File not saved";
	uchar rfile[SEDLINESIZE];
	uchar bufile[SEDLINESIZE];
	int cntargs;
	char *args[40];

#ifdef ROM256
	int bsssize;
	extern int end, edata;
	char *sptr;
#endif
	SPRINTF(statmes, "^%c ", korq);
	statline(2, STATUS);
	ch = rawgetc(STDIN);
	if (ch < ' ')
		ch |= 0x40;
	ch = toupper(ch);
	statmes[2] = ch;
	statline(cur_x, cur_y);

	if (korq == 'Q')
	{
		switch (ch)
		{
		case 'A':	/* Substitute */
			tgets(subsold, "Replace? ");
			if (!*subsold)
				break;
			tgets(subsnew, "  With? ");
			if (editsignalled())
				break;
			getnreps();
			if (editsignalled())
				break;
			substitute();
			break;
		case 'B':
			tempptr = blkstart;
			goto tomarker;
		case 'C':	/* To end of file */
			wantline = -1;
			goto toline;
		case 'D':	/* Forward 80 uchars */
			if ((cur_ptr += 80) > (end_buf - 2))
				cur_ptr = end_buf - 2;
			if (cur_ptr > (tempptr = getnext(cur_lp) - 2))
				cur_ptr = tempptr;
			break;
		case 'E':	/* To top of screen */
			cur_lp = top_lp;
			cur_lin = top_lin;
			cur_y = top_y;
			set_param(cur_lp);
			fix_cpos();
			break;
		case 'F':	/* Find */
			tgets(srchstr, "Locate? ");
			if (!*srchstr)
				break;
			getnreps();
			if (editsignalled())
				break;
			search();
			break;
		case 'G':	/* Go to line */
			wantline = tgetnum("Line? ");
toline:			if (!wantline)
				wantline = 1;
			if (wantline < 0)
				wantline = 1000000;	/* Look! */
			if (wantline <= top_lin)
			{	/* Search from front of file */
				cur_lin = 1;
				cur_lp = cur_ptr = mem_buf;
			}
			else
			{
				cur_lin = top_lin;
				cur_lp = cur_ptr = top_lp;
			}
			while ((wantline > cur_lin) && (cur_lp < end_buf))
			{
				cur_lp = getnext(cur_lp);
				cur_lin++;
			}
			if (cur_lp == end_buf)
			{
				cur_lp = get_prev(cur_lp);
				cur_lin--;
			}
			cur_ptr = cur_lp;
			newcurs();
			set_param(cur_lp);
			fix_cpos();
			break;
		case 'K':
			tempptr = blkend;
			goto tomarker;
		case 'R':	/* Start of file */
			wantline = 1;
			goto toline;
		case 'S':	/* Back 80 uchars */
			if ((cur_ptr -= 80) < cur_lp)
			{	/* Backed over a line */
				cur_ptr = cur_lp;
			}
			break;
		case 'U':	/* Review undo buffers */
			undoreview();
			break;
		case 'X':	/* Bottom of screen */
			cur_lin = bot_lin - 1;
			cur_lp = get_prev(bot_lp);
			set_param(cur_lp);
			cur_y = bot_y - lin_siz;
			fix_cpos();
			break;
		case 'Y':
			if (MASK(*cur_ptr) != '\r')
				delete(getnext(cur_ptr) - cur_ptr);
			break;
		default:
			if ((ch >= '0') && (ch <= '9'))
				tempptr = markers[ch - '0'];
			else
			{
				bad_cmd();
				goto out;
			}
tomarker:		if (!tempptr)
				sstatmes("No marker");
			else
			{
				cur_ptr = tempptr;
				redoptr();
				newcurs();
			}
out:			break;
		}
	}
	else if (korq == 'K')
	{	/* Control-K command */
		blksize = blkend - blkstart;
		newcur_ptr = cur_ptr;
		switch (ch)
		{
		case 'A':	/* Edit another file */
			tgets(rfile, "File to edit? ");
			args[0] = "edit";
			args[1] = (char *)0;	/* Prepare to exec edit */
			cntargs = SLICEARGS(rfile, args + 1, 1);
			cntargs++;		/* Account for leading "edit" */
			if (cntargs < 2)
				break;		/* Must edit one file! */
			gotoxy(0, height - 1);
			if (modflag)
				PRINTF("%s", filenotsaved);
			hil(0);
			clr_eol();
			prcrlf();
#ifndef ROM256
			if (EXECA(args) < 0)
				getret();	/* Fire off another copy of EDIT */
			else
				unfreeze();
#else
			/* Prepare to save ALL bss and then recur */
			bsssize = (long)&end - (long)&edata;
			i = OPTION(OPT_MEMERRFLAG, 0);
			sptr = (char *)getmem0(bsssize);
			OPTION(OPT_MEMERRFLAG, i);
			if ((int)sptr < 0)
				sstatmes("Insufficient memory");
			else
			{
				movmem(&edata, sptr, bsssize);	/* Saved */
				clearmem(&edata, bsssize);	/* Why not? */
				i = sedit(cntargs, args);
				movmem(sptr, &edata, bsssize);	/* Restored */
				FREEMEM(sptr);

				if (i < 0)
					getret();
				else
					unfreeze();
			}
#endif
			break;
		case 'B':	/* Mark block start */
			blkstart = cur_ptr;
			goto chkblk;
		case 'C':	/* Block copy */
			if (blkok() && blkcpy('C'))
				goto movend;
			break;
		case 'X':
		case 'D':	/* File write */
			first = mem_buf;
			last = end_buf;
			fname = file_name;
wrout:			tgetsn(fname, "Filename? ");
			if (*fname)
			{
				strcpy(bufile, fname);
				if (*fname != '>' && !envbits(ENVB_NOBAK))
				{
					strcat(bufile, ".bak");
					UNLINK(bufile);
					RENAME(fname, bufile);
				}
				if (!write_file(fname, first, last))
				{
					if (ch == 'D')
						modflag = 0;
					if (ch == 'X')
					{
						modflag = 0;
						goto dockq;
					}
				}
			}
			break;
		case 'E':	/* System command */
			if (envbits(ENVB_NOSHELLOUT))
				goto noshell;
			gotoxy(0, height - 1);
			hil(0);
			prcrlf();
			if (modflag)
				PRINTF(filenotsaved);
			*rfile = '\0';
			PRINTF("\r\n-$");
			IEXEC(rfile);
			getret();
			break;
		case 'F':	/* Freeze screen */
			if (frozen)
				unfreeze();
			else
			{
				if ((i = cur_y + cur_x / width) > (SCR_TOP))
				{
					scr_top = i + 1;
					frozen = 1;
					if (scr_top > (height - 10))
						scr_top = (height - 10);
					/* Draw frozen screen separator */
					gotoxy(0, scr_top - 1);
					equalsigns();
					redraw(cur_lp, cur_lin, (scr_top + height)/2);
				}
			}
			break;
		case 'H':	/* Set / clear block markers */
			if (blkset)
			{
				blkset = 0;
				redhere();
			}
			else
				goto chkblk;
			break;
		case 'I':	/* Indefinite shell escape */
			if (envbits(ENVB_NOSHELLOUT))
			{
noshell:			sstatmes("Permission denied");
				break;
			}
			gotoxy(0, height - 2);
			PRINTF("Type 'QUIT' to return to editor  ");
			if (modflag)
				PRINTF(filenotsaved);
			hil(0);
			clr_eol();
			prcrlf();
			clr_eol();
			IEXEC("edit %s");
			unfreeze();
			break;
		case 'K':	/* Block end */
			blkend = cur_ptr;
chkblk:			if (blkstart && blkend && (blkstart < blkend))
			{
				blkset = 1;
				redhere();
			}
			else
			{
				if (blkset)
				{
					blkset = 0;
					redhere();	/* Clear any highlighting */
				}
				blkset = 0;
			}
			break;
		case 'P':
			if (blkok())
				predelete(blkstart, blkend - blkstart);
			break;
		case 'Q':	/* Quit */
dockq:			if (modflag)
			{
				sstatmes("Not saved yet");
				modflag = 0;
			}
			else
			{
				gotoxy(0, height - 1);
				hil(0);
				clr_eol();
				quitflag = 1;
			}
			break;
		case 'R':	/* Read file */
			tgets(rfile, "Filename? ");
			if (*rfile)
			{
				read_file(rfile, 1);
				draw(cur_lp, cur_lin, cur_y);
			}
			break;
		case 'V':	/* Block move */
			if (blkok() && blkcpy('V') && blkdel())
			{
movend:				cur_ptr = newcur_ptr - blksize;
				blkstart = cur_ptr;
				blkend = cur_ptr + blksize;
				blkset = 1;
				redoptr();
				redhere();
			}
			break;
		case 'W':	/* Block write */
			if (blkok())
			{
				first = blkstart;
				last = blkend;
				fname = rfile;
				*fname = '\0';	/* Force new filename */
				goto wrout;
			}
			break;
		case 'Y':	/* Block delete */
			if (blkok() && blkdel())
			{
				cur_ptr = newcur_ptr;
				redoptr();
				redhere();
			}
			break;
		default:
			if ((ch >= '0') && (ch <= '9'))
			{
				markers[ch-'0'] = cur_ptr;
			}
			else
				bad_cmd();
			break;
		}	/* switch */
		newcur_ptr = 0;	/* Speed up block swapping code (a bit) */
	}		/* else */
	chk_end();
}

/* Kludge end of file */
chk_end()
{
	if (!endatcr())
		blkswap(end_buf, 0, "\r\n", 2);
}

/* Get a return char */
getret()
{
	int ch;

	hil(2);
	PRINTF("edit: hit ENTER ");
	while ((ch = rawgetc(STDIN)) != '\r' && (ch >= 0))
		;
	resthil();
	unfreeze();
}

/*
 * Search for the string at srchstr
 */

search()
{
	if (*srchstr)
	{
		while (nreps-- && !editsignalled())
		{
			if (locate(srchstr, 1))
				nreps = 0;	/* Force an end */
			else
				nfound++;
		}
		newcurs();
	}
	prep_nfound();
}

/* Unfreeze the screen */
unfreeze()
{
	frozen = 0;
	scr_top = SCR_TOP;
	redhere();
}

/*
 * Substitute *subsold with *subsnew nreps times from cur_ptr.
 */

substitute()
{
	register int newlen, cnt;
	register int oldlen;

	oldlen = strlen(subsold);

	if (oldlen)
	{
		newlen = strlen(subsnew);
		while (nreps-- && !editsignalled())
		{
			if ( locate(subsold, 0) ||
				(cur_ptr + oldlen >= end_buf) )
				nreps = 0;
			else
			{
				blkswap(cur_ptr, oldlen, subsnew, newlen);
				nfound++;
				cnt = newlen;
				while (cnt--)
				{	/* Move cur_ptr to end of new text */
					if (MASK(*cur_ptr) == '\r')
					{
						cur_lin++;
						cur_lp = cur_ptr + 2;
					}
					cur_ptr++;
				}
			}
		}
		if (nfound)
			redwhole();
	}
	prep_nfound();
}

/*
 * Search from cur_ptr+off for the passed string. If found, set up cur_ptr,
 * cur_lin, cur_lp and return 0.
 */

locate(str, off)
register uchar *str;
register int off;
{
	register uchar *loc_lp;
	register uchar *loc_ptr;
	register int strsize;

	strsize = strlen(str);
	loc_ptr = cur_ptr+off;

	if (loc_ptr >= end_buf)
		return 1;

	for ( ; ; )
	{
		if (!(loc_ptr = (uchar *)INDEX(loc_ptr, *str)))
			return 1;	/* Not found */
		if (!STRNCMP(loc_ptr, str, strsize))
		{	/* Found it */
			cur_ptr = loc_ptr;
			loc_lp = cur_lp;
			while ((loc_lp = getnext(loc_lp)) <= loc_ptr)
			{	/* Now restore cur_lin, cur_lp */
				cur_lin++;
				cur_lp = loc_lp;
			}
			return 0;
		}
		loc_ptr++;
	}
}
