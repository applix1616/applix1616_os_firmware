/*
 * Definitions for the 6522 VIA device
 */
#ifndef VIA_H
#define VIS_H

#define VIABASE		0x700100		/* Base address of VIA */

#define V_BREG		((unsigned char *) VIABASE)	/* Port B I/O register	*/
#define V_AREG		((unsigned char *) VIABASE+2)	/* Port A I/O register	*/
#define V_DDRB		((unsigned char *) VIABASE+4)	/* Port B DDR		*/
#define V_DDRA		((unsigned char *) VIABASE+6)	/* Port A DDR		*/
#define V_T1CL		((unsigned char *) VIABASE+8)	/* T1 L.O. latches	*/
#define V_T1CH		((unsigned char *) VIABASE+10)	/* T1 H.O. counter	*/
#define V_T1LL		((unsigned char *) VIABASE+12)	/* T1 L.O. latches	*/
#define V_T1LH		((unsigned char *) VIABASE+14)	/* T1 H.O. latches	*/
#define V_T2CL		((unsigned char *) VIABASE+16)	/* T2 L.O. latches	*/
#define V_T2CH		((unsigned char *) VIABASE+18)	/* T2 H.O. counter	*/
#define V_SR		((unsigned char *) VIABASE+20)	/* Shift register	*/
#define V_ACR		((unsigned char *) VIABASE+22)	/* Aux control register	*/
#define V_PCR		((unsigned char *) VIABASE+24)	/* Peripheral cont. reg	*/
#define V_IFR		((unsigned char *) VIABASE+26)	/* Interrupt flag reg	*/
#define V_IER		((unsigned char *) VIABASE+28)	/* Interrupt enable reg	*/
#define V_BREGNH	((unsigned char *) VIABASE+30)	/* No H/S port (sus)	*/

/*
 * Define VIA port A I/O bits
 */

#define CLRKBL		0x80		/* Clear K/B delay line latch	*/
#define KBRESET		0x40		/* K/B reset / S/R clock	*/
#define CLRCASIRQ	0x20		/* Clear Cassette IRQ/data	*/
#define ECASRIRQ	0x10		/* Cassette read IRQ enable	*/
#define EN640_MASK	0x08		/* 640 column mode enable	*/
#define ECASWIRQ	0x04		/* Cassette write IRQ enable	*/
#define CENTSTB		0x02		/* Centronics strobe		*/
#define CENTBUSY	0x01		/* Centronics printer busy 	*/

#endif

