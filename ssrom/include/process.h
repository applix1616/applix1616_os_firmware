/*
 * Header file for process control
 */
#ifndef PROCESS_H
#define PROCESS_H



/* Max number of processes */
#define MAXPIDS		64

/* User registers as pushed on the stack */
typedef struct userregs
{
	long regs[15];		/* The registers d0-d7/a0-a6 */
	ushort sr;		/* Status register */
	long pc;		/* Program counter */
} USERREGS;

/* A linked list of these structures hangs off a process's process table */
typedef struct procsig
{
	struct procsig *next;
	long sigtype;
	long sigarg;
	long whofrom;
} PROCSIG;

typedef struct proc
{
	int pid;		/* The process ID */
	char *name;		/* Something to identify it */
	int parent;		/* Parent ID */
	int child;		/* Child if waiting */
	uint idev, odev, edev;	/* Standard in, out, error descriptors */
	long ticks;		/* How long it has run */
	long starttime;		/* When it was scheduled */
	uint whennext;		/* When it is to restart if sleeping */
	short flags;		/* Permanent status flags */
	long loadaddr;		/* Load address for file */
	long stack;		/* Its stack space */
	long stackspace;	/* Its stack size */
	long stackbot;		/* The lowest address where its SP can get */
	long sp;		/* Its current stack pointer */
	int exitcode;		/* Value left from exit */
	char *cur_path;		/* Its working directory */
	int timeslice;		/* How many ticks it gets per go */
	long sigvec;		/* Signal entry point */
	PROCSIG *procsig;	/* List of pending signals */
	long padd;		/* Was sigval2 */
	int uid;		/* Process user ID: inherited */
	int umask;		/* File creation mask: inherited */
	char **argv;		/* Argument vector */
	int homeshell;		/* PID of iexec to which this belongs */
	char *pad0;		/* Was user struct pointer: unused */
	long alarmtime;		/* When an alarm is to be sent */

/* V4.2 additions */
	/* Stdin, out, err when process started up */
	uint orig_idev, orig_odev, orig_edev;
	int (*snoozevec)();	/* If non-zero, call here to see if
				 * it wants to run */
	long snoozearg1;	/* Arguments passed to snoozevec */
	long snoozearg2;
	short nomemerrflag;	/* If set, don't signal on getmem failure */
	int lastchildpid;	/* Last child we started */
	int pg;			/* Process group */
	long childtime;		/* Accumulated time of children */
	struct shellenv *shellenv;	/* Shell process's environment */
	struct ipcblock *ipcblock;	/* Block IPC buffers */
	/* V4.6 */
	long conal1, conal2, conal3;
	long tscount;		/* Downcounter for negative timeslices */
	long bgtimeslice;	/* Timeslice for async processes */
} PROC;

/* Bits in proc.flags */
#define PS_BLOCKED	1	/* Process is sleeping on proc.child */
#define PS_PARBLOCKED	2	/* This process has blocked its parent */
#define PS_EXIT		4	/* Exit is pending */
#define PS_BINARY	8	/* Program needs text, data, bss freeing */
#define PS_KILLED	0x10	/* Program died via a kill */
#define PS_SIGPENDING	0x20	/* Signal it when it is rescheduled */
#define PS_TRACETOG	0x40	/* Trace mode switch pending */
#define PS_SHELLPROC	0x80	/* It is a shell process */
#define PS_NOSPCHECK	0x100	/* Suppress stack bound checking */
#define PS_SIGBLOCK	0x200	/* Ignoring signals */
#define PS_BSIGPENDING	0x400	/* Blocked signal pending */
#define PS_STOPPED	0x800	/* Stopped with SIGSTOP */
#define PS_SIGEXIT	0x1000	/* Signal this pid when anyone exits */

/* Bits in the flag to schedprep */
#define SCP_ISBINARY	1	/* Program text to be freed on exit */
#define SCP_ISASYNC	2	/* Invoke asynchronously */
#define SCP_ISSHELL	4	/* Is an interactive command shell process */


/* Inherited environment associated with a shell type process */
struct shellenv
{
	ulong envbits;			/* Environment bits. See 'envbits.h' */
	ushort nusers;			/* User count for this structure */
	char *ibcvec;			/* Inbuilt command disable vector */
	struct envstring *envstring;	/* Environment string list */
};

/*
 * Environment string variable storage. A linked list of these
 * hang off the shell environment structure
 */
typedef struct envstring
{
	struct envstring *next;
	char *name, *setting;
	ushort mode;		/* String substitution mode. See 'envbits.h' */
	ushort uid;		/* Its owner */
} ENVSTRING;


#endif /* PROCESS_H */
