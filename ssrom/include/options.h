/*
 * Define options in the 'option' system call
 */

#define OPT_PROMPT	0	/* Turn prompt on/off */
#define OPT_VERBOSE	1	/* Turn verbose mode on/off */
#define OPT_DIRMODE	2	/* Directory listing mode */
#define OPT_EXCEPCLS	3	/* Clear screen on exceptions */
#define OPT_DISKVERBOSE	4	/* Disk controller code verbose mode */
#define OPT_MEMERRFLAG	5	/* Halt on out of memory */
#define OPT_EOFCHAR	6	/* End-of-file character */
#define OPT_EXECSSPACE	7	/* Set exec file default stack space */
#define OPT_CANWRITERB	8	/* Can write root block */
#define OPT_ENKBRESET	9	/* Enable ALT^R */
#define OPT_ENALTS	10	/* Enable ALT-S */
#define OPT_ENSPKEYS	11	/* Enable ALT keys */
#define OPT_ENERRBEEP	12	/* Enable beep on errors */
#define OPT_XRESTART	13	/* If true, warm boot on exceptions */
#define OPT_TRACEREGS	14	/* If true, display regiters in trace handler */
#define OPT_MISC	15	/* Miscellaneous flags */
#define OPT_UMASK	16	/* Set new umask */
#define OPT_LOWERNAMES	17	/* Lower case filename creation enabled */
#define OPT_VIDOMODE	18	/* Set video output terminal emulation */
#define OPT_AUTOREHASH	19	/* Reread xpath directories on command miss */

#define OPT_READ	((unsigned )0x80000000)	/* Top bit set to read */

/* Bits in misc option */
#define OPM_ALTCINT	2	/* Generate SIGINT on ALT-C */
#define OPM_NKPCQ	4	/* '5' key on numeric keypad is ^Q */
#define OPM_STDERR	16	/* Internal error diags on stderr, not con: */
#define OPM_DONT_REPORT_FREE_BLOCK_READS 32 /* ... */
#define OPM_CASESENSITIVE 64	/* Make commands & files case sensitive */
