/*
 * Define storage structures, etc
 */

#ifndef STOREDEF_H
#define STOREDEF_H

#define KB_BUFSIZE	201		/* Size of K/B type-ahead queue */
#define SER_TXBUFSIZE	48		/* Size of default serial Tx buffers */
#define SER_RXBUFSIZE	200		/* Size of default serial Rx buffers */
#define CENT_BUFSIZE	65		/* Size of Centronics circular buffer */

/*
 * Copies of write-only latches
 */

#define FMEM	0x300
#define vlval	(* (ushort *) FMEM)		/* 300		*/
#define clval	(* (ushort *) (FMEM+2))		/* 302		*/
#define dlval	(* (ushort *) (FMEM+4))		/* 304		*/
#define alval	(* (ushort *) (FMEM+6))		/* 306		*/
#define palval	( (ushort *) (FMEM+8))		/* 308,A,C,E	*/

/* Keyboard state images */

#define ak_ctrl		(*(ushort *)(FMEM+0x10))/* 310: Control is depressed */
#define ak_shift	(*(ushort *)(FMEM+0x12))/* 312: Shift is depressed */
#define ak_alt		(*(ushort *)(FMEM+0x14))/* 314: Alt is depressed */
#define ak_capslock	(*(ushort *)(FMEM+0x16))/* 316: Caps are locked */
#define ak_numlock	(*(ushort *)(FMEM+0x18))/* 318: Keypad is in numeric mode */


#endif
