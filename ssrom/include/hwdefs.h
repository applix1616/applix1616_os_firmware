/*
 * I/O definitions for the nameless wonder
 */

#ifndef HWDEFS_H
#define HWDEFS_H

#define ROMSTART	((unsigned char *) 0x500000)	/* Start of ROMs */

#define CENTLATCH	((unsigned char *) 0x600001)	/* Centronics latch */
#define DACLATCH	((unsigned char *) 0x600081)	/* DAC latch */
#define VIDLATCH	((unsigned char *) 0x600101)	/* Video latch */
#define AMUXLATCH	((unsigned char *) 0x600181)	/* Analog multiplexor latch */
#define IPORT		((unsigned char *) 0x700081)	/* Input port */
#define PAL_0		((unsigned char *) 0x600000)	/* Pallette 0 entry */
#define PAL_1		((unsigned char *) 0x600020)	/* Pallette 1 entry */
#define PAL_2		((unsigned char *) 0x600040)	/* Pallette 2 entry */
#define PAL_3		((unsigned char *) 0x600060)	/* Pallette 3 entry */

#define	SCC_B		((unsigned char *) 0x700000)	/* SCC B channel */
#define	SCC_A		((unsigned char *) 0x700004)	/* SCC A channel */

#define CRTC_ADDR	((unsigned char *) 0x700180)	/* 6845 address reg */
#define CRTC_DATA	((unsigned char *) 0x700182)	/* 6845 data reg */

#define CRTC_SAVE	0x15ba		/* Value to save in the CRTC cursor register */

/*
 * Interrupt related definitions
 */

#define CAS_SPL()	_spl4()			/* Disable cassette IRQ's */
#define SCC_SPL()	_spl3()			/* Disable SCC interrupts */
#define VIA_SPL()	_spl2()			/* Disable VIA interrupts */

#ifdef GPCPU
#define SCC_SPL()	_spl3()			/* Disable SCC interrupts */
#define VIA_SPL()	_spl2()			/* Disable VIA interrupts */
#endif /* GPCPU*/

#define CAS_IVEC	0x70			/* Level 4 autovector */
#define S_INTVEC	0x6c			/* Level 3 */
#define V_INTVEC	0x68			/* Level 2 */

/* Simulated vectors */
#define	V_VECBASE	0x100			/* The VIA vector base	*/
#define VIV_T1		V_VECBASE		/* Timer 1 timeout	*/
#define VIV_T2		(V_VECBASE+4)		/* Timer 2 timeout	*/
#define VIV_CB1		(V_VECBASE+8)		/* CB1 interrupt	*/
#define VIV_CB2		(V_VECBASE+12)		/* CB2 interrupt	*/
#define	VIV_SR		(V_VECBASE+16)		/* Shift register	*/
#define VIV_CA1		(V_VECBASE+20)		/* CA1 interrupt	*/
#define	VIV_CA2		(V_VECBASE+24)		/* CA2 interrupt	*/

#define S_VECBASE	0x140			/* The SCC vector base	*/
#define SIV_RXA		S_VECBASE		/* RxA interrupt	*/
#define SIV_TXA		(S_VECBASE+4)		/* TxA interrupt	*/
#define SIV_ESA		(S_VECBASE+8)		/* Ext/stat A		*/
#define SIV_RXB		(S_VECBASE+12)		/* RxB interrupt	*/
#define SIV_TXB		(S_VECBASE+16)		/* TxB interrupt	*/
#define SIV_ESB		(S_VECBASE+20)		/* Ext/stat B		*/

/*
 * Define the colour mapping
 */

#define PC_BLACK	0
#define PC_DGREY	1
#define PC_DBLUE	2
#define PC_MBLUE	3
#define PC_DGREEN	4
#define PC_GREEN	5
#define PC_BGREY	6
#define PC_LBLUE	7
#define PC_DRED		8
#define PC_RED		9
#define PC_DVIOLET	10
#define PC_VIOLET	11
#define PC_BROWN	12
#define PC_YELLOW	13
#define PC_LGREY	14
#define PC_WHITE	15

#define EXPANDCOLOUR320(c)	(c|(c<<4)|(c<<8)|(c<<12)|(c<<16)|(c<<20)|(c<<24)|(c<<28))

/* Define bits in Centronics latch */

/* Define bits in analog multiplexor latch */

#define CASMOTOR	0x80		/* Cassette motor: active low */
#define LEDBIT		0x08		/* Status LED: active low */

/* Define bits in input port */

#define CASDAT		0x02		/* Latched cassette data */
#define ADCDATA		0x01		/* ADC comparator output */

#endif

