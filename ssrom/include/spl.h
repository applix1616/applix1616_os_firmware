/* TABS4 */

/* Definitions for spl() and friends */

extern int _spl7(void);
extern int _spl6(void);
extern int _spl5(void);
extern int _spl4(void);
extern int _spl3(void);
extern int _spl2(void);
extern int _spl1(void);
extern int _spl0(void);
extern void _splx(int);
