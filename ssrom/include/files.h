/*
 * Header file for file I/O
 */

#ifndef NIODRIVERS
#define NIODRIVERS	16	/* Number of char device drivers */
#endif

#ifndef STDIN
#define STDERR		0x100	/* Standard error file descriptor */
#define STDOUT		0x101	/* Standard output file descriptor */
#define STDIN		0x102	/* Standard input file descriptor */
#endif

#define DEF_MAXFILES	32	/* Number of files (if not in MRDRIVERS) */
#define DEF_NDCENTRIES	20	/* Number of directory cache entries */
#define MAXFILES	236	/* Absolute maximum number of fcbs */
#define FNAMESIZE	32	/* Max file name size */
#define TPALOC		0x4000	/* Where files load in */

/* open() modes */
/* These are an embarrassment */
#define SSO_RDONLY	1
#define SSO_WRONLY	2
#define SSO_APPEND	SSO_WRONLY
#define SSO_RDWR	3
#define SSO_TRUNCATE	4

#define PIPEBUFSIZE	2048	/* Buffer size for a pipe */
#define PIPEOUTPUT	1	/* Output end of pipe */
#define PIPEINPUT	2	/* Input end of pipe */

/* Disk directory entry. Must be 64 bytes long! */
struct dir_entry
{
	char file_name[FNAMESIZE];	/* Upper case only */
/* 32 */ char date[8];
/* 40 */ ushort uid;		/* User ID */
/* 42 */ char *load_addr;	/* Code execution point */
/* 46 */ int file_size;		/* Total length (No. of blocks for directories)*/
/* 50 */ ushort statbits;	/* Status bits: see below */
/* 52 */ ushort blkmapblk;	/* Block map (start block for directories) */
/* 54 */ ushort magic;		/* If == V4_2MAGIC, ffblocks OK */
	ushort ffblocks[4];	/* First four blocks in file */
};

/* Masks for statbits field above */
#define BACKEDUP	1	/* File has been backed up somewhere */
#define DIRECTORY	2	/* It is a directory */
#define LOCKED		4	/* Cannot be altered */
#define FS_NOREAD	8	/* Disable others from reading */
#define FS_NOWRITE	16	/* Disable others from modifying */
#define FS_NOEXEC	32	/* Disable others from executing */
#define FS_LINK		64	/* Symbolic link thingy */
#define FS_HASFFB	128	/* ffblocks valid (V4.2 OS) */
#define FS_HIDDEN	256	/* Hidden from directory listings */
#define FS_BORING	512	/* Not to be backed up */	
#define V4_2MAGIC	0xd742

/* Maximum size of file for which we don't need to read blockmap block */
#define MAXFFSIZE	(4 * BLOCKSIZE)

/* Number of bytes/directory entry */
#define DIRENTSIZE	64

/* Define the PROCESSDIR() system call modes */
#define PD_DELETE	0	/* delete file */
#define PD_RENAME	1	/* rename file */
#define PD_READIT	2	/* read from directory, return position */
#define PD_WRITEIT	3	/* write directory entry out */
#define PD_SETUID	4	/* Set owner */
#define PD_WREMPTY	5	/* write to empty position, return position */
#define PD_SETDATE	6	/* change date to date at *buf */
#define PD_CLEARSTATBITS 7	/* clear attribute bits (buf == bitmask) */
#define PD_SETSTATBITS	8	/* set attribute bits (buf == bitmask) */

