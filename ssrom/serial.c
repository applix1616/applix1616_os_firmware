/* TABS4
 * Serial I/O drivers
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
#include <storedef.h>


#include <blkerrcodes.h>
//#include <splfns.h>
#include "scc.h"
#include "chario.h"
#include "spl.h"
#include "mchutil.h"
#include "movmem.h"
#include "circbuf.h"
#include "serial.h"

long brclock;

/* Main data structures for SCC. Up to 4 chips suported */
struct scc scc[NSCCCHANS];

/* Circular buffers */
struct circ_buf scca_ocbuf, sccb_ocbuf;
struct circ_buf scca_icbuf, sccb_icbuf;

extern int reslevel;
extern struct chardriver *chardrivers;

void comms_init(void)
{
	register int chan;	/* Charlie */
	register struct scc *sccptr;
	int scc_rxaisr();	/* The C RxA code */
	int scc_txaisr();
	int scc_esaisr();
	int scc_rxbisr();
	int scc_txbisr();
	int scc_esbisr();
	static struct scc_prog def_sprog = { 9600, 3, 3, 0, 1 };

	if (reslevel == 0)
		brclock = 3750000L;

/* Set up vectors to channel A&B Rx&Tx interrupt code */
	*((int *) SIV_RXA) = (int) scc_rxaisr;
	*((int *) SIV_TXA) = (int) scc_txaisr;
	*((int *) SIV_ESA) = (int) scc_esaisr;
	*((int *) SIV_RXB) = (int) scc_rxbisr;
	*((int *) SIV_TXB) = (int) scc_txbisr;
	*((int *) SIV_ESB) = (int) scc_esbisr;

	NEW_CBUF(0, 0, 0);	/* Set up default circular buffers */
	NEW_CBUF(1, 0, 0);
	NEW_CBUF(2, 0, 0);
	NEW_CBUF(3, 0, 0);

	sccptr = &scc[0];
	sccptr->data = SCC_A + SCC_DATA;
	sccptr->cont = SCC_A + SCC_CONT;
	sccptr->txcbuf = &scca_ocbuf;
	sccptr->rxcbuf = &scca_icbuf;
	sccptr->chardriver = chardrivers + 1;	/* SA: fd = 1 */
	sccptr->splmask = 0x0300;
	sccptr->cdindex = 1;
	sccptr->wr15image = 0;
	sccptr->dosfc = 0;
	sccptr->sent_xoff = 0;

	sccptr++;
	sccptr->data = SCC_B + SCC_DATA;
	sccptr->cont = SCC_B + SCC_CONT;
	sccptr->txcbuf = &sccb_ocbuf;
	sccptr->rxcbuf = &sccb_icbuf;
	sccptr->chardriver = chardrivers + 2;	/* SB: fd = 2 */
	sccptr->splmask = 0x0300;
	sccptr->cdindex = 2;
	sccptr->wr15image = 0;
	sccptr->dosfc = 0;
	sccptr->sent_xoff = 0;

	for (chan = 0, sccptr = scc; chan < 2; chan++, sccptr++)
	{
		if (reslevel == 0)
			sccptr->dohfc = 1;
		if (reslevel == 0 || (PROG_SIO(chan, &scc[chan].curprog) < 0))
			PROG_SIO(chan, &def_sprog);
	}
}

/*
 * System call 82: Program a serial port. Return non-zero if bad data supplied
 * If spptr == 0, return pointer to current setting structure
 * If spptr == 1, return pointer to whole driver structure
 */

int _prog_sio(uint chan,struct scc_prog *spptr)
{
	register int br, tc;
	register struct scc *sccptr;
	volatile register uchar *contptr;	/* Control register */
	register int s;

	static uchar bitmask[] = { 0x1f, 0x3f, 0x7f, 0xff };
/* Look-up table for programming WR3 */
	static uchar wr3lut[] = { 0x21, 0xa1, 0x61, 0xe1 };
/* Look-up table for programming WR4. Stop bits & parity */
	static char wr4lusb[] = { 0x44, 0x48, 0x4c };
	static char wr4lupa[] = { 0x40, 0x41, 0x43 };
/* Look-up table for programming WR5 */
	static uchar wr5lut[] = { 0x8a, 0xca, 0xaa, 0xea };

	sccptr = &scc[chan];

	if (spptr == 0)
		return (int)&sccptr->curprog;
	if (spptr == (struct scc_prog *)1)
		return (int)sccptr;

	if ((chan > NSCCCHANS) || (spptr->rxbits > 3) || (spptr->txbits > 3) ||
			(spptr->parity > 2) || (spptr->stopbits > 2))
		return -1;

	sccptr->rxmask = bitmask[spptr->rxbits];
	contptr = sccptr->cont;

/* All values are valid. Now set it up */
	s = newspl(sccptr->splmask);

	*contptr = 3;		/* Auto enable, Rx enable, Rx bits */
	sccptr->wr3image = wr3lut[spptr->rxbits];
	if (!sccptr->dohfc)
		sccptr->wr3image &= ~SCC_AUTOENABLES;
	*contptr = sccptr->wr3image;

	*contptr = 4;		/* Clock mode, stop bits, parity */
	*contptr = wr4lusb[spptr->stopbits] | wr4lupa[spptr->parity];

	*contptr = 5;		/* DTR, Tx bits, Tx enable, RTS */
	*contptr = sccptr->wr5image = wr5lut[spptr->txbits];

	*contptr = 11;		/* Tx, Rx from BR gen */
	*contptr = 0x52;

	br = (int)spptr->brate;
	br &= 0xffff;
	tc = (brclock + (br << 4)) / (br << 5) - 2;
	*contptr = 12;		/* Time constant low */
	*contptr = (uchar)tc;
	*contptr = 13;		/* Time constant high */
	*contptr = (uchar)(tc >> 8);

	*contptr = 14;		/* BRG enable */
	*contptr = 1;

	*contptr = 15;					/* DCD, CTS, break, etx/stat IE */
	*contptr = sccptr->wr15image;	/* Interrupt on DCD only */

	*contptr = 0;		/* Enable interrupt on next Rx char */
	*contptr = 0x20;

	*contptr = 1;		/* Enable Rx, Tx and ext interrupts */
	*contptr = 0x13;

	*contptr = 9;		/* Master interrupt enable */
	*contptr = 8;

	*contptr = 0x10;	/* Reset external/status interrupts */

	_splx(s);

	/* Record the setting */
	movmem(spptr, &sccptr->curprog, sizeof(*spptr));
	return 0;
}

/*
 * Return status of an input circular buffer (number of buffered chars)
 */

int scc_istat(int chan)
{
	int s, nch;

	s = newspl(scc[chan].splmask);
	nch = cb_bnum(scc[chan].rxcbuf);
	_splx(s);
	return nch;
}

/*
 * Return status of an output circular buffer (number of unused chars)
 */

int scc_ostat(chan)
{
	int s, nch;

	s = newspl(scc[chan].splmask);
	nch = cb_bfree(scc[chan].txcbuf);
	_splx(s);
	return nch;
}

/*
 * Return a char from a channel. If this leaves more than 32 bytes free in the
 * circular buffer, assert RTS
 */

unsigned short scc_getc(int chan)
{
	register unsigned short ch, s;
	register struct scc *sccptr;
	volatile register uchar *contptr;

	sccptr = scc + chan;
	ch = get_cbuf(sccptr->rxcbuf);

	/* If > 32 bytes free, assert RTS */
	if (sccptr->dohfc)
	{
		s = newspl(sccptr->splmask);
		if (cb_bfree(sccptr->rxcbuf) > 32)
		{
			contptr = sccptr->cont;
			*contptr = 5;
			*contptr = (sccptr->wr5image |= SCC_RTS);
		}
		splx(s);
	}
	if (sccptr->dosfc && sccptr->sent_xoff)
	{
		s = newspl(sccptr->splmask);
		/* Maybe we need to send an xon? */
		if (cb_bfree(sccptr->rxcbuf) > 32)
		{
			sccptr->needs_xon = 1;
			if (*(sccptr->cont) & SCC_TXBE)		/* Buffer empty? */
				do_tx(chan);
		}
		splx(s);
	}
	return ch;
}

/*
 * Transmit a char to a channel. If the Tx buffer is empty, force transmission
 * of a char from the SCC
 */

scc_putc(ch, chan)
{
	register uint head;
	register int s;
	register struct scc *sccptr;
	volatile register struct circ_buf *cbptr;

	sccptr = scc + chan;

	/* Quick look to see if these is room */
	cbptr = sccptr->txcbuf;
	head = cbptr->head;
	if (++head >= cbptr->cbsize)
		head = 0;
	if (head == cbptr->tail)
		wroom_cbuf(sccptr->txcbuf);	/* Wait until there is a gap */

	s = newspl(sccptr->splmask);
	put_cbuf(sccptr->txcbuf, ch, -1);	/* Put char in buffer */
	if (*(sccptr->cont) & SCC_TXBE)		/* Buffer empty? */
		do_tx(chan);
	splx(s);
	return 0;
}

/*
 * SCC Interrupt service routines
 */

scc_rxaisr()
{ do_rx(0); }

scc_txaisr()
{ do_tx(0); }

scc_esaisr()
{ do_es(0); }

scc_rxbisr()
{ do_rx(1); }

scc_txbisr()
{ do_tx(1); }

scc_esbisr()
{ do_es(1); }

/*
 * General SCC Rx ISR. Move char from data reg to circular buffer
 */

do_rx(chan)
{
	register struct scc *sccptr;
	volatile register uchar *contptr;

	sccptr = scc + chan;

	put_cbuf(sccptr->rxcbuf, *(sccptr->data) & sccptr->rxmask,
		sccptr->cdindex);

/* If 16 bytes free, drop RTS */
	if (sccptr->dohfc)
	{
		if (cb_bfree(sccptr->rxcbuf) < 16)
		{
			contptr = sccptr->cont;
			*contptr = 5;
			*contptr = (sccptr->wr5image &= ~SCC_RTS);
		}
	}
	if (sccptr->dosfc)
	{
		if (!sccptr->sent_xoff)
		{
			if (cb_bfree(sccptr->rxcbuf) < 16)
			{
				sccptr->needs_xoff = 1;				/* Send the xoff */
				if (*(sccptr->cont) & SCC_TXBE)		/* Buffer empty? */
					do_tx(chan);
			}
		}
	}
}

/*
 * General Transmit ISR code. If there are chars in buffer, send one, else
 * reset pending Tx interrupt.
 * Also reset the tx pending interrupt if the device is xoffed
 */

void do_tx(int chan)
{
	register struct scc *sccptr;
	volatile register struct circ_buf *cbptr;

	sccptr = scc + chan;
	cbptr = sccptr->txcbuf;

	if (!sccptr->chardriver->rawmode)
	{
		if (sccptr->needs_xoff)
		{
			*(sccptr->data) = sccptr->xoffchar;
			sccptr->sent_xoff = 1;
			sccptr->needs_xoff = 0;
			return;
		}
		else if (sccptr->needs_xon)
		{
			*(sccptr->data) = sccptr->xonchar;
			sccptr->sent_xoff = 0;
			sccptr->needs_xon = 0;
			return;
		}
	}
	if ((cbptr->head != cbptr->tail) && !sccptr->chardriver->xoffed)
	{
		sccptr->chardriver->txcount++;
		*(sccptr->data) = get_cbuf(cbptr);
	}
	else				/* Reset Tx int pending command */
		*(sccptr->cont) = 0x28;
	return;
}

do_es(chan)
{
	register struct scc *sccptr;
	uchar val;

	sccptr = scc + chan;
	val = *(sccptr->cont);
	*(sccptr->cont) = 0x10;		/* Reset the interrupt source */
	if (!(val & SCC_DCD))
	{
		switch (sccptr->chardriver->hupmode)
		{
		case 1:
		  CDMISC(sccptr->cdindex, CDM_SENDSIGHUP,0,0,0);
			break;
		case 2:
		  CDMISC(sccptr->cdindex, CDM_KILLUSER,0,0,0);
			break;
		case 3:
		  CDMISC(sccptr->cdindex, CDM_SENDSIGINT,0,0,0);
			break;
		default:
			return;
		}
		sccptr->chardriver->xoffed = 0;
	}
}

/*
 * Handle miscellaneous requests
 */

scc_misc(fd, chan, cmd, p1, p2, p3)
register int chan;
register ushort cmd;
{
	register long retval;
	register struct scc *sccptr;
	volatile register uchar *cont;
	volatile register struct circ_buf *cbptr;
	extern ushort romver;
	int inc;
	int s;

	if (cmd == CDM_MIORVEC || cmd == CDM_MIOWVEC)
		return 0;

	sccptr = scc + chan;
	s = newspl(sccptr->splmask);
	cont = sccptr->cont;
	cbptr = sccptr->rxcbuf;
	retval = 0;
	inc = 0;

	switch (cmd)
	{
	case CDM_SETHUPMODE:
		inc = *cont;				/* Untangle pointers */
		sccptr->wr15image = (p1 ? 0x08 : 0x00);	/* Interrupt on DCD only */
		*cont = 15;					/* DCD, CTS, break ie */
		*cont = sccptr->wr15image;
		break;
	case CDM_SETMODE:	/* Set the baud rate, etc */
		retval = PROG_SIO(chan, p1);
		break;
	case CDM_READMODE:	/* Read the baud rate, etc */
		movmem(&sccptr->curprog, p1, sizeof(sccptr->curprog));
		break;
	case CDM_SETDTR:	/* Set/clear DTR */
	case CDM_SETRTS:	/* Set/clear RTS */
		*cont = 5;
		if (p1)
			*cont = (sccptr->wr5image |=
				((cmd == CDM_SETDTR) ? SCC_DTR:SCC_RTS));
		else
			*cont = (sccptr->wr5image &=
				~((cmd == CDM_SETDTR) ? SCC_DTR:SCC_RTS));
		break;
	case CDM_READDCD:	/* Return DCD state */
		retval = *cont & SCC_DCD;
		break;
	case CDM_READCTS:	/* Return CTS state */
		retval = *cont & SCC_CTS;
		break;
	case CDM_READBREAK:
		retval = *cont & SCC_RXBREAK;
		break;
	case CDM_SETHFC:	/* Set/clear H/W flow control */
		sccptr->dohfc = p1;
		*cont = 5;	/* Assert both signals */
		*cont = sccptr->wr5image |= (SCC_RTS|SCC_DTR);
		sccptr->wr3image = p1 ? (sccptr->wr3image | SCC_AUTOENABLES) :
					(sccptr->wr3image & ~SCC_AUTOENABLES);
		*cont = 3;	/* Turn off autoenables */
		*cont = sccptr->wr3image;
	case CDM_READHFC:
		retval = sccptr->dohfc;
		break;
	case CDM_SETBREAK:	/* Start/stop break on Tx */
		sccptr->wr5image = p1 ? (sccptr->wr5image | SCC_TXBREAK) :
					(sccptr->wr5image & ~SCC_TXBREAK);
		*cont = 5;
		*cont = sccptr->wr5image;
		break;
	case CDM_TXCOUNT:	/* Number of chars still to Tx */
		cbptr = sccptr->txcbuf;
	case CDM_RXCOUNT:	/* Number of chars in Rx buffer */
		retval = cb_bnum(cbptr);
		break;
	case CDM_TXROOM:	/* Number of chars free in Tx */
		cbptr = sccptr->txcbuf;
	case CDM_RXROOM:	/* Number of chars free in Rx */
		retval = cb_bfree(cbptr);
		break;
	case CDM_TXFLUSH:	/* Flush all output chars */
		splx(s);
		for ( ; ; )
		{
			if (!cb_bnum(sccptr->txcbuf))
			{	/* No characters buffered */
				int all_sent;

				newspl(sccptr->splmask);
				*cont = 1;				/* Get at the 'all sent' bit */
				all_sent = *cont & 1;
				splx(s);
				if (all_sent)
					break;
			}
			SLEEP(1);
		}
		s = newspl(sccptr->splmask);
		break;
	case CDM_TXPURGE:	/* Dump all buffered tx chars */
		cbptr = sccptr->txcbuf;
	case CDM_RXPURGE:	/* Dump all buffered Rx chars */
		cbptr->head = 0;
		cbptr->tail = 0;
		break;
	case CDM_RXPEEK:	/* Have a peek at next Rx char */
		if (cb_bnum(cbptr))
			retval = cbptr->buf[cbptr->tail];
		else
			retval = -1;
		break;
	case CDM_SETTXBSIZE:	/* Set Tx buffer size */
		cbptr = sccptr->txcbuf;
		inc = 1;
	case CDM_SETRXBSIZE:	/* Set Rx buffer size */
		inc += chan * 2;	/* Arg to new_cbuf */
		/* If second arg == 0, new_cbuf installs default buffer */
		/* If second arg == 1, new_cbuf allocates & installs buffer */
		if (chan < 2)
			retval = NEW_CBUF(inc, p1 ? 1 : 0, p1);
		break;
	case CDM_READTXBSIZE:	/* Read Tx buffer size */
		cbptr = sccptr->txcbuf;
	case CDM_READRXBSIZE:	/* Read Rx buffer size */
		retval = cbptr->cbsize;
		break;
	case CDM_VERSION:	/* Driver version */
		retval = romver;
		break;
	case CDM_SETSFC:	/* Set software flow control */
		sccptr->dosfc = p1;
		sccptr->xonchar = p2;
		sccptr->xoffchar = p3;
		/* Send an XON if we are turning it on */
		sccptr->needs_xon = (sccptr->dosfc ?  1 : 0);
		sccptr->needs_xoff = 0;
		sccptr->sent_xoff = 0;
		if (sccptr->needs_xon)
		{
			int s;

			s = newspl(sccptr->splmask);
			if ((*sccptr->cont) & SCC_TXBE)
				do_tx(chan);
			splx(s);
		}
		break;
	case CDM_READSFC:	/* Read software flow control */
		retval = BEC_BADARG;
		if (p1 == 0)
			retval = sccptr->dosfc;
		if (p1 == 1)
			retval = sccptr->xonchar;
		if (p1 == 2)
			retval = sccptr->xoffchar;
		break;
	}

	splx(s);
	return retval;
}
