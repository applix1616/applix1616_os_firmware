/*
 * Backward index function: return a pointer to the char after the last
 * occurrence of ch in str. eg:
 *
 * bindex("AAAA/BBBB/CCCC/DDDD/EEEE", '/') returns a pointer
 * to here                     |
 *
 * If no occurrence of ch, return str.
 * If ch is last char in string, return pointer to terminal null
 */

char *bindex(char *str,char ch)
{
	register char *cp;

	cp = str;
	while (*str)
	{
		if (*str++ == ch)
			cp = str;
	}
	return cp;
}
