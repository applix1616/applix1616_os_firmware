/*
 * Date and time code.
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>


#include "mchutil.h"
#include "setmem.h"
#include "movmem.h"
#include "datetime.h"

//#include <splfns.h>

extern int reslevel;

/* Arrays of maximum and minimum possible values of date & time. */
static uchar maxval[] = {99, 12, 31, 23, 59, 59, 9};
static uchar minval[] = {0, 1, 1, 0, 0, 0, 0};

/* Array of days in months */
static uchar ndays[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

/* Month name array */
static char *monthname[] = {"???","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep",
	"Oct","Nov","Dec"};

uchar datetime[7];	/* The array with the time in it */
uchar bootdate[7];	/* The time of the first valid SETDATE() */

int time_count;
int timeinc;

void inc_time_date(void);



void dt_init(void)
{
	register uint i, s;

/* If any of the current values are impossible, flush out the time */
#ifdef GPCPU
	s = TIMER_SPL();
#else
	s = VIA_SPL();
#endif

	timeinc = TIMEINC;
 /* Use setdate to verify current time */
	if ((reslevel == 0) && SETDATE(datetime))
		clearmem(datetime, 7);

	SET_VSVEC(inc_time_date, 1, 0);	/* Install the vector */
	if (reslevel < 2)
		time_count = 0;
	if (reslevel)
	{	/* Compensate for time lost during reset */
		for (i = 0; i < 30; i++)
			inc_time_date();
	}
	_splx(s);
}

void inc_time_date(void)
{
	if ( (time_count += timeinc) < 100000 ) return;
	time_count -= 100000;
	if (++datetime[TENTHS] < 10) return;
	datetime[TENTHS] = 0;
	if (++datetime[SECOND] < 60) return;
	datetime[SECOND] = 0;
	if (++datetime[MINUTE] < 60) return;
	datetime[MINUTE] = 0;
	if (++datetime[HOUR] < 24) return;
	datetime[HOUR] = 0;

/* It is now tomorrow. Is it FEB 28 on a leap year? */
	if ( (!(datetime[YEAR] & 3)) && (datetime[MONTH] == 2) &&
		(datetime[DAY] == 28))
	{
		datetime[DAY]++;	/* Go to the 29'th */
		return;
	}
	if (++datetime[DAY] <= ndays[datetime[MONTH]]) return;
	datetime[DAY] = 1;
	if (++datetime[MONTH] <= 12) return;
	datetime[MONTH] = 1;
	datetime[YEAR]++;
}

/*
 * System call 23: Send a date string. If pointer == 0, return boot date
 */

uchar *_getdate(uchar *str)
{
	int s;

	if (str == 0)
		return bootdate;
	else
	{
#ifdef GPCPU
		s = TIMER_SPL();
#else
		s = VIA_SPL();
#endif
		movmem(datetime, str, 7);
		_splx(s);
	}
	return str;
}

/*
 * System call 24: Set date & time. Return 0 if OK, -1 if bad time
 */

int _setdate(uchar *str)
{
	int s;

	if (checkdate(str))
		return -1;
	if (checkdate(datetime))
	{	/* Date was bad, is now good: save it as boot date */
		movmem(str, bootdate, 7);
	}
#ifdef GPCPU
	s = TIMER_SPL();
#else
	s = VIA_SPL();
#endif
	movmem(str, datetime, 7);
	_splx(s);
	return 0;
}

int checkdate(uchar *str)
{
	register int i;

	for (i = 0; i < 7; i++)
	{
		if ((str[i] > maxval[i]) || (str[i] < minval[i]))
			return -1;
	}
	if (str[DAY] > ndays[str[MONTH]])
		return -1;
	return 0;
}

/*
 * Format up the data
 */

char *dtformat(char *dtarray,char *buf)
{
	SPRINTF(buf, "%02d:%02d:%02d %02d %s %04d",
		(int)(dtarray[HOUR]), (int)(dtarray[MINUTE]),
		(int)(dtarray[SECOND]), (int)(dtarray[DAY]),
		monthname[dtarray[MONTH] % 13], (int)(dtarray[YEAR]) + 1900);
	return buf;
}

/*
 * System call 83: Return date/time string
 *
 * V4.2: added extra args.
 *	if (buf == 0 && arg1 == 0)
 *	{
 *		convert time at *arg2 into text at *arg3
 *	}
 *	else if (buf == 0 && arg1 == 1)
 *	{
 *		if (arg2 == 0)
 *			Return current time increment
 *		else
 *			Set time increment to arg2.
 *	}
 */

char *_gettdstr(buf, arg1, arg2, arg3)
char *buf;
long arg1, arg2, arg3;
{
	char localbuf[7];
	char *retval;

	if (buf == (char *)0)
	{	/* V4.2 extensions */
		if (arg1 == 0)
		{	/* Convert time */
			retval = dtformat((char *)arg2,(char *) arg3);
		}
		else
		{	/* Set/read time increment */
			if (arg2)
				timeinc = arg2;
			retval = (char *)timeinc;
		}
	}
	else
	{
		GETDATE(localbuf);
		retval = dtformat(localbuf, buf);
	}
	return retval;
}

/* Convert number of ticks into MINUTES:SECS:HUNDREDTHS */
char *tosecs(char *buf,int ticks)
{
	SPRINTF(buf, "%d:%02d.%02d", ticks/3000, (ticks/50)%60, (ticks%50)*2);
	return buf;
}

void birthday(void)
{
	static uchar message[] =
	{
		13,10,
		'I'+0x80,'t'+0x80,' '+0x80,'i'+0x80,'s'+0x80,' '+0x80,
		'A'+0x80,'n'+0x80,'d'+0x80,'r'+0x80,'e'+0x80,'w'+0x80,
		'\''+0x80,'s'+0x80,' '+0x80,'b'+0x80,'i'+0x80,'r'+0x80,
		't'+0x80,'h'+0x80,'d'+0x80,'a'+0x80,'y'+0x80,':'+0x80,
		' '+0x80,'h'+0x80,'a'+0x80,'v'+0x80,'e'+0x80,' '+0x80,
		't'+0x80,'h'+0x80,'e'+0x80,' '+0x80,'d'+0x80,'a'+0x80,
		'y'+0x80,' '+0x80,'o'+0x80,'f'+0x80,'f'+0x80,13,10,0
	};
	uchar *cp;

	if (datetime[MONTH] == 7 && datetime[DAY] == 15)
	{
		cp = message;
		while (*cp)
			PUTCHAR(*cp++ & 0x7f);
	}
}
