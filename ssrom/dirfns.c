/* TABS4 NONDOC
 * Directory related code
 */

#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <blkio.h>
#include <blkerrcodes.h>
#include <mondefs.h>
#include <reloc.h>
#include <options.h>
#include <envbits.h>


typedef struct
{
  int l, r;
}QUICK_STRUCT;


EXTERN int curstuffvalid;	/* Zero if new disk detected */
EXTERN char *cur_path;		/* Full path of current directory */
EXTERN int cur_pbdvr;		/* Block driver number of durrent path */
EXTERN struct dir_entry cur_pdir; /* Copy of entry for current directory */
EXTERN char *cur_dir;		/* Cached copy of current directory contents */
EXTERN char last_path;
EXTERN struct blkdriver bdrivers[];
EXTERN char slash;
EXTERN int miscmodes;
extern void swapmem(char *m1, char *m2,int size);

char *dowa(), *putinmem(), *skipwhite(), *stoupper();
void realstoupper(char *str);


/*
 * System call 65: change directory.
 * If path == 0, return pointer to current directory name
 */

int _chdir(register char *path)
{
	register int retval;
	register char *temp;
	struct dir_entry dirent;
	char buf[FNAMESIZE];

	if (!path)
		return (int)cur_path;

	if (!strucmp(path, slash))
	{	/* cd to '/' */
		retval = get_bdev(cur_path, 0);
		if (retval < 0)
			return retval;
		path = bdrivers[retval].devname;
	}

	if ((retval = FILESTAT(path, &dirent)) < 0)
		return retval;
	if (!(dirent.statbits & DIRECTORY))
		return BEC_NOTDIR;

	temp = (char *)GETFULLPATH(path, 1);	/* The new current path */
	if (retval = CHKPERM(&dirent, FS_NOREAD, temp))
	{
		dofreemem(temp);
		return retval;
	}

	fsentry();
/* Allocate new memory for the name & directory cache */
	curstuffvalid = 0;	/* Force physical reads */

	dofreemem(cur_path);
	stoupper(cur_path = temp);
	dofreemem(cur_dir);
	cur_dir = (char *)getmem1(dirent.file_size * BLOCKSIZE);
	if ((int)cur_dir < 0)
	{
		curstuffvalid = 0;
		retval = BEC_NOBUF;
		goto out;
	}
	movde(&dirent, &cur_pdir);
	cur_pbdvr = get_bdev(cur_path, buf);

	BDMISC(cur_pbdvr, MB_DIRREAD, 0);
	if ((retval = MULTIBLKIO(cur_pbdvr, MIO_SREAD, cur_dir,
			dirent.blkmapblk, dirent.file_size)) < 0)
	{
		retval = retval;
		goto out;
	}
	curstuffvalid = 1;
	retval = 0;
out:	fsexit();
	if (retval >= 0)
		movstr(last_path, cur_path, 80);
	return retval;
}

/*
 * System call 66: make a directory
 */

int _mkdir(char *path, int ndirblks)
{
	register int retval;
	register char *fullpath;
	struct dir_entry dirent;

	fullpath = (char *)GETFULLPATH(path, 0);
	if ((retval = FILESTAT(fullpath, &dirent)) != BEC_NOFILE)
	{
		if (retval >= 0)
			retval = BEC_EXISTS;
	}
	else
	{
		fsentry();
		retval = find_file(fullpath, 0, 2, 0, ndirblks);
		fsexit();
	}
	dofreemem(fullpath);
	return retval;
}

/*
 * System call 124
 *
 * Allocate memory for, read in and sort the whole of a directory.
 * We are passed the pathname and the memory allocation mode
 * path 	Pointer to name of this directory
 * If sortmode != 0, sort the directory (sorted according to sortmode)
 * sortmode == 1: date
 * sortmode == 2: alpha
 *
 * Return number of directory entries in directory at *psize
 */

int _rdalldir(register char *path, int gmmode, uint sortmode, uint *psize)
{
	register int retval;
	register char *fullpath;
	register int bdvrnum;
	register struct dir_entry *dirbuf;
	struct dir_entry dirent;
	int decmpdate(), decmpalpha();

	fsentry();
	fullpath = (char *)GETFULLPATH(path, 0);
	if ((retval = FILESTAT(fullpath, &dirent)) < 0)
		goto err;

	if (retval = CHKPERM(&dirent, FS_NOREAD, fullpath))
		goto err;

	if (!(dirent.statbits & DIRECTORY))
	{
		retval = BEC_NOTDIR;
		goto err;
	}

/* Allocate room for the directory */
	if ((retval = GETMEM(dirent.file_size * BLOCKSIZE, gmmode)) < 0)
	{
		retval = BEC_NOBUF;
		goto err;
	}
	dirbuf = (struct dir_entry *)retval;

	if ((bdvrnum = get_bdev(fullpath, 0)) < 0)
	{
		retval = bdvrnum;
		goto err2;
	}
/* Read the directory in */
	BDMISC(bdvrnum, MB_DIRREAD, 0);
	if ((retval = MULTIBLKIO(bdvrnum, MIO_SREAD, dirbuf, dirent.blkmapblk,
				dirent.file_size)) < 0)
		goto err2;
	if (sortmode)
		QSORT(dirbuf, dirent.file_size * DIRPERBLOCK,
			DIRENTSIZE, (sortmode == 1) ? decmpdate : decmpalpha);
	*psize = dirent.file_size * DIRPERBLOCK;
	retval = (int)dirbuf;
	goto out;
err2:	dofreemem(dirbuf);
err:	dofreemem(fullpath);
out:
	fsexit();
	return retval;
}

/*
 * Compare two directory entries by date. Return < 0 if *de1 < *de2
 *
 * Two empty directory entries are equal to minimise the work needed.
 * An empty directory entry is greater than a non-empty,
 * to put empties at the end.
 * A directory is less than a file to put directories at the start.
 */

int decmpdate(register struct dir_entry *de1, register struct dir_entry *de2)
{
	register uchar *p1, *p2, i;

	if (!de1->file_name[0])
	{
		if (!de2->file_name[0])
			return 0;	/* Empties are equal */
		/* Return de1 > de2 */
		return 1;
	}

	p1 = (uchar *)de1->date;
	p2 = (uchar *)de2->date;
	for (i = 0; i < 7; i++)
	{
		if (*p1 != *p2)
			return *p1 - *p2;
		p1++;
		p2++;
	}
	return 0;
}

/*
 * Compare two directory entries alphabetically. Return < 0 if *de1 < *de2
 *
 * Two empty directory entries are equal to minimise the work needed.
 * An empty directory entry is greater than a non-empty,
 * to put empties at the end.
 * A directory is less than a file to put directories at the start.
 */

int decmpalpha(register struct dir_entry *de1, register struct dir_entry *de2)
{
	if (!de1->file_name[0])
	{
		if (!de2->file_name[0])
			return 0;	/* Empties are equal */
		/* Return de1 > de2 */
		return 1;
	}

/* We know de1 is a live directory entry */
	if (!de2->file_name[0])
		/* Return de1 < de2 */
		return -1;

/* Both are valid */
	if ((de1->statbits & DIRECTORY) && (!(de2->statbits & DIRECTORY)))
		return -1;
	if ((!(de1->statbits & DIRECTORY)) && (de2->statbits & DIRECTORY))
		return 1;

/* Both directories or both files */
	return strucmp(de1->file_name, de2->file_name);
}





/*
 * System call 92:
 *	Quicksort based on the algorithm given in
 *	"Algorithms + Data Structures = Programs" by N. Wirth.
 *
 * Re-ripped off from the Hitech library
 */

int _qsort(char *base,uint nel,uint width,register int(*compar)(void))
{
  register char *x;
  int i, j;
  register int l, r;
  int s;
  QUICK_STRUCT stack[20];

  x = (char *)getmem0(width + 1);
  x[width] = 0;
  s = 0;
  stack[0].l = 0;
  stack[0].r = nel - 1;

  do
  {
    /* take top request from stack */
    l = stack[s].l;
    r = stack[s--].r;

    do
    {
      i = l;
      j = r;
      movmem(base + width * ((i+j)/2), x, width);
      
      do
      {
	while (entry(compar, base + i*width, x) < 0) i++;
	while (entry(compar, x, base + j*width) < 0) j--;

	if((i == j) || (i < j))
	{
	  swapmem(base + (i*width), base + (j*width), width);
	  i++;
	  j--;
	}
      } while(i <= j);

      if (j-l < r-i)
      {
	if(i < r)
	{	/* stack right partition */
	  stack[++s].l = i;
	  stack[s].r = r;
	}
	r = j;		/* continue with left */
      }
      else
      {
	if(l < j)
	{
	  stack[++s].l = l;
	  stack[s].r = j;
	}
	l = i;
      }
    } while (l < r);
  }  while (s >= 0);
  dofreemem(x);
  return 0;
}

/*
 * Convert string to upper case if enabled
 */

char *stoupper( char *str)
{
	if (!(miscmodes & OPM_CASESENSITIVE) && !envbits(ENVB_LOWERNAMES))
		realstoupper(str);
	return str;
}

void realstoupper(char *str)
{
	register char *cp;

	cp = str;
	while (*cp)
	{
		*cp = toupper(*cp);
		cp++;
	}
}

/* Return true if a file is hidden */
int hidden(register struct dir_entry *pdirent)
{
	return envbits(ENVB_HIDEFILES) && (pdirent->statbits & FS_HIDDEN);
}

int strucmp(register char *s1, register char *s2)
{
	if (miscmodes & OPM_CASESENSITIVE)
		return strcmp(s1, s2);
	return __strucmp(s1, s2);
}
