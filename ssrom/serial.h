/* TABS4
 * Serial I/O drivers
 */
#ifndef SERIAL_H
#define SERIAL_H

extern void comms_init(void);
extern void do_tx(int chan);

#endif
