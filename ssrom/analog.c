/*
 * Analog I/O functions
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
#include <storedef.h>
#include <via.h>
//#include <splfns.h>

ushort iporhist, ipandhist;	/* For catching pulses on input port */
char *fttable, *ftptr;		/* For free tone use */
int fttlen, ftlength;		/* For free tone use */
char analogisrin;		/* Flag: vsvec installed */
int volume;			/* Beep volume: 255 = max */
int (*beepvec)();		/* Beep routine */

extern ushort nobeep;		/* Suppress beeps */
extern int reslevel;
extern short t1intson;
extern char *buf;

extern _ftisr();


int _lbeep();

anio_init()
{
	iporhist = 0;
	ipandhist = 0xff;
	ftlength = 0;
	analogisrin = 0;
	t1intson = 0;
	if (reslevel == 0)
		volume = 60;
	beepvec = _lbeep;
}

/*
 * System call 70: Select an analog input
 */

_anipsel(ipnum)
int ipnum;
{
	int s;

	s = SCC_SPL();			/* No interrupts during diddling */
	*AMUXLATCH = alval = (alval & 0xf8) | (ipnum & 7);
	_splx(s);
}

/*
 * System call 71: Select an analog output
 */

_anopsel(opnum)
int opnum;
{
	int s;

	s = SCC_SPL();
	*AMUXLATCH = alval = (alval & 0xcf) | ((opnum & 3) << 4);
	_splx(s);
}

/*
 * System call 72: Enable/disable analog outputs (non-zero enables)
 */

_anopdis(val)
int val;
{
	int s;

	s = SCC_SPL();
	if (val)
		alval &= 0xbf;		/* Drop INHIBIT on 4052 chip */
	else
		alval |= 0x40;
	*AMUXLATCH = alval;
	_splx(s);
}

/*
 * System call 73: Perform analog to digital conversion
 */

extern int _speed;

_adc()
{
	register int val;
	register unsigned short mask, adcbit;
	register uchar *dlptr;
	register uchar *ip;

	dlptr = DACLATCH;
	ip = IPORT;
	adcbit = ADCDATA;		/* Bit where comparator output is read */

	mask = 0x80;
	val = 0;
	do
	{
		*dlptr = val | mask;		/* Trial value */
		adcpause();
		if (_speed)
			adcpause();		/* Do it twice at 15 MHz */
		if (!(*ip & adcbit))
			val |= mask;
		mask >>= 1;
	}
	while (mask);
	return val;
}

adcpause()
{}

/*
 * System call 74: Set DAC value
 */

_dac(val)
int val;
{
	*DACLATCH = dlval = val;
}

/*
 * System call 75: Set LED on/off
 */

_set_led(val)
int val;
{
	int s;

	s = SCC_SPL();
	if (val)
		*AMUXLATCH = (alval &= (char)(~LEDBIT));
	else
		*AMUXLATCH = (alval |= LEDBIT);
	_splx(s);
}

/*
 * Do a beep (for bell char)
 */

beep()
{
	lbeep(1000, 0);
}

lbeep(len, low)
{
	if (!nobeep)
		_entry(beepvec, len, low);
}

_lbeep(len, low)
{
	register ushort i;
	register uchar *ptr;
	int k;
	int j;
	uchar buf[128];

	if (ftlength)
		return;		/* A sound is already happening */

	ANOPSEL(0);
	ANOPDIS(1);
	ptr = buf;
	if (low)
		low = 2;
	else
		low = 1;
	for (i = 0; i < 16384; i += 128)      /*for (i = 0; i < 128*128; i += 128)*/
		*ptr++ = ((SINE(i) * volume) >> 8) ^ 0x80;

	k = (len / low);
	j = (low * 32);
	FREETONE(buf, 128,k,j);

	for (j = 0; j < 100000; j++)
		if (!ftlength)
			return;
	DIST1INTS();
	ftlength = 0;
}

/*
 * System call 76: Output a waveform from DAC
 * table  Where the waveform resides
 * tablen Where the table wraps around at
 * length How many bytes to output
 * period Interrupt rate (375000/period)
 */
void _freetone(char *table, int tablen, int length, int period)
{

	while (t1intson)
		;			/* Wait for previous tone to end */
	fttable = table;
	ftptr = table;
	fttlen = tablen;
	ftlength = length;

	ENT1INTS(_ftisr, period);	/* Start interrupts */
}

/*
 * System call 77: Return time until previous freetone ends
 */

_fttime()
{
	return ftlength;
}

/*
 * System call 78: Return instantaneous value of input port
 */

_rdiport()
{
	return (uint) *IPORT;
}

/*
 * System call 79: Return OR-ed and AND-ed status of input port
 */

_rdbiport()
{
	uint ipsave;
	int ipbufisr();

	if (!analogisrin)
	{
		analogisrin = 1;
		SET_VSVEC(ipbufisr, 1, 0);
	}

	ipsave = iporhist | (ipandhist << 8);
	iporhist = 0;
	ipandhist = 0xff;
	return ipsave;
}

/* Input port buffering ISR: called by vertical sync interrupts */
ipbufisr()
{
	iporhist |= *IPORT;
	ipandhist &= *IPORT;
}

/* Xbus system call stubs */
_xbread()
{
	eprintf("Xbus driver not installed\r\n");
}
