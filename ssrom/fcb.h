/*
 * Data structures used in file system manipulation
 */

/*
 * Copyright (c) 1990, Applix pty limited. Andrew Morton.
 */
#ifndef FCB_H
#define FCB_H


#ifndef MAXPIDS			/* Bit of wizardry to simplify inclusion */
#	define MAXPIDS	64
#	define UNDOMAXPIDS
#endif

struct apipe
{
	ushort head, tail;	/* Buffer pointers */
	uchar data[PIPEBUFSIZE];
	uchar flags;		/* Status */
};
struct pipefcb
{
	uchar isoutput;		/* PIPEINPUT, PIPEOUTPUT or 0 if closed */
	struct apipe *apipe;	/* The common I/O structure */
};

/* File control block structure */
typedef struct fcb
{
	char *file_name;	/* The full pathname */
	ushort openmode;	/* Mode with which file was opened/created */
	ushort bdvrnum;		/* Appropriate driver number */
	ushort *blkmap;		/* Pointer to the file's block map */
	ushort bmindex;		/* Current index into blkmap */
	char *blkbuf;		/* Pointer to buffer for disk I/O */
	ushort dirblk;		/* Block which holds file's directory entry */
	ushort dirindex;	/* The position in the directory block */
	uint file_size;		/* Accumulated length */
	uint file_pos;		/* Where we are */
	ushort blkmapblk;	/* Where the file's block map starts on disk */
	ushort changed;		/* Flag: current block must be written out */
	ushort blkstat;		/* Flag: 0: block must be read */
	ushort nusers;		/* Number of processes using file */
	char owners[MAXPIDS];	/* pids who own or have inherited it */
	struct pipefcb *pipefcb; /* If non-zero, it is a pipe */
	ushort uid;		/* uid the file will get */
	ushort needsflush;	/* Altered since last close(0x80000000 | fd) */
} FCB ;

#ifdef UNDOMAXPIDS
#	undef MAXPIDS
#	undef UNDOMAXPIDS
#endif

#endif
