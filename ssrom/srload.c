/*
 * S record loader
 */

#include <types.h>
#include <syscalls.h>
#include <storedef.h>
#include <files.h>
#include <blkerrcodes.h>

uchar cksm;		/* S-record checksum */
ushort loading;		/* Set if we are loading S records */
char vbose;

static rx_sheader(), rx_hbyte(), rx_nibble();

/*
 * Interpret S records from standard input
 */

int load_srec(int argc,char *argv[])
{
	register int ofd;
	register int retval = 0;
	register char *ofname;
	int cnt;			/* Position in current record */
	char *mem_ptr;			/* Where code is going */
	char *firstaddr;		/* Entry point */
	char ch;
	struct dir_entry dirent;
	int nloaded;

	vbose = verbose();
	nloaded = loading = 0;

	if (argc == 2)
	{
		ofname = argv[1];
		if ((ofd = CREAT(ofname, 0, 0)) < 0)
			return cantcreat(ofname, ofd);
	}

	while (rx_sheader(&firstaddr, &cnt))
		;
	loading = 1;

	mem_ptr = firstaddr;		/* Code entry point */

	do
	{
		if (vbose)
			PRINTF("\rLoad address = $%X", mem_ptr);
		do
		{
			if (argc == 1)
				*mem_ptr++ = rx_hbyte();
			else
			{
				ch = rx_hbyte();
				if ((retval = WRITE(ofd, &ch, 1)) < 0)
				{
					cantwrite(ofname, ofd, retval);
					goto err;
				}
			}
			nloaded++;
		}
		while (--cnt);
		rx_hbyte();
		if (cksm != 0xff)
		{
			eprintf("\rS-record checksum error.\r\n");
			goto aborted;
		}
	}
	while (!rx_sheader(&mem_ptr, &cnt));

	if (vbose)
	{
		prcrlf();
		prloaded(nloaded, firstaddr);
	}
err:
	if (vbose)
		prcrlf();
	if (argc == 2)
	{
		if ((retval = doclose(ofname, ofd)) < 0)
			goto aborted;
		/* Diddle load address */
		if ((retval = FILESTAT(ofname, &dirent)) < 0)
		{
			cantread(ofname, ofd, retval);
			goto naborted;
		}
		dirent.load_addr = firstaddr;
		if ((retval = PROCESSDIR(ofname, &dirent, 3)) < 0)
		{
			cantwrite(ofname, ofd, retval);
			goto naborted;
		}
	}
	goto naborted;
aborted:
	if (vbose)
		prcrlf();
	if (argc == 2)
		retval = CLOSE(ofd);
naborted:
	ding();
	return retval;
}

/*
 * Read an S header. Return true if it is S9. Return the address and count
 */

static
rx_sheader(address, cnt)
char **address;
int *cnt;
{
	register uint ch, i;
	register uint addr;

wsrec:	do
	{
		do
		{
			ch = GETCHAR();
			if (!loading && vbose)
				PUTCHAR(ch);
		}
		while (ch != 'S');
		ch = GETCHAR();
		if (!loading && vbose)
			PUTCHAR(ch);
		i = ch - '0';
	}
	while (i > 9);
	if (i > 6) return 1;	/* S7, S8 or S9 */
	if ((i == 0) || (i > 3)) goto wsrec;

/* It is an S1, S2 or S3 record */
	cksm = 0;
	*cnt = rx_hbyte() - 1;	/* Allow for Length byte */
	addr = 0L;

	do
	{
		addr = (addr << 8) | (rx_hbyte() & 0xff);
		(*cnt)--;
	}
	while (i--);

	*address = (char *)addr;
	return 0;
}

/*
 * Read a hex ascii byte, updating checksum
 */

static
rx_hbyte()
{
	char temp;

	temp = rx_nibble() << 4;
	temp |= rx_nibble();
	cksm += temp;
	return temp;
}

/*
 * Read a hex nibble
 */

static
rx_nibble()
{
	return hdtoi(GETCHAR());
}
