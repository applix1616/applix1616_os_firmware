/*
 * Binary program loading code
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <reloc.h>
#include <files.h>
#include <blkerrcodes.h>


#include "blockio.h"
#include "newmem.h"
#include "dirfns.h"
#include "monfns.h"
#include "nomemfor.h"
#include "spspace.h"
#include "psprintf.h"
#include "movmem.h"
#include "setmem.h"
#include "loadreloc.h"

int execstackspace;
extern uint maxfiles;
extern int ouruid;

int dofload(char *path,int gmmode,int verbose,int *pstackspace);
int _dofload(char *path,int gmmode,int verbose,int * pstackspace);


/*
 * System call 11: load in relocatable code, relocating as we go.
 */

int _loadrel(uint ifd,int addr)
{
	if (ifd >= (NIODRIVERS + maxfiles) || (ifd < NIODRIVERS))
		return BEC_BADARG;	/* Bad file ID */
	return loadreloc(ifd, addr,(char *) FILESTAT(ifd, 0), 0);
}

/*
 * System call 69: allocate memory for file, pull it in, relocate it and return
 * the start address. We are passed the pathname and the memory allocation mode.
 * If it is an exec file, try to load at the address specified in the directory
 * entry.
 *
 * If it is neither, return error
 */

int _floadrel(char *path,int gmmode)
{
	int stackspace;
	return dofload(path, gmmode, 0, &stackspace);	/* Not verbose mode */
}

/*
 * If verbose == 1, print things out. Set user id to 0 to permit loading
 */

int dofload(char *path,int gmmode,int verbose,int *pstackspace)
{
	int _ouruid, retval;
	char *fullpath;

	/*
	 * Do the assignment expansion as the correct UID so the
	 * assignment expansion works correctly
	 */
	fullpath = (char *)GETFULLPATH(path, 0);
	if ((int)fullpath < 0)
		return (int)fullpath;
	_ouruid = ouruid;
	ouruid = 0;
	retval = _dofload(fullpath, gmmode, verbose, pstackspace);
	ouruid = _ouruid;
	dofreemem(fullpath);
	return retval;
}

int _dofload(char *path,int gmmode,int verbose,int * pstackspace)
{
	register int i;			/* Must be signed */
	register int retval;
	register int laddr;
	register int ifd;
	int isexecfile;
	int *laddrsave;
	struct rel_header rel_header;
	struct dir_entry dirent;
	extern char *suffixes;

	/* Work out if it is .EXEC or .XREL */
	i = strlen(path);
	if ((i -= 5) < 0)
		goto badpath;
	if (!strucmp(path + i,&suffixes[2]))	/* .EXEC */
		isexecfile = 1;
	else
		if (!strucmp(path + i, &suffixes[0]))	/* .XREL */
			isexecfile = 0;
		else
badpath:		return BEC_FNBAD;	/* Not .xrel or .exec */

	if (!isexecfile)
	{	/* Load the .xrel file */
		if ((ifd = OPEN(path, SSO_RDONLY)) < 0)
		{
			if (verbose)
				cantopen(path, ifd);
			return ifd;
		}
		retval = READ(ifd, &rel_header, sizeof(rel_header));
		if (retval != sizeof(rel_header))
		{
			if (retval >= 0)
				retval = BEC_BADMAGIC;
			if (verbose)
				cantread(path, ifd, retval);
			CLOSE(ifd);
			return retval;
		}
		if (rel_header.magic1 != RELMAG1)
		{
			if (verbose)
				cantread(path, ifd, BEC_BADMAGIC);
			CLOSE(ifd);
			return BEC_BADMAGIC;
		}
		if ((laddr = dogetmem(rel_header.textlen + rel_header.datalen +
			rel_header.bsslen + 2, gmmode, path)) < 0)
		{
			CLOSE(ifd);
			return BEC_NOBUF;
		}
		*pstackspace = rel_header.stackspace;
		if (*pstackspace == 0)
			*pstackspace = BIN_STACKSPACE;
		SEEK(ifd, 0, 0);	/* Back to start */
		if ((retval = loadreloc(ifd, laddr, path, 0)) < 0)
		{
			CLOSE(ifd);
			dofreemem((void *)laddr);
			if (verbose)
				cantread(path, ifd, retval);
			return retval;
		}
		CLOSE(ifd);
		return laddr;
	}

	/* It is a .exec file */
	if ((retval = FILESTAT(path, &dirent)) < 0)
	{
		if (verbose)
			cantopen(path, retval);
		return retval;
	}
	laddr = (int)dirent.load_addr;
	if (GETFMEM(laddr, dirent.file_size, gmmode) != laddr)
	{
		if (verbose)
			nomemfor(path);
		return BEC_NOBUF;
	}

	laddrsave = (int *)laddr;
	laddr = mload(0, 0, 0, 1 + verbose, path);

	/* 4.2 bug fix */
	if (laddr < 0)
		dofreemem(laddrsave);

	*pstackspace = execstackspace;
	return laddr;
}

/*
 * Load from the open file, returning the next free address or an error code
 * If 'copyheader' == 1, copy the header info to RAM. Clear the BSS area.
 */

int loadreloc(int ifd,int addr,char *fname,int copyheader)
{
	register int bufindex;
	register uchar ch;
	register uchar *buf;
	int intptr;
	uchar _ch;
	int _intptr;
	struct rel_header rel_header;
	int retval, readlen;
	uchar _buf[BLOCKSIZE];

	if (addr & 1)
		return -1;	/* Odd addr */

	if ((retval = READ(ifd, &rel_header, sizeof(rel_header))) < 0)
		goto badread;

	if (rel_header.magic1 != RELMAG1)
	{
		eprintf("Bad header magic\r\n");
		return -1;
	}

/* Copy the header into memory */
	if (copyheader)
	{
		movmem(&rel_header, (void *)addr, sizeof(rel_header));
		addr += sizeof(rel_header);
	}

/* Load the code in */
	readlen = rel_header.textlen + rel_header.datalen;
	if ((retval = READ(ifd, addr, readlen)) != readlen)
		goto badread;

/* Clear the BSS */
	clearmem((void *)(addr + readlen), rel_header.bsslen);

/* Seek past the symbols */
	if ((retval = SEEK(ifd, rel_header.symtablen, 1)) < 0)
		goto badread;

/* Do the relocations */
	if ((retval = READ(ifd, &_intptr, 4)) != 4)
		goto badread;
	intptr = _intptr;
	if (intptr)
	{
		buf = _buf;
		bufindex = BLOCKSIZE;
		intptr += addr;		/* Real address */
		do
		{
			*((int *)intptr) += addr - rel_header.text_begin;
rdagain:		if (copyheader)
			{	/* Reading MRDRIVERS, must do exact read */
				if ((retval = READ(ifd, &_ch, 1)) != 1)
					goto badread;
				ch = _ch;
			}
			else
			{	/* Buffer the relocation tables */
				if (bufindex == BLOCKSIZE)
				{
					if ((retval = READ(ifd,buf,BLOCKSIZE))<0)
						goto badread;
					bufindex = 0;
				}
				ch = buf[bufindex++];
			}
			if (ch == 1)
			{
				intptr += 254;
				goto rdagain;
			}
			else
				intptr += ch;
		}
		while (ch);
	}
	return addr + readlen + rel_header.bsslen;

badread:
	if (retval < 0)
		return cantread(fname, ifd, retval);
	else
	{
		eprintf("Truncated xrel file\r\n");
		return -1;
	}
}
