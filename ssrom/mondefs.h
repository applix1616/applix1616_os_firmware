/*
 * Monitor definitions
 */

#define MAXMONPAR	256	/* Maximum number of parameters */

/* Definitions for monitor command front end */
#define EMPTY		1	/* Possible entries in argtype */
#define NUMBER		2
#define STRING		4
#define QSTRING		8

/* Definitions for command interpreter */
#define NMONCMDS	80

/* Number of execution paths */
#define NXPATHS		10

/* Number of data paths */
#define NDPATHS		10

/* Maximum length of an execution or data path */
#define MAXPATHLENGTH	32

/*
 * Editor stuff
 */

#define MAXIPLINE	512

struct led_buf
{
  uint idev;
  uint odev;
  uint edev;
  char *buf;
};

#define QUITCODE	0x126fde89

/*
 * Some definitions for the alias functions in the AEXECA
 * system call.
 */

#define NALIASES	16	/* Maximum that can be installed */

#define AL_SETVEC	0	/* Install a vector */
#define AL_READVEC	1	/* Read a vector */
#define AL_RMVEC	2	/* Remove a vector */
#define AL_VECTABLE	3	/* Return table address */
#define AL_TABLESIZE	4	/* Return size of table */
#define AL_RMALL	5	/* Remove all aliases */
