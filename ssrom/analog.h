/*
 * Analog I/O functions
 */
#ifndef ANALOG_H
#define ANALOG_H


extern void anio_init(void);
extern void lbeep(int len, int low);

#endif
