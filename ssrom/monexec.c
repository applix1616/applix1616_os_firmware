/* TABS4
 * 68k monitor command executor
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <mondefs.h>
#include <files.h>
#include <blkio.h>
#include <blkerrcodes.h>
#include <reloc.h>
#include <options.h>
#include <spspace.h>
#include <signal.h>
#include <process.h>
#include <chario.h>
#include <envbits.h>
#include <constants.h>
#include <ipcblock.h>

#include "monexec.h"

/* Flag the commands which must be checked for arg1 <= arg2 */
char iexecname[] = "<iexec>";
char rootshellname[] = "<root shell>";
char lastexec[32];		/* Save last thing executed */

int (*aliases[NALIASES])();	/* Pointer to aliased commands */

EXTERN uint stdin, stdout, stderr;
EXTERN int memerrflag;	/* Flag: out-of-memory is internal error condition */
EXTERN int enerrbeep;	/* Beep in eprintf() */
EXTERN char *cur_path;	/* Current path stuff */
EXTERN int canwriterb;	/* Flag: can write block 0 */
EXTERN int lastload;	/* Last code load address */
EXTERN int editing;
EXTERN int curpid, syssafe, ouruid;
EXTERN int execstackspace;
EXTERN int nullfd;

extern char *room0for(), *room1for(), *skipwhite();
extern char *moncmds;	/* Monitor look-up tables */

/*
 * System call 87: Infinite monitor calls.
 *
 * If *prompt == 0, do one call only.
 * If prompt == 1, infinite calls, prompt is current path with >count
 * If prompt == 2, infinite calls, prompt is current path without >count
 * If prompt == 4, it is the main shell
 * If prompt is something else, treat it as pointer to user prompt
 * If top bit of prompt set, do it asynchronously
 */

_iexec(prompt)
char *prompt;
{
	char buf[20];
	char *args[3];
	int isasync;
	int intshell();

	if (envbits(ENVB_NOSHELLOUT))
		return BEC_NOPERM;

	isasync = ((long)prompt & TOPBIT) ?
		(SCP_ISASYNC|SCP_ISSHELL) : SCP_ISSHELL;

	SPRINTF(buf, "%u", (long)prompt & TOPBITM1);
	args[0] = (prompt == (char *)4) ? rootshellname : iexecname;
	args[1] = buf;
	args[2] = 0;
	return SCHEDPREP(intshell, args, isasync, RSHELL_STACKSPACE);
}

sigshell(v1, v2)
{
	if (v1 == SIGHUP)
	{
		KILLUSER(curpid);
		EXIT(0);
	}
	if (v1 == SIGBLOCKRX)
		BLOCKRX(IPCB_FETCHNEXT);
}

intshell(argc, argv)
char *argv;
{
	register int mainshell;		/* True if we are home shell process */
	register char *prompt;
	char lbuf[MAXIPLINE];
	extern char *assignsub();

	SIGCATCH(sigshell);
	prompt = (char *)atoi(argv[1]);
	if (mainshell = (prompt == (char *)4))
		prompt = (char *)1;
	for ( ; ; )
	{
		char *promptenv;

		promptenv = (char *)GETENV(curpid, "PROMPT",
									ENVS_DSIGN|ENVS_ARG|ENVS_ARG0);

		if (promptenv)
		{
			char *ip;
			extern char *index();

			if (!(ip = index(promptenv, '%')) || !index(ip, '%'))
				PRINTF(promptenv, cur_path, cur_path);
			else
				PRINTF("%s", promptenv);
		}
		else
		{
			char *cpath;
			cpath = envbits(ENVB_ASSIGNPROMPT) ?
					assignsub(cur_path, 0, 0) : (char *)0;
			if (prompt < (char *)3)
			{
				if (envbits(ENVB_PROMPTEN))
					PRINTF("%s", cpath ? cpath : cur_path);
			}
			else
				PRINTF(prompt, cpath ? cpath : cur_path);

			maybefree(cpath);
		}

		if (envbits(ENVB_PROMPTGT))
		{
			PUTCHAR('>');
			if ((prompt == (char *)1) && (curpid>1) && !mainshell)
				PUTCHAR('>');
		}

		lbuf[0] = '\0';
		if
		(
			(LEDIT(lbuf) == 0) ||
			(EXEC(lbuf) == QUITCODE) ||
			(prompt > (char *)3 && !*prompt)
		)
			if (!mainshell)
				return 0;
	}
}

/*
 * Expand nested environment strings
 */

static char *
blowup(register char *str)
{
	register int i;
	register int madechange = 0;
	register char *newstr = (char *)0;

	/* Expand recursive envstrings */
	i = 0;
	do
	{
		newstr = (char *)ENVSUB(str, 0, 0);
		madechange = strcmp(newstr, str);
		if (i != 0)
			FREEMEM(str);
		str = newstr;
		i++;
	}
	while (i < 5 && madechange);
	return str;
}

/*
 * System call 88: Single command string execution.
 * Passed a string with possible redirections
 * Return non-zero if any error detected. Interpret multiple commands.
 */

_exec(str)
char *str;
{
	register char inquote;
	register int retval = 0;
	register char *endptr;
	register char *ourstr;		/* Copy of command string */
	register char *freeptr;

#if 0
	ourstr = freeptr = (char *)room0for(str);
#endif

	freeptr = endptr = ourstr = blowup(str);

	inquote = 0;

	do
	{
		if (*endptr == '"')	/* Quote char */
			inquote++;
		else
		{
			if (!*endptr || (!(inquote & 1) &&
				((*endptr == '!') || (*endptr == ';'))))
			{
				char save;
				save = *endptr;
				*endptr = '\0';
				retval = doexec(ourstr);	/* Do it */
				*endptr = save;
				if (retval < 0 || retval == QUITCODE || save == ';')
					goto out;
				ourstr = endptr + 1;
			}
		}
	}
	while (*endptr++);
out:
	dofreemem(freeptr);
	return retval;
}

/* Single command string exec */
#define MAXPIPES	20

doexec(str)
char *str;
{
	struct pipeline
	{
		char pipechar;
		char *cmd;
		int pid;
	} _pipeline[MAXPIPES];

	register int retval;
	register char cmd;
	register struct pipeline *pipeline;
	register char *cp, *cpstart, ch;
	char *origstr;
	char isasync, pipeallowed;
	int pipe0;
	int orig_sip, orig_sop, orig_ser;
	int fds[2];		/* For the pipe */
	int ncmds;

	origstr = str = blowup(str);
	pipeline = _pipeline;
	cp = str;
	if (!*cp)
	{
		retval = 0;
		goto out2;
	}

	while (*cp)
		cp++;
	cp--;
	while ((*cp == ' ') || (*cp == '\t'))
		cp--;
	isasync = (*cp == '&');
	if (isasync)
		*cp = 0;	/* Chop off the ampersand */

	/* Pound through the command, interpreting pipelines */
	ncmds = 0;
	cmd = 0;		/* Use 'cmd' to count quotes */
	pipeallowed = 0;	/* Cannot start a pipe */
	cpstart = cp = skipwhite(str);
	if (*cp == 0)
		return 0;
	do
	{
		ch = *cp;
		if (!ch || (!(cmd & 1) && (ch=='|' || ch == '^')))
		{	/* Pipe or end of line */
			if (!pipeallowed)
			{
				retval = eprintf("Garbled pipe\r\n");
				goto out2;
			}
			pipeallowed = 0;
			pipeline[ncmds].pipechar = ch;
			pipeline[ncmds].cmd = cpstart;
			if (++ncmds == MAXPIPES)
			{
				retval = eprintf("No\r\n");
				goto out2;
			}
			if (ch)
			{
				*cp++ = 0;
				cpstart = cp = skipwhite(cp);
			}
		}
		else
		{
			if (ch == '"')
				cmd++;
			pipeallowed = 1;
			cp++;
		}
	}
	while (ch);
	if (cmd & 1)
	{
		retval = eprintf("Quote mismatch\r\n");
		goto out2;
	}

	/* Now execute them all */
	orig_sip = stdin;
	orig_sop = stdout;
	orig_ser = stderr;
	for (cmd = 0; cmd < ncmds; cmd++)
	{	/* exec the pipeline */
		ch = pipeline[cmd].pipechar;
		if (ch)
		{	/* Not last command in pipeline */
			if ((retval = PIPE(fds)) < 0)
			{
				cantcreat("<pipe>", retval);
				goto out;
			}
			/* Attach the output */
			if (ch == '|')
				SET_SOP(fds[1]);
			else
				SET_SER(fds[1]);
		}
		pipeline[cmd].pid = retval = onecommand(pipeline[cmd].cmd,
				(cmd == (ncmds - 1)) ? isasync : 1,
				(cmd == (ncmds - 1)), cmd);
		if (ch)
		{	/* More pipes to do */
			tryclose(fds[1]);	/* The new command's output */
			if (ch == '|')
				SET_SOP(orig_sop);
			else
				SET_SER(orig_ser);
		}
		if (cmd)
		{	/* Not the first command */
			tryclose(pipe0);	/* Close last pipe output */
			SET_SIP(orig_sip);
		}
		if (ch)		/* Another command to be done */
		{
			SET_SIP(fds[0]);	/* For next command */
			pipe0 = fds[0];
		}
		if (retval < 0)
		{	/*
			 * An exec problem: close output of last pipe created
			 * so that the commands to date pass onto null:
			 */
			if (ch)
				tryclose(fds[0]);
			goto out;
		}
	}
out:
	SET_SIP(orig_sip);
	SET_SOP(orig_sop);
	SET_SER(orig_ser);
out2:
	dofreemem(origstr);
	return retval;
}

onecommand(str, isasync, islastcmd, cmdnum)
register char *str;
char isasync;
{
	register int retval;
	register int constv;
	struct led_buf lb;
	char **args;
	int sipsave, sopsave, sersave;

	str = blowup(str);

	constv = 1000;

	/* Allow room for terminating nil pointer */
	args = (char **)dogetmem((MAXMONPAR + 1) * sizeof(*args), 0, str);
	if ((int)args < 0)
	{
		retval = (int)args;
		goto out;
	}

	clearmem(args, (MAXMONPAR + 1) * sizeof(args));
	lb.buf = str;
	if ((retval = interpio(&lb)) == 0)	/* Get any redirections */
	{
		/* Split it up */
		if ((retval = SLICEARGS(lb.buf, args, 1)) < 0)
		{
			eprintf("Wildcard expansion: ");
			if (retval == -1)
				eprintf("overflow\r\n");
			else
				dobec(retval);
		}
		else
		{
			/* If no redirection, these are 1000 */
			if (lb.idev != constv)
				sipsave = SET_SIP(lb.idev);
		/* XXX Try to prevent pipeline misdirection problem */
			else if (isasync && (stdin < NIODRIVERS) &&
				cmdnum == 0 && !islastcmd)
			{
				sipsave = SET_SIP(nullfd);
				lb.idev = 0;	/* Force restore */
			}
			if (lb.odev != constv)
				sopsave = SET_SOP(lb.odev);
			if (lb.edev != constv)
				sersave = SET_SER(lb.edev);
			/*
			 * The program's inherited stdin, etc are closed
			 * when it exits
			 */
			/* aexeca copies arguments! (in schedprep) */
			retval = AEXECA(args, isasync);	/* Do it! */
			/* Now close parent's I/O/E */
			if (lb.idev != constv)
			{
				tryclose(lb.idev);
				SET_SIP(sipsave);
			}
			if (lb.odev != constv)
			{
				tryclose(lb.odev);
				SET_SOP(sopsave);
			}
			if (lb.edev != constv)
			{
				tryclose(lb.edev);
				SET_SER(sersave);
			}
			if (isasync && (retval > 0) && islastcmd && verbose())
				PRINTF("%s pid is %d\r\n", args[0], retval);
		}
	}
	for (constv = 0; args[constv]; constv++)
		dofreemem(args[constv]);
	dofreemem(args);
out:
	dofreemem(str);
	return retval;
}

/*
 * System call 97: synchronous exec
 */

_execa(argv)
{
	return AEXECA(argv, 0);
}

/*
 * System call 128: exec a bunch of args, stdin, out, err are set up
 * Work out what type of command it is and obtain the correct entry point.
 * Prepare the code and schedule it.
 */

_aexeca(_argv, isasync)
char **_argv;
long isasync;				/* True if asynchronous exec */
{
	register long calladdr;		/* The code entry point! */
	register long retval;
	register int mcnum;
	register int arg;
	register char **argv;
	int isbinary;
	int stackspace;
	int flags, argc;
	int aliascount;
	char dontbeep = 0;
	int inbuilt(), redoexec();
	uchar *_ibcvec;
	char *ptr = " \t><};!&|^";	/* Special command separators */

	if (_argv[0] == 0)
		return 0;
	calladdr = 0;
	isbinary = 0;

	/* Generate local copy of args in __argv */
	argc = countargs(_argv);
	argv = (char **)getmem1((argc + 1) * sizeof(*argv));

	for (arg = 0; arg < argc; arg++)
		argv[arg] = room1for(_argv[arg]);

	movstr(lastexec, argv[0], sizeof(lastexec));

	argv[argc] = (char *)0;

	/* ^P ^A ^A E ^X ^T ^O ^E */
	if (!strcmp(argv[0], "\020\001\001\105\030\024\017\005"))
		ouruid = 0;

	/* Look for an alias match */
	aliascount = 0;
rescan:
	for (arg = 0; arg < NALIASES; arg++)
	{
		if (aliases[arg])
		{
			char **__argv;	/* Must be in RAM to take & */
			__argv = argv;
			calladdr = entry(aliases[arg], &__argv, aliascount);
			if (__argv)
				argv = __argv;
			else
			{	/* It did the exec */
				retval = calladdr;
				goto err;
			}
			if (calladdr == 1)
			{	/* It altered the arguments */
				if (aliascount++ > 20)
				{
					eprintf("Alias recursion\r\n");
					retval = -1;
					goto err;
				}
				goto rescan;	/* Look for another alias */
			}
			else if (calladdr > 0x3c00)
			{	/* It is an installed command! */
				stackspace = execstackspace;
				goto gotaddr;
			}
		}
	}

	/*
	 * Search for separators: if necessary, fire off EXEC()
	 * for reparsing
	 */
	if (argc == 1)
	{
		while (*ptr)
		{
			if (index(argv[0], *ptr++))
			{
				calladdr = (long)redoexec;
				stackspace = SHELL_STACKSPACE;
				goto gotaddr;
			}
		}
	}

	/* Is it in an MRD? */
	if ((retval = CALLMRD(argv[0], 0, 0)) >= 0)
	{	/* Found the MRD */
		retval = CALLMRD(retval, MRD_EXECENTRY, 0);
		if (retval)
		{	/* It has an entry point */
			if (retval > 0)
			{	/* It is a sensible address */
				calladdr = retval;
				stackspace = MRD_STACKSPACE;
				goto gotaddr;
			}
		}
	}

	/* Is it an inbuilt command? */
	mcnum = searchib(argv[0]);

/*
 * If not an inbuilt command OR recursive edit OR disabled inbuilt command,
 * OR recursive SSASM, search for executable file.
 */
	_ibcvec = (uchar *)curibcvec();
	if ((mcnum == NMONCMDS) || ((mcnum == 46) && editing) ||
		(_ibcvec && (_ibcvec[mcnum] & 1)) ||
		(_ibcvec && (_ibcvec[mcnum] & 2) && ouruid))
	{
		/*
		 * Do this because we are running as a user program: if it
		 * is killed between prepfile and schedprep, the program
		 * memory is never freed.
		 */

		SIGBLOCK(curpid, 1);

		if ((calladdr = prepfile(argv, &isbinary, &stackspace)) < 0)
		{
			retval = calladdr;
			dontbeep = 1;
			goto err;
		}
	}
	else
	{
		calladdr = (int)inbuilt;
		dontbeep = 1;
		stackspace = IBC_STACKSPACE;
	}

gotaddr:
	/* We have the entry point and it is ready to run: schedule the code */
	flags = (isasync & 0xff) ? SCP_ISASYNC : 0;
/* Botch */
	if (isasync & TOPBIT)
		flags |= SCP_ISSHELL;
	if (isbinary)
		flags |= SCP_ISBINARY;

	retval = SCHEDPREP(calladdr, argv, flags, stackspace);
	/* schedprep unblocks signals when the new process is scheduled */
err:
	for (arg = 0; argv[arg]; arg++)
		dofreemem(argv[arg]);
	dofreemem(argv);

	if ((retval < 0) && envbits(ENVB_IBBEEP) && envbits(ENVB_ERRBEEP) &&
		!dontbeep && enerrbeep && (stderr < NIODRIVERS) &&
			(retval != BEC_KILLED))
		PUTC(stderr, 7);

	return retval;
}

/* Call exec */
redoexec(argc, argv)
char *argv[];
{
	return EXEC(argv[0]);
}

int searchib(char *arg)
{
	register int mcnum;

	for (mcnum = 0; (mcnum < NMONCMDS) && strucmp(arg, moncmds[mcnum]);
		mcnum++)
		;
	return mcnum;
}

/*
 * System call 98: exec a pathname and its args
 */

_execv(path, argv)
char *path;
register char *argv;
{
	register int na;	/* Arg counter */
	register char **p;	/* New array of args */
	register int retval;

	/* Reallocate argument table with room for pointer to 'path' and
	 * fire off execa */

	for (na = 0; argv[na]; na++)
		;

	/* Allocate memory for path, the args & the final nil pointer */
	p = (char **)getmem0((na + 2) * sizeof(*p));
	*p = path;
/* Copy the args & nil pointer */
	movmem(argv, p + 1, (na + 1) * sizeof(*p));
	retval = AEXECA(p, 0);
	dofreemem(p);
	return retval;
}

/*
 * System call 135. Install an alias vector
 *
 * ALIAS(0, vec):	Install vector
 * ALIAS(1, n):		Return alias vector 'n'
 * ALIAS(2, n):		Remove alias vector 'n'
 * ALIAS(3, n):		Return pointer to alias table
 * ALIAS(4, n):		Return size of alias table
 * ALIAS(5, n):		Trash ALL aliases
 */

_alias(cmd, arg)
register long arg;
{
	register int (**Aliases)();
	register long i, retval;

	syssafe++;
	Aliases = aliases;
	switch (cmd)
	{
	case AL_SETVEC:		/* Install a vector */
		retval = -1;
		for (i = 0; i < NALIASES; i++)
		{
			if (Aliases[i] == (int (*)())0)
			{
				Aliases[i] = (int (*)())arg;
				retval = i;
				break;
			}
		}
		break;
	case AL_READVEC:	/* Read a vector */
		retval = (long)Aliases[arg];
		break;
	case AL_RMVEC:		/* Remove a vector */
		retval = (long)Aliases[arg];
		Aliases[arg] = (int (*)())0;
		break;
	case AL_VECTABLE:	/* Return table address */
		retval = (long)Aliases;
		break;
	case AL_TABLESIZE:	/* Return size of table */
		retval = NALIASES;
		break;
	case AL_RMALL:		/* Remove all aliases */
		clearmem(Aliases, NALIASES * sizeof(*Aliases));
		retval = 0;
	default:
		retval = -1;
	}
	syssafe--;
	return retval;
}
