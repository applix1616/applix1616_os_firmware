/*
 * Vertical sync interrupt code
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
#include <storedef.h>
//#include <splfns.h>

#ifdef GPCPU
#include <vectors.h>
#include <pit.h>
#include <gpiomap.h>

#define CLOCKFREQ 10000000		/* CPU clock frequency */

/* Set the timer at 50 Hz */
#define TIMER_PRELOAD	(CLOCKFREQ/32)/50
#endif /*GPCPU*/

#ifdef A1616

#include <via.h>

#endif /*A1616*/

#define NVSVECS		32	/* Number of VS interrupt vectors */

struct vsuse
{
  int (*vector)();	/* Code entry point */
  uint rate;		/* Call frequency */
  uint callval;		/* Constant call value */
  uint retval;		/* Value from previous call */
  uint count;		/* Counter for next call */
};

struct vsuse vsuses[NVSVECS];		/* The structures */

/* The vertical sync interrupt counter */
int vsi_count;

/* Max. number of entries in table */
int maxvsent;

#ifdef A1616
/* Flag to assembly code: it WAS a clock tick */
int wastick;
#endif

/*
 * Initialise.
 */

#ifdef GPCPU
vsint_init(reslevel)
{
	int vs_aisr();
	int s;

	s = TIMER_SPL();
	if (!reslevel)
		vsi_count = 0;

	clearmem(vsuses, sizeof(vsuses));

	*((int *) (T_INTVEC*4)) = (int)vs_aisr;	/* Set up vector */

/* Now initialise PI/T timer to produce interrupts */

	movepol(&PIT->p_cpr, TIMER_PRELOAD);
	PIT->p_tivr = T_INTVEC;		/* Set up with the standard vector */
	PIT->p_tcr = P_TOACKVEC | P_CNTLOAD | P_PC2PRESC | P_TIMERGO;
	PIT->p_tcr |= P_TIMERGO;	/* Start the timer */
	maxvsvecs = 0;
	_splx(s);
}
#endif /*GPCPU*/

#ifdef A1616
void vsint_init(void)
{
	int vs_isr();
	int s;

	s = VIA_SPL();
	vsi_count = 0;

	*((int *) VIV_CA2) = (int) vs_isr;
	clearmem(vsuses, sizeof(vsuses));
	maxvsent = 0;

	*V_PCR = (*V_PCR & 0xf1) | 0x06;	/* CA2 positive action */
	*V_IER = 0x81;				/* Enable CA2 interrupt */
	_splx(s);
}
#endif /*A1616*/

/*
 * System call 16: Install a vertical sync vector. Return vector number,
 * else -1 if no room
 * If vec == 0, return start of table
 */

_set_vsvec(vec, rate, callval)
int (*vec)();
{
	register int i;
	register struct vsuse *vsuptr;
	register int s;

	if (vec == 0)
		return (int)vsuses;
	if (vec == (int (*)())1)
		return (int)vsuses + rate;

	i = 0;
	vsuptr = vsuses;
#ifdef GPCPU
	s = TIMER_SPL();
#else
	s = VIA_SPL();
#endif
	while (vsuptr->vector && (i < NVSVECS))
	{
		vsuptr++;
		i++;
	}

	if (i == NVSVECS)
	{
		i = -1;
		goto out;
	}

	vsuptr->rate = rate;
	vsuptr->callval = callval;
	vsuptr->retval = 0;
	vsuptr->count = 0;
	vsuptr->vector = vec;
	if (i > maxvsent)
		maxvsent = i;
out:	_splx(s);
	return i;
}

/*
 * System call 17: Remove a previously installed VS vector, returning the old value.
 */

_clr_vsvec(vecnum)
int vecnum;
{
	if (vecnum < NVSVECS)
	{
		vsuses[vecnum].vector = 0;
		return 0;
	}
	else
		return (int)vsuses;
}

/*
 * System call 18 entry point: Return number of ticks since startup
 */

_get_ticks()
{
	return vsi_count;
}

/*
 * C part of VIA ISR. Scan vector table, calling the code pointed to by
 * all installed vectors.
 */

vs_isr()
{
	register int (*cptr)();		/* Code pointer */
	register struct vsuse *vsuptr, *lastptr;

	vsi_count++;
#ifdef GPCPU
	PIT->p_tsr = P_TZDS;	/* Clear the timer interrupt */
	if ((vsi_count & 15) == 0)
		*LATCH ^= LED;
#endif /*GPCPU*/

#ifdef A1616
	*V_IFR = 0x01;		/* Clear the interrupt bit */
	wastick = 1;
#endif /*A1616*/

	vsuptr = vsuses;
	lastptr = vsuses + maxvsent;
	while (vsuptr <= lastptr)
	{
		if (cptr = vsuptr->vector)
		{
			if (++(vsuptr->count) >= vsuptr->rate)
			{	/* Time to call it */
				vsuptr->count = 0;
				vsuptr->retval = (*cptr)(vsuptr->callval,
					vsuptr->retval);
			}
		}
		vsuptr++;
	}
}
