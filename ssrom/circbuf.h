/* TABS4
 * Circular buffer managing functions (chars only)
 */
#ifndef CIRCBUF_H
#define CIRCBUF_H



/*
 * Circular buffer structure
 */
typedef struct circ_buf
{
	uint head;	/* Offset from buf where next byte is written to */
	uint tail;	/* Offset from buf where next byte is read from */
	uint cbsize;	/* Size of the buffer */
	uchar *buf;	/* Pointer to the data */
	short freeable;	/* If true, *buf is allocated mem */
} CIRCBUFFER;

extern void cbuf_init(void);
extern int cb_bnum(CIRCBUFFER *cb);
extern int cb_bfree(CIRCBUFFER *cb);
extern char get_cbuf(CIRCBUFFER *cb);

#endif
