/* TABS8 */
/*
 Applix 1616/OS ROM assembler startup code

 Copyright (C) 1987 by Applix pty limited

 28 Jan 1987. Programmer: Andrew K.P. Morton
*/

#include "asm68k.h"

	.data
pwruplongs:		ds.l 1
obramstart: 	ds.l 1
obramsize:		ds.l 1
is68010:			ds.l 1
excepmes:			ds.l 1
srsave:				ds.l 1
pcsave:				ds.l 1
regsave:			ds.l 1
fcsave:				ds.l 1
accaddr:			ds.l 1
irsave:				ds.l 1
wastype0:			ds.l 1
excepsyscall:	ds.l 1
badsyscall:		ds.l 1


	.text

.equ BERR_VEC, 8	/* Bus error vector */
.equ ILLEGALVEC, 16	/* Illegal instruction vector */

.equ ROMSTART,	0x500000	/* Start of ROMs */

.equ CENTLATCH, 0x600001 /* Centronics latch */
.equ DACLATCH,	0x600081	/* DAC latch */
.equ VIDLATCH,	0x600101	/* Video latch */
.equ AMUXLATCH,	0x600181	| AMUXLATCH	equ	$600181	; Analog multiplexor latch
.equ PAL0,	0x600000	| PAL0		equ	$600000	; Pallette entries
.equ PAL1,	0x600020	| PAL1		equ	$600020
.equ PAL2,	0x600040	| PAL2		equ	$600040
.equ PAL3,	0x600060	| PAL3		equ	$600060

.equ SCCBASE,	0x700000	| SCCBASE		equ	$700000	; SCC start
.equ IPORT,	0x700081	| IPORT		equ	$700081	; Input port
.equ VIABASE,	0x700100	| VIABASE		equ	$700100	; VIA start
.equ CRTCBASE,	0x700180	| CRTCBASE	equ	$700180	; CRTC start

.equ SCCBC,	SCCBASE+0	| SCCBC		equ	SCCBASE+0	; SCC B control
.equ SCCBD,	SCCBASE+2	| SCCBD		equ	SCCBASE+2	; SCC B data
.equ SCCAC,	SCCBASE+4	| SCCAC		equ	SCCBASE+4	; SCC A control
.equ SCCAD,	SCCBASE+6	| SCCAD		equ	SCCBASE+6	; SCC A data

.equ CRTCADDR,	CRTCBASE	| CRTCADDR	equ	CRTCBASE	; CRTC address register
.equ CRTCDATA,	CRTCBASE+2	| CRTCDATA	equ	CRTCBASE+2	; CRTC data register

.equ V_BREG,	VIABASE		| V_BREG		equ	VIABASE		; Port B I/O register
.equ V_AREG,	VIABASE+2	| V_AREG		equ	VIABASE+2	; Port A I/O register
.equ V_DDRB,	VIABASE+4	| V_DDRB		equ	VIABASE+4	; Port B DDR
.equ V_DDRA,	VIABASE+6	| V_DDRA		equ	VIABASE+6	; Port A DDR

;
|; Start of code
;
	.extern _initsp, _pwruplongs, main


	.global	__coldboot,__warmboot
	.global	_asbootup,_excepsyscall
	.global	_lastsyscall,_obramsize
	.global	_user_patch, user_patch

/* #define MDEBUG */

#ifdef MDEBUG
	.global	_Tloc
	.comm	_Tloc,4

.macro D nn
	move.l	a0,0x4100
	move.l	_Tloc,a0
	move.l	#\nn,(a0)
	move.l	a0,_Tloc
	move.l	0x4100,a0
.endm

/* #define D(nn)				\
	move.l	a0,$4100	!	\
	move.l	_Tloc,a0	!	\
	move.l	#nn,(a0)+	!	\
	move.l	a0,_Tloc	!	\
	move.l	$4100,a0
*/

#else
#define D(nn)
#endif

/*****************************************
* everything starts here
******************************************/
_asbootup:
bootup:
	bra	tootle

user_patch:
_user_patch:
	dc.l	0		/* This is for user-patched ROMs*/

tootle:
#ifdef MDEBUG
	move.l	#0x11000,_Tloc
#endif
	move.b	d0,ROMSTART		/* Write to ROM Bank out ROMs */
	or.w		#0x0700,sr		/* priority level 7 spl7 */
	move.l	_initsp,a7		/* Set up Stack pointer (non-reset entry) */
	reset									/* reset the everthing else on reset line */
D(1)
dofn:
	move.b	IPORT,d0		/* If switch 3 open */
	bmi	toc							/* set things up and branch to C */

	move.b	d0,DACLATCH		| Echo it
	lsr.b	#4,d0			/* Ignore bottom 4 bits */
	and.l	#7,d0			| Only use 3 bits
	asl.l	#2,d0
	move.l	#ijtab,a0
	move.l	0(a0,d0.w),a0		/* Get code address */
	jmp	(a0)			| Go to it


/***********************/
__coldboot:
	clr.l	pwruplongs		/* mon.c Ensure level 0 reset: zap the power up words */
	jmp		bootup

ijtab:
		dc.l	latchtest	/* 0: Test analog, video, dac, cent, pallette */
		dc.l	sccrwtest	/*; 1: SCC */
		dc.l	viarwtest	/*; 2: VIA */
		dc.l	crtcrwtest	/*; 3: CRTC */
		dc.l	iporttest	/*; 4: IPORT */
		dc.l	memrwtest	/*; 5: Read / write memory */
		dc.l	memtest		/*; 6: Memory test */
		dc.l	vidtest		/*; 7: Video test */

/* Write incrementing pattern to latches, pallette */

latchtest:
		clr.b	d3		/* LED / relay bits */
lt3:
		move.l	#30000,d1	/* Loop counter */

lt2:
		move.b	d0,CENTLATCH
		move.b	d0,DACLATCH
		move.b	d0,VIDLATCH

		move.b	d0,d2
		and.b	#0x77,d2		/* Preserve LED & relay bits */
		or.b	d3,d2
		move.b	d2,AMUXLATCH

		move.b	d0,PAL0
		move.b	d0,PAL1
		move.b	d0,PAL2
		move.b	d0,PAL3
		addq.b	#1,d0
		subq.l	#1,d1
		bne	lt2
		eor.b	#0x88,d3
		bra	lt3

/* Read/write SCC */
sccrwtest:
		move.l	#SCCBASE,a0
		bra	rwtest

/* Read/write VIA */
viarwtest:
		move.b	#0xfe,V_DDRA		/* Port A has one input bit. */
		move.b	#0xff,V_DDRB
vrwtest:
		clr.b	d2
vrwtest3:
		move.l	#100000,d1
vrwtest2:
		move.b	d0,V_AREG
		move.b	d0,V_BREG
		move.b	V_BREG,DACLATCH		/* Echo the read back byte */
		addq.b	#1,d0
		subq.l	#1,d1
		bne.b	vrwtest2
		eor.b	#0x8,d2
		move.b	d2,AMUXLATCH
		bra.b	vrwtest3

/* Read/write CRTC */
crtcrwtest:
		move.l	#CRTCBASE,a0
		bra			rwtest

/* Read from Input Port */
iporttest:
		move.l	#IPORT,a0
		bra	rtest

/* Read / write memory */
memrwtest:
		move.l	#0x55aa,a0
		bra	rwtest

/* General input port test: read from port and write to Centronics latch.
; Input port address in A0. */
rtest:
		clr.b	d1
rtest3:
		move.l	#100000,d0
rtest2:
		move.b	(a0),DACLATCH
		subq.l	#1,d0
		bne.b	rtest2
		eor.b	#0x08,d1
		move.b	d1,AMUXLATCH		/*; Flash LED */
		bra.b	rtest3

/*; General output port test: increment latch contents. Latch address in A0 */
wtest:
		clr.b	d2
wtest3:
		move.l	#100000,d1
wtest2:
		move.b	d0,(a0)
		addq.b	#1,d0
		subq.l	#1,d1
		bne.b	wtest2
		eor.b	#0x08,d2
		move.b	d2,AMUXLATCH
		bra.b	wtest3

/* ;General read/write test. Write a value to port, read it back, echo to centronics port,
;increment value & loop */
rwtest:
		clr.b	d2
rwtest3:
		move.l	#100000,d1
rwtest2:
		move.b	d0,(a0)
		move.b	(a0),DACLATCH
		addq.b	#1,d0
		subq.l	#1,d1
		bne.b	rwtest2
		eor.b	#0x08,d2
		move.b	d2,AMUXLATCH
		bra.b	rwtest3

/* ; Do memory test (this simplistic test will shortly be replaced) */
memtest:
		clr.w	d0
		clr.w	d1
		clr.w	d2		/*; Start value */

memtest2:
		move.w	d2,d0
		move.l	#0,a0
fillmem:
		move.w	d0,(a0)+
		addq.w	#1,d0
		cmp.l	#0x80000,a0
		bne.b	fillmem

		move.w	d2,d0
		move.l	#0,a0
checkmem:
		cmp.w	(a0)+,d0
		bne.b	badmem
		addq.w	#1,d0
		cmp.l	#0x80000,a0
		bne.b	checkmem

		add.w	#0x101,d2	/* ; New pattern */
		eor.b	#0x88,d1
		move.b	d1,AMUXLATCH
		bra	memtest2

badmem:
		subq.l	#2,a0		/*; Point to bad word */
		move.l	a0,d0
		move.b	d0,DACLATCH	/*  Low byte of address */
		lsr.l	#8,d0
		move.b	d0,VIDLATCH	/*; Next byte of address */
		lsr.l	#8,d0
readbad2:
		move.b	d0,AMUXLATCH	/*; Bits 16-23 */
		move.l	#30000,d1
readbad:
		move.w	(a0),d2
		subq.l	#1,d1
		bne.b	readbad
		eor.b	#8,d0
		bra.b	readbad2

/*;
; Do Video test
; */
		.global	_cvidtest
vidtest:
		moveq.l	#1,d7
		bra.b	toc2

/*;vidtest:	move.l	#vtnext,a5
;		bra	crtcstart
;vtnext:
;		move.l	#$40000,a7	; Middle of RAM
;		bsr	getcpu		;Ascertain CPU type
;		bsr	setupram
;		clr.l	_obramst:w
;		jmp	_cvidtest
*/



/**********************************************************
* Decription: Code executed when switch 3 not in test mode
***********************************************************/
toc:
		clr.l	d7
toc2:
		move.l	#tocnext,a5
 D(2)
		bra	crtcstart
tocnext:
 D(3)
		bsr	getcpu		/* ;Ascertain CPU type */
 D(4)
		clr.l	d3		/* ; Search for start of on-board RAM */
btloop:		add.l	#0x00100000,d3	/* ; Go in 1 Meg steps */
		move.l	d3,-(a7)
 D(5)
		bsr	_berrtest	/* ; See if there is RAM there */
 D(6)
		addq.l	#4,a7		/*; Adjust stack */
		tst.w	d0		/*; Returns 1 if bus error */
		bne.b	noram
		cmp.l	#0x00500000,d3	/*; Do we have a full complement? */
		bne.w	btloop
noram:
		sub.l	#0x00100000,d3	/* ; Start of on-board RAM */
		move.l	d3,obramstart
 D(7)
/* ; New stuff for 1/2 meg expansion */
		move.l	#0x80000,d0
		move.l	d0,obramsize	/*; Assume 1/2 meg only. */
		move.l	d3,a0		/*; Point to start of on-board RAM */
		move.l	(a0),a2		/*; Save what's there */
		clr.l	(a0)
		move.l	0(a0,d0.l),a1	/* ; Save what's there */
		move.l	#-1,0(a0,d0.l)	/* ; Set expansion RAM */
		tst.l	(a0)              /* Still zero? */
		bne	notexpansion
		move.l	0(a0,d0.l),d1
		addq.l	#1,d1
		bne	notexpansion
		move.l	#0x100000,obramsize  /* Is 1 meg */
		move.l	a1,0(a0,d0.l)        /* Restore memory */


    .extern main

notexpansion:
 D(8)
		move.l	a2,(a0)		/*; Restore memory*/
not512k:
		move.l	#0x10000,a7	/*; Working stack*/
 D(9)
		bsr	setupram	/*; Set up vectors, etc*/
 D(10)
		move.l	d7,-(a7)	/*; Flag to main*/
 D(11)
		jsr	main		/*; To C code (does not return)*/

/*;
; Stick numbers into CRTC so that the 15 MHz RAM control starts
; A5 holds return address
;
*/
crtc_tab:
	dc.l 0

crtcstart:
		move.l	#crtc_tab,a0					/* crtcint.c has the table */
		moveq.l	#0,d0		/*; Counter */
wcrtc:
		move.b	d0,CRTCADDR
		move.b	(a0)+,CRTCDATA
		addq.l	#1,d0
		cmp.b	#14,d0
		bne.b	wcrtc
	/*; Wait for the internal registers to reload*/
		move.l	#100000,d0	/*; Over 20 msec*/
crwait:
		subq.l	#1,d0		/*; 8 */
		bne.b	crwait		/*; 10. 18 * 100000 = (108 msec)*/
		jmp	(a5)

/*;
; Work out whether the CPU is 68000 or 68010
;
*/
getcpu:
		move.b	#1,is68010
		move.l	ILLEGALVEC,d0		/*; Save current vector */
		move.l	#ill,ILLEGALVEC
		move.l	a7,d1		/* Save SP*/
		dc.w	0x42c0		/* MOVE CCR,d0 (68010 instr)*/
illret:
		move.l	d1,a7		/* Restore SP*/
		move.l	d0,ILLEGALVEC	/*; Restore vector*/
		rts
ill:
		clr.b	is68010
		bra.b	illret

/*;
; Set up all the hardware exception vectors, traps, etc
;
*/
setupram:
		move.l	#vtabe,d0
		sub.l	#vtab,d0	/*; Size of vector table*/
		move.l	#vtab,a0
L1:
		move.l	(a0)+,a1	/* Get pointer to vector address*/
		move.l	(a0)+,(a1)	/*; Move vector in*/
		subq.b	#8,d0
		bne.b	L1

		moveq.l	#15,d0		/*; Set up all trap vectors*/
		move.l	#0x80,a0		/*; Pointer to trap vectors*/
L2:
		move.l	#htrap,(a0)+
		dbf	d0,L2		/*; Do for 16 vectors*/

		rts

/*;
; Test for a bus error at the supplied location.
; Usage: berrtest(loc). Returns 1 if bus error, else 0
;
*/
		.global	_berrtest, berrtest
berrtest:
_berrtest:
		move.l	4(a7),a0	/*; Location to test */
		move.l	a2,-(a7)
		bsr			bt2
		move.l	(a7)+,a2
		rts

bt2:
		move.l	BERR_VEC,a1	/*; Save current vector*/
		move.l	#bthberr,BERR_VEC
		clr.l	d0
		move.l	a7,a2		/*; Save SP*/
		move.b	(a0),d1		/*; Maybe get a bus error: alters d0*/
gotberr:
		move.l	a1,BERR_VEC	/*; Restore vector*/
		rts

bthberr:
		move.l	a2,a7			/*; Restore sp*/
		moveq.l	#1,d0
		bra.b	gotberr

/*;
; Entry points for hardware exceptions, interrupts, etc
;
*/
berrmes:		dc.l 0
addresserr: dc.l 0
illegalmes: dc.l 0
zdividemes:	dc.l 0
chkmes:			dc.l 0
trapvmes:		dc.l 0
privmes:		dc.l 0
l1111mes:		dc.l 0
unintmes:   dc.l 0
spurmes:		dc.l 0
trapmes:		dc.l 0
irq1mes:		dc.l 0
irq2mes:		dc.l 0
irq3mes:		dc.l 0
irq4mes:		dc.l 0
irq5mes:		dc.l 0
irq6mes:		dc.l 0
irq7mes:		dc.l 0

hbuserr:
		move.l	#berrmes,excepmes				/* mon.c */
		bra	hexcep0
haddresserr:
		move.l	#addresserr,excepmes
		bra	hexcep0
hillegal:
		move.l	#illegalmes,excepmes
		bra	hexcep1
hzdivide:
		move.l	#zdividemes,excepmes
		bra	hexcep2
hchk:
		move.l	#chkmes,excepmes
		bra	hexcep2
htrapv:
		move.l	#trapvmes,excepmes
		bra	hexcep2
hpriv:
		move.l	#privmes,excepmes
		bra	hexcep1
/* h1010:		move.l	#_l1010mes,_excepmes
		bra	hexcep1 */
h1111:
		move.l	#l1111mes,excepmes
		bra	hexcep1
hunint:
		move.l	#unintmes,excepmes
		bra	hexcep1
hspur:
		move.l	#spurmes,excepmes
		bra	hexcep1
htrap:
		move.l	#trapmes,excepmes
		bra	hexcep2
hirq1:
		move.l	#irq1mes,excepmes
		bra	hexcep1
hirq2:
		move.l	#irq2mes,excepmes
		bra	hexcep1
hirq3:
		move.l	#irq3mes,excepmes
		bra	hexcep1
hirq4:
		move.l	#irq4mes,excepmes
		bra	hexcep1
hirq5:
		move.l	#irq5mes,excepmes
		bra	hexcep1
hirq6:
		move.l	#irq6mes,excepmes
		bra	hexcep1
hirq7:
		move.l	#irq7mes,excepmes
		bra	hexcep1

htrace:
		move.w	(a7),srsave
		move.l	2(a7),pcsave
		movem.l	d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6/a7,regsave
		jsr	trace
		movem.l	regsave,d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6/a7
		rte

/*;
; Handle type 0 exceptions. Bus error and address error.
;
*/
		.global	_regsave
		.global	_pcsave,_accaddr

hexcep0:
		tst.b	is68010
		beq	hex068k

		clr.w	fcsave			/*; 68010 exception 0 code */
		move.l	10(a7),accaddr
		move.l	24(a7),irsave
		move.w	(a7),srsave
		move.l	2(a7),pcsave
		move.b	#1,wastype0
		add.l	#58,a7
		bra.b	cont1

hex068k:	move.w	(a7)+,fcsave		/*; 68000 exception 0 code */
		move.l	(a7)+,accaddr
		move.w	(a7)+,irsave
		move.b	#1,wastype0		/*; Flag exception type to C*/
		bra.b	contexcep

hexcep1:
hexcep2:
		clr.w	wastype0
contexcep:
	 move.w	(a7)+,srsave
   move.l	(a7)+,pcsave
cont1:
    movem.l	d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a3/a4/a5/a6/a7,regsave
		or.w	#0x0700,sr
		move.w	lastsyscall,excepsyscall
		clr.l	-(a7)					/*(sp) */
		jsr	cexception		/*; To C code*/

/*; Bad system call entry point*/
		.global	__empty
__empty:	move.l	#badsyscall,excepmes
		bra.b	cont1

/*; Table of vectors and their addresses*/
		.global	__linea			/*; Line A handler*/
/* __linea:
	rts */

vtab:
		dc.l	8,hbuserr
		dc.l	0x0c,haddresserr
		dc.l	0x10,hillegal
		dc.l	0x14,hzdivide
		dc.l	0x18,hchk
		dc.l	0x1c,htrapv
		dc.l	0x20,hpriv
		dc.l	0x24,htrace
		dc.l	0x28,__linea			/* systrap.s */
		dc.l	0x2c,h1111
		dc.l	0x3c,hunint
		dc.l	0x60,hspur
		dc.l	0x64,hirq1
		dc.l	0x68,hirq2
		dc.l	0x6c,hirq3
		dc.l	0x70,hirq4
		dc.l	0x74,hirq5
		dc.l	0x78,hirq6
		dc.l	0x7c,hirq7
vtabe:		nop
