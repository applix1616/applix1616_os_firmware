/*
 * Video window structure
 */
#ifndef WINDOWS_H
#define WINDOWS_H

struct window
		{
		unsigned short x_start;		/* The character start positions */
		unsigned short y_start;
		unsigned short x_end;		/* End positions */
		unsigned short y_end;
		unsigned short bg_col;		/* The background and foreground colours */
		unsigned short fg_col;
		unsigned short curs_x;		/* Where the cursor is in window */
		unsigned short curs_y;
		};

/*
 * Define the plot modes
 */

#define PM_WRITE	0			/* Write pixel to video RAM */
#define PM_OR		1			/* OR pixel with video RAM */
#define PM_AND		2			/* AND ~(pixel) with video RAM */
#define PM_XOR		3			/* XOR pixel with video RAM */
#define PM_READ		4			/* Read current setting */

/*
 * Mousetrap arguments
 */

#define MT_PCVEC	0			/* Install pre-char vector */
#define MT_ACVEC	1			/* Install after-char vector */
#define MT_PSVEC	2			/* Install pre-scroll vector */
#define MT_ASVEC	3			/* Install after-scroll vector */


#endif

