/* TABS4
 * File level I/O driver assist functions
 */
#ifndef FILEFNS_H
#define FILEFNS_H

extern int fsbusy;
extern void file_init(void);
extern int chkfpc(char *pathname);
extern void fsentry(int x);
extern void fsexit(int x);
extern void freeblk(register struct blkdriver *bdptr,register uint block);
extern int find_file(register char *fullpath,uint fcbnum, register int mode, int statbits, char *load_addr);
extern void free_fcb(int fcbnum,int ispipe);
extern int flush(struct fcb *fcbptr);
extern int refill(struct fcb *fcbptr);
extern int wrbitmap(int bdvrnum);
extern int checkfname(uchar *name);
extern int get_bdev(char *fullpath,char *fname);
extern int get_fcb(char *fullpath,int ispipe);
extern int get_free(ushort bdvrnum);
extern int getdirblks(int bdvrnum, int nblks);


#endif
