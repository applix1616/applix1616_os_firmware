/* TABS4
 * Task scheduling stuff
 */
#ifndef KERNEL_H
#define KERNEL_H

extern int mtnotup;		/* If true, scheduler has not started */

extern void startsched(void);
extern int curpidname(int mode);
extern void sigput(PROC *proc,int sigtype,int sigarg);
extern void freeprocsignals(PROC *proc);

#endif
