# 1 "viaaisr.s"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "viaaisr.s"



# 1 "asm68k.h" 1
# 5 "viaaisr.s" 2


  .text
# 26 "viaaisr.s"
.equ V_VECBASE,0x100
.equ VIABASE, 0x700100
.equ V_IFR,VIABASE+26
.equ V_IER,VIABASE+28

 .global _via_aisr
 .global _syssafe,_syssp,_switchpending,_timeslice,_wastick

_via_aisr:
  movem.l %d0/%d1/%d2/%d3/%d4/%d5/%d6/%d7/%a0/%a1/%a2/%a3/%a4/%a5/%a6,-(%a7)
  move.b V_IFR,%d4
  and.b V_IER,%d4
  move.l #V_VECBASE,%a4
  asl.b #1,%d4
vloop: asl.b #1,%d4
  bcc.b noint
  move.l (%a4),%a0
  jsr (%a0)
noint: addq.l #4,%a4
  tst.b %d4
  bne.b vloop


  tst.l wastick
  beq.b notyet
  clr.l wastick

  subq.l #1,timeslice
  bpl.b notyet
  tst.l syssafe
  bne.b notsafe

  move.l %a7,%d0
  move.l syssp,%a7
  movem.l (%a7)+,%d1/%d2/%d3/%d4/%d5/%d6/%d7/%a0/%a1/%a2/%a3/%a4/%a5/%a6
  rte

notsafe: move.l %a7,switchpending
notyet:
  movem.l (%a7)+,%d0/%d1/%d2/%d3/%d4/%d5/%d6/%d7/%a0/%a1/%a2/%a3/%a4/%a5/%a6
  rte
