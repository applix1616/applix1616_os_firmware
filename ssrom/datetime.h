/*
 * Date and time code.
 */
#ifndef DATETIME_H
#define DATETIME_H

#define YEAR	0
#define MONTH	1
#define DAY	2
#define HOUR	3
#define MINUTE	4
#define SECOND	5
#define TENTHS	6

/* The time is incremented by the following number of usecs at each interrupt */
#define TIMEINC	19968

#define DT_FORMATSTRING	"%02d:%02d:%02d %02d %s 19%02d"


extern void dt_init(void);
extern void birthday(void);
extern char *tosecs(char *buf,int ticks);
extern int checkdate(uchar *str);
extern char *dtformat(char *dtarray,char *buf);


#endif
