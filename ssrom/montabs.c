
#include <string.h>
#include <types.h>
#include <mondefs.h>
#include <syscalls.h>
#include <blkerrcodes.h>
#include <files.h>
#include "montabs.h"

extern int obramstart;
extern int pwruplongs[];
extern long vsi_count;

/* Monitor look-up tables */
char *moncmds[NMONCMDS] =
{
		"mdb",		/* 0 */
		"mdw",		/* 1 */
		"mdl",		/* 2 */
		"mrdb",		/* 3 */
		"mrdw",		/* 4 */
		"mrdl",		/* 5 */
		"mwb",		/* 6 */
		"mww",		/* 7 */
		"mwl",		/* 8 */
		"mwa",		/* 9 */
		"mfb",		/* 10 */
		"mfw",		/* 11 */
		"mfl",		/* 12 */
		"mfa",		/* 13 */
		"mmove",	/* 14 */
		"mcmp",		/* 15 */
		"msearch",	/* 16 */
		"base",		/* 17 */
		"expr",		/* 18 */
		"go",		/* 19 */
		"srec",		/* 20 */
		"help",		/* 21 */
		"move",		/* 22 */
		"mwaz",		/* 23 */
		"cio",		/* 24 */
		"terma",	/* 25 */
		"termb",	/* 26 */
		"serial",	/* 27 */
		"fkey",		/* 28 */
		"setdate",	/* 29 */
		"date",		/* 30 */
		"copy",		/* 31 */
		"syscall",	/* 32 */
		"delete",	/* 33 */
		"cat",		/* 34 */
		"rename",	/* 35 */
		"dir",		/* 36 */
		"msave",	/* 37 */
		"mload",	/* 38 */
		"tsave",	/* 39 */
		"tload",	/* 40 */
		"dirs",		/* 41 */
		"itload",	/* 42 */
		"cd",		/* 43 */
		"echo",		/* 44 */
		"mkdir",	/* 45 */
		"edit",		/* 46 */
#ifdef notdef
		"ssio",		/* 47 */
#endif
		"",		/* 47 */
		"quit",		/* 48 */
		"tarchive",	/* 49 */
		"pause",	/* 50 */
		"tverify",	/* 51 */
		"ascii",	/* 52 */
		"time",		/* 53 */
		"xpath",	/* 54 */
		"option",	/* 55 */
		"volumes",	/* 56 */
		"touch",	/* 57 */
		"filemode",	/* 58 */
		"type",		/* 59 */
#ifdef HAVE_SSASM
		"ssasm",	/* 60 */
#else
		"",			/* 60 */
#endif
		"assign",	/* 61 */
		"ps",		/* 62 */
		"kill",		/* 63 */
		"wait",		/* 64 */
		"set",		/* 65 */
		"term",		/* 66 */
		"vmode",	/* 67 */
#ifdef ZMODEM
		"getz",		/* 68 */
		"putz",		/* 69 */
		"geta",		/* 70 */
		"getb",		/* 71 */
		"puta",		/* 72 */
		"putb",		/* 73 */
		"printa",	/* 74 */
		"printb",	/* 75 */
		"boota",	/* 76 */
		"bootb"		/* 77 */
#endif
};

/* Flag the commands which must be checked for arg1 <= arg2 */
char chk1le2[NMONCMDS] =
{
		1,1,1,0,0,
		0,0,0,0,0,
		1,1,1,1,1,
		1,1,0,0,0,
		0,0,0,0,0,
		0,0,0,0,0,
		0,0,0,0,0,
		0,0,1,0,0,
		0,0,0,0,0,
		0,0,0,0,0,
		0,0,0,0,0,
		0,0,0,0,0,
		0,0,0,0,0,
		0
	};

/*
 * Syntax tables: each arg maps into a nybble as follows:
 *
 * bit 3: if set, optional entry
 * bit 2: if set, must be string (or numeric)
 * bit 1: if set, must be numeric
 * bit 0: if set, must be no entry
 */

char csyntax[NMONCMDS][4] =
{
	{ 0x33, 0x11, 0x11, 0x11 },		/* mdb */
	{ 0x33, 0x11, 0x11, 0x11 },		/* mdw */
	{ 0x33, 0x11, 0x11, 0x11 },		/* mdl */
	{ 0x21, 0x11, 0x11, 0x11 },		/* mrb */
	{ 0x21, 0x11, 0x11, 0x11 },		/* mrw */
	{ 0x21, 0x11, 0x11, 0x11 },		/* mrl */
	{ 0x23, 0x33, 0x33, 0x33 },		/* mwb */
	{ 0x23, 0x33, 0x33, 0x33 },		/* mww */
	{ 0x23, 0x33, 0x33, 0x33 },		/* mwl */
	{ 0x27, 0x11, 0x11, 0x11 },		/* mwa */
	{ 0x22, 0x21, 0x11, 0x11 },		/* mfb */
	{ 0x22, 0x21, 0x11, 0x11 },		/* mfw */
	{ 0x22, 0x21, 0x11, 0x11 },		/* mfl */
	{ 0x22, 0x61, 0x11, 0x11 },		/* mfa */
	{ 0x22, 0x21, 0x11, 0x11 },		/* mmove */
	{ 0x22, 0x21, 0x11, 0x11 },		/* mcmp */
	{ 0x22, 0x23, 0x33, 0x33 },		/* msearch */
	{ 0x23, 0x33, 0x33, 0x33 },		/* base */
	{ 0x25, 0x35, 0x35, 0x35 },		/* expr */
	{ 0x27, 0x77, 0x77, 0x77 },		/* go */
	{ 0x71, 0x11, 0x11, 0x11 },		/* srec */
	{ 0x77, 0x77, 0x77, 0x77 },		/* help */
	{ 0x66, 0x77, 0x77, 0x77 },		/* move */
	{ 0x27, 0x11, 0x11, 0x11 },		/* mwaz */
	{ 0x31, 0x11, 0x11, 0x11 },		/* cio */
	{ 0x71, 0x11, 0x11, 0x11 },		/* terma */
	{ 0x71, 0x11, 0x11, 0x11 },		/* termb */
	{ 0x23, 0x33, 0x33, 0x33 },		/* serial */
	{ 0x26, 0x11, 0x11, 0x11 },		/* fkey */
	{ 0x22, 0x22, 0x22, 0x11 },		/* setdate */
	{ 0x11, 0x11, 0x11, 0x11 },		/* date */
	{ 0x66, 0x77, 0x77, 0x77 },		/* copy */
	{ 0x27, 0x77, 0x77, 0x11 },		/* syscall */
	{ 0x67, 0x77, 0x77, 0x77 },		/* delete */
	{ 0x77, 0x77, 0x77, 0x77 },		/* cat */
	{ 0x66, 0x11, 0x11, 0x11 },		/* rename */
	{ 0x77, 0x77, 0x77, 0x77 },		/* dir */
	{ 0x22, 0x61, 0x11, 0x11 },		/* msave */
	{ 0x63, 0x11, 0x11, 0x11 },		/* mload */
	{ 0x67, 0x77, 0x77, 0x77 },		/* tsave */
	{ 0x71, 0x11, 0x11, 0x11 },		/* tload */
	{ 0x77, 0x77, 0x77, 0x77 },		/* dirs */
	{ 0x11, 0x11, 0x11, 0x11 },		/* itload */
	{ 0x71, 0x11, 0x11, 0x11 },		/* cd */
	{ 0x77, 0x77, 0x77, 0x77 },		/* echo */
	{ 0x67, 0x77, 0x77, 0x77 },		/* mkdir */
	{ 0x63, 0x11, 0x11, 0x11 },		/* edit */
	{ 0x11, 0x11, 0x11, 0x11 },		/* ssio */
	{ 0x11, 0x11, 0x11, 0x11 },		/* quit */
	{ 0x67, 0x77, 0x77, 0x77 },		/* tarchive */
	{ 0x21, 0x11, 0x11, 0x11 },		/* pause */
	{ 0x11, 0x11, 0x11, 0x11 },		/* tverify */
	{ 0x71, 0x11, 0x11, 0x11 },		/* ascii */
	{ 0x67, 0x77, 0x77, 0x77 },		/* time */
	{ 0x77, 0x77, 0x77, 0x77 },		/* xpath */
	{ 0x23, 0x11, 0x11, 0x11 },		/* option */
	{ 0x11, 0x11, 0x11, 0x11 },		/* volumes */
	{ 0x67, 0x77, 0x77, 0x77 },		/* touch */
	{ 0x22, 0x67, 0x77, 0x77 },		/* filemode */
	{ 0x67, 0x77, 0x77, 0x77 },		/* type */
#ifdef HAVE_SSASM
	{ 0x77, 0x77, 0x77, 0x77 },		/* ssasm */
#else
	{ 0x77, 0x77, 0x77, 0x77 },		/* ssasm */
#endif
	{ 0x77, 0x11, 0x11, 0x11 },		/* assign */
	{ 0x11, 0x11, 0x11, 0x11 },		/* ps */
	{ 0x67, 0x77, 0x77, 0x77 },		/* kill */
	{ 0x61, 0x11, 0x11, 0x11 },		/* wait */
	{ 0x77, 0x77, 0x77, 0x77 },		/* set */
	{ 0x67, 0x11, 0x11, 0x11 },		/* term */
	{ 0x22, 0x33, 0x11, 0x11 },		/* vmode */
#ifdef ZMODEM
	{ 0x11, 0x11, 0x11, 0x11 },		/* getz */
	{ 0x67, 0x77, 0x77, 0x77 },		/* putz */
	{ 0x77, 0x77, 0x77, 0x77 },		/* geta */
	{ 0x77, 0x77, 0x77, 0x77 },		/* getb */
	{ 0x67, 0x77, 0x77, 0x77 },		/* puta */
	{ 0x67, 0x77, 0x77, 0x77 },		/* putb */
	{ 0x67, 0x77, 0x77, 0x77 },		/* printa */
	{ 0x67, 0x77, 0x77, 0x77 },		/* printb */
	{ 0x77, 0x77, 0x77, 0x77 },		/* boota */
	{ 0x77, 0x77, 0x77, 0x77 }		/* bootb */
#endif
	};

static char mdmes[] = "start_address end_address";
static char mrdmes[] = "address_to_examine";
static char mwmes[] = "interactive_address%saddress data ...";
static char mwames[] = "interactive_address%saddress string";
static char mfmes[] = "start_address end_address value";
static char movecmpmes[] = "start_address end_address dest_address";
static char zeroormorepaths[] = "pathname ...";
static char oneormorepaths[] = "pathname ...";
static char minusl[] = "-l";
#ifdef ZMODEM
static char remoteargs[] = "remote arguments";
#endif

/* Usage messages */
static char *usages[NMONCMDS] =
{
/* mdb */	mdmes,
/* mdw */	mdmes,
/* mdl */	mdmes,
/* mrb */	mrdmes,
/* mrw */	mrdmes,
/* mrl */	mrdmes,
/* mwb */	mwmes,
/* mww */	mwmes,
/* mwl */	mwmes,
/* mwa */	mwames,
/* mfb */	mfmes,
/* mfw */	mfmes,
/* mfl */	mfmes,
/* mfa */	"start_address end_address string",
/* mmove */	movecmpmes,
/* mcmp */	movecmpmes,
/* msearch */	"start_address end_address byte ...",
/* base */	"number number ...",
/* expr */	"number operator number operator number ...",
/* go */	"start_address arg ...",
/* srec */	"filename",
/* help */	"command ...",
/* move */	"pathname pathname%spathname ... directoryname",
/* mwaz */	mwames,
/* cio */	"ascii_eof_code",
/* terma */	minusl,
/* termb */	minusl,
/* serial */	"a|b baudrate rxbits txbits parity stopbits set_sfc xonchar xoffchar",
/* fkey */	"key_number string",
/* setdate */	"year month day hour minute second",
/* date */	"",
/* copy */	"sourcefile destfile%ssourcefile ... directoryname",
/* syscall */	"call_number d1val d2val a0val a1val a2val",
/* delete */	oneormorepaths,
/* cat */	zeroormorepaths,
/* rename */	"oldname newname",
/* dir */	zeroormorepaths,
/* msave */	"start_address end_address pathname",
/* mload */	"pathname load_address",
/* tsave */	oneormorepaths,
/* tload */	"pathname",
/* dirs */	zeroormorepaths,
/* itload */	"",
/* cd */	"directory",
/* echo */	"-n string ...",
/* mkdir */	"directory ...",
/* edit */	"pathname tabwidth",
/* ssio */	"",
/* quit */	"",
/* tarchive */	oneormorepaths,
/* pause */	"number_of_ticks",
/* tverify */	"",
/* ascii */	"d | D | h | H",
/* time */	"command",
/* xpath */	"(status)%s- (delete all)%sdirectory ... (set paths)%s+ directory ... (add paths)",
/* option */	"option_number setting",	/* Handled specially */
/* volumes */	"",
/* touch */	oneormorepaths,
/* filemode */	"setting mask pathname ...\r\n\tsetting: 0:clear, 1:set\r\n\tmask:    1:archived, 4:locked, 8:no read, $10:no write, $20:no exec",
/* type */	oneormorepaths,
/* ssasm */	"",		/* Handled specially */
/* assign */	"(current assignments)%s/OLD (deletes)%s/OLD /NEW (assigns)",
/* ps */	"",
/* kill */	"-ak | -NN pid or process_name ...",
/* wait */	"pid or process_name",
/* set */	"-e|a name or name=setting ...",
/* term */	"DEV: -l",
/* vmode */	"Hfreq Vfreq Hadjust Vadjust",
#ifdef ZMODEM
/* getz */	"",
/* putz */	oneormorepaths,
/* geta */	zeroormorepaths,
/* getb */	zeroormorepaths,
/* puta */	oneormorepaths,
/* putb */	oneormorepaths,
/* printa */	oneormorepaths,
/* printb */	oneormorepaths,
/* boota */	remoteargs,
/* bootb */	remoteargs
#endif
};

/*
 * Do the help function
 */

int help(int argc,char *argv[])
{
	register int arg;
	register int retval = 0;

	if (argc == 1)
		return dispcmds();
	for (arg = 1; arg < argc; arg++)
		retval |= dohelp(argv[arg], STDOUT);
	return retval;
}

int dohelp(char *cmd,int fd)
{
	register char *buf;
	register int i;
	char _buf[150], __buf[150];

	if (!*cmd)
		return 0;
	buf = _buf;
	for (i = 0; i < NMONCMDS; i++)
	{
		if (!strucmp(cmd, moncmds[i]))
		{
			FPRINTF(fd, "Usage: %s ", cmd);
			stoupper(strcpy(__buf, cmd));
			SPRINTF(buf, "\r\n       %s ", __buf);
			FPRINTF(fd, usages[i], buf, buf, buf, buf, buf);
			FPRINTF(fd, "\r\n");
			if (i == 55)
			{	/* Further info for option */
				optionhelp();
			}
			return 0;
		}
	}
	eprintf("%s: no such inbuilt command\r\n", cmd);
	return -1;
}

/*
 * Display sorted list of internal commands
 */

ptrcmp(p1, p2)
char **p1, **p2;
{
	return strucmp(*p1, *p2);
}

dispcmds()
{
	register int i, index, j;
	register char *cs;
	uchar *ibcvec;
	char buf[100];
	char *mccopy[NMONCMDS];		/* Copy of commands */

	ibcvec = (uchar *)curibcvec();
	PRINTF("Inbuilt commands:\r\n");
	movmem(moncmds, mccopy, sizeof(mccopy));
	QSORT(mccopy, NMONCMDS, 4, ptrcmp);
	for (i = 0; i < NMONCMDS; i++)
	{
		cs = "%s\t";
		if (*mccopy[i])
		{
			if (ibcvec)
			{	/* Find the command */
				for (j = 0; j < NMONCMDS; j++)
				{
					if (mccopy[i] == moncmds[j])
						index = j;
				}
				if (ibcvec[index])
					cs = "*%s\t";
			}
			PRINTF(cs, stoupper(strcpy(buf, mccopy[i])));
		}
	}
	prcrlf();
	return 0;
}

/*
 * System call 125
 * Low-level miscellaneous O/S control functions
 */

EXTERN int ouruid;		/* User ID */
EXTERN int umask;		/* File creation mask */
EXTERN int (*beepvec)();
EXTERN int volume;
EXTERN int noffbs;
EXTERN uint bdlockin;
EXTERN int sodflag;
EXTERN long brclock;
EXTERN short t1intson;
EXTERN int obramsize;

#ifdef IOCACHE
EXTERN uint cachedevs;		/* mask of cached devices */
#endif

_oscontrol(cmd, arg, arg2, arg3)
{
	char *fullpath;
	register int ret;
	extern int _lbeep(), charbang();
	extern int do_rx(), do_tx();

	switch (cmd)
	{
	case 0:		/* Set inbuilt command vector */
		setibcvec(arg);
		break;
	case 1:
		return obramstart;
	case 3:		/* Read inbuilt command vector */
		return curibcvec();
	case 4:		/* Trash power up bytes */
		pwruplongs[0] = 0;
		break;
	case 5:		/* Get pointer to an xpath */
		return xpname(arg);
	case 6:		/* Get pointer to an assign */
		return getassname(arg);
	case 7:		/* Set umask */
		return (umask = arg);
	case 8:		/* Read umask */
		return umask;
	case 9:		/* Set uid */
		ouruid = arg;
		break;
	case 10:	/* Read uid */
		return ouruid;
	case 11:	/* Get block driver number from pathname */
		fullpath = (char *)GETFULLPATH(arg, 0);
		ret = get_bdev(arg, 0);
		dofreemem(fullpath);
		return ret;
	case 12:	/* Dump char device last lines */
		return dumplastlines(arg);
	case 13:	/* Set wild compare vector */
		return 0;
	case 14:	/* Read wild compare vector */
		return 0;
#ifdef IOCACHE
	case 15:	/* Set device cache mask */
		cachedevs = arg;
	case 16:	/* Read device cache maak */
		return cachedevs;
#endif
	case 17:	/* Reinitialise video */
		video_init(arg);
		break;
	case 18:	/* Reinitialise k/b */
		kb_init(arg);
		break;
	case 19:	/* Set beep volume */
		volume = arg;
	case 20:	/* Read beep volume */
		return volume;
	case 21:	/* Set beep vector */
		beepvec = (arg) ? (int(*)())arg : _lbeep;
	case 22:	/* Read beep vector */
		return (int)beepvec;
	case 23:	/* Enable/disable first four block stuff */
		noffbs = arg;
	case 24:	/* Read ffb enable */
		return noffbs;
	case 25:	/* Disable memory fault signals */
		return setmemfault(arg);
	case 26:	/* Return pointer to serial ISR's */
		return arg ? (long)do_rx : (long)do_tx;
	case 27:	/* Set block device lockin mask */
		bdlockin = arg;
	case 28:	/* Read blockdevice lockin mask */
		return bdlockin;
	case 29:	/* Read start_of_day */
		return sodflag;
	case 30:	/* Return pointer to charbang */
		return (int)charbang;
	case 31:	/* Set/read the baud rate clock */
		if (arg > 1)
			brclock = arg;
		return brclock;
	case 32:	/* Return t1ints state */
		return t1intson;
	case 33:	/* Trash a uid's assigns */
		return trashassign(arg);
	case 34:	/* Trash some environ strings */
		return trashenvstrings(arg);
	case 35:	/* Do env string substitution */
		return (long)__envsub(arg, arg2, arg3);
	case 36:
		return _doassign(arg, arg2);
	case 37:
		return (int)(&vsi_count);
	case 38:
		return obramsize;
	default:
		return BEC_BADARG;
	}
	return 0;
}
