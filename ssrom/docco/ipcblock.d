.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "IPC block communications"
.footer   "Page $#p"


              Description of the block send and receive
        interprocess communication mechanism in 1616/OS V4.2.
                        Andrew Morton, Applix
                            13 June 1990



     The IPB block send/receive facilities allow any number of��processes to send arbitrary length blocks of data to other processes.��Data sent to a particular process is queued on behalf of that process��and made available in order of receipt.  Because the blocks are��queued at the receiver the sender will never be blocked by the block��send system call.

     The sending process may arrange for the receiver to receive a��special signal when the sent block is available for receipt.

     When a block is sent to a process the operating system��allocates memory for it and a special header, copies the sent block��into that memory, prepares for the block to be available to the��receiver and then returns.  The memory into which the block is copied��is allocated as mode 0 memory in the receiving process's address��space.  It is the receiver's responsibility to free this memory.


.cp 10
The block send system call

     The transmission of an IPC block is done via one of the modes��of the PROCCNTL system call.  The usage is:

blocktx(destpid, addr, length, sig)     Send an IPC block
d0:  129
d1:  46
d2:  destpid                  Pid to send block to
a0:  addr                     Address of block to send
a1:  length                   Length of block to send in bytes
a2:  sig                      0: Don't signal; 1: signal destpid
Return:                       0 or error code


     The arguments are used as follows:

destpid

     The PID of the process to which the block is being sent.  As��always with the PROCCNTL system call, this may be either a direct PID��number in the range 0 to 63, or a pointer to a string which is either��a decimal number in the range 0 to 63, or the name of the process.

addr

     The starting address of the block of memory (in the��transmitting process's address space) which is to be sent.

length

     The length of the block in bytes.

sig

     This argument determines whether or not the receiving process��is to be sent a signal when the block is ready for receipt.  Set��'sig' to 0 if no signal is to be sent or 1 if a signal is to be sent.��If a signal is requested, it is sent via

     SIGSEND(DESTPID, SIGBLOCKRX, GETPID())

     where SIGBLOCKRX is equal to 23 decimal.


     If this system call returns a negative error code this probably��indicates that 'destpid' is invalid or there is not enough memory to��allocate the receiver's buffer.


.cp 10
The block receive system call

     This system call is used by a process for manipulating its IPC��block receive queue.

     This system call interfaces with the linked list of incoming��data blocks which the operating system maintains on behalf of each��process.  Each element in the linked list is defined by the following��simple C data structure (defined in 'ipcblock.h'):

typedef struct ipcblock
{
    struct ipcblock *next;    /* Link to next structure */
    long whofrom;             /* The sender PID */
    long blocklen;            /* Size of data block */
    long padd6;             /* Expansion */
    unsigned char data2;    /* The data is actually placed here */
} IPCBLOCK;

     The operating system adds structures to tbe end of this linked��list as they are sent to the receiving process and removes them from��the head as they are read off.  It is possible for the receiver to��directly manipulate and inspect the list if necessary.

     Note that the above header and the data block to which it��refers are allocated in a single hunk.  The data block originally��sent by the transmitter starts at the 0'th element of the 'data'��array and overruns the structure.  The 'blocklen' field defines the��number of bytes which are present in the 'data' array. The operating��system allocates sufficient extra memory beyond that required for��this structure for storage of the data block.

     The contents and format of the 'data' area are completely��defined by the calling application: it is the application's��responsibility to impose further data structure onto the sent blocks.�� It is guaranteed that the 'data' array will be aligned on an even��address boundary.

     It is the receiving process's responsibility to free the memory��taken by this structure / data block when it is no longer needed. The��correct address to free is that at which the structure starts.  Do��not attemt to free the address of the ipcblock.data element.

     BLOCKRX() is implemented as another mode to the PROCCNTL system��call.  The usage is:

blockrx(mode)            Receive an interprocess block
d0:  129
d1:  47
d2:  mode                Mode of operation
Return:                  Varies

     Where the following modes, defined in ipcblock.h  are valid:

mode = IPCB_GETHEAD (0)

     Returns a pointer to the first 'ipcblock' structure in the��receiving process's incoming list.  This refers to the next available��block.  The list may be inspected by going down its link pointers. ��The list is terminated by a zero value in ipcblock.next.

mode = IPCB_BLOCKCOUNT (1)

     Returns the number of IPC blocks which the calling process��currently has available for reading.

mode = IPCB_BYTECOUNT (2)

     Returns the number of bytes in all the IPC blocks which are��currently queued for receipt by the calling process.  This byte count��does not include the size of the ipcblock headers.  It is simply a��summation of all the ipcblock.blocklen elements.

mode = IPCB_FETCHNEXT (3)

     If the calling process has one or more blocks available for��receipt, this call removes the block at the head of the list and��returns a pointer to it in register d0.  That is, it returns an��(IPCBLOCK *).  If no data is available for receipt this call will��immediately return a nil pointer (d0 contains 0).

     A process may use this mode to poll its incoming IPC block��queue.


mode = IPCB_FETCHWAIT (4)

     Waits until a block is available for the calling process and,��when it is available, this call removes the block from the head of��the list and returns a pointer to it, as with IPCB_FETCHNEXT.

     This mode blocks the calling process until it has an IPC block��ready for reading.

     It is intended that in future releases of 116/OS this mode of��the BLOCKRX() system call will be interruptible by signals.  That is,��if the process is blocked in the BLOCKRX() system call and receives��and catches the signal this call will return a nil pointer.  For this��reason it is important that the return value from��BLOCKRX(IPCB_FETCHWAIT) be tested for zero before use.


.include "/usr/lib/trailer.dh"

