.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "proccntl system call"
.footer   "Page $#p"



The OSCONTROL system call

     This system call has been greatly expanded to give access to a��wide range of miscellaneous operating system internals.

     As many of the old modes have been altered it is here��redocumented from scratch.

     As many of the modes to this system call have unrelated��functions the different modes each have their own name and macro��definition in 'syscalls.h' and 'syscalls.mac'.


.cp 10
oscontrol(mode, arg, arg2, arg3)
d0:  125
d1:  mode:          0 to 36
d2:  arg:           Varies
a0:  arg2:          Varies, not used in most modes
a1:  arg3:          Varies, not used in most modes

d1 = mode = 0
SETIBCVEC(vec)
d2:  vec: Pointer to command disable vector.

     Changes the inbuilt command disable vector for the current��process's home shell process to that pointed to by 'vec'. The vector��is a string of bytes, each one of which corresponds to an inbuilt��command. The vector remains in force until it is either changed or��removed or the last process attached to this process's home shell��exits. See the 'Shell process' documentation for more details.

     If 'vec' is zero the inbuilt disable command for the current��process's home shell is removed. All inbuilt commands are enabled.

     When a process sets its home shell's disable vector, commands��are disabled for all processes which share that home shell process.��If one of those processes invoke a new shell type process, that��process inherits a copy of its parent's command disable vector, so��even if the original shell process exits the new disable vector��remains in force.

     The bytes in the vector can have values of 0, 1 or 2, where 0��means the corresponding command is enabled, 1 disables it and 2��disables it if the current user ID (UID) is not zero. Mode 2 is��probably the most useful: if a user executes a program which changes��its UID to zero that program can then execute any inbuilt commands,��which is presumably one reason why it changed its user ID.

     The correspondence between byte positions in the vector and the��inbuilt commands is listed below. This list is in order of the��evolution of 1616/OS:

   Command   Offset

     mdb       0
     mdw       1
     mdl       2
     mrdb      3
     mrdw      4
     mrdl      5
     mwb       6
     mww       7
     mwl       8
     mwa       9
     mfb       10
     mfw       11
     mfl       12
     mfa       13
     mmove     14
     mcmp      15
     msearch   16
     base      17
     expr      18
     go        19
     srec      20
     help      21
     move      22
     mwaz      23
     cio       24
     terma     25
     termb     26
     serial    27
     fkey      28
     setdate   29
     date      30
     copy      31
     syscall   32
     delete    33
     cat       34
     rename    35
     dir       36
     msave     37
     mload     38
     tsave     39
     tload     40
     dirs      41
     itload    42
     cd        43
     echo      44
     mkdir     45
     edit      46
     <unused>  47
     quit      48
     tarchive  49
     pause     50
     tverify   51
     ascii     52
     time      53
     xpath     54
     option    55
     volumes   56
     touch     57
     filemode  58
     type      59
     <ssasm>   60        * May not be present
     assign    61
     ps        62
     kill      63
     wait      64
     set       65


d1 = mode = 1
OBRAMSTART()
Return:   d0:       Address of on-board RAM start

     Returns the address of the start of the 1616's on-board RAM.��This is zero if the 1616 does not have a memory expansion board,��$100000 if is has one megabyte of expansion memory, $200000 for two��megabytes, etc.


d1 = mode = 2
Unused.


d1 = mode = 3
CURIBCVEC()
Return:   d0:       Current inbuilt command disable vector

     Returns a pointer to the command disable vector for the current��process's home shell process. Returns zero if there is no disable��vector in force.


d1 = mode = 4
TRASHPWRUPWORD()
Return:        None

     Invalidates the 1616/OS power-up words so that next time a��WARMBOOT() system call is executed (by ALT-^R, pressing reset,��rebooting on bus error, etc) the system will undergo a level zero��reset.


d1 = mode = 5
XPNAME(arg)
d2:       arg:      xpath index
Return:   d0:       Pointer to xpath directory name

     Returns a pointer to a null-terminated string which is one of��the execution search paths, as appears when the 'xpath' command is��executed. If there is no search path directory corresponding with��'arg' then the return value is -1. If 'arg' is invalid the return��value is negative. If the return value is <= 0 then it is not a valid��xpath directory pathname.


d1 = mode = 6
GETASSNAME(arg)
d2:       arg:      Assign index
Return:   d0:       Pointer to assignment string

     This call provides readback of the current assignments, as set��by the 'assign' inbuilt command. 'arg' is an index into the assign��table. If bit 31 of 'arg' is clear the return value is a pointer to��the 'old' part of the assignment. If bit 31 is set, the return is a��pointer to the 'new' part, where the order is

     assign /new /old

     If bit 30 of 'arg' is set the return value is the User ID of��the assignment: the UID of the process which originally made the��assignment.

     The return value is zero or negative if there is no assignment��corresponding to 'arg'.


d1 = mode = 7
SETUMASK(arg)
d2:       arg:      New UMASK
Return:   d0:       Old UMASK

     Sets the file creation mask for the current process. The mask��is used in setting the mode bits in any files/directories which are��created. A process's umask is passed on to any child processes when��they are created. The UMASK bits are defined in 'files.h'. The��default UMASK is (FS_NOEXEC | FS_NOREAD | FS_NOWRITE), which means��any new files cannot be executed, read or written by other users. Do��not set the LOCKED bit in the UMASK, or files cannot be modified��after they have been created, and commands like 'copy' fail when they��try to alter the status bits and date of the destination file after��copying the data.


d1 = mode = 8
READUMASK()
Return:   d0:       Current UMASK

     Returns the file current file creation mask for the current��process. 

d1 = mode = 9
SETUID(arg)
d2:       arg:      New UID
Return:   None

     Sets this process's User ID to 'arg'. The UID is inherited by��child processes when they are created.


d1 = mode = 10
READUID()
Return:   d0:       Current UID

     Returns the current User ID for the calling process.


d1 = mode = 11
GET_BDEV(path)
d2:       path:     Pointer to null-terminated pathname
Return:   d0:       Block device driver number.

     This call is passed a pointer to a file/directory pathname,��which need not be a full pathname. It returns the block device number��of the block device upon which that file/directory resides. Can��return a negative error code if 'path' is invalid.

d1 = mode = 12
DUMPLASTLINES(arg)
d2:       arg:      Character device driver number or -1 for all
Return:   d0:       None

     Removes all the last line recall buffer contents for the��character device driver whose driver number is 'arg'. If 'arg' is -1��then the last lines are removed for every character device driver in��the system. This is done by the memory manager on a GETMEM() failure,��in an attempt to defragment memory.


d1 = mode = 13
SETWILDCOMP(vec)
d2:       vec:      Pointer to new wildcard comparison function
Return:   None

     This call permits displacement of the internal 1616/OS routine��which performs wildcard matching in the SLICEARGS() system call,��which is used for command line expansion. Designed to permit the��installation of more sophisticated regular expression matching code,��the requirements of the installed function are not documented at this��stage.

     If 'vec' is zero, the default internal comparison function is��installed.


d1 = mode = 14
READWILDCOMP()
Return:   d0:       Address of current wildcard comparision function

     Returns a pointer to the current wildcard matching function,��which may be in the ROMs or external.


d1 = mode = 15
Unused


d1 = mode = 16
Unused


d1 = mode = 17
VIDEO_INIT(level)
d2:       level:         Level of initialisation
Return:   None

     Reinitialises the video driver. The argument corresponds to the��desired level of initialisation. If 'level' is zero, the��initialisation is performed as if a level zero reset was happening,��etc.


d1 = mode = 18
KB_INIT(level)
d2:       level:         Level of initialisation
Return:   None

     The same as the above, for the keyboard driver.


d1 = mode = 19
SETBEEPVOL(vol)
d2:       vol:           New beep volume
Return:   d0:            vol

     Sets the volume level for beeps. The speaker output is scaled��by the passed value. The default volume is 50. Do not set this��greater than 127.


d1 = mode = 20
READBEEPVOL()
Return:   d0:            Current beep volume

     Returns the current beep volume setting. Programs which emit��souinds from the loudspeaker should first read this setting and use��it to scale the output levels, to comply with the user's  desired��loudness level.

d1 = mode = 21
SETBEEPVEC(vec)
d2:       vec:           Pointer to beep code
Return:   d0:            vec

     Installs a new function which is called when the system is��ringing the beep (printing a control-G to the CON: driver).

     If 'vec' is zero, the default internal beep vector is installed.

     The code pointed to by 'vec' is called with two arguments on��the stack:

     4(sp):         Non-zero if a different beep sound is
               desired (Caused by the ESC-b sequence)
     8(sp):         Length. Either 1000 or 2000. Ignore this.

     Any new beep code can call the FREETONE() system call, but��cannot do any sophisticated system calls such as character/file I/O,��memory manager calls, etc. The function should preserve all registers.


d1 = mode = 22
READBEEPVEC()
Return:   d0:            Current beep vector

     Returns a pointer to the current beep vector function.


d1 = mode = 23
NOUSEFFBS(arg)
d1:       arg:           Mode
Return:   d0:            Current mode

     If 'arg' = 1, the system will not use the 'first four block'��(FFB) information contained in a file's directory entry. If arg = 0,��the FFB information is used to remove the need to read the blockmap��block of files which are less than 4k long. If 'arg' is zero, the FFB��information is used.

     This system call is hopefully unnecessary, but can be used to��prevent any possible confusion betweem files written under different��operating system versions.


d1 = mode = 24
READFFBS()
Return:   d0:            Mode

     Returns current FFB disable flag. Normally zero.


d1 = mode = 25
SETMEMFAULT(mode)
d2:       mode:          1 or 0

     Can be used to suppress the sending of a SIGSEGV signal to the��current process on a GETMEM() failure. If 'mode' is zero, signalling��is enabled (default).


d1 = mode = 26
RXTXPTR(n)
d2:       n:             Selector
Return:   d0:            ISR vector

     Returns a pointer to the operaing system's internal SCC��interrupt receive (n = 1) or transmit (n = 0) functions. This is��designed to permit full use of the operating system's serial driver��facilities by a driver for any additional SCCs attached to the 1616��bus.


d1 = mode = 27
SETBDLOCKIN(mask)
d2:       mask:          Lockin enable mask
Return:   d0:            mask

     'mask' is a bitmask which is used in the low-level disk drivers��just before a call is made to the physical device driver to perform��I/O. If the bit corresponding the the block device is set in the��mask, the system does a LOCKIN(1) system call before doing the I/O.��This can only be done if the device driver does no SLEEP() or��SNOOZE() system calls. Basically designed for improving the��performance of the /S0-/S3 SCSI drivers under heavy process loads.��Bit 0 of 'mask' corresponds to /RD, etc.


d1 = mode = 28
READBDLOCKIN()
Return:   d0:            Lockin enable mask

     Returns current BDLOCKIN mask.


d1 = mode = 29
STARTOFDAY()
Return:   d0:            True if start of day

     Returns true if the last level 0 reset was caused by powering��the system on, rather than a syscall(101).


d1 = mode = 30
PFASTCHAR()
Return:   d0:            Pointer to fastchar function

     Returns a pointer to a very low level internal operating system��function called 'fastchar' which draws characters on the screen.��'fastchar' is called with two arguments which must be pushed onto the��stack. One is a pointer to a string of characters, the second is a��byte count.

     From assembler:

     move.l    #125,d0
     move.l    #30,d1
     trap #7                  ; get pointer to fastchar function
     move.l    d0,a0
     move.l    #count,-(sp)
     move.l    #string,-(sp)
     jsr       (a0)
     add.l     #8,sp

     From C:

     int (*fastchar)() = (int (*)())OSCONTROL(30, 0);
     fastchar("string", 6);

     This function is fast.

     fastchar returns if it encounters a control character in the��string (ASCII value less than $20). It returns the number of��characters actually printed in d0. Characters are drawn starting from��the current cursor position, offset by the current window start.��Characters are masked by the current foreground and background��colours before being written into video memory.

     fastchar will blow up if it is asked to print beyond the last��column of the display.

d1 = mode = 31
SETBRCLOCK(n)
d1:       n:             New baud rate multiplier
Return:   d0:            Baud rate multiplier

     Sets a new multiplier for the SCC programming baud rate clock.��The default for this is 3,750,000, which is the rate of the clock��signal going into the SCC. If this clck signal frequency is altered,��this system call can be used to ensure that the SCC prgramming��functions still function correctly.

     If 'n' is zero, the mutiplier is unaltered and the current��value is returned.


d1 = mode = 32
TIMER1USED()
Return:   d0:            True is timer1 is in use

     Returns true if a process is currently using the VIA timer1��interrupt source in the freetone() system call.


d1 = mode = 33
TRASHASSIGN(uid)
d2:       uid:           User ID
Return:   None

     Removes all ASSIGNs made by the user whose UID is 'uid'.

d1 = mode = 34
TRASHENVSTRINGS(pid)
d2:       pid:           Process ID or -1
Return:   None

     Removes all the environment strings for the shell process which��a is the home shell for the process whose PID is 'pid'. If 'pid' is��equal to -1 then every environment string in the system is removed.


d1 = mode = 35
ENVSUB(in, dollaronly, memmode)
d2:       in:            Pointer to null terminated allocated string
a0:       dollaronly:    Flag
a1:       memmode:       Memory allocation mode
Return:   d0:            New string

     This system call gives access to the internal environment��string substitution code. It preforms substitutions on the passed��string pointed to by 'in', returning a string, space for which is��allocated via the GETMEM() system call. The memory allocation mode��for the string is specified by 'memmode', which should be 0 or 1.

     Substitutions are performed as described in the documentation��for the 'set' inbuilt command. If 'dollaronly' is non-zero then only��the '$' type substitutions are performed. Substitution is also��controlled by the relevant bits in the current process's home shell's��environment mode bits field.


d1 = mode = 36
DOASSIGN(argc, argv)
d2:       argc:          Argument count
a0:       argv:          Pointer to list of pointers to strings
Return:   d0:            Error code or zero

     Low-level access to the ASSIGN command. 'argv' points to a list��of pointers to null-terminated strings, the syntax of which is the��same as that required by the ASSIGN inbuilt command. 'argc' is a��count of the number of entries in 'argv'.

.include "/usr/lib/trailer.dh"
