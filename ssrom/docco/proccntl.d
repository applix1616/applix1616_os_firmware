.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "proccntl system call"
.footer   "Page $#p"


                New modes to the proccntl system call
                            Andrew Morton
                            6 April 1990


     This system call has had many new modes added.


.cp 10
csvec(vec)          Install context switch vector
d0:  129
d1:  18
d2:  vec:           Pointer to context switch catcher
Return:             Old context switch vector

     Installs a vector to a user-written piece of code which is��called every time the operating system deschedules or reschedules a��process. Returns the old vector value. Pass zero to deinstall.

     The user code is called with the following arguments:

     4(sp)     PID being scheduled/descheduled
     8(sp)     Pointer to process table entry for process
     12(sp)    Current system tick count, as returned by GET_TICKS()
     16(sp)    0: PID is being scheduled
               1: PID is being descheduled

     The user code can perform extended process accounting, etc. It ��should preserve all registers.


.cp 10
getpcurpid()             Return pointer to current PID
d0:  129
d1:  19
Return:                  Pointer to longword

     Returns a pointer to the location in operating system memory��where the current process ID is always kept.


.cp 10
readsigvec(pid)          Return signal handler vector
d0:  129
d1:  20
d2:  pid:                Process ID or pointer to process name
Return:                  Process's signal handler vector

     Reads the signal handler vector from the indicated process's��process table entry. This is zero if the process has no signal��handler. Returns negative error code if 'pid' is invalid.


.cp 10
fsbptr()                 Return pointer to file system busy flag
d0:  129
d1:  21
Return:                  Pointer to longword

     Returns a pointer to a longword which, if non-zero, indicates��that a process is using the file system.


.cp 10
fspptr()                 Return pointer to file system owner PID
d0:  129
d1:  22
Return:                  Pointer to longword

     Returns a pointer to a longword which holds the PID of the��process which is currently using the file system. This PID is only��valid when the file system busy flag is true.


.cp 10
ssptr()                  Return pointer to system safety semaphore
d0:  129
d1:  23
Return:                  Pointer to longword

     Returns a pointer to the longword which the system uses to lock��processes in and out (see the lockin() call). This has been  made��available so that programs can directly alter the semaphore:��incrementing it is equivalent to performing a lockin(1). Decrementing��it is equivalent to a lockin(0).


.cp 10
killuser(homeshell)      Terminate all processes attached to a shell
d0:  129
d1:  24
d2:  homeshell:          Home shell PID
Return:                  Number of processes killed or negative error code

     'homeshell' identifies a shell-type process. All processes��whose home shell process is identified by 'homeshell' are��uncondiionally terminated. The process identified by 'homeshell' is��not terminated.


.cp 10
sigblock(pid, mode)      Block/unblock signals
d0:  129
d1:  25
d2:  pid                 Process identifier
a0:  mode                1: Block, 0: Unblock
Return:                  0 or negative error code

     This call is used to prevent signals from being sent to the��identified process.


alarm(ticks)             Set an alarm
d0:  129
d1:  26
d2:  ticks               Ticks until alarm
Return:                  Time until next alarm or negative error code

     The alarm() system call permits a process to request that a��signal (SIGALRM: 14) be sent to it after 'ticks' 50 Hz system ticks��have alapsed. If 'ticks' is zero, any pending alarm is removed. If��'ticks' is equal to -1 the number of ticks until the next alarm is��returned. The return value is the number of ticks until this process��is to be sent a SIGALRM; this value is equal to $80000000 if there is��no alarm pending.


.cp 10
sigblocker(rootpid, sig, arg)
d0:  129
d1:  27
d2:  rootpid             Start PID
a0:  sig                 Signal to send
a1:  arg                 Argument to pass

     This system call sends the specified signal and argument to the��process which is blocking the process identified by 'rootpid'.  The��operating system descends the process table starting from 'rootpid'��until it locates the process which is blocking all its parents up to��'rootpid' and signals it.  This system call is used in the processing��of the ALT-^C interrupt.


.cp 10
snooze(vec, arg1, arg2)  Sleep until (vec) returns true
d0:  129
d1:  28
d2:  vec                 Pointer to snooze function
a0:  arg1                Argument passed to (vec)
a0:  arg2                Argument passed to (vec)
Return:                  0, 1 or negative error code

     The snooze function is designed so that a process can go to��sleep until some condition which is defined by the process becomes��true. The operating system scheduler calls the user-written function��which is pointed to by 'vec' within the scheduling loop and, when it��rturns non-zero, the process is rescheduled (the call to snooze��returns).

     The arguments to snooze() are passed on to the user code when��the system polls it.

     A snooze call terminates when either the 'vec' function returns��true to the scheduler, or when the process is signalled. The��process's signal handler is called before the snooze call returns.��snooze() returns 0 if it was broken due to the 'vec' code returning��true, or 1 if the snooze was broken due to receipt of a signal.

     The user code pointed to by 'vec' receives the following��arguments when the scheduler polls it:

     4(sp)     arg1, as passed to snooze()
     8(sp)     arg2, as passed to snooze()
     12(sp)    Current system tick count, as returned by GET_TICKS()

     The user code should perform whatever tests are needed and��return 0 (keep snoozing) or 1 (break the snooze). It should preserve��all registers and not perform any sophisticated system calls, such as��file system I/O, character I/O, memory manager calls, etc: It is��called with an indeterminate PID, UID, memory manager state, etc.

     An example: A piece of code (in C) which waits for an I/O port��to become true.

#include <syscalls.h>

waitforbit(ptr, mask)
unsigned char *ptr;           /* Pointer to port */
unsigned char mask;           /* Bit to wait for */
{
     int testfunc();
     while (SNOOZE(testfunc, ptr, mask))
          ;    /* Loop until broken due to bit setting, not due to signal */
}

/* The scheduler calls this function */
int testfunc(ptr, mask, ticks)
unsigned char *ptr, mask;
unsigned long ticks;
{
     if (*ptr & mask)
          return 1;
     return 0;
}


.cp 10
siguser(pid, sig, arg)        Send a signal to processes
d0:  129
d1:  29
d2:  pid                      PID of shell process
a0:  sig                      Signal to send
a1:  arg                      Argument to signal
Return:                       Number of processes signalled, or error code

     This system call sends the specified signal, with the specified��argument to all processes whose home shell process is identified by��'pid'. The home shell process itself is not signalled.


.cp 10
findhomeshell(pid)            Find  process's home shell PID
d0:  129
d1:  30
d2:  pid                      Process whose home shell PID is to be found
Return:                       Home shell PID or error code

     Locates the PID of the specified process's home shell process.��If 'pid' refers to a shell typer process then its home shell process��PID is returned, not its own PID.


.cp 10
setshnice(pid, nice)          Set nice level of processes
d0:  129
d1:  31
d2:  pid                      Shell type proces PID
a0:  nice                     Time slice value
Return:                       0 or error code

     Sets the time slice period (the number of ticks allocated per��scheduling period) of every process whose home shell PID is that��identified by 'pid'. 'pid' must refer to a shell type process. The��nice level of the shell type process identified by 'pid' is not��altered.


.cp 10
lastchild(pid)                Return last child PID
d0:  129
d1:  32
d2:  pid                      Process ID
Return:                       Last child PID

     Returns the PID of the last process which was asynchronously��started by the process identified by 'pid'. This can be useful in��writing reentrant code, where a single function is scheduled by the��same piece of code more than one time. Returns zero if no child��process is available.


.cp 10
swpptr()                      Return pointer to switch pending flag
d0:  129
d1:  33
Return:                       Pointer to longword

     Returns a pointer to an operating system variable which, if��set, indicates that the currently running process is to be��deschedueled as soon as it leaves a locked in state.


.cp 10
killdown(startpid)            Kill a group of processes
d0:  129
d1:  34
d2:  startpid                 Pid to start search from
Return:                       0 or error code

     Searches down the process/child list from the specified��process, unconditionally terminating all child processes. If one of��these is the calling process it is skipped.

     This call is performed by the 'kill -ka' inbuilt command to��kill off a process and all its blocking children and all their��blocking children.


.cp 10
sigdown(startpid, sig, arg)   Signal a group of processes
d0:  129
d1:  35
d2:  startpid                 Pid to start search from
a0:  sig                      Signal to send
a1:  arg                      Argument to pass with signal
Return:                       0 or error code

     Like killdown(), except the passed signal and argument are sent��to the processes, rather than killing them.


.cp 10
killuid(uid)                  Kill a user's processes
d0:  129
d1:  36
d2:  uid                      User ID
Return:                       0 or error code

     Unconditionally terminates all processes whose user ID is��identified by 'uid'. If the current (calling) process is identified��in this group of processes it is killed after all the other processes.


.cp 10
siguid(uid, sig, arg)         Signal a user's processes
d0:  129
d1:  37
d2:  uid                      User ID
a0:  sig                      Signal to send
a1:  arg                      Argument to send with signal
Return:                       0 or error code

     Sends the specified signal, with the specified argument to all��processes owned by the user identified by 'uid'. If one of these��processes is the current (calling) process in this group of processes��it is signalled after all the other processes have been.


.cp 10
setsigexit(pid, mode)         Signal process on exits
d0:  129
d1:  38
d2:  pid                      Process to signal
a0:  mode                     1: enable; 0: disable
Return:                       0 or error code

     If enabled, this call results in the identified process being ��signalled whenever ANY other process exits. The identified process's��signal handler is passed a SIGEXIT (22) and the exitting process's��PID.

     If 'mode' is zero it disables this sending of SIGEXIT.


.cp 10
setpg(pg)                     Set process group
d0:  129
d1:  39
d2:  pg                       New process group identifier
Return:                       New process group

     A process group is a number which identifies a group of��processes and may be used to collectively identify a group of��processes for the purposes of killing them or signalling them.��Normally all processes run with a process group ID of 2. This call��allows a process to modify its process group number. All processes��inherit their parent's process group number when they are created.

     If 'pg' is 1, the current process's group is set to a new��value, which is initially 3, and then 4 on the next call to setpg(1),��etc.

     If 'pg' is zero then the current process's process group number��is unaltered. A process may use this to find out its current process��group number.

     For all other valuse of 'pg' the current process's process��group number becomes 'pg'.

     The new process group number for the current process is always��returned.


.cp 10
sigpg(pg, sig, arg)           Signal processes in a process group
d0:  129
d1:  40
d2:  pg                       Process group number
a0:  sig                      Signal to send
a1:  arg                      Argument to send with signal
Return:                       0 or error code

     All processes running with the specified process group number��are signalled with the passed signal and signal argument. If the��calling process belongs to that group it is signalled after all of��the other members.


.cp 10
killpg(pg)                    Kill processes in a group
d0:  129
d1:  41
d2:  pg                       Process group number
Return:                       0 or error code

     All processes running with the specified process group number��are unconditionally terminated. If the calling process belongs to��that group it is killed after all of the other members.


.cp 10
setprocumask(pid, umask)      Set file creation mask for a process
d0:  129
d1:  42
d2:  pid:                     ID of process to alter
a0:  umask:                   New umask
Return:                       New umask

     Sets the specified process's file creation mask to 'umask'.��Returns new umask. If 'umask' is equal to -1 the current file��creation mask is returned unaltered.


.cp 10
setenvbits(pid, mask, mode)   Set/clear/read a process's environment bits
d0:  129
d1:  43
d2:  pid                      Process ID
a0:  mask                     Bit mask for environment bits
a1:  set                      1: set bits; 0: clear bits; 2: read
Return:                       New environment bits

     This system call enables manipulation of the environment bits��field attached to a shell type process. The use of these bits is��described in the shell process documentation.

     This call locates the home shell process of the pid identified��by 'pid' and modifies its environment bits. This will change the��environment of that shell process, all those processes which have it��as their home shell process as well as all new processes created by��that shell process and those processes which have it as a home shell.

     If 'set' is zero, set bits in 'mask' are used to clear bits in��the environment. ('mask' is inverted and ANDed into the environment��bits)

     If 'set' is one, 'mask' is ORed into the environment bits.

     If 'set' is two, the environment bits are unaltered. This��provides readback.

     In all modes, the new environment bits are returned. The��'setenv' program is provided for manipulation of these bits.

     Every time this call is performed the system records the��resulting environment but setting and uses it to start off the root��shell process when the system is next reset.


.cp 10
getenvbits(pid)               Read environment bits
d0:  129
d1:  44
d2:  pid                      Process identifier
Return:                       Environment bits or error code

     This call returns the environment bits for the home shell��process of the prcess identified by 'pid'.


.cp 10
nametopid(pid)                Convert string to PID
d0:  129
d1:  45
d2:  pid                      PID identifier
Return:                       PID number or error code

     This call converts its argument into a process ID. If 'pid' is��a number in the range 0 to MAXPIDS (currently 64) it is simply��returned. If 'pid' points to a null-terminated string of digits the��string is evaluated as a decimnal number and returned. If 'pid'��points to a null terminated name of a process the process table is��searched for a process with a matching name. The PID of the first one��encountered is retrurned.


.cp 10
blocktx(destpid, addr, length, sig)     Send an IPC block
d0:  129
d1:  46
d2:  destpid                  Pid to send block to
a0:  addr                     Address of block to send
a1:  length                   Length of block to send in bytes
a2:  sig                      0: Don't signal; 1: signal destpid
Return:                       0 or error code

     This call sends a block of data to the identified PID. See the��interprocess block transmission documentation for more details.


.cp 10
blockrx(mode)            Receive an interprocess block
d0:  129
d1:  47
d2:  mode                Mode of operation
Return:                  Varies

     A process uses this call for manipulation of its interprocess��block communication input queue. See the interprocess block��transmission documentation for more details.


.cp 10
setenv(pid, name, setting, mode)   Manipulate environment strings
d0:  129
d1:  48
d2:  pid                 Process ID
a0:  name                Name part of environment variable
a1:  setting             Setting part of environment variable
a2:  mode                Mode of environment string substitution.
Return:                  0, 1 or error code

     This system call permits alteration of the environment variable��strings which are attached to the home shell process of the process��identified by 'pid'. These strings are shared by the home shell and��by all processes which share that home shell.

     'name' points to a null-terminated string of any length which��identifies the environment string. 'setting' is the new string which��is associated with the 'name' string. If 'setting' is zero (a nil��pointer) then the environment string identified by 'name' is removed.

     'mode' determines how the new name and setting are to be��handled by the envsub() system call, which is used in command line��substitution. It is a combination of the following bits, defined in��'envbits.h'

ENVS_DSIGN     1    Enable substitution in '$' type usage
ENVS_ARG       2    Enable substitution in all arguments
ENVS_ARG0      4    Enable substitution in the first part of a command

     Note that the enabling of substitution is also controlled by��bits in the home shell's environment bits.

     This call returns 0 if all went well and 'name' did not��previously exist in the environment. It returns 1 if all went well��and 'name' was previously defined in the environment. A negative��error code is returned on memory allocation failure, bad PID, etc.

     See the documentation for shell type processes and the 'set'��inbuilt command for more details.


.cp 10
getenv(pid, name, mode)       Look up an environment string variable
d0:  129
d1:  49
d2:  pid                      Process identifier
a0:  name                     Pointer to 'name' string
a1:  mode                     Search mode
Return:                       0, error code or pointer to setting

     This call provides access to the environment string variables��attached to the home shell process of the process identified by 'pid'.

     'name' points to a null-terminated string which identifies an��environment variable. The string comparison is case sensitive.

     'mode' is a mask consisting of a combination of ENVS_DSIGN,��ENVS_ARG0 and ENVS_ARG1. If the result of ANDing 'mode' with the��environment variable's mode (as passed when it was installed) is��non-zero then a match is valid.

     If no match is found a value of zero is returned. If a matching��string is found and the ANDing of 'mode' and the environment��variable's mode succeeds, this call returns a pointer to the��variable's 'setting' string.

     If 'name' is less than $4000 then it is assumed to be a numeric��index into the environment varaibles. If 'name' does not exceed the��number of currently installed environment strings then a pointer to��the corresponding ENVSTRING structure (defined in 'process.h') is��returned. The 'mode' field is ignored in this mode. If 'name' exceeds��the number of installed environment strings then a nil pointer is��returned.

.include "/usr/lib/trailer.dh"
