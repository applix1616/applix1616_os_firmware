.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "Shell process environment"
.footer   "Page $#p"

          The shell process environment under 1616/OS V4.2b
                  Andrew Morton, Applix pty limited
                           24th March 1990



Introduction

     A special sort of process known as a shell-type process has��been introduced. The most common shell process is that which the��IEXEC() system call starts up: the process which displays a prompt,��reads a command from the keyboard and executes it.

     Shell processes are special because they are treated as the��leading process in a group of processes. An 'enviroment' is��associated with each shell process running in the 1616. The��environment consists of an inbuilt command disable vector, a group of��option bits and a collection of 'environment variables' which have��various uses.


.cp 10




.include "/usr/lib/trailer.dh"
