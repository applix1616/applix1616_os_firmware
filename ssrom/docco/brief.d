.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "1616/OS V4.2a Quickies"
.footer   "Page $#p  $#d"

                  Overview of 1616/OS V4.2a changes
                  Andrew Morton, Applix pty limited
                            10 March 1990

.cp 10
Introduction

     This document contains the shortest possible description of the��changes from 1616/OS Version 4.1a to Version 4.2a.

     There is nothing really major in this release. It has a has a��number of enhancements, bug fixes and alterations, most notable being��the improved file system and video driver performance, enhanced��character device control, automatic directory growth, unlimited file��sizes, fixed file permissions and a line editor enhancement.


.cp 10
Changes

.cp 10
******
     close() system call improved by changing order of writes to��save a seek to device bitmap and back.

.cp 10
******
     iexec() system call termintaes when it receives a SIGHUP��(signal 1)

.cp 10
******
     The message 'pid: terminated' is now emitted from the 'kill'��inbuilt command, not from the scheduler, so it comes out on a��rational character device.

.cp 10
******
     Added option 15 (OPT_MISC): miscellaneous option bits:

Bit 0: OPM_NOBAK:   If this bit is set, edit & DrDoc don't creat���������'.bak' files (For Colin)

Bit 1: OPM_ALTCINT: Broken.

Bit 2: OPM_NKPCQ:   If this bit is set, the '5' key on the numeric���������keypad generates a '^Q' in non-numeric mode.

Bit 3: OPM_BEEPONERR: If this bit is set and option 12 (enable beep���������on errors) is set then the system emits a beep on standard���������error whenever any program exits with a negative exit code.

.cp 10
******
     Char device drivers are cleared and reinstalled at all levels��of reset. The only attribute which is preserved is the CON: eof��character.

.cp 10
******
     Fixed file permissioning.

.cp 10
******
     Enhanced the 'kill' inbuilt command:

     kill -akNN pid pid

     -a:  Use the SIGDOWN or KILLDOWN system calls to signal/kill������������all children as well.
     -k:  Unconditionally kill the addressed processes, rather than������������signalling.
     NN:  Specify a (decimal) signal to send, rather than SIGTERM������������(signal 15)

These may be combined. The rather unsuccesful attempt to permit PID��wildcards in this command has been dumped.

.cp 10
******
     Fixed problem where memory for a .EXEC file was never freed if��an I/O error occurred while loading it from disk.

.cp 10
******
     Note that alarm(0) clears pending alarm, returns -(ticks until��alarm)

.cp 10
******
     The codes for ALT-S, ALT-^R, etc are now returned if their��special meanings are optioned out. Part of the new char driver magic.

.cp 10
******
     Added the line editor ESC key command: type a few chars, hit��ESC and a search is made back through the history for a line starting��with those characters. Repeated ESC's perform repeated searches.

.cp 10
******
     Had a problem correctly wedging stdin of an asynch process to��NULL:. Fixed.

.cp 10
******
     CLOSE(-1) did not always flush all open files. Fixed.

.cp 10
******
     Keyboard driver now alters state of caps lock & numlock based��on make & break codes, rather than counting make codes, for keyboards��which autorepeat on these keys.

.cp 10
******
     Pressing the scroll-lock key generates the code 243, which is��ALT-S. This may be used to stop and start scrolling.

.cp 10
******
     The line editor ^A, ^F and ^T commands now terminate on the��following characters:

     space, tab, . ! , : ; ( )   {  } / -

.cp 10
******
     Fixed a wee bug in the 'type' command which made it only type��one file under MGR.

.cp 10
******
     The last line recall depth is now set in MRDRIVERS file. The��default is 10 lines.

.cp 10
******
     A problem involving the flashing cursor across DEF_WIND calls��has been fixed.

.cp 10
******
     A pseudo-device called "TTY:" has been added. This does not��take up room in the char device table, but an OPEN or CREATE of this��device returns an inspired guess at the file descriptor of the��character device which the user is looking at. This device should be��used where a program definitely wants to get interactive input from��the user, ignoring standard input redirection.

.cp 10
******
     The character and block device installation routines now��correctly delete the named driver when an install is done with the��passed I/O equal to zero.

.cp 10
******
     Added SIGBLOCKER(), signal types SIGSTOP, SIGCONT. See PROCCNTL��documentation.

.cp 10
******
     The '*' character is now valid (as well as 'x') for��multiplication in the 'expr' inbuilt command. Should be escaped by��double quotes.

.cp 10
******
     Passing a '%E' to the PRINTF/SPRINTF/FPRINTF system calls��causes conversion from integer to system error message, so a negative��system call return value can be passed directly to FPRINTF for��diagnostics, rather than converting with ERRMES() and then using��'%s'. Be sure that the program will never be used on earlier versions��of the OS before using this!

.cp 10
******
     There was a problem in the process scheduler which meant that��the SLEEP() system call actually slept for one tick more than was��desired. This has been fixed, but this may affect some programs.

.cp 10
******
     Fixed a bug in the reset startup code which trashed memory at��the start of on-board RAM + $10000, which limited RAM disk size to��around 300k. RAM disk maximum is now 1 Mbyte

.cp 10
******
     Typing control-^ (CTL-SHIFT-6) now generates ascii code 30.
     Typint control-@ (CTL-SHIFT-2) noe generates ascii code 00.

.cp 10
******
     The call FIND_BDVR(-1) is supposed to return a pointer to the��block device structure for the current process's currently logged��device, but it in fact returned a pointer to the last device which��CD'ed to. Fixed.

.cp 10
******
     Added lots of modes to OSCONTROL. See the relevant document.

.cp 10
******
     Fixed spelling of the privilege viloation exception message. (!)

.cp 10
******
     When a file which is open for writing is interrogated by the��FILESTAT system call it is first flushed to disk, so the returned��file size, etc is more up to date.

.cp 10
******
     Fixed a problem in FILESTAT which made it much slower than��necessary.

.cp 10
******
     Fixed a problem in EDIT where it could emit random cursor��positioning sequences.

.cp 10
******
     Enhanced the GETTDSTR() system call:

     GETTDSTR(buf, arg1, arg2, arg3)

     GETTDSTR(buf, x, x, x)
     If 'buf' is non-zero, read current time to memory at *buf, as��in previous OS versions.

     GETTDSTR(0, 0, dateptr, stringptr)
     If 'buf' is zero and 'arg1' is zero, convert the 8 byte time��pointed to by 'arg2' into human readable string form at memory��pointed to by 'arg3'.

     GETTDSTR(0, 1, increment, x)
     GETTDSTR(0, 1, 0, x)
     If 'buf' is zero and 'arg1' is equal to 1 then set the��date/time increment to 'arg2', provided 'arg2' is non-zero. If 'arg2'��is zero then simply return current value of date/time increment.

     The time increment is the number of microseconds between��vertical sync interrupts. Normally 19968, this may be varied to trim��the clock under different video programming modes.

.cp 10
******
     Changed the order of bitmap and directory writes in UNLINK().��Saves a seek, a little quicker now.

.cp 10
******
     Control has been added for disposing of all the last-line��history of a character device, or of all character devices. This is��documented with OSCONTROL(). Used by the memory manager to defragment��and to increase free memory when a GETMEM fails. May be used to��prevent successive users from seeing what others have typed.

     The presence of this control means that a process's last line��history can be dumped by another process at any time.

     If your last lines mysteriously disappear then it probably��means that a process has requested more memory than was available.

.cp 10
******
     There was a problem with the ^KV command in EDIT. Fixed.

.cp 10
******
     Tidied up 'DIRS' output so it is readable on displays which are��not 80 characters wide.

.cp 10
******
     Running a 0 length .XREL file does not blow up now.

.cp 10
******
     Put in support for CDMISC() system call, miscellaneous access��to individual char device drivers. See other documentation for��details.

.cp 10
******
     The maximum file size restriction has been lifted by ensuring��that the blockmap for a file extends across contiguous blocks, one��per half megabyte. This means that every time the system grows a file��across a 0.5 Mbyte multiple it must relocate the entire blockmap. If��there is nowhere to put it a disk full error results. At the time of��writing 'fscheck' does not understand files longer than 0.5 Mbytes.

.cp 10
******
     Full directories are automatically grown by one block (16��entries). No mechanism is provided for automatically shrinking them.��The size specification has been removed from the 'mkdir' inbuilt��command, so multiple directories can be made in one command.

.cp 10
******
     Rationalised handling of GETFULLPATH() sustem call. It is��called far less frequently. The usage of block device volume names to��refer to the device has been removed, because having more than one��valid pathname referring to the same file has become difficult and��restrictve to support.

.cp 10
******
     Blockmap is not written out when closing files of zero length.

.cp 10
******
     Rather indirect access to the internal wildcard comparison��routine has been made available so that a full regular expression��handler could be added. Rather experimantal.

.cp 10
******
     The last 4 words of a directory entry are used to identify��where on the device the first four blocks of a file are located. This��speeds up access to small (<= 4k) files by removing the need to read��the blockmap block. The blockmap block is always created and valid,��whatever the file size.

     The FFB information is used only if:

     The file is less than 4097 bytes long,
     and bit 7 of its status bits (FS_HASFFB) is set
     and the 'magic' field of the directory entry is equal to $d742
     and FFB usage has not been disabled via OSCONTORL().

Some of this complexity is debugging stuff, some is due to the need��to be able to take a V4.2 disk, modify it under V4.1 and then reread��it under V4.2. The 'magic' field in the directory entry will change��with each ROM release from now on, so it is possible to tell what��version of the OS a file was created under.

.cp 10
******
     SETVSVEC(1, NN, 0) returns a pointer to a data structure which��repretents the state of vertical sync vector number NN.

.cp 10
******
     Doubled the maximum permissible length of lines within .SHELL��files.

.cp 10
******
     THe ABORTSTAT() system call is still supported, but is not used��within the ROMS and use of it is not encouraged. Use signals instead.

.cp 10
******
     There was a bug in V4.1 in which searches for a hunk of free��blocks on a disk would not wrap around to the start of the disk if��the disk was not a multiple of 8 blocks in size. Now the disk size is��internally rounded down, and 'blockdev' will not permit the creation��of a volume which is not a multiple of 8 blocks.

     Unfortunately this change may result in blocks being lost from��any files which occupy the last few blocks of a V4.1 volume.

.cp 10
******
     Added some shadow registers for the keyboard state. These are��WORD addresses, so read and write 16 bits at a time. These are��defined in 'storedef.h'

     Name      Word address             Usage

   ak_ctrl        $310            If non-zero, control key currently������������������������������������depressed

   ak_shift       $312            Bit 0: Left shift currently������������������������������������depressed. Bit 1: Right shift������������������������������������currently depressed.

   ak_alt         $314            Alt is depressed

   ak_capslock    $316            Capslock currently active

   ak_numlock     $318            Keypad is curently in numeric mode


.cp 10
******
     The internal low-level video and keyboard initialisation��routines are accessible via OSCONTROL().

.cp 10
******
     The status bits of a file are now correctly preserved if it is��modified. The UID is changed to that of the modifier.

     The CREAT() system call does not preseve any of the status��bits, etc of a file is it previously existed.


.cp 10
******
     The SET_KVEC system call has been expanded:

     SET_KVEC(0):   Install default scan code handler vector.
     SET_KVEC(1):   Set keyboard raw mode.
     SET_KVEC(2):   Clear keyboard raw mode, return previous raw����������������������mode state.
     SET_KVEC(3):   Read current raw mode state.

If the keyboard driver is in raw mode, all scan code interpretation��is disabled, so calls to GETCHAR(), or READ(), etc will return raw��keyboard scan codes. Of course this keyboard mode will get rather��tangled up under windowing systems such as MGR and VCON, where other��processes expect the keyboard to emit ASCII codes.

One thing that should be done before setting the keyboard into raw��mode is to set the CON: character device driver into raw mode also.��This disables input character interpretation, so if the user strikes��a key whose scan code corresponds to the current xon character, xoff��character, eof character of reset character, embarrassing things will��not happen.

.cp 10
******
     The serial I/O driver has been extensively worked, partly due��to changes associated with character devices in general. The driver��supports up to 4 SCCs, so a bus expansion card with 3 SCCs should be��easy to accommodate.

     There is a device associated with each SCC channel, described��in 'scc.h' (struct scc). The PROG_SIO system call gives access to��these structures. PROG_SIO(chan, 1) returns the address of the scc��structure for SCC channel 'chan', where 'chan' is 0 for SA: and 1 for��SB:.

     Many new things can be done to the SCC and its associated��buffers via the CDMISC() system call. See the character device��documentation for details.

     Note that detection of a break condition on the SCC is only��possible when the receiver is enabled. ie, DCD is asserted or DCD is��being ignored by having the SCC ignoring hardware flow control.

     The OS's idea of the SCC's main clock frequency can be adjusted��via OSCONTROL, for applications where the SCC is clocked at��frequencies other than 3,750,000 Hz.

.cp 10
******
     For diagnosis of absolutely disastrous crashes, the system now��dumps its exception state into memory at address $8000 at the start��of the exception handler (unless process kill on exception is��enabled). The dump formet is as follows:

     $8000 - $803f  Registers d0-d7/a0-a7
     $8040          Access address
     $8044          Function code
     $8048          Instruction register
     $804c          Status register
     $8050          3Program counter
     $8054          Pointer to a string describing the exception type

.cp 10
******
     Option 3 (exception mode) has been expanded:

     Bit 0: Stack backtrace enable
     Bit 1: If set, kill process, rather than rebooting
     Bit 2: Halt until reset on exception
     Bit 3: If Bit 2 is set, ensure that a level 0 reset will occur��������������when reset is hit.

So setting option 3 to $0c or $0d will ensure that the FIRST��exception source will be recorded, after which the system stops.��Resetting will then cause a level 0 reset. Useful for debugging��faulty memory resident driver code.

.cp 10
******
     Worked the NEW_CBUF system call, mainly so that it can itself��keep track of and allocate the character device buffers.

     NEW_CBUF(dev, 0, x)
          Set the buffer back to the default internal one within the������������$400-$3c00 memory space.
     NEW_CBUF(dev, 1, len)
          Set buffer size to 'len' bytes, allocated as mode 1������������memory. This will be freed by the system when the buffer������������size is again altered.
     NEW_CBUF(dev, -1, x)
          Return a pointer to the circular buffer structure������������associated with device 'dev'. Described in 'storedef.h'.
     NEW_CBUF(dev, addr, len)
          Set the buffer to the memory at 'addr', length 'len'.

.cp 10
******
     Standard input for EDIT and DrDoc now reverts to 'TTY:' if an��end-of-file is detected. This can happen if the editor is getting its��standard input from a file.

.cp 10
******
     SIGPIPE (signal 13) has been implemented. When a process��attempts to write to a pipe and there is no process available to��reead from the pipe's output the writing process is sent a SIGPIPE��signal.

.cp 10
******
     The options have taken a bit of a mauling, particularly those��which used to pertain to special keyboard characters. Much of their��functionality has been moved from the keyboard driver to the��character device drivers, so some of the modes to the OPTION system��call operate upon the user's current 'TTY:' device, not simply 'CON:'.

.cp 10
******
     Beep volume is installable via OSCONTROL(). You may install a��vector to your own beep code. See OSCONTROL documentation for details.

.cp 10
******
     Standard error can now be piped at the command line level with��the '^' character. There is no way to pipe both the standard output��and standard error of a command.

.cp 10
******
     GETMEM(11, 0) allocates a new pid for SCHEDPREP. This was done��so that the memory manager is entirely accessible through the system��call interface and hance can be replaced.

.cp 10
******
     System call 134 is reserved for Jeremy Fitzhardinge's symbolic��link code.

.cp 10
******
     The size of the RAM LRU directory entry cache is configurable��in the MRDRIVERS file. The default is 20 entries. Varying this will��probably have little effect.

.cp 10
******
     Behaviour when the memory allocator detects an error is now��configurable on a per-process level, as well as globally. See the��OSCONTROL documentation for details.

.cp 10
******
     The serial driver is more efficient when it is in raw mode, and��when hardware flow control is disabled.

.cp 10
******
     Fixed the ALT-^C while ALT-S underline cursor problem.

.cp 10
******
     The TERMA and TERMB commands run as separate receive and��transmit processes so they are not so CPU hungry.

.cp 10
******
     Added a mechanism by which programs can install new commands��into AEXECA. These can actually alter the argv vector before AEXECA��uses it, so aliasing and argument substitution may be performed. The��installed vector can take over the command entirely, so programs can��dynamically replace or act as a front end to inbuilt commands, MRDs��or disk-based commands.

.cp 10
******
     Fixed a freemem problem with wildcard expansion.

.cp 10
******
     The disk boot code now uses switch 2 on warm boots as well as��on level 0 resets.

.cp 10
******
     Pressing the shift key disables caps lock, if it is set.

.cp 10
******
     Added the << function to .SHELL files.

     When the shell file interpreter encounters a line such as

          command args ...                  <<EOFMARKER
          <stuff>
          <more stuff>
          EOFMARKER

     it asynchronously executes the command with its standard input��attached to a pipe. The data up to the line EOFMARKER is fed down the��command's  standard input, after which normal interpretation of the��shell file continues.

     To initiate this mechanism the openeing marker must be all��upper case characters in the ranga 'A' to 'Z' only. There must be no��trailing white space and no space between the '<<' and the opening��marker.

     The closing marker must be identical to the openeing marker and��on a line of its own. If it has an '&' character added to it the��shell interpreter will not wait for the command to terminate before��proceeding on with the sell file from the closing marker.

.cp 10
******
     There is now a shadow register set for the 6845 video��controler, and an associated system call.

crtc_init(mode, ptr)
d0:            140
d1:  mode      Programming mode
d2:  ptr       Pointer to 14 byte memory buffer
Return:        None

     mode = 0:
          Moves the 14 bytes pointed to by 'ptr' into the CRCT registers 0 to 13.
     mode = 1:
          Moves the OS image of the registers to memory pointed to bt 'ptr'.
     mode = 2:
          Restores the CRTC and its OS image to the default settings.

.cp 10
******
     GETFMEM() now fails if the requested address is not on a��32-byte boundary. The call used to massage the adress to a suitable��value but this led to some problems.

.cp 10
******
     Benefitting from multi-char I/O, the video driver is now up to��10 times faster. Best performance is for white characters on a black��background with no attributes set.

.cp 10
******
     The speed of the 'type' and 'cio' internal commands has��benefitted from some buffering.

.cp 10
******
     It has always been the case that attempting to set a process's��time slice to -1 with PROCCNTL(10, PID, TIMESLICE) returns the��process's current timeslice without changing it.

.cp 10
******
     A process should not use the CWD() system call upon itself. Use��CHDIR() instead.

.cp 10
******
     Changed the format of the PS command's output. Includes the��process timeslice and all its arguments. A new status bit 'H' is��added in the listing. This appears if the process is currently��suspended due to receipt of a SIGSTOP signal.

.cp 10
******
     Added a new system call:

chkperm(pdirent, mask, fullpath)
d0:  141
d1:  pdirent:  Pointer to directory entry
d2:  mask:     Access mode
a0:  fullpath: Name of file being accessed
Return:        BEC_NOPERM or 0

     This system call checks that the current user is permitted to��access the file/directory described by 'fullpath'. A copy of the��file's directory entry is pointed to by 'pdirent'. This routine has��been made a system call so that more extensive permission checking��may be performed, based upon the full pathname.

     The normal pathname checking code is as follows:

_chkperm(pdirent, mask, fullpath)
struct dir_entry *pdirent;
ushort mask;
char *fullpath;
{
     int ouruid;

     ouruid = GETUID();
     if ((mask & pdirent->statbits) && ouruid && (pdirent->uid != ouruid))
          return BEC_NOPERM;
     return 0;
}

.include "/usr/lib/trailer.dh"
