.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "Modes of the 1616/OS OPEN system call"
.footer   "Page $#p"

     There has been some confusion over the modes to the OPEN system��call, so here is a little note to clear things up:

MODE = 1: O_RDONLY
     The file is opened read only.  The file pointer is positioned��at the start of the file.


MODE = 2: O_APPEND
     The file is opened for read/write and the file pointer is��placed at the end of the file.  Note that this is also defined to��O_WRONLY, which is very obsolete and should not be used: files which��are opened for writing are always readable also.


MODE = 3: O_RDWR:
     The file is opened for read/write and the file pointer is��placed at the start of the file.

MODE = 4: O_TRUNCATE:
     The file's size is reduced to zero and it is opened in��read/write mode with the file pointer at the start (which is also the��end!).  This mode was added in 1616/OS V4.2 because CREAT() needs to��use it in the case where the file being CREATed already exists.  This��makes CREATs more efficient and simplifies preservation of the file's��attribute bits and permissions.

.include "/usr/lib/trailer.dh"
