.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "Char devices"
.footer   "Page $#p"

              Enhancements to character device drivers

Driver miscellaneous entry point

     Character device drivers now support an optional miscellaneous��entry point. It is installed using the ADD_IPDVR() system call. The��normal form of the system call is

     add_ipdvr(iovec, statvec, name, passval)

The extended form is

     add_xipdvr(iovec, statvec, name, passval, miscvec)

Where the 'iovec' must have bit 31 set to indicate to the system��call that 'miscvec' is valid.

     Every input driver MUST have a correspnding output vector, to��keep the device handles in the correct order. The input driver must��be installed and removed before the corresponding output driver.

     Calling ADD_IPDVR() or ADD_XIPDVR() with 'iovec' set to zero��will result in the removal of the character device driver.

     The miscellaneous vector points to a routine within the driver��which is called whenever a program performs a CDMISC() system call.

     The writing of character device drivers is not covered here,��but basically the miscellaneous routine receives the following��arguments:

     4(sp):    The character device driver number (its handle)��     8(sp):    The device's 'passval', as given when its input�����������������driver was installed.
     12(sp):   The CDMISC() command (see below)
     16(sp):   Argument 1
     20(sp):   Argument 2
     24(sp):   Argument 3

     The driver should return zero for any command which it does not��recognise.

     Miscellaneous entry points are optional. In 1616/OS only SA:��and SB: implement it fully. The CON: device has a miscellaneous entry��point simply for the purposes of returning the address of the video��driver multi char write routine.

.cp 10
The FIND_DRIVER system call

     This system call has been worked to provide individual access��to each device's chardriver structure. 

find_driver(ioro, name)
d0:       95
d1:       ioro:          Unused
d2:       name:          Pointer to device name or a device handle
Return:   Device handle or pointer to chardriver structure or error code.

     If 'name' is less than 16 this system call returns a pointer to��the chardriver structure for the corresponding character device��driver. Otherwise 'name' is assumed to be a pointer to a string such��as "CON:" and a search is performed for that character device driver.��If found its handle is returned, otherwise a negative error code is��returned.

.cp 10
The CDMISC system call

     A large data structure (chario.h: struct chardriver) is��associated with each character device driver. The CDMISC() system��call provides access to this data structure and to the driver's��miscellaneous entry point if it has one.

cdmisc(dvrnum, cmd, arg1, arg2, arg3)
d0:  133
d1:  dvrnum:   Character device driver number (device handle)
d2:  cmd:      Command
a0:  arg1:     Argument 1
a1:  arg2:     Argument 2
a2:  arg3:     Argument 3
Return:        Varies. Negative if 'dvrnum' is bad.

     'dvrnum' is the handle of the character device. It is the��device's file descriptor. It is the number which was returned when��the device's driver was installed.

     This call will return zero if 'cmd' does not require a return��value, or if 'cmd' requests a call to the device's miscellaneous��entry point and it does not have one.

     'cmd' defines the mode of the system call. At this stage��commands 0 to 31 are acted upon by the CDMISC() code in the operating��system and require no action by the driver itself. Other commands are��device specific and are acted upon by the miscellaneous code within��the device driver.

     The values for 'cmd' are defined in 'chario.h'.

0:   cmd = CDM_OPEN
     An OPEN() or CREAT() system call upon this device has been��perfomed.

1:  cmd = CDM_CLOSE
     The device has been closed. Closes and opens do not balance��correctly if the user program does not explicitly do it. CHaracter��devices are not closed if they were opened by a command line I/O��redirection.

2:   cmd = CDM_SETSIGCHAR
     The device driver compares incoming characters with the��'sigintchar' element in the chardriver structure. When a match occurs��a SIGINT is sent to the process which is blocking the shell running��from that character device. The 'sigintchar' for CON: is normally��$83, (ALT-^C). This call sets the 'sigintchar' to 'arg1'. Set it to��256 to disable.

3:   cmd = CDM_READSIGCHAR
     Returns the current setting of the addressed device's sigint char.

4:   cmd = CDM_SETEOFCHAR
     Sets the end-of-file character for the addressed character��device. Set to 256 to disable.

5:   cmd = CDM_READEOFCHAR
     Returns the current eofchar for the addressed device.

8:   cmd = CDM_SETXOFFCHAR
     'arg1' sets the character which the device driver uses for flow��control. This is the 'xoffchar' in the chardriver structure. The��driver compares incoming characters with this character. When a match��occurs output from the device is suspended until an 'xonchar' is��read. The normal 'xonchar' for CON: is $D3, which corresponds to��ALT-S. Set to 256 to disable.

9:   cmd = CDM_READXOFFCHAR
     Returns the current 'xoffchar' for the addressed device.

10:  cmd = CDM_SETXONCHAR
     'arg1' sets the 'xonchar', which, when received, restarts��suspended output. Normally $D3 for the CON: driver (ALT-S). Set this��to 256 to disable.

11:  cmd = CDM_READXONCHAR
     Returns current 'xonchar' for the addressed device.

12:  cmd = CDM_SETRESETCHAR
     The character device driver compares incoming characters with��the 'resetchar' field in the chardriver structure. If a match is��found the driver performs a WARMBOOT() system call. This field is $92��for the CON: driver (ALT-^R). This command moves 'arg1' to the��'resetchar' for the addressed device. Use 256 to disable.

13:  cmd = CDM_READRESETCHAR
     Returns the current 'resetchar' for the addressed device.

14:  cmd = CDM_SENDSIGINT
     This command is performed by the character device driver at��interrupt time when it matches an incoming character with the��'sigintchar' field in the chardriver structure. The operating system��records the character device driver number and will shortly send a��SIGINT to the process which is blocking the shell running off that��device. This is how ALT-^C works.

15:  cmd = CDM_SENDSIGHUP
     Like command 14, except a SIGHUP is sent.

16:  cmd = CDM_KILLUSER
     Like the above, but a KILLUSER() system call is performed upon��the appropriate shell process.

17:  cmd = CDM_SETRAWMODE
     Sets the 'rawmode' field of the addressed device's chardriver��structure to 'arg1'. If 'rawmode' is set all input processing is��disabled: SIGINT, resets, xon, xoff characters are all passed��through. This facility is provided so that the device may be put in��raw mode without having to individually record and disable every��magic character in the structure.

18:  cmd = CDM_READRAWMODE
     Returns the 'rawmode' field from the addressed device's��chardriver structure.

19:  cmd = CDM_MIORVEC
     If the character device driver supports multichar reads it must��return the address of its multichar read routine when it receives��this command. If the driver does not support multichar reads, it��returns 0.

     The multichar read entry point is called with the following��arguments:

     4(sp):    Device handle (or file descriptor)
     8(sp):    Memory transfer address
     12(sp):   Number of bytes to transfer
     16(sp):   Pass value with which the input device driver was�����������������installed.

     The driver returns the number of bytes actually read to the��passed address, which must be less than or equal to the passed byte��count. The driver should return if an incoming character matches the��chardriver end of file character and the device is not in raw mode.��It should return BEC_RPASTEOF if an eofchar is read as the very first��byte. It should return on a newline character. It should compare each��character with the resetchar, eofchar, sigintchar, etc and take the��appropriate action, provided the device is not in raw mode.

     The multichar read entry point is called directly from within��the operating system, so it should preserve the machine registers��(except for d0).

     The device driver obtains a pointer to the chardriver structure��for the addressed device using the FIND_DRIVER() system call. For��performance these tables should be found at installation time and��their addresses should be saved within the driver's storage.

20:  cmd = CDM_MIOWVEC
     Similar to CDM_MIORVEC, the driver returns its multi char write��entry point or 0 if not implemented. The write code is called in a��similar manner to the read code, expect that it must only return��after all the characters have been sent and it need do no special��character processing, except for looking at the 'xoffed' field in the��chardriver structure to see if output is suspended. The value passed��to the multichar write code at 16(sp) is the pass value which was��supplied when the output device was installed, rather than the input��device.

21:  cmd = CDM_SETHUPMODE
     Sets the 'hupmode' field of the device's chardriver structure��to 'arg1'. The 'hupmode' field tells the character device driver what��to do when a loss of carrier (DCD signal) is detected at interrupt��time.

     hupmode = 1:   Send a SIGHUP to all processes which are running����������������������from a shell running off the device
     hupmode = 2:   Perform a KILLUSER() system call upon the shell����������������������running on the device.
     hupmode = 3:   Send a SIGINT to the process which is blocking����������������������the shell running off that device.

     It is the character device driver's responsibility to perform��these actions at interrupt time when loss of carrier is detected,��based on its 'hupmode' field.

     The SA: and SB: drivers only enable their DCD interrupts when��the respective 'hupmode' field is non-zero.

22:  cmd = CDM_READHUPMODE
     Return the addressed device's 'hupmode'.

23:  cmd = CDM_HASMISCVEC
     Returns true if the addressed device has a miscellaneous entry��point vector installed.

24:  cmd = CDM_SETUSERBITS
     There is a 'userbits' field in the char device structure the��use of which is basically not defined at this stage, except for bit 0��which, if set, is intended to tell the driver to perform TVI950 to��something else escape code translation.

     If 'arg1' is zero, 'arg2' is ORed into the 'userbits'.
     If 'arg1' is 1, 'arg2' is inverted and ANDed into the 'userbits'.
     Otherwise the 'userbits' field is returned unchanged.


The following call modes are not handled by the operating system.��Any action or return value is handled by the device specific driver��code, which may be in the ROMs, or in an MRD.

32:  cmd = CDM_SETMODE
     'arg1' is treated as a pointer to the standard serial I/O��programming structure as described in the documentation for the��PROG_SIO() system call. The system call

     CDMISC(handle, CDM_SETMODE, pointer, 0, 0)

is a now a preferable way of programming a serial device, as it��should work with other hardware, if added. From the command line, use��the 'chdev' program for this.

33:  cmd = CDM_READMODE
     The driver moves the standard serial I/O programming structure��to memory pointed to by 'arg1'.

34:  cmd = CDM_SETDTR
     If 'arg1' is non-zero, the driver asserts the device's DTR��signal; otherwise it is cleared.

     eg:  CDMISC(OPEN("SA:", 0), CDM_SETDTR, 1, 0, 0) will assert������������DTR on serial channel A.

35:  cmd = CDM_SETRTS
     Same as above, for RTS signal.

36:  cmd = CDM_READDCD
     Returns non-zero if the addressed device's DCD signal is��currently asserted.

37:  cmd = CDM_READCTS
     Returns non-zero if the addressed device's CTS signal is��currently asserted.
     
38:  cmd = CDM_READBREAK
     Returns non-zero if the addressed device is receiving a BREAK��condition.      

39:  cmd = CDM_SETHFC
     If 'arg1' is non-zero, sets the device into hardware flow��control mode. For the SCC this is the default. DCD qualifies receive��data, CTS is used for hardware flow control, DTR is always asserted,��RTS is negated when the device receive buffer is nearly full.

     When hardware flow control is disabled the SCC asserts both RTS��and DTR and then runs in 3 wire mode, competely ignoring the��handshake signals. The SCC channel is taken out of 'auto-enables'��mode.

40:  cmd = CDM_SETBREAK
     If 'arg1' is non-zero, start a break condition on the��transmitter of the addressed device. Otherwise clear the break��condition.

41:  cmd = CDM_TXCOUNT
     Returns the number of characters still buffered for��transmission from the addressed device.

42:  cmd = CDM_RXCOUNT
     Returns the number of characters which are available in the��software receive buffer which the driver maintains for the device. 

43:  cmd = CDM_TXROOM
     Returns the number of characters which can be sent to the��device's output channel (via the WRITE system call, etc) before the��write will block due to the software output buffer filling up.

44:  cmd = CDM_RXROOM
     Returns the number of characters which can still be received on��this device before its software receive buffering overruns.

45:  cmd = CDM_TXFLUSH
     Wait until all buffered transmit characters have been sent.

46:  cmd = CDM_TXPURGE
     Zero the transmit buffer pointers, dumping all pending output.

47:  cmd = CDM_RXPURGE
     Zero the receive buffer pointers, dumping all pending input.

48:  cmd = CDM_RXPEEK
     Returns the next character which will be read from the input��device (via a READ(), GETCHAR() system call, etc). If no character is��available, returns -1.

49:  cmd = CDM_SETTXBSIZE
     Sets the size of the device's transmit buffer to 'arg1'

50:  cmd = CDM_SETRXBSIZE
     Sets the size of the device's receive buffer to 'arg1'

51:  cmd = CDM_READTXBSIZE
     Returns the current size of the device's transmit buffer.

52:  cmd = CDM_READRXBSIZE
     Returns the current size of the device's receive buffer.

53:  cmd = CDM_VERSION
     Returns the low-level device driver version number. For the��drivers within 1616/OS (SA: and SB:) the operating system version is��returned here.


Changes to the serial drivers SA: and SB:

     The ROM drivers for the SCC have been altered so that they��implement all of the character device miscellaneous functions above.��This entailed the enabling of interrupts on the DCD input pins. This��change can cause 1616s with V4.2 ROMs to hang up or to run very��slowly. The reason for this is incorrect serial port strapping��causing a large number of interrupts on the DCD pin (probably open��circuit): the fix is to ensure the DCD pin is strapped high or low.

     Note that this should only happen when a program has changed��the serial driver's 'hupmode' (hang-up mode) setting.  Normally the��SA: and SB: drivers are set up to perform no action on loss of DCD,��so the DCD interrupts are disabled. Setting a channel's 'hupmode'��non-zero causes the driver to enable the respective channel's DCD��interrupts.

.include "/usr/lib/trailer.dh"
