.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "Using 1616/OS V4.7a as a diskless Unix client"
.footer   "Page $#p"


            Using 1616/OS V4.7a as a diskless Unix client
                            Andrew Morton
                          18 February 1992


A number of the 4.7a features which are described in the file��"4.7.doc" are designed for the use of a 1616 as a diskless client of��a Unix based host.

The features in the OS have been designed so that considerable��functionality can be had without adding too much complexity and code��to the OS ROMs.  The philosophy is that the ROMs offer the basic��services and that more sophisticated, application specific tasks are��performed by programs downloaded from the host.


Some suggestions about configuring the host:


                         Design of 'rz1616'
                         ==================

The 'puta' command invokes the program 'rz1616' on the host.  At the��time of writing the 'rz1616' program is a simple Bourne shell script��which invokes 'rz'.

It is intended that 'rz1616' be more sophisticated.  For example, it��can check write access permissions in the current directory and if��they are denied it can chdir to another directory.  This directory��should be called '$HOME/1616files'.  Alternatively, 'rz1616' can��force all files to be placed under '$HOME/1616files'.

It is desirable that 'puta' be able to specify a host directory for��placing files.  This is simple to implement, as 'puta' sends all of��its command line arguments to 'rz1616'.  So it is suggested that when��the user enters the command

     puta -ddirpath file1 file1

That 'rz1616' place the incoming files in the directory 'dirpath'. ��The best way of doing this is to 'cd' there and then invoke 'rz'.  If��'dirpath' cannot be written to then 'rz1616' should refrain from��sending the 'X' characters back to 'puta', and 'puta' will quickly��time out.

Reminder: "puta -a filename" will force 'rz' at teh Unix end to��strip '\r' characters from the file, for 1616 to Unix conversion. ��'rz1616' must pass the '-a' flag to 'rz' for this to happen.

Another reminder: When 'puta' is sending files it ignores filenames��beginning with a '-' character, under the assumption that they are��flags to 'rz1616'.  Thus in the example above, 'puta' will not��attempt to send the file "-ddirpath".  If there had been a space��after the "-d" the 'puta' would have attempted to send the file��'dirpath'.  This would not cause any difficulties, however.

A note about Zmodem: If something goes wrong and the Unix system is��still running 'sz' or 'rz' the user may send the Unix system a Zmodem��abort sequence from the keyboard.  This is done by sending ten or so��control-X characters.


                         Design of 'sz1616'
                         ==================


'sz1616' is currently a simple Bourne shell script.  A more��sophisticated 'sz1616' should do a number of additional things.

There should be support for a common area of files, as there is��little point in a whole class of students having private copies of��commonly used 1616 programs and files.  So when 'sz1616' is asked to��send a group of files it should do the following:

arg_list = "sz ";
for each filename
{
     if (stat(filename) == 0)
     {
          add filename to arg_list;
     }
     else if (stat($HOME/1616files/filename) == 0)
     {
          add $HOME/1616files/filename to arg_list
     }
     else if (stat($SSFILES/filename) == 0)
     {
          add $SSFILES/filename to arg_list
     }
     else if (stat(/usr/local/lib/1616files/filename) == 0)
     {
          add /usr/local/lib/1616files/filename to arg_list
     }
}

if (arg_list != "sz ")
{
     echo("XXX");        /* Sync with 'geta' command */
     echo("XXX");
     system(arg_list);   /* Send the files */
}
else
     exit();             /* Let 'geta' timeout */

That's the way I'd do it anyway......

Reminder:  Giving 'geta' the '-a' flag sends this flag to 'sz1616',��which should pass it onto 'sz'.  'sz' will then send control��information to 'getz' on the 1616 which will then perform NL to CR-NL��conversion.


                               Booting
                               =======

The 1616 will need a decent receive buffer to aviod overruns during��Zmodem receive, and to speed up terminal output.  One or two kbytes��is sufficient.

The 'CDMISC' system call can be used to vary the buffer size.  For��the SA: port:

     CDMISC(1, CDM_SETRXBSIZE, 2048);
or
     syscall 133 .50 .2048

This command may be put in the 'boot1616.shell' file.

This startup file should also set the XON/XOFF protocol, which��defaults to being turned off.  The CDMISC system call may be used for��this.
