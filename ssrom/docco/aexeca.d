.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "The aexeca system call"
.footer   "Page $#p"

The aexeca() system call starts a new 1616/OS process.  It is passed��two arguments:

aexeca(argv, flag)

where 'argv' is a pointer to a table of pointers to null-terminated��strings.  The last pointer in this table must have a value of zero. ��The first string is the name of the command to be executed and the��remaining strings represent the arguments to that command.

The 'flags' argument has the following usage:

Bit 0:
     If set, the new process is run asynchronously with respect to��the caller.  That is, it is run in the background.  In this case��'aexeca' will return the newly created process's PID.
     If bit 0 is clear, the new process runs synchronously, so the��caller is suspended until the new program terminates.  In this case,��the call to 'aexeca' returns the called program's exit code.

Bit 31:
     If set, the new process is a 'shell type' process, which means��that it receives a new copy of its parent shell's environment, rather��than sharing it.

The 'execa' system call is identical to 'aexeca', with a 'flags'��argument of zero.

The called program receives a copy of the 'argv' array, as well as��the 'argtype' and 'argval' arrays, which are calculated from the��'argv' array during the 'schedprep' system call.

Note that the 'aexeca' system call should ONLY be called with��pointers to strings in the 'argv' array.  Do not attempt to pass��numeric data in this array, as the operating system will interpret��the numbers as pointers to strings and this may well crash the system.

To pass the value of internal variables to a sub-program it is��necessary to convert them to string representations first.  This can��be done quite simply by using the 'sprintf' system call to format the��arguments.  The called program may read the numbers from the 'argval'��array, as the operating system performs the string to numeric��conversion for you.  Note that this is done using the leading '.'��convention for decimal numbers.

The following program demonstrates the use of 'aexeca' from within��an assembly program:

*
* Demonstrate the use of the execa system call.
* execute the commnad 'TYPE TEST.S' synchronously.
*
          move.l    #cmdname,argvbuf
          move.l    #testname,argvbuf+4 * Set up argument pointers
          clr.l     argvbuf+8           * Terminal nil pointer
          move.l    #argvbuf,d1         * Pointer to argument pointers
          move.l    #97,d0              * execa system call number
          trap #7
          tst.l     d0                  * Error?
          bmi  errorhandler
          rts                           * Return the code to the system

errorhandler:  nop
          rts

asmname:  dc.b 'TYPE',0
testname: dc.b 'TEST.S',0
argvbuf:  ds.l 10                       * Pointer array
          end


.include "/usr/lib/trailer.dh"
