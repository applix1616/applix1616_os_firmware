.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "Shell process environment"
.footer   "Page $#p"

          The shell process environment under 1616/OS V4.2b
                  Andrew Morton, Applix pty limited
                           24th March 1990



Introduction

     A special sort of process known as a shell-type process has��been introduced. The most common shell process is that which the��IEXEC() system call starts up: the process which displays a prompt,��reads a command from the keyboard and executes it.

     Shell processes are special because they are treated as the��leading process in a group of processes. An 'enviroment' is��associated with each shell process running in the 1616. The��environment consists of an inbuilt command disable vector, a group of��option bits and a collection of 'environment variables' which have��various uses.





.eject
.header "Character device drivers"

              Enhancements to character device drivers

Driver miscellaneous entry point

     Character device drivers now support an optional miscellaneous��entry point. It is installed using the ADD_IPDVR() system call. The��normal form of the system call is

     add_ipdvr(iovec, statvec, name, passval)

The extended form is

     add_xipdvr(iovec, statvec, name, passval, miscvec)

Where the 'iovec' must have bit 31 set to indicate to the system��call that 'miscvec' is valid.

     Every input driver MUST have a correspnding output vector, to��keep the device handles in the correct order. The input driver must��be installed and removed before the corresponding output driver.

     Calling ADD_IPDVR() or ADD_XIPDVR() with 'iovec' set to zero��will result in the removal of the character device driver.

     The miscellaneous vector points to a routine within the driver��which is called whenever a program performs a CDMISC() system call.

     The writing of character device drivers is not covered here,��but basically the miscellaneous routine receives the following��arguments:

     4(sp):    The character device driver number (its handle)��     8(sp):    The device's 'passval', as given when its input�����������������driver was installed.
     12(sp):   The CDMISC() command (see below)
     16(sp):   Argument 1
     20(sp):   Argument 2
     24(sp):   Argument 3

     The driver should return zero for any command which it does not��recognise.

     Miscellaneous entry points are optional. In 1616/OS only SA:��and SB: implement it fully. The CON: device has a miscellaneous entry��point simply for the purposes of returning the address of the video��driver multi char write routine.

.cp 10
The FIND_DRIVER system call

     This system call has been worked to provide individual access��to each device's chardriver structure. 

find_driver(ioro, name)
d0:       95
d1:       ioro:          Unused
d2:       name:          Pointer to device name or a device handle
Return:   Device handle or pointer to chardriver structure or error code.

     If 'name' is less than 16 this system call returns a pointer to��the chardriver structure for the corresponding character device��driver. Otherwise 'name' is assumed to be a pointer to a string such��as "CON:" and a search is performed for that character device driver.��If found its handle is returned, otherwise a negative error code is��returned.

.cp 10
The CDMISC system call

     A large data structure (chario.h: struct chardriver) is��associated with each character device driver. The CDMISC() system��call provides access to this data structure and to the driver's��miscellaneous entry point if it has one.

cdmisc(dvrnum, cmd, arg1, arg2, arg3)
d0:  133
d1:  dvrnum:   Character device driver number (device handle)
d2:  cmd:      Command
a0:  arg1:     Argument 1
a1:  arg2:     Argument 2
a2:  arg3:     Argument 3
Return:        Varies. Negative if 'dvrnum' is bad.

     'dvrnum' is the handle of the character device. It is the��device's file descriptor. It is the number which was returned when��the device's driver was installed.

     This call will return zero if 'cmd' does not require a return��value, or if 'cmd' requests a call to the device's miscellaneous��entry point and it does not have one.

     'cmd' defines the mode of the system call. At this stage��commands 0 to 31 are acted upon by the CDMISC() code in the operating��system and require no action by the driver itself. Other commands are��device specific and are acted upon by the miscellaneous code within��the device driver.

     The values for 'cmd' are defined in 'chario.h'.

0:   cmd = CDM_OPEN
     An OPEN() or CREAT() system call upon this device has been��perfomed.

1:  cmd = CDM_CLOSE
     The device has been closed. Closes and opens do not balance��correctly if the user program does not explicitly do it. CHaracter��devices are not closed if they were opened by a command line I/O��redirection.

2:   cmd = CDM_SETSIGCHAR
     The device driver compares incoming characters with the��'sigintchar' element in the chardriver structure. When a match occurs��a SIGINT is sent to the process which is blocking the shell running��from that character device. The 'sigintchar' for CON: is normally��$83, (ALT-^C). This call sets the 'sigintchar' to 'arg1'. Set it to��256 to disable.

3:   cmd = CDM_READSIGCHAR
     Returns the current setting of the addressed device's sigint char.

4:   cmd = CDM_SETEOFCHAR
     Sets the end-of-file character for the addressed character��device. Set to 256 to disable.

5:   cmd = CDM_READEOFCHAR
     Returns the current eofchar for the addressed device.

8:   cmd = CDM_SETXOFFCHAR
     'arg1' sets the character which the device driver uses for flow��control. This is the 'xoffchar' in the chardriver structure. The��driver compares incoming characters with this character. When a match��occurs output from the device is suspended until an 'xonchar' is��read. The normal 'xonchar' for CON: is $D3, which corresponds to��ALT-S. Set to 256 to disable.

9:   cmd = CDM_READXOFFCHAR
     Returns the current 'xoffchar' for the addressed device.

10:  cmd = CDM_SETXONCHAR
     'arg1' sets the 'xonchar', which, when received, restarts��suspended output. Normally $D3 for the CON: driver (ALT-S). Set this��to 256 to disable.

11:  cmd = CDM_READXONCHAR
     Returns current 'xonchar' for the addressed device.

12:  cmd = CDM_SETRESETCHAR
     The character device driver compares incoming characters with��the 'resetchar' field in the chardriver structure. If a match is��found the driver performs a WARMBOOT() system call. This field is $92��for the CON: driver (ALT-^R). This command moves 'arg1' to the��'resetchar' for the addressed device. Use 256 to disable.

13:  cmd = CDM_READRESETCHAR
     Returns the current 'resetchar' for the addressed device.

14:  cmd = CDM_SENDSIGINT
     This command is performed by the character device driver at��interrupt time when it matches an incoming character with the��'sigintchar' field in the chardriver structure. The operating system��records the character device driver number and will shortly send a��SIGINT to the process which is blocking the shell running off that��device. This is how ALT-^C works.

15:  cmd = CDM_SENDSIGHUP
     Like command 14, except a SIGHUP is sent.

16:  cmd = CDM_KILLUSER
     Like the above, but a KILLUSER() system call is performed upon��the appropriate shell process.

17:  cmd = CDM_SETRAWMODE
     Sets the 'rawmode' field of the addressed device's chardriver��structure to 'arg1'. If 'rawmode' is set all input processing is��disabled: SIGINT, resets, xon, xoff characters are all passed��through. This facility is provided so that the device may be put in��raw mode without having to individually record and disable every��magic character in the structure.

18:  cmd = CDM_READRAWMODE
     Returns the 'rawmode' field from the addressed device's��chardriver structure.

19:  cmd = CDM_MIORVEC
     If the character device driver supports multichar reads it must��return the address of its multichar read routine when it receives��this command. If the driver does not support multichar reads, it��returns 0.

     The multichar read entry point is called with the following��arguments:

     4(sp):    Device handle (or file descriptor)
     8(sp):    Memory transfer address
     12(sp):   Number of bytes to transfer
     16(sp):   Pass value with which the input device driver was�����������������installed.

     The driver returns the number of bytes actually read to the��passed address, which must be less than or equal to the passed byte��count. The driver should return if an incoming character matches the��chardriver end of file character and the device is not in raw mode.��It should return BEC_RPASTEOF if an eofchar is read as the very first��byte. It should return on a newline character. It should compare each��character with the resetchar, eofchar, sigintchar, etc and take the��appropriate action, provided the device is not in raw mode.

     The multichar read entry point is called directly from within��the operating system, so it should preserve the machine registers��(except for d0).

     The device driver obtains a pointer to the chardriver structure��for the addressed device using the FIND_DRIVER() system call. For��performance these tables should be found at installation time and��their addresses should be saved within the driver's storage.

20:  cmd = CDM_MIOWVEC
     Similar to CDM_MIORVEC, the driver returns its multi char write��entry point or 0 if not implemented. The write code is called in a��similar manner to the read code, expect that it must only return��after all the characters have been sent and it need do no special��character processing, except for looking at the 'xoffed' field in the��chardriver structure to see if output is suspended. The value passed��to the multichar write code at 16(sp) is the pass value which was��supplied when the output device was installed, rather than the input��device.

21:  cmd = CDM_SETHUPMODE
     Sets the 'hupmode' field of the device's chardriver structure��to 'arg1'. The 'hupmode' field tells the character device driver what��to do when a loss of carrier (DCD signal) is detected at interrupt��time.

     hupmode = 1:   Send a SIGHUP to all processes which are running����������������������from a shell running off the device
     hupmode = 2:   Perform a KILLUSER() system call upon the shell����������������������running on the device.
     hupmode = 3:   Send a SIGINT to the process which is blocking����������������������the shell running off that device.

     It is the character device driver's responsibility to perform��these actions at interrupt time when loss of carrier is detected,��based on its 'hupmode' field.

     The SA: and SB: drivers only enable their DCD interrupts when��the respective 'hupmode' field is non-zero.

22:  cmd = CDM_READHUPMODE
     Return the addressed device's 'hupmode'.

23:  cmd = CDM_HASMISCVEC
     Returns true if the addressed device has a miscellaneous entry��point vector installed.

24:  cmd = CDM_SETUSERBITS
     There is a 'userbits' field in the char device structure the��use of which is basically not defined at this stage, except for bit 0��which, if set, is intended to tell the driver to perform TVI950 to��something else escape code translation.

     If 'arg1' is zero, 'arg2' is ORed into the 'userbits'.
     If 'arg1' is 1, 'arg2' is inverted and ANDed into the 'userbits'.
     Otherwise the 'userbits' field is returned unchanged.


The following call modes are not handled by the operating system.��Any action or return value is handled by the device specific driver��code, which may be in the ROMs, or in an MRD.

32:  cmd = CDM_SETMODE
     'arg1' is treated as a pointer to the standard serial I/O��programming structure as described in the documentation for the��PROG_SIO() system call. The system call

     CDMISC(handle, CDM_SETMODE, pointer, 0, 0)

is a now a preferable way of programming a serial device, as it��should work with other hardware, if added. From the command line, use��the 'chdev' program for this.

33:  cmd = CDM_READMODE
     The driver moves the standard serial I/O programming structure��to memory pointed to by 'arg1'.

34:  cmd = CDM_SETDTR
     If 'arg1' is non-zero, the driver asserts the device's DTR��signal; otherwise it is cleared.

     eg:  CDMISC(OPEN("SA:", 0), CDM_SETDTR, 1, 0, 0) will assert������������DTR on serial channel A.

35:  cmd = CDM_SETRTS
     Same as above, for RTS signal.

36:  cmd = CDM_READDCD
     Returns non-zero if the addressed device's DCD signal is��currently asserted.

37:  cmd = CDM_READCTS
     Returns non-zero if the addressed device's CTS signal is��currently asserted.
     
38:  cmd = CDM_READBREAK
     Returns non-zero if the addressed device is receiving a BREAK��condition.      

39:  cmd = CDM_SETHFC
     If 'arg1' is non-zero, sets the device into hardware flow��control mode. For the SCC this is the default. DCD qualifies receive��data, CTS is used for hardware flow control, DTR is always asserted,��RTS is negated when the device receive buffer is nearly full.

     When hardware flow control is disabled the SCC asserts both RTS��and DTR and then runs in 3 wire mode, competely ignoring the��handshake signals. The SCC channel is taken out of 'auto-enables'��mode.

40:  cmd = CDM_SETBREAK
     If 'arg1' is non-zero, start a break condition on the��transmitter of the addressed device. Otherwise clear the break��condition.

41:  cmd = CDM_TXCOUNT
     Returns the number of characters still buffered for��transmission from the addressed device.

42:  cmd = CDM_RXCOUNT
     Returns the number of characters which are available in the��software receive buffer which the driver maintains for the device. 

43:  cmd = CDM_TXROOM
     Returns the number of characters which can be sent to the��device's output channel (via the WRITE system call, etc) before the��write will block due to the software output buffer filling up.

44:  cmd = CDM_RXROOM
     Returns the number of characters which can still be received on��this device before its software receive buffering overruns.

45:  cmd = CDM_TXFLUSH
     Wait until all buffered transmit characters have been sent.

46:  cmd = CDM_TXPURGE
     Zero the transmit buffer pointers, dumping all pending output.

47:  cmd = CDM_RXPURGE
     Zero the receive buffer pointers, dumping all pending input.

48:  cmd = CDM_RXPEEK
     Returns the next character which will be read from the input��device (via a READ(), GETCHAR() system call, etc). If no character is��available, returns -1.

49:  cmd = CDM_SETTXBSIZE
     Sets the size of the device's transmit buffer to 'arg1'

50:  cmd = CDM_SETRXBSIZE
     Sets the size of the device's receive buffer to 'arg1'

51:  cmd = CDM_READTXBSIZE
     Returns the current size of the device's transmit buffer.

52:  cmd = CDM_READRXBSIZE
     Returns the current size of the device's receive buffer.

53:  cmd = CDM_VERSION
     Returns the low-level device driver version number. For the��drivers within 1616/OS (SA: and SB:) the operating system version is��returned here.


Changes to the serial drivers SA: and SB:

     The ROM drivers for the SCC have been altered so that they��implement all of the character device miscellaneous functions above.��This entailed the enabling of interrupts on the DCD input pins. This��change can cause 1616s with V4.2 ROMs to hang up or to run very��slowly. The reason for this is incorrect serial port strapping��causing a large number of interrupts on the DCD pin (probably open��circuit): the fix is to ensure the DCD pin is strapped high or low.

     Note that this should only happen when a program has changed��the serial driver's 'hupmode' (hang-up mode) setting.  Normally the��SA: and SB: drivers are set up to perform no action on loss of DCD,��so the DCD interrupts are disabled. Setting a channel's 'hupmode'��non-zero causes the driver to enable the respective channel's DCD��interrupts.



.eject
.header "1616/OS V4.2a"

                  Overview of 1616/OS V4.2a changes
                  Andrew Morton, Applix pty limited
                            10 March 1990

.cp 10
Introduction

     This document contains the shortest possible description of the��changes from 1616/OS Version 4.1a to Version 4.2a.

     There is nothing really major in this release. It has a has a��number of enhancements, bug fixes and alterations, most notable being��the improved file system and video driver performance, enhanced��character device control, automatic directory growth, unlimited file��sizes, fixed file permissions and a line editor enhancement.


.cp 10
Changes

.cp 10
******
     close() system call improved by changing order of writes to��save a seek to device bitmap and back.

.cp 10
******
     iexec() system call termintaes when it receives a SIGHUP��(signal 1)

.cp 10
******
     The message 'pid: terminated' is now emitted from the 'kill'��inbuilt command, not from the scheduler, so it comes out on a��rational character device.

.cp 10
******
     Added option 15 (OPT_MISC): miscellaneous option bits:

Bit 0: OPM_NOBAK:   If this bit is set, edit & DrDoc don't creat���������'.bak' files (For Colin)

Bit 1: OPM_ALTCINT: Broken.

Bit 2: OPM_NKPCQ:   If this bit is set, the '5' key on the numeric���������keypad generates a '^Q' in non-numeric mode.

Bit 3: OPM_BEEPONERR: If this bit is set and option 12 (enable beep���������on errors) is set then the system emits a beep on standard���������error whenever any program exits with a negative exit code.

.cp 10
******
     Char device drivers are cleared and reinstalled at all levels��of reset. The only attribute which is preserved is the CON: eof��character.

.cp 10
******
     Fixed file permissioning.

.cp 10
******
     Enhanced the 'kill' inbuilt command:

     kill -akNN pid pid

     -a:  Use the SIGDOWN or KILLDOWN system calls to signal/kill������������all children as well.
     -k:  Unconditionally kill the addressed processes, rather than������������signalling.
     NN:  Specify a (decimal) signal to send, rather than SIGTERM������������(signal 15)

These may be combined. The rather unsuccesful attempt to permit PID��wildcards in this command has been dumped.

.cp 10
******
     Fixed problem where memory for a .EXEC file was never freed if��an I/O error occurred while loading it from disk.

.cp 10
******
     Note that alarm(0) clears pending alarm, returns -(ticks until��alarm)

.cp 10
******
     The codes for ALT-S, ALT-^R, etc are now returned if their��special meanings are optioned out. Part of the new char driver magic.

.cp 10
******
     Added the line editor ESC key command: type a few chars, hit��ESC and a search is made back through the history for a line starting��with those characters. Repeated ESC's perform repeated searches.

.cp 10
******
     Had a problem correctly wedging stdin of an asynch process to��NULL:. Fixed.

.cp 10
******
     CLOSE(-1) did not always flush all open files. Fixed.

.cp 10
******
     Keyboard driver now alters state of caps lock & numlock based��on make & break codes, rather than counting make codes, for keyboards��which autorepeat on these keys.

.cp 10
******
     Pressing the scroll-lock key generates the code 243, which is��ALT-S. This may be used to stop and start scrolling.

.cp 10
******
     The line editor ^A, ^F and ^T commands now terminate on the��following characters:

     space, tab, . ! , : ; ( )   {  } / -

.cp 10
******
     Fixed a wee bug in the 'type' command which made it only type��one file under MGR.

.cp 10
******
     The last line recall depth is now set in MRDRIVERS file. The��default is 10 lines.

.cp 10
******
     A problem involving the flashing cursor across DEF_WIND calls��has been fixed.

.cp 10
******
     A pseudo-device called "TTY:" has been added. This does not��take up room in the char device table, but an OPEN or CREATE of this��device returns an inspired guess at the file descriptor of the��character device which the user is looking at. This device should be��used where a program definitely wants to get interactive input from��the user, ignoring standard input redirection.

.cp 10
******
     The character and block device installation routines now��correctly delete the named driver when an install is done with the��passed I/O equal to zero.

.cp 10
******
     Added SIGBLOCKER(), signal types SIGSTOP, SIGCONT. See PROCCNTL��documentation.

.cp 10
******
     The '*' character is now valid (as well as 'x') for��multiplication in the 'expr' inbuilt command. Should be escaped by��double quotes.

.cp 10
******
     Passing a '%E' to the PRINTF/SPRINTF/FPRINTF system calls��causes conversion from integer to system error message, so a negative��system call return value can be passed directly to FPRINTF for��diagnostics, rather than converting with ERRMES() and then using��'%s'. Be sure that the program will never be used on earlier versions��of the OS before using this!

.cp 10
******
     There was a problem in the process scheduler which meant that��the SLEEP() system call actually slept for one tick more than was��desired. This has been fixed, but this may affect some programs.

.cp 10
******
     Fixed a bug in the reset startup code which trashed memory at��the start of on-board RAM + $10000, which limited RAM disk size to��around 300k. RAM disk maximum is now 1 Mbyte

.cp 10
******
     Typing control-^ (CTL-SHIFT-6) now generates ascii code 30.
     Typint control-@ (CTL-SHIFT-2) noe generates ascii code 00.

.cp 10
******
     The call FIND_BDVR(-1) is supposed to return a pointer to the��block device structure for the current process's currently logged��device, but it in fact returned a pointer to the last device which��CD'ed to. Fixed.

.cp 10
******
     Added lots of modes to OSCONTROL. See the relevant document.

.cp 10
******
     Fixed spelling of the privilege viloation exception message. (!)

.cp 10
******
     When a file which is open for writing is interrogated by the��FILESTAT system call it is first flushed to disk, so the returned��file size, etc is more up to date.

.cp 10
******
     Fixed a problem in FILESTAT which made it much slower than��necessary.

.cp 10
******
     Fixed a problem in EDIT where it could emit random cursor��positioning sequences.

.cp 10
******
     Enhanced the GETTDSTR() system call:

     GETTDSTR(buf, arg1, arg2, arg3)

     GETTDSTR(buf, x, x, x)
     If 'buf' is non-zero, read current time to memory at *buf, as��in previous OS versions.

     GETTDSTR(0, 0, dateptr, stringptr)
     If 'buf' is zero and 'arg1' is zero, convert the 8 byte time��pointed to by 'arg2' into human readable string form at memory��pointed to by 'arg3'.

     GETTDSTR(0, 1, increment, x)
     GETTDSTR(0, 1, 0, x)
     If 'buf' is zero and 'arg1' is equal to 1 then set the��date/time increment to 'arg2', provided 'arg2' is non-zero. If 'arg2'��is zero then simply return current value of date/time increment.

     The time increment is the number of microseconds between��vertical sync interrupts. Normally 19968, this may be varied to trim��the clock under different video programming modes.

.cp 10
******
     Changed the order of bitmap and directory writes in UNLINK().��Saves a seek, a little quicker now.

.cp 10
******
     Control has been added for disposing of all the last-line��history of a character device, or of all character devices. This is��documented with OSCONTROL(). Used by the memory manager to defragment��and to increase free memory when a GETMEM fails. May be used to��prevent successive users from seeing what others have typed.

     The presence of this control means that a process's last line��history can be dumped by another process at any time.

     If your last lines mysteriously disappear then it probably��means that a process has requested more memory than was available.

.cp 10
******
     There was a problem with the ^KV command in EDIT. Fixed.

.cp 10
******
     Tidied up 'DIRS' output so it is readable on displays which are��not 80 characters wide.

.cp 10
******
     Running a 0 length .XREL file does not blow up now.

.cp 10
******
     Put in support for CDMISC() system call, miscellaneous access��to individual char device drivers. See other documentation for��details.

.cp 10
******
     The maximum file size restriction has been lifted by ensuring��that the blockmap for a file extends across contiguous blocks, one��per half megabyte. This means that every time the system grows a file��across a 0.5 Mbyte multiple it must relocate the entire blockmap. If��there is nowhere to put it a disk full error results. At the time of��writing 'fscheck' does not understand files longer than 0.5 Mbytes.

.cp 10
******
     Full directories are automatically grown by one block (16��entries). No mechanism is provided for automatically shrinking them.��The size specification has been removed from the 'mkdir' inbuilt��command, so multiple directories can be made in one command.

.cp 10
******
     Rationalised handling of GETFULLPATH() sustem call. It is��called far less frequently. The usage of block device volume names to��refer to the device has been removed, because having more than one��valid pathname referring to the same file has become difficult and��restrictve to support.

.cp 10
******
     Blockmap is not written out when closing files of zero length.

.cp 10
******
     Rather indirect access to the internal wildcard comparison��routine has been made available so that a full regular expression��handler could be added. Rather experimantal.

.cp 10
******
     The last 4 words of a directory entry are used to identify��where on the device the first four blocks of a file are located. This��speeds up access to small (<= 4k) files by removing the need to read��the blockmap block. The blockmap block is always created and valid,��whatever the file size.

     The FFB information is used only if:

     The file is less than 4097 bytes long,
     and bit 7 of its status bits (FS_HASFFB) is set
     and the 'magic' field of the directory entry is equal to $d742
     and FFB usage has not been disabled via OSCONTORL().

Some of this complexity is debugging stuff, some is due to the need��to be able to take a V4.2 disk, modify it under V4.1 and then reread��it under V4.2. The 'magic' field in the directory entry will change��with each ROM release from now on, so it is possible to tell what��version of the OS a file was created under.

.cp 10
******
     SETVSVEC(1, NN, 0) returns a pointer to a data structure which��repretents the state of vertical sync vector number NN.

.cp 10
******
     Doubled the maximum permissible length of lines within .SHELL��files.

.cp 10
******
     THe ABORTSTAT() system call is still supported, but is not used��within the ROMS and use of it is not encouraged. Use signals instead.

.cp 10
******
     There was a bug in V4.1 in which searches for a hunk of free��blocks on a disk would not wrap around to the start of the disk if��the disk was not a multiple of 8 blocks in size. Now the disk size is��internally rounded down, and 'blockdev' will not permit the creation��of a volume which is not a multiple of 8 blocks.

     Unfortunately this change may result in blocks being lost from��any files which occupy the last few blocks of a V4.1 volume.

.cp 10
******
     Added some shadow registers for the keyboard state. These are��WORD addresses, so read and write 16 bits at a time. These are��defined in 'storedef.h'

     Name      Word address             Usage

   ak_ctrl        $310            If non-zero, control key currently������������������������������������depressed

   ak_shift       $312            Bit 0: Left shift currently������������������������������������depressed. Bit 1: Right shift������������������������������������currently depressed.

   ak_alt         $314            Alt is depressed

   ak_capslock    $316            Capslock currently active

   ak_numlock     $318            Keypad is curently in numeric mode


.cp 10
******
     The internal low-level video and keyboard initialisation��routines are accessible via OSCONTROL().

.cp 10
******
     The status bits of a file are now correctly preserved if it is��modified. The UID is changed to that of the modifier.

     The CREAT() system call does not preseve any of the status��bits, etc of a file is it previously existed.


.cp 10
******
     The SET_KVEC system call has been expanded:

     SET_KVEC(0):   Install default scan code handler vector.
     SET_KVEC(1):   Set keyboard raw mode.
     SET_KVEC(2):   Clear keyboard raw mode, return previous raw����������������������mode state.
     SET_KVEC(3):   Read current raw mode state.

If the keyboard driver is in raw mode, all scan code interpretation��is disabled, so calls to GETCHAR(), or READ(), etc will return raw��keyboard scan codes. Of course this keyboard mode will get rather��tangled up under windowing systems such as MGR and VCON, where other��processes expect the keyboard to emit ASCII codes.

One thing that should be done before setting the keyboard into raw��mode is to set the CON: character device driver into raw mode also.��This disables input character interpretation, so if the user strikes��a key whose scan code corresponds to the current xon character, xoff��character, eof character of reset character, embarrassing things will��not happen.

.cp 10
******
     The serial I/O driver has been extensively worked, partly due��to changes associated with character devices in general. The driver��supports up to 4 SCCs, so a bus expansion card with 3 SCCs should be��easy to accommodate.

     There is a device associated with each SCC channel, described��in 'scc.h' (struct scc). The PROG_SIO system call gives access to��these structures. PROG_SIO(chan, 1) returns the address of the scc��structure for SCC channel 'chan', where 'chan' is 0 for SA: and 1 for��SB:.

     Many new things can be done to the SCC and its associated��buffers via the CDMISC() system call. See the character device��documentation for details.

     Note that detection of a break condition on the SCC is only��possible when the receiver is enabled. ie, DCD is asserted or DCD is��being ignored by having the SCC ignoring hardware flow control.

     The OS's idea of the SCC's main clock frequency can be adjusted��via OSCONTROL, for applications where the SCC is clocked at��frequencies other than 3,750,000 Hz.

.cp 10
******
     For diagnosis of absolutely disastrous crashes, the system now��dumps its exception state into memory at address $8000 at the start��of the exception handler (unless process kill on exception is��enabled). The dump formet is as follows:

     $8000 - $803f  Registers d0-d7/a0-a7
     $8040          Access address
     $8044          Function code
     $8048          Instruction register
     $804c          Status register
     $8050          3Program counter
     $8054          Pointer to a string describing the exception type

.cp 10
******
     Option 3 (exception mode) has been expanded:

     Bit 0: Stack backtrace enable
     Bit 1: If set, kill process, rather than rebooting
     Bit 2: Halt until reset on exception
     Bit 3: If Bit 2 is set, ensure that a level 0 reset will occur��������������when reset is hit.

So setting option 3 to $0c or $0d will ensure that the FIRST��exception source will be recorded, after which the system stops.��Resetting will then cause a level 0 reset. Useful for debugging��faulty memory resident driver code.

.cp 10
******
     Worked the NEW_CBUF system call, mainly so that it can itself��keep track of and allocate the character device buffers.

     NEW_CBUF(dev, 0, x)
          Set the buffer back to the default internal one within the������������$400-$3c00 memory space.
     NEW_CBUF(dev, 1, len)
          Set buffer size to 'len' bytes, allocated as mode 1������������memory. This will be freed by the system when the buffer������������size is again altered.
     NEW_CBUF(dev, -1, x)
          Return a pointer to the circular buffer structure������������associated with device 'dev'. Described in 'storedef.h'.
     NEW_CBUF(dev, addr, len)
          Set the buffer to the memory at 'addr', length 'len'.

.cp 10
******
     Standard input for EDIT and DrDoc now reverts to 'TTY:' if an��end-of-file is detected. This can happen if the editor is getting its��standard input from a file.

.cp 10
******
     SIGPIPE (signal 13) has been implemented. When a process��attempts to write to a pipe and there is no process available to��reead from the pipe's output the writing process is sent a SIGPIPE��signal.

.cp 10
******
     The options have taken a bit of a mauling, particularly those��which used to pertain to special keyboard characters. Much of their��functionality has been moved from the keyboard driver to the��character device drivers, so some of the modes to the OPTION system��call operate upon the user's current 'TTY:' device, not simply 'CON:'.

.cp 10
******
     Beep volume is installable via OSCONTROL(). You may install a��vector to your own beep code. See OSCONTROL documentation for details.

.cp 10
******
     Standard error can now be piped at the command line level with��the '^' character. There is no way to pipe both the standard output��and standard error of a command.

.cp 10
******
     GETMEM(11, 0) allocates a new pid for SCHEDPREP. This was done��so that the memory manager is entirely accessible through the system��call interface and hance can be replaced.

.cp 10
******
     System call 134 is reserved for Jeremy Fitzhardinge's symbolic��link code.

.cp 10
******
     The size of the RAM LRU directory entry cache is configurable��in the MRDRIVERS file. The default is 20 entries. Varying this will��probably have little effect.

.cp 10
******
     Behaviour when the memory allocator detects an error is now��configurable on a per-process level, as well as globally. See the��OSCONTROL documentation for details.

.cp 10
******
     The serial driver is more efficient when it is in raw mode, and��when hardware flow control is disabled.

.cp 10
******
     Fixed the ALT-^C while ALT-S underline cursor problem.

.cp 10
******
     The TERMA and TERMB commands run as separate receive and��transmit processes so they are not so CPU hungry.

.cp 10
******
     Added a mechanism by which programs can install new commands��into AEXECA. These can actually alter the argv vector before AEXECA��uses it, so aliasing and argument substitution may be performed. The��installed vector can take over the command entirely, so programs can��dynamically replace or act as a front end to inbuilt commands, MRDs��or disk-based commands.

.cp 10
******
     Fixed a freemem problem with wildcard expansion.

.cp 10
******
     The disk boot code now uses switch 2 on warm boots as well as��on level 0 resets.

.cp 10
******
     Pressing the shift key disables caps lock, if it is set.

.cp 10
******
     Added the << function to .SHELL files.

     When the shell file interpreter encounters a line such as

          command args ...                  <<EOFMARKER
          <stuff>
          <more stuff>
          EOFMARKER

     it asynchronously executes the command with its standard input��attached to a pipe. The data up to the line EOFMARKER is fed down the��command's  standard input, after which normal interpretation of the��shell file continues.

     To initiate this mechanism the openeing marker must be all��upper case characters in the ranga 'A' to 'Z' only. There must be no��trailing white space and no space between the '<<' and the opening��marker.

     The closing marker must be identical to the openeing marker and��on a line of its own. If it has an '&' character added to it the��shell interpreter will not wait for the command to terminate before��proceeding on with the sell file from the closing marker.

.cp 10
******
     There is now a shadow register set for the 6845 video��controler, and an associated system call.

crtc_init(mode, ptr)
d0:            140
d1:  mode      Programming mode
d2:  ptr       Pointer to 14 byte memory buffer
Return:        None

     mode = 0:
          Moves the 14 bytes pointed to by 'ptr' into the CRCT registers 0 to 13.
     mode = 1:
          Moves the OS image of the registers to memory pointed to bt 'ptr'.
     mode = 2:
          Restores the CRTC and its OS image to the default settings.

.cp 10
******
     GETFMEM() now fails if the requested address is not on a��32-byte boundary. The call used to massage the adress to a suitable��value but this led to some problems.

.cp 10
******
     Benefitting from multi-char I/O, the video driver is now up to��10 times faster. Best performance is for white characters on a black��background with no attributes set.

.cp 10
******
     The speed of the 'type' and 'cio' internal commands has��benefitted from some buffering.

.cp 10
******
     It has always been the case that attempting to set a process's��time slice to -1 with PROCCNTL(10, PID, TIMESLICE) returns the��process's current timeslice without changing it.

.cp 10
******
     A process should not use the CWD() system call upon itself. Use��CHDIR() instead.

.cp 10
******
     Changed the format of the PS command's output. Includes the��process timeslice and all its arguments. A new status bit 'H' is��added in the listing. This appears if the process is currently��suspended due to receipt of a SIGSTOP signal.

.cp 10
******
     Added a new system call:

chkperm(pdirent, mask, fullpath)
d0:  141
d1:  pdirent:  Pointer to directory entry
d2:  mask:     Access mode
a0:  fullpath: Name of file being accessed
Return:        BEC_NOPERM or 0

     This system call checks that the current user is permitted to��access the file/directory described by 'fullpath'. A copy of the��file's directory entry is pointed to by 'pdirent'. This routine has��been made a system call so that more extensive permission checking��may be performed, based upon the full pathname.

     The normal pathname checking code is as follows:

_chkperm(pdirent, mask, fullpath)
struct dir_entry *pdirent;
ushort mask;
char *fullpath;
{
     int ouruid;

     ouruid = GETUID();
     if ((mask & pdirent->statbits) && ouruid && (pdirent->uid != ouruid))
          return BEC_NOPERM;
     return 0;
}



.eject
.header "The OSCONTROL system call"

                      The OSCONTROL system call

     This system call has been greatly expanded to give access to a��wide range of miscellaneous operating system internals.

     As many of the old modes have been altered it is here��redocumented from scratch.

     As many of the modes to this system call have unrelated��functions the different modes each have their own name and macro��definition in 'syscalls.h' and 'syscalls.mac'.


.cp 10
oscontrol(mode, arg, arg2, arg3)
d0:  125
d1:  mode:          0 to 36
d2:  arg:           Varies
a0:  arg2:          Varies, not used in most modes
a1:  arg3:          Varies, not used in most modes

d1 = mode = 0
SETIBCVEC(vec)
d2:  vec: Pointer to command disable vector.

     Changes the inbuilt command disable vector for the current��process's home shell process to that pointed to by 'vec'. The vector��is a string of bytes, each one of which corresponds to an inbuilt��command. The vector remains in force until it is either changed or��removed or the last process attached to this process's home shell��exits. See the 'Shell process' documentation for more details.

     If 'vec' is zero the inbuilt disable command for the current��process's home shell is removed. All inbuilt commands are enabled.

     When a process sets its home shell's disable vector, commands��are disabled for all processes which share that home shell process.��If one of those processes invoke a new shell type process, that��process inherits a copy of its parent's command disable vector, so��even if the original shell process exits the new disable vector��remains in force.

     The bytes in the vector can have values of 0, 1 or 2, where 0��means the corresponding command is enabled, 1 disables it and 2��disables it if the current user ID (UID) is not zero. Mode 2 is��probably the most useful: if a user executes a program which changes��its UID to zero that program can then execute any inbuilt commands,��which is presumably one reason why it changed its user ID.

     The correspondence between byte positions in the vector and the��inbuilt commands is listed below. This list is in order of the��evolution of 1616/OS:

   Command   Offset

     mdb       0
     mdw       1
     mdl       2
     mrdb      3
     mrdw      4
     mrdl      5
     mwb       6
     mww       7
     mwl       8
     mwa       9
     mfb       10
     mfw       11
     mfl       12
     mfa       13
     mmove     14
     mcmp      15
     msearch   16
     base      17
     expr      18
     go        19
     srec      20
     help      21
     move      22
     mwaz      23
     cio       24
     terma     25
     termb     26
     serial    27
     fkey      28
     setdate   29
     date      30
     copy      31
     syscall   32
     delete    33
     cat       34
     rename    35
     dir       36
     msave     37
     mload     38
     tsave     39
     tload     40
     dirs      41
     itload    42
     cd        43
     echo      44
     mkdir     45
     edit      46
     <unused>  47
     quit      48
     tarchive  49
     pause     50
     tverify   51
     ascii     52
     time      53
     xpath     54
     option    55
     volumes   56
     touch     57
     filemode  58
     type      59
     <ssasm>   60        * May not be present
     assign    61
     ps        62
     kill      63
     wait      64
     set       65


d1 = mode = 1
OBRAMSTART()
Return:   d0:       Address of on-board RAM start

     Returns the address of the start of the 1616's on-board RAM.��This is zero if the 1616 does not have a memory expansion board,��$100000 if is has one megabyte of expansion memory, $200000 for two��megabytes, etc.


d1 = mode = 2
Unused.


d1 = mode = 3
CURIBCVEC()
Return:   d0:       Current inbuilt command disable vector

     Returns a pointer to the command disable vector for the current��process's home shell process. Returns zero if there is no disable��vector in force.


d1 = mode = 4
TRASHPWRUPWORD()
Return:        None

     Invalidates the 1616/OS power-up words so that next time a��WARMBOOT() system call is executed (by ALT-^R, pressing reset,��rebooting on bus error, etc) the system will undergo a level zero��reset.


d1 = mode = 5
XPNAME(arg)
d2:       arg:      xpath index
Return:   d0:       Pointer to xpath directory name

     Returns a pointer to a null-terminated string which is one of��the execution search paths, as appears when the 'xpath' command is��executed. If there is no search path directory corresponding with��'arg' then the return value is -1. If 'arg' is invalid the return��value is negative. If the return value is <= 0 then it is not a valid��xpath directory pathname.


d1 = mode = 6
GETASSNAME(arg)
d2:       arg:      Assign index
Return:   d0:       Pointer to assignment string

     This call provides readback of the current assignments, as set��by the 'assign' inbuilt command. 'arg' is an index into the assign��table. If bit 31 of 'arg' is clear the return value is a pointer to��the 'old' part of the assignment. If bit 31 is set, the return is a��pointer to the 'new' part, where the order is

     assign /new /old

     If bit 30 of 'arg' is set the return value is the User ID of��the assignment: the UID of the process which originally made the��assignment.

     The return value is zero or negative if there is no assignment��corresponding to 'arg'.


d1 = mode = 7
SETUMASK(arg)
d2:       arg:      New UMASK
Return:   d0:       Old UMASK

     Sets the file creation mask for the current process. The mask��is used in setting the mode bits in any files/directories which are��created. A process's umask is passed on to any child processes when��they are created. The UMASK bits are defined in 'files.h'. The��default UMASK is (FS_NOEXEC | FS_NOREAD | FS_NOWRITE), which means��any new files cannot be executed, read or written by other users. Do��not set the LOCKED bit in the UMASK, or files cannot be modified��after they have been created, and commands like 'copy' fail when they��try to alter the status bits and date of the destination file after��copying the data.


d1 = mode = 8
READUMASK()
Return:   d0:       Current UMASK

     Returns the file current file creation mask for the current��process. 

d1 = mode = 9
SETUID(arg)
d2:       arg:      New UID
Return:   None

     Sets this process's User ID to 'arg'. The UID is inherited by��child processes when they are created.


d1 = mode = 10
READUID()
Return:   d0:       Current UID

     Returns the current User ID for the calling process.


d1 = mode = 11
GET_BDEV(path)
d2:       path:     Pointer to null-terminated pathname
Return:   d0:       Block device driver number.

     This call is passed a pointer to a file/directory pathname,��which need not be a full pathname. It returns the block device number��of the block device upon which that file/directory resides. Can��return a negative error code if 'path' is invalid.

d1 = mode = 12
DUMPLASTLINES(arg)
d2:       arg:      Character device driver number or -1 for all
Return:   d0:       None

     Removes all the last line recall buffer contents for the��character device driver whose driver number is 'arg'. If 'arg' is -1��then the last lines are removed for every character device driver in��the system. This is done by the memory manager on a GETMEM() failure,��in an attempt to defragment memory.


d1 = mode = 13
SETWILDCOMP(vec)
d2:       vec:      Pointer to new wildcard comparison function
Return:   None

     This call permits displacement of the internal 1616/OS routine��which performs wildcard matching in the SLICEARGS() system call,��which is used for command line expansion. Designed to permit the��installation of more sophisticated regular expression matching code,��the requirements of the installed function are not documented at this��stage.

     If 'vec' is zero, the default internal comparison function is��installed.


d1 = mode = 14
READWILDCOMP()
Return:   d0:       Address of current wildcard comparision function

     Returns a pointer to the current wildcard matching function,��which may be in the ROMs or external.


d1 = mode = 15
Unused


d1 = mode = 16
Unused


d1 = mode = 17
VIDEO_INIT(level)
d2:       level:         Level of initialisation
Return:   None

     Reinitialises the video driver. The argument corresponds to the��desired level of initialisation. If 'level' is zero, the��initialisation is performed as if a level zero reset was happening,��etc.


d1 = mode = 18
KB_INIT(level)
d2:       level:         Level of initialisation
Return:   None

     The same as the above, for the keyboard driver.


d1 = mode = 19
SETBEEPVOL(vol)
d2:       vol:           New beep volume
Return:   d0:            vol

     Sets the volume level for beeps. The speaker output is scaled��by the passed value. The default volume is 50. Do not set this��greater than 127.


d1 = mode = 20
READBEEPVOL()
Return:   d0:            Current beep volume

     Returns the current beep volume setting. Programs which emit��souinds from the loudspeaker should first read this setting and use��it to scale the output levels, to comply with the user's  desired��loudness level.

d1 = mode = 21
SETBEEPVEC(vec)
d2:       vec:           Pointer to beep code
Return:   d0:            vec

     Installs a new function which is called when the system is��ringing the beep (printing a control-G to the CON: driver).

     If 'vec' is zero, the default internal beep vector is installed.

     The code pointed to by 'vec' is called with two arguments on��the stack:

     4(sp):         Non-zero if a different beep sound is
               desired (Caused by the ESC-b sequence)
     8(sp):         Length. Either 1000 or 2000. Ignore this.

     Any new beep code can call the FREETONE() system call, but��cannot do any sophisticated system calls such as character/file I/O,��memory manager calls, etc. The function should preserve all registers.


d1 = mode = 22
READBEEPVEC()
Return:   d0:            Current beep vector

     Returns a pointer to the current beep vector function.


d1 = mode = 23
NOUSEFFBS(arg)
d1:       arg:           Mode
Return:   d0:            Current mode

     If 'arg' = 1, the system will not use the 'first four block'��(FFB) information contained in a file's directory entry. If arg = 0,��the FFB information is used to remove the need to read the blockmap��block of files which are less than 4k long. If 'arg' is zero, the FFB��information is used.

     This system call is hopefully unnecessary, but can be used to��prevent any possible confusion betweem files written under different��operating system versions.


d1 = mode = 24
READFFBS()
Return:   d0:            Mode

     Returns current FFB disable flag. Normally zero.


d1 = mode = 25
SETMEMFAULT(mode)
d2:       mode:          1 or 0

     Can be used to suppress the sending of a SIGSEGV signal to the��current process on a GETMEM() failure. If 'mode' is zero, signalling��is enabled (default).


d1 = mode = 26
RXTXPTR(n)
d2:       n:             Selector
Return:   d0:            ISR vector

     Returns a pointer to the operaing system's internal SCC��interrupt receive (n = 1) or transmit (n = 0) functions. This is��designed to permit full use of the operating system's serial driver��facilities by a driver for any additional SCCs attached to the 1616��bus.


d1 = mode = 27
SETBDLOCKIN(mask)
d2:       mask:          Lockin enable mask
Return:   d0:            mask

     'mask' is a bitmask which is used in the low-level disk drivers��just before a call is made to the physical device driver to perform��I/O. If the bit corresponding the the block device is set in the��mask, the system does a LOCKIN(1) system call before doing the I/O.��This can only be done if the device driver does no SLEEP() or��SNOOZE() system calls. Basically designed for improving the��performance of the /S0-/S3 SCSI drivers under heavy process loads.��Bit 0 of 'mask' corresponds to /RD, etc.


d1 = mode = 28
READBDLOCKIN()
Return:   d0:            Lockin enable mask

     Returns current BDLOCKIN mask.


d1 = mode = 29
STARTOFDAY()
Return:   d0:            True if start of day

     Returns true if the last level 0 reset was caused by powering��the system on, rather than a syscall(101).


d1 = mode = 30
PFASTCHAR()
Return:   d0:            Pointer to fastchar function

     Returns a pointer to a very low level internal operating system��function called 'fastchar' which draws characters on the screen.��'fastchar' is called with two arguments which must be pushed onto the��stack. One is a pointer to a string of characters, the second is a��byte count.

     From assembler:

     move.l    #125,d0
     move.l    #30,d1
     trap #7                  ; get pointer to fastchar function
     move.l    d0,a0
     move.l    #count,-(sp)
     move.l    #string,-(sp)
     jsr       (a0)
     add.l     #8,sp

     From C:

     int (*fastchar)() = (int (*)())OSCONTROL(30, 0);
     fastchar("string", 6);

     This function is fast.

     fastchar returns if it encounters a control character in the��string (ASCII value less than $20). It returns the number of��characters actually printed in d0. Characters are drawn starting from��the current cursor position, offset by the current window start.��Characters are masked by the current foreground and background��colours before being written into video memory.

     fastchar will blow up if it is asked to print beyond the last��column of the display.

d1 = mode = 31
SETBRCLOCK(n)
d1:       n:             New baud rate multiplier
Return:   d0:            Baud rate multiplier

     Sets a new multiplier for the SCC programming baud rate clock.��The default for this is 3,750,000, which is the rate of the clock��signal going into the SCC. If this clck signal frequency is altered,��this system call can be used to ensure that the SCC prgramming��functions still function correctly.

     If 'n' is zero, the mutiplier is unaltered and the current��value is returned.


d1 = mode = 32
TIMER1USED()
Return:   d0:            True is timer1 is in use

     Returns true if a process is currently using the VIA timer1��interrupt source in the freetone() system call.


d1 = mode = 33
TRASHASSIGN(uid)
d2:       uid:           User ID
Return:   None

     Removes all ASSIGNs made by the user whose UID is 'uid'.

d1 = mode = 34
TRASHENVSTRINGS(pid)
d2:       pid:           Process ID or -1
Return:   None

     Removes all the environment strings for the shell process which��a is the home shell for the process whose PID is 'pid'. If 'pid' is��equal to -1 then every environment string in the system is removed.


d1 = mode = 35
ENVSUB(in, dollaronly, memmode)
d2:       in:            Pointer to null terminated allocated string
a0:       dollaronly:    Flag
a1:       memmode:       Memory allocation mode
Return:   d0:            New string

     This system call gives access to the internal environment��string substitution code. It preforms substitutions on the passed��string pointed to by 'in', returning a string, space for which is��allocated via the GETMEM() system call. The memory allocation mode��for the string is specified by 'memmode', which should be 0 or 1.

     Substitutions are performed as described in the documentation��for the 'set' inbuilt command. If 'dollaronly' is non-zero then only��the '$' type substitutions are performed. Substitution is also��controlled by the relevant bits in the current process's home shell's��environment mode bits field.


d1 = mode = 36
DOASSIGN(argc, argv)
d2:       argc:          Argument count
a0:       argv:          Pointer to list of pointers to strings
Return:   d0:            Error code or zero

     Low-level access to the ASSIGN command. 'argv' points to a list��of pointers to null-terminated strings, the syntax of which is the��same as that required by the ASSIGN inbuilt command. 'argc' is a��count of the number of entries in 'argv'.


.eject
.header "Modes of the OPEN system call"

                Modes of the 1616/OS OPEN system call

     There has been some confusion over the modes to the OPEN system��call, so here is a little note to clear things up:

MODE = 1: O_RDONLY
     The file is opened read only.  The file pointer is positioned��at the start of the file.


MODE = 2: O_APPEND
     The file is opened for read/write and the file pointer is��placed at the end of the file.  Note that this is also defined to��O_WRONLY, which is very obsolete and should not be used: files which��are opened for writing are always readable also.


MODE = 3: O_RDWR:
     The file is opened for read/write and the file pointer is��placed at the start of the file.

MODE = 4: O_TRUNCATE:
     The file's size is reduced to zero and it is opened in��read/write mode with the file pointer at the start (which is also the��end!).  This mode was added in 1616/OS V4.2 because CREAT() needs to��use it in the case where the file being CREATed already exists.  This��makes CREATs more efficient and simplifies preservation of the file's��attribute bits and permissions.



.eject
.header "New modes of the PROCCNTL system call"

                New modes to the proccntl system call
                            Andrew Morton
                            6 April 1990


     This system call has had many new modes added.


.cp 10
csvec(vec)          Install context switch vector
d0:  129
d1:  18
d2:  vec:           Pointer to context switch catcher
Return:             Old context switch vector

     Installs a vector to a user-written piece of code which is��called every time the operating system deschedules or reschedules a��process. Returns the old vector value. Pass zero to deinstall.

     The user code is called with the following arguments:

     4(sp)     PID being scheduled/descheduled
     8(sp)     Pointer to process table entry for process
     12(sp)    Current system tick count, as returned by GET_TICKS()
     16(sp)    0: PID is being scheduled
               1: PID is being descheduled

     The user code can perform extended process accounting, etc. It ��should preserve all registers.


.cp 10
getpcurpid()             Return pointer to current PID
d0:  129
d1:  19
Return:                  Pointer to longword

     Returns a pointer to the location in operating system memory��where the current process ID is always kept.


.cp 10
readsigvec(pid)          Return signal handler vector
d0:  129
d1:  20
d2:  pid:                Process ID or pointer to process name
Return:                  Process's signal handler vector

     Reads the signal handler vector from the indicated process's��process table entry. This is zero if the process has no signal��handler. Returns negative error code if 'pid' is invalid.


.cp 10
fsbptr()                 Return pointer to file system busy flag
d0:  129
d1:  21
Return:                  Pointer to longword

     Returns a pointer to a longword which, if non-zero, indicates��that a process is using the file system.


.cp 10
fspptr()                 Return pointer to file system owner PID
d0:  129
d1:  22
Return:                  Pointer to longword

     Returns a pointer to a longword which holds the PID of the��process which is currently using the file system. This PID is only��valid when the file system busy flag is true.


.cp 10
ssptr()                  Return pointer to system safety semaphore
d0:  129
d1:  23
Return:                  Pointer to longword

     Returns a pointer to the longword which the system uses to lock��processes in and out (see the lockin() call). This has been  made��available so that programs can directly alter the semaphore:��incrementing it is equivalent to performing a lockin(1). Decrementing��it is equivalent to a lockin(0).


.cp 10
killuser(homeshell)      Terminate all processes attached to a shell
d0:  129
d1:  24
d2:  homeshell:          Home shell PID
Return:                  Number of processes killed or negative error code

     'homeshell' identifies a shell-type process. All processes��whose home shell process is identified by 'homeshell' are��uncondiionally terminated. The process identified by 'homeshell' is��not terminated.


.cp 10
sigblock(pid, mode)      Block/unblock signals
d0:  129
d1:  25
d2:  pid                 Process identifier
a0:  mode                1: Block, 0: Unblock
Return:                  0 or negative error code

     This call is used to prevent signals from being sent to the��identified process.


alarm(ticks)             Set an alarm
d0:  129
d1:  26
d2:  ticks               Ticks until alarm
Return:                  Time until next alarm or negative error code

     The alarm() system call permits a process to request that a��signal (SIGALRM: 14) be sent to it after 'ticks' 50 Hz system ticks��have alapsed. If 'ticks' is zero, any pending alarm is removed. If��'ticks' is equal to -1 the number of ticks until the next alarm is��returned. The return value is the number of ticks until this process��is to be sent a SIGALRM; this value is equal to $80000000 if there is��no alarm pending.


.cp 10
sigblocker(rootpid, sig, arg)
d0:  129
d1:  27
d2:  rootpid             Start PID
a0:  sig                 Signal to send
a1:  arg                 Argument to pass

     This system call sends the specified signal and argument to the��process which is blocking the process identified by 'rootpid'.  The��operating system descends the process table starting from 'rootpid'��until it locates the process which is blocking all its parents up to��'rootpid' and signals it.  This system call is used in the processing��of the ALT-^C interrupt.


.cp 10
snooze(vec, arg1, arg2)  Sleep until (vec) returns true
d0:  129
d1:  28
d2:  vec                 Pointer to snooze function
a0:  arg1                Argument passed to (vec)
a0:  arg2                Argument passed to (vec)
Return:                  0, 1 or negative error code

     The snooze function is designed so that a process can go to��sleep until some condition which is defined by the process becomes��true. The operating system scheduler calls the user-written function��which is pointed to by 'vec' within the scheduling loop and, when it��rturns non-zero, the process is rescheduled (the call to snooze��returns).

     The arguments to snooze() are passed on to the user code when��the system polls it.

     A snooze call terminates when either the 'vec' function returns��true to the scheduler, or when the process is signalled. The��process's signal handler is called before the snooze call returns.��snooze() returns 0 if it was broken due to the 'vec' code returning��true, or 1 if the snooze was broken due to receipt of a signal.

     The user code pointed to by 'vec' receives the following��arguments when the scheduler polls it:

     4(sp)     arg1, as passed to snooze()
     8(sp)     arg2, as passed to snooze()
     12(sp)    Current system tick count, as returned by GET_TICKS()

     The user code should perform whatever tests are needed and��return 0 (keep snoozing) or 1 (break the snooze). It should preserve��all registers and not perform any sophisticated system calls, such as��file system I/O, character I/O, memory manager calls, etc: It is��called with an indeterminate PID, UID, memory manager state, etc.

     An example: A piece of code (in C) which waits for an I/O port��to become true.

#include <syscalls.h>

waitforbit(ptr, mask)
unsigned char *ptr;           /* Pointer to port */
unsigned char mask;           /* Bit to wait for */
{
     int testfunc();
     while (SNOOZE(testfunc, ptr, mask))
          ;    /* Loop until broken due to bit setting, not due to signal */
}

/* The scheduler calls this function */
int testfunc(ptr, mask, ticks)
unsigned char *ptr, mask;
unsigned long ticks;
{
     if (*ptr & mask)
          return 1;
     return 0;
}


.cp 10
siguser(pid, sig, arg)        Send a signal to processes
d0:  129
d1:  29
d2:  pid                      PID of shell process
a0:  sig                      Signal to send
a1:  arg                      Argument to signal
Return:                       Number of processes signalled, or error code

     This system call sends the specified signal, with the specified��argument to all processes whose home shell process is identified by��'pid'. The home shell process itself is not signalled.


.cp 10
findhomeshell(pid)            Find  process's home shell PID
d0:  129
d1:  30
d2:  pid                      Process whose home shell PID is to be found
Return:                       Home shell PID or error code

     Locates the PID of the specified process's home shell process.��If 'pid' refers to a shell typer process then its home shell process��PID is returned, not its own PID.


.cp 10
setshnice(pid, nice)          Set nice level of processes
d0:  129
d1:  31
d2:  pid                      Shell type proces PID
a0:  nice                     Time slice value
Return:                       0 or error code

     Sets the time slice period (the number of ticks allocated per��scheduling period) of every process whose home shell PID is that��identified by 'pid'. 'pid' must refer to a shell type process. The��nice level of the shell type process identified by 'pid' is not��altered.


.cp 10
lastchild(pid)                Return last child PID
d0:  129
d1:  32
d2:  pid                      Process ID
Return:                       Last child PID

     Returns the PID of the last process which was asynchronously��started by the process identified by 'pid'. This can be useful in��writing reentrant code, where a single function is scheduled by the��same piece of code more than one time. Returns zero if no child��process is available.


.cp 10
swpptr()                      Return pointer to switch pending flag
d0:  129
d1:  33
Return:                       Pointer to longword

     Returns a pointer to an operating system variable which, if��set, indicates that the currently running process is to be��deschedueled as soon as it leaves a locked in state.


.cp 10
killdown(startpid)            Kill a group of processes
d0:  129
d1:  34
d2:  startpid                 Pid to start search from
Return:                       0 or error code

     Searches down the process/child list from the specified��process, unconditionally terminating all child processes. If one of��these is the calling process it is skipped.

     This call is performed by the 'kill -ka' inbuilt command to��kill off a process and all its blocking children and all their��blocking children.


.cp 10
sigdown(startpid, sig, arg)   Signal a group of processes
d0:  129
d1:  35
d2:  startpid                 Pid to start search from
a0:  sig                      Signal to send
a1:  arg                      Argument to pass with signal
Return:                       0 or error code

     Like killdown(), except the passed signal and argument are sent��to the processes, rather than killing them.


.cp 10
killuid(uid)                  Kill a user's processes
d0:  129
d1:  36
d2:  uid                      User ID
Return:                       0 or error code

     Unconditionally terminates all processes whose user ID is��identified by 'uid'. If the current (calling) process is identified��in this group of processes it is killed after all the other processes.


.cp 10
siguid(uid, sig, arg)         Signal a user's processes
d0:  129
d1:  37
d2:  uid                      User ID
a0:  sig                      Signal to send
a1:  arg                      Argument to send with signal
Return:                       0 or error code

     Sends the specified signal, with the specified argument to all��processes owned by the user identified by 'uid'. If one of these��processes is the current (calling) process in this group of processes��it is signalled after all the other processes have been.


.cp 10
setsigexit(pid, mode)         Signal process on exits
d0:  129
d1:  38
d2:  pid                      Process to signal
a0:  mode                     1: enable; 0: disable
Return:                       0 or error code

     If enabled, this call results in the identified process being ��signalled whenever ANY other process exits. The identified process's��signal handler is passed a SIGEXIT (22) and the exitting process's��PID.

     If 'mode' is zero it disables this sending of SIGEXIT.


.cp 10
setpg(pg)                     Set process group
d0:  129
d1:  39
d2:  pg                       New process group identifier
Return:                       New process group

     A process group is a number which identifies a group of��processes and may be used to collectively identify a group of��processes for the purposes of killing them or signalling them.��Normally all processes run with a process group ID of 2. This call��allows a process to modify its process group number. All processes��inherit their parent's process group number when they are created.

     If 'pg' is 1, the current process's group is set to a new��value, which is initially 3, and then 4 on the next call to setpg(1),��etc.

     If 'pg' is zero then the current process's process group number��is unaltered. A process may use this to find out its current process��group number.

     For all other valuse of 'pg' the current process's process��group number becomes 'pg'.

     The new process group number for the current process is always��returned.


.cp 10
sigpg(pg, sig, arg)           Signal processes in a process group
d0:  129
d1:  40
d2:  pg                       Process group number
a0:  sig                      Signal to send
a1:  arg                      Argument to send with signal
Return:                       0 or error code

     All processes running with the specified process group number��are signalled with the passed signal and signal argument. If the��calling process belongs to that group it is signalled after all of��the other members.


.cp 10
killpg(pg)                    Kill processes in a group
d0:  129
d1:  41
d2:  pg                       Process group number
Return:                       0 or error code

     All processes running with the specified process group number��are unconditionally terminated. If the calling process belongs to��that group it is killed after all of the other members.


.cp 10
setprocumask(pid, umask)      Set file creation mask for a process
d0:  129
d1:  42
d2:  pid:                     ID of process to alter
a0:  umask:                   New umask
Return:                       New umask

     Sets the specified process's file creation mask to 'umask'.��Returns new umask. If 'umask' is equal to -1 the current file��creation mask is returned unaltered.


.cp 10
setenvbits(pid, mask, mode)   Set/clear/read a process's environment bits
d0:  129
d1:  43
d2:  pid                      Process ID
a0:  mask                     Bit mask for environment bits
a1:  set                      1: set bits; 0: clear bits; 2: read
Return:                       New environment bits

     This system call enables manipulation of the environment bits��field attached to a shell type process. The use of these bits is��described in the shell process documentation.

     This call locates the home shell process of the pid identified��by 'pid' and modifies its environment bits. This will change the��environment of that shell process, all those processes which have it��as their home shell process as well as all new processes created by��that shell process and those processes which have it as a home shell.

     If 'set' is zero, set bits in 'mask' are used to clear bits in��the environment. ('mask' is inverted and ANDed into the environment��bits)

     If 'set' is one, 'mask' is ORed into the environment bits.

     If 'set' is two, the environment bits are unaltered. This��provides readback.

     In all modes, the new environment bits are returned. The��'setenv' program is provided for manipulation of these bits.

     Every time this call is performed the system records the��resulting environment but setting and uses it to start off the root��shell process when the system is next reset.


.cp 10
getenvbits(pid)               Read environment bits
d0:  129
d1:  44
d2:  pid                      Process identifier
Return:                       Environment bits or error code

     This call returns the environment bits for the home shell��process of the prcess identified by 'pid'.


.cp 10
nametopid(pid)                Convert string to PID
d0:  129
d1:  45
d2:  pid                      PID identifier
Return:                       PID number or error code

     This call converts its argument into a process ID. If 'pid' is��a number in the range 0 to MAXPIDS (currently 64) it is simply��returned. If 'pid' points to a null-terminated string of digits the��string is evaluated as a decimnal number and returned. If 'pid'��points to a null terminated name of a process the process table is��searched for a process with a matching name. The PID of the first one��encountered is retrurned.


.cp 10
blocktx(destpid, addr, length, sig)     Send an IPC block
d0:  129
d1:  46
d2:  destpid                  Pid to send block to
a0:  addr                     Address of block to send
a1:  length                   Length of block to send in bytes
a2:  sig                      0: Don't signal; 1: signal destpid
Return:                       0 or error code

     This call sends a block of data to the identified PID. See the��interprocess block transmission documentation for more details.


.cp 10
blockrx(mode)            Receive an interprocess block
d0:  129
d1:  47
d2:  mode                Mode of operation
Return:                  Varies

     A process uses this call for manipulation of its interprocess��block communication input queue. See the interprocess block��transmission documentation for more details.


.cp 10
setenv(pid, name, setting, mode)   Manipulate environment strings
d0:  129
d1:  48
d2:  pid                 Process ID
a0:  name                Name part of environment variable
a1:  setting             Setting part of environment variable
a2:  mode                Mode of environment string substitution.
Return:                  0, 1 or error code

     This system call permits alteration of the environment variable��strings which are attached to the home shell process of the process��identified by 'pid'. These strings are shared by the home shell and��by all processes which share that home shell.

     'name' points to a null-terminated string of any length which��identifies the environment string. 'setting' is the new string which��is associated with the 'name' string. If 'setting' is zero (a nil��pointer) then the environment string identified by 'name' is removed.

     'mode' determines how the new name and setting are to be��handled by the envsub() system call, which is used in command line��substitution. It is a combination of the following bits, defined in��'envbits.h'

ENVS_DSIGN     1    Enable substitution in '$' type usage
ENVS_ARG       2    Enable substitution in all arguments
ENVS_ARG0      4    Enable substitution in the first part of a command

     Note that the enabling of substitution is also controlled by��bits in the home shell's environment bits.

     This call returns 0 if all went well and 'name' did not��previously exist in the environment. It returns 1 if all went well��and 'name' was previously defined in the environment. A negative��error code is returned on memory allocation failure, bad PID, etc.

     See the documentation for shell type processes and the 'set'��inbuilt command for more details.


.cp 10
getenv(pid, name, mode)       Look up an environment string variable
d0:  129
d1:  49
d2:  pid                      Process identifier
a0:  name                     Pointer to 'name' string
a1:  mode                     Search mode
Return:                       0, error code or pointer to setting

     This call provides access to the environment string variables��attached to the home shell process of the process identified by 'pid'.

     'name' points to a null-terminated string which identifies an��environment variable. The string comparison is case sensitive.

     'mode' is a mask consisting of a combination of ENVS_DSIGN,��ENVS_ARG0 and ENVS_ARG1. If the result of ANDing 'mode' with the��environment variable's mode (as passed when it was installed) is��non-zero then a match is valid.

     If no match is found a value of zero is returned. If a matching��string is found and the ANDing of 'mode' and the environment��variable's mode succeeds, this call returns a pointer to the��variable's 'setting' string.

     If 'name' is less than $4000 then it is assumed to be a numeric��index into the environment varaibles. If 'name' does not exceed the��number of currently installed environment strings then a pointer to��the corresponding ENVSTRING structure (defined in 'process.h') is��returned. The 'mode' field is ignored in this mode. If 'name' exceeds��the number of installed environment strings then a nil pointer is��returned.


.eject
.header "The block IPC mechanism"

              Description of the block send and receive
        interprocess communication mechanism in 1616/OS V4.2.
                        Andrew Morton, Applix
                            13 June 1990



     The IPB block send/receive facilities allow any number of��processes to send arbitrary length blocks of data to other processes.��Data sent to a particular process is queued on behalf of that process��and made available in order of receipt.  Because the blocks are��queued at the receiver the sender will never be blocked by the block��send system call.

     The sending process may arrange for the receiver to receive a��special signal when the sent block is available for receipt.

     When a block is sent to a process the operating system��allocates memory for it and a special header, copies the sent block��into that memory, prepares for the block to be available to the��receiver and then returns.  The memory into which the block is copied��is allocated as mode 0 memory in the receiving process's address��space.  It is the receiver's responsibility to free this memory.


.cp 10
The block send system call

     The transmission of an IPC block is done via one of the modes��of the PROCCNTL system call.  The usage is:

blocktx(destpid, addr, length, sig)     Send an IPC block
d0:  129
d1:  46
d2:  destpid                  Pid to send block to
a0:  addr                     Address of block to send
a1:  length                   Length of block to send in bytes
a2:  sig                      0: Don't signal; 1: signal destpid
Return:                       0 or error code


     The arguments are used as follows:

destpid

     The PID of the process to which the block is being sent.  As��always with the PROCCNTL system call, this may be either a direct PID��number in the range 0 to 63, or a pointer to a string which is either��a decimal number in the range 0 to 63, or the name of the process.

addr

     The starting address of the block of memory (in the��transmitting process's address space) which is to be sent.

length

     The length of the block in bytes.

sig

     This argument determines whether or not the receiving process��is to be sent a signal when the block is ready for receipt.  Set��'sig' to 0 if no signal is to be sent or 1 if a signal is to be sent.��If a signal is requested, it is sent via

     SIGSEND(DESTPID, SIGBLOCKRX, GETPID())

     where SIGBLOCKRX is equal to 23 decimal.


     If this system call returns a negative error code this probably��indicates that 'destpid' is invalid or there is not enough memory to��allocate the receiver's buffer.


.cp 10
The block receive system call

     This system call is used by a process for manipulating its IPC��block receive queue.

     This system call interfaces with the linked list of incoming��data blocks which the operating system maintains on behalf of each��process.  Each element in the linked list is defined by the following��simple C data structure (defined in 'ipcblock.h'):

typedef struct ipcblock
{
    struct ipcblock *next;    /* Link to next structure */
    long whofrom;             /* The sender PID */
    long blocklen;            /* Size of data block */
    long padd6;             /* Expansion */
    unsigned char data2;    /* The data is actually placed here */
} IPCBLOCK;

     The operating system adds structures to tbe end of this linked��list as they are sent to the receiving process and removes them from��the head as they are read off.  It is possible for the receiver to��directly manipulate and inspect the list if necessary.

     Note that the above header and the data block to which it��refers are allocated in a single hunk.  The data block originally��sent by the transmitter starts at the 0'th element of the 'data'��array and overruns the structure.  The 'blocklen' field defines the��number of bytes which are present in the 'data' array. The operating��system allocates sufficient extra memory beyond that required for��this structure for storage of the data block.

     The contents and format of the 'data' area are completely��defined by the calling application: it is the application's��responsibility to impose further data structure onto the sent blocks.�� It is guaranteed that the 'data' array will be aligned on an even��address boundary.

     It is the receiving process's responsibility to free the memory��taken by this structure / data block when it is no longer needed. The��correct address to free is that at which the structure starts.  Do��not attemt to free the address of the ipcblock.data element.

     BLOCKRX() is implemented as another mode to the PROCCNTL system��call.  The usage is:

blockrx(mode)            Receive an interprocess block
d0:  129
d1:  47
d2:  mode                Mode of operation
Return:                  Varies

     Where the following modes, defined in ipcblock.h  are valid:

mode = IPCB_GETHEAD (0)

     Returns a pointer to the first 'ipcblock' structure in the��receiving process's incoming list.  This refers to the next available��block.  The list may be inspected by going down its link pointers. ��The list is terminated by a zero value in ipcblock.next.

mode = IPCB_BLOCKCOUNT (1)

     Returns the number of IPC blocks which the calling process��currently has available for reading.

mode = IPCB_BYTECOUNT (2)

     Returns the number of bytes in all the IPC blocks which are��currently queued for receipt by the calling process.  This byte count��does not include the size of the ipcblock headers.  It is simply a��summation of all the ipcblock.blocklen elements.

mode = IPCB_FETCHNEXT (3)

     If the calling process has one or more blocks available for��receipt, this call removes the block at the head of the list and��returns a pointer to it in register d0.  That is, it returns an��(IPCBLOCK *).  If no data is available for receipt this call will��immediately return a nil pointer (d0 contains 0).

     A process may use this mode to poll its incoming IPC block��queue.


mode = IPCB_FETCHWAIT (4)

     Waits until a block is available for the calling process and,��when it is available, this call removes the block from the head of��the list and returns a pointer to it, as with IPCB_FETCHNEXT.

     This mode blocks the calling process until it has an IPC block��ready for reading.

     It is intended that in future releases of 116/OS this mode of��the BLOCKRX() system call will be interruptible by signals.  That is,��if the process is blocked in the BLOCKRX() system call and receives��and catches the signal this call will return a nil pointer.  For this��reason it is important that the return value from��BLOCKRX(IPCB_FETCHWAIT) be tested for zero before use.


.eject
.header "The AEXECA system call"

                       The aexeca system call


The aexeca() system call starts a new 1616/OS process.  It is passed��two arguments:

aexeca(argv, flag)

where 'argv' is a pointer to a table of pointers to null-terminated��strings.  The last pointer in this table must have a value of zero. ��The first string is the name of the command to be executed and the��remaining strings represent the arguments to that command.

The 'flags' argument has the following usage:

Bit 0:
     If set, the new process is run asynchronously with respect to��the caller.  That is, it is run in the background.  In this case��'aexeca' will return the newly created process's PID.
     If bit 0 is clear, the new process runs synchronously, so the��caller is suspended until the new program terminates.  In this case,��the call to 'aexeca' returns the called program's exit code.

Bit 31:
     If set, the new process is a 'shell type' process, which means��that it receives a new copy of its parent shell's environment, rather��than sharing it.

The 'execa' system call is identical to 'aexeca', with a 'flags'��argument of zero.

The called program receives a copy of the 'argv' array, as well as��the 'argtype' and 'argval' arrays, which are calculated from the��'argv' array during the 'schedprep' system call.

Note that the 'aexeca' system call should ONLY be called with��pointers to strings in the 'argv' array.  Do not attempt to pass��numeric data in this array, as the operating system will interpret��the numbers as pointers to strings and this may well crash the system.

To pass the value of internal variables to a sub-program it is��necessary to convert them to string representations first.  This can��be done quite simply by using the 'sprintf' system call to format the��arguments.  The called program may read the numbers from the 'argval'��array, as the operating system performs the string to numeric��conversion for you.  Note that this is done using the leading '.'��convention for decimal numbers.

The following program demonstrates the use of 'aexeca' from within��an assembly program:

*
* Demonstrate the use of the execa system call.
* execute the commnad 'TYPE TEST.S' synchronously.
*
          move.l    #cmdname,argvbuf
          move.l    #testname,argvbuf+4 * Set up argument pointers
          clr.l     argvbuf+8           * Terminal nil pointer
          move.l    #argvbuf,d1         * Pointer to argument pointers
          move.l    #97,d0              * execa system call number
          trap #7
          tst.l     d0                  * Error?
          bmi  errorhandler
          rts                           * Return the code to the system

errorhandler:  nop
          rts

asmname:  dc.b 'TYPE',0
testname: dc.b 'TEST.S',0
argvbuf:  ds.l 10                       * Pointer array
          end


.include "/usr/lib/trailer.dh"
.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.include /home/andrew/drdoc/hpletter.dh

Changes in 1616/OS from V4.2e

20:10:43 16 May 1990

Added stdin:, stdout:, stderr: pseudo devs.

These special filenames may be used in places where a program��insists on being given a filename, but you want input/output to be��directed to stdin/stdout or stderr.

Put wr15 image in scc struct so that hupmode interrupts are��preserved across prog_sio calls.

OPEN() and CREAT() now check to see if it's a char device AFTER the��call to GETFULLPATH, so Jeremy's symlinks get a chance to convert a��linked path into a character device name.

System call 85 is henceforth defined to be an illegal system call. ��This is so that under future OS versions it is possible to tell if a��particular system call exists by comparing it's entry in the syscall��jump table with that for call 85.

The character driver's 'xoffed' flag is cleared when the driver��processes a SIGHUP, so people don't disconnect from a modem leaving��flow control off.

The system no longer disposes of all environment strings on a memory��allocation failure.

17:02:07 30 May 1990

Changed the 'Bad XREL header' error message to 'Bad magic number'��for more genericity.

The 'Block NN unreserved' message from the file system is now��turnable offable by setting bit 5 of option 15.
ie:

#include <options.h>
OPTION(15, OPTION(15|OPM_READ, 0)|OPM_DONT_REPORT_FREE_BLOCK_READS);


######Document new_cbuf new modes

The BLOCKRX system call now breaks out of a wait for an incoming��block if a signal is received and caught.  The BLOCKRX call returns 0��if this happens, rather than the address of a received block.  This��MUST be checked for.

OSCONTROL(37) returns a pointer to a 32 bit number which is the��number of ticks since boot time.  This is the number normally��returned by GET_TICKS().

00:04:05 16 Jul 1990

Put in some code to detect the presence of Philip Hutchison's 1/2��Meg memory expansion.  #######Video?  OSCONTROL(38) now returns the��amount of on-board memory (512k or 1 Mbyte).

01:43:46 28 Jul 1990

In edit, the delay before redrawing the status line is 1 second if��the ENVB_LOWBAUD bit is set in your shell's environment bits. ��Otherwise the delay is 18/50ths of a second.

01:35:01 03 Aug 1990

Put optional case sensitivity in the file system, inbuilt commands,��everywhere else.  Case sensitivity is turned on by setting bit 6 of��option 15.
ie:

#include <options.h>
OPTION(15, OPTION(15|OPT_READ)|OPM_CASESENSITIVE);

Case sensitivity breaks things and will need careful setup and��installation.  In particular it confuses 'make' and there is some��wierdness in the naming of block device drivers (/rd versus /RD in��particular).  I would appreciate some testing and reporting on this��one.

The $path environment string, which provides additional search paths��beyond the 'xpath' settings has been changed to $PATH (upper case).
eg:

set "PATH=/f0 /f0/bin"


19:16:24 18 Aug 1990

There was a bug in the file system wherein the code which allocates��contiguous blocks (used in mkdir, directory growth, file growth over��a 512k boundary) bit not correctly mark the corresponding blocks in��the volume bitmap as needing writing out.  This does not apply to��volumes < 8 Mbytes.  Is is usually benign, as the memory copy of the��bitmaps is correct and is usually written out shortly after in normal��operations.  This bug only bites when a file/directory is grown and��the system is crashed/halted very shortly after, before the memory��copy of the bitmaps is written out.


02:11:05 28 Sep 1990

     Altered the keyboard ISR so that it no longer raises interrupt��priority to SCC_SPL(), which shuts out SCC interrupts.

19:00:35 08 Nov 1990

Added/modified inbuilt terminal mode commands:

term  -l DEVICE:
terma -l       ( == term -l sa: )
termb -l       ( == term -l sb: )

The -l flag makes these commands print every char to stderr, as well��as stdout, so you can type

     terma -l }logfile

and capture a log of the terminal session.

The 'term' commands now use rawgetchar() rather than getchar(), so��the control-D end-of-file character setting is ignored:  if an EOF��char is typed it is sent through verbatim.

Added the 'vmode' inbuilt command.

vmode hfreq vfreq hoffset voffset

Used for altering the video frequency.  Attempts to correct the��time-of-day clock for altered vertical interrupt frequency.

Made the default video border colour 0000, rather than 0001, for��monitors which clamp black level after hsync.  This is overridden by��the video colour field of the MRDRIVERS file header.


00:02:44 20 Nov 1990

Noticed that sending SIGSTOP sleeps sender until receiver is out of��the file system.

Added escape codes to alter the shape of the cursor:
######

Put in an escape code to cause the video driver to pause:
     ESC 0x03 NN
Causes a delay of NN 20 msec ticks,

Put in better command line wildcarding:

*              Any sequence of zero or more characters
?              Any single character
abc          Any of 'a', 'b' or 'c'
a-k          Any of 'a', 'b', ..., 'k'
~abc         Any character not 'a', 'b' or 'c'
~a-z         Any character not in 'a', 'b', ..., 'z'
a-cxy0-2     Any character in 'a', 'b', 'c', 'x', 'y', '0', '1', '2'
~a-cxy0-2    Any character not in 'a', 'b', 'c', 'x', 'y', '0', '1', '2'

Examples:
     *.ch    All .c and .h files
     a-m*    All files starting with 'a', 'b', ..., 'm'
     *.~ch   All files not ending in .c or .h


21:38:55 11 Feb 1991

The initial PC in the ROMs (bytes 4-7) held the address of a C��function, rather than the assembly language startup functions.  That��C code did JSR's, etc and hence didn't work if the system RAM was not��operative.  A consequence of this was that the board would do nothing��(not even test #0) if the RAM was broken.

Did something to the 'assign' command.  Not too sure what now, I��think the command "ASSIGN -" was supposed to delete all assigns but��did not and that was fixed.

Altered the environment string command alias searching so that it��makes multiple attempts to expand the command.  This means that��complicated commands using nested settings work correctly.  It has��the undesirable side effect that recursive settings get expanded��wildly.  Try

set -e "echo=echo hello"
echo

and you'll get my drift.

22:42:11 13 Feb 1991

Changed the set_vap and set_vdp system calls will work for an entire��1 Mbyte of on-board RAM.

************* 4.4a release

01:16:13 26 Feb 1991

Woeful problem in the bitmap block reserving code resulted in the��last block not being reserved for MKDIR, directory growth, file��growth, etc.  Quickly fixed.

************* 4.5a release

Changed the 'move' command so that 

MOVE FOO BAR

will set BAR's load address to that of FOO, rather than preserving��BAR's original load address (if it existed).

Altered the CREAT() call so that the new load address passed to the��CREAT call overrides any existing load address in the file if it��existed.

