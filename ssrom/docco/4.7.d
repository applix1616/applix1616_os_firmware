.; DOC LM1 RM70 JRR TABS5              (DrDoc startup stuff)
.pinclude "/usr/lib/epson_fx-80+.codes"
.sinclude "/usr/lib/1616.codes"
.include  "/usr/lib/doc.dh"
.header   "1616/OS V4.7 changes"
.footer   "Page $#p"

This document describes the changes to 1616/OS V4.7a, relative to��V4.4.

************* 4.4a Released

01:16:13 26 Feb 1991
 Fixed a bug in directory block allocation which had horrid���consequences with mkdir.

************* 4.5a Released

 Changed the 'move' inbuilt command so that the file load address is���preserved across copies, so copy foo bar gives 'bar' 'foo's load���address.

11:02:20 27 Apr 1991

 Changed the scheduler so that processes which have a negative���'nice' value are only eligible for scheduling every (-timeslice)���times.  So a process with nice -4 will consume 1/4 as much CPU as a���process at nice 0.

18:52:41 28 Apr 1991
 Added ^O recall to the editor.  The ^O key cycles back through the���command line history a word at a time.

 Added ^C word-match recall feature to line editor: Type in part of���a word and hit ^C.  The line editor searches back through the���history a word at a time looking for a match.

 Added pathname completion to the editor:  type in a partial���pathname and hit TAB.  The line editor fills in the rest of the���pathname.

 Added a 'background nice' feature to each process.  This is a field���in the process table which determines what 'nice' value a process's���children will run at if they are backgrounded (run asynchronously). ���This is because background processes are usually to run at lower���priorities.  When a process starts a synchronous process (the new���one blocks the parent) the new process gets the parent's normal nice���value.  When the child is asynchronous its nice value comes from the���parent's BGNICE field.  By default process's foreground nice values���are 1 and background nice is 0, so backgrounded processes run at���half the priority of their parents.

 When a backgrounded child is scheduled both its foreground and���background nice values are the same: obtained from the parent's���background nice value.

19:05:08 27 Jan 1992

 Added XON/XOFF for input streams.  Only implemented for the serial���drivers, this option permits the drivers to send XON/XOFF sequences���to the remote transmitter in response to the receive buffer filling���or emptying.

 To turn this on from the command line, use

    serial a baudrate rxbits txbits parity
          stopbits set_sfc xonchar xoffchar

 Where 'set_sfc' is 1 to set software flow control and 0 to stop it.��� 'xoffchar' is the character to be sent to stop the remote system,���'xonchar' is the character to be sent to restart it.

 For example,

     serial a .9600 8 8 0 2 1 11 13

 enables software flow control and sets the XON/XOFF characters to���control-Q and control-S.

23:34:24 04 Feb 1992

 New debug feature added to the memory manager:  In debug mode,���getmem() will initialise all new mode 0 memory to the value���0x99999999 and all new mode 1 memory to 0xbbbbbbbb.  When memory is���freed it is set to 0xdddddddd.

 This feature is designed to trap the usage of unalocated memory,���and the usage of uninitialised memory.  Any application which uses���memory after it has been freed, or uses it before initialising it���will probably crash with the debug mode turned on, permitting the���programmer to diagnose the error.

 GETMEM(N, 12) turns on malloc debug mode
 GETMEM(0, 12) turns off malloc debug mode
 GETMEM(255, 12) reads malloc debug mode

 The value 'N' determines which debug features are used.

 If bit 0 of 'N' is set, memory is initialised to���0x99999999/0xbbbbbbbb by GETMEM().

 If bit 1 of 'N' is set, memory is initialised to���0x99999999/0xbbbbbbbb by GETFMEM().

 If bit 2 of 'N' is set, memory is initialised to���0xdddddddd by FREEMEM().

23:29:04 06 Feb 1992

 Added VI mode to the keyboard driver.  In this mode the cursor keys���generate codes appropriate to the VI editor.

     Printing the escape sequence ESC S B Turns on VI mode
     Printing the escape sequence ESC S A Turns on VI mode

The system call CDMISC(0, CDM_ALTERNATE, 1, 0, 0) turns on VI mode.
The system call CDMISC(0, CDM_ALTERNATE, 0, 0, 0) turns off VI mode.
The system call CDMISC(0, CDM_ALTERNATE, 2, 0, 0) reads VI mode.

WHere CDM_ALTERNATE is 57.

20:56:54 16 Feb 1992

 Added zmodem to the ROMs.  New inbuilt comamnds:

getz
     Similar to the 'sz' command, receives files via standard input�������and standard output.  No options are supported.

putz filenames
     Similar to the 'sz' command, sends the named files via standard�������input and standard output.  No options are supported.

geta options filenames
getb options filenames
     These commands get files from a remote host attached to serial�������port SA: or SB:.  These commands send the string

     sz1616 options filenames

     to the remote system and then wait for the remote system to�������send an 'X' back.  Once this happens, 'getz' is called and�������zmodem files are placed in the current 1616 directory.

     The 'geta/getb' command retries the 'sz1616' command five�������times.  If an 'X' does not come back the transfer is aborted. �������This has been added because some (sun) systems seem to have�������trouble with switching line disciplines when characters are�������buffered.

     All options and filenames are passed to the remote system,�������where they are interpreted.  'geta/getb' ignore all arguments. �������The '-a' argument tells the Zmodem protcol to do LF to CR-LF�������conversion.

puta options filenames
putb options filenames
     These commands send files to a remote host attached to serial�������port SA: or SB:.  These commands send the string

     rz1616 options filenames

     to the remote system and then wait for the remote system to�������send an 'X' back.  Once this happens, 'putz' is called and the�������named files are sent to the remote system with Zmodem protocol. �������Any options (as indicated by a leading '-' character) are�������ignored.  It is up to the remote 'rz1616' command to interpret�������them.

     The 'puta/putb' command retries the 'rz1616' command five�������times.  If an 'X' does not come back the transfer is aborted. �������This has been added because some (sun) systems seem to have�������trouble with switching line disciplines when characters are�������buffered.

     All options and filenames are passed to the remote system,�������where they are available for interpretation.  'puta/putb' ignore�������all arguments.  The '-a' argument tells the Zmodem protcol at�������the host to do CR-LF to LF conversion.
 
printa options filenames
printb options filenames
     These commands are basically identical to 'puta/putb' except�������that the string sent to the remote system is

     print1616 options filenames

     The remote system should send the 'X's, receive the files in�������Zmodem protocol and print them out.

boota options
bootb options
     These commands run the command

     boot1616 options

     on the remote host, wait for an 'X' to be echoed and then drop�������into a Zmodem receive.  Once the receive terminates the command�������"boot1616" is executed on the 1616.

     The intent here is that the remote host's 'boot1616' command�������upload a file to the 1616 called 'boot1616.shell' and this�������command, when executed can download more files and set the�������system and environment up for use.


Support for ROM additions.

     Ten bytes into the ROMs (at address 0x50000a) is a longword��with the value 0x00000000.  If the user wishes to add his own code to��the ROMs he should place a pointer to the code's initialisation point��at this address.  The operating system will call the entry point��three times.

The first time is after the OS has attempted to install the SSDCC��disk controller device driver. If the user has installed disk drivers��then this is the time to install them with the ADD_BDRIVER system��call.  The user's entry point is called with the C code:

     (*user_code)(reset_level, 0);

The second time is after the memory resident drivers have been��installed and have had their initialisation entry points called.  The��user's entry point is called with the C code:

     (*user_code)(reset_level, 1);

The third time the user's entry point is called is immediately��before the system drops into the root shell process.  The user's code��is called by:

     (*user_code)(reset_level, 2);


No guarantees are made about which parts of the ROM are available��for adding new code.  The user will have to determine whqat locations��are free within new versions of 1616/OS as they come along.

There are currently approximately 85 kbytes available in 1616/OS��V4.7a 1 Mbit EPROMs.


syscalls.mac in ROM

If the 1616 has no SSDCC controller card, the file 'syscalls.mac' is��written out to the RAM drive.

