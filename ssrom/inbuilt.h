/* TABS4 NONDOC
 * 68k monitor command executor
 */

#ifndef INBUILT_H
#define INBUILT_H


extern void ding(void);
extern int verbose(void);
extern void mon_init(void);
extern int move(register int argc,char *argv[], int copymode);
extern int domove(char *from,char *to,int copymode);

#endif
