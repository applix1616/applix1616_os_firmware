/* TABS4
 * File level I/O driver assist functions
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <files.h>

#include <blkerrcodes.h>
#include <options.h>


//#include <splfns.h>
#include "debug.h"
#include "chario.h"
#include "newmem.h"
#include "cachecont.h"
#include "psprintf.h"
#include "setmem.h"
#include "movstr.h"
#include "dirfns.h"
#include "fileio.h"
#include "ischdev.h"
#include "stoupper.h"
#include "blockio.h"
#include "roomfor.h"
#include "splitpath.h"
#include "fcb.h"
#include "movmem.h"
#include "bindex.h"
#include "filefns.h"



/*
 * fsbusy is true if a process is within the file system. All
 * processes wait on fsbusy when they want access to the file system
 */
int fsbusy;
int fsbpid;			/* The process which owns the file system */
int ouruid;			/* Our user ID */
int umask;			/* File creation mask */
int noffbs;			/* If true, don't use first four blocks */

char *slash = "/";

extern struct blkdriver bdrivers[];
extern int curstuffvalid;		/* Zero if new disk detected */
extern char *cur_path;			/* Full path of current directory */
extern int cur_pbdvr;			/* Block driver number of durrent path */
extern struct dir_entry cur_pdir; /* Copy of entry for current directory */
extern char *cur_dir;			/* Cached copy of current directory contents */
extern int curpid;				/* Current process ID */
extern int syssafe;				/* Lock current process in */
extern struct fcb **ppfcbs;		/* File control blocks */
extern uint maxfiles;			/* Number of file control blocks */
extern int reslevel;

extern char *index(), *splitpath();


int usefulffb(struct dir_entry *dirptr,ushort *blkmap);
int isdevid(char *path);


void file_init(void)
{
	if (reslevel == 0)
		noffbs = 0;
	umask = FS_NOREAD|FS_NOWRITE|FS_NOEXEC;
	ouruid = 0;
	ppfcbs = (struct fcb **)getzmem1(maxfiles * sizeof(*ppfcbs));
	/* Initialise the current directory stuff */
	curstuffvalid = 0;
	cur_path = (char *)getmem1(1);
	*cur_path = '\0';
	cur_dir = (char *)getmem1(1);
	cache_init();
}

/*
 * Find an unused fcb, mark it as used. Return fcb index, else
 * error if none free. Set up the pointers to the data buffer
 * and block map. Put the full pathname into the fcb.
 * Passed a full pathname.
 */
int get_fcb(char *fullpath,int ispipe)
{
	register int fcbnum;
	register struct fcb *fcbptr;

	for (fcbnum = 0; fcbnum < maxfiles; fcbnum++)
	{
		if ((fcbnum > 0x80) && (fcbnum < 0x83))
			continue; /* The magic stdin, out, err descriptors */
		fcbptr = ppfcbs[fcbnum];
		if (!fcbptr)
		{	/* An unused entry */
			fcbptr = (struct fcb *)getzmem1(sizeof(*fcbptr));
			if ((int)fcbptr < 0)
				return BEC_NOBUF;
			ppfcbs[fcbnum] = fcbptr;
			if (!ispipe)
			{
				fcbptr->blkbuf = (char *)getmem1(BLOCKSIZE);
				if ((int)fcbptr->blkbuf < 0)
					return BEC_NOBUF;
				fcbptr->file_name = room1for(fullpath);
			}
			fcbptr->owners[curpid] = 1;
			fcbptr->nusers = 1;
			return fcbnum;
		}
	}
	return BEC_NOFCBS;
}

/*
 * Free up an FCB
 */

void free_fcb(int fcbnum,int ispipe)
{
	register struct fcb *fcbptr;

	fcbptr = ppfcbs[fcbnum];
	if (!ispipe)
	{
		maybefree((int)fcbptr->blkbuf);
		maybefree((int)fcbptr->blkmap);
	}
	maybefree((int)fcbptr->file_name);
	dofreemem(fcbptr);
	ppfcbs[fcbnum] = 0;
}

/*
 * Find a free block from the device bitmap. Mark it as used, and return it.
 * Return code if device full.
 * Version 3.3a Alteration: search from last place
 */

int get_free(ushort bdvrnum)
{
	register struct blkdriver *bdptr;
	register uchar *bmptr;
	register ushort block;
	register ushort mask;
	register ushort nb8;		/* Number of blocks / 8 */
	register ushort bmoffset;
	register int retval;

	bdptr = (struct blkdriver *)&bdrivers[bdvrnum];
	nb8 = (bdptr->rootblock.nblocks + 7) / 8;
	bmoffset = bdptr->bmoffset;		/* Take up old search position */
	bmptr = (uchar *)bdptr->bitmap + bmoffset; /* Pointer into bitmap */
	for (block = 0; block < nb8; block++)
	{
		if (bmoffset >= nb8)
		{	/* Wrap around */
			bmoffset = 0;
			bmptr = (uchar *)bdptr->bitmap;
		}
		if (*bmptr != 0xff)
		{	/* Found a free entry */
			block = 0;
			for (mask = 0x80; mask; mask >>= 1, block++)
			{	/* Find the free bits in this byte */
				if (!(mask & *bmptr))
				{	/* Found a free bit/block */
					block += (bmptr - (uchar *)bdptr->bitmap) * 8;
					/*
					 * 4.1 Bug: if nblocks != multiple of 8, this fails,
					 * without going back to start of disk.
					 */
#ifdef notdef
					if (block >= bdptr->rootblock.nblocks)
						retval = BEC_DISKFULL;
					else
#endif
					{
						*bmptr |= mask;	/* Mark it */
						bdptr->bitmapchanged |=
							1 << (block/(BLOCKSIZE*8));
						retval = block;
						bdptr->nblksused++;
					}
					goto out;
				}
			}
		}
		bmptr++;
		bmoffset++;
	}
	retval = BEC_DISKFULL;
out:	bdptr->bmoffset = bmoffset;	/* For next time */
	return retval;
}

/*
 * Get a hunk of contiguous free blocks
 */

int getdirblks(int bdvrnum, int nblks)
{
	register struct blkdriver *bdptr;
	register uchar *bmptr;
	register uint startblk, endblk, blk;

	bdptr = &bdrivers[bdvrnum];
	bmptr = bdptr->bitmap;

	for (startblk = 0; startblk < bdptr->rootblock.nblocks - nblks; startblk++)
	{
		int _s, _e;

		endblk = startblk + nblks;
		for (blk = startblk; blk < endblk; blk++)
		{
			if (bmptr[blk/8] & (0x80 >> (blk & 7)))
				goto used;
		}
		for (blk = startblk; blk < endblk; blk++)
			bmptr[blk/8] |= (0x80 >> (blk & 7));	/* Reserve blocks */

/* V4.4: this was a bug.  Was: 'bdptr->bitmapchanged = 1;' */
		_s = startblk / (BLOCKSIZE * 8);
		_e = endblk / (BLOCKSIZE * 8);
		for (blk = _s; blk <= _e; blk++)
			bdptr->bitmapchanged |= 1 << blk;

		bdptr->nblksused += nblks;
		return startblk;
used:	;
	}
	return BEC_DISKFULL;
}

/*
 * Free up a block in a driver's bitmap.
 */

void freeblk(register struct blkdriver *bdptr,register uint block)
{
	uint mask, blkindex;

	mask = 0x80 >> (block & 7);	/* Mask for bit in byte */
	blkindex = block / 8;		/* Address of byte in table */

/* Some internal consistency checking */
	if ((block >= bdptr->rootblock.nblocks) ||
		(block < bdptr->rootblock.dirstart + bdptr->rootblock.ndirblocks))
	{
		eprintf("Attempted to release block %d!\r\n"); //, block);
		return;
	}

	if (!(bdptr->bitmap[blkindex] & mask))
		eprintf("Releasing %s block %d: already free\r\n"); //,bdptr->devname, block);
	else	/* Block count doesn't change if block already free! */
		bdptr->nblksused--;

	bdptr->bitmap[blkindex] &= ~(mask);
	bdptr->bitmapchanged |= 1 << (block / (BLOCKSIZE * 8));
}

/*
 * Write out the block driver's bitmap, update red tape
 * The block driver member 'bitmapchanged' tells us which bitmap blocks need rewriting
 */

int wrbitmap(int bdvrnum)
{
	register struct blkdriver *bdptr;
	register ushort mask;
	register int i, retval;
	int finalretval = 0;

	bdptr = &bdrivers[bdvrnum];
	/*
	 * Verify that the bitmap is not corrupted
	 */
	if (BDMISC(bdvrnum, MB_USEDBLOCKS, 0) != bdptr->nblksused)
	{
		conprintf("Disaster averted: corrupted bitmap at $%x not written\r\n Reset system & run fscheck on %s\r\n",
			bdptr->bitmap, bdptr->devname);
		bdptr->bitmapchanged = 0;
		return BEC_IOERROR;
	}

	for (mask = 1, i = 0; mask; mask <<= 1, i++)
	{
		OPTION(OPT_CANWRITERB, 1);
		if (bdptr->bitmapchanged & mask)
		{	/* Check all the bitmap blocks */
			retval = BLKWRITE(bdptr->rootblock.bitmapstart + i,
				bdptr->bitmap + i * BLOCKSIZE, bdvrnum);
			if (retval >= 0)
				bdptr->bitmapchanged &= ~mask;
			else
				finalretval = retval;
		}
		OPTION(OPT_CANWRITERB, 0);
	}
	return finalretval;
}

/*
 * Find a file in the directory and prepare a file control block
 * If mode = 0 (create), set up directory entry &
 *		write out the directory block.
 * If mode = 1 (read/write), set up the FCB.
 * Put many values into the fcb.
 * If mode = 2, create a directory entry, length 'load_addr'
 * If mode == 3, open it (like mode 1) and truncate
 * Statbits is copied into directory on creat
 */
int find_file(register char *fullpath,uint fcbnum, register int mode, int statbits, char *load_addr)
{
	register int dirpos, bdvrnum, retval;
	register struct fcb *fcbptr;
	register int i;
	struct dir_entry dirent;
	struct blkdriver *bdptr;
	char fname[FNAMESIZE];
	char *zbuf;
	int totblks;

	clearmem(&dirent, sizeof(dirent));
	if ((retval = bdvrnum = get_bdev(fullpath, fname)) < 0)
		goto err;
	fcbptr = ppfcbs[fcbnum];
	bdptr = &bdrivers[bdvrnum];
	if (mode != 1 && mode != 3)
	{
		movstr(dirent.file_name, fname, FNAMESIZE);
		stoupper(dirent.file_name);
	}
	GETDATE(dirent.date);		/* Put current time in FCB */
	switch (mode)
	{
	case 0:	/* Create */
		if ((retval = get_free(bdvrnum)) < 0)	/* The block map block */
			goto err;
		fcbptr->blkmapblk = retval;
		dirent.uid = ouruid;
		dirent.load_addr = load_addr;
		dirent.file_size = 0;
		dirent.blkmapblk = fcbptr->blkmapblk;
		dirent.statbits = statbits;
		dirent.magic = V4_2MAGIC;
		fcbptr->file_size = 0;
		fcbptr->uid = ouruid;
		if ((retval = (int)(fcbptr->blkmap =
				(ushort *)getmem1(BLOCKSIZE))) < 0)
			goto err;
		break;
	case 1:	/* Open for reading / writing */
	case 3:	/* Open and truncate */
		if ((retval = dirpos = PROCESSDIR(fullpath, &dirent, 2)) < 0)
			goto err;
		if (dirent.statbits & DIRECTORY)
		{
			retval = BEC_ISDIR;
			goto err;
		}
		BDMISC(bdvrnum, MB_BMBREAD, 0);
		totblks = bmbin(dirent.file_size);

		/* Allocate storage for the blockmap */
		if ((retval = (int)(fcbptr->blkmap =
				(ushort *)getmem1(totblks  * BLOCKSIZE))) < 0)
					goto err;

		/* If it is < 4k, maybe don't need to read block map */
		if (!usefulffb(&dirent, fcbptr->blkmap))
		{
			if (dirent.file_size &&
				(retval = MULTIBLKIO(bdvrnum, MIO_SREAD, fcbptr->blkmap,
					dirent.blkmapblk, totblks)) < 0)
					goto err;
		}
		fcbptr->file_size = dirent.file_size;
		fcbptr->blkmapblk = dirent.blkmapblk;
		/* Non-uid 0 modifying file: make him own it */
		if (ouruid)
			dirent.uid = ouruid;
		/* V4.4: Make CREAT() with different load_addr work correctly */
		if (mode == 3)
			dirent.load_addr = load_addr;
		fcbptr->uid = dirent.uid;
		break;
	case 2:	/* mkdir */
		dirent.uid = ouruid;
		dirent.load_addr = 0;
		dirent.file_size = (uint)load_addr;
		if ((retval = getdirblks(bdvrnum,(int )load_addr)) < 0)
			goto err;
		dirent.blkmapblk = retval;
		if ((retval = dogetmem(BLOCKSIZE, 0, "mkdir")) < 0)
			goto err;
		zbuf = (char *)retval;
		if ((retval = wrbitmap(bdvrnum)) < 0)
		{
			for (i = 0; i < (uint)load_addr; i++)
			{	/* Re-free the allocated blocks */
				freeblk(bdptr, dirent.blkmapblk + i);
			}
			goto xerr;
		}
		dirent.statbits = umask | DIRECTORY;

		/* Zap the new directory */
		clearmem(zbuf, BLOCKSIZE);
		BDMISC(bdvrnum, MB_DIRREAD, 0);
		for (i = 0; i < (int)load_addr; i++)
		{
			if ((retval = BLKWRITE(dirent.blkmapblk + i, zbuf, bdvrnum)) < 0)
			{
xerr:			dofreemem(zbuf);
				goto err;
			}
		}
		dofreemem(zbuf);
		break;
	}

	/* If it is creat, write the bitmap BEFORE the directory entry */
	if ((mode == 0) && ((retval = wrbitmap(bdvrnum)) != 0))
		goto err;

	if (mode != 1)	/* Create or directory make or truncate */
	{	/* Write the new directory entry */
		if ((retval = dirpos =
				PROCESSDIR(fullpath, &dirent, (mode == 3) ? 3 : 5)) < 0)
		{	/* Some sort of error */
#ifdef notdef
			if (mode == 0)	/* creat */
			{	/* Free the allocated block */
				freeblk(bdptr, fcbptr->blkmapblk);
			}
#endif
			goto err;
		}
	}

	/*
	 * Truncate mode: free all blocks, all directory blocks except first
	 */

	if (mode == 3 && dirent.file_size)
	{	/* First free the data blocks */
		for (i = 0; i < (dirent.file_size + BSIZEM1) / BLOCKSIZE; i++)
			freeblk(bdptr, fcbptr->blkmap[i]);
		/* Now free any extra blockmapblock blocks */
		i = totblks;
		while (--i > 0)
			freeblk(bdptr, dirent.blkmapblk + i);
		dirent.file_size = 0;
		fcbptr->file_size = 0;
		if ((retval = wrbitmap(bdvrnum)) != 0)
			goto err;
	}

	if (mode != 2)
	{	/* If it is a file, set up a few more FCB fields */
		/* The block which contains the directory entry for the file */
		fcbptr->dirblk = dirpos & 0xffff;
		fcbptr->dirindex = dirpos >> 16; /* Position in the block */
		fcbptr->bmindex = 0;	/* Index into block map */
		fcbptr->file_pos = 0;	/* File pointer */
		fcbptr->blkstat = 0;	/* Buffered block is invalid */
		fcbptr->changed= 0;		/* Buffered block doesn't need writing */
		fcbptr->needsflush = 1;	/* Safe */
	}

	retval = 0;
err:
	return retval;
}

/*
 * Flush out a block buffer. Clear the fcb's 'changed' bit
 */

int flush(struct fcb *fcbptr)
{
	int retval;

	if (!fcbptr->changed)
		return 0;
	BDMISC(fcbptr->bdvrnum, MB_NDIRREAD, 0);
	retval = BLKWRITE(fcbptr->blkmap[fcbptr->bmindex], fcbptr->blkbuf,
				fcbptr->bdvrnum);
	fcbptr->changed = 0;
	return retval;
}

/*
 * Refill the buffer of a file. If necessary, write out the current block.
 */

int refill(struct fcb *fcbptr)
{
	register int retval = 0;

	if (fcbptr->changed)
	{
		if ((retval = flush(fcbptr)))
			goto err;
	}
	fcbptr->bmindex = fcbptr->file_pos / BLOCKSIZE;
	/* Don't preread if at end of file, pointing at new block */
	if ((fcbptr->file_pos != fcbptr->file_size) || (fcbptr->file_pos & BSIZEM1))
	{
		BDMISC(fcbptr->bdvrnum, MB_NDIRREAD, 0);
		retval = BLKREAD(fcbptr->blkmap[fcbptr->bmindex],
			fcbptr->blkbuf, fcbptr->bdvrnum);
		fcbptr->blkstat = 1;	/* The block is valid */
	}
err:	return retval;
}

/*
 * Given the passed full pathname, find the directory entry one level higher and
 * return it. For example: if fullpath = "/RD/FRED/BILL/JOHN" then we find the
 * entry in /RD/FRED for BILL and return it in the passed structure. Return
 * negative error or the block device number for this file
 */

static int gethdirent(char *fullpath,struct dir_entry *dirptr)
{
	int bdvrnum, dirpos, retval;
	char *hfullpath;

	hfullpath = room0for(fullpath);
	if (!isdevid(fullpath))
		splitpath(hfullpath);

	retval = findde(hfullpath,(uint *) &bdvrnum,(uint *) &dirpos, dirptr);
	dofreemem(hfullpath);
	return (retval < 0) ? retval : bdvrnum;
}

/*
 * System call 118
 *
 * Directory munching code.
 * Mode = 0: delete file
 * Mode = 1: rename file
 * Mode = 2: read from directory, return position.
 * Mode = 3: write directory entry out
 * Mode = 4: Set uid
 * Mode = 5: write to an empty position, return position.
 * Mode = 6: change date to date at *buf
 * Mode = 7: clear attribute bits (buf == bitmask)
 * Mode = 8: set attribute bits (buf == bitmask)
 *
 * Return code on error, else position: bits 0-15 are directory block, 16-31 are index.
 */

int _processdir(char *pathname,char *buf,uint mode)
{
	register struct dir_entry *dirptr;
	register struct blkdriver *bdptr;
	register char *fullpath;
	register uint dirindex;
	register int bdvrnum;
	register ushort blkmapblk;
	register int block;
	register int retval;
	uint blknum;
	char fname[FNAMESIZE], newfname[FNAMESIZE];
	struct dir_entry pardirent;
	ushort bits;
	int bs;
	char *dirb;
	static char *pname = "processdir";

	if ((mode == 7 || mode == 8) && ischdev(pathname))
	{	/* Can alter perms on char devices */
		if ((retval = OPEN(pathname, 1)) >= 0)	/* mode7: clear bits */
		  retval = CDMISC(retval, CDM_SETPERM, (mode == 7) ? 1 : 0, buf,0);
		goto crash;
	}

	if ((retval = dogetmem(BLOCKSIZE, 0, pname)) < 0)
		goto crash2;
	dirb = (char *)retval;

	fsentry(0);
	bs = (int )buf;
	bits = (ushort)bs;

	fullpath = (char *)GETFULLPATH(pathname, 0);
	if (mode == 2)
	{	/* Read entry */
		int dpos, bdv;
		retval = (int)findde(fullpath,(uint *) &bdv,(uint *) &dpos,(struct dir_entry *) buf);
		if (retval >= 0)
			retval = dpos;
		goto out2;
	}

	if (isdevid(fullpath))
	{	/* Cope with a read of "/RD", etc */
		retval = BEC_ISDIR;
		goto out;
	}
	if ((bdvrnum = retval = get_bdev(fullpath, fname)) < 0)
		goto out;
	if (mode == 1)
	{	/* Crunch on second filename */
		char *fp;

		fp = (char *)GETFULLPATH(buf, 0);
		retval = get_bdev(fp, newfname);
		dofreemem(fp);
		if (retval < 0)
			goto out;
		if (!*newfname)
		{
			retval = BEC_FNBAD;
			goto out;
		}
	}

	bdptr = &bdrivers[bdvrnum];
	if ((retval = newdisk(bdvrnum)))
		goto out;
	if ((retval = gethdirent(fullpath, &pardirent)) < 0)
		goto out;

	/* Scan for the desired directory entry */
	BDMISC(bdvrnum, MB_DIRREAD, 0);
	for (blknum = pardirent.blkmapblk;
		blknum < (pardirent.blkmapblk + pardirent.file_size); blknum++)
	{
		if ((retval = BLKREAD(blknum, dirb, bdvrnum)) < 0)
			goto out;
		dirptr = (struct dir_entry *)dirb;
		for (dirindex = 0; dirindex < DIRPERBLOCK; dirindex++, dirptr++)
		{
			if (!strucmp(dirptr->file_name, fname) ||
				(!*dirptr->file_name && (mode == 5)))
				goto gotit;
		}
	}

/* New directory grow stuff */
	if (mode == 5)
	{
		char *olddir;
		char *dirpath;

		dirpath = room0for(fullpath);
		splitpath(dirpath);
		if (isdevid(dirpath))
		{	/* Root directory: can't be helped */
			retval = BEC_DIRFULL;
		}
		else
		{	/* OK. Read in the full directory */
			olddir = (char *)getmem0((pardirent.file_size + 1) * BLOCKSIZE);
			if ((int)olddir < 0)
			{
				retval = (int)olddir;
			}
			else
			{	/* Got the buffer. Read it */
				BDMISC(bdvrnum, MB_DIRREAD, 0);
				retval = MULTIBLKIO(bdvrnum, MIO_SREAD, olddir,
					pardirent.blkmapblk, pardirent.file_size);
				if (retval >= 0)
				{
					int dirb;
					clearmem(olddir + pardirent.file_size * BLOCKSIZE,
							BLOCKSIZE);
					dirb = getdirblks(bdvrnum, pardirent.file_size + 1);
					if (dirb < 0)
						retval = dirb;
					else
					{	/* Write the data back to new position */
						retval = MULTIBLKIO(bdvrnum, MIO_SWRITE, olddir,
								dirb, pardirent.file_size + 1);
						if (retval >= 0)
						{	/* Now alter parent pointer */
							int oldbmb;
							oldbmb = pardirent.blkmapblk;
							pardirent.blkmapblk = dirb;
							pardirent.file_size++;
							retval = PROCESSDIR(dirpath, &pardirent, 3);
							/* V4.7: Fix up entries for any open files */
							fixOpenFiles(bdvrnum, oldbmb,
										pardirent.file_size - 1, dirb);
							if (retval >= 0)
							{	/* Release old blocks */
								int i;
								for (i = 0; i < pardirent.file_size - 1; i++)
									freeblk(&bdrivers[bdvrnum], oldbmb + i);
								/* All done! */
								retval = wrbitmap(bdvrnum);
							}
						}
					}
				}
				dofreemem(olddir);
			}
		}
		dofreemem(dirpath);
		cachedel((char *)bdvrnum);
		dumpcurdir(bdvrnum);
		if (retval >= 0)
		{
			retval = PROCESSDIR(pathname, buf, mode);
			goto out2;
		}
	}
	else
		retval = BEC_NOFILE;
	goto out;

gotit:
	if ((mode == 5 && (retval = CHKPERM(&pardirent, FS_NOWRITE, fullpath))) ||
		(mode != 5 && (retval = CHKPERM(dirptr, FS_NOWRITE, fullpath))))
		goto out;
	switch (mode)
	{
	case 0:	/* Unlink */
		cachedel(fullpath);
		if (dirptr->statbits & LOCKED)
		{
			retval = BEC_LOCKED;
			goto out;
		}
		if (dirptr->statbits & DIRECTORY)
		{	/* Remove a directory: must check that it is empty */
			int buf2;
			struct dir_entry tdir;
			if ((buf2 = getmem1(BLOCKSIZE)) < 0)
			{
				retval = BEC_NOBUF;
				goto out;
			}
			retval = READDIR(bdvrnum, buf2, &tdir, 0, dirptr);
			dofreemem((void *)buf2);
			if (retval >= 0)
				retval = BEC_DIRNEMPTY;
			if (retval < -1)
				goto out;
			for (block = dirptr->blkmapblk;
				block < dirptr->blkmapblk + dirptr->file_size;
				block++)
				freeblk(bdptr, block);
		}
		else
		{	/* Remove a file */
			ushort *blkmap;
			int totblks;

			totblks = bmbin(dirptr->file_size);

			if ((retval = dogetmem(totblks * BLOCKSIZE, 0, pname)) < 0)
				goto crash;
			blkmap = (ushort *)retval;

			blkmapblk = dirptr->blkmapblk;
			if (dirptr->file_size)
			{
				if (!usefulffb(dirptr, blkmap))
				{
					BDMISC(bdvrnum, MB_BMBREAD, 0);
					if ((retval = MULTIBLKIO(bdvrnum, MIO_SREAD, blkmap,
							dirptr->blkmapblk, totblks)) < 0)
					{
							dofreemem(blkmap);
							goto out;
					}
				}
				for (block = 0; block < (dirptr->file_size +
					BSIZEM1) / BLOCKSIZE; block++)
				{
						freeblk(bdptr, blkmap[block]);
				}
			}
			for (block = 0; block < totblks; block++)
				freeblk(bdptr, blkmapblk + block);
		}
#ifdef notdef	/* Moved under 4.2 */
		if (retval = wrbitmap(bdvrnum))
			goto out;
#endif
		dirptr->file_name[FNAMESIZE-1] = dirptr->file_name[0];
		dirptr->file_name[0] = '\0';
		break;
	case 1:	/* Rename */
		if (dirptr->statbits & LOCKED)
		{
			retval = BEC_LOCKED;
			goto out;
		}
		strcpy(dirptr->file_name, newfname);
		stoupper(dirptr->file_name);
		cachedel(fullpath);
		break;
#ifdef notdef
	case 2:	/* Read directory entry */
		movde(dirptr, buf);
		break;
#endif
	case 5:	/* Write to empty position */
	case 3:	/* Modify directory entry */
		if ((mode == 3) && (dirptr->statbits & LOCKED))
		{
			retval = BEC_LOCKED;
			goto out;
		}
		movde(buf, dirptr);
		if (!*dirptr->file_name)	/* Zapping the file? */
			cachedel(fullpath);
		break;
	case 4:	/* Change uid */
	  bs = (int )buf;
	  dirptr->uid = (ushort)bs;
	  break;
	case 6:	/* Change date */
		movmem(buf, dirptr->date, sizeof(dirptr->date));
		break;
	case 7:	/* Clear attribute bits */
		dirptr->statbits &= (~bits) | DIRECTORY;
		break;
	case 8:	/* Set attribute bits */
		dirptr->statbits |= bits & ~(DIRECTORY|FS_HASFFB);
		break;
	default:
		retval = BEC_BADARG;
		goto out;
	}

	/* Re-write directory entry */
	BDMISC(bdvrnum, MB_DIRREAD, 0);
	retval = BLKWRITE(blknum, dirb, bdvrnum);

	/* Write blockmap now when deleting: correct order & more efficient */
	if (mode == 0 && (retval = wrbitmap(bdvrnum)) < 0)
		goto out;

out:
	if (retval >= 0)
	{
		retval = (dirindex << 16) | blknum;	/* Position */
		if ((mode != 0) && *dirptr->file_name)
			cacheadd(fullpath, bdvrnum, dirptr, retval);
	}
	else
		cachedel(fullpath);
out2:
	dofreemem(fullpath);
	fsexit(0);
	dofreemem(dirb);
crash2:
crash:
	return retval;
}

/*
 * Return true if passed path is a device ID such as /RD, /F0, etc
 */

int isdevid(char *path)
{
	return ((*path == '/') && (bindex(path, '/') == path + 1));
}

/*
 * Check that a file is there & closed
 */

int chkfpc(char *pathname)
{
	register int retval;
	struct dir_entry dirent;

	if ((retval = FILESTAT(pathname, &dirent)) < 0)
		return retval;
	if (retval)
		return BEC_FOPEN;
	return 0;
}

/*
 * Interpret a full pathname. Put the final filename into *fname,
 * return block driver number.
 */
int get_bdev(char *fullpath,char *fname)
{
	register char *cp;
	register int bdvrnum;
	char devbuf[FNAMESIZE];

	if (fname)
	{
		movstr(fname, bindex(fullpath, '/'), FNAMESIZE);
		stoupper(fname);	/* Get the filename part */
	}

	/* Get the name of the block device */
	movstr(devbuf, fullpath, FNAMESIZE);
	if (isdevid(devbuf))
	{
		if (fname)
			*fname = '\0';	/* No filename */
	}
	else
	{
		if ( ((cp = index(devbuf, '/')) == 0) ||
		     ((cp = index(cp + 1, '/')) == 0) )
		{
			bdvrnum = BEC_NOBDRIVER;
			goto err;
		}
		*cp = '\0';	/* Isolate the device identifier */
	}
	bdvrnum = FIND_BDVR(devbuf);
err:
	return bdvrnum;
}

/*
 * Return true if the passed string is unacceptable filename
 */

int checkfname(uchar *name)
{
	if (!*name)
		goto err;	/* Null string */
	for ( ; ; )
	{
		if (!*name)
			return 0;
		if ((*name < '#') || (*name > 0x7e))
			goto err;
		name++;
	}
err:	return -1;
}

/*
 * System call 68:
 * Work out whether two pathnames refer to the same file / directory.
 * Return true if not.
 */

int _pathcmp(char *p1,char *p2)
{
	register char *fp1, *fp2;
	int retval;

	fp1 = (char *)GETFULLPATH(p1, 0);
	fp2 = (char *)GETFULLPATH(p2, 0);

	retval = strucmp(fp1, fp2);

	dofreemem(fp1);
	dofreemem(fp2);

	return retval;
}

/*
 * System call 141:
 *
 * Check permissions in passed directory entry against passed mask
 */

int _chkperm(struct dir_entry *pdirent,ushort mask,char *fullpath)
{
	if ((mask & pdirent->statbits) && ouruid && (pdirent->uid != ouruid))
		return BEC_NOPERM;
	return 0;
}

/*
 * Enter critical region of the file system
 */

int wherebusy;

void fsentry(int x)
{
	int fssnooze();

	syssafe++;
	while (fsbusy && (fsbpid != curpid))
	{
		if (syssafe != 1)
		{
		conprintf("Panic in fsentry%x: syssafe=%d, curpid=%d, fsbpid=%d, wherebusy=0x%x, fsbusy=%d\r\n",
			*(&x - 1), syssafe, curpid, fsbpid, wherebusy, fsbusy);
			conprintf("Reset, please ");
			for ( ; ; );
		}
		syssafe--;
		SNOOZE(fssnooze, curpid, 0);
		syssafe++;
	}
	fsbpid = curpid;
	fsbusy++;
	wherebusy = *(&x - 1);
	syssafe--;
}

int fssnooze(int ourpid)
{
		return !(fsbusy && (fsbpid != ourpid));
}

/* Exitting a critical region */
void fsexit(int x)
{
	syssafe++;
	if (fsbusy < 1)
	{
		conprintf("Panic in fsexit%x: syssafe=%d, curpid=%d, fsbpid=%d, wherebusy=0x%x, fsbusy=%d\r\n",
		*(&x - 1), syssafe, curpid, fsbpid, wherebusy, fsbusy);
	}
	wherebusy = 0;
	fsbusy--;
	syssafe--;
}

/*
 * If passed directory entry has first four blocks in it and
 * is < 4k, copy block info across and return true.
 */

int usefulffb(struct dir_entry *dirptr,ushort *blkmap)
{
	if ((dirptr->file_size <= MAXFFSIZE) && (dirptr->statbits & FS_HASFFB) &&
			(dirptr->magic == V4_2MAGIC) &&
			(dirptr->ffblocks[0] > 7) && !noffbs)
	{
		movmem(dirptr->ffblocks, blkmap, sizeof(dirptr->ffblocks));
		return 1;
	}
	return 0;
}
