/*
 * Compare strings of given length, ignoring case
 */

int strnucmp(char *s1,char *s2,int n)
{
	while (n--)
	{
		if ((*s1 != *s2) && (toupper(*s1) != toupper(*s2)))
			return *s1 - *s2;
		s1++;
		s2++;
	}
	return 0;
}
