/*
 * Split a file pathname: /FRED/BILL/AAA becomes /FRED/BILL. Return pointer to AAA
 */

char *splitpath(char *str)
{
	register char *cp;
	char *bindex();

	cp = bindex(str, '/');
	if (*cp)
		*(cp - 1) = '\0';
	return cp;
}
