/* TABS4
 * SCSI driver
 */

#include <types.h>
#include <syscalls.h>
#include <blkerrcodes.h>
#include "5380.h"

struct cmdblock
{
	uchar opcode;
	uchar lbah;	/* Logical block address high and LUN */
	uchar lbam;	/* Logical block address mid */
	uchar lbal;
	uchar length;	/* Optional transfer length */
	uchar control;	/* Control byte, normally zero */
};

#ifdef DEBUG
#define IOFAULT IoFault(__LINE__), BEC_IOERROR
#else
#define IOFAULT BEC_IOERROR
#endif

#define Phasematch()	(*BUS_AND_STAT & BS_PHASEMATCH)
#define Req()			(*CUR_BUS_STAT & CB_REQ)

#define CMDLENGTH sizeof(struct cmdblock)
#define TIMEOUT 200000

#define BlockLength	(1024 / SectorsPerBlock)
#define SECTORSPERBLOCK  1024 /* added this in could not find it anywhere */


int scsi_stat;
int scsi_msg;
int scsi_sense;

int *ssptr;				/* Pointer to process lock variable */
int dummyssptr;			/* ssptr points here if we are not locking */
int SectorsPerBlock = SECTORSPERBLOCK;
uchar *hioptr;			/* Memory pointer on exit from haulio() */
uchar readwaitstate = 0;	/* Don't use wait states on reads */
uchar writewaitstate = 0;	/* Don't use wait states on writes */
extern int ScsiId;

static scsiout(), timeout(), cleanupbus();

#define D(p)			/* Debug thingy */

/*
 * Low level read: do it multiply if more than 256 blocks requested
 */

scsi_brw(lun, block, nblocks, ptr, isread)
uchar *ptr;
{
	int nblks;
	register int retval = 0;
	int retry = 2;

	nblocks *= SectorsPerBlock / 2;
	block *= SectorsPerBlock / 2;
	scsi_stat = 0;
	scsi_msg = 0;

	while (nblocks)
	{
		nblks = (nblocks > 255) ? 255 : nblocks;
doretry:
		retval = scsi_cmd(isread ? 8 : 10, lun, block, nblks, 0);
		if (statusstate())
		{
			if (retry--)
			{
/*				eprintf("scsi_brw: Retrying");*/
				goto doretry;
			}
			return IOFAULT;
		}
		if (retval == 0)
			retval = (isread) ? scsiin(ptr, nblks * BlockLength, TC_DATAIN):
					scsiout(ptr, nblks * BlockLength, TC_DATAOUT);
		if (!retval)
			retval |= scsistat();
		if (retval)
			return retval;
		nblocks -= nblks;
		block += nblks;
		ptr += BlockLength * nblks;
	}
	return 0;
}

/*
 * Select the SCSI device, transfer a command block
 */

scsi_cmd(op, lun, block, length, control)
{
	struct cmdblock cmdblock;

	cmdblock.opcode = op;
	cmdblock.lbah = (block >> 16) | (lun << 5);
	cmdblock.lbam = block >> 8;
	cmdblock.lbal = block;
	cmdblock.length = length;
	cmdblock.control = control;

	return scsi_send(lun, &cmdblock, CMDLENGTH);
}

scsi_send(lun, addr, len)
{
	uchar x;

	x = *RES_PARINT;
	if (*CUR_BUS_STAT & (CB_BSY|CB_SEL))
	{	/* Bit rude this: if the bus is in use, reset it */
#ifdef notdef
		eprintf("Bus busy: resetting");
		reset_scsi();
#endif
	}
	*TARGET_CMD = TC_DATAOUT;		/* Select controller */
	*SCSI_MODE = EN_PARITY;

	*OP_DATA = SCSI_ID(ScsiId)|SCSI_ID(6);	/* Controller ID */

	*OP_DATA = SCSI_ID(ScsiId);	/* Controller ID */

	*INIT_CMD = ASRT_DB;		/* Assert Data bus */
	*INIT_CMD = ASRT_SEL|ASRT_DB;	/* Assert SEL and Data bus */
	if (timeout(CUR_BUS_STAT, CB_BSY, CB_BSY))
	{
		eprintf("scsi_cmd: BSY not asserted");
		return IOFAULT;
	}
	*INIT_CMD = 0;		/* Negate SEL and Data bus */
	return scsiout(addr, len, TC_COMMAND);
}

static
scsiout(ptr, length, phase)
register uchar *ptr;
register uint length;
{
	register uint nblocks;
	register int retval;

	D(eprintf("SCSIOUT(phase=%d, length=%d)", phase, length));

	*TARGET_CMD = phase;		/* Set up desired phase */
	*INIT_CMD = ASRT_DB;		/* Assert Data bus */
	if (timeout(CUR_BUS_STAT, CB_REQ, CB_REQ))
	{
		eprintf("scsiout: No initial REQ");
		return IOFAULT;
	}
	if (!Phasematch())
	{
		eprintf("scsiout: No initial phase match");
		return IOFAULT;
	}
	length--;	/* One less than desired to allow for EOP */
	*SCSI_MODE = EN_PARITY|DMA_MODE;
	delay(100);	/* ? */
	*S_DMA_SEND = 0;	/* Start DMA mode transfers */
	delay(100);	/* ? */

	/* Wait for DMA request */
	if (timeout(BUS_AND_STAT, BS_DMAREQ, BS_DMAREQ))
	{
		eprintf("scsiout: No DMAreq");
		return IOFAULT;
	}

	/* Do sectors */
	if (writewaitstate && (length > 512))
	{	/* Multi block transfer */
		nblocks = length / 512;	/* Number of 512 byte blocks - 1 */
		if (retval = haulio(nblocks, ptr, 0))
		{
			eprintf("scsiout: haulio failed: %d. %d bytes sent",
						retval, hioptr - ptr);
			return IOFAULT;
		}
		ptr += nblocks * 512;
		/* Now prepare to read 511 bytes from the last block */
		length = 511;
	}

	/* Wait for DMA request */
	if (timeout(BUS_AND_STAT, BS_DMAREQ, BS_DMAREQ))
	{
		eprintf("scsiout: No DMAreq");
		return IOFAULT;
	}

	/* Do bytes */
	if ((retval = haulio(length, ptr, 2)) < 0)
	{
		eprintf("scsiout: haulio failed(2): %d, %d bytes sent",
					retval, hioptr - ptr);
		return IOFAULT;
	}
	ptr += length;

	/* Wait for DMA request */
	if (timeout(BUS_AND_STAT, BS_DMAREQ, BS_DMAREQ))
	{
		eprintf("scsiout2: No DMAreq");
		return IOFAULT;
	}

	/* Write last byte and assert EOP to end DMA */
	*EOP_OP_DATA = *ptr++;

#ifdef notdef
	/* Boring byte at a time thing */
	length++;
	PRINTF("scsiout: sending %d bytes ", length);
	while (length--)
	{
		if (timeout(CUR_BUS_STAT, CB_REQ, CB_REQ))
		{
			eprintf("yuk1: length = %d", length);
			return IOFAULT;
		}
		*OP_DATA = *ptr++;
		*INIT_CMD = ASRT_DB|ASRT_ACK;
		if (timeout(CUR_BUS_STAT, CB_REQ, 0))
		{
			eprintf("yuk2: length = %d", length);
			return IOFAULT;
		}
		*INIT_CMD = ASRT_DB;
	}
#endif

/* Wait until it stops requesting bytes */
	delay(100);
	if (timeout(CUR_BUS_STAT, CB_REQ, CB_REQ))
	{
		eprintf("scsiout yuk0: length = %d", length);
		return IOFAULT;
	}

	while (Phasematch())
	{
		PRINTF("<W>");
		if (timeout(CUR_BUS_STAT, CB_REQ, CB_REQ))
		{
			eprintf("yuk1: length = %d", length);
			return IOFAULT;
		}
		*OP_DATA = 0;
		*INIT_CMD = ASRT_DB|ASRT_ACK;		/* Assert ACK */
		if (timeout(CUR_BUS_STAT, CB_REQ, 0))
		{
			eprintf("yuk2: length = %d", length);
			return IOFAULT;
		}
		*INIT_CMD = ASRT_DB;			/* Negate ACK */
	}

	*INIT_CMD = ASRT_DB;				/* Negate all signals */
	*SCSI_MODE = EN_PARITY;
	*INIT_CMD = 0;		/* Float the data bus */
#ifdef notdef
	eprintf("scsiout end:");
	delay(1000);
	eprintf("scsiout end:");
#endif
	return 0;
}

/*
 * Read a block from the SCSI bus
 */

scsiin(ptr, length, phase)
register uchar *ptr;
register uint length;
{
	register int nblocks;
	int retval;
	int i;
	int nread;

	D(eprintf("SCSIIN(phase=%d, length=%d)", phase, length));
	D(delay(2000));
	*TARGET_CMD = phase;
	if (timeout(CUR_BUS_STAT, CB_REQ, CB_REQ))
	{
		eprintf("scsiin: No req");
		return IOFAULT;
	}
	D(delay(2000));
	D(PRINTF("W"));
	for (i = 0; i < 100000; i++)
	{
		delay(1);
		if (Phasematch())
			break;
	}
	D(PRINTF("A"));
	if (!Phasematch())
	{
		eprintf("scsiin: Phase mismatch");
		return IOFAULT;
	}
	D(PRINTF("B"));
	length--;			/* One less than desired to allow for EOP */
	*INIT_CMD = 0;		/* Make sure bus is I/P */
	*SCSI_MODE = EN_PARITY|DMA_MODE;
	*S_DMAI_RCV = 0;	/* Start DMA mode transfers */

	D(PRINTF("length before read=%d\r\n", length));
#ifdef notdef
	/* Boring byte at a time thing */
	nread = 0;
	while (length--)
	{
		if (timeout(CUR_BUS_STAT, CB_REQ, CB_REQ))
		{
			eprintf("yuk1: length = %d", length);
			return IOFAULT;
		}
		*ptr++ = *CUR_DATA;
		nread++;
		*INIT_CMD = ASRT_ACK;
		if (timeout(CUR_BUS_STAT, CB_REQ, 0))
		{
			eprintf("yuk2: length = %d", length);
			return IOFAULT;
		}
		*INIT_CMD = 0;
	}
	PRINTF("nread: %d\r\n", nread);
#endif

	D(PRINTF("C"));

	/* Wait for DMA req */
	if (timeout(BUS_AND_STAT, BS_DMAREQ, BS_DMAREQ))
	{
		eprintf("scsiin: before haulio: no DMAreq");
		return IOFAULT;
	}

	/* Do sectors */
	if (readwaitstate && (length > 512))
	{	/* Multi block transfer */
		nblocks = length / 512;	/* Number of 512 byte blocks - 1 */
		if (retval = haulio(nblocks, ptr, 1))
		{
			eprintf("scsiin: haulio failed: %d. %d bytes read",
							retval, hioptr - ptr);
			return IOFAULT;
		}
		ptr += nblocks * 512;
		/* Now prepare to read 511 bytes from the last block */
		length = 511;
	}

	/* Do bytes */
	if ((retval = haulio(length, ptr, 3)) < 0)
	{
		eprintf("scsiin: haulio failed(2): %d. %d bytes read",
					retval, hioptr - ptr);
		return IOFAULT;
	}
	ptr += length;

	D(PRINTF("D"));
	/* Read last byte and assert EOP to end DMA */
	if (timeout(BUS_AND_STAT, BS_DMAREQ, BS_DMAREQ))
	{
		eprintf("scsiin: before eop: no DMAreq");
		return IOFAULT;
	}
	*ptr++ = *EOP_IP_DATA;

	D(PRINTF("E"));
	*SCSI_MODE = EN_PARITY;		/* Turn off DMA mode */
	D(PRINTF("F"));
	*TARGET_CMD = 0;
	D(PRINTF("G"));
	return 0;
}

statusstate()
{
	if (timeout(CUR_BUS_STAT, CB_REQ, CB_REQ))
	{
		eprintf("statusstate: no REQ\r\n");
		return IOFAULT;
	}
	if ((*CUR_BUS_STAT & (CB_CD|CB_IO|CB_MSG)) == (CB_CD|CB_IO))
	{
		scsi_stat = -1;
		scsi_msg = -1;
		scsistat();
		FPRINTF(0, "status returned true: scsi_stat=%d, scsi_msg=%d\r\n",
				scsi_stat, scsi_msg);
		if (scsi_stat & 2)
		{
			FPRINTF(0, "Calling sense..");
			sense(0);
			FPRINTF(0, "Sense() returns 0x%x\r\n", scsi_sense);
		}
		return IOFAULT;
	}
	return 0;
}

sense(lun)
{
	register int retval, i;
	uchar buf[20];

	scsi_sense = -1;
	buf[0] = 0xff;
	if ((retval = scsi_cmd(3, lun, 0, 0, 0)) < 0)
	{
		eprintf("sense: scsi_cmd error ");
		return retval;
	}
	if ((*CUR_BUS_STAT & (CB_CD|CB_IO)) == (CB_CD|CB_IO))
	{
		eprintf("sense: calling scsistat ");
		scsistat();
		return -1;
	}
	if ((retval = scsiin(buf, 4, TC_DATAIN) < 0))
	{
		eprintf("sense: scsiin error ");
		return retval;
	}
	scsistat();
	scsi_sense = buf[0];
}

/*
 * Fetch the status and message bytes
 */

scsistat()
{
	*TARGET_CMD &= ~ASRT_DB;
	if (timeout(CUR_BUS_STAT, CB_REQ, CB_REQ))
	{
		eprintf("scsistat: No REQ (1)");
		if (!(*CUR_BUS_STAT & CB_BSY))
		{
			eprintf("scsistat: BSY died!");
			return 0;
		}
		/* No REQ, BSY asserted: probably a format command */
		return IOFAULT;
	}
	/* We are expecting the bus to be in status phase */
	*TARGET_CMD = TC_STATUS;
	delay(10);
	if (!Phasematch())
	{
		eprintf("scsistat: Bus not in status phase");
statfail:
		cleanupbus();
		return IOFAULT;
	}
	scsi_stat = *CUR_DATA;
	*INIT_CMD = ASRT_ACK;	/* Ack it */
	if (timeout(CUR_BUS_STAT, CB_REQ, 0))
	{
		eprintf("scsistat: disk did not negate REQ (1)");
		goto statfail;
	}
	*INIT_CMD = 0;
	
	*TARGET_CMD = TC_MSGIN;		/* Wait for message in phase */
	if (timeout(CUR_BUS_STAT, CB_REQ, CB_REQ))
	{
		eprintf("scsistat: No REQ (2)");
		goto statfail;
	}
	if (!Phasematch())
	{
		eprintf("scsistat(2): phase error: not in message phase");
		goto statfail;
	}
	scsi_msg = *CUR_DATA;
	*INIT_CMD = ASRT_ACK;	/* Ack it */
	if (timeout(CUR_BUS_STAT, CB_REQ, 0))
	{
		eprintf("scsistat: disk did not negate REQ (2)");
		goto statfail;
	}
	*INIT_CMD = 0;
	if (timeout(CUR_BUS_STAT, CB_BSY, 0))
	{
		eprintf("scsistat: disk did not release bus");
		goto statfail;
	}
	return 0;
}

/*
 * scsistat encountered a wierd bus problem: Attemp to recover
 */

static
cleanupbus()
{
	register int counter, retval;
	register uchar *tc = TARGET_CMD;
	register uchar *bas = BUS_AND_STAT;

	*INIT_CMD = 0;
	for (counter = 0; counter < 10000; counter++)
	{
		if (!(*CUR_BUS_STAT & CB_BSY))
		{	/* Bus is free */
			retval = 1;
			goto cbout;
		}
		if (!(*CUR_BUS_STAT & CB_REQ))
		{	/* No REQ: bus invalid */
			delay(5);
			continue;
		}
		*tc = TC_DATAOUT;
		if (Phasematch())
		{	/* Disk is expecting a data byte: strobe it out */
dataout:
			*INIT_CMD = ASRT_DB;
			*OP_DATA = 0;
			*INIT_CMD = ASRT_DB | ASRT_ACK;
			delay(1);
			*INIT_CMD = ASRT_DB;
			*INIT_CMD = 0;
			continue;
		}
		*tc = TC_DATAIN;
		if (Phasematch())
		{	/* Disk is sending us a byte: strobe it in */
			*INIT_CMD = ASRT_ACK;
			delay(1);
			*INIT_CMD = 0;
			continue;
		}
		*tc = TC_STATUS;
		if (Phasematch())
		{	/* Disk wants to send status */
			scsi_stat = *CUR_DATA;
			*INIT_CMD = ASRT_ACK;
			delay(1);
			*INIT_CMD = 0;
			continue;
		}
		*tc = TC_MSGIN;
		if (Phasematch())
		{	/* Disk wants to send status */
			scsi_msg = *CUR_DATA;
			*INIT_CMD = ASRT_ACK;
			delay(1);
			*INIT_CMD = 0;
			continue;
		}
		*tc = TC_MSGOUT;
		if (Phasematch())
			goto dataout;
		*tc = TC_COMMAND;
		if (Phasematch())
			goto dataout;
	}
	retval = 3;
cbout:
	eprintf("cleanupbus: exit state = %d", retval);
	return retval;
}

/*
 * Wait for the selected bits at the selected address to go to the selected
 * levels. Timeout on 'delay' * approx 6 uSec. (15MHz machine)
 * Return 0 if the bit changes as desired.
 */

static
timeout(addr, mask, level)
register uchar *addr;
register uchar mask;
int level;
{
	register uint delay = TIMEOUT;

	if (level)
	{
		while (delay--)
		{
			if ((*addr & mask) == mask)
				return 0;
		}
	}
	else
	{
		while (delay--)
		{
			if (!(*addr & mask))
				return 0;
		}
	}
	return IOFAULT;
}

/* Clear error status, etc */
clear_errors()
{
	scsi_stat = 0;
	scsi_msg = 0;
}

/* General initialisation */
Scsi_init()
{
	uchar x;

	*INIT_CMD = 0;
	*SCSI_MODE = EN_PARITY;
	*OP_DATA = 0;
	*TARGET_CMD = 0;
	x = *RES_PARINT;
}

/* Assert /RST for a while */
reset_scsi()
{
	*INIT_CMD = ASSERT_RST;
	delay(5000);
	*INIT_CMD = 0;
	delay(15000);

	/* Fetch pointer to process lock variable: used in haulio */
#ifdef IOPROCESSLOCK
	if (GETROMVER() >= 0x41)
		ssptr = (int *)SSPTR();
	else
#endif
		ssptr = &dummyssptr;

#ifdef notdef
	{
	/*
	 * The following seems to be needed by the 20 Mbyte Seagate after
	 * reset: dunno why
	 */
		int retval;

		retval = scsi_cmd(0, 0, 0, 0, 0);
		retval = scsistat();
		return retval;
	}
#endif
	return 0;
}

#ifdef DEBUG
IoFault(line)
{
	FPRINTF(0, "I/O error detected at line %d\r\n", line);
}
#endif
