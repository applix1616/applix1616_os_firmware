/*
 * Cassette I/O stuff
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
#include <via.h>
#include <storedef.h>
//#include <splfns.h>

#define MAXCOUNT	37	/* 52 usec timeout */
#define BYTETIMEOUT	3000	/* Number of loops for a byte receive timeout */
#define OVERRUN		30	/* Motor overrun time (in 20 msec increments) */
#define WSTABILISE	7500	/* Delay for speed settling (in 100 usec's) */
#define CASBSIZE	16	/* FIFO for cassette I/O */

/* Cassette read ISR states */
#define LEADERLOCK	0
#define STARTWAIT	1
#define RDBYTES		2

uchar cas_cksm;
ushort casdata;			/* The flag & data from the cassette ISR */
ushort casr_state;		/* What the cassette ISR must do */

/* Stuff for the Cassette write ISR */
ushort cwmask, cwtemp, casbufcnt;	/* Must be short (for caswaisr.s) */
int (*caswcptr)();		/* Pointer to transmit state code */

/* The following are used for assembling cassette data at interrupt time */
ushort casmask;
ushort ldr_size, ldrcnt;
uchar casbuf[CASBSIZE];
uint cashead, castail;		/* Indexes into casbuf */

ushort mtimer;			/* Timer for motor overrun */
ushort vsvecnum;		/* Holds the local VS vector number */

char casvectorin;		/* Flag: vector has been installed */

static uchar rdbyte(void);
static motoron(), motoroff();
static void wrbyte(uchar bval);

EXTERN ushort nobeep;		/* Prevent bells */

/*
 * Initialise the cassette system
 */

cas_init()
{
	*V_DDRA |= CLRCASIRQ|ECASRIRQ|ECASWIRQ;
	*V_AREG &= ~CLRCASIRQ;
	*V_AREG |= ECASRIRQ|ECASWIRQ;
	motoroff();
	mtimer = 0;
	casvectorin = 0;
}

casinit2()
{
	int motor_timer();
	if (!casvectorin)
	{
		casvectorin = 1;
		SET_VSVEC(motor_timer, 1, 0);
	}
}

/*
 * System call 21: Raw cassette O/P
 *
 * Raw cassette output routine: write a hunk of memory to tape, with
 * the supplied leader length (in bytes). Write out:
 *
 *		Leader
 *		Length high byte
 *		Length low byte
 *		Length high byte + length low byte
 * 		Data bytes
 */

_caswraw(register char *start, register int length, register int leadersize)
{
	int casw_aisr();	/* The assembler state machine stuff */
	int cws0(), cws1();	/* Start up states */

	casinit2();

	nobeep = 1;
	if (!motoron())
	{
		delay(WSTABILISE);	/* Wait for it to steady */
	}

	caswcptr = cws1;	/* Where to start up at */

	ENT1INTS(casw_aisr, MAXCOUNT);	/* Start the interrupts */
	*V_DDRA |= CLRCASIRQ;		/* Set to O/P */
	casbufcnt = cashead = castail = 0;

	while (leadersize--) wrbyte(0xff);
	wrbyte(0xfe);		/* Sync bit */
	cas_cksm = 0;		/* Start accumulation */
	wrbyte(length >> 24);
	wrbyte(length >> 16);
	wrbyte(length >> 8);
	wrbyte(length);
	wrbyte(cas_cksm);
	cas_cksm = 0;
	while (length--)
		wrbyte(*start++);
	wrbyte(cas_cksm);

/* Now send enough trailing bytes to ensure that the whole buffer has been written */
	for (length = 0; length <= CASBSIZE; length++)
		wrbyte(0);		/* Finish off last bit */

	caswcptr = cws0;	/* Go into idle state */
	DIST1INTS();		/* Interrupts off */
	mtimer = OVERRUN;	/* Set up for motor overrun */
	nobeep = 0;
}

/*
 * System call 22: Raw cassette I/P
 *
 * Read a hunk from tape. Wait for (8*leadersize) ones and then lock in. Read
 * length high, length low, length checksum, data. Return -1 on error, else
 * return the length of the block. Return an error if the data block is longer
 * than 'maxhunk'.
 */

_casrraw(register uchar *buf, int leadersize, int maxhunk)
{
	register int blocksize, retval;
	register int rbyte, ledval;
	register ushort i;
	int rb2, s;
	int casr_aisr();			/* The assembler stuff */

	casinit2();
	nobeep = 1;
	motoron();
	leadersize <<= 3;			/* Bytes to bits */
	s = CAS_SPL();
	*((int *) CAS_IVEC) = (int) casr_aisr;
	casr_state = LEADERLOCK;
	ldr_size = leadersize;
	cashead = castail = ldrcnt = 0;
	SET_LED(ledval = 0);

	*V_DDRA |= ECASRIRQ|CLRCASIRQ;	/* Ensure the ECASRIRQ bit is O/P */
	*V_AREG &= ~ECASRIRQ;		/* Enable cassette interrupts */
	*V_AREG |= CLRCASIRQ;		/* Unreset IRQ flip-flop */
	_splx(s);

	while (casr_state == LEADERLOCK)
		;			/* Wait for lock */
	SET_LED(ledval ^= 1);
	while (casr_state == STARTWAIT)
		;			/* Wait for start bit */
	SET_LED(ledval ^= 1);

/* The header has been found */
	cas_cksm = 0;

	for (i = 0; i < 4; i++)
	{
		if ((rbyte = rdbyte()) == -1)
		{
			retval = -1;
			goto casrexit;
		}
		blocksize = ((blocksize << 8) | rbyte);
	}
	rbyte = cas_cksm & 0xff;
	if (rbyte != (rb2 = rdbyte()))		/* Length checksum error? */
	{
		eprintf("Header checksum error: %x -> %x\r\n", rbyte, rb2);
		retval = -1;
		goto casrexit;
	}
	SET_LED(ledval ^= 1);

	if (blocksize > maxhunk)
	{
		eprintf("Found blocksize too large: %x\r\n", blocksize);
		retval = -1;
		goto casrexit;
	}

	cas_cksm = 0;
	retval = blocksize;
	while (blocksize--)		/* Read it all in */
	{
		if ((rbyte = rdbyte()) == -1)
		{
			eprintf("Read error\r\n");
			retval = -1;
			goto casrexit;
		}
		*buf++ = rbyte;
	}
	rbyte = cas_cksm & 0xff;

	SET_LED(ledval ^= 1);

/* Check the checksum */
	if (rbyte != (rb2 = rdbyte()))
	{
		retval = -1;
		eprintf("Final xsum: %x -> %x\r\n", rbyte, rb2);
	}

casrexit:	;
	s = CAS_SPL();

	*V_AREG |= ECASRIRQ;		/* Disable cassette interrupts */
	*V_AREG &= (char) ~CLRCASIRQ;
	*V_DDRA &= (char) ~ECASRIRQ;	/* Program as input to prevent accidents */
	mtimer = OVERRUN;		/* Start the motor off timer */
	_splx(s);
	nobeep = 0;
	return retval;			/* Return either -1 or blocksize */
}

/*
 * Wait for a byte to be assembled. Return -1 if error, else the byte
 */

static uchar rdbyte(void)
{
	ushort timeout;
	uchar dat;

	timeout = 0;

	do
		timeout++;
	while ((timeout != BYTETIMEOUT) && (cashead == castail));

	if (timeout == BYTETIMEOUT) return -1;
	dat = casbuf[castail++];
	if (castail >= CASBSIZE)
		castail = 0;
	cas_cksm += dat;
	return dat;
}

/*
 * Cassette read ISR
 */

casr_cisr()
{
	register ushort cdat;

	*V_AREG &= ~CLRCASIRQ;		/* Reset IRQ */
	*V_AREG |= CLRCASIRQ;

	cdat = *IPORT & 2;
	switch (casr_state)
	{
	case LEADERLOCK:	/* Waiting for ldr_size 1's */
		if (cdat)
		{
			if (ldrcnt >= ldr_size)
				casr_state = STARTWAIT;
			else
				ldrcnt++;
		}
		else
			ldrcnt = 0;
		break;
	case STARTWAIT:
		if (!cdat)
		{
			casr_state = RDBYTES;
			casmask = 0x80;		/* Mask for first bit */
			casdata = 0;
		}
		break;
	case RDBYTES:
		if (cdat)
			casdata |= casmask;
		casmask >>= 1;
		if (!casmask)
		{	/* Full byte assembled */
			casbuf[cashead++] = casdata;
			if (cashead >= CASBSIZE)
				cashead = 0;
			casmask = 0x80;
			casdata = 0;
		}
		break;
	}
}
		
/*
 * Write a byte on tape
 */

static void wrbyte(uchar bval)
{
	while (casbufcnt > (CASBSIZE - 2))
		;	/* Wait for room */
	casbuf[cashead++] = bval;
	if (cashead >= CASBSIZE)
		cashead = 0;
	casbufcnt++;
	cas_cksm += bval;	/* Accumulate checksum */
}

/*
 * Turn on cassette motor. Return true if it was on, else 0
 */

static motoron()
{
	int motsave, s;

	s = CAS_SPL();
	mtimer = 0;		/* Prevent motor stopping */
	motsave = alval;
	*AMUXLATCH = alval = (motsave & ~CASMOTOR);
	_splx(s);
	return !(motsave & CASMOTOR);
}

/*
 * Turn off cassette motor
 */

static
motoroff()
{
	int s;

	s = CAS_SPL();
	*AMUXLATCH = (alval |= CASMOTOR);
	_splx(s);
}

/*
 * This function is called every vertical interrupt after cassette I/O.
 * When the time-out counter is exhausted, turn off the motor and remove the
 * reference to this function from the vertical sync vector list
 */

motor_timer()
{
	if (mtimer && (mtimer-- == 1))
		motoroff();
}
