/* TABS4 NONDOC
 * Enviroment variable manipulation
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <process.h>
#include <files.h>
//#include "envbits.h"
#include "env.h"

extern char *room1for(), *movstr(), *getmem1(), *getzmem1(), *index();
extern char *room0for(), *getmem0(), *roomfor();

EXTERN int curpid, mtnotup, ouruid;

/*
 * Delete an entry. Return 1 if it was there
 */

rmenv(proc, name)
PROC *proc;
char *name;
{
	register ENVSTRING *envstring, *prev;

	prev = (ENVSTRING *)0;
	envstring = proc->shellenv->envstring;

	while (envstring && strcmp(envstring->name, name))
	{
		prev = envstring;
		envstring = envstring->next;
	}
	if (envstring)
	{	/* Found it */
		maybefree(envstring->name);
		maybefree(envstring->setting);
		if (prev)
			prev->next = envstring->next;
		else
			proc->shellenv->envstring = envstring->next;
		dofreemem(envstring);
		return 1;
	}
	return 0;
}

/*
 * Find an entry, return pointer to setting, else 0 if not there
 * If name < 0x4000, return env structure indexed by name, 0 if absent.
 * The 'mode' field must intersect with the environ string's
 * mode.
 * Called from PROCCNTL()
 */

char *
_getenv(proc, name, mode)
PROC *proc;
char *name;
{
	register ENVSTRING *envstring;
	register char *retval;
	register long iname, count;

	iname = (long)name;
	envstring = proc->shellenv->envstring;
	count = 0;
	retval = (char *)0;
	while (envstring)
	{
		if (iname < TPALOC)
		{
			if (count == iname)
			{
				retval = (char *)envstring;
				break;
			}
		}
		else if ((envstring->uid == ouruid) &&
			(mode & envstring->mode) && !strcmp(envstring->name,name))
		{
			retval = envstring->setting;	/* Found */
			break;
		}
		envstring = envstring->next;
		count++;
	}
	return retval;
}

/*
 * Set an environment string. If setting == (char *)0, remove
 * setting for 'name'.
 * Return 1 if 'name' existed.
 * Called from PROCCNTL()
 * Mode = ENVS_DSIGN for '$' only, ENVS_ARG for argument only
 */

_setenv(proc, name, setting, mode)
PROC *proc;
char *name, *setting;
{
	register ENVSTRING *envstring, *prev;
	register int retval;

	retval = rmenv(proc, name);
	if (setting)
	{	/* Add new value to end */
		envstring = proc->shellenv->envstring;
		prev = (ENVSTRING *)0;
		while (envstring)
		{
			prev = envstring;
			envstring = envstring->next;
		}
		envstring = (ENVSTRING *)getzmem1(sizeof(*envstring));
		if (prev)
			prev->next = envstring;
		else	/* Head of list */
			proc->shellenv->envstring = envstring;
		envstring->name = room1for(name);
		envstring->setting = room1for(setting);
		envstring->mode = mode;
		envstring->uid = proc->uid;
	}
	return retval;
}

/*
 * Unlink all of a process's environment strings
 * If pid == -1, trash ALL environment strings
 */

trashenvstrings(pid)
{
	int apid;
	if (pid == -1)
	{
		for (apid = 0; apid < MAXPIDS; apid++)
			trashens(apid);
	}
	else
		trashens(pid);
}

trashens(pid)
{
	register ENVSTRING **penvstring;
	register PROC *proc;

	proc = (PROC *)GETPROCTAB(pid);
	if ((long)proc < 0)
		return;
	penvstring = &(proc->shellenv->envstring);
	while (*penvstring)
		rmenv(proc, (*penvstring)->name);
}

/*
 * Copy one process's environment strings into another's.
 * Process tables must be set up. Called from SCHEDPREP()
 */

void copyenvstrings(PROC *toproc,PROC *fromproc)
{
	register ENVSTRING *envstring;
	int mask;

	envstring = fromproc->shellenv->envstring;
	while (envstring)
	{
		if ((envstring->uid == 0) || (envstring->uid == toproc->uid))
			mask = -1;			/* Inherit all modes */
		else
			mask = ENVS_DSIGN;	/* Don't inherit executables */

		_setenv(toproc, envstring->name, envstring->setting,envstring->mode & mask);
		envstring = envstring->next;
	}
}

/*
 * EXEC system call environ string substitution code:
 * Passed a string, perform substitutions, return a string allocated
 * in mode 'memmode' memory with the massaged output
 */

char *
__envsub(in, dollaronly, memmode)
char *in;
uint memmode;
{
	register char quote;
	register int resindex;
	register char ch;
	int startindex;		/* Start of macro position */
	int endindex;		/* Char after end of macro */
	int substart;		/* Where subst string starts */
	int subend;			/* Where it ends */
	int settinglen;
	register char *cp, *result;
	char *name, *setting, *newresult;
	int newlen, firstbit;
	int getenvmode, isinarg;

	if (memmode > 1)
	{
		conprintf("Garbage memmode to ENVSUB(%s, %d, %d)\r\n",
			in, dollaronly, memmode);
		memmode = 0;
	}

	if (mtnotup)
		return roomfor(in, memmode);

	getenvmode = envbits(ENVB_DODSIGN) ? ENVS_DSIGN : 0;
	if (envbits(ENVB_DOARG))
		getenvmode |= ENVS_ARG;
	if (envbits(ENVB_DOARG0))
		getenvmode |= ENVS_ARG0;

	quote = 0;
	firstbit = 2;
	result = roomfor(in, memmode);

	for (resindex = 0 ; ; resindex++)
	{
		cp = result + resindex;
		if (!(ch = *cp))
			break;
		if (firstbit && !iswhite(*cp))
		{	/* Invalidate argv0 matching */
			firstbit--;
			if (firstbit == 0)
				getenvmode &= ~ENVS_ARG0;
		}

		if (ch == '"')
		{
			quote = !quote;
			continue;
		}
		if (quote)
			continue;
		isinarg = 0;
		if (((ch == '$') && (getenvmode & ENVS_DSIGN)) ||
			(isinarg = ( !dollaronly && (getenvmode & (ENVS_ARG|ENVS_ARG0))
 				&& (!resindex || iswhite(cp-1)) &&
					!iswhite(*cp))))
		{	/* It is a macro */
			int xbits;

			xbits = (ch == '$') ? ENVS_DSIGN : (ENVS_ARG|ENVS_ARG0);
			startindex = resindex;
			if (!isinarg)
			{
				resindex++;
				cp++;		/* Advance to '(' or first char */
			}
			if (!*cp)
				break;
			if (*cp == '(' && !isinarg)
			{	/* In parenthesis */
				resindex++;
				substart = resindex;
				cp++;	/* Advance to first char */
				while (*cp && (*cp != ')'))
					cp++;
				if (!*cp)
					break;
				subend = cp - result;
				cp++;
				endindex = cp - result;
			}
			else
			{	/* Search to end of word */
				substart = resindex;
				while ((ch = *cp) &&
					!(isinarg ? (int)iswhite(*cp) :
						(int)index("\" \t./", *cp)))
					cp++;
				subend = endindex = cp - result;
			}
			/* Isolate the environment string */
			name = (char *)GETMEM(subend - substart + 3, memmode);
			movstr(name, result + substart, subend - substart + 1);
			setting = (char *)GETENV(curpid, name, getenvmode & xbits);
			dofreemem(name);
			if (setting == (char *)0)
				continue;

			/* Substitute */
			settinglen = strlen(setting);
			newlen = strlen(result) + settinglen + 3; /* Too much, but safe */
			newresult = (char *)GETMEM(newlen, memmode);
			movmem(result, newresult, startindex);
			movmem(setting, newresult + startindex, settinglen);
			strcpy(newresult + startindex + settinglen,
				result + endindex);
			dofreemem(result);
			result = newresult;
			resindex = startindex + settinglen - 1;
		}
	}
	return result;
}

/*
 * The 'set' inbuilt command
 */

int set(int argc,char *argv)
{
	register int arg, count, retval;
	register char *cp;
	register ENVSTRING *envstring;
	char *av;
	int setmode;

	if (argc == 1)
	{
		for (count = 0; count < TPALOC; count++)
		{
			envstring = (ENVSTRING *)GETENV(curpid, count, -1);
			if ((long)envstring <= 0)
				break;
			if (envstring->mode & ENVS_ARG)
				cp = "-a ";
			else if (envstring->mode & ENVS_ARG0)
				cp = "-e ";
			else
				cp = "";
			PRINTF("set %s\"%s=%s\"\r\n",
					cp, envstring->name, envstring->setting);
		}
		return 0;
	}

	av = &argv[1];
	if ((argc == 2) && !strcmp(av, "-"))
	{
		trashens(curpid);
		return 0;
	}

	retval = 0;
	arg = 2;
	if (!strucmp(av, "-e"))
		setmode = ENVS_ARG0|ENVS_DSIGN;
	else if (!strucmp(av, "-a"))
		setmode = ENVS_ARG|ENVS_ARG0|ENVS_DSIGN;
	else
	{
		setmode = ENVS_DSIGN;
		arg = 1;
	}

	for ( ; arg < argc; arg++)
	{
		av = &argv[arg];
		cp = index(av, '=');
		if (cp)
		{	/* New setting */
			*cp = '\0';
			SETENV(curpid, av, cp + 1, setmode);
		}
		else
		{	/* Delete old */
			if (!SETENV(curpid, av, 0, 0))
			{
				eprintf("'%s': No such setting\r\n", av);
				retval = -1;
			}
		}
	}
	return retval;
}

/* Look up decimal environment string, return default if it is < 10 */
igenv(s, dfault)
char *s;
{
	char *ep;
	int ret;

	ep = (char *)GETENV(curpid, s, -1);
	ret = ep ? atoi(ep) : 0;
	if (ret < 10)
		ret = dfault;
	return ret;
}
