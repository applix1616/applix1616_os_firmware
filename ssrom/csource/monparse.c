/* TABS4 NONDOC
 * Code for interpreting & checking input lines
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <mondefs.h>
#include <blkio.h>
#include <blkerrcodes.h>
#include <envbits.h>
#include <options.h>
//#include <splfns.h>

EXTERN int miscmodes;

char *index(), *bindex(), *skipwhite(), *room0for();

/*
 * System call 93
 *
 * Split string into sub-strings, skipping white space, etc. Allocate memory
 * for the sub-strings. Stash pointers to sub-strings in *argv.
 * Return number of strings separated or negative error code.
 * If canexpand != 0, perform wildcard expansion
 */

_sliceargs(str, argv, canexpand)
register char *str;	/* The string */
register char **argv;	/* The pointers */
{
	register uint i;
	register ushort argc;
	ushort xargc;
	int retval;
	char lbuf[MAXIPLINE];
	char doublequote;

	argc = 0;
	while (*(str = skipwhite(str)) && (argc < (MAXMONPAR - 1)))
	{	/* Go through string, putting entries into argv */
		i = 0;
		doublequote = 0;
		if (*str == '"')
		{
			if (*++str == '"')	/* Double quote */
			{
				doublequote = 1;
				goto dquote;
			}
			/* Single quote */
			while (*str && (*str != '"'))
				lbuf[i++] = *str++;
			lbuf[i] = '\0';
			if (*str == '"')	/* " or end of line */
				str++;
			if (i)
				goto stash;
		}
		else
		{
dquote:			while (*str && !iswhite(*str))
				lbuf[i++] = *str++;
			if (doublequote && i && (lbuf[i-1] == '"'))
				i--;	/* Strip a quote */
			lbuf[i] = '\0';
			if (canexpand && !doublequote)
			{
				xargc = argc;	/* Can't point to register */
				if (retval = wildexp(lbuf, &xargc, argv))
				{
					argv[xargc] = 0;
					return retval;
				}
				argc = xargc;
			}
			else
stash:				argv[argc++] = room0for(lbuf);
		}
	}
	if (argc >= (MAXMONPAR - 1))		/* Too many args */
	{
		argv[argc] = (char *)0;
		return -1;
	}

	argv[argc] = (char *)0;		/* Terminal null */
	return argc;
}

/*
 * System call 91
 *
 * Passed a pointer to the array of pointers to strings,
 * interpret and evaluate the strings, put the results in *type and *value
 */

_clparse(argv, type, value)
uchar **argv;
register char *type;		/* Argument type */
register uint *value;		/* Evaluated args */
{
	register uint val;
	register ushort argt;
	register uchar *str;
	int sign;
	int v1;

	/* argtype entries beyond nargs must be EMPTY */
	bsetmem(type, EMPTY, MAXMONPAR * sizeof(*type));

/* Look for values */
	for( ; str = *argv; argv++, type++, value++)
	{
		argt = NUMBER;		/* Assume it is a number */
		val = 0;
		switch (*str)
		{
		case '.':
			str++;
			if (*str == '-')
			{
				str++;
				sign = -1;
			}
			else
				sign = 1;

			if (*str == '\0')
				argt = STRING;
			else
			{
				while (*str)
				{
					if (isdigit(*str))
						val = val * 10 + (uint)(*str - '0');
					else
						argt = STRING;
					str++;
				}
				v1 = val;	/* Make it an int */
				v1 *= sign;
				val = v1;
			}
			break;
		case '\'':
			if (str[1] && !str[2])
				val = str[1];
			else
				argt = STRING;
			break;
		case '%':
			if (!str[1])
				argt = STRING;	/* % sign on its own */
			else
			{
				while (*++str)
				{
					val <<= 1;
					if (*str == '1')
						val++;
					else
					{
						if (*str != '0')
							argt = STRING;
					}
				}
			}
			break;
		default:
			do
			{
				if (ishex(*str))
					val = (val << 4) + hdtoi(*str);
				else
					argt = STRING;
			}
			while (*++str);
			break;
		}
		*type = argt;
		/* If it is a string, valuei points to it (for SYSCALL command) */
		*value = (argt == STRING) ? (uint)*argv : val;
	}
	return 0;
}


/*
 * We are called with a single string from the command line: if it
 * contains wilds, tramp through the directory allocating space
 * for matches. If it doesn't, allocate space for it alone.
 */

wildexp(str, pnargs, argv)
register char *str;
ushort *pnargs;			/* Pointer to number of args found */
char **argv;			/* Where we are putting them */
{
	register int haswild;		/* Flag: name has wild chars */
	register int retval;
	register char *fname;		/* Wild filename without prefix */
	register char *fullpath, *strcopy;
	char *ostrcopy;
	int i, eb;
	uint dirsize;
	char gotmatch = 0;
	struct dir_entry *pdir, *opdir;
	long caseinsensitive;

	haswild = glob_pattern_p(str);		/* Does it have wildcards? */
	if (!haswild)
	{
nowild:		if ((int)(argv[*pnargs] = room0for(str)) < 0)
			return BEC_NOBUF;
		(*pnargs)++;
		return 0;
	}
	ostrcopy = strcopy = room0for(str);
	fullpath = (char *)GETFULLPATH(strcopy, 0);
	if (index(strcopy, '/'))		/* Is it compound? */
	{
		fname = bindex(strcopy, '/');	/* Our local filename */
		if (!glob_pattern_p(fname))
		{
			dofreemem(fullpath);
			dofreemem(ostrcopy);
			goto nowild;
		}
		*(fname - 1) = '\0';		/* Separate file & path */
	}
	else
	{
		fname = strcopy;
		strcopy = "";
	}
	stoupper(fname);
	/* Truncate the pathname: it MUST have a '/' in it by now */
	*(bindex(fullpath, '/') - 1) = 0;

	retval = rdallmode(fullpath, &dirsize);

	dofreemem(fullpath);
	if (retval < 0)
		goto out;
	opdir = pdir = (struct dir_entry *)retval;

	caseinsensitive = !(miscmodes & OPM_CASESENSITIVE);
	if (caseinsensitive)
		realstoupper(fname);

	eb = envbits(ENVB_HIDEFILES);

	for (i = 0; i < dirsize; i++, pdir++)
	{	/* Loop through directory */
		char uppername[FNAMESIZE];

		if (caseinsensitive)
		{
			strcpy(uppername, pdir->file_name);
			realstoupper(uppername);
		}

		if (pdir->file_name[0] &&
			!(eb && (pdir->statbits & FS_HIDDEN)) &&
			glob_match(fname,
					caseinsensitive ? uppername : pdir->file_name))
		{	/* A match */
			char *cp;
			if ((int)(cp = (char *)getmem0(strlen(pdir->file_name) +
				strlen(strcopy) + 2)) < 0)
			{
				retval = BEC_NOBUF;
				goto out2;
			}
			if (*strcopy)
			{
				strcpy(cp, strcopy);
				strcat(cp, "/");
			}
			else
				*cp = '\0';

			strcat(cp, pdir->file_name);	/* cat to prefix */
			argv[(*pnargs)++] = cp;
			gotmatch = 1;
		}
	}
out2:
	dofreemem(opdir);
out:
	dofreemem(ostrcopy);
	if (!gotmatch && (retval >= 0))
		goto nowild;
	return (retval < 0) ? retval : 0;
}

#ifdef notdef
_wild(fname, wild)
register char *fname, *wild;
{
	while (*wild)
	{
		if (*wild == '?')
		{
			wild++;
			if (*fname)
				fname++;
			else
				return 0;
		}
		else
		{
			if (*wild == '*')
			{
				wild++;
				if (!*wild)
					return 1;	/* Matched */
				while (*fname)
				{
					if ((*wildcomp)(fname, wild))
						return 1;
					if (*fname)
						fname++;
					else
						return 0;
				}
				return 0;
			}
			else
			{
				if (((miscmodes & OPM_CASESENSITIVE) ?
					*fname++ : toupper(*fname++)) != *wild++)
					return 0;
			}
		}
	}
	return (*fname == *wild);
}
#endif
