/* TABS4 NONDOC */

#include <syscalls.h>

extern char dn_sa[], dn_sb[];

z_setio(is_sb)
{
	int fd;
	char *name;

	fd = OPEN(name = (is_sb ? dn_sb : dn_sa), 2);
	if (fd < 0)
		return cantopen(name, fd);
	SET_SIP(fd);
	SET_SOP(fd);
	return 0;
}

/*
 * Remotely boot.
 * Run the program 'boot1616' on the host, and assume
 * that it will start a zmodem send.  When it's
 * completed, run "boot1616".
 */

remote_boot(is_sb, argc, argv)
char *argv;
{
	int ret;
	static char booter[] = "boot1616";
	int i, o;

	i = SET_SIP(-1);
	o = SET_SOP(-1);
	ret = z_get_put(is_sb, 0, booter, argc, argv);
	SET_SOP(o);
	SET_SIP(i);
	if (ret >= 0)
		ret = EXEC(booter);
	return ret;
}

Zslowput(c)
{
	PUTCHAR(c);
	SLEEP(1);
}

void
SLOWprintf(fmt)
{
	SPR(&fmt, Zslowput, 0);
}

/*
 * Get/put named files to/from a serial port
 * Could do fancy things with directory paths,
 * etc.  Instead we just run "sz1616" or "rz1616" on the other end.
 * echo all args to the Unix end for any option
 * processing
 */

int
z_get_put(is_sb, is_put, command, argc, argv)
char *command;
char *argv[];
{
	int ret = 0;

	if (is_put)
	{	/* Check that all files are there */
		int i, fd;

		for (i = 1; i < argc; i++)
		{
			if ((fd = OPEN(argv[i], 1)) < 0)
				cantopen(argv[i], ret = fd);
			else
				CLOSE(fd);
		}
		if (ret)
			EXIT(ret);
	}

	if ((ret = z_setio(is_sb)) >= 0)
	{
		int retry;

		for (retry = 0; retry < 5; retry++)
		{
			int i, tm;

			SLOWprintf("\r%s", command);
			for (i = 1; i < argc; i++)
				SLOWprintf(" %s", argv[i]);
			SLOWprintf("\r\n");
			for (tm = 0; tm < 25; tm++)
			{
				while (SGETCHAR())
				{
					if (GETCHAR() == 'X')
						goto cont;
				}
				SLEEP(1);
			}
		}

		eprintf("%s: Cannot sync with host\r\n", argv[0]);
		EXIT(-1);
cont:
		/* Now fetch them in */
		if (is_put)
		{
			argv[0] = "putz";
			ret = EXECA(argv);
		}
		else
		{
			ret = EXEC("getz");
		}
	}
	return ret;
}
