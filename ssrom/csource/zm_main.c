/* TABS4 NONDOC */

#include <types.h>
#include <syscalls.h>
#include <chario.h>
#include <scc.h>
#include "zmodem.h"

extern char	Attn;

int	Zifd, Zofd;
char	*Ziobuf;
int	tryzhdrtype;
char	*ZPathname;
char	Z_is_rz;
int	Zrawmode_save;
struct	scc_prog Zscc_prog;
char	needs_new_line;
char	Zisinteractive;

extern int	Zlastsent;

#define BSIZE 128
unsigned char 	*sl_buf;
long		sl_bindex;

unsigned char	*in_buf;
long		in_index, in_count;

void
sl_bflush(void)
{
	if (sl_bindex)
	{
		WRITE(Zofd, sl_buf, sl_bindex);
		sl_bindex = 0;
	}
}

void
sendline(char c)
{
	sl_buf[sl_bindex++] = c;
	if (sl_bindex >= BSIZE)
		sl_bflush();
}

/*
 * timeout is in tenths of seconds
 */

int
readline(time)
int time;
{
	int c, nbytes_there;

	if (in_count)
	{
		c = in_buf[in_index++];
		in_count--;
	}
	else if (nbytes_there = SGETC(Zifd))
	{
		if (nbytes_there == 1)
			c = GETC(Zifd);
		else
		{
			if (nbytes_there > BSIZE)
				nbytes_there = BSIZE;
			in_count = READ(Zifd, in_buf, nbytes_there);
			if (in_count != nbytes_there)
			{
				AMprintf("readline(): READ returns 0x%x", in_count);
				bibi();
			}
			c = in_buf[0];
			in_index = 1;
			in_count--;
		}
	}	
	else
	{
		/* OK, start timing down */
		time *= 5;
		time += GET_TICKS();

		while (SGETC(Zifd) == 0)
		{
			if (time < GET_TICKS())
				return TIMEOUT;
			SLEEP(0);
		}
		c = GETC(Zifd);
	}
	return c;
}

void
purgeline()
{
	while (SGETC(Zifd))
		GETC(Zifd);
}


/* called by signal interrupt or terminate to clean things up */
bibi(n)
{
	if (Z_is_rz)
		zmputs(Attn);
	canit();
	AMprintf("Caught signal %d; exitting", n);
	Zexit(1);
}

Zexit(code)
{
	ZgoUnraw();
	/* Flush input */
	SLEEP(50);
	while (SGETC(Zifd))
	{
		while (SGETC(Zifd))
		{
			GETC(Zifd);
		}
		SLEEP(10);
	}
	EXIT(code ? -1 : 0);
}

AMprintf(str)
char *str;
{
	char c;

	if (!Zisinteractive)
		return;

	if ((c = *str) == '\001')
	{	/* It needs a \r */
		str++;
		needs_new_line = 1;
	}
	else
	{
		if (needs_new_line)
			FPRINTF(STDERR, "\r\n");
		needs_new_line = 0;
	}
	_doprint(STDERR, &str);
	if (*str)
		FPRINTF(STDERR, (c == '\001') ? "\r":"\r\n");
}

/* send cancel string to get the other end to shut up */
canit()
{
	static char canistr[] = { 24,24,24,24,24,24,24,24,24,24,8,8,8,8,8,8,8,8,8,8,0 };

	zmputs(canistr);
}

ZgoRaw()
{
	struct scc_prog _prog;

	PUTCHAR('\r');							/* This may get trashed... */
	CDMISC(Zifd, CDM_TXFLUSH, 0, 0, 0);		/* Wait for output to drain */
	SLEEP(25);
	Zrawmode_save = CDMISC(Zifd, CDM_READRAWMODE, 0, 0, 0);
	CDMISC(Zifd, CDM_SETRAWMODE, 1, 0, 0);
	CDMISC(Zifd, CDM_READMODE, &Zscc_prog, 0, 0);
	CDMISC(Zifd, CDM_READMODE, &_prog, 0, 0);
	_prog.rxbits = 3;
	_prog.txbits = 3;
	CDMISC(Zifd, CDM_SETMODE, &_prog, 0, 0);
}

ZgoUnraw()
{
	PUTCHAR('\r');							/* This may get trashed... */
	CDMISC(Zifd, CDM_TXFLUSH, 0, 0, 0);		/* Wait for output to drain */
	SLEEP(25);
	CDMISC(Zifd, CDM_SETRAWMODE, Zrawmode_save, 0, 0);
	CDMISC(Zifd, CDM_SETMODE, &Zscc_prog, 0, 0);
}
	
int
zm_main(int is_rz, int argc, char *argv)
{
	extern char dn_null;
	int ret;

	Ziobuf = (char *)getmem0(KSIZE);
	sl_buf = (unsigned char *)getmem0(BSIZE);
	sl_bindex = 0;
	in_buf = (unsigned char *)getmem0(BSIZE);
	in_count = 0;
	tryzhdrtype = ZRINIT;
	ZPathname = 0;
	Zlastsent = 0;
	Zifd = SET_SOP(-1);	/* set modem in as the same as stdin */
	Zofd = SET_SOP(-1);	/* set modem out as the same as stdin */

	ZgoRaw();
	SIGCATCH(bibi);
	needs_new_line = 0;
	Zisinteractive = ISINTERACTIVE(GETPID());

	if (SET_SER(-1) == Zofd)
		SET_SER(OPEN(dn_null, 2));
	if (Z_is_rz = is_rz)
		ret = rz_main(argc, argv);
	else
		ret = sz_main(argc, argv);
	AMprintf("");
	ZgoUnraw();
	EXIT(ret);
}
