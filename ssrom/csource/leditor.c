/* TABS4
 * Line editor.
 */
#include <string.h>
#include <types.h>
#include <syscalls.h>

#include <files.h>
#include <chario.h>
#include <blkerrcodes.h>
//#include <splfns.h>

#include "mondefs.h"
#include "leditor.h"

#define CTL(c)	((c) - 0x40)
#define _EOF	-256		/* Something wierd */

/* Editor externals */
struct ledthing
{
	ushort cursor;		/* Where the cursor is in the string */
	uchar *newbuf;		/* Pointer to new string */
	uchar *oldbuf;		/* Pointer to last displayed string */
	ushort cursscrnpos;	/* Where the lt->cursor is on the screen */
	ushort oldlen;		/* Length of previously displayed string */
	ushort len;
	int outfd;
};

/* Number of last lines */
int nlastlines;

EXTERN struct chardriver *chardrivers;
EXTERN int syssafe;

extern char *skipwhite(), *index(), *room1for();
extern uchar *rev_fnw(), *fwd_fnw();
extern uchar *room0for(), *getzmem0();

/* Initialise the line editor stuff */

ledit_init()
{
	/* In case anyone uses return from ledit as pointer without checking */
	*(long *)0 = '\0';
}

/*
 * System call 86: Old line editor. Length = MAXIPLINE
 */

_ledit(lbuf)
char *lbuf;
{
	return NLEDIT(lbuf, MAXIPLINE);
}

/*
 * System call 84: Edit the line at *lbuf. Return pointer to buffer.
 * Max line length is passed
 */

_nledit(lbuf, length)
{
	return FNLEDIT(lbuf, length, STDIN, STDOUT);
}

/*
 * System call 142: lowest level line editor
 */

_fnledit(lbuf, length, infd, outfd)
register uchar *lbuf;
int length;
{
	register int ch;
	register int i;
	register uint llindex;	/* Index into lastlines */
	register int nllm1;		/* Number of lastlines - 1 */
	register struct ledthing *lt;
	register uchar **lastlines;
	uchar arrow;
	uchar ***plastlines;
	uchar *ob;		/* Copy of previously displayed string */
	register uchar *pp;
	int nxtch = 0;
	int olen;
	struct ledthing _lt;

	nllm1 = nlastlines - 1;
	fixfd(&infd);
	lastlines = (uchar **)0;
	syssafe++;
	if (infd < NIODRIVERS)
	{
		plastlines = (uchar ***)&chardrivers[infd].lastlines;
		lastlines = *plastlines;
		if (!lastlines)
		{
			*plastlines = lastlines =
				(uchar **)getzmem1(nlastlines * sizeof(*lastlines));
		}
	}
	syssafe--;

	lt = &_lt;
	if (length > MAXIPLINE)
	length = MAXIPLINE;
	ob = (uchar *)getmem0(MAXIPLINE);

	*ob = 0;
	llindex = nllm1;
	lt->newbuf = lbuf;
	lt->oldbuf = ob;
	lt->len = strlen(lbuf);
	lt->cursor = 0;
	lt->oldlen = 0;
	lt->cursscrnpos = 0;
	lt->outfd = outfd;

	redisp(lt);
leditloop:
	if (nxtch)
	{
		ch = nxtch;
		nxtch = 0;
	}
	else
		ch = rawgetc(infd);

	if (ch < 0)
	{
		/*
		 * rawgetchar only returns error at end of disk file,
		 * etc, so terminate the line, or quit if line empty
		 */
		if (lt->len == 0)
			ch = _EOF;
		else
			ch = '\r';
	}
	else
	{	/* Is it ^D on an empty line on char device? */
		if ((infd < NIODRIVERS) && !chardrivers[infd].rawmode &&
		    (ch == CDMISC(infd, CDM_READEOFCHAR,0,0,0)) &&
				!lt->len)
		{	/* ^D on empty line: quit */
			ch = _EOF;
		}
	}

	/* Is it time to abort? */
	if (ch == _EOF)
		goto eof;	/* eofchar first entered */

	switch (ch)
	{
	case CTL('A'):	/* ^A Backward word */
		if (lt->cursor)
		{
			pp = rev_fnw(lbuf + lt->cursor, lbuf);
			lt->cursor = pp - lbuf;
		}
		break;
	case CTL('B'):	/* ^B Goto end */
		lt->cursor = lt->cursor ? 0 : lt->len;
		break;
	case CTL('D'):	/* ^D Forward char */
		if (lt->cursor < lt->len)
			lt->cursor++;
		break;
	case '\033':	/* ESC - search back for history */
		olen = lt->len;
		for (i = nlastlines - 1; i > 0; i--)
		{
			syssafe++;
			if (lastlines && *plastlines && lastlines[i])
			{
				if (!strncmp(lbuf, lastlines[i], olen))
				{
					int ch2;
					strcpy(lbuf, lastlines[i]);
					getlen(lt);
					lt->cursor = lt->len;
					syssafe--;
					redisp(lt);
					ch2 = rawgetc(infd);
					syssafe++;
					if (ch2 != '\033')
					{
						nxtch = ch2;
						syssafe--;
						goto got;
					}
				}
			}
			syssafe--;
		}
got:
		break;
	case CTL('I'):		/* Filename completion */
		{
			uchar *dirname;
			uchar *filename;
			struct dir_entry *dirent = 0;
			int nfilenames = 0;
			int ndirentries;
			int namelen;
			int cursfwd;		/* Dist to move cursor to get to end */
			uchar *cp, *ocp, *cp3;

			cp = lbuf + lt->cursor;
			while (cp > lbuf && !iswhite(*cp))
				cp--;
			if (iswhite(*cp))
				cp++;
			dirname = (uchar *)getmem0(length + 4);
			ocp = dirname;
			while (*cp && !iswhite(*cp))
				*ocp++ = *cp++;
			*ocp = '\0';
			cursfwd = cp - (lbuf + lt->cursor);

			/* Split up the directory and filename bit */
			if (!*dirname)
			{	/* No directories specified */
				filename = (uchar *)getmem0(length + 4);
				strcpy(filename, dirname);
				strcpy(dirname, ".");
			}
			else

			/* dirname == "" or aaa or aaa/bbb or /path/aa */

			if (*dirname && *dirname != '/' && index(dirname, '/'))
			{	/* aaa/bbb or aaa/bbb/ccc, etc */
				filename = room0for(splitpath(dirname));
			}
			else if (*dirname == '/' && index(dirname + 1, '/'))
			{	/* /path/xxx */
				filename = room0for(splitpath(dirname));
			}
			else if (!index(dirname, (int) "/"))
			{
				filename = room0for(dirname);
				strcpy(dirname, ".");
			}
			else if (*dirname == 0)
			{
				strcpy(dirname, ".");
				filename = getzmem0(2);
			}

			if (lastchar(dirname) == '/')
				dirname[strlen(dirname) - 1] = 0;

			namelen = strlen(filename);

			/* Read all files */
			dirent = (struct dir_entry *)RDALLDIR(dirname, 0, 2, &ndirentries);
			if ((int)dirent > 0)
			{
				/* Count all matching filenames */
				nfilenames = 0;
				{
					int ii;
					for (ii = 0; ii < ndirentries; ii++)
					{
						cp3 = (uchar *)dirent[ii].file_name;
						if (*cp3)
						{
							if (*filename == '\0' ||
									!strncmp(filename, cp3, namelen))
								nfilenames++;
							else
								*cp3 = 0;
						}
					}
				}

				if (nfilenames)
				{
					/* Now start matching */
					int fnindex = 0;
					int ii;
					int ch2;
					int olen = lt->cursor;

					for ( ; ; )
					{
						cp3 = (uchar *)dirent[fnindex].file_name;
						if (*cp3)
						{
							int scp3 = strlen(cp3);

							/* Insert the filename */
							/* 4.7 fix for inserting at the wrong place */
							lt->cursor += cursfwd;
							for (ii = namelen; ii < scp3; ii++)
								insch(lt, cp3[ii], length);

							/* Now restore original cursor offset */
/*							lt->cursor = olen;*/
/*							lt->cursor -= (scp3 - namelen) + cursfwd; */
							getlen(lt);
							redisp(lt);
							ch2 = rawgetc(infd);
							if (ch2 != CTL('I'))
							{
								if (ch2 == '\033')
								{	/* Restore old buffer */
									lt->cursor = olen;
									for (ii = namelen; ii < strlen(cp3); ii++)
										remch(lt);
								}
								else
									nxtch = ch2;
								goto allgot;
							}
							lt->cursor = olen;
							for (ii = namelen; ii < strlen(cp3); ii++)
								remch(lt);
						}
						fnindex++;
						if (fnindex >= ndirentries)
							fnindex = 0;
					}
				}
allgot:			;
				dofreemem(dirent);
			}
			dofreemem(filename);
			dofreemem(dirname);
			break;
		}

	case CTL('O'):	/* Cycle back through history one word at a time */
	case CTL('C'):	/* Match partial word with history */
		{
			int llindex = nlastlines;
			int cindex;
			uchar *pattern;
			int patlen;
			char iscc = (ch == CTL('C'));

			if (iscc)
			{
				cindex = lt->cursor;
				pattern = lt->newbuf;
				/* Find start of word to match */
				while (cindex && (!iswhite(pattern[--cindex])))
					;
				/* cindex == 0 || iswhite(patterncindex) */
				if (cindex)
					pattern += cindex + 1;
				patlen = lt->cursor - (pattern - lt->newbuf);
			}

			cindex = 0;
			for ( ; ; )
			{
				syssafe++;		/* Nail down the lastlines */
				if (!lastlines || !*plastlines)
				{
					syssafe--;
					break;
				}
				if (cindex < 1)
				{
					if (llindex < 1 || !(pp = lastlines[--llindex]))
					{	/* No more lines to look at */
						syssafe--;
						break;
					}
					cindex = strlen(pp);
				}

				/* Back up to a non-white char: the end of a word */
				while (cindex && iswhite(pp[--cindex]))
						;
				if (iscc)
				{	/* Back up to start of word */
					while (cindex && !iswhite(pp[--cindex]))
						;
					if (cindex)
						cindex++;	/* Skip leading blank */
				}

				if (cindex > 0 || iscc)
				{	/* On the last char of a word */
					int ninserted = 0;
					uchar ch2;

					if (iscc)
					{	/* cindex == 0 || iswhite(ppcindex - 1) */
						int ci2;

						if (strncmp(pp + cindex, pattern, patlen))
						{	/* No match: back up to white char */
							if (cindex)
								cindex--;
							goto cont;
						}
						ci2 = cindex + patlen;
						do
						{
							insch(lt, pp[ci2], length);
							ninserted++;
						}
						while (pp[++ci2] && !iswhite(pp[ci2]));
					}
					else
					{
						do
						{
							insch(lt, pp[cindex], length);
							lt->cursor--;		/* Insert in reverse order */
							ninserted++;
						}
						while (cindex && !iswhite(pp[--cindex]));
						lt->cursor += ninserted;
					}
					syssafe--;		/* Don't need last lines */
					redisp(lt);
					ch2 = rawgetc(infd);
					if (ch != ch2)
					{
						nxtch = ch2;
						goto gotO;		/* Exit point */
					}
					/* Another ^O/^C: Delete inserted word */
					lt->cursor -= ninserted;
					while (ninserted--)
						remch(lt);
				}	/* If cindex > 0 */
				else
				{
cont:
					syssafe--;
				}
			}	/* Next word/line */
		}
gotO:
		break;
	case CTL('W'):	/* ^W */
		arrow = 2;
		goto udarrow;
	case CTL('E'):	/* ^E Previous line */
		arrow = 1;		/* Flag: up arrow */
		goto udarrow;
	case CTL('F'):	/* ^F Forward word */
		if (lt->cursor != lt->len)
		{
			pp = fwd_fnw(lbuf + lt->cursor, 1);
			lt->cursor = pp - lbuf;
		}
		break;
	case CTL('G'):	/* ^G, DEL Delete char forward */
	case 0x7f:
		remch(lt);
		getlen(lt);
		break;
	case CTL('H'):	/* ^H, Delete char backward */
		if (lt->cursor)
		{
			lt->cursor--;
			remch(lt);
			lt->len--;
		}
		break;
	case CTL('J'):	/* Possibly end of line during redirection */
		break;
	case CTL('S'):	/* ^S Backward char */
	case CTL('Z'):	/* ^Z Backward char */
		if (lt->cursor)
		{
			lt->cursor--;
			redisp(lt);
		}
		break;
	case CTL('T'):	/* ^T Delete word forward */
		pp = fwd_fnw(lbuf + lt->cursor, 1);
		i = pp - (lbuf + lt->cursor);
		while (i--)
			remch(lt);
		getlen(lt);
		break;
	case CTL('V'):	/* ^V Delete line backward */
		strcpy(lbuf, lbuf + lt->cursor);
		lt->cursor = 0;
		getlen(lt);
		break;
	case CTL('X'):	/* ^X Previous line recall */
		arrow = 0;
/*
 * If arrow == 0, scroll forward.
 * If arrow == 1, scroll to previous.
 * If arrow == 2, insert previous line.
 */
udarrow:
		if (!lastlines || !*plastlines)
			break;
		syssafe++;
		{
			if (arrow != 2)
				lbuf[lt->cursor = lt->len = 0] = '\0';
			if ((!arrow) && (++llindex >= nlastlines))
				llindex = 0;		/* Wrap */
			i = 0;
			if (lastlines[llindex])
			{
			  while (ch = lastlines[llindex][i++]) /*while (ch = lastlinesllindexi++)*/
					insch(lt, ch, length);
			}
			if (arrow && (--llindex >= nlastlines))
				llindex = nllm1;
		}
		syssafe--;
		break;
	case CTL('Y'):	/* ^Y Delete line forward */
		lbuf[lt->cursor] = '\0';
		lt->len = lt->cursor;
		break;
	case '\r':	/* Terminator */
eof:
		lt->cursor = lt->len;
		redisp(lt);	/* Move lt->cursor to end */
		if (lastlines && *plastlines)
		{
			/* Don't copy empty or same string */
			if (lt->len && (!lastlines[nllm1] ||
				strcmp(lbuf, lastlines[nllm1])))
			{
				maybefree(lastlines[0]);
				movmem(lastlines + 1, lastlines,
					(nllm1) * sizeof(*lastlines));
				lastlines[nllm1] = (uchar *)room1for(lbuf);
			}
		}
		FPRINTF(outfd, "\r\n");
		dofreemem(ob);
		return (ch == _EOF) ? 0 : (int)lbuf;
	case CTL('P'):	/* ^P Insert control char */
		ch = rawgetc(infd);
	default:	/* Normal char */
		insch(lt, ch, length);
		break;
	}
	redisp(lt);
	goto leditloop;
}

/*
 * Redisplay buffer
 */

void redisp(struct ledthing *lt)
{
	register ushort cp;		/* Current buffer position */
	register ushort newspos;	/* Current screen position */
	register uchar pe;		/* Print enable */
	register uchar ch;		/* Current char */
	int outfd;
	ushort newcurspos;	/* New lt->cursor screen position */
	unsigned olsave;		/* Temp save for lt->oldlen */

	newspos = 0;
	cp = 0;
	pe = 0;
	outfd = lt->outfd;

/* Scan the whole new buffer */
	do
	{
		ch = lt->newbuf[cp];

		if (newspos == lt->cursscrnpos)
			pe = 1;	/* Start printing when the lt->cursor is picked up */

		if (!pe && (ch != lt->oldbuf[cp]))
		{	/* Move the lt->cursor back to where they differ */
			while (lt->cursscrnpos > newspos)
			{
				PUTC(outfd, '\b');
				--(lt->cursscrnpos);
			}
			pe = 1;
		}

		if (cp == lt->cursor)
			newcurspos = newspos;	/* New lt->cursor position */

		if (ch == '\t')
		{
			do
				if (pe)
					PUTC(outfd, ' ');
			while (++newspos & 7);
		}
		else
		{
			if (ch)
			{
				if (ch < 0x20)
				{	/* Control char */
					if (pe)
					{
						PUTC(outfd, '^');
						PUTC(outfd, ch | 0x40);
					}
					newspos += 2;
				}
				else
				{
					if (pe)
						PUTC(outfd, ch);
					newspos++;
				}
			}
		}

		lt->oldbuf[cp] = ch;
		cp++;
	}
	while (ch);

	olsave = newspos;

/* New string is out. Print any necessary spaces */
	while (newspos < lt->oldlen)
	{
		PUTC(outfd, ' ');
		newspos++;
	}

/* Back up to correct lt->cursor position */
	while (newspos > newcurspos)
	{
		PUTC(outfd, '\b');
		--newspos;
	}

	lt->oldlen = olsave;
	lt->cursscrnpos = newspos;
}

/*
 * Remove the char at the lt->cursor
 */

remch(lt)
register struct ledthing *lt;
{
	if (lt->newbuf[lt->cursor])
		strcpy(lt->newbuf + lt->cursor, lt->newbuf + lt->cursor + 1);
}

/*
 * Insert a char in the buffer
 */

insch(lt, ch, length)
register struct ledthing *lt;
char ch;
{
	register uchar *ptr;

	if (lt->len < (length - 1))
	{
		ptr = lt->newbuf + lt->cursor++;
		movmem(ptr, ptr + 1, strlen(ptr) + 1);
		*ptr = ch;
		lt->len++;
	}
}

/*
 * Restore len
 */

getlen(lt)
struct ledthing *lt;
{
	lt->len = strlen(lt->newbuf);
}

/*
 * Interpret and strip off any I/O redirection commands. Open any files
 */

EXTERN uint stdin, stdout, stderr;	/* The numbers of the standard drivers */

interpio(lb)
register struct led_buf *lb;
{
	register char *ptr;
	register int dev, inquote;
	char *gtptr, *ltptr, *sqptr;

/* Truncate any comments */
	ptr = lb->buf;
	inquote = 0;
	while (*ptr)
	{
		if (*ptr == '"')
			inquote ^= 1;
		if (!inquote && (*ptr == ';'))
		{
			*ptr = '\0';	/* Truncate */
			break;
		}
		ptr++;
	}

	ptr = lb->buf;

	lb->odev = stdout;
	lb->edev = stderr;
	lb->idev = stdin;

/* Standard output redirection */
	if ((dev = getredir(ptr, '>', &gtptr)) < 0)
		return dev;	/* Output failure */
	lb->odev = dev;

/* Standard error redirection */
	if ((dev = getredir(ptr, '}', &sqptr)) < 0)
	{
		CLOSE(lb->odev);
		return dev;	/* Output failure */
	}
	lb->edev = dev;

/* Standard input redirection */
	if ((dev = getredir(ptr, '<', &ltptr)) < 0)
	{
		CLOSE(lb->odev);
		CLOSE(lb->edev);
		return dev;	/* Input failure */
	}
	lb->idev = dev;

	if (gtptr)
		*gtptr = '\0';		/* Strip off redirections */
	if (ltptr)
		*ltptr = '\0';
	if (sqptr)
		*sqptr = '\0';
	return 0;	/* All ok */
}

/*
 * Search for and interpret a redirection and return:
 * 1000 if no redirection
 * Error code if redirection and it failed
 * fd if to/from a char device or file
 */

getredir(buf, ch, gtltptr)
char *buf;
register char ch;
char **gtltptr;
{
	register char *ptr, *ptr2, c;
	register int dvr, appending;
	int inquote;
	char devbuf[MAXIPLINE];

	*gtltptr = (char *)0;		/* Assume not found */

	ptr = buf;
	inquote = 0;
	while (((c = *ptr++) != ch) || inquote)
	{
		if (c == '"')
			inquote ^= 1;
		if (!c)
			return 1000;
	}

	*gtltptr = ptr - 1;	/* Set up pointer to the '>' or '<' char */

	appending = 0;
	if (((ch == '>') && (*ptr == '>')) ||
	    ((ch == '}') && (*ptr == '}')))
	{	/* appending >> or }} */
		appending = 1;
		ptr++;
	}

	ptr = skipwhite(ptr);
	ptr2 = devbuf;
	while (*ptr && !index("\t \"<>}", *ptr))
		*ptr2++ = *ptr++;
	*ptr2 = '\0';
	if (ch == '<')
	{
		if ((dvr = OPEN(devbuf, SSO_RDONLY)) < 0)
			cantopen(devbuf, dvr);
	}
	else
	{
		if (appending)
		{
			if ((dvr = OPEN(devbuf, SSO_APPEND)) < 0)
				cantopen(devbuf, dvr);
		}
		else if ((dvr = CREAT(devbuf, 0, 0)) < 0)
			cantcreat(devbuf, dvr);
	}
	return dvr;
}
