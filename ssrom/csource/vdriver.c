/* TABS4 NONDOC
 * Video driver
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
#include <via.h>
#include <storedef.h>

#include <chario.h>
//#include <splfns.h>
#include "windows.h"

#define CTL(c)	(c - '@')	/* Control char */

#define NROWS		25
#define NCOLS		80
#define CHARSETSIZE	256	/* The number of printable chars supported */

ushort defchtab[8*CHARSETSIZE];	/* Character images */
ushort curssave[16];
char curschanged;		/* True if cursor changed on xoff */

int wclr_eol(), build_chtab(), inv_char();

struct window in_wind;

/* The mouse support vectors */
int (*mt_pcvec)();	/* Called before a char is written */
int (*mt_acvec)();	/* Called after a char is written */
int (*mt_off)();	/* Remove mouse cursor */
int (*mt_on)();		/* Turn it back on */

int pixelbyte();

/*
 * The video context starts here!
 */

ushort
	mode_640,		/* If true, in 640 mode */
	curs_rate,		/* Flash speed */
	thisfg,
	thisbg,
	cur_vap,		/* The current access page */
	cur_vdp,		/* The current display page */
	vstate,			/* Terminal emulation state */
	esc1save,		/* Save for second char in ESC sequence */
	esc2save,		/* Save for third char in ESC sequence */
	blkcurs,		/* Block cursor flag */
	waswrap,		/* Flag for suppressing newline after wrap */
	insertmode,		/* Character insert mode */
/* A copy of the current window definitions */
	x_start,
	y_start,
	x_end,
	y_end,
	bg_col,
	fg_col,
	curs_x,
	curs_y,
	s_bg_col,		/* Save locations */
	s_fg_col,
	s_curs_x,		/* If s_curs_x == 0xffff, position non poppable */
	s_curs_y,
	x_size,
	y_size,
	curpmode,		/* Current graphics plot mode */
	gfgcol,			/* Graphics foreground colour */
	ogfgcol;		/* User's foreground colour */

int (*plotfunc)();		/* Pointer to our current graphics dot function */
int vidomode;			/* If true, no esc or control processing is done */
uint underline;			/* Current underline mask */
uint g_xsize, g_ysize;
int g_xstart, g_ystart;
struct window *cur_window;	/* Pointer to current window structure */
ushort *vidram;			/* Pointer to start of video access RAM */
ushort *chtab;
ushort curs_mask[8];		/* The flashing mask */
int curs_enable;		/* Turn cursor on/off */


/* END of video driver context */

/*
 * A table of pointers for saving the video context.
 */

ushort *shortptrs[] = {
	&mode_640,
	&curs_rate,
	&thisfg,
	&thisbg,
	&cur_vap,
	&cur_vdp,
	&vstate,
	&esc1save,
	&esc2save,
	&blkcurs,
	&waswrap,
	&insertmode,
	&x_start,
	&y_start,
	&x_end,
	&y_end,
	&bg_col,
	&fg_col,
	&curs_x,
	&curs_y,
	&s_bg_col,
	&s_fg_col,
	&s_curs_x,
	&s_curs_y,
	&x_size,
	&y_size,
	&curpmode,
	&gfgcol,
	&ogfgcol,
	palval, palval + 1, palval + 2, palval + 3,
	0
	};

int *longptrs[] = {
	&curs_enable,
	(int *)&plotfunc,
	&vidomode,
	(int *)&underline,
	(int *)&g_xsize,
	(int *)&g_ysize,
	&g_xstart,
	&g_ystart,
	(int *)&cur_window,
	(int *)&vidram,
	(int *)&chtab,
	0
	};

ushort hcurs_x, hcurs_y;	/* Cursor flash location */
ushort curs_stat;		/* 1 = cursor currently inverted */
ushort curs_count;		/* Flash counter */
ushort ourcurs;			/* Hardware cursor follows output cursor */
ushort hcur_vdp;		/* Copy of current cur_vdp */
ushort hmode_640;		/* Copy of current hardware setting */
ushort *hvidram;		/* Video RAM pointer for cursor */

/*
 * Video control flag vidomode. If bit 0 set, no ESC processing is done. If bit 1
 * is set, no control char processing is done
 */
#define VO_NOESC	1	/* Ignore all esc sequences */
#define VO_NOCONTROL	2	/* Ignore all control chars */
#define VO_TVI925	4	/* Emulate 925 closer */
/* V4.7 */
#define VO_ONLCR	8	/* Map NL to CR-NL on output */

/* Masks for underline */
#define UNDERLINE		(unsigned)0x80000000
#define BOLD		0x40000000
#define ITALICS		0x20000000
#define SUBSCRIPT	0x10000000
#define SUPERSCRIPT	0x08000000

/* Buffers for adrawvch */
ushort subbuffer[16];
ushort itbuffer[8];
ushort boldbuf[8];

/* The initial graphics window */
struct window first_wind =
	{
		0,	/* x_start */
		0,	/* y_start */
		NCOLS,	/* x_end */
		NROWS,	/* y_end */
		0,	/* bg_col */
		0xffff,	/* fg_col */
		0, 	/* curs_x */
		0	/* curs_y */
	};

extern uchar charset[];
EXTERN int obramstart;		/* Start of on-board RAM */
EXTERN int syssafe;		/* Process lock-out semaphore */
EXTERN struct chardriver *chardrivers;
EXTERN int switchpending;

/*
 * Set up the video display in 80 columns. The VIA must have been initialised.
 * The screen buffer is at the top of on-board RAM
 */

video_init(reslevel)
{
	int curs_flash();
	struct window dummywind;

	CRTC_INIT(2, 0);
	mt_pcvec = 0;
	mt_acvec = 0;
	mt_on = 0;
	mt_off = 0;
	vstate = 0;
	curs_enable = 0;
	curs_stat = 0;
	curs_count = 0;
	curschanged = 0;
	underline = 0;
	vidomode = 0;
	insertmode = 0;
	waswrap = 0;
	hcurs_x = 0;
	hcurs_y = 0;
	ourcurs = 1;
	s_curs_x = 0xffff;

	SET_640(1);
	chtab = defchtab;

	SET_VAP(15);
	hvidram = vidram;
	SET_VDP(15);

	normcurs(12);

/* Set up display parameters */
	cur_window = &dummywind;	/* To keep save_cwind happy */
	if (reslevel != 3)
		DEF_WIND(0);

	blkcurs = 0;				/* This was in the next block */

	if (reslevel < 2)
		build_chtab();

	if (reslevel < 4)
		SET_VSVEC(curs_flash, 1, 0);
}

/*
 * Set the border & pallette values based on the passed longword
 */

void set_colours(register int val)
{
	register short i;

	for (i = 0; i < 4; i++)
	{
		SET_PAL(i, val & 15);
		val >>= 4;
	}
	SET_BDCOL(val);
}

normcurs(rate)
{
	short buf[32];

	wsetmem(buf, 0xffff, sizeof(buf) / sizeof(*buf));
	SCURS_MODE(rate, 2, buf);
}

/*
 * System call 31: Set/clear 640 mode:
 * 0 = set 320
 * 1 = set 640
 * 2 = read mode
 */

_set_640(mode)
int mode;
{
	*V_DDRA |= EN640_MASK;
	switch (mode)
	{
	case 0:
		mode_640 = 0;
		*V_AREG &= ~EN640_MASK;
		break;
	case 1:
		mode_640 = 1;
		*V_AREG |= EN640_MASK;
		break;
	}
	if (ourcurs)
		hmode_640 = mode_640;
	setpmode();	/* Set the graphics pixel draw mode */
	calcgfgcol();	/* Recalculate graphics colour mask */
	return mode_640;
}

/*
 * System call 32: Set the display page. If 'page' == -1, return current
 */

_set_vdp(page)
register int page;
{
	register int s;

	if (page < 0)
		return cur_vdp;
	s = SCC_SPL();
	cur_vdp = (page &= 31);
	hcur_vdp = cur_vdp;
	curs_norm();
	vlval = ((vlval & 0xf0) | (page & 0x0f));	/* V4.5a */
	*VIDLATCH = vlval;
	_splx(s);
}

/*
 * System call 33: Set the video access page. If 'page' == -1, return current
 */

_set_vap(page)
register int page;
{
	int cesave;

	if (page < 0)
		return cur_vap;
	syssafe++;
	stop_curs(&cesave);
	cur_vap = (page &= 31);		/* 4.5a: Enable upper half meg */

	/* Calculate start of video RAM (base is at a multiple of 1 Mbyte) */
	vidram = (ushort *)((page << 15) | obramstart);
	if (ourcurs)
		hvidram = vidram;
	free_curs(&cesave);
	syssafe--;
}

/* System call 34: Set foreground texture/colour */
_set_fgcol(col)
int col;
{ fg_col = col; }

/* System call 35: Set background texture/colour */
_set_bgcol(col)
int col;
{ bg_col = col; }

/* System call 36: Set border colour */
_set_bdcol(col)
int col;
{
	int old;

	old = (vlval >> 4) & 15;
	vlval = ((vlval & 0xf) | (col << 4));
	*VIDLATCH = vlval;
	return old;
}

/* System call 37: Set a pallette entry */
_set_pal(pal_pos, col)
register int pal_pos;
int col;
{
	int old;

	old = palval[pal_pos];
	palval[pal_pos] = col;
	pal_pos = (pal_pos & 3) << 5;	/* Index into pallette array */
	PAL_0[pal_pos] = col;
	return old;
}

/* System call 38: Return pointer to a current char shape */
ushort *
_rdch_shape(chno)
uint chno;
{
	return &chtab[(chno & 0xff) << 3];
}

/* System call 39: Redefine a char shape */
_def_chshape(chno, defptr)
register uint chno;
char *defptr;
{
	if (chno == (uint)-1)
		return (int)charset;

	if (chno < CHARSETSIZE)
		movmem(defptr, &chtab[chno << 3], 16);
}

/* System call 40: Define a window. 0 = default. */
struct window *
_def_wind(wptr)
register struct window *wptr;
{
	int cesave;

	if (wptr == (struct window *)1)
	{	/* Return pointer to current window definition */
		save_cwind();
	}
	else
	{
		syssafe++;
		stop_curs(&cesave);
		if (!wptr)
		{
			wptr = &in_wind;
			movmem(&first_wind, wptr, sizeof(in_wind));
			if (!mode_640)
				in_wind.x_end = 40;
		}
		if (wptr != cur_window)
		{
			save_cwind();
			cur_window = wptr;
		}
		get_cwind();

		hcurs_x = curs_x + x_start;
		hcurs_y = curs_y + y_start;

		free_curs(&cesave);
		syssafe--;
	}
	return cur_window;
}

/* System call 41: Return address of the byte containing a pixel in video RAM */
_vid_address(x, y)
int x, y;
{ return pixelbyte(y, x); }

/*
 * System call 42: Move current window and a buffer area.
 * Mode = 0: swap window <-> buffer.
 * Mode = 1: move window --> buffer.
 * Mode = 2: move buffer --> window.
 */

_move_wind(buf, mode)
register char *buf;
int mode;
{
	register ushort offset;
	register ushort nwords, y;
	int cesave;
	char *vmovline();
					/* Set up for the swap */
	offset = x_start << 1; 		/* The horizontal offset */
	nwords = x_size;		/* Number of words on each line */
	if (!mode_640)
	{
		offset <<= 1;
		nwords <<= 1;
	}

	syssafe++;
	stop_curs(&cesave);
	mouseoff();
	for (y = y_start; y < y_end; y++)
	{
		buf = vmovline(offset + y * (NCOLS*4), buf, nwords, mode);
	}
	mouseon();
	free_curs(&cesave);
	syssafe--;
}

/* System call 44: Fill the current window in with a colour */
_fill_wind(col)
register int col;
{
	register ushort y;
	int cesave;

	syssafe++;
	if (ourcurs)
		stop_curs(&cesave);
	mouseoff();
	for (y = 0; y < y_size; y++)
		wclr_eol(y, 0, col);	/* Use clreol to fill it */
	mouseon();
	if (ourcurs)
		free_curs(&cesave);
	syssafe--;
}

/* System call 45: Set cursor mode. Only change if value non-zero.  */
_scurs_mode(rate, enable, mask)
register int rate;
register int enable;		/* 1 = off, 2 = on */
register ushort *mask;
{
	register int s;
	register int retval = 0;

	syssafe++;
	mouseoff();
	s = VIA_SPL();
	curs_norm();
	if (enable == 3)
		normcurs(12);		/* Default cursor */
	else
	{
		if (rate)
			curs_rate = rate;
		switch (enable)
		{
		case 1:
		case 2:
			curs_enable = enable - 1;
			blkcurs = 0;
			break;
		case 4:	retval = curs_rate;
			break;
		case 5:	retval = curs_enable;
			break;
		case 6:	retval = (int)curs_mask;
			break;
		case 7: blkcurs = 1;
			break;
		case 8: blkcurs = 0;
			break;
		case 9: retval = blkcurs;
			break;
		}

		if (mask)
			swapmem(mask, curs_mask, sizeof(curs_mask));
	}
	_splx(s);
	mouseon();
	syssafe--;
	return retval;
}

/* System call 130: new character table */
_newchset(ptr)
register ushort *ptr;
{ chtab = (ptr) ? ptr : defchtab; }

/*
 * Clear to eol function: fill from 'x' to the end of the current window with
 * 'col'
 */

wclr_eol(y, x, col)
ushort y;
register ushort x;
ushort col;
{
	register uint offset, nwords;

	offset = (x_start + x) << 1;
	nwords = x_size - x;
	if (!mode_640)
	{
		offset <<= 1;
		nwords <<= 1;	/* Chars are twice as wide */
	}
	offset += (y + y_start) * (NCOLS*4);
	vidsetmem(offset, nwords, col);
}

/*
 * Move line function: move the line in the current window at 'from' to 'to'
 */

wmov_line(from, to)
ushort from, to;
{
	register ushort foffset, toffset, nwords;

	nwords = x_size;
	if (!mode_640)
		nwords <<= 1;
	foffset = (y_start * (NCOLS*4)) + (x_start << (mode_640 ? 1 : 2) );
	toffset = foffset + to * (NCOLS*4);
	foffset += from * (NCOLS*4);
	vmove(foffset, toffset, nwords);
}

/*
 * Write chars in the current window from the supplied buffer. Stop
 * on ESC or if count expires.
 * Return number of chars written
 */

wind_putbuf(pbuf, count)
uchar *pbuf;
{
	register uint cvtemp;
	register uchar ch;
	register uint index;
	register uchar *buf;
	char fastdraw;

	index = 0;
	fastdraw = (!insertmode && !mt_pcvec && !mt_acvec && !(vidomode & VO_NOCONTROL));
	while (count > 0)
	{
		buf = pbuf + index;
		if (((ch = *buf) < ' ') && (!(vidomode & VO_NOCONTROL)))
		{	/* Control char */
			switch (ch)
			{
			case 0:		/* Can be a pad char */
				break;
			case '\033':	/* ESC */
				if (!(vidomode & VO_NOESC))
					return index;
				break;
			case '\b':
				waswrap = 0;
				if (curs_x == 0)
				{
					curs_x = x_size;
					if (curs_y != 0)
						curs_y--;
					else
						curs_x = 1;
				}
				curs_x--;
				break;
			case '\t':
				if (vidomode & VO_TVI925)
				{
					waswrap = 0;
					curs_x = ((curs_x + 8) & ~7);
					if (curs_x >= x_size)
						curs_x = 0;
				}
				else
					wind_putb2("        ", 8 - (curs_x & 7));
				break;
			case 0x07:
				beep();
				break;
			case CTL('K'):		/* Cursor up */
				if (curs_y)
					curs_y--;
				waswrap = 0;
				break;
			case CTL('V'):		/* Cursor down */
				if (curs_y >= (y_size - 1))
					break;
				waswrap = 0;
				/* Fall through to LF */
			case '\n':
				if (waswrap)
				{	/* Suppress newline after wrap */
					waswrap = 0;
					break;
				}
newline:
				if (++curs_y == y_size)
				{	/* Scroll up */
					curs_y--;
					if (mt_off)	/* Warn mouse driver */
						(*mt_off)();
					if (full_screen())
					{	/* Whole screen used: can fast scroll */
						scrollup(vidram, bg_col);
					}
					else
					{
						for (cvtemp=1; cvtemp<y_size; cvtemp++)
							wmov_line(cvtemp, cvtemp - 1);
						wclr_eol(curs_y, 0, bg_col);
					}
					if (mt_on)	/* Tell mouse driver it is done */
						(*mt_on)();
				}
				/* V4.7 */
				if (!(vidomode & VO_ONLCR))
					break;
			case '\r':
				curs_x = 0;
				break;
			case 12:	/* Form feed or cursor right */
				if (vidomode & VO_TVI925)
				{
					if (curs_x < (x_size - 1))
						curs_x++;
					break;
				}
				/* Else fall through to clear screen for old 1616 stuff */
			case CTL('Z'):	/* Control-Z */
				FILL_WIND(bg_col);
			case CTL('^'):		/* Home cursor */
				curs_x = curs_y = 0;
				waswrap = 0;
				break;
			default:	/* Unknown control char */
				goto pcout;
			}
			count--;
			index++;
		}
		else
		{
			if (fastdraw)
			{
				/* Work out how many simple chars we can send */
				cvtemp = x_size - curs_x;
				if (cvtemp > count)
					cvtemp = count;
				cvtemp = drawvchars(buf, cvtemp);
			}
			else
			{
pcout:
				if (insertmode)
					hscroll(1);

				/* Tell mouse driver we are about to draw a char */
				if (mt_pcvec)
					(*mt_pcvec)(curs_y + y_start, curs_x + x_start, ch);

				draw_vchar(curs_y + y_start, curs_x + x_start, ch,
					fg_col | underline, bg_col);

				if (mt_acvec)
					(*mt_acvec)();
				cvtemp = 1;
			}

			index += cvtemp;
			count -= cvtemp;
			curs_x += cvtemp;

			if (waswrap = (curs_x == x_size))
			{
				curs_x = 0;
				count++;			/* Adjust for increment above */
				index--;
				goto newline;		/* Do a LF */
			}
		}
		if (switchpending)
			break;
	}
	return index;
}

/* Call wind_putbuf() until all data sent */
wind_putb2(pbuf, count)
{
	int cnt;

	do
	{
		cnt = wind_putbuf(pbuf, count);
		count -= cnt;
		pbuf += cnt;
	}
	while (count > 0);
}

/* Video status routine: Always ready unless xoffed */
video_ostat()
{
	return !chardrivers[0].xoffed;
}

/*
 * Video character output routine
 */

video_putc(ch)
uchar ch;
{
	return video_mwrite(0, &ch, 1);
}

/*
 * Multichar output routine
 */

video_mwrite(fd, buf, nbytes, passval)
long fd;
register uchar *buf;
long nbytes, passval;
{
	register ushort i;
	register ushort ch;
	register struct chardriver *chardriver;
	register uchar *bufend;
	unsigned int from, to;
	int cesave;
	int cursorshapechanged;

	cursorshapechanged = 0;
	if (curschanged)
	{	/* Looks like interrupt during xoff: restore cursor */
		SCURS_MODE(0, 0, curssave);
		curschanged = 0;
	}

	chardriver = chardrivers + fd;
	chardriver->txcount += nbytes;

	bufend = buf + nbytes;

	while (buf < bufend)
	{
		/* Output suspended? */
		if (chardriver->xoffed)
		{
			/* Change cursor when stopped */
			clearmem(curssave, 14);		/* Zero it */
			wsetmem(curssave + 7, 0xffff, 9);
			SCURS_MODE(0, 0, curssave);	/* Set new cursor */
			curschanged = 1;
			while (chardriver->xoffed)
				SLEEP(1);
			SCURS_MODE(0, 0, curssave);	/* Restore old mask */
			curschanged = 0;
		}

		syssafe++;			/* Reentrancy protection */
		if (ourcurs)
			stop_curs(&cesave);
		ch = *buf;
		switch (vstate)
		{
		case 0:		/* Normal output */
			if ((ch == 0x1b) && !(vidomode & VO_NOESC))
			{
				buf++;
				vstate = 1;
			}
			else
				buf += wind_putbuf(buf, bufend - buf);
			break;
		case 1:		/* ESC received */
			buf++;
			switch (ch)
			{
			case 0x01:	/* Push cursor pos */
				s_curs_x = curs_x;
				s_curs_y = curs_y;
				s_fg_col = fg_col;
				s_bg_col = bg_col;
				goto c1done;
			case 0x02:	/* Pop cursor pos */
				if (s_curs_x != 0xffff)
				{
					curs_x = s_curs_x;
					curs_y = s_curs_y;
					fg_col = s_fg_col;
					bg_col = s_bg_col;
				}
				goto c1done;
			case 'T':	/* Clear to eol */
			case 't':	/* Clear to eol (?) */
			case 'Y':	/* Clear to end of display */
			case 'y':	/* Clear to end of display */
				mouseoff();
				wclr_eol(curs_y, curs_x, bg_col);
				if (ch == 'Y' || ch == 'y')
				{
					for (i = curs_y + 1; i < y_size; i++)
						wclr_eol(i, 0, bg_col);
				}
				mouseon();
				goto c1done;
			case 'R':	/* Delete line */
				i = curs_y;
				mouseoff();
				while (++i < y_size)
					wmov_line(i, i - 1);
				wclr_eol(y_size - 1, 0, bg_col);
				mouseon();
				goto c1done;
			case '*':	/* Clear screen */
				wind_putb2("\032", 1);
				goto c1done;
			case 'j':	/* Reverse scroll */
				mouseoff();
				for (i = y_size - 1; i > 0; i--)
					wmov_line(i - 1, i);
				wclr_eol(0, 0, bg_col);
				mouseon();
				goto c1done;
			case 'E':	/* Insert line */
				i = y_size;
				mouseoff();
				while (--i != curs_y)
					wmov_line(i - 1, i);
				wclr_eol(curs_y, 0, bg_col);
				mouseon();
				goto c1done;
			case ')':	/* Start highlight */
				SET_FGCOL(0xaaaa);
				SET_BGCOL(0x5555);
				goto c1done;
			case '(':	/* End highlighting */
				SET_FGCOL(0xffff);
				SET_BGCOL(0);
				goto c1done;
			case 'I':	/* Back tab ??? */
				if (curs_x)
				{
					do
						curs_x--;
					while (curs_x & 7);
				}
				goto c1done;
			case 'q':	/* Enter insert mode */
				insertmode = 1;
				goto c1done;
			case 'r':	/* End insert mode */
				insertmode = 0;
				goto c1done;
			case 'Q':	/* Character insert */
				if (curs_x < (x_size - 1))
				{
					hscroll(1);
					wind_putb2(" \b", 2);
				}
				goto c1done;
			case 'W':	/* Delete char */
				hscroll(0);
				goto c1done;
			case 'b':	/* Visible bell: just beep */
				lbeep(1000, 1);
				goto c1done;
			case '=':
			case 'B':
			case 'F':
			case 'M':
			case 'S':
			case 'P':
			case 'G':
			case '.':	/* Set cursor type */
			case 0x03:	/* Delay awhile */
				esc1save = ch;
				vstate = 2;
				break;
			default:
c1done:			vstate = 0;
				break;
			}
			break;
		case 2:		/* Got ESC, esc1save set up */
			buf++;
			ch -= 32;	/* All values are offset now */
			esc2save = ch;
			if (mode_640)
				ch = (ch & 3) | (ch << 2);
			ch = (ch & 15) | (ch << 4);	/* Pad out colours */
			ch = (ch & 255) | (ch << 8);
			switch (esc1save)
			{
			case '=':	/* Cursor position */
			case 'M':	/* Move a line */
			case 'P':	/* Set pallette entry */
				vstate = 3;
				break;
			case 'F':	/* Set foreground */
				SET_FGCOL(ch);
				goto c2done;
			case 'B':	/* Set background */
				SET_BGCOL(ch);
				goto c2done;
			case 'S':	/* Set border colour OR set/clear k/b VI mode */
				{
					char kbmode = 99;

					if (esc2save == 'A' - 32)
						kbmode = 0;
					else if (esc2save == 'B' - 32)
						kbmode = 1;
					else
						SET_BDCOL(esc2save);
					if (kbmode != 99)
						CDMISC(0, CDM_ALTERNATE, kbmode, 0, 0);
					goto c2done;
				}
			case 'G':	/* Set underline / inverse */
				ch = esc2save + 32;	/* Restore to real code */
				if (ch == '0')		/* Clear all modes */
					underline &= ~0xff000000;
				else
				{
					if (ch & 1)
					{	/* Start subscript */
						underline |= SUBSCRIPT;
					}
					if (ch & 2)
					{	/* Start superscript */
						underline |= SUPERSCRIPT;
					}
					if (ch & 4)
					{	/* Begin standout (reverse) mode */
						underline |= BOLD;
					}
					if (ch & 8)
					{
						underline |= UNDERLINE;
					}
					if (ch & 0x40)
					{	/* Start italics */
						underline |= ITALICS;
					}
				}
				goto c2done;
			case '.':	/* Set cursor type */
				cursorshapechanged = esc2save;
				goto c2done;
			case 0x03:		/* Delay for N ticks */
				syssafe--;
				if (!syssafe)
					SLEEP(esc2save + ' ');
				syssafe++;
				goto c2done;
			default:
c2done:			vstate = 0;
				break;
			}
			break;
		case 3:
			buf++;
			switch (esc1save)
			{
			case '=':	/* Cursor position */
				curs_y = esc2save % y_size;
				curs_x = (ch - 32) % x_size;
				break;
			case 'M':	/* Move line */
				from = esc2save;
				to = ch - ' ';
				if ((from < x_size) && (to < x_size))
				{
					mouseoff();
					wmov_line(from, to);
					mouseon();
				}
				break;
			case 'P':	/* Set pallette */
				SET_PAL(esc2save & 3, ch);
				break;
			}
		default:
			vstate = 0;
			break;
		}
		if (ourcurs)
		{
			hcurs_x = curs_x + x_start;
			hcurs_y = curs_y + y_start;
			free_curs(&cesave);
		}
		syssafe--;
		if (switchpending)
		{		/* Do a system call to deschedule */
			GETROMVER();
		}
		if (cursorshapechanged)
		{	/* Must do this after free_curs */
			switch (cursorshapechanged)
			{
			case '0' - ' ':		/* Off */
				SCURS_MODE(0, 1, 0);
				break;
			case '1' - ' ':		/* Flash block */
				normcurs(12);
				break;
			case '2' - ' ':		/* Steady block */
				{
					short buf32;

					wsetmem(buf, 0xffff, sizeof(buf) / sizeof(*buf));
					SCURS_MODE(0, 7, buf);
				}
				break;
			case '3' - ' ':		/* Flashing underline */
			case '4' - ' ':		/* Steady underline */
				{
					ushort mask[16];
					clearmem(mask, 16);		/* Zero it */
					wsetmem(mask + 7, 0xffff, 9);
					SCURS_MODE(12,
						(cursorshapechanged == '4' - ' ') ? 7 : 2,
						mask);
				}
				break;
			}
			cursorshapechanged = 0;
		}
	}
	return 0;
}

/*
 * Bang out a string of chars. Can do it real quick if no attributes
 * are set, 640 mode. Stop on control char. Return number of chars sent.
 */

drawvchars(buf, count)
uchar *buf;
int count;
{
	register int row, col, ret;

	if (!underline && mode_640)
		return charbang(buf, count);
	else
	{
		row = curs_y + y_start;
		col = curs_x + x_start;
		ret = 0;
		while (count-- && (*buf >= ' '))
		{
				draw_vchar(row, col++, *buf++, fg_col | underline, bg_col);
				ret++;
		}
		return ret;
	}
}

/*
 * Stick a char into video RAM
 * Note: Also system call 43
 */

void _draw_vchar(uint row, uint col, uint ch, int fgmask, int bgmask)
{
	register uchar *chsptr, bits;
	register int i;
	ushort lwidech[8];		/* left 320 mode char image */
	ushort rwidech[8];		/* right 320 mode char image */
	static uchar bitexlut[] =
		{	/* Map nibbles into bytes */
			0x00, 0x03, 0x06, 0x0f,
			0x30, 0x33, 0x36, 0x3f,
			0x60, 0x63, 0x66, 0x6f,
			0xf0, 0xf3, 0xf6, 0xff
		};

	chsptr = (uchar *)(&chtab[ch << 3]);	/* Pointer to char shape */

	if (ch <= ' ' || ch > 0x7f || !(underline & (SUBSCRIPT|SUPERSCRIPT)))
	{	/* Can't use subscript or superscript */
		fgmask &= ~(SUBSCRIPT|SUPERSCRIPT);
	}
	else
	{	/* Ascii printable AND sub/super is set */
		extern uchar subtable[];
		bgmask |= subtable[ch - 0x20] << 24;	/* Pass the scrunching mask */
	}

	if (mode_640)
		adrawvch(row, col, chsptr, fgmask, bgmask);
	else
	{	/* 320 mode: build wide char in widech */
		for (i = 0; i < 8; i++)
		{
			bits = *chsptr++;
			lwidech[i] = (bitexlut[bits>>4] << 8) | bitexlut[bits&15];
			bits = *chsptr++;
			rwidech[i] = (bitexlut[bits>>4] << 8) | bitexlut[bits&15];
		}
		adrawvch(row, col*2, lwidech, fgmask, bgmask);
		adrawvch(row, col*2+1, rwidech, fgmask, bgmask);
	}
}

/*
 * Convert compressed character set into 16 bit mode
 */

build_chtab()
{
	register ushort ch, wmask, bmask, temp;
	register uchar *bptr;	/* Pointer to byte image */

	bptr = charset;
	chtab = defchtab;
	for (ch = 0; ch < 2048; ch++)
	{
		temp = 0;
		bmask = 0x80;
		wmask = 0xc000;
		do
		{
			if (bmask & *bptr)
				temp |= wmask;
			wmask >>= 2;
			bmask >>= 1;
		}
		while (bmask);
		bptr++;
		chtab[ch] = temp;
	}
}

/*
 * Return true if the current window encompasses the entire screen
 * (80*25 in 640 mode, 40*25 in 320 mode)
 */

full_screen()
{
	return	(
		(x_start == 0) &&
		(y_start == 0) &&
		(y_end == NROWS) &&
		(x_end == (mode_640 ? NCOLS : NCOLS/2))
		);
}

/*
 * Save the current screen settings
 */

save_cwind()
{
	register struct window *cw;

	cw = cur_window;

	cw->x_start = x_start;
	cw->y_start = y_start;
	cw->x_end = x_end;
	cw->y_end = y_end;
	cw->bg_col = bg_col;
	cw->fg_col = fg_col;
	cw->curs_x = curs_x;
	cw->curs_y = curs_y;
}

/*
 * Set up new screen parameters from *cur_window
 */

get_cwind()
{
	register struct window *cw;

	cw = cur_window;

	x_start = cw->x_start;
	y_start = cw->y_start;
	x_end = cw->x_end;
	y_end = cw->y_end;
	x_size = x_end - x_start;
	y_size = y_end - y_start;
	bg_col = cw->bg_col;
	fg_col = cw->fg_col;
	curs_x = cw->curs_x;
	curs_y = cw->curs_y;
	g_xsize = x_size << 3;
	g_ysize = y_size << 3;
	g_xstart = x_start << 3;
	g_ystart = y_start << 3;
}

/*
 * Cursor flash code. Called at 50 Hz
 */

curs_flash()
{
	if (curs_enable && !blkcurs)
	{
		if (++curs_count >= curs_rate)
		{
			curs_count = 0;		/* Restart counter */
			inv_char();			/* Flip the char */
			curs_stat = ~curs_stat;
		}
	}
}

/*
 * Set normal cursor. Always called at VIA spl level.
 */

curs_norm()
{
	if (curs_stat)
	{
		inv_char();
		curs_stat = 0;
	}
/*	curs_count = curs_rate - 1;	/* Cursor will come on soon */
}

/*
 * Stop cursor, leaving old curs_enable value at *cesave. This is used by
 * functions which must stop the cursor code from altering the display
 * memory, without going to a higher interrupt priority.
 */

stop_curs(cesave)
int *cesave;
{
	int s;				/* Need this variable! */

#ifdef _HI_TECH_C
#error
#endif

/* Bit of assembly code to speed things up */
#ifdef _HI_TECH_C
	asm("move.w	sr,d0");
	asm("move.w	d0,-4(a6)		; s = sr");
	asm("or.w	#$0200,sr");
#elseif GCC
	asm(" move.w sr,d0");
	asm("move.w d0,-4(a6)");
	asm("or.w #$0200,sr");
#else
	s = VIA_SPL();
#endif

	if (curs_stat)
		curs_norm();
	*cesave = curs_enable;
	curs_enable = 0;

#ifdef _HI_TECH_C
	asm("move.w	-4(a6),d0");
	asm("move.w	d0,sr");
#elseif GCC
 asm(" move.w -4(a6),d0" : "move.w d0,sr" );
#else
	_splx(s);
#endif
}

/*
 * Restore cursor to previous state
 */

free_curs(cesave)
int *cesave;
{
	int s;

	curs_enable = *cesave;
	if (blkcurs && curs_enable)
	{
		s = VIA_SPL();
		inv_char();
		curs_stat = 1;
		_splx(s);
	}
	curs_count = curs_rate - 1;	/* Cursor will come on soon */
}

/*
 * Exclusive or the char at (hcurs_x, hcurs_y) with the 8 words at curs_mask
 */

inv_char()
{
	register uint crsh;

	crsh = hcurs_x;

	if (hmode_640)
	{
		do_inv(crsh);
	}
	else
	{
		crsh <<= 1;
		do_inv(crsh);
		do_inv(crsh + 1);
	}
}

do_inv(hcurs)
ushort hcurs;
{
	register ushort *maskptr;
	register ushort *_vidram;

	_vidram = &hvidram[((hcurs_y) * (NCOLS*2) + hcurs) & 0xfff];
	maskptr = curs_mask;

	*_vidram ^= *maskptr++;
	_vidram += 0x1000;
	*_vidram ^= *maskptr++;
	_vidram += 0x1000;
	*_vidram ^= *maskptr++;
	_vidram += 0x1000;
	*_vidram ^= *maskptr++;

	_vidram -= 0x3000 - NCOLS;

	*_vidram ^= *maskptr++;
	_vidram += 0x1000;
	*_vidram ^= *maskptr++;
	_vidram += 0x1000;
	*_vidram ^= *maskptr++;
	_vidram += 0x1000;
	*_vidram ^= *maskptr++;
}

/*
 * Horizontal ops for insert/delete char
 * If mode == 1, move chars right.
 * If mode == 0, move them left and zap the RH column
 */

hscroll(mode)
{
	register uint screenaddr;	/* Pointer to video RAM */
	register int nbytes, yoff;
	register int charsize;

	nbytes = x_size - curs_x - 1;
	if (nbytes < 0)
		return;			/* YERK! */
	charsize = mode_640 ? 2 : 4;	/* Bytes per char on screen */
	nbytes *= charsize;

	mouseoff();
	for (yoff = 0; yoff < 8; yoff++)
	{
		screenaddr = pixelbyte((curs_y + y_start) * 8 + yoff,
				(curs_x + x_start) * (mode_640 ? 8 : 16));
		if (mode)	/* Move right */
			movmem(screenaddr, screenaddr + charsize, nbytes);
		else
			movmem(screenaddr + charsize, screenaddr, nbytes);
	}
	if (mode == 0)
	{	/* Must clear last column */
		if (mt_pcvec)	/* Tell mouse driver we are about to draw a char */
			(*mt_pcvec)(curs_y + y_start, curs_x + x_start, ' ');
		draw_vchar(curs_y + y_start, x_size + x_start - 1, ' ',
			fg_col, bg_col);
		if (mt_acvec)
			(*mt_acvec)();
	}
	mouseon();
}

/* Turn mouse cursor on */
mouseon()
{
	if (mt_on)
		(*mt_on)();
}

/* Turn mouse cursor off */
mouseoff()
{
	if (mt_off)
		(*mt_off)();
}



/*
 * System call 46: install a mouse intercept vector
 */
int _mousetrap(unsigned int cmd, int (*vec)())
{
	int (**vp)();
	int (*temp)();
	static int (**pvp)() = { &mt_pcvec, &mt_acvec, &mt_on, &mt_off };



	if (cmd == 4)	/* Copy video context to user memory */
		return movecontext(vec, 1);
	if (cmd == 5)	/* Restore video context from user memory */
		return movecontext(vec, 0);
	if (cmd == 6)	/* Return context size */
		return sizeof(shortptrs) + sizeof(longptrs) + sizeof(curs_mask);
	vp = &pvp[cmd & 3];
	temp = *vp;
	if (cmd < 4)
		*vp = vec;
	return (int)temp;
}

/*
 * Copy video context in/out.
 *
 * If we are copying a NEW context in and ptr bit 31 is set, the new context
 * holds the flashing cursor
 */

movecontext(ptr, copyout)
register int *ptr;
char copyout;
{
	register ushort hascurs;
	register int i, s;
	register ushort **sptrs, *smptr;
	int **lptrs, *lmptr;
	int cesave;

	hascurs = (!copyout && ((uint)ptr & 0x80000000));
	ptr = (int *)((int)ptr & 0x7fffffff);

	syssafe++;
	/* Cursor switch? */
	if (hascurs)
	{
		mouseoff();
		stop_curs(&cesave);
	}

	sptrs = shortptrs;
	if (copyout)
	{
		while (smptr = *sptrs++)
			*ptr++ = (int)*smptr;
	}
	else
	{
		while (smptr = *sptrs++)
			*smptr = (ushort)(*ptr++);
	}
	lptrs = longptrs;
	if (copyout)
	{
		while (lmptr = *lptrs++)
			*ptr++ = *lmptr;
		movmem(curs_mask, ptr, sizeof(curs_mask));
	}
	else
	{
		while (lmptr = *lptrs++)
			*lmptr = *ptr++;
		movmem(ptr, curs_mask, sizeof(curs_mask));
	}

	s = VIA_SPL();

	/* If this screen has the cursor, set it */
	if (!copyout)
		ourcurs = hascurs;
	if (hascurs)
	{
		if (cur_vdp != hcur_vdp)
		{	/* Displaying wrong page */
			SET_VDP(cur_vdp);
			for (i = 0; i < 4; i++)
				SET_PAL(i, palval[i]);
		}
		if (mode_640 != hmode_640)
			SET_640(mode_640);
		hcurs_x = curs_x + x_start;
		hcurs_y = curs_y + y_start;
		hvidram = vidram;
		free_curs(&curs_enable);
	}
	_splx(s);

	if (hascurs)
		mouseon();

	syssafe--;
}
