/*
 * Code called by ROM test 7. Test the video, etc
 */

#include <syscalls.h>
#include <hwdefs.h>

cvidtest()
{
	register int bordcol = 0;
	register int i, y;

	SET_640(0);
loop:
	for (i = 0; i < 16; i++)
	{
		SET_BDCOL(bordcol);
		wsetmem(0x78000, bordcol, 0x4000);
		bordcol += 0x1111;
		twosec();
	}
	SET_BDCOL(PC_BLACK);
	SET_640(1);
	PRINTF("\014Applix 1616/OS video test code.");
	for (i = 0x20; i < 255; PUTCHAR(i++))
		;
	twosec();
	SET_640(0);
	PUTCHAR(12);
	for (y = 0; y < 200; y += 4)
	{
		SGFGCOL(0x3333);
		DRAWLINE(0, 0, 319, y);
		SGFGCOL(0x7777);
		DRAWLINE(0, 199, 319, y);
		SGFGCOL(0xcccc);
		DRAWLINE(319, 0, 0, y);
		SGFGCOL(0xeeee);
		DRAWLINE(319, 199, 0, y);
	}
	twosec();
	goto loop;
}

twosec()
{ delay(20000); }
