/* TABS4 NONDOC */

#include <syscalls.h>

/*
 * Create the /rd/syscalls.mac file
 */

EXTERN int reslevel;
EXTERN int ssddvers;
extern int ssasm_macros_length;
extern unsigned char ssasm_macros;
static char ss_name[] = "/rd/syscalls.mac";

do_ssasm_macros()
{
	if (reslevel == 0 && ssddvers == 0)
	{
		int fd, ret = 0;

		if ((fd = CREAT(ss_name, 0, 0)) < 0)
			cantcreat(ss_name, fd);
		else
		{
			if ((ret = WRITE(fd, ssasm_macros, ssasm_macros_length)) < 0)
			{
				cantwrite(ss_name, ret);
				CLOSE(fd);
			}
			else
				ret = doclose(ss_name, fd);
			if (ret < 0)
				UNLINK(ss_name);
		}
	}
	return;
}
