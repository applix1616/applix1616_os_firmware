/*
 * MRD block driver for the 68000 bus SCSI hard disk controller
 */

#include <syscalls.h>
#include <types.h>
#include <ssdd.h>
#include <files.h>
#include <blkio.h>
#include <blkerrcodes.h>
#include <reloc.h>
#include <5380.h>

#define VERSION	0x11

int havescsi;			/* Flag: the board is plugged in */
unsigned long partoffs[4];	/* Partition offsets */

scsi_init(reslevel)
{
	havescsi = 0;
	if (berrtest(INIT_CMD))
		return 0;	/* No controller board */
	havescsi = 1;
	if (init_scsi(reslevel) < 0)
		havescsi = 0;
	else
	{
		if (!reslevel)
			cprintf("SCSI disk code installed");
	}
}

scsi_read(block, buffer, unit)
uint block;
uchar *buffer;
{
	block += partoffs[unit];
	return scsi_brw(0, block * 2, 2, buffer, 1);
}

scsi_write(block, buffer, unit)
uint block;
uchar *buffer;
{
	block += partoffs[unit];
	return scsi_brw(0, block * 2, 2, buffer, 0);
}

scsi_multirw(isread, addr, firstblock, nblocks, unit)
{
	int retval;

	firstblock += partoffs[unit];
/*printf(" MULTI(%d,0x%x,%d,%d) ", isread, addr, firstblock, nblocks);*/
	retval = scsi_brw(0, firstblock * 2, nblocks * 2, addr, isread);
	return (retval < 0) ? BEC_IOERROR : 0;
}

/*
 * Miscellaneous entry point
 */

scsi_misc(code, arg1, unit)
{
	switch (code)
	{
#ifdef notdef
	case MB_RESET:
		break;
#endif
	case MB_FLUSH:
		break;
	case MB_VERS:	/* Get driver version */
		return 1;
	case MB_NEWDISK:
		break;
	}
	return 0;
}

/*
 * Install drivers: return -1 if problems
 */

init_scsi(reslevel)
{
	int retval, bufsize;
	unsigned char blkbuf[BLOCKSIZE];
	register struct rootblock *rbptr;
	register unsigned char *buf, *cp;
	struct multidriver multidriver;
	register int i, j;

	int _scsi0_read(), _scsi0_write(), _scsi0_misc(), _scsi0_multirw();
	int _scsi1_read(), _scsi1_write(), _scsi1_misc(), _scsi1_multirw();
	int _scsi2_read(), _scsi2_write(), _scsi2_misc(), _scsi2_multirw();
	int _scsi3_read(), _scsi3_write(), _scsi3_misc(), _scsi3_multirw();

	static int (*reads[])() =
		{_scsi0_read,_scsi1_read,_scsi2_read,_scsi3_read};
	static int (*writes[])() =
		{_scsi0_write,_scsi1_write,_scsi2_write,_scsi3_write};
	static int (*miscs[])() =
		{_scsi0_misc,_scsi1_misc,_scsi2_misc,_scsi3_misc};
	static int (*multirw[])() =
		{_scsi0_multirw,_scsi1_multirw,_scsi2_multirw,_scsi3_multirw};

	multidriver.version = VERSION;

	rbptr = (struct rootblock *)blkbuf;
	Scsi_init();
#ifdef notdef
	retval = reset_scsi();
	if (retval)
	{
		Sprintf("scsi_reset");
		rmdrivers();
	}
#endif
	partoffs[0] = 0;
	retval = scsi_read(0, blkbuf, 0); /* Read the partition table */
	if (retval < 0)
	{
		Sprintf("Partition table read");
		rmdrivers();
		return -1;
	}

	cp = blkbuf;
	for (i = 0; i < 4; i++)
	{
		partoffs[i] = 0;
		for (j = 0; j < 4; j++)
		{
			partoffs[i] |= *cp++ << (j * 8);
		}
		partoffs[i] &= 0x00ffffff;
		partoffs[i] /= 2;
/*		PRINTF("Partition %d: %d\r\n", i, partoffsi);*/
	}
	for (i = 0; i < 4; i++)
	{
		char name[10];
		long nblocks;

		if ((partoffs[i] + 1 <= partoffs[i]) && (i != 3))
		{	/* No more partitions */
			goto done;
		}
		/* Read the root block */
		retval = scsi_read(ROOTBLOCK, rbptr, i);
		if (retval < 0)
			continue;
		nblocks = rbptr->nblocks;
/*		PRINTF("Unit %d: %d blocks\r\n", i, nblocks);*/
		if (nblocks <= 0)
			continue;
		bufsize = ((nblocks + 8191) / 8) & ~BSIZEM1;
		if (bufsize > 20000)
		{
			FPRINTF(STDERR,"SCSI panic: bufsize = %d\r\n",bufsize);
			rmdrivers();
			return -1;
		}
		buf = (unsigned char *)GETMEM(bufsize, 1);
		SPRINTF(name, "\001/S%d", i);
		multidriver.misc = miscs[i];
		multidriver.multirw = multirw[i];
		INST_BDVR(reads[i], writes[i], &multidriver, name, buf);
	}
done:	return 0;
}

rmdrivers()
{
	char buf[20];
	register int i;

	cprintf("Trashing /S0-/S3");
	for (i = 0; i < 4; i++)
	{
		SPRINTF(buf, "/S%d", i);
		INST_BDVR(0, 0, 0, buf, 0);
	}
}

void Sprintf(str, p1, p2, p3, p4, p5, p6)
char *str;
{
	extern int scsi_stat, scsi_msg, scsi_err;
	uint val, cbs;
	static char *phases[] =
	{
		"Data out", "Unspecified(1)", "Command", "Message out",
		"Data in", "Unspecified(2)", "Status", "Message in"
	};

	PRINTF(str, p1, p2, p3, p4, p5, p6);
	cbs = *CUR_BUS_STAT;
	PRINTF(" C_B_S: 0x%x, B_A_S: 0x%x ", cbs, *BUS_AND_STAT);
	val = 0;
	if (cbs & CB_MSG)
		val |= 1;
	if (cbs & CB_CD)
		val |= 2;
	if (cbs & CB_IO)
		val |= 4;
	PRINTF("Bus phase: %s\r\n", phases[val]);

	if (scsi_stat)
		PRINTF("scsi_stat: 0x%x ", scsi_stat);
	if (scsi_msg)
		PRINTF("scsi_msg: 0x%x ", scsi_msg);
	if (scsi_err)
		PRINTF("scsi_err: 0x%x ", scsi_err);
	if (scsi_err || scsi_msg || scsi_stat)
		PRINTF("\r\n");
}

Sdelay(n)
{
	while (n--)
		;
}
