/* TABS4 */
#include <string.h>
#include <types.h>
#include <files.h>
#include <syscalls.h>
#include "zmodem.h"

#define DEFBYTL 2000000000L			/* default rx file size */

int				Zconv;

extern int		Zifd;				/* input file descriptor */
extern int		Zofd;				/* output file descriptor */
extern int		Zfile_fd;
extern int		AKPMVerbose;
extern char		*ZPathname;
extern char		*Ziobuf;

/* Junk in zm.c */
extern char		Rxhdr[];
extern char		Txhdr[];
extern char		Attn;
extern int		Rxcount;
extern int		Rxtimeout;

extern long		Bytesleft;		/* number of bytes of incoming file left */
extern long		AKPMtotsize;

extern int tryzhdrtype;			/* Header type to send corresponding to Last
				 			 	* rx close */

char *room0for();

/*
 * Process incoming file information header
 */
procheader(name)
char *name;
{
	/*
	 * Process ZMODEM remote file management requests
	 */
	CLOSE(Zfile_fd);
	if (ZPathname)
		PROCESSDIR(ZPathname, BACKEDUP, PD_SETSTATBITS);

	Bytesleft = DEFBYTL;

	AKPMtotsize = Bytesleft = atoi(name + 1 + strlen(name));
	ZPathname = room0for(name);

	Zfile_fd = CREAT(name, 0, 0);
	if (Zfile_fd < 0)
		return ERROR;
	return OK;
}

/*
 * Putsec writes the n characters of buf to receive file Zfile_fd.
 */
putsec(buf, n)
char *buf;
register n;
{
	register int ret;

	if (Zconv == ZCNL)
	{
		char lbuf[1025];
		register char *lbp, *lbe;

		lbe = lbuf + sizeof(lbuf) - 1;
		lbp = lbuf;

		while (n-- && ret >= 0)
		{
			char c;

			if ((c = *buf++) == '\n')
				*lbp++ = '\r';
			*lbp++ = c;
			if (lbp >= lbe || (n <= 0))
			{
				ret = WRITE(Zfile_fd, lbuf, lbp - lbuf);
				lbp = lbuf;
			}
		}
	}
	else
		ret = (WRITE(Zfile_fd, buf, n) < 0) ? ERROR : OK;
	return ret;
}

/*
 * Initialize for Zmodem receive attempt, try to activate Zmodem sender
 *  Handles ZSINIT frame
 *  Return ZFILE if Zmodem filename received, -1 on error,
 *   ZCOMPL if transaction finished,  else 0
 */
tryz()
{
	register c, n;

	for (n = 10; --n >= 0; )
	{
		/* Set buffer length (0) and capability flags */
		stohdr(0L);
		Txhdr[ZF0] = CANFC32 | CANFDX | CANOVIO;
		zshhdr(tryzhdrtype, Txhdr);
again:
		switch (zgethdr(Rxhdr))
		{
		case ZRQINIT:
		case ZEOF:
		case TIMEOUT:
		case ZCOMPL:
		default:
			continue;
		case ZFILE:						/* File name from sender */
			tryzhdrtype = ZRINIT;
			Zconv = Rxhdr[ZF0];
			c = zrdata(Ziobuf, KSIZE);	/* Get the filename */
			if (c == GOTCRCW)
				return ZFILE;
			/* Failed to get filename */
			zshhdr(ZNAK, Txhdr);
			goto again;
		case ZSINIT:
			if (zrdata(Attn, ZATTNLEN) == GOTCRCW)
				zshhdr(ZACK, Txhdr);
			else
				zshhdr(ZNAK, Txhdr);
			goto again;
		case ZFREECNT:
			stohdr(~0);
			zshhdr(ZACK, Txhdr);
			goto again;
#if 0
		case ZCOMMAND:
			cmdzack1flg = RxhdrZF0;
			if (zrdata(Ziobuf, KSIZE) == GOTCRCW)
			{
				AMprintf("GOT GOTCRCW");
#ifdef god
				if (cmdzack1flg & ZCACK1)
					stohdr(0L);
				else
					stohdr((long) sys2(Ziobuf));
#endif
				purgeline();	/* dump impatient questions */
				do
					zshhdr(ZCOMPL, Txhdr);
				while (++errors < 10 && zgethdr(Rxhdr) != ZFIN);
				ackbibi();
#ifdef god
				if (cmdzack1flg & ZCACK1)
					exec2(Ziobuf);
#endif
				return ZCOMPL;
			}
			zshhdr(ZNAK, Txhdr);
			goto again;
#endif
		case ZFIN:
			ackbibi();
			return ZCOMPL;
		case ZCAN:
			return ERROR;
		}
	}
	return 0;
}

/*
 * Receive 1 or more files with ZMODEM protocol
 */
rzfiles()
{
	register c;

	for (;;)
	{
		c = rzfile();
		Rxstatus();
		AMprintf("");
		ZPathname = "??filename??";
		switch (c)
		{
		case ZEOF:
		case ZSKIP:
			switch (tryz())
			{
			case ZCOMPL:
				return OK;
			default:
				return ERROR;
			case ZFILE:
				break;
			}
			continue;
		default:
			return c;
		case ERROR:
			return ERROR;
		}
	}
}


/*
 * Receive a file with ZMODEM protocol
 *  Assumes file name frame is in Ziobuf
 */
rzfile()
{
	register c, errcnt;
	long rxbytes;

	if (procheader(Ziobuf) == ERROR)
		return (tryzhdrtype = ZSKIP);

	errcnt = 10;
	rxbytes = 0l;

	for (;;)
	{
		stohdr(rxbytes);
		zshhdr(ZRPOS, Txhdr);
nxthdr:
		switch (c = zgethdr(Rxhdr))
		{
		default:
#ifdef NOISY
			AMprintf("rzfile: zgethdr returned %d", c);
#endif
			return ERROR;
		case ZNAK:
		case TIMEOUT:
			if (--errcnt < 0)
			{
#ifdef NOISY
				AMprintf("rzfile: zgethdr returned %d", c);
#endif
				return ERROR;
			}
		case ZFILE:
			zrdata(Ziobuf, KSIZE);
			continue;
		case ZEOF:
			if (rclhdr(Rxhdr) != rxbytes)
				continue;
			if (closeit())
			{
				tryzhdrtype = ZFERR;
#ifdef NOISY
				AMprintf("rzfile: closeit returned <> 0");
#endif
				return ERROR;
			}
#ifdef NOISY
			AMprintf("rzfile: normal EOF");
#endif
			return c;
		case ERROR:	/* Too much garbage in header search error */
			if (--errcnt < 0)
			{
#ifdef NOISY
				AMprintf("rzfile: zgethdr returned %d", c);
#endif
				return ERROR;
			}
			zmputs(Attn);
			continue;
		case ZDATA:
			if (rclhdr(Rxhdr) != rxbytes)
			{
				if (--errcnt < 0)
					return ERROR;
				zmputs(Attn);
				continue;
			}
	moredata:
			switch (c = zrdata(Ziobuf, KSIZE))
			{
			case ZCAN:
#ifdef NOISY
				AMprintf("rzfile: zgethdr returned %d", c);
#endif
				return ERROR;
			case ERROR:	/* CRC error */
				if (--errcnt < 0)
				{
#ifdef NOISY
					AMprintf("rzfile: zgethdr returned %d", c);
#endif
					return ERROR;
				}
				zmputs(Attn);
				continue;
			case TIMEOUT:
				if (--errcnt < 0)
				{
#ifdef NOISY
					AMprintf("rzfile: zgethdr returned %d", c);
#endif
					return ERROR;
				}
				continue;
			case GOTCRCW:
				errcnt = 10;
				putsec(Ziobuf, Rxcount);
				rxbytes += Rxcount;
				stohdr(rxbytes);
				zshhdr(ZACK, Txhdr);
				goto nxthdr;
			case GOTCRCQ:
				errcnt = 10;
				putsec(Ziobuf, Rxcount);
				rxbytes += Rxcount;
				stohdr(rxbytes);
				zshhdr(ZACK, Txhdr);
				goto moredata;
			case GOTCRCG:
				errcnt = 10;
				putsec(Ziobuf, Rxcount);
				rxbytes += Rxcount;
				goto moredata;
			case GOTCRCE:
				errcnt = 10;
				putsec(Ziobuf, Rxcount);
				rxbytes += Rxcount;
				goto nxthdr;
			}
		}
	}
}

zmputs(s)
char *s;
{
	while (*s)
		sendline(*s++);
	sl_bflush();
}

/*
 * Close the receive dataset, return OK or ERROR
 */
closeit()
{
	if (CLOSE(Zfile_fd) < 0)
	{
		AMprintf("file close ERROR\r\n");
		return ERROR;
	}
	return OK;
}


/*
 * Ack a ZFIN packet, let byegones be byegones
 */
ackbibi()
{
	register n;

	stohdr(0L);
	for (n = 5; n; n--)
	{
		zshhdr(ZFIN, Txhdr);
		for (;;)
		{
			switch (readline(100))
			{
			case 'O':
				readline(1);	/* Discard 2nd 'O' */
				/* ***** FALL THRU TO ***** */
			case TIMEOUT:
				return;
			default:
				break;
			}
		}
	}
}

/*
 * Let's receive something already.
 */

wcreceive()
{
	register c;

	if (c = tryz())
	{
		if (c == ZCOMPL)
			return OK;
		if (c == ERROR)
			goto fubar;
		c = rzfiles();
		if (c)
			goto fubar;
	}
	return OK;
fubar:
	canit();

	if (Zfile_fd>= 0)
		CLOSE(Zfile_fd);

	return ERROR;
}

rz_main(argc, argv)
char *argv;
{
	int exitcode;

	Rxtimeout = 100;

	if (exitcode = wcreceive())
	{
		canit();
		canit();
	}
	return exitcode;
}
