/*
 * Screen editor functions
 */

#ifdef ROM256
#define EXTERN	extern
#endif

#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <mondefs.h>
#include <sedit.h>
#include <files.h>
#include <windows.h>
#include <envbits.h>
//#include <splfns.h>

#define NUNDOS	10	/* Number of undo buffers */

uchar *udbufs[NUNDOS];

EXTERN uchar *mem_buf;	/* Start of text */
EXTERN uchar *end_buf;	/* Points to null after last line */
EXTERN int top_lin;	/* Line number for line at top of screen */
EXTERN int bot_lin;	/* Number of line beyond last displayed */
EXTERN int cur_lin;	/* Current */
EXTERN uchar *top_lp;	/* Points to source for top line */
EXTERN uchar *bot_lp;	/* Points to the string beyond the last displayed */
EXTERN uchar *cur_lp;	/* Start of current line */
EXTERN uchar *cur_ptr;	/* Current working pointer */
EXTERN int cur_off;	/* cur_ptr - cur_lp (column offset of cursor) */
EXTERN int save_coff;	/* Flag for preserving cur_off */
EXTERN int bot_y;	/* Display line at which no text is displayed */
EXTERN int top_y;	/* Display line of top displayed line */
EXTERN int cur_y;	/* Current Y cursor pos */
EXTERN int cur_x;	/* Current X cursor pos */
EXTERN int lin_siz;	/* Number of screen lines occupied by a string */
EXTERN int lin_dlen;	/* Number of screen uchars occupied by a string */
EXTERN int lin_len;	/* Number of real uchars in a string */
EXTERN int width;	/* Screen width */
EXTERN int height;	/* Screen height */
EXTERN int scrnleft;	/* Used for preventing printing from going off the bottom */
EXTERN int max_mem;	/* Maximum allowed buffer size */
EXTERN int ibindex;	/* Index for insert buffer */
EXTERN int nreps;	/* Number of repeats for search/substitute */
EXTERN int nfound;	/* Number of successful searches/substitutes */
EXTERN int dlscren;	/* Flag: Enable scrolling in draw_lin() */
EXTERN int modflag;	/* Flag: file modified */
EXTERN uchar *blkstart;	/* Start of block */
EXTERN uchar *blkend;	/* End of block */
EXTERN uchar *newcur_ptr;/* Used for keeping track of where we are */
EXTERN uchar **markers;	/* Markers */
EXTERN int blkset;	/* Flag: block is marked */
EXTERN int hilit;	/* Flag: highlight mode is set */
EXTERN int scr_top;	/* Current screen top */
EXTERN int frozen;	/* Flag: screen frozen */
EXTERN int tabwidth;

EXTERN uchar *file_name;
EXTERN uchar *statmes;	/* Top left status message */

EXTERN uchar *srchstr;	/* Was static in search() */
EXTERN uchar *subsold;	/* Old string for substitute */
EXTERN uchar *subsnew;	/* New string for substitute */

EXTERN ushort efilestat; /* Edited file's status bits */
EXTERN long euserid;	/* Edited file's user ID */

uchar *getnext(), *get_prev();
uchar *fwd_fnw(), *rev_fnw();
void sstatmes(char *str);

//uchar *index();

static uchar ftoolarge[] = "File too large";

/* Return two-thirds of the current page size (for control-C & R) */
mpaglen()
{
	return (2 * (height - scr_top)) / 3;
}

/* Output buffer for speeding screen output */
#define PBSIZE 256
uchar *pbufdata;
ushort pbufindex;

struct pbuf *ppbuf;

/*
 * Draw the line at *lp on the screen at position d, y. Stop on \r. Return the
 * new position
 */

draw_lin(lp, d, y)
register uchar *lp;
register int d;
register int y;
{
	register uchar ch;
	register char dohil;
	char tabo;
	uchar _pbufdata[PBSIZE];

	pbufdata = _pbufdata;		/* Global pointer */
	pbufindex = 0;

	scrnleft = (height - y) * width - d - 1;
	dohil = (blkset || hilit);	/* Call 'sethil' if true */
	gotoxy(d, y);

	while (scrnleft || dlscren)
	{
		if (dohil && hildif(lp))
		{
			flushpbuf();
			hil(hilit);
		}
		if ((ch = MASK(*lp++)) < 0x20)
		{
			{
				switch (ch)
				{
				case '\t':
					tabo = tabwidth - (d % tabwidth);
					d += tabo;
					while (tabo--)
						lputchar(' ');
					break;
				case '\r':
				case '\0':
					flushpbuf();
					tclrhil();
					clr_eol();
					resthil();
					goto out;
				default:
					lputchar('^');
					lputchar(ch | 0x40);
					d += 2;
					break;
				}
			}
		}
		else
		{
			lputchar(ch);
			d++;
		}
	}
	flushpbuf();
out:
	return d;
}

flushpbuf()
{
	if (pbufindex)
	{
		WRITE(STDOUT, pbufdata, pbufindex);
		pbufindex = 0;
	}
}

/*
 * If there is room on the screen, print a char
 *
 * ONLY draw_lin CAN CALL THIS FUNCTION
 */

lputchar(ch)
uchar ch;
{
	if (scrnleft)
	{
		pbufdata[pbufindex++] = ch;
		scrnleft--;
	}
	else
	{
		if (dlscren)	/* Make more room */
		{
			flushpbuf();
			up_scrl(1);
			gotoxy(width - 1, height - 2);
			pbufdata[pbufindex++] = ch;
			scrnleft--;
		}
	}
	if (pbufindex == PBSIZE)
		flushpbuf();
}

/*
 * If the passed pointer is in the block, set highlight mode
 */

sethil(ptr)
uchar *ptr;
{
	if (hildif(ptr))
		hil(hilit);
}

hildif(ptr)
uchar *ptr;
{
	if (blkset && (ptr >= blkstart) && (ptr < blkend))
	{
		if (!hilit)
		{
			hilit = 1;
			return 1;
		}
	}
	else
	{
		if (hilit)
		{
			hilit = 0;
			return 1;
		}
	}
	return 0;
}

/*
 * Draw whole screen, with the line at *lp starting at display line y. Update
 * all the red tape for screen management. The line at *lp has number lin
 */

redraw(lp, lin, y)
uchar *lp;
int lin, y;
{
	register uchar *cp;

	top_lp = lp;
	top_y = y;
	top_lin = lin;
	for (;;)
	{	/* Back up through file until we go off the top */
		if ((top_y == scr_top) || (top_lp == mem_buf))
			break;
		set_param(cp = get_prev(top_lp));
		if ((top_y - lin_siz) < scr_top)
			break;
		top_y -= lin_siz;
		top_lin--;
		top_lp = cp;
	}
	top_y = scr_top;
	draw(top_lp, top_lin, scr_top);	/* Redraw the whole lot */
}

/*
 * Redraw whole screen with current line in middle
 */

redwhole()
{
	int draw_y;

	draw_y = (height + scr_top) / 2;	/* Where to centre things */
	set_param(cur_lp);
	if ((lin_siz + draw_y) >= height)
		draw_y = height - lin_siz;
	redraw(cur_lp, cur_lin, draw_y);
}

/*
 * Redraw the screen at the current cursor position, provided the whole line
 * fits on screen
 */

redhere()
{
	set_param(cur_lp);
	if ((cur_y + lin_siz) > height)
		cur_y = height - lin_siz;
	redraw(cur_lp, cur_lin, cur_y);
}

/*
 * Draw the screen from display row y to the bottom. Update display red tape,
 * set cur_lp if found
 */

draw(lp, lin, y)
uchar *lp;
int lin;
register int y;
{
	bot_lin = lin;
	bot_lp = lp;

	while (y < height)
	{
		if (bot_lp == cur_lp)
		{
			cur_y = y;	/* Found our current line */
			cur_lin = bot_lin;
		}

		if (bot_lp == end_buf)
		{
			int lowbaud = envbits(ENVB_LOWBAUD);

			bot_y = y;
			do
			{
				draw_lin(".\r", 0, y++);
			}
			while ((y < height) && !lowbaud);
			if (lowbaud)
			{
				PRINTF(CLEAREOS);
				y = height;
			}
		}
		else
		{
			set_param(bot_lp);
			draw_lin(bot_lp, 0, y);
			if (y + lin_siz > height)	/* Partial line */
				y = height;
			else
			{
				bot_y = y += lin_siz;
				bot_lp = getnext(bot_lp);
				++bot_lin;
			}
		}
	}
}

/*
 * Scan a line, set up cur_x (if found), set lin_siz = number of screen rows
 * occupied, lin_dlen = printed length of line, lin_len = real length.
 */

int curcur;		/* Character offset into line where cursor is */

set_param(lp)
register uchar *lp;
{
	register int dlen;	/* Displayed length */
	register int len;	/* Real length */
	register uchar c;
	register uchar *olp;		/* Original lp */

	olp = lp;
	dlen = len = 0;
	curcur = 0;

	for (;;) 
	{
		if (lp == cur_ptr)	/* Found our X pos */
			cur_x = dlen;
		if (dlen <= cur_off)
			curcur = lp - olp;	/* Offset position */

		if ((c = MASK(*lp++)) < 0x20)
		{
			switch(c)
			{
			case '\t':
#ifdef notdef
				do
					dlen++;
				while (dlen % tabwidth);
#endif
				dlen += tabwidth;
				dlen -= dlen % tabwidth;
				len++;
				break;
			case '\r':
			case '\0':
				lin_siz = dlen / width + 1;
				lin_dlen = dlen;
				lin_len = len;
				return;
			default:
				dlen += 2;	/* For ^X type output */
				len++;
				break;
			}
		}
		else
		{
			dlen++;
			len++;
		}
	}
}

/*
 * Preserve the vertical column by moving cursor
 */

fix_cpos()
{
	set_param(cur_lp);
	if (cur_off > lin_dlen)	/* This line too short */
		cur_ptr = cur_lp + lin_len;
	else
		cur_ptr = cur_lp + curcur;
	set_param(cur_lp);
	save_coff = 1;		/* Prevent alteration of cur_off */
}

/*
 * Scan from *lp for the next line of source. Return a pointer to it, else
 * to the end of the buffer
 */

uchar *
getnext(lp)
register uchar *lp;
{
	*end_buf = 0;
	lp = (uchar *)INDEX(lp, '\r');
	return (lp == 0) ?  end_buf : lp + 2;
}

/*
 * With lp pointing to the start of a line, scan backwards looking for the
 * start of the previous line. 
 */

uchar *
get_prev(lp)
register uchar *lp;
{
	lp -= 2;	/* Now points to \r */
	while (MASK(*--lp) != '\r' && lp >= mem_buf)
		;
	return (lp < mem_buf) ? mem_buf : lp + 2;
}

/*
 * Scroll up sufficiently to display *bot_lp. Reset bottom stuff, y stuff,
 * current stuff.
 */

scrl_up()
{
	register int want;

	set_param(bot_lp);
	want = lin_siz - (height - bot_y);
	if (want < 0)
		want = 0;
	up_scrl(want);
	draw(bot_lp, bot_lin, bot_y - want);
	top_y -= want;
	while (top_y < scr_top)
	{
		set_param(top_lp);
		top_y += lin_siz;
		top_lp = getnext(top_lp);
		top_lin++;
	}
}

/*
 * When called, cur_y is the y coord of the desired line, and may be negative.
 * If it is negative, scroll down until we just fit. If a sub-line is pushed
 * off, redraw bottom
 */

scrl_dn()
{
	while (cur_y < scr_top)
	{
		++cur_y;
		dn_scrl();
		if (++bot_y > height)
		{
			bot_lp = get_prev(bot_lp);
			--bot_lin;
			set_param(bot_lp);
			bot_y -= lin_siz;
		}
	}
	top_lp = cur_lp;
	top_y = scr_top;
	top_lin = cur_lin;
	draw_lin(cur_lp, 0, scr_top);
}

read_file(name, mode)
uchar *name;
int mode;		/* 0 = whole file, 1 = insert */
{
	register int ch;
	register int bufi;
	register int fd;
	register uchar *tmp;
	int nbr;
	uchar buf[1025];

	if ((fd = OPEN(name, SSO_RDONLY)) < 0)
	{
		INTERPBEC(fd, statmes);
		return -1;
	}

	if (!mode)
	{
		if ((nbr = READ(fd, mem_buf, max_mem)) == max_mem)
			sstatmes(ftoolarge);
		if (nbr < 0)
		{
			INTERPBEC(nbr, statmes);
			CLOSE(fd);
			return -1;
		}
		if (!nbr)
		{
			CLOSE(fd);
			return -1;
		}
		mem_buf[nbr] = 0;
		end_buf = mem_buf + nbr;
	}
	else
	{
		tmp = cur_ptr;
		bufi = 0;
		do
		{
			if ((ch = GETC(fd)) < 0)
				ch = '\0';
			if (ch)
				buf[bufi++] = ch;
			if ((bufi > 1023) || !ch)
			{		/* Move the block */
				if ((end_buf + bufi) > (mem_buf + max_mem))
				{
					sstatmes(ftoolarge);
					goto rfexit;
				}
				blkswap(tmp, 0, buf, bufi);
				tmp += bufi;
				bufi = 0;
			}
		}
		while (ch);
rfexit:		;
	}
	CLOSE(fd);
	*end_buf = 0;
	modflag = 1;
	return (end_buf == mem_buf) ? -1 : 0;
}

write_file(name, first, last)
uchar *name;
uchar *first, *last;
{
	register int ofd;
	register int ec;
	struct dir_entry dirent;
	uchar *nname;

	if (*name == '>')
		ofd = OPEN(nname = name + 1, 2);
	else
		ofd = CREAT(nname = name, 0, 0);

	if (ofd < 0)
	{
		INTERPBEC(ofd, statmes);
		return -1;
	}

	if (((ec = WRITE(ofd, first, last - first)) < 0) ||
			((ec = CLOSE(ofd)) < 0))
	{
		CLOSE(ofd);
		INTERPBEC(ec, statmes);
		return -1;
	}
	/* Diddle uid and status bits */
	if ((ofd >= NIODRIVERS) && (euserid != -1) &&
			(FILESTAT(nname, &dirent) >= 0))
	{
		dirent.statbits = efilestat;
		dirent.uid = euserid;
		PROCESSDIR(nname, &dirent, 3);
	}
	return 0;
}

/*
 * Replace the bytes between (to) and (to + oldlen - 1) with the bytes between
 * (from) and (from + newlen - 1)
 */

blkswap(to, oldlen, from, newlen)
uchar *to, *from;
int oldlen, newlen;
{
	register int distance;		/* How far we are moving */
	register int i;
	register uchar **pptr;
	register uchar *source, *dest;
	uchar *ptr;

	distance = newlen - oldlen;
	source = to + oldlen;
	dest = to + newlen;

	if ((end_buf + distance) > (mem_buf + max_mem))
	{
		sstatmes(ftoolarge);
		return -1;
	}

	for (i = 0; i < 13; i++)
	{	/* Scan the markers, updating any which have moved */
		switch (i)
		{
		case 10:
			pptr = &blkstart;
			break;
		case 11:
			pptr = &blkend;
			break;
		case 12:
			pptr = &newcur_ptr;
			break;
		default:
			pptr = &markers[i];
			break;
		}

		if (ptr = *pptr)
		{
			if (ptr >= source)
				ptr += distance;	/* Within moved block */
			else
			{
				if ((distance < 0) && (ptr >= dest))
					ptr = dest;	/* Marker in deleted block */
			}
			*pptr = ptr;	/* New value */
		}
	}
	blkset = (blkset && (blkstart < blkend));

	movmem(source, dest, end_buf - to - oldlen);
	end_buf += distance;
	movmem(from, to, newlen);
	*end_buf = 0;
	modflag = 1;
	return 0;
}

/*
 * Delete n chars from *cur_ptr. Only allow a delete up to end of line.
 * If *cur_ptr == '\r', join the two lines.
 * Redraw screen, update bottom stuff
 */

delete(n)
register int n;
{
	register int c, l;
	register int ntodel;	/* Number of lines to delete */
	register uchar *tempptr;
	int nlinnext;		/* Number of displayed lines on next line */

	if (cur_ptr >= end_buf - 2)
		return;
	l = lin_siz;
	if ((tempptr = (uchar *)INDEX(cur_ptr, '\r')) == 0)	/* No CR at end */
		tempptr = end_buf;
	c = tempptr - cur_ptr;
	if (n > c)
		n = c;		/* Don't go beyond end of line */
	if (n == 0)
		n = 2;
	if ((c = MASK(*cur_ptr)) == '\r')
	{
		set_param(getnext(cur_lp));
		nlinnext = lin_siz;
	}
	if (n > 1)
		predelete(cur_ptr, n);
	blkswap(cur_ptr, n, cur_ptr, 0);
	bot_lp -= n;
	set_param(cur_lp);
	if ((lin_siz + cur_y) >= height)
	{	/* Joined line won't fit on screen */
		bot_lp = cur_lp;
		bot_y = cur_y;
		bot_lin = cur_lin;
		scrl_up();
	}
	else
	{
		if (c == '\r')
			bot_lin--;
		else
/* Adjust so that ntodel does not reflect length of next line */
			nlinnext = 0;	
		draw_lin(cur_ptr, cur_x, cur_y);
		if (cur_ptr == end_buf) return;
		if ((c == '\r') || (l != lin_siz))
		{	/* Need to move the rest of the screen up */
			ntodel = l + nlinnext - lin_siz;
			dellines(cur_y + lin_siz, ntodel);
			bot_y -= ntodel;
			draw(bot_lp, bot_lin, bot_y);
		} 
	}
}

/*
 * The cursor has changed. If still on the screen then move it, else
 * redraw the screen
 */

newcurs()
{
	register uchar *tempptr;

	if ((cur_ptr < bot_lp) && (cur_ptr >= top_lp))
	{	/* New string is on screen. Find its location */
		tempptr = top_lp;
		cur_y = top_y;
		while (tempptr <= cur_ptr)
		{
			set_param(tempptr);
			cur_y += lin_siz;
			tempptr = getnext(tempptr);
		}
		cur_y -= lin_siz;	/* Adjust for going beyond */
	}
	else
		redwhole();
}

/*
 * Prepare the search/substitute result message
 */

prep_nfound()
{
	SPRINTF(statmes, "%u found", nfound);
}

/* Return true if block is set up ok */
blkok()
{
	if (blkset && blkstart && blkend && (blkstart < blkend))
		return 1;
	else
	{
		sstatmes("No block");
		return 0;
	}
}

/*
 * Do a block copy. The pointers are ok. Return 0 if error
 */

blkcpy(c)
char c;
{
	if
	(
		(cur_ptr >= blkstart) &&
		(
			(c == 'V' && cur_ptr <= blkend) ||
			(c == 'C' && cur_ptr < blkend)
		)
	)
	{
		sstatmes("In block");
		return 0;
	}
/* Note: May have to move from blkend to precompensate for the first move in blkswap() */
	blkswap(cur_ptr, 0, (cur_ptr < blkstart) ? blkend : blkstart,
		blkend - blkstart);
	return 1;
}

/*
 * Do a block delete. The pointers are OK. Return 0 if error
 */

blkdel()
{
	predelete(blkstart, blkend - blkstart);
	blkswap(blkstart, blkend - blkstart, blkend, 0);
	return 1;
}

/*
 * Refind the current position, set up cur_lp, cur_lin.
 */

redoptr()
{
	register uchar *tempptr;
	register unsigned i;

	i = cur_ptr - mem_buf;	/* Number of bytes to scan */
	cur_lin = 1;
	cur_lp = tempptr = mem_buf;
	while (i--)
	{	/* More uchars to go */
		if (MASK(*tempptr++) == '\r')
		{
			cur_lin++;
			cur_lp = tempptr + 1;
		}
	}
}

/*
 * Prepare for a deletion: place the block to be deleted in the buffer
 */

predelete(ptr, nbytes)
uchar *ptr;
{
	register uchar *ret;

	if ((*ptr == '\r') && (nbytes < 3))
		return;

	if (udbufs[NUNDOS-1])
		FREEMEM(udbufs[NUNDOS-1]);	/* Dump the old one */
	ret = (uchar *)getmem0(nbytes + 1);
	if ((int)ret < 0)
		ret = 0;
	else
	{
		movmem(ptr, ret, nbytes);
		ret[nbytes] = 0;
	}
	/* Make room */
	movmem(udbufs, udbufs + 1, (NUNDOS - 1) * sizeof(*udbufs));
	udbufs[0] = ret;
}

/*
 * Insert the contents of an undo buffer
 */

putundobuf(bufnum)
uint bufnum;
{
	int ret;

	if ((bufnum >= NUNDOS) || (!udbufs[bufnum]))
		return -1;
	ret = blkswap(cur_ptr, 0, udbufs[bufnum], strlen(udbufs[bufnum]));
	draw(cur_lp, cur_lin, cur_y);
	return ret;
}

/* Print out undo buffer contents */

undoreview()
{
	register uchar *cp, ch;
	register short column;
	register int bufindex;

	column = height - NUNDOS - 2;
	if (column < 1)
		column = 1;
	gotoxy(0, column);
	equalsigns();
	for (bufindex = 0; bufindex < NUNDOS; bufindex++)
	{
		PRINTF("\r\n%d: ", bufindex);
		if (cp = udbufs[bufindex])
		{
			column = 3;
			while ((ch = MASK(*cp++)) && (column < width - 2))
			{
				if (ch < 0x20)
				{
					if (ch == '\n')
						opchar('/');
					else if (ch == '\t')
						opchar(' ');
					else
						column--;	/* Predec */
				}
				else
					opchar(ch);
				column++;
			}
			if (ch)
				opchar('+');
		}
		clr_eol();
	}
	hil(2);
	PRINTF("\r\nHit a key ");
	resthil();
	clr_eol();
	rawgetc(STDIN);
	redwhole();
}

equalsigns()
{
	int i;

	hil(2);
	for (i = 0; i < width; i++)
		opchar('=');
	resthil();
}

bad_cmd()
{
	sstatmes("Bad command");
}

void sstatmes(char *str)
{
	strcpy(statmes, str);
}

/* Call tgetsn, clearing string first */
tgets(buf, prompt)
uchar *buf, *prompt;
{
	*buf = '\0';
	tgetsn(buf, prompt);
}

/*
 * Print a string at the top and get response.
 */

tgetsn(buf, prompt)
uchar *buf, *prompt;
{
	register uchar *cp, *cp2;
	register int len;
	uchar lbuf[SEDLINESIZE];

	gotoxy(TGETPOS, STATUS);
	hil(2);
	PRINTF(prompt);
	clr_eol();
	strcpy(lbuf, buf);
	len = width - TGETPOS - strlen(prompt);
	if (len <= 15)
		len = 15;
	NLEDIT(lbuf, len);
	resthil();
	clearmem(buf, SEDLINESIZE);
/* Copy, changing any ^N's to CR-LF */
	cp = lbuf;
	cp2 = buf;
	do
	{
		if (*cp == 0x0e)
		{	/* Add  cr-lf if there is room */
			*cp2++ = '\r';
			*cp2++ = '\n';
		}
		else
			*cp2++ = *cp;
		cp++;
	}
	while (*cp && (cp2 < (buf + (SEDLINESIZE - 2))));
	*cp = '\0';
}

/*
 * Print a string and get a number. Return zero if bad number
 */

tgetnum(prompt)
char *prompt;
{
	uchar buf[SEDLINESIZE];

	tgets(buf, prompt);
	return atoi(buf);
}

/*
 * Ask for and evaluate the number of repetitions.
 */

getnreps()
{
	nreps = tgetnum("  Repetitions? ");
	if (!nreps)
		nreps++;
}

/*
 * Return true if the last line in the file is terminated by a CR-LF
 */

endatcr()
{
	return (MASK(*(end_buf - 2)) == '\r');
}

/*
 * Once-off initialisation
 */

sedinit()
{
	register struct window *wptr;

	wptr = (struct window *)DEF_WIND(1);
	width = igenv("SCREENWIDTH", wptr->x_end - wptr->x_start);
	height = igenv("SCREENHEIGHT", wptr->y_end - wptr->y_start);
	frozen = blkset = hilit = dlscren = srchstr[0] =
		subsold[0] = statmes[0] = 0;
	scr_top = SCR_TOP;
	newcur_ptr = blkend = blkstart = 0;
/* Look! */
	clearmem(markers, 40);
	clearmem(udbufs, sizeof(udbufs));
}

/*
 * Search backwards for a non-white after a white or for a stoppable uchar
 * If not called by the editor, startptr is the start of the string.
 */

uchar *
rev_fnw(ptr, startptr)
register uchar *ptr, *startptr;
{
	register int inwhite;	/* 0 = in non-white, 1 = in white */
	register int iss;

	if ((!startptr && (ptr == mem_buf)) || ((inwhite = issorwh(*--ptr)) == 2))
		return ptr;
	for ( ; ; )
	{
		if
		(
		  (
		    !startptr &&
		    (	/* Called by edit */
		      (ptr == mem_buf) ||
		      (MASK(*ptr) == '\r')
		    )
		  ) ||
		  (
		    (iss = issorwh(*ptr)) == 2
		  ) ||
		  (	/* Called by line editor */
			startptr && (ptr <= startptr)
		  )
		)
			return ptr;
		if (inwhite)	/* In white, waiting for non-white */
			inwhite = iss;
		else		/* In non-white, waiting for white */
			if (iss) return ptr + 1;
		ptr--;
	}
}

/*
 * Search forwards for a non-white after a white or for a stoppable uchar
 * If notedit == 1, this function is not being called by the editor.
 */

uchar *
fwd_fnw(ptr, notedit)
register uchar *ptr;
register char notedit;
{
	register int inwhite;	/* 0 = in non-white, 1 = in white */
	register int iss;

	inwhite = issorwh(*ptr);
	for ( ; ; )
	{
		if (!notedit && (ptr == end_buf))
			return end_buf;
		ptr++;
		iss = issorwh(*ptr);
		if ((!notedit && (MASK(*ptr) == '\r')) ||
			(*ptr == '\0') || (iss == 2))
			return ptr;
		if (inwhite == 1)	/* In white space, waiting for non-white */
		{
			if (!iss)
				return ptr;
		}
		else		/* In non-white, waiting for white */
			inwhite = iss;
	}
}

/*
 * Return 1 if white, 2 if stoppable uchar, else 0
 */

issorwh(ch)
register uint ch;
{
	if ((ch == ' ') || (ch == '\t') || (ch == '\n'))
		return 1;
	return (INDEX(".!,:;(){}/-", ch)) ? 2 : 0;
}

up_scrl(n)
int n;
{
	dellines(scr_top, n);
}

/* Delete a line */

dellines(linenum, nlines)
int linenum, nlines;
{
	if (linenum < height)
	{
		while (nlines--)
		{
			gotoxy(0, linenum);
			PRINTF(DELLINE);
		}
	}
}

/*
 *	move the cursor to the appropriate position.
 */

gotoxy(x, y)
int x, y;
{
	if (x >= width)
	{
		y += x / width;
		x %= width;
	}
#ifdef GPCPU
	PRINTF("\033%d;%dH", y + 1, x + 1);
#else
	opchar(MVCURS1);
	opchar(MVCURS2);
	opchar(y + ' ');
	opchar(x + ' ');
#endif
}


clr_eol()
{
	PRINTF(CLREOL);
}

clr_scrn()
{
	PRINTF(CLRSCRN);
}

dn_scrl()
{
	gotoxy(0, scr_top);
	PRINTF(INSLIN);	/* Insert a line at top of screen */
}

/* If highlight set, clear it */
tclrhil()
{
	if (hilit)	/* Select black background */
		hil(0);
}

/* Restore highlight */
resthil()
{
	hil((hilit) ? 1 : 0);	/* Select the background */
}

/* Set a background colour */
hil(val)
{
#ifdef GPCPU
	PRINTF("\033%dm", val);
#else
	PRINTF("\033B%c", val + 0x20);
#endif
}

statline(x, y)
{
	int i;
	char buf[200];

	gotoxy(0, STATUS);
	hil(2);
	/* Kludge here to get around limit of 5 args to system call */
	SPRINTF(buf, "%-15s Char=%-5u Line=%-4u ",
		statmes,
		cur_ptr-mem_buf+1+ibindex,
		cur_lin);
	SPRINTF(buf + strlen(buf), " Column=%-3u %2d%%  '%s'",
		cur_ptr-cur_lp+1+ibindex,
		(100 * (cur_ptr - mem_buf)) / (end_buf - mem_buf),
		file_name);

	i = strlen(buf);
	if (i >= width)
		i = width - 1;
	WRITE(STDOUT, buf, i);
	clr_eol();
	resthil();
	gotoxy(x, y);
}
