/*
 * Graphics driver
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
#include <storedef.h>
#include <windows.h>
#include <options.h>

EXTERN ushort mode_640;		/* If true, in 640 mode */
EXTERN ushort gfgcol;		/* Graphics foreground colour */
EXTERN ushort ogfgcol;		/* User's foreground colour */
EXTERN uint g_xsize, g_ysize;
EXTERN uint g_xstart, g_ystart;

EXTERN char *pixelbyte();
EXTERN ushort *vidram;		/* Pointer to start of video access RAM */

EXTERN int (*plotfunc)();	/* Pointer to our current graphics dot function */
EXTERN ushort curpmode;		/* Current plot mode */

int (*grwarn)();
extern int curpid, curhomeshell, syssafe;

static docircle();

/*
 * Initialise graphics stuff
 */

gr_init()
{
	grwarn = (int (*)())0;
	ogfgcol = 0xffff;
	calcgfgcol();
	SDOTMODE(PM_WRITE);		/* Default mode: write */
}

/* Inform external code about graphics use */
grtodo()
{
	if (grwarn)
	{
		(*grwarn)(curpid, curhomeshell);
		syssafe++;
	}
}

/* Finished with graphics */
grdone()
{
	if (grwarn)
		syssafe--;
}

/*
 * System call 61: set the dot mode
 *
 * PM_WRITE		0			Write pixel to video RAM
 * PM_OR		1			OR pixel with video RAM
 * PM_AND		2			AND ~(pixel) with video RAM
 * PM_XOR		3			XOR pixel with video RAM
 * PM_READ		4			Read current state
 * PM_SETVEC		5			Set vector for grwarn
 * PM_READVEC		6			Read vector for grwarn
 */

_sdotmode(mode, arg)
uint mode;
{
	if (mode < PM_READ)
	{
		curpmode = mode;
		setpmode();
	}
	else if (mode == 5)
	{
		grwarn = (int (*)())arg;
		return;
	}
	else if (mode == 6)
		return (int) grwarn;

	return curpmode;
}

/* Set the pixel plot entry point based upon mode_640 and curpmode */

setpmode()
{
	int hwritepix(), horpix(), handpix(), hxorpix();
	int lwritepix(), lorpix(), landpix(), lxorpix();
	static int ((*fns[])()) =
	{
		lwritepix, lorpix, landpix, lxorpix,	/* 320 mode */
		hwritepix, horpix, handpix, hxorpix	/* 640 mode */
	};
	plotfunc = fns[(mode_640 * 4 + curpmode) & 7];
}

/*
 * C coded line code: calls clipping dot functions for lines in which
 * one or both of the endpoints are outside the current window
 */

/* MUST CALL grtodo() before this function */
cline(x0, y0, x1, y1)
int x0, y0, x1, y1;
{
	int delta_x, delta_y, neg_x, neg_y, y;
	char swap_xy;
	register int xp, yp, s, x;

	delta_x = x1 - x0;
	delta_y = y1 - y0;
	if (neg_x = (delta_x < 0)) delta_x = -delta_x;
	if (neg_y = (delta_y < 0)) delta_y = -delta_y;
	if (swap_xy = (delta_x < delta_y))
	{
		s = delta_x;
		delta_x = delta_y;
		delta_y = s;
	}
	x = y = 0;
	s = -(delta_x / 2);
	do
	{
		if (swap_xy)
		{
			yp = x;
			xp = y;
		}
		else
		{
			xp = x;
			yp = y;
		}
		if (neg_x) xp = -xp;
		if (neg_y) yp = -yp;
		_set_pel(xp + x0, yp + y0, gfgcol);
		s += delta_y;
		x++;
		if (s > 0)
		{
			s -= delta_x;
			y++;
		}
	}
	while (x <= delta_x);
	grdone();
}

/*
 * Calculate graphics foreround colour mask based upon the user's colour and
 * the current horizontal mode
 */

calcgfgcol()
{
	gfgcol = colex(ogfgcol);
}

/*
 * System call 56: Set graphics foreground colour
 */

_sgfgcol(val)
int val;
{
	ogfgcol = val;	/* Original foreground colour */
	calcgfgcol();
}

/*
 * System call 59: Raw circle draw
 */

_rcircle(x0, y0, radius)
int x0, y0;
int radius;
{
	docircle(x0, y0, radius, plotfunc);	/* Draw circle using raw dot code */
}

/*
 * System call 60: Draw circle confined by window
 */

_circle(x0, y0, radius)
int x0, y0;
int radius;
{
	int _set_pel();

	docircle(x0, y0, radius, _set_pel);	/* Use window confined dot routine */
}

/*
 * Circle routine. Uses the supplied dot code
 */

static
docircle(x0, y0, radius, dotfn)
register int x0, y0;
int radius;
register int (*dotfn)();
{
	register int x, y, s;
	register int val;	/* Scrounge a register var */

	grtodo();
	x = radius;
	y = 0;
	s = -radius;
	val = gfgcol;

	do
	{
		(*dotfn)( x+x0,  y+y0, val);
		(*dotfn)( x+x0, -y+y0, val);
		(*dotfn)(-x+x0,  y+y0, val);
		(*dotfn)(-x+x0, -y+y0, val);
		(*dotfn)( y+x0,  x+y0, val);
		(*dotfn)( y+x0, -x+y0, val);
		(*dotfn)(-y+x0,  x+y0, val);
		(*dotfn)(-y+x0, -x+y0, val);
		s += y << 1;
		y++;
		if (s++ > 0)
		{
			x--;
			s -= x * 2;
		}
	}
	while (y <= x);
	grdone();
}

/*
 * Area fill routine.
 * Fill the area at x, y with 'val'. If val > 15, it is a pointer
 * to a 16 longword texture table.
 * This is a hacked up (and allegedly slightly broken) version of Jeremy Fitzhardinge's
 * algorithm.
 */

_fill(x, y, val)
register ushort x, y;
uint val;
{
#define UFFLAG 1
#define LFFLAG 2
#define STACKSPACE 4000
	register uint flags;
	ushort *xstack, *ystack;
	ushort xsave;
	uint sp;
	ushort absx;
	ushort xstart, ystart, xsize, ysize;

	register uint *scp, *uscp, *lscp;
	uint *scpmax, *scpmin;
	register uint *texture;		/* Pointer to texture array */
	register uint pixmask;		/* The current pixel mask */
	register uint bgval;		/* The current background value */
	uint leftmask, rightmask;	/* left & right aligned data masks */
	uint leftbg, rightbg;		/* left & right aligned bg colour values */
	uint smask;			/* Mask for x offset within word */
	ushort shiftdist;
	uint wfgcol, wbgcol;
	uint pmsave, bgsave;
	uint *scpsave, *lscpsave, *uscpsave;
	int opsave, retval;

	wbgcol = READ_PEL(x, y);
	if (wbgcol == (mode_640 ? val & 3 : val))
		return -1;		/* Filling with bg colour: error */

	grtodo();
	wfgcol = colex(val);
	wbgcol = colex(wbgcol);
	if (val < 16)
		texture = (uint *)0;
	else
		texture = (uint *)val;

	if (mode_640)
	{
		leftmask = 0xc0000000;	/* Left & right aligned pixel masks */
		rightmask = 0x3;
		shiftdist = 2;
		smask = 15;
	}
	else
	{
		leftmask = 0xf0000000;
		rightmask = 0xf;
		shiftdist = 4;
		smask = 7;
	}
	leftbg = leftmask & wbgcol;	/* Left & right aligned bg colour */
	rightbg = rightmask & wbgcol;

/* Check the texture array */
	if (texture)
	{
		uint i;
		for (i = 0; i < 16; i++)
		{
			bgval = texture[i];
			for (pixmask = leftmask; pixmask; pixmask >>= shiftdist)
			{
				if ((pixmask & wbgcol) == (pixmask & bgval))
				{
					retval = -1;
					goto out;
				}
			}
		}
	}
	xstart = g_xstart;			/* Window offsets in pixels */
	ystart = g_ystart;
	xsize = g_xsize;
	ysize = g_ysize;
	if ((x >= xsize) || (y >= ysize))	/* Initial point outside window */
	{
		retval = -1;
		goto out;
	}

	scpmin = (uint *)(vidram);		/* Start of video RAM */
	scpmax = scpmin + 0x2000;		/* End of video RAM */
	opsave = OPTION(OPT_MEMERRFLAG, 0);
	sp = 0;
	/* Get space for the backtrack stack */
	xstack = (ushort *)getmem0(STACKSPACE * sizeof(*xstack) * 2);
	OPTION(OPT_MEMERRFLAG, opsave);		/* Restore memory option */
	if (((int)xstack) < 0)
	{
		retval = (int)xstack;
		goto out;
	}
	ystack = xstack + STACKSPACE;
	retval = 0;

fill1:
	absx = x + xstart;
	/* Get pointer to video longword for the current pixel */
	scp = (uint *)((uint)pixelbyte(y + ystart, mode_640 ? absx : absx * 2) & ~3);
	uscp = scp - 0x800;		/* Pointer to row above */
	if (uscp < scpmin)
		uscp += 0x1fd8;
	lscp = scp + 0x800;		/* Pointer to row below */
	if (lscp >= scpmax)
		lscp -= 0x1fd8;
	pixmask	= leftmask >> ((absx & smask) * shiftdist);
	bgval = pixmask & wbgcol;
	if (texture)
		wfgcol = texture[y & 15];

	flags = UFFLAG|LFFLAG;		/* Set upper & lower full flags */

	if ((*scp & pixmask) != bgval)
		goto noline;
	xsave = x;
	pmsave = pixmask;
	bgsave = bgval;
	scpsave = scp; lscpsave = lscp; uscpsave = uscp;
rghtlin:
	*scp = (*scp & ~pixmask) | (wfgcol & pixmask);	/* We set a dot! */
	if (++y >= ysize)
		goto rghtdof;

	if ((*lscp & pixmask) != bgval)
	{
		flags |= LFFLAG;
		goto rghtdof;
	}
	if (!(flags & LFFLAG))
		goto rghtdof;
	flags &= ~LFFLAG;
	xstack[sp] = x; ystack[sp++] = y;
	if (sp >= STACKSPACE)
		goto spoverflow;
rghtdof:
	if ((y -= 2) >= ysize)
		goto rghtupf;
	if ((*uscp & pixmask) != bgval)
	{
		flags |= UFFLAG;
		goto rghtupf;
	}
	if (!(flags & UFFLAG))
		goto rghtupf;
	flags &= ~UFFLAG;
	xstack[sp] = x; ystack[sp++] = y;
	if (sp >= STACKSPACE)
		goto spoverflow;
rghtupf:
	y++;
	if (++x >= xsize)
		goto stprght;
	pixmask >>= shiftdist;
	if (!pixmask)
	{
		scp++; uscp++; lscp++;
		bgval = leftbg; pixmask = leftmask;
	}
	else
		bgval >>= shiftdist;
	if ((*scp & pixmask) == bgval)
		goto rghtlin;
stprght:
	x = xsave;
	pixmask = pmsave;
	bgval = bgsave;
	scp = scpsave; lscp = lscpsave; uscp = uscpsave;
	flags = UFFLAG|LFFLAG;

leftlin:
	x--;
	pixmask <<= shiftdist;
	if (!pixmask)
	{
		scp--; uscp--; lscp--;
		bgval = rightbg; pixmask = rightmask;
	}
	else
		bgval <<= shiftdist;
	if (x >= xsize)
		goto noline;
	if ((*scp & pixmask) != bgval)
		goto noline;
	*scp = (*scp & ~pixmask) | (wfgcol & pixmask);

	if (++y >= ysize)
		goto leftdof;
	if ((*lscp & pixmask) != bgval)
	{
		flags |= LFFLAG;
		goto leftdof;
	}
	if (!(flags & LFFLAG))
		goto leftdof;
	flags &= ~LFFLAG;
	xstack[sp] = x; ystack[sp++] = y;
	if (sp >= STACKSPACE)
		goto spoverflow;
leftdof:
	y -= 2;
	if (y >= ysize)
		goto leftupf;
	if ((*uscp & pixmask) != bgval)
	{
		flags |= UFFLAG;	/* Fixed */
		goto leftupf;
	}
	if (!(flags & UFFLAG))
		goto leftupf;
	flags &= ~UFFLAG;
	xstack[sp] = x; ystack[sp++] = y;
	if (sp >= STACKSPACE)
	{
spoverflow:
		retval = -1;
		goto fillexit;
	}
leftupf:
	y++;
	goto leftlin;
noline:
out:
	if (!sp)
	{
fillexit:
		dofreemem(xstack);
		grdone();
		return retval;
	}
	x = xstack[--sp]; y = ystack[sp];
	goto fill1;
}

/*
 * Expand a colour into a 32 bit repeating mask, based upon current horizontal mode
 */

colex(m)
register uint m;
{
	if (mode_640)
	{
		m &= 3;
		m |= (m << 2);
	}
	m &= 0x000f;
	m |= m << 4;
	m |= m << 8;
	m |= m << 16;
	return m;
}
