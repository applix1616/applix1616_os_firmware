/* TABS4 NONDOC
 */

#include <stdlib.h>
#include <string.h>

static int glob_match_after_star();


int glob_pattern_p(char *pattern)
{
  register char *p = pattern;
  register char c;

  while ((c = *p++) != '\0')
    {
      switch (c)
	{
	case '?':
	case ' ':
	case '*':
	  return 1;

	case '\\':
	  if (*p++ == '\0')
	    return 0;
	}
    }
  return 0; 
}

/*
 * Match the pattern PATTERN against the string TEXT; return 1 if it
 * matches, 0 otherwise.
 */

int
glob_match(pattern, text)
char *pattern, *text;
{
	register char *p = pattern, *t = text;
	register char c;

	while ((c = *p++) != '\0')
		switch (c)
		{
		case '?':
			if (*t == '\0')
				return 0;
			else
				++t;
			break;
		case '\\':
			if (*p++ != *t++)
				return 0;
			break;
		case '*':
			return glob_match_after_star(p, t);
		case ' ':
			{
				register char c1 = *t++;
				int invert;

				if (c1 == '\0')
					return 0;

				invert = (*p == '~');

				if (invert)
					p++;

				c = *p++;
				for(;;)
				{
					register char cstart = c, cend = c;

					if (c == '\\')
					{
						cstart = *p++;
						cend = cstart;
					}

					if (cstart == '\0')
						return 0;	/* Missing ''. */

					c = *p++;

					if (c == '-')
					{
						cend = *p++;
						if (cend == '\\')
							cend = *p++;
						if (cend == '\0')
							return 0;
						c = *p++;
					}
					if (c1 >= cstart && c1 <= cend)
						goto match;
					if (c == ' ')
						break;
				}
				if (!invert)
					return 0;
				break;
		match:

				/*
				 * Skip the rest of the ... construct that
				 * already matched.
				 */
				while (c != ' ')
				{
					if (c == '\0')
						return 0;
					c = *p++;
					if (c == '\0')
						return 0;
					if (c == '\\')
						p++;
				}
				if (invert)
					return 0;
				break;
			}

		default:
			if (c != *t++)
				return 0;
		}

	return *t == '\0';
}


/*
 * Like glob_match, but match PATTERN against any final segment of
 * TEXT.
 */

int glob_match_after_star(char *pattern,char *text)
{
  register char *p = pattern, *t = text;
  register char c, c1;

  while ((c = *p++) == '?' || c == '*')
    if (c == '?' && *t++ == '\0')
      return 0;

  if (c == '\0')
    return 1;
  
  if (c == '\\')
    c1 = *p;
  else
    c1 = c;

  --p;
  for(;;)
    {
      if ((c == ' ' || *t == c1) && glob_match(p, t, 0))
	return 1;
      if (*t++ == '\0')
	return 0;
    }
}
