
#include <string.h>


/*
 * Code for interpreting block error codes
 */

static char *blkerrs[] =
{
	"Error code -1",
	"Write protected",
	"Bad block device",
	"No space for driver",
	"I/O error",
	"Invalid block",
	"Disk full",
	"Invalid fd",

	"Bad filename",
	"File full",
	"File not open",
	"File not open for reading",
	"File not open for writing",
	"File open for reading",
	"File open for writing",
	"Too many files open",
	"No such file or directory",
	"Read past EOF",
	"Duplicate filename",
	"Bad argument",
	"Seek past EOF",
/*	"Write-only (cannot seek)",*/
	"",
	"File is open",
	"Out of memory",
	"Directory",
	"Not a directory",
	"Directory not empty",
	"Directory is full",
	"File is locked",
	"Bad magic number",
	"User interrupt",
	"Permission denied",
	"Exec depth exceeded",
	"Process count exceeded",
	"Process killed",
	"Read/write closed pipe",
	"Invalid PID",
	"File/directory exists"
};

/*
 * System call 114: Interpret a block error code, put diagnostic message at *buf
 * Return pointer to buf.
 */

char *_interpbec(register int bec, register char *buf)
{
	if (bec >= 0)
		SPRINTF(buf, "Code $%x: no error", bec);
	else
	{
		bec = -1 - bec;	/* Convert into index */
		if (bec >= (sizeof(blkerrs) / sizeof(*blkerrs)))
			SPRINTF(buf, "Error code -%d", bec + 1);
		else
			strcpy(buf, blkerrs[bec]);
	}
	return buf;
}

/*
 * System call 122: Return pointer to error message string
 */

char *_errmes(register int bec)
{
	if (bec >= 0)
		return "No error";
	bec = -1 - bec;
	if (bec >= (sizeof(blkerrs) / sizeof(*blkerrs)))
		return "Unknown error";
	return(blkerrs[bec]);
}
