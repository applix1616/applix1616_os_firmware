/*
 * Enable and disable Timer1 output interrputs
 */

#include <types.h>
#include <syscalls.h>
#include <hwdefs.h>
#include <via.h>
//#include <splfns.h>

short t1intson;

/*
 * System call 26: Enable interrupt entry to the supplied code at
 * the supplied period
 */

_ent1ints(isrvec, preload)		/* Start the interrupts */
int isrvec;
register uint preload;
{
	register int s;

	if (CPUSPEED())
		preload *= 2;
	s = CAS_SPL();		/* Disable interrupts */

	*((int *) CAS_IVEC) = isrvec;

	*V_ACR |= 0xc0;		/* Timer 1 continuous mode, PB7 O/P */
	*V_T1CL = preload & 0xff;
	*V_T1CH = preload >> 8;
	*V_DDRB |= 0x80;	/* Make PB7 an output */
	*V_DDRA |= ECASWIRQ;	/* Prepare to set ECASWIRQ low */
	*V_AREG &= ~ECASWIRQ;	/* Enable write interrupts */
	t1intson = 1;
	_splx(s);		/* Enable interrupts (we hope) */
}

/*
 * System call 27: Disable the interrupts
 */

_dist1ints()
{
	int s;

	s = CAS_SPL();
	*V_AREG |= ECASWIRQ;	/* Disable write interrupts */
	*V_DDRA &= ~ECASWIRQ;	/* Make it harder to do damage */
	*V_ACR &= (char) ~0xc0;	/* Stop the timer */
	*V_BREG &= ~80;		/* Drop PB7 to ensure no interrupts */
	t1intson = 0;
	_splx(s);
}
