/* TABS4 */

#define READCHECK
#define rdchk(fdes) SGETC(fdes)

#include <string.h>
#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <scc.h>
#include <chario.h>
#include "zmodem.h"

extern unsigned		Baudrate;
extern long			Bytesleft, AKPMtotsize;
extern int			Rxtimeout;
extern long			Txpos;
extern long			Rxpos;
extern char			Txhdr[];
extern char			Rxhdr;
extern char			*ZPathname;
extern char			*Ziobuf;
extern int			Zfile_fd;
extern int			Zifd;				/* raw descriptor of input */
extern int			Zofd;				/* raw descriptor of output */

int					blklen;				/* length of transmitted records */
unsigned			Rxbuflen;			/* Receiver's max buffer length */
int					Tframlen;			/* Override for tx frame length */
long				Lastread;			/* Beginning offset of last buffer read */
int					Lastc;				/* Count of last buffer read or -1 */
char				Dontread;			/* Don't read the buffer, it's still there */

char *room0for();

sz_main(argc, argv)
char *argv;
{
	int exitcode = 0;

	blklen = SECSIZ;
	Rxbuflen = 16384;
	Tframlen = 0;

	Rxtimeout = 600;

	stohdr(0L);
	zshhdr(ZRQINIT, Txhdr);

	if (wcsend(argc, argv) == ERROR)
	{
		exitcode = 1;
		canit();
	}
	return exitcode;
}

wcsend(argc, argp)
char *argp[];
{
	register n;

	for (n = 0; n < argc; ++n)
	{
		if (argp[n][0] != '-' && wcs(argp[n]) == ERROR)
			return ERROR;
		AMprintf("");
	}
	saybibi();
	return OK;
}

wcs(oname)
char *oname;
{
	char name[PATHLEN];

	strcpy(name, oname);

	if ((Zfile_fd = OPEN(oname, 1)) < 0)
		return OK;	/* pass over it, there may be others */
	Lastread = 0;
	Lastc = -1;
	Dontread = FALSE;

	switch (wctxpn(name))
	{
	case ERROR:
		return ERROR;
	case ZSKIP:
		return OK;
	}
	return 0;
}

/*
 * generate and transmit pathname block consisting of
 *  pathname (null terminated),
 *  file length, mode time and file mode in octal
 *  as provided by the Unix fstat call.
 *  N.B.: modifies the passed name, may extend it!
 */
wctxpn(name)
char *name;
{
	register char *p, *q;
	struct dir_entry dirent;

	ZPathname = room0for(name);

	for (p = name, q = Ziobuf; *p; )
	{
		if ((*q++ = *p++) == '/')
			q = Ziobuf;
	}
	*q++ = 0;
	p = q;
	while (q < (Ziobuf + KSIZE))
		*q++ = 0;
	if ((Zfile_fd != SET_SIP(-1)) && *name && (FILESTAT(name, &dirent) >= 0))
		SPRINTF(p, "%u 00 00", dirent.file_size);

	/* force 1k blocks if name won't fit in 128 byte block */
	if (Ziobuf[125])
		blklen = KSIZE;
	else
	{			/* A little goodie for IMP/KMD */
		blklen = SECSIZ;
		Ziobuf[127] = (dirent.file_size + 127) >> 7;
		Ziobuf[126] = (dirent.file_size + 127) >> 15;
	}
	Bytesleft = AKPMtotsize = dirent.file_size;
	return zsendfile(Ziobuf, 1 + strlen(p) + (p - Ziobuf));
}

/* fill buf with count chars */
/* return number read */
zfilbuf(buf, count)
register char *buf;
{
	int ret;

	ret = READ(Zfile_fd, buf, count);
	if (ret < 0)
		return 0;
	return ret;
}

/* Send file name and related info */
zsendfile(buf, blen)
char *buf;
{
	register c;

	for (;;)
	{
		Txhdr[ZF0] = 0;	/* file conversion request */
		Txhdr[ZF1] = 0;	/* file management request */
		Txhdr[ZF2] = 0;	/* file transport request */
		Txhdr[ZF3] = 0;
		zsbhdr(ZFILE, Txhdr);
		zsdata(buf, blen, ZCRCW);
again:
		c = zgethdr(Rxhdr);
		switch (c)
		{
		case ZRINIT:
			goto again;
		case ZCAN:
		case TIMEOUT:
		case ZABORT:
		case ZFIN:
			return ERROR;
		case ZSKIP:
			CLOSE(Zfile_fd);
			return c;
		case ZRPOS:
			SEEK(Zfile_fd, Rxpos, 0);
			Txpos = Rxpos;
			Lastc = -1;
			Dontread = FALSE;
			return zsendfdata();
		default:
			continue;
		}
	}
}

/* Send the data in the file */
zsendfdata()
{
	register c, e;
	register newcnt;
	static struct scc_prog sccSpeed;

	/* get speed	 */
	if (CDMISC(Zofd, CDM_READMODE, &sccSpeed, 0, 0) < 0)
		Baudrate = 9600;/* AKPM: Erk */
	else
		Baudrate = sccSpeed.brate;

	if (Baudrate == 0)
		Baudrate = 9600;

	if (Baudrate > 300)
		blklen = 256;
	if (Baudrate >= 2400)
		blklen = KSIZE;
	if (Rxbuflen && blklen > Rxbuflen)
		blklen = Rxbuflen;

	goto horrible_code_chuck;
somemore:
waitack:
	c = getinsync();
	switch (c)
	{
	default:
	case ZCAN:
		CLOSE(Zfile_fd);
		return ERROR;
	case ZSKIP:
		CLOSE(Zfile_fd);
		return c;
	case ZACK:
	case ZRPOS:
		break;
	case ZRINIT:
		return OK;
	}

#ifdef READCHECK
	/*
	 * If the reverse channel can be tested for data, this logic may be
	 * used to detect error packets sent by the receiver, in place of
	 * setjmp/longjmp rdchk(fdes) returns non 0 if a character is
	 * available
	 */
	while (rdchk(Zifd))
	{
		switch (readline(1))
		{
		case CAN:
		case ZPAD:
			goto waitack;
		case XOFF:	/* Wait a while for an XON */
		case XOFF | 0200:
			readline(100);
		}
	}
#endif

horrible_code_chuck:

	newcnt = Rxbuflen;
	stohdr(Txpos);
	zsbhdr(ZDATA, Txhdr);

	do
	{
		if (Dontread)
			c = Lastc;
		else
		{
			c = zfilbuf(Ziobuf, blklen);
			Lastread = Txpos;
			Lastc = c;
		}
		Dontread = FALSE;
		if (c < blklen)
			e = ZCRCE;
		else if (Rxbuflen && (newcnt -= c) <= 0)
			e = ZCRCW;
		else
			e = ZCRCG;
		zsdata(Ziobuf, c, e);
		Txpos += c;
		if (e == ZCRCW)
			goto waitack;

#ifdef READCHECK
		/*
		 * If the reverse channel can be tested for data, this logic
		 * may be used to detect error packets sent by the receiver,
		 * in place of setjmp/longjmp rdchk(fdes) returns non 0 if a
		 * character is available
		 */
		while (rdchk(Zifd))
		{
			switch (readline(1))
			{
			case CAN:
			case ZPAD:
				/* zcrce - dinna wanna start a ping-pong game */
				zsdata(Ziobuf, 0, ZCRCE);
				goto waitack;
			case XOFF:	/* Wait a while for an XON */
			case XOFF | 0200:
				readline(100);
			}
		}
#endif
	} while (c == blklen);

	for (;;)
	{
		stohdr(Txpos);
		zsbhdr(ZEOF, Txhdr);
		switch (getinsync())
		{
		case ZACK:
			continue;
		case ZRPOS:
			goto somemore;
		case ZRINIT:
			return OK;
		case ZSKIP:
			CLOSE(Zfile_fd);
			return c;
		default:
			CLOSE(Zfile_fd);
			return ERROR;
		}
	}
}

/*
 * Respond to receiver's complaint, get back in sync with receiver
 */
getinsync()
{
	register c;

	for (;;)
	{
		c = zgethdr(Rxhdr);
		switch (c)
		{
		case ZCAN:
		case ZABORT:
		case ZFIN:
		case TIMEOUT:
			return ERROR;
		case ZRPOS:
			if (Lastc >= 0 && Lastread == Rxpos)
				Dontread = TRUE;
			else
				SEEK(Zfile_fd, Rxpos, 0);
			Txpos = Rxpos;
			return c;
		case ZACK:
			return c;
		case ZRINIT:
		case ZSKIP:
			CLOSE(Zfile_fd);
			return c;
		case ERROR:
		default:
			zsbhdr(ZNAK, Txhdr);
			continue;
		}
	}
}

/*	Say "bibi" to the receiver, try to do it cleanly
*/
saybibi()
{
	for (;;)
	{
		stohdr(0L);
		zsbhdr(ZFIN, Txhdr);
		switch (zgethdr(Rxhdr))
		{
		case ZFIN:
			sendline('O');
			sendline('O');
			sl_bflush();
		case ZCAN:
		case TIMEOUT:
			return;
		}
	}
}
