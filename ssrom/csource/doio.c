/* TABS8 NONDOC
 * Low-level communication with block device drivers
 */

#ifdef IOCACHE

#include <types.h>
#include <syscalls.h>
#include <files.h>
#include <blkio.h>
#include "debug.h"

#define INVALIDDVR	((ushort)-1)

struct iocache
{
	ushort block;
	ushort bdvrnum;
	uint age;
	uchar dataBLOCKSIZE;
};

uint iocachesize;
struct iocache *iocache;
ulong cachedevs;		/* Mask of cached devices */

EXTERN struct blkdriver bdrivers;
EXTERN char indirblock;
EXTERN int reslevel;

iocache_init(size)
{
	register int i;

	if (reslevel == 0)
		cachedevs = 0;

	DOCACHEING = 0;
	iocachesize = size;
	if (size)
	{
		iocache = (struct iocache *)
			getmem1(iocachesize * sizeof(*iocache));
		for (i = 0; i < iocachesize; i++)
		{
			iocachei.bdvrnum = INVALIDDVR;
			iocachei.age = i;
		}
	}
}

/*
 * At last, perform the device driver call
 */

doio(bdvrnum, isread, addr, firstblock, nblocks)
register uint bdvrnum;
uint isread, addr;
register uint firstblock;
uint nblocks;
{
	struct blkdriver *bdptr;
	register uint block;
	register int retval, (*rw)();
	register struct iocache *iocptr;
	uint max, maxpos;

	bdptr = &bdriversbdvrnum;

	if ((nblocks == 1) && (cachedevs & (1<<bdvrnum)) &&
		iocachesize && indirblock && (block > 1))
	{
		/* Search cache */
#ifdef DEBUG
		if (CACHEDEBUG)
			conprintf("%c:b=%d, idb=%d", isread ? 'R' : 'W',
#endif
		firstblock, indirblock);
		for (iocptr = iocache; iocptr < iocache + iocachesize; iocptr++)
		{
			if (iocptr->bdvrnum == bdvrnum &&
					iocptr->block == firstblock)
			{	/* Hit! */
#ifdef DEBUG
				if (CACHEDEBUG)
					conprintf("Hit %d ", iocptr - iocache);
#endif
				if (isread)
				{
					movmem(iocptr->data, addr, BLOCKSIZE);
					retval = 0;
				}
				else
				{	/* Writing: move new data */
					movmem(addr, iocptr->data, BLOCKSIZE);
					retval = (*bdptr->bwrite)(firstblock, addr);
				}
				/* Make it newer in cache */
				iocptr->age = 0;
				for (iocptr = iocache; iocptr < iocache + iocachesize; iocptr++)
					iocptr->age++;
				return retval;
			}
		}

		/* Not there: find oldest cache entry */
		max = 0;
		for (iocptr = iocache; iocptr < iocache + iocachesize; iocptr++)
		{
			if (iocptr->age > max)
			{
				maxpos = iocptr - iocache;
				max = iocptr->age;
			}
		}

#ifdef DEBUG
		if (CACHEDEBUG)
			conprintf("Miss. evicting %d ", maxpos);
#endif
		iocptr = iocache + maxpos;
		if (isread)
		{
			retval = (*bdptr->bread)(firstblock, iocptr->data);
			if (retval < 0)	
				iocptr->bdvrnum = INVALIDDVR;
			else
			{
				iocptr->bdvrnum = bdvrnum;
				iocptr->block = firstblock;
				movmem(iocptr->data, addr, BLOCKSIZE);
			}
		}
		else
		{
			movmem(addr, iocptr->data, BLOCKSIZE);	/* Our copy */
			iocptr->bdvrnum = bdvrnum;
			iocptr->block = firstblock;
			retval = (*bdptr->bwrite)(firstblock, addr);
		}
		/* Make it newer in cache */
		iocptr->age = 0;
		for (iocptr = iocache; iocptr < iocache + iocachesize; iocptr++)
			iocptr->age++;
		return retval;
	}

	if (bdptr->multirw)
		retval = (*bdptr->multirw)(isread, addr, firstblock, nblocks);
	else
	{
		/*
	 	 * If the device driver does not support multi-block I/O,
		 * fudge it.
		 */
		rw = (isread) ? bdptr->bread : bdptr->bwrite;
		for (block = 0; block < nblocks; block++)
		{
			retval = (*rw)(firstblock++, addr);
			addr += BLOCKSIZE;
			if (retval < 0)
				goto err;
		}
	}
err:	return retval;
}

/*
 * Invalidate device cache entries
 */

iocachetrash(bdvrnum)
{
	register struct iocache *iocptr;

	for (iocptr = iocache; iocptr < iocache + iocachesize; iocptr++)
	{
		if (iocptr->bdvrnum == bdvrnum)
		{
			iocptr->bdvrnum = INVALIDDVR;
			iocptr->age = 0x40000000;
		}
	}
}
#endif /*IOCACHE*/
