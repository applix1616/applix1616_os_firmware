/* General function */

#include <syscalls.h>


#include "cantdo.h"

int _cantdo(char *file,int ec,char *mes)
{
	trap7(120, STDERR, "Cannot %s %s: ", mes, file); /* fprintf to stderr */
	return dobec(ec);
}
