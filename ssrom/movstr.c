/* Move strings, terminating to correct length. Return pointer to new string */

#include "movmem.h"
#include "movstr.h"


char *movstr(char *to,char * from,int len)
{
	_movmem(from, to, len);
	to[len - 1] = '\0';
	return to;
}
