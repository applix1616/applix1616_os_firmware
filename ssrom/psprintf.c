/* printf/sprintf/puts/fputs code */
//#include <string.h>
#include <types.h>
#include <syscalls.h>


#include "mondefs.h"
#include <files.h>
#include "envbits.h"
#include <options.h>
#include "index.h"
#include "psprintf.h"


#define PMAXLINE 1024

int enerrbeep;			/* If true, can beep on errors */

extern uint stdout, stderr, conout;
extern int miscmodes;

void _doprint(int fd,char **pformat);

/* A buffer for speeding up output */
#define PBSIZE	256
struct printbuf
{
	ushort index;
	char buf[PBSIZE];
	int fd;
};

/* System call 48: formatted output */
void _printf(void *format ) /*char *format)*/
{
	_doprint(stdout,(char **) &format);
}

/* Print nasty internal message */
void conprintf(void *format,...)
{
	_doprint(((stderr < NIODRIVERS) && (miscmodes & OPM_STDERR)) ?
			stderr : 0, (char **)&format);
}
/* System call 120: fprintf */
void _fprintf(int fd, char *format)
{
	_doprint(fd, &format);
}
/* Error routine: send output to stderr with beep. Suppress double beeps */
int eprintf(char *format)
{
	_doprint(STDERR, &format);
	if (enerrbeep && envbits(ENVB_IBBEEP) && index(format, (int)'\n'))
		PUTC(STDERR, 7);
	return -1;
}

void _doprint(int fd,char **pformat)
{
	int _putinbuf();
	struct printbuf printbuf;

	printbuf.index = 0;
	printbuf.fd = fd;
	_spr(pformat, _putinbuf, &printbuf);
	if (printbuf.index)
		WRITE(fd, printbuf.buf, printbuf.index);
}

_putinbuf(ch, p)
char ch;
register struct printbuf *p;
{
	p->buf[p->index++] = ch;
	if (p->index == PBSIZE)
	{
		WRITE(p->fd, p->buf, PBSIZE);
		p->index = 0;
	}
}

static void _sspr(char c,char **strptr)
{
	 	*(*strptr)++ = c;
}

/* System call 49: sprintf function */
void _sprintf(char *buffer, char *format)
{
	_spr(&format, _sspr, &buffer);	/* call _spr to do all the work */
	*buffer = '\0';
}

void _spr(char **fmt,register int (*putcf)(), char **arg1)
{
	register char c;
	register int base;
	register int width, precision;
	register char *sptr;
	register int *args;
	char *format, *wptr, wbuf[PMAXLINE], pf, ljflag, zfflag;

	format = *fmt++;    	/* fmt first points to the format string */
	args = (int *)fmt;	/* now fmt points to the first arg value */

	while (c = *format++)
	{
		if (c == '%')
		{
			wptr = wbuf;
			precision = 6;
			ljflag = 0;
			pf = 0;
			zfflag = 0;

			if (*format == '-')
			{
				format++;
				ljflag++;
			}

			if (*format == '0') zfflag++;	/* test for zero-fill */
			width = (isdigit(*format)) ? _gv2(&format) : 0;

			if ((c = *format++) == '.')
			{
				precision = _gv2(&format);
				pf++;
				c = *format++;
			}

			switch(toupper(c))
			{
			case 'D':
				if (*args < 0)
				{
					*wptr++ = '-';
					*args = -*args;
					width--;
				}
			case 'U':
				base = 10;
				goto val;
			case 'B':
				base = 2;
				goto val;
			case 'O':
				base = 8;
				goto val;
			case 'X':
				base = 16;

val:  				width -= _uspr(&wptr, *args++, base);
				goto pad;
			case 'C':
				*wptr++ = *args++;
				width--;
				goto pad;
			case 'E':	/* System error message */
				sptr = (char *)ERRMES(*args++);
				goto doerr;
			case 'S':
				sptr = (char *)*args++;
				if (sptr == 0)
					sptr = "(null)";
doerr:				if (!pf)
					precision = PMAXLINE;
				while (*sptr && precision &&
					(wptr - wbuf < (PMAXLINE - 1)))
				{
					*wptr++ = *sptr++;
					precision--;
					width--;
				}

pad:				*wptr = '\0';
				wptr = wbuf;
				if (!ljflag)
					while (width-- > 0)
						(*putcf)(zfflag ? '0' : ' ', arg1);

				while (*wptr)
					(*putcf)((char)(*wptr++), arg1);

				if (ljflag)
					while (width-- > 0)
						(*putcf)(' ', arg1);
				break;

			default:
				(*putcf)(c, arg1);
			}
		}
		else
		{
			(*putcf)(c, arg1);
		}
	} /* while */
}

/*
 * Internal routine used by "_spr" to perform ascii-
 * to-decimal conversion and update an associated pointer:
 */

int _gv2(sptr)
register char **sptr;
{
	register int n;

	n = 0;
	while (isdigit(**sptr)) n = 10 * n + *(*sptr)++ - '0';
	return n;
}

_uspr(string, n, base)
register char **string;
register uint n, base;
{
	register int length;

	if (n < base)
	{
		*(*string)++ = (n < 10) ? n + '0' : n + 55;
		return 1;
	}

	length = _uspr(string, n / base, base);
	_uspr(string, n % base, base);

	return length + 1;
}

/*
 * System call 121: fputs
 */

_fputs(fd, str)
int fd;
char *str;
{
	int retval;

	while (*str)
	{
		if ((retval = PUTC(fd, *str++)) < 0)
			return retval;
	}
	return 0;
}

/*
 * System call 123: fgets
 */

char *
_fgets(fd, buf)
int fd;
char *buf;
{
	char *fgets2();
	return fgets2(fd, buf, MAXIPLINE);
}

char *
fgets2(fd, buf, count)
register int fd;
register char *buf;
register int count;
{
	register int ch;
	register char *ptr;

	ptr = buf;
	count--;

	if ((ch = GETC(fd)) == '\n')
		ch = GETC(fd);
	if (ch < 0)
		return (char *)0;

	/* V4.7: add \n case */
	while ((ch != '\r') && (ch != '\n') && (ch >= 0) && (--count))
	{
		*ptr++ = ch;
		ch = GETC(fd);
	}

	*ptr = '\0';
	return buf;
}
